<table width="550">
<#escape x as x!>
  <tr>
    <td colspan="2">尊敬的${userName}:</b></font></td>
  </tr>
  <tr>
    <td colspan="2">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;您好!请求编号为：${eventCode}的请求升级给您！</td>
  </tr>
  
  <#if (historyRecordDTO?exists && historyRecordDTO?size>0) >
	  <tr>
		<td colspan="2">
			<table>
				<tr><td>动作</td><td>详细</td><td>操作者</td><td>操作时间</td></tr>
				<#list historyRecordDTO as historyRecord>
					<tr><td>${historyRecord.logTitle}</td><td>${historyRecord.logDetails}</td><td>${historyRecord.operator}</td><td>${historyRecord.createdTime?string("yyyy-MM-dd HH:mm:ss")}</td></tr>
				</#list>
			</table>
		</td>
	  </tr>	
  </#if>
  <tr>
    <td colspan="2"><br>谢谢！</td>
  </tr>
  <tr>
    <td colspan="2">Wstuo Team</td>
  </tr>


</#escape>
</table>