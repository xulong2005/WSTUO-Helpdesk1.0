package com.wstuo.itsm.config.visit.action;

import org.springframework.beans.factory.annotation.Autowired;

import com.opensymphony.xwork2.ActionSupport;
import com.wstuo.itsm.config.visit.dto.VisitDTO;
import com.wstuo.itsm.config.visit.dto.VisitItemDTO;
import com.wstuo.itsm.config.visit.service.IVisitItemService;
import com.wstuo.itsm.config.visit.service.IVisitService;
import com.wstuo.common.dto.PageDTO;

/**
 * 请求回访Action类
 * @author QXY
 *
 */
@SuppressWarnings("serial")
public class VisitAction extends ActionSupport {

	@Autowired
	private IVisitItemService visitItemService;
	@Autowired
	private IVisitService visitService;
	
	private PageDTO paDto;
	/**
	 * 回访DTO
	 */
	private VisitDTO visitDTO;
	private VisitItemDTO visitItemDTO;
	private int page = 1;
    private int rows = 10;
    private String sord;
    private String sidx;
    private Long[] ids;
    /**
     * 回访编号
     */
    private Long visitNo;
    private boolean useStatus=false;
  
	public PageDTO getPaDto() {
		return paDto;
	}
	public void setPaDto(PageDTO paDto) {
		this.paDto = paDto;
	}
	public VisitDTO getVisitDTO() {
		return visitDTO;
	}
	public void setVisitDTO(VisitDTO visitDTO) {
		this.visitDTO = visitDTO;
	}
	public VisitItemDTO getVisitItemDTO() {
		return visitItemDTO;
	}
	public void setVisitItemDTO(VisitItemDTO visitItemDTO) {
		this.visitItemDTO = visitItemDTO;
	}
	public int getPage() {
		return page;
	}
	public void setPage(int page) {
		this.page = page;
	}
	public int getRows() {
		return rows;
	}
	public void setRows(int rows) {
		this.rows = rows;
	}
	public String getSord() {
		return sord;
	}
	public void setSord(String sord) {
		this.sord = sord;
	}
	public String getSidx() {
		return sidx;
	}
	public void setSidx(String sidx) {
		this.sidx = sidx;
	}
	public Long[] getIds() {
		return ids;
	}
	public void setIds(Long[] ids) {
		this.ids = ids;
	}
    
	
	public Long getVisitNo() {
		return visitNo;
	}
	public void setVisitNo(Long visitNo) {
		this.visitNo = visitNo;
	}
	
	public boolean isUseStatus() {
		return useStatus;
	}
	public void setUseStatus(boolean useStatus) {
		this.useStatus = useStatus;
	}
	/**
	 * 回访项
	 * @return String
	 */
	public String visitFindPager(){
		int start = (page - 1) * rows;
		paDto = visitService.findPagerVisit(visitDTO,start,rows, sord, sidx,useStatus);
		paDto.setRows(rows);
		paDto.setPage(page);
		return  SUCCESS;
	}
	
	/**
	 * 回访项添加
	 * @return String
	 */
	public String visitSave(){
		
		visitService.visitSave(visitDTO);
		return  SUCCESS;
	}
	
	/**
	 * 回访项更新
	 * @return String
	 */
	public String visitUpdate(){
		
		visitService.visitUpdate(visitDTO);
		return  SUCCESS;
	}
	
	/**
	 * 回访项删除
	 * @return String
	 */
	public String visitDelete(){
		visitService.visitDelete(ids);
		return  SUCCESS;
	}
	
	
	/**
	 * 查询回访项
	 * @return String
	 */
	public String findVisit(){
		int start = (page - 1) * rows;
		paDto = visitService.findAllVisit(visitDTO,start, rows, sord, sidx,useStatus);
		paDto.setRows(rows);
		paDto.setPage(page);
		return  SUCCESS;
	}
	
	/**---------------------------------------------------------------*/
	
	/**
	 * 回访选择项查询
	 * @return String
	 */
	public String visitItemFindPager(){
		paDto = visitItemService.findPagerVisitItems(visitNo,page,rows, sord, sidx);
		paDto.setRows(rows);
		paDto.setPage(page);
		return  SUCCESS;
	}
	
	/**
	 * 回访选择项添加
	 * @return String
	 */
	public String visitItemSave(){
		
		
		visitItemService.visitItemSave(visitItemDTO);
		
		return  SUCCESS;
	}
	
	/**
	 * 回访选择项更新
	 * @return String
	 */
	public String visitItemUpdate(){
		
		visitItemService.visitItemUpdate(visitItemDTO);
		return  SUCCESS;
	}
	
	/**
	 * 回访选择项删除
	 * @return String
	 */
	public String visitItemDelete(){
		
		visitItemService.visitItemDelete(visitNo,ids);
		return  SUCCESS;
	}
	
	/**
	 * 回访保存
	 * @return String
	 */
	public String visitResult(){

		return  SUCCESS;
	}
	
	
	/**
	 * 请求回访
	 * @return String
	 */
	public String requestVisit(){
		int start = (page - 1) * rows;
		rows=1000;
		paDto = visitService.findAllVisit(visitDTO,start, rows, sord, sidx,true);
		paDto.setRows(rows);
		paDto.setPage(page);
		return "requestVisit";
	}
}
