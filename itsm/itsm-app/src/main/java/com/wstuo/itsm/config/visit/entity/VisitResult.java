package com.wstuo.itsm.config.visit.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import com.wstuo.common.entity.BaseEntity;

/**
 * 请求回访结果实体类
 * @author QXY
 *
 */
@SuppressWarnings({ "rawtypes", "serial" })
@Entity
public class VisitResult extends BaseEntity{
	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
	private Long VisitResultNo;
	private Long eno;
	private String visitItemName;
	private String visitItemValue;
	public Long getVisitResultNo() {
		return VisitResultNo;
	}
	public void setVisitResultNo(Long visitResultNo) {
		VisitResultNo = visitResultNo;
	}
	public Long getEno() {
		return eno;
	}
	public void setEno(Long eno) {
		this.eno = eno;
	}
	public String getVisitItemName() {
		return visitItemName;
	}
	public void setVisitItemName(String visitItemName) {
		this.visitItemName = visitItemName;
	}
	public String getVisitItemValue() {
		return visitItemValue;
	}
	public void setVisitItemValue(String visitItemValue) {
		this.visitItemValue = visitItemValue;
	}
	
	
}
