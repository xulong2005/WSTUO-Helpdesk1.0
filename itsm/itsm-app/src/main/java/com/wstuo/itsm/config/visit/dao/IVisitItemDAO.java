package com.wstuo.itsm.config.visit.dao;

import com.wstuo.itsm.config.visit.entity.VisitItem;
import com.wstuo.common.dao.IEntityDAO;

/**
 * 请求回访事项DAO接口类
 * @author QXY
 *
 */
public interface IVisitItemDAO extends IEntityDAO<VisitItem> {

}
