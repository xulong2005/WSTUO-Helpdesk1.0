package com.wstuo.itsm.config.accessFromExternal.action;

import org.springframework.beans.factory.annotation.Autowired;

import com.opensymphony.xwork2.ActionSupport;
import com.wstuo.common.security.utils.AppContext;
import com.wstuo.common.util.StringUtils;

/**
 * 返回页面Action类
 * @author QXY
 *
 */
@SuppressWarnings("serial")
public class GotopageAction extends ActionSupport {
	
	/**
	 * 时间类型
	 */
	private String eventType;
	/**
	 * 时间编号
	 */
	private String eventId;
	/**
	 * 去到页面类型
	 */
	private String pageType;
	@Autowired
	private AppContext appctx;
	public String getEventType() {
		return eventType;
	}
	public void setEventType(String eventType) {
		this.eventType = eventType;
	}
	public String getEventId() {
		return eventId;
	}
	public void setEventId(String eventId) {
		this.eventId = eventId;
	}
	public String getPageType() {
		return pageType;
	}
	public void setPageType(String pageType) {
		this.pageType = pageType;
	}
	
	/**
	 *  外部入口
	 * @return String
	 */
	public String AccessFromExternal(){
		if(StringUtils.hasText(eventType) && StringUtils.hasText(eventId)){
			appctx.setAttribute("eventType", eventType);
			appctx.setAttribute("eventId", eventId);
		}
		if(StringUtils.hasText(pageType)){
			appctx.setAttribute("pageType", pageType);
		}
		return "goto";
	}

}
