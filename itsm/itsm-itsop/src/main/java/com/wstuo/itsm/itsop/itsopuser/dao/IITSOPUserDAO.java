package com.wstuo.itsm.itsop.itsopuser.dao;

import java.util.List;

import com.wstuo.itsm.itsop.itsopuser.dto.ITSOPUserQueryDTO;
import com.wstuo.itsm.itsop.itsopuser.entity.ITSOPUser;
import com.wstuo.common.dao.IEntityDAO;
import com.wstuo.common.dto.PageDTO;

/**
 * 外包客户管理DAO类.
 * @author QXY
 */
public interface IITSOPUserDAO extends IEntityDAO<ITSOPUser>{

	/**
	 * 分页查找外包客户.
	 * @param qdto
	 * @return String
	 */
	PageDTO findITSOPUserPager(ITSOPUserQueryDTO qdto);
	
	/**
     * 查询与我相关的客户
     * @param qdto ITSOPUserQueryDTO
     * @return 与我相关的全部客户List
     */
	List<ITSOPUser> findMyRelatedCustomer(String loginName);
	/**
	 * 判断外包客户总数
	 * @return
	 */
	Long countAllITSOPNumber();
}
