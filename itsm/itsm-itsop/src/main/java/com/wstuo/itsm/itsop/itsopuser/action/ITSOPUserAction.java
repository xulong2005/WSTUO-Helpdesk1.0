package com.wstuo.itsm.itsop.itsopuser.action;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.opensymphony.xwork2.ActionSupport;
import com.wstuo.itsm.itsop.itsopuser.dto.ITSOPUserDTO;
import com.wstuo.itsm.itsop.itsopuser.dto.ITSOPUserGridDTO;
import com.wstuo.itsm.itsop.itsopuser.dto.ITSOPUserQueryDTO;
import com.wstuo.itsm.itsop.itsopuser.service.IITSOPUserService;
import com.wstuo.common.dto.PageDTO;
import com.wstuo.common.exception.ApplicationException;

/**
 * 外包客户管理Action类.
 * @author QXY
 */
@SuppressWarnings("serial")
public class ITSOPUserAction extends ActionSupport{
	private static final Logger LOGGER = Logger.getLogger(ITSOPUserAction.class);
	@Autowired
	private IITSOPUserService itsopUserService;
	
	private PageDTO pageDTO;
	private ITSOPUserDTO itsopUserDTO;
	private ITSOPUserQueryDTO itsopUserQueryDTO=new ITSOPUserQueryDTO();
	private Long [] ids;
	private Long id;
    private int page = 1;
    private int rows = 10;
    private String fileName;
    private InputStream exportStream;
	private List<ITSOPUserGridDTO> itsopUserGridDTO=new ArrayList<ITSOPUserGridDTO>();
	private String message;
	
	private String sidx;
	private String sord;
	private String effect;
	private String loginName;
	private int fix;
	private int totalSize;
	 
	
	
	public int getTotalSize() {
		return totalSize;
	}

	public void setTotalSize(int totalSize) {
		this.totalSize = totalSize;
	}

	public int getFix() {
		return fix;
	}

	public void setFix(int fix) {
		this.fix = fix;
	}

	public String getEffect() {
		return effect;
	}

	public void setEffect(String effect) {
		this.effect = effect;
	}

	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public int getRows() {
		return rows;
	}

	public void setRows(int rows) {
		this.rows = rows;
	}
	
	
	public String getSidx() {
		return sidx;
	}

	public void setSidx(String sidx) {
		this.sidx = sidx;
	}

	public String getSord() {
		return sord;
	}

	public void setSord(String sord) {
		this.sord = sord;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public PageDTO getPageDTO() {
		return pageDTO;
	}

	public void setPageDTO(PageDTO pageDTO) {
		this.pageDTO = pageDTO;
	}

	public ITSOPUserDTO getItsopUserDTO() {
		return itsopUserDTO;
	}

	public void setItsopUserDTO(ITSOPUserDTO itsopUserDTO) {
		this.itsopUserDTO = itsopUserDTO;
	}

	public ITSOPUserQueryDTO getItsopUserQueryDTO() {
		return itsopUserQueryDTO;
	}

	public void setItsopUserQueryDTO(ITSOPUserQueryDTO itsopUserQueryDTO) {
		this.itsopUserQueryDTO = itsopUserQueryDTO;
	}

	public Long[] getIds() {
		return ids;
	}

	public void setIds(Long[] ids) {
		this.ids = ids;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	public String getLoginName() {
		return loginName;
	}

	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}

	public List<ITSOPUserGridDTO> getItsopUserGridDTO() {
		return itsopUserGridDTO;
	}

	public void setItsopUserGridDTO(List<ITSOPUserGridDTO> itsopUserGridDTO) {
		this.itsopUserGridDTO = itsopUserGridDTO;
	}

	/**
	 * 文件名称
	 * @return String
	 */
	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	/**
	 * 下载输出的数据流
	 * @return String
	 */
	public InputStream getExportStream() {
		return exportStream;
	}

	public void setExportStream(InputStream exportStream) {
		this.exportStream = exportStream;
	}

	/**
	 * 分页查找外包客户.
	 * @return PageDTO
	 */
	public String findITSOPUserPager()
	{
		if(sidx!=null && sord!=null){
			itsopUserQueryDTO.setSidx(sidx);
			itsopUserQueryDTO.setSord(sord);
		}
		int start = ( page - 1 ) * rows;
		itsopUserQueryDTO.setStart( start );
		itsopUserQueryDTO.setLimit( rows );
		pageDTO = itsopUserService.findITSOPUserPager(itsopUserQueryDTO);
		pageDTO.setPage( page );
		pageDTO.setRows( rows );
		return SUCCESS;
	}
	
	/**
	 * 分页查找我负责外包客户.
	 * @return PageDTO
	 */
	public String findMySupportITSOPUserPager()
	{
		if(sidx!=null && sord!=null){
			itsopUserQueryDTO.setSidx(sidx);
			itsopUserQueryDTO.setSord(sord);
		}
		int start = ( page - 1 ) * rows;
		itsopUserQueryDTO.setStart( start );
		itsopUserQueryDTO.setLimit( rows );
		
		if(itsopUserQueryDTO.getLoginName().equals("all")){
			itsopUserQueryDTO.setLoginName(null);
		}
		pageDTO = itsopUserService.findMySupportITSOPUserPager(itsopUserQueryDTO);
		pageDTO.setPage( page );
		pageDTO.setRows( rows );
		return SUCCESS;
	}

	/**
	 * 添加外包客户.
	 */
	public String addITSOPUser()
	{
		message = itsopUserService.addITSOPUser(itsopUserDTO);
		return "message";
	}

	/**
	 * 修改外包客户.
	 */
	public String updateITSOPUser()
	{
		itsopUserService.updateITSOPUser(itsopUserDTO);
		return SUCCESS;
	}

	/**
	 * 删除外包客户（多个）.
	 */
	public String deleteITSOPUser()
	{
		try{
			itsopUserService.deleteITSOPUser(ids);
    	}catch(Exception ex){
    		LOGGER.error(ex);
    		throw new ApplicationException("ERROR_DATA_CAN_NOT_DELETE",ex);
    	}
		return SUCCESS;
	}
	
	/**
	 * 删除外包客户（单个）.
	 * @param id
	 */
	public String deleteITSOPUser(Long id)
	{
		try{
			itsopUserService.deleteITSOPUser(id);
    	}catch(Exception ex){
    		LOGGER.error(ex);
    		throw new ApplicationException("ERROR_DATA_CAN_NOT_DELETE",ex);
    	}
		return SUCCESS;
	}
	
	 
    /**
     * 导出数据.
     * @return String
     */
    public String exportITSOPUser()
    {
    	fileName="ITSOPOrganization.csv";
        try{
        	itsopUserQueryDTO.setStart(0);
        	itsopUserQueryDTO.setLimit(10000);
        	exportStream=itsopUserService.exportITSOPUser(itsopUserQueryDTO);
        }catch(Exception ex){
        	if(ex.getMessage().indexOf("JMS")!=-1){
        		throw new ApplicationException("label_expoort_error_jmsException");
        	}
        	LOGGER.error(ex);
		}	
    	return "exportFileSuccessful";
    }
	/**
	 * 导入外包客户
	 * @return String
	 */
    public String importITSOPUser()
    {
    	effect=itsopUserService.importITSOPUser(itsopUserDTO.getImportFile(),fix);
    	return "importResult";
    }
    
    /**
     * 自动补全时验证用户是否存在
     * @return String
     */
    public String autoVerification()
    {
        ids=itsopUserService.autoVerification( itsopUserDTO.getOrgName() );
        return "autoVerificationResult";
    }
    /**
     * 自动补全时验证用户是否存在
     * @return String
     */
    public String findMyAllCustomer()
    {
        ids=itsopUserService.findMyAllCustomer(loginName);
        return "autoVerificationResult";
    }
    /**
	 * 取外包总数
	 * @return
	 */
    public String getitsopAlreadyUsed()
    {
    	totalSize=itsopUserService.getitsopAlreadyUsed();
        return "totalSize";
    }
}
