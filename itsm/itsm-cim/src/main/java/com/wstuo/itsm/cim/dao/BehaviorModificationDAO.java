package com.wstuo.itsm.cim.dao;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import com.wstuo.common.dao.BaseDAOImplHibernate;
import com.wstuo.common.dto.PageDTO;
import com.wstuo.common.util.MathUtils;
import com.wstuo.common.util.StringUtils;
import com.wstuo.itsm.cim.dto.CIDTO;
import com.wstuo.itsm.cim.entity.BehaviorModification;

/**
 * 配置修改行为
 * @author QXY
 */
public class BehaviorModificationDAO extends BaseDAOImplHibernate<BehaviorModification> implements IBehaviorModificationDAO{
	/**
	 * 查询配置项修改行为
	 */
	public PageDTO findReleasePager(CIDTO cidto) {
        final DetachedCriteria dc = DetachedCriteria.forClass(BehaviorModification.class);
        if(MathUtils.isLongEmpty(cidto.getCiId())){
        	 dc.add(Restrictions.eq("ciId", cidto.getCiId()));
        }
        if(StringUtils.hasText(cidto.getChangeCode())){
        	dc.add(Restrictions.eq("changeCode", cidto.getChangeCode()));
        }
        dc.addOrder(Order.desc("updateDate"));
        return super.findPageByCriteria(dc, 0, 10000);
	}
}
