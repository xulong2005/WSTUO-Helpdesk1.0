package com.wstuo.itsm.cim.service;

import org.springframework.transaction.annotation.Transactional;

import com.wstuo.itsm.cim.dto.CIHistoryNoticeQueryDTO;
import com.wstuo.itsm.cim.entity.CI;

public interface ICiExpiredService {

		/**
		 * 配置项检查是否到期通知
		 */
		@Transactional
		public void configureItemDetectExpiredNotice();
		
		/**
		 * set CIHistoryNoticeQueryDTO
		 * @param ci
		 * @param type
		 */
		CIHistoryNoticeQueryDTO setCIHistoryNoticeQueryDTO(CI ci,Long type);
		/**
		 * save CIHistoryNotice
		 * @param ci
		 * @param type
		 */
		void saveCIHistoryNotice(CI ci,Long type);
		/**
		 * 配置项生命周期到期通知
		 */
		void ciLifeCycleExpiredNotice();
		/**
		 * 预警时间到期通知
		 */
		void ciWarningDateExpiredNotice();

		/**
		 * 配置项通知规则
		 * 
		 * @param noticeRuleNo
		 *            通知规则编号
		 * @param loginNames
		 * @param ciOwner
		 * @param ciUse
		 * @param visitPath
		 */
		public void cimProcessNotice(String noticeRuleNo, CI ci);
	
}
