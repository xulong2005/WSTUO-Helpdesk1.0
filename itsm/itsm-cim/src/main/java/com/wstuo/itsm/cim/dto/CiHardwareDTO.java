package com.wstuo.itsm.cim.dto;

import com.wstuo.common.dto.BaseDTO;

/**
 * 配置项配置信息DTO
 * @author WSTUO_QXY
 *
 */
@SuppressWarnings("serial")
public class CiHardwareDTO extends BaseDTO {
	private Long ciId;
	private Long ciHardwareId;
	private String lastLogin;
	private String computerName;
	private String serialNumber;
	private String systemName;
	private String systemVersion;
	private String mainboard;
	private String cpu;
	private String cpuGrade;
	private String cpuSpeed;
	private String memorySize;
	private String hardDriveModel;
	private String hardDriveSize;
	private String cdDrive;
	private String networkAdapter;
	private String desktopMonitor;
	private String monitor;
	private String remark;
	private String networkDescription;
	private String networkMacAddress;
	private String networkIpAddress;
	public Long getCiHardwareId() {
		return ciHardwareId;
	}
	public void setCiHardwareId(Long ciHardwareId) {
		this.ciHardwareId = ciHardwareId;
	}
	public String getLastLogin() {
		return lastLogin;
	}
	public void setLastLogin(String lastLogin) {
		this.lastLogin = lastLogin;
	}
	public String getComputerName() {
		return computerName;
	}
	public void setComputerName(String computerName) {
		this.computerName = computerName;
	}
	public String getSerialNumber() {
		return serialNumber;
	}
	public void setSerialNumber(String serialNumber) {
		this.serialNumber = serialNumber;
	}
	public String getSystemName() {
		return systemName;
	}
	public void setSystemName(String systemName) {
		this.systemName = systemName;
	}
	public String getSystemVersion() {
		return systemVersion;
	}
	public void setSystemVersion(String systemVersion) {
		this.systemVersion = systemVersion;
	}
	public String getMainboard() {
		return mainboard;
	}
	public void setMainboard(String mainboard) {
		this.mainboard = mainboard;
	}
	public String getCpu() {
		return cpu;
	}
	public void setCpu(String cpu) {
		this.cpu = cpu;
	}
	public String getCpuGrade() {
		return cpuGrade;
	}
	public void setCpuGrade(String cpuGrade) {
		this.cpuGrade = cpuGrade;
	}
	public String getCpuSpeed() {
		return cpuSpeed;
	}
	public void setCpuSpeed(String cpuSpeed) {
		this.cpuSpeed = cpuSpeed;
	}
	public String getMemorySize() {
		return memorySize;
	}
	public void setMemorySize(String memorySize) {
		this.memorySize = memorySize;
	}
	public String getHardDriveModel() {
		return hardDriveModel;
	}
	public void setHardDriveModel(String hardDriveModel) {
		this.hardDriveModel = hardDriveModel;
	}
	public String getHardDriveSize() {
		return hardDriveSize;
	}
	public void setHardDriveSize(String hardDriveSize) {
		this.hardDriveSize = hardDriveSize;
	}
	public String getCdDrive() {
		return cdDrive;
	}
	public void setCdDrive(String cdDrive) {
		this.cdDrive = cdDrive;
	}
	public String getNetworkAdapter() {
		return networkAdapter;
	}
	public void setNetworkAdapter(String networkAdapter) {
		this.networkAdapter = networkAdapter;
	}
	public String getDesktopMonitor() {
		return desktopMonitor;
	}
	public void setDesktopMonitor(String desktopMonitor) {
		this.desktopMonitor = desktopMonitor;
	}
	public String getMonitor() {
		return monitor;
	}
	public void setMonitor(String monitor) {
		this.monitor = monitor;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public String getNetworkDescription() {
		return networkDescription;
	}
	public void setNetworkDescription(String networkDescription) {
		this.networkDescription = networkDescription;
	}
	public String getNetworkMacAddress() {
		return networkMacAddress;
	}
	public void setNetworkMacAddress(String networkMacAddress) {
		this.networkMacAddress = networkMacAddress;
	}
	public String getNetworkIpAddress() {
		return networkIpAddress;
	}
	public void setNetworkIpAddress(String networkIpAddress) {
		this.networkIpAddress = networkIpAddress;
	}
	public Long getCiId() {
		return ciId;
	}
	public void setCiId(Long ciId) {
		this.ciId = ciId;
	}
	
	public CiHardwareDTO(){
		
	}
	
	public CiHardwareDTO(Long ciId, Long ciHardwareId, String lastLogin,
			String computerName, String serialNumber, String systemName,
			String systemVersion, String mainboard, String cpu,
			String cpuGrade, String cpuSpeed, String memorySize,
			String hardDriveModel, String hardDriveSize, String cdDrive,
			String networkAdapter, String desktopMonitor, String monitor,
			String remark, String networkDescription, String networkMacAddress,
			String networkIpAddress) {
		super();
		this.ciId = ciId;
		this.ciHardwareId = ciHardwareId;
		this.lastLogin = lastLogin;
		this.computerName = computerName;
		this.serialNumber = serialNumber;
		this.systemName = systemName;
		this.systemVersion = systemVersion;
		this.mainboard = mainboard;
		this.cpu = cpu;
		this.cpuGrade = cpuGrade;
		this.cpuSpeed = cpuSpeed;
		this.memorySize = memorySize;
		this.hardDriveModel = hardDriveModel;
		this.hardDriveSize = hardDriveSize;
		this.cdDrive = cdDrive;
		this.networkAdapter = networkAdapter;
		this.desktopMonitor = desktopMonitor;
		this.monitor = monitor;
		this.remark = remark;
		this.networkDescription = networkDescription;
		this.networkMacAddress = networkMacAddress;
		this.networkIpAddress = networkIpAddress;
	}
	
	
	
}
