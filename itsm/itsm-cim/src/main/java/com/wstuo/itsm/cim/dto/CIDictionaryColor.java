package com.wstuo.itsm.cim.dto;

import java.util.ArrayList;
import java.util.List;


public class CIDictionaryColor {
	private List<CIDictionaryitems> status = new ArrayList<CIDictionaryitems>();
	private List<CIDictionaryitems> cirelationlist = new ArrayList<CIDictionaryitems>();
	
	public List<CIDictionaryitems> getStatus() {
		return status;
	}
	public void setStatus(List<CIDictionaryitems> status) {
		this.status = status;
	}
	public List<CIDictionaryitems> getCirelationlist() {
		return cirelationlist;
	}
	public void setCirelationlist(List<CIDictionaryitems> cirelationlist) {
		this.cirelationlist = cirelationlist;
	}
}
