package com.wstuo.itsm.cim.dto;

public class CIRelevanceEdgesDTO {
	private Long relevanceId;
	private Long unCiRelevance;
	private Long ciRelevance;
	private String ciRelationTypeName;//类型名称
	
	public Long getRelevanceId() {
		return relevanceId;
	}
	public void setRelevanceId(Long relevanceId) {
		this.relevanceId = relevanceId;
	}
	public Long getUnCiRelevance() {
		return unCiRelevance;
	}
	public void setUnCiRelevance(Long unCiRelevance) {
		this.unCiRelevance = unCiRelevance;
	}
	public Long getCiRelevance() {
		return ciRelevance;
	}
	public void setCiRelevance(Long ciRelevance) {
		this.ciRelevance = ciRelevance;
	}
	public String getCiRelationTypeName() {
		return ciRelationTypeName;
	}
	public void setCiRelationTypeName(String ciRelationTypeName) {
		this.ciRelationTypeName = ciRelationTypeName;
	}
}
