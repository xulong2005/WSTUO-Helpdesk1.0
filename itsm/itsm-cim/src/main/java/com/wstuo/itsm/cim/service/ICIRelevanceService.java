package com.wstuo.itsm.cim.service;


import com.wstuo.common.dto.PageDTO;
import com.wstuo.itsm.cim.dto.CIRelevanceDTO;
import com.wstuo.itsm.cim.dto.CiRelevanceTreeViewDTO;

/**
 * 关联配置项Service类接口
 * @author QXY
 */
public interface ICIRelevanceService
{
	/**
    *
    * 关联配置项分页查询方法
    * @return PageDTO
    */
    PageDTO configureItemRelevanceFindPager( Long ciRelevanceId, int start, int limit,String sord,String sidx);

    /**
     * 关联配置项添加
     */
    void configureItemRelevanceAdd( CIRelevanceDTO ciRelevanceDTO );

    /**
     * 关联配置项更新
     */
    void configureItemRelevanceUpdate( CIRelevanceDTO ciRelevanceDTO );

    /**
     * 关联配置项批量删除
     */
    void configureItemRelevanceDelete( Long[] relevanceIds );
    /**
     * 关联配置项树
     * @param ciId
     * @return List<CiRelevanceTreeViewDTO>
     */
    CiRelevanceTreeViewDTO ciReleTree(Long ciId,String forward,String back);
    
    /**
     * 判断当前关联配置项是否已关联
     * @param ciReleDto
     * @return boolean
     */
    boolean isCIRelevance(CIRelevanceDTO ciReleDto);
    /**
     * 根据ID获取关联配置项
     * @param id
     * @return CIRelevanceDTO
     */
    CIRelevanceDTO findCiRelevanceById(Long id);
}
