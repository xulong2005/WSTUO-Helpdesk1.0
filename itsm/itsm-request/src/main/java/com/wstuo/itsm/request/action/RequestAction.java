package com.wstuo.itsm.request.action;

import java.io.File;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;
import org.springframework.beans.factory.annotation.Autowired;

import com.opensymphony.xwork2.ActionSupport;
import com.wstuo.common.bpm.api.IBPI;
import com.wstuo.common.bpm.dto.ProcessAssignDTO;
import com.wstuo.common.bpm.dto.ProcessDetailDTO;
import com.wstuo.common.bpm.dto.ProcessHandleDTO;
import com.wstuo.common.config.attachment.entity.Attachment;
import com.wstuo.common.config.category.dto.EventCategoryDTO;
import com.wstuo.common.config.customfilter.dto.KeyTransferDTO;
import com.wstuo.common.config.template.dto.TemplateDTO;
import com.wstuo.common.config.template.service.ITemplateService;
import com.wstuo.common.tools.dto.HistoryRecordDTO;
import com.wstuo.itsm.domain.util.ModuleUtils;
import com.wstuo.itsm.itsop.itsopuser.dto.CustomerDataCountDTO;
import com.wstuo.itsm.request.dto.CommentDTO;
import com.wstuo.itsm.request.dto.RequestCountResultDTO;
import com.wstuo.itsm.request.dto.RequestDTO;
import com.wstuo.itsm.request.dto.RequestDetailDTO;
import com.wstuo.itsm.request.dto.RequestGridDTO;
import com.wstuo.itsm.request.dto.RequestHttpDTO;
import com.wstuo.itsm.request.dto.RequestQueryDTO;
import com.wstuo.itsm.request.dto.RequestSimpleDTO;
import com.wstuo.itsm.request.entity.Request;
import com.wstuo.itsm.request.service.IMailToRequestService;
import com.wstuo.itsm.request.service.IRequestService;
import com.wstuo.common.dto.FullTextQueryDTO;
import com.wstuo.common.dto.PageDTO;
import com.wstuo.common.exception.ApplicationException;
import com.wstuo.common.security.service.IUserInfoService;
import com.wstuo.common.util.StringUtils;

/**
 * 请求Action类
 * 
 * @author will
 * 
 */
@SuppressWarnings("serial")
public class RequestAction extends ActionSupport {
	private static final Logger LOGGER = Logger.getLogger(RequestAction.class);
	@Autowired
	private IRequestService requestService;
	@Autowired
	private IBPI bpi;
	@Autowired
	private IMailToRequestService mailToRequestService;
	@Autowired
	private ITemplateService templateService;
	@Autowired
	private IUserInfoService userInfoService;
	private RequestDTO requestDTO = new RequestDTO();
	private RequestDetailDTO requestDetailDTO = new RequestDetailDTO();
	private RequestGridDTO requestGridDTO = new RequestGridDTO();
	private RequestQueryDTO requestQueryDTO = new RequestQueryDTO();
	private int page = 1;
	private int rows = 10;
	private PageDTO requests;
	private Long eno;
	private Long[] enos;
	private RequestCountResultDTO rcrd;
	private Set<String> outcomesSet = new HashSet<String>();// 当前任务所有的出口
	private String taskId;
	private String sidx;
	private String sord;
	private Long aid;
	private List<Attachment> attachment;
	private String fileName = "";
	private InputStream exportStream;
	private String effect = "";
	private HistoryRecordDTO historyRecordDto;
	private Boolean result=false;
	private String visitRecord;
	private List<CustomerDataCountDTO> countsDTO = new ArrayList<CustomerDataCountDTO>();
	private String language;
	private List<EventCategoryDTO> serviceDirectorys; // 解决方案
	private Long[] historyDataNos;
	private Long[] serviceDirectory;
	private TemplateDTO templateDTO;
	private String fitType;
	private Long requestId;
	private Long slaNo;
	private String loginName;
	private File file;
	private String endto;// 监控软件提交过来的固定参数，手机号码
	private String message;// 监控软件提交过来的固定参数，警报信息
	private String password;// 监控软件调用Action的用户验证密码
	private String strResule;// 返回的字符串结果
	private List<ProcessDetailDTO> processDetailDTO = new ArrayList<ProcessDetailDTO>();
	private ProcessHandleDTO processHandleDTO = new ProcessHandleDTO();
	private ProcessAssignDTO processAssignDTO = new ProcessAssignDTO();
	private Boolean isAll=false;
	private CommentDTO commentDTO;
	private Long[] cinos;
	private Boolean isAllowAccessRes;
	private KeyTransferDTO ktd = new KeyTransferDTO();//报表数据传递
	//北塔告警类型：告警触发   告警恢复；其中告警恢复过滤掉
	private String alarmstatus;
	
	public String getAlarmstatus() {
		return alarmstatus;
	}

	public void setAlarmstatus(String alarmstatus) {
		this.alarmstatus = alarmstatus;
	}
	public Boolean getIsAllowAccessRes() {
		return isAllowAccessRes;
	}

	public void setIsAllowAccessRes(Boolean isAllowAccessRes) {
		this.isAllowAccessRes = isAllowAccessRes;
	}

	public Long[] getCinos() {
		return cinos;
	}

	public void setCinos(Long[] cinos) {
		this.cinos = cinos;
	}

	public CommentDTO getCommentDTO() {
		return commentDTO;
	}

	public void setCommentDTO(CommentDTO commentDTO) {
		this.commentDTO = commentDTO;
	}

	public TemplateDTO getTemplateDTO() {
		return templateDTO;
	}

	public void setTemplateDTO(TemplateDTO templateDTO) {
		this.templateDTO = templateDTO;
	}

	public List<EventCategoryDTO> getServiceDirectorys() {
		return serviceDirectorys;
	}

	public void setServiceDirectorys(List<EventCategoryDTO> serviceDirectorys) {
		this.serviceDirectorys = serviceDirectorys;
	}

	public Long[] getServiceDirectory() {
		return serviceDirectory;
	}

	public void setServiceDirectory(Long[] serviceDirectory) {
		this.serviceDirectory = serviceDirectory;
	}

	public Long[] getHistoryDataNos() {
		return historyDataNos;
	}

	public void setHistoryDataNos(Long[] historyDataNos) {
		this.historyDataNos = historyDataNos;
	}
	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public InputStream getExportStream() {
		return exportStream;
	}

	public void setExportStream(InputStream exportStream) {
		this.exportStream = exportStream;
	}

	public String getEffect() {
		return effect;
	}

	public void setEffect(String effect) {
		this.effect = effect;
	}

	public String getSidx() {
		return sidx;
	}

	public void setSidx(String sidx) {
		this.sidx = sidx;
	}

	public String getSord() {
		return sord;
	}

	public void setSord(String sord) {
		this.sord = sord;
	}

	public List<RequestSimpleDTO> getSimples() {
		return simples;
	}

	public void setSimples(List<RequestSimpleDTO> simples) {
		this.simples = simples;
	}

	private List<RequestSimpleDTO> simples = new ArrayList<RequestSimpleDTO>();

	public RequestCountResultDTO getRcrd() {
		return rcrd;
	}

	public void setRcrd(RequestCountResultDTO rcrd) {
		this.rcrd = rcrd;
	}

	public Long getEno() {
		return eno;
	}

	public void setEno(Long eno) {
		this.eno = eno;
	}

	public Long[] getEnos() {
		return enos;
	}

	public void setEnos(Long[] enos) {
		this.enos = enos;
	}

	public RequestDetailDTO getRequestDetailDTO() {
		return requestDetailDTO;
	}

	public void setRequestDetailDTO(RequestDetailDTO requestDetailDTO) {
		this.requestDetailDTO = requestDetailDTO;
	}

	public IRequestService getRequestService() {
		return requestService;
	}

	public void setRequestService(IRequestService requestService) {
		this.requestService = requestService;
	}

	public RequestDTO getRequestDTO() {
		return requestDTO;
	}

	public void setRequestDTO(RequestDTO requestDTO) {
		this.requestDTO = requestDTO;
	}

	public RequestQueryDTO getRequestQueryDTO() {
		return requestQueryDTO;
	}

	public void setRequestQueryDTO(RequestQueryDTO requestQueryDTO) {
		this.requestQueryDTO = requestQueryDTO;
	}

	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public int getRows() {
		return rows;
	}

	public void setRows(int rows) {
		this.rows = rows;
	}

	public PageDTO getRequests() {
		return requests;
	}

	public void setRequests(PageDTO requests) {
		this.requests = requests;
	}

	public RequestGridDTO getRequestGridDTO() {
		return requestGridDTO;
	}

	public void setRequestGridDTO(RequestGridDTO requestGridDTO) {
		this.requestGridDTO = requestGridDTO;
	}

	public Set<String> getOutcomesSet() {
		return outcomesSet;
	}

	public void setOutcomesSet(Set<String> outcomesSet) {
		this.outcomesSet = outcomesSet;
	}

	public String getTaskId() {
		return taskId;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}

	public Long getAid() {
		return aid;
	}

	public void setAid(Long aid) {
		this.aid = aid;
	}

	public List<Attachment> getAttachment() {
		return attachment;
	}

	public void setAttachment(List<Attachment> attachment) {
		this.attachment = attachment;
	}

	public HistoryRecordDTO getHistoryRecordDto() {
		return historyRecordDto;
	}

	public void setHistoryRecordDto(HistoryRecordDTO historyRecordDto) {
		this.historyRecordDto = historyRecordDto;
	}


	public String getVisitRecord() {
		return visitRecord;
	}

	public void setVisitRecord(String visitRecord) {
		this.visitRecord = visitRecord;
	}

	public List<CustomerDataCountDTO> getCountsDTO() {
		return countsDTO;
	}

	public void setCountsDTO(List<CustomerDataCountDTO> countsDTO) {
		this.countsDTO = countsDTO;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public String getFitType() {
		return fitType;
	}

	public void setFitType(String fitType) {
		this.fitType = fitType;
	}

	public Long getRequestId() {
		return requestId;
	}

	public void setRequestId(Long requestId) {
		this.requestId = requestId;
	}

	public Long getSlaNo() {
		return slaNo;
	}

	public void setSlaNo(Long slaNo) {
		this.slaNo = slaNo;
	}

	public String getLoginName() {
		return loginName;
	}

	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}

	public File getFile() {
		return file;
	}

	public void setFile(File file) {
		this.file = file;
	}

	public String getEndto() {
		return endto;
	}

	public void setEndto(String endto) {
		this.endto = endto;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getStrResule() {
		return strResule;
	}

	public void setStrResule(String strResule) {
		this.strResule = strResule;
	}

	public List<ProcessDetailDTO> getProcessDetailDTO() {
		return processDetailDTO;
	}

	public void setProcessDetailDTO(List<ProcessDetailDTO> processDetailDTO) {
		this.processDetailDTO = processDetailDTO;
	}

	public ProcessHandleDTO getProcessHandleDTO() {
		return processHandleDTO;
	}

	public void setProcessHandleDTO(ProcessHandleDTO processHandleDTO) {
		this.processHandleDTO = processHandleDTO;
	}
	
	public ProcessAssignDTO getProcessAssignDTO() {
		return processAssignDTO;
	}

	public void setProcessAssignDTO(ProcessAssignDTO processAssignDTO) {
		this.processAssignDTO = processAssignDTO;
	}

	public KeyTransferDTO getKtd() {
		return ktd;
	}

	public void setKtd(KeyTransferDTO ktd) {
		this.ktd = ktd;
	}

	
	public Boolean getResult() {
		return result;
	}

	public void setResult(Boolean result) {
		this.result = result;
	}

	public Boolean getIsAll() {
		return isAll;
	}

	public void setIsAll(Boolean isAll) {
		this.isAll = isAll;
	}

	/**
	 * find requests by pager.
	 * 
	 * @return requests
	 */
	public String findRequests() {
		int start = (page - 1) * rows;
		requestQueryDTO.setStart(start);
		requestQueryDTO.setLimit(rows);
		requests = requestService.findRequestByPage(requestQueryDTO, sidx, sord);
		requests.setPage(page);
		requests.setRows(rows);
		return "requests";
	}

	/**
	 * find requests by client
	 * 
	 */
	public String findRequestsByclient() {
		int start = (page - 1) * rows;
		requestQueryDTO.setStart(start);
		requestQueryDTO.setLimit(rows);
		requests = requestService.findRequestByPage(requestQueryDTO, sidx, sord);
		requests.setPage(page);
		requests.setRows(rows);
		return "requests";
	}

	/**
	 * 自定义分页查询请求
	 * 
	 * @return SUCCESS
	 */
	public String findPagerRequestByCustomFilter() {
		int start = (page - 1) * rows;
		requestQueryDTO.setStart(start);
		requestQueryDTO.setLimit(rows);
		requests = requestService.findRequestPagerByCustomFilter(requestQueryDTO, sidx, sord);
		requests.setPage(page);
		requests.setRows(rows);
		return "requests";
	}

	/**
	 * save request.
	 * 
	 * @return String
	 */
	public String saveRequest() {
		requestDTO.setRelatedConfigureItemNos(cinos);
		eno = requestService.saveRequest(requestDTO);
		return "eno";
	}

	/**
	 * save request form client
	 */
	public String saveRequestClient() {
		eno = requestService.saveRequest(requestDTO);
		return "eno";
	}
	/**
	 * update request.
	 * 
	 * @return String
	 */
	public String updateRequest() {
		requestDTO.setRelatedConfigureItemNos(cinos);
		requestService.updateRequest(requestDTO);
		return SUCCESS;
	}

	/**
	 * 保存解决方案
	 * 
	 * @return String
	 */
	public String saveSolutions() {

		requestService.saveSolutions(requestDTO);
		return SUCCESS;
	}

	/**
	 * find request by id
	 * 
	 * @return String
	 * 
	 */
	public String findRequestById() {
		requestDetailDTO = requestService.findRequestById(requestQueryDTO.getEno());
		return "singleRequest";
	}

	/**
	 * find request by id
	 * 
	 * @return String
	 * 
	 */
	public String findRequestByIdByClient() {
		requestDetailDTO = requestService.findRequestById(requestQueryDTO.getEno());

		return "singleRequest";
	}

	/**
	 * 删除请求
	 * 
	 * @return String
	 */
	public String deleteRequests() {
		try {
			requestService.deleteRequests(enos);
		} catch (Exception ex) {
			LOGGER.error(ex);
			throw new ApplicationException("ERROR_DATA_CAN_NOT_DELETE", ex);
		}
		return SUCCESS;
	}

	/**
	 * 请求统计
	 * 
	 * @return String
	 */
	public String countRequest() {
		rcrd = requestService.countRequest(requestQueryDTO);
		return "rcrd";

	}

	/**
	 * 请求流程任务统计
	 * 
	 * @return String
	 */
	public String countRequestFlowTask() {
		rcrd = requestService.countRequestFlowTask(requestQueryDTO);
		return "rcrd";

	}

	/**
	 * 请求动作
	 * 
	 * @return SUCCESS
	 */
	public String requestAction() {

		if (requestDTO != null
				&& requestDTO.getOptType() != null
				&& (requestDTO.getOptType().equals("requestVisit") || requestDTO
						.getOptType().equals("requestVisit_re"))
				&& requestDTO.getVisitPath() == null) {
			// 生成回访访问的网络路径
			HttpServletRequest request = ServletActionContext.getRequest();
			String url = "";
			url = request.getScheme() + "://";
			url += request.getHeader("host");
			String str = request.getRequestURI();
			url = url + str.substring(0, str.lastIndexOf("/"))
					+ "/itsm/request/requestVisit.jsp?";
			if (StringUtils.hasText(language)) {
				url = url + "language=" + language;
			}
			requestDTO.setVisitPath(url);
		}
		requestService.requestAction(requestDTO, historyRecordDto);
		return SUCCESS;
	}

	/**
	 * 请求详细信息
	 * 
	 * @return requestDetails
	 */
	public String requestDetails() {
		if (eno != null) {
			requestDetailDTO = requestService.findRequestById(eno);
			if(!StringUtils.hasText(requestDetailDTO.getProcessKey())){
				requestDetailDTO.setProcessKey(bpi.getProcessDefinitionId(null, ModuleUtils.ITSM_REQUEST));
			}
			if(StringUtils.hasText(requestDetailDTO.getPid()) &&  !bpi.isEnded(requestDetailDTO.getPid())){
				requestDetailDTO.setFlowStart(true);
	    	}
			if (requestDetailDTO.getPid() != null) {
				outcomesSet = bpi.getOutcomesByPid(requestDetailDTO.getPid());
				processDetailDTO = bpi.findProcessInstanceDetail(requestDetailDTO.getPid());
			}
			if(requestDetailDTO.getRequestCategoryNo()!=null&&requestDetailDTO.getRequestCategoryNo()>0){
				isAllowAccessRes = userInfoService.isAllowAccessResByCurrentUser("Request_Category_"+requestDetailDTO.getRequestCategoryNo());
			}else{
				isAllowAccessRes = true;
			}
			
		}
		return "requestDetails";
	}
	
	/**
	 * 请求详细信息
	 * 
	 * @return requestDetails
	 */
	public String requestDetailsWap() {
		if (eno != null) {
			requestDetailDTO = requestService.findRequestById(eno);
			if(!StringUtils.hasText(requestDetailDTO.getProcessKey())){
				requestDetailDTO.setProcessKey(bpi.getProcessDefinitionId(null, ModuleUtils.ITSM_REQUEST));
			}
			if(StringUtils.hasText(requestDetailDTO.getPid()) &&  !bpi.isEnded(requestDetailDTO.getPid())){
				requestDetailDTO.setFlowStart(true);
	    	}
			if (requestDetailDTO.getPid() != null) {
				outcomesSet = bpi.getOutcomesByPid(requestDetailDTO.getPid());
				processDetailDTO = bpi.findProcessInstanceDetail(requestDetailDTO.getPid());
			}
			if(requestDetailDTO.getRequestCategoryNo()!=null&&requestDetailDTO.getRequestCategoryNo()>0){
				isAllowAccessRes = userInfoService.isAllowAccessResByCurrentUser("Request_Category_"+requestDetailDTO.getRequestCategoryNo());
			}else{
				isAllowAccessRes = true;
			}
			
		}
		return "requestDetailsWap";
	}

	/**
	 * 请求详细信息
	 * 
	 * @return requestDetails1
	 */
	public String requestDetails1() {

		if (eno != null) {
			requestDetailDTO = requestService.findRequestById(eno);
			if (requestDetailDTO.getPid() != null) {
				outcomesSet = bpi.getOutcomesByPid(requestDetailDTO
						.getPid());
			}
		}

		return "requestDetails1";
	}

	/**
	 * 获取请求详细并返回
	 * 
	 * @return requestDetails1
	 */
	public String showRequestInfo() {
		if (eno != null) {
			requestDetailDTO = requestService.findRequestById(eno);
		}
		return "requestDetailDTO";
	}

	/**
	 * 邮件转请求
	 * 
	 * @return String
	 * @throws UnsupportedEncodingException
	 */
	public String emailToRequest() throws UnsupportedEncodingException {

		RequestDTO requestDtos = new RequestDTO();

		String etitle = new String(requestDetailDTO.getEtitle().getBytes(
				"ISO-8859-1"), "UTF-8");

		requestDtos.setEtitle(etitle);
		requestDTO = requestDtos;
		return "requestAdd";
	}

	/**
	 * 页面跳转，根据编号查找请求解决方案，再转到知识添加页面.
	 * 
	 * @return String
	 */
	public String jumpToAddKnowledge() {

		requestDetailDTO = requestService.findRequestById(eno);
		commentDTO = requestService.findCommentDTOByEno(eno);
		return "addKnowledgePage";
	}
	public String jumpToAddKnowledgeWap() {

		requestDetailDTO = requestService.findRequestById(eno);
		commentDTO = requestService.findCommentDTOByEno(eno);
		return "addKnowledgePageWap";
	}

	/**
	 * 导出数据.
	 */
	public String exportRequest() {
		fileName = "Helpdesk.csv";
		try {
			if (requestQueryDTO != null && requestQueryDTO.getFilterId() != null) {
				requestService.exportRequestItems(requestQueryDTO.getFilterId(), sidx, sord);
			} else if (requestQueryDTO != null && StringUtils.hasText(requestQueryDTO.getAlias())) {// 全文检索
				requestService.exportRequestItems(requestQueryDTO, sidx, sord);
			} else {
				requestQueryDTO.setStart(0);
				requestQueryDTO.setLimit(10000);
				requestService.exportRequestItems(requestQueryDTO, sidx, sord);
			}
		} catch (Exception ex) {
			ex.printStackTrace();
			if (ex!=null && ex.getMessage()!=null && ex.getMessage().indexOf("JMS") != -1) {
				throw new ApplicationException(
						"label_expoort_error_jmsException");
			}
			LOGGER.error(ex);
		}
		return "exportFileSuccessful";

	}

	/**
	 * 全文检索.
	 */
	public String analogousRequest() {
		int start = (page - 1) * rows;
		requestQueryDTO.setStart(start);
		requestQueryDTO.setLimit(rows);
		requests = requestService.fullSearch(requestQueryDTO, sidx, sord);
		requests.setPage(page);
		requests.setRows(rows);
		return "requests";
	}

	/**
	 * 邮件回访保存
	 * 
	 */
	public String requestVisitByEmail() {
		result = requestService.requestVisitByEmail(eno, visitRecord);
		return "result";
	}

	/**
	 * 负责客户的数据统计
	 * 
	 */
	public String requestDataCountByCompanyNo() {
		countsDTO = requestService
				.requestDataCountByCompanyNo(loginName, isAll);
		return "countResult";
	}

	/**
	 * 恢复数据.
	 */
	public String restoreData() {
		requestService.restoreData(historyDataNos);
		return SUCCESS;
	}

	/**
	 * 邮箱扫描+邮件自动转请求
	 * 
	 * @return String
	 */
	public String scanEmail() {
		try {
			mailToRequestService.convertMail();
		} catch (Exception e) {
			LOGGER.error(e);
			throw new ApplicationException("ERROR_SCANEMAILMESSAGE\n");
		}
		return SUCCESS;
	}

	/**
	 * 重新匹配SLa
	 */
	public String reFitSLA() {
		requestService.reFitSLA(fitType, requestId, slaNo, historyRecordDto);
		return SUCCESS;
	}

	/**
	 * 导入请求
	 */
	public String importRequest() {
		try {
			effect = requestService.importRequest(file);
		} catch (Exception ex) {
			LOGGER.error(ex);
		}

		return "importResult";
	}

	/**
	 * 保存请求内容模板
	 * 
	 * @return String
	 */
	public String saveRequestTemplate() {
		requestDTO = requestService.convertRequestTemplateValue(requestDTO,
				cinos);
		templateService.saveTemplate(requestDTO, templateDTO);
		return SUCCESS;
	}

	/**
	 * 创建请求
	 */
	public String createRequest() {
	    
		HttpServletRequest request = ServletActionContext.getRequest();
	    String agent = request.getHeader("User-Agent");
	    boolean isMSIE = (agent != null && agent.indexOf("MSIE") != -1);
		try {
		    if (isMSIE) {
		        endto = new String(endto.getBytes("iso-8859-1"), "gb2312");
                message = new String(message.getBytes("iso-8859-1"), "gb2312");
		    } else {
    	        endto = new String(endto.getBytes("iso-8859-1"), "utf-8");
                message = new String(message.getBytes("iso-8859-1"), "utf-8");
		    }
		} catch (UnsupportedEncodingException e) {
			LOGGER.error(e);
		}
		RequestHttpDTO dto=new RequestHttpDTO();
		dto.setPhoneNumber(endto);
		dto.setMessage(message);
		dto.setLoginName(loginName);
		dto.setPassword(password);
		strResule = requestService.createRequest(dto);

		return "strResult";
	}

	/**
	 * 流程处理
	 * 
	 * @return String
	 */
	public String processHandle() {
		requestService.processHandle(processHandleDTO);
		return SUCCESS;
	}
	
	/**
	 * 任务指派
	 * @return String
	 */
	public String assignTask(){
		requestService.assignTask(processHandleDTO);
		return SUCCESS;
	}


	/**
	 * 分组和单项报表数据查询
	 * 
	 * @return String
	 */
	public String findRequestsByCrosstabCell() {
		int start = (page - 1) * rows;
		requests = requestService.findRequestsByCrosstabCell(ktd, start, rows, sidx, sord);
		requests.setPage(page);
		requests.setRows(rows);
		return "requests";
	}

	/**
	 * 根据请求查找解决方案
	 * 
	 * @return String
	 */
	public String findSolutionByRequestId() {
		serviceDirectorys = requestService
				.findSolutionByRequestId(requestQueryDTO);
		return "serviceDirectories";
	}

	/**
	 * 保存服务目录
	 * 
	 * @return String
	 */
	public String saveServiceDirectory() {
		requestService.saveServiceDirectory(requestDetailDTO, serviceDirectory);
		return SUCCESS;
	}

	/**
	 * 删除服务目录
	 * 
	 * @return String
	 */
	public String removeServiceDirectory() {
		requestService.removeServiceDirectory(requestDetailDTO,
				serviceDirectory[0]);
		return SUCCESS;
	}

	/**
	 * 根据Eno查询附件
	 */
	public String findCommentDTOByEno() {
		commentDTO = requestService.findCommentDTOByEno(eno);
		return "comment";
	}

	/**
	 * 删除对应eno下的附件
	 */
	public String deleteAttachment() {
		requestService.deleteAttachment(eno, aid);
		return SUCCESS;
	}

	/**
	 * 根据繁忙程度自动指派
	 */
	public String workloadStatusStatistics() {
		requests = requestService.workloadStatusStatistics(page, rows, sidx, sord);
		requests.setPage(page);
		requests.setRows(rows);
		return "requests";
	}
	
	/**
	 * 重建索引
	 */
	public String reindex(){
		requestService.reindexRequest();
    	return SUCCESS; 
    }
	public String findRequestReturnPageDTO(){
    	 int start = (page - 1) * rows;
         
         requestQueryDTO.setStart(start);
         requestQueryDTO.setLimit(rows);
         requests = requestService.findRequestByPage(requestQueryDTO,sidx,sord);
         requests.setPage(page);
         requests.setRows(rows);
    	return "pageData";
    }
	
    public String saveTicket(){
    	eno= requestService.saveTicket(requestDTO);
    	return "eno";
    }
    
    /**
     * 开启流程
     */
    public String openProcess(){
    	requestService.openProcess(processHandleDTO);
    	return SUCCESS;
    }
    
    
    public String requestBatchClose(){
    	requestService.requestBatchClose(requestDTO);
    	return SUCCESS;
    }
    /**
     * 查看流程任务指派人和组
     */
    public String findProcessAssignDTO(){
    	processAssignDTO = requestService.findProcessAssignDTO(processHandleDTO);
    	return "processAssignDTO";
    }
    
}