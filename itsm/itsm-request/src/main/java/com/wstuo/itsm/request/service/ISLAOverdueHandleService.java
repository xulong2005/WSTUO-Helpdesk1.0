package com.wstuo.itsm.request.service;

/**
 * SLA逾期处理(升级及通知)Service
 * @author QXY
 */
public interface ISLAOverdueHandleService {

	void slaOverdueHandle();
}
