package com.wstuo.itsm.request.dto;

import java.util.Date;

/**
 * 定期任务列表DTO类
 * @author WSTUO_QXY
 *
 */
public class RequestDraftGridDTO {
	private Long requestDraftID;//ID
	private String requestDraftName;//标题
	private String requestJson;//描述
	private Date createTime;
	private Date lastUpdateTime;
	public Long getRequestDraftID() {
		return requestDraftID;
	}
	public void setRequestDraftID(Long requestDraftID) {
		this.requestDraftID = requestDraftID;
	}
	public String getRequestDraftName() {
		return requestDraftName;
	}
	public void setRequestDraftName(String requestDraftName) {
		this.requestDraftName = requestDraftName;
	}
	public String getRequestJson() {
		return requestJson;
	}
	public void setRequestJson(String requestJson) {
		this.requestJson = requestJson;
	}
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	public Date getLastUpdateTime() {
		return lastUpdateTime;
	}
	public void setLastUpdateTime(Date lastUpdateTime) {
		this.lastUpdateTime = lastUpdateTime;
	}
	
	
	
	
}
