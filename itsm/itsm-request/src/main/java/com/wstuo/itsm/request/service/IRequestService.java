package com.wstuo.itsm.request.service;

import java.io.File;
import java.util.Date;
import java.util.List;

import com.wstuo.common.bpm.dto.ProcessAssignDTO;
import com.wstuo.common.bpm.dto.ProcessHandleDTO;
import com.wstuo.common.config.category.dto.EventCategoryDTO;
import com.wstuo.common.config.customfilter.dto.KeyTransferDTO;
import com.wstuo.common.tools.dto.ExportPageDTO;
import com.wstuo.common.tools.dto.ExportQueryDTO;
import com.wstuo.common.tools.dto.HistoryRecordDTO;
import com.wstuo.itsm.request.entity.Request;
import com.wstuo.itsm.itsop.itsopuser.dto.CustomerDataCountDTO;
import com.wstuo.itsm.request.dto.CommentDTO;
import com.wstuo.itsm.request.dto.RequestCountResultDTO;
import com.wstuo.itsm.request.dto.RequestDTO;
import com.wstuo.itsm.request.dto.RequestDetailDTO;
import com.wstuo.itsm.request.dto.RequestHttpDTO;
import com.wstuo.itsm.request.dto.RequestQueryDTO;
import com.wstuo.common.dto.PageDTO;
import com.wstuo.common.security.entity.User;
/**
 * 请求服务层接口类
 * 
 * @author QXY
 * 
 */
public interface IRequestService {
	public static final String ENTITYCLASS = "com.wstuo.itsm.request.entity.Request";
	public static final String FILTERCATEGORY = "request";
	public static final String MODULECATEGORY = "Request";
	public static final String REQUEST_NEW="request_new";
	/**
	 * sla匹配标识：手动
	 */
	public static final String SLAFITTYPE_MANUAL_SIGN = "0";
	/**
	 * sla匹配标识：规则匹配
	 */
	public static final String SLAFITTYPE_RULESMATCH_SIGN = "1";
	/**
	 * 请求升级标识 ,value:2L
	 */
	public static final Long REQUEST_UPGRADEAPPLY_SIGN = 2L;
	/**
	 * 分页查询请求
	 * 
	 * @param qdto
	 * @param sidx
	 * @param sord
	 * @return PageDTO
	 */
	PageDTO findRequestByPage(RequestQueryDTO qdto, String sidx, String sord);

	/**
	 * 自定义分页查询请求列表.
	 * 
	 * @param qdto
	 * @return PageDTO
	 */
	PageDTO findRequestPagerByCustomFilter(RequestQueryDTO qdto, String sidx, String sord);

	/**
	 * 保存请求
	 * 
	 * @param qdto
	 * @return Long
	 */
	Long saveRequest(RequestDTO qdto);

	/**
	 * 查询请求详细信息
	 * 
	 * @param id
	 * @return RequestDetailDTO
	 */
	RequestDetailDTO findRequestById(Long id);
	/**
	 * 编辑请求
	 * 
	 * @param dto
	 */
	void updateRequest(RequestDTO dto);
	/**
	 * 删除请求
	 * 
	 * @param id
	 */
	void deleteRequests(Long[] id);
	/**
	 * 请求相关统计
	 * 
	 * @param qdto
	 * @return RequestCountResultDTO
	 */
	RequestCountResultDTO countRequest(RequestQueryDTO qdto);

	/**
	 * 请求流程任务统计
	 * 
	 * @param qdto
	 * @return RequestCountResultDTO
	 */
	RequestCountResultDTO countRequestFlowTask(RequestQueryDTO qdto);

	/**
	 * 保存解决方案
	 * 
	 * @param dto
	 */
	void saveSolutions(RequestDTO dto);

	/**
	 * 请求动作
	 * 
	 * @param requestDTO
	 * @param hrDTO
	 */
	void requestAction(RequestDTO requestDTO, HistoryRecordDTO hrDTO);

	/**
	 * 邮件回访保存
	 * 
	 * @param eno
	 * @param visitRecord
	 * @return boolean
	 */
	boolean requestVisitByEmail(Long eno, String visitRecord);

	/**
	 * 导出数据字典到EXCEL.
	 */
	void exportRequestItems(RequestQueryDTO requestQueryDTO, String sidx, String sord);
	/**
	 * 导出
	 * 
	 * @param p
	 */
	void exportData(ExportPageDTO p);
	/**
	 * 分页全文检索.
	 * 
	 * @param queryDTO
	 * @param sidx
	 * @param sord
	 * @return PageDTO
	 */
	PageDTO fullSearch(RequestQueryDTO requestQueryDTO, String sidx, String sord);


	/**
	 * 负责客户的数据统计
	 * 
	 * @param loginName
	 *            登录名
	 * @param isAll
	 *            是否要把统计结果为0的显示
	 * @return 统计结果
	 */
	List<CustomerDataCountDTO> requestDataCountByCompanyNo(String loginName, boolean isAll);

	/**
	 * 恢复数据.
	 * 
	 * @param historyDaQXYos
	 *            Long[]
	 */
	void restoreData(Long[] historyDaQXYos);

	/**
	 * 根据过滤器结果导出数据.
	 * 
	 * @param filterId
	 */
	void exportRequestItems(Long filterId, String sidx, String sord);


	/**
	 * 判断SLA状态
	 * 
	 * @param req
	 * @return String
	 */
	String getSlaStatus(Request req);
	/**
	 * 更新请求SLA状态
	 */
	void updateRequestSLAStatus();

	/**
	 * 邮件通知
	 * 
	 * @param request
	 * @param dto
	 */
	void addRequestNotice(Request request, RequestDTO dto);

	/**
	 * 重新匹配SLA
	 * 
	 * @param fitType
	 * @param requestId
	 * @param slaNo
	 * @param his
	 */
	void reFitSLA(String fitType, Long requestId, Long slaNo, HistoryRecordDTO his);
	/**
	 * 请求导入
	 * 
	 * @param importFile
	 * @return String
	 */
	String importRequest(File importFile);
	/**
	 * 监控软件HTTP集成创建请求
	 * 
	 * @param phoneNumber
	 * @param message
	 * @param loginName
	 * @param password
	 * @return String
	 */
	String createRequest(RequestHttpDTO httpDto);

	/**
	 * 流程处理
	 * 
	 * @param phDTO
	 */
	void processHandle(ProcessHandleDTO phDTO);
	/**
	 * 任务指派
	 */
	void assignTask(ProcessHandleDTO dto);
	/**
	 * 根据流程实例ID，去更新指定活动下的对应状态
	 * 
	 * @param eno
	 * @param pid
	 */
	void updateRequestStatus(Long eno, String pid);

	/**
	 * 分组和单项报表数据查询
	 * 
	 * @return PageDTO
	 */
	PageDTO findRequestsByCrosstabCell(KeyTransferDTO ktd, int rows, int start, String sidx, String sord);
	/**
	 * 根据条件查询出导出数据
	 * 
	 * @param dto
	 */
	void commonExportData(ExportQueryDTO dto);

	/**
	 * 保存服务目录
	 * 
	 * @param requestDetailDTO
	 * @param serviceDirectory
	 */
	void saveServiceDirectory(RequestDetailDTO requestDetailDTO, Long[] serviceDirectory);
	/**
	 * 移除服务目录
	 * 
	 * @param requestDetailDTO
	 * @param serviceDirectoryId
	 */
	void removeServiceDirectory(RequestDetailDTO requestDetailDTO, Long serviceDirectoryId);
	/**
	 * 查询解决方案
	 * 
	 * @param requestQueryDTO
	 * @return List<EventCategoryDTO>
	 */
	List<EventCategoryDTO> findSolutionByRequestId(RequestQueryDTO requestQueryDTO);

	/**
	 * 解除挂起重新计算SLA
	 * 
	 * @param eno
	 * @param pendingDateStart
	 *            挂起开始时间
	 * @param pendingDateEnd
	 *            挂起结束时间
	 */
	void removePendingReCalcRequestSLATime(Long eno, Date pendingDateStart, Date pendingDateEnd);
	/**
	 * 通知规则
	 * 
	 * @param noticeRuleNo
	 * @param request
	 * @param loginNames
	 * @param users
	 */
	void requestProcessNotice(String noticeRuleNo, Request request, List<String> loginNames, List<User> users, String visitPath, String operator, String emailAddress, String outCome);
	/**
	 * 查找解决方案附件
	 * 
	 * @param eno
	 * @return CommentDTO
	 */
	CommentDTO findCommentDTOByEno(Long eno);

	/**
	 * 删除解决方案附件
	 * 
	 * @param eno
	 * @param aid
	 */
	void deleteAttachment(Long eno, Long aid);

	/**
	 * 根据繁忙程度指派技术员
	 */
	//User automaticallyAssignedByBusy(Long categoryId,String belongsGroupIds);
	User automaticallyAssignedByBusy(Long categoryId,String belongsGroupIds,List<String> userName);

	/**
	 * 技术员繁忙程度统计
	 * 
	 * @param page
	 * @param rows
	 * @return PageDTO
	 */
	PageDTO workloadStatusStatistics(int page, int rows, String sidx, String sord);
	/**
	 * 转换模板值
	 * 
	 * @param requestDTO
	 * @param cinos
	 * @return RequestDTO
	 */
	RequestDTO convertRequestTemplateValue(RequestDTO requestDTO, Long[] cinos);
	/**
	 * 创建请求的消费方法
	 * 
	 * @param dto
	 */
	void saveRequestConsumer(RequestDTO dto);


	/**
	 * 重建请求的索引
	 */
	void reindexRequest();
	
	/**
    * 精简版请求添加
    * @param paramRequestDTO
    */
	Long saveTicket(RequestDTO paramRequestDTO) throws NullPointerException;
	
	
	/**
	 * 实体转DTO
	 * @param req
	 * @param dto
	 */
	void entity2dto(Request req, RequestDTO dto);
	
	/**
	 * 开启流程
	 */
	String openProcess(ProcessHandleDTO phDTO);
	
	/**
	 * 查询关联表单的请求数量
	 * @param formId
	 * @return
	 */
	Integer findRquestNumByFormId(Long[] formIds);
	/**
	 * 查看流程任务指派人和组
	 * @param phDTO
	 * @return
	 */
	ProcessAssignDTO findProcessAssignDTO(ProcessHandleDTO phDTO);
	
	/**
	 * 批量关闭请求
	 * @param requestDTO
	 */
	void requestBatchClose(RequestDTO requestDTO);
	/**
	 * 统计请求分类报表
	 * @return
	 */
    List requestCatagoryReport();
    /**
	 * 统计请求状态报表
	 * @return
	 */
	List requestStatusReport();
	/**
	 * 统计SLA响应率报表
	 * @return
	 */
	List requestSLAStatusReport();
	/**
	 * 统计SLA完成率报表
	 * @return
	 */
	List requestSLACompleteReport();
	/**
	 * 技术员工作量统计
	 * @return
	 */
	List engineerWorkloadReport();
	/**
	 * 技术员忙碌程度统计
	 * @return
	 */
	List workloadStatusStatistics();
	/**
	 * 技术员处理请求耗时
	 * @return
	 */
	List tctakeTimeperRequest();
	/**
	 * 技术员工时统计报表
	 * @return
	 */
	List technicianCostTime();
}
