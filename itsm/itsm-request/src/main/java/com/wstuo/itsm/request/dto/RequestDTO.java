package com.wstuo.itsm.request.dto;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.wstuo.common.config.category.entity.EventCategory;
import com.wstuo.common.tools.dto.HistoryRecordDTO;
import com.wstuo.common.dto.BaseDTO;

/**
 * RequestDTO.
 * @author QXY
 *
 */
@SuppressWarnings( "serial" )
public class RequestDTO extends BaseDTO {
	
	//EventDTO 2011-09-01 QXY
    private Long eno;
    private Long ecategoryNo;
    private String ecategoryName;
    private String etitle;			//标题
    private String edesc;
    private Long effectRangeNo;
    private String effectRangeName;
    private String effectRemark;
    private Long seriousnessNo;
    private String seriousnessName;
    private Long priorityNo;
    private String priorityName;
    private Long statusNo;
    private String statusName;
    private String address;
    private Date createdOn;
    private Long createdByNo;
    private String createdByName;
    private String createdByEmail;
    private Long technicianNo;
    private String technicianName;
    private String pid;
    private Long[] relatedConfigureItemNos;//关联配置项编号
    private String solutions;
    private Date slaDoneDate;
    private Date slaResponseDate;
    private String keyWord;
    private Date lastUpdateTime;
    private Map<String,String> attrVals =new HashMap<String, String>();
    
    //OLD
	private Long imodeNo;
    private Long levelNo;
    private Long assigneeNo;
    private String assigneeName;  //指派人姓名
    private Long assigneeGroupNo;
    private String assigneeGroupName;	//指派人所属组名字
	private Long maxCompleteTime; 
    private Long responseTime=800L;
    private Long completeTime=1200L;
    private Long servicesNo=0L;
    private Boolean isFCR=false;
    private String optType;
    private Date closeTime;
    private String requestCode;			//请求编号
    private Long ownerNo=0L;
    private Long remarkStatusNo;
	private Long requestResolvedUserNo;
    private Date requestResolvedTime;
    private String matchRuleName;
    private Long upgradeApplySign;
    
    //SLA最迟响应时间
    @Deprecated
    private Long maxCompleteTimeBack;
    
    //自动升级标识
    private String autoUpdate="no";
    
    private Long requestCategoryNo;
    private String requestCategoryName;
    //审批人
    private Long updateLevelNo=0L;
    private Long approvalNo=0L;
    
    private String approvalName;
    //审批结果
    private String approvalResult;
    private String taskId;
    //当前用户
    private String currentUser;
    //操作用户
    private String creator;
    private String visitRecord;
    private String attachmentStr;
    private Long emailMessage;
    private String companyName;//客户名称
    private Long companyNo;//客户No
    private Long organizationNo;
    private Long[] organizationNos;
    private String actionName;//动作名称，新增或编辑
    private Boolean hang = false;//挂起(20110831-QXY)
    private Date requestResponseTime;
    private Date requestCompleteTime;
   
	private Long visitState;
    private String visitPath;
    private Long visitId;

	//SLA
    private Long maxResponseTime;
    private Long slaNo;
    private Long trueStartTime=0L;
    private Long currTime=new Date().getTime();
    private String remark;
    private Boolean auditResult=false;//审核结果
    private String processNextStep;//流程下一步
    
    //2012-02-21
    private String processKey;
    
    //2012-03-23
    private Long slaStatusNo;
    private Long[] aids;
    private String assignType;//任务指派
    //匹配配置项分类规则
    private Long cicategoryNo;
    private Long[] cicategoryNos;
    private Long assginTechnicalGroupId;
    private String assginTechnicalGroupName;
    private Boolean slaExpired=false;//SLA是否已过期
    private Long requestServiceDirNo;
    private String requestServiceDirName;
    private Long locationNo;
    private Long slaContractNo;
    private Long slaRuleNo;
    private Long offmodeNo;//请求的关闭方式编号
    private String offmodeName;//请求的关闭方式名称
    private CommentDTO solutionsComment;//解决方案
    private List<EventCategory> serviceDirectory = new ArrayList<EventCategory>();   //服务目录
    private Long[] serviceDirIds;
    private String serviceDirIdsStr;
    private String weekNo;
    private List<Long> relatedCiNos;//关联配置项编号
    private Map<Long,String> serviceNos;
    private Map<Long,Long> scores; //服务分值
    private Boolean mailToRequest=false;//符合邮件转成请求
    private Map<String,Object> piv = new HashMap<String, Object>();//流程实例变量
    private List<HistoryRecordDTO> historyRecordDTO = new ArrayList<HistoryRecordDTO>();  //历史记录
    private String eventCode;//编号
    private String ecode;//邮件模板使用的请求编号
    private String operator; //处理人 
    private String activityName;
    private String outcome;
    private String eventType;
    /*有邮件使用*/
    private Date responsesTime;
    private Date maxResponsesTime;
    private Date maxCompletesTime;
    private String createdByPhone;
    private String complexity;
    private String imodeName;
    private String ciname;
    private String email;
    private String phone;
    private String orgName;
    private String attName;
    private String moblie;
    private String officeAddress; //办公地址
    private String loginUserName; //用户登陆名
    private Boolean flowStart = false;
    private Boolean requestClose = false;
    private Long formId;
    private String isShowBorder;
        //排班
    private Boolean attendance = false;
    private String variablesAssigneeType ;
    private Boolean isNewForm;
    
    private Long[] technologyGroup;//请求人技术组
    private Long locationNos;
    
    private String locationName;//地点
    
    private List<String> outcomes;
    private String moduleType;
    private String randomParam;
    
    private Long[] enos;//批量关闭请求
    private String[]requestCodes;
    private String closeCode;
    
    private Long taskAssigneeNo;
    private String taskassigneeGroupNo;
    
    private Long knowledgeNo;
    
    
	public Long getKnowledgeNo() {
		return knowledgeNo;
	}
	public void setKnowledgeNo(Long knowledgeNo) {
		this.knowledgeNo = knowledgeNo;
	}
	public String getCloseCode() {
		return closeCode;
	}
	public void setCloseCode(String closeCode) {
		this.closeCode = closeCode;
	}
	public Boolean getRequestClose() {
		return (requestClose == null)?false:requestClose;
	}
	public void setRequestClose(Boolean requestClose) {
		this.requestClose = requestClose;
	}
	public String getTaskassigneeGroupNo() {
		return taskassigneeGroupNo;
	}
	public void setTaskassigneeGroupNo(String taskassigneeGroupNo) {
		this.taskassigneeGroupNo = taskassigneeGroupNo;
	}
	public Long getTaskAssigneeNo() {
		return taskAssigneeNo;
	}
	public void setTaskAssigneeNo(Long taskAssigneeNo) {
		this.taskAssigneeNo = taskAssigneeNo;
	}
	public String[] getRequestCodes() {
		return requestCodes;
	}
	public void setRequestCodes(String[] requestCodes) {
		this.requestCodes = requestCodes;
	}
	public Long[] getEnos() {
		return enos;
	}
	public void setEnos(Long[] enos) {
		this.enos = enos;
	}
	public List<String> getOutcomes() {
		return outcomes;
	}
	public void setOutcomes(List<String> outcomes) {
		this.outcomes = outcomes;
	}
	public String getModuleType() {
		return moduleType;
	}
	public void setModuleType(String moduleType) {
		this.moduleType = moduleType;
	}
	public String getRandomParam() {
		return randomParam;
	}
	public void setRandomParam(String randomParam) {
		this.randomParam = randomParam;
	}
	public Long getLocationNos() {
		return locationNos;
	}
	public void setLocationNos(Long locationNos) {
		this.locationNos = locationNos;
	}
	public String getLocationName() {
		return locationName;
	}
	public void setLocationName(String locationName) {
		this.locationName = locationName;
	}
	public Long[] getTechnologyGroup() {
		return technologyGroup;
	}
	public void setTechnologyGroup(Long[] technologyGroup) {
		this.technologyGroup = technologyGroup;
	}
	public Boolean getIsNewForm() {
		return isNewForm;
	}
	public void setIsNewForm(Boolean isNewForm) {
		this.isNewForm = isNewForm;
	}
	public String getVariablesAssigneeType() {
		return variablesAssigneeType;
	}
	public void setVariablesAssigneeType(String variablesAssigneeType) {
		this.variablesAssigneeType = variablesAssigneeType;
	}
	public String getIsShowBorder() {
		return isShowBorder;
	}
	public void setIsShowBorder(String isShowBorder) {
		this.isShowBorder = isShowBorder;
	}
	public Long getFormId() {
		return formId;
	}
	public void setFormId(Long formId) {
		this.formId = formId;
	}
	public Boolean getFlowStart() {
		return flowStart;
	}
	public void setFlowStart(Boolean flowStart) {
		this.flowStart = flowStart;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getOrgName() {
		return orgName;
	}
	public void setOrgName(String orgName) {
		this.orgName = orgName;
	}
	public String getAttName() {
		return attName;
	}
	public void setAttName(String attName) {
		this.attName = attName;
	}
	public String getMoblie() {
		return moblie;
	}
	public void setMoblie(String moblie) {
		this.moblie = moblie;
	}
	public String getOfficeAddress() {
		return officeAddress;
	}
	public void setOfficeAddress(String officeAddress) {
		this.officeAddress = officeAddress;
	}
	public String getLoginUserName() {
		return loginUserName;
	}
	public void setLoginUserName(String loginUserName) {
		this.loginUserName = loginUserName;
	}
	public String getCiname() {
		return ciname;
	}
	public void setCiname(String ciname) {
		this.ciname = ciname;
	}
	public String getImodeName() {
		return imodeName;
	}
	public void setImodeName(String imodeName) {
		this.imodeName = imodeName;
	}
	public String getComplexity() {
		return complexity;
	}
	public void setComplexity(String complexity) {
		this.complexity = complexity;
	}
	public String getCreatedByPhone() {
		return createdByPhone;
	}
	public void setCreatedByPhone(String createdByPhone) {
		this.createdByPhone = createdByPhone;
	}
	public Date getResponsesTime() {
		return responsesTime;
	}
	public void setResponsesTime(Date responsesTime) {
		this.responsesTime = responsesTime;
	}
	public Date getMaxResponsesTime() {
		return maxResponsesTime;
	}
	public void setMaxResponsesTime(Date maxResponsesTime) {
		this.maxResponsesTime = maxResponsesTime;
	}
	public Date getMaxCompletesTime() {
		return maxCompletesTime;
	}
	public void setMaxCompletesTime(Date maxCompletesTime) {
		this.maxCompletesTime = maxCompletesTime;
	}
	public String getEventType() {
		return eventType;
	}
	public void setEventType(String eventType) {
		this.eventType = eventType;
	}
	public String getOutcome() {
		return outcome;
	}
	public void setOutcome(String outcome) {
		this.outcome = outcome;
	}
	public String getActivityName() {
		return activityName;
	}
	public void setActivityName(String activityName) {
		this.activityName = activityName;
	}
	public String getOperator() {
		return operator;
	}
	public void setOperator(String operator) {
		this.operator = operator;
	}
	public String getEcode() {
		return ecode;
	}
	public void setEcode(String ecode) {
		this.ecode = ecode;
	}
	public Long getVisitId() {
		return visitId;
	}
	public void setVisitId(Long visitId) {
		this.visitId = visitId;
	}
	public List<HistoryRecordDTO> getHistoryRecordDTO() {
		return historyRecordDTO;
	}
	public void setHistoryRecordDTO(List<HistoryRecordDTO> historyRecordDTO) {
		this.historyRecordDTO = historyRecordDTO;
	}
	public Map<Long, Long> getScores() {
		return scores;
	}
	public void setScores(Map<Long, Long> scores) {
		this.scores = scores;
	}
	public Map<Long, String> getServiceNos() {
		return serviceNos;
	}
	public void setServiceNos(Map<Long, String> serviceNos) {
		this.serviceNos = serviceNos;
	}
	public List<Long> getRelatedCiNos() {
		return relatedCiNos;
	}
	public void setRelatedCiNos(List<Long> relatedCiNos) {
		this.relatedCiNos = relatedCiNos;
	}
	public String getServiceDirIdsStr() {
		return serviceDirIdsStr;
	}
	public void setServiceDirIdsStr(String serviceDirIdsStr) {
		this.serviceDirIdsStr = serviceDirIdsStr;
	}
	//是否根据繁忙程度自动指派
    private Boolean automaticallyAssigned = false;
    
	public Long[] getRelatedConfigureItemNos() {
		return relatedConfigureItemNos;
	}
	public void setRelatedConfigureItemNos(Long[] relatedConfigureItemNos) {
		this.relatedConfigureItemNos = relatedConfigureItemNos;
	}
	public String getWeekNo() {
		return weekNo;
	}
	public void setWeekNo(String weekNo) {
		this.weekNo = weekNo;
	}
	public Long[] getServiceDirIds() {
		return serviceDirIds;
	}
	public void setServiceDirIds(Long[] serviceDirIds) {
		this.serviceDirIds = serviceDirIds;
	}
	public Long getRequestServiceDirNo() {
		return requestServiceDirNo;
	}
	public void setRequestServiceDirNo(Long requestServiceDirNo) {
		this.requestServiceDirNo = requestServiceDirNo;
	}
	public List<EventCategory> getServiceDirectory() {
		return serviceDirectory;
	}
	public void setServiceDirectory(List<EventCategory> serviceDirectory) {
		this.serviceDirectory = serviceDirectory;
	}
	
	public CommentDTO getSolutionsComment() {
		return solutionsComment;
	}
	public void setSolutionsComment(CommentDTO solutionsComment) {
		this.solutionsComment = solutionsComment;
	}
	public Long getOffmodeNo() {
		return offmodeNo;
	}
	public void setOffmodeNo(Long offmodeNo) {
		this.offmodeNo = offmodeNo;
	}
	public String getOffmodeName() {
		return offmodeName;
	}
	public void setOffmodeName(String offmodeName) {
		this.offmodeName = offmodeName;
	}
	public Long getSlaRuleNo() {
		return slaRuleNo;
	}
	public void setSlaRuleNo(Long slaRuleNo) {
		this.slaRuleNo = slaRuleNo;
	}
	public Long getSlaContractNo() {
		return slaContractNo;
	}
	public void setSlaContractNo(Long slaContractNo) {
		this.slaContractNo = slaContractNo;
	}
	public Long getLocationNo() {
		return locationNo;
	}
	public void setLocationNo(Long locationNo) {
		this.locationNo = locationNo;
	}
	public String getRequestServiceDirName() {
		return requestServiceDirName;
	}
	public void setRequestServiceDirName(String requestServiceDirName) {
		this.requestServiceDirName = requestServiceDirName;
	}
	public Long getAssginTechnicalGroupId() {
		return assginTechnicalGroupId;
	}
	public void setAssginTechnicalGroupId(Long assginTechnicalGroupId) {
		this.assginTechnicalGroupId = assginTechnicalGroupId;
	}
	public String getAssginTechnicalGroupName() {
		return assginTechnicalGroupName;
	}
	public void setAssginTechnicalGroupName(String assginTechnicalGroupName) {
		this.assginTechnicalGroupName = assginTechnicalGroupName;
	}
	public Long[] getAids() {
		return aids;
	}
	public void setAids(Long[] aids) {
		this.aids = aids;
	}
	public String getProcessKey() {
		return processKey;
	}
	public void setProcessKey(String processKey) {
		this.processKey = processKey;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public Long getOrganizationNo() {
		return organizationNo;
	}
	public void setOrganizationNo(Long organizationNo) {
		this.organizationNo = organizationNo;
	}

	@Override
	public String toString() {
	    StringBuffer result = new StringBuffer();
	    result.append("RequestDTO")
        .append("[")
        .append("eno=").append(eno).append(", ")
        .append("ecategoryNo=").append(ecategoryNo).append(", ")
        .append("ecategoryName=").append(ecategoryName).append(", ")
        .append("etitle=").append(etitle).append(", ")
        .append("edesc=").append(edesc).append(", ")
        .append("effectRangeNo=").append(effectRangeNo).append(", ")
        .append("effectRangeName=").append(effectRangeName).append(", ")
        .append("effectRemark=").append(effectRemark).append(", ")
        .append("seriousnessNo=").append(seriousnessNo).append(", ")
        .append("seriousnessName=").append(seriousnessName).append(", ")
        .append("priorityNo=").append(priorityNo).append(", ")
        .append("priorityName=").append(priorityName).append(", ")
        .append("statusNo=").append(statusNo).append(", ")
        .append("statusName=").append(statusName).append(", ")
        .append("address=").append(address).append(", ")
        .append("createdOn=").append(createdOn).append(", ")
        .append("createdByNo=").append(createdByNo).append(", ")
        .append("createdByName=").append(createdByName).append(", ")
        .append("technicianNo=").append(technicianNo).append(", ")
        .append("technicianName=").append(technicianName).append(", ")
        .append("pid=").append(pid).append(", ")
        .append("solutions=").append(solutions).append(", ")
        .append("slaDoneDate=").append(slaDoneDate).append(", ")
        .append("slaResponseDate=").append(slaResponseDate).append(", ")
        .append("keyWord=").append(keyWord).append(", ")
        .append("lastUpdateTime=").append(lastUpdateTime).append(", ")
        .append("imodeNo=").append(imodeNo).append(", ")
        .append("levelNo=").append(levelNo).append(", ")
        .append("assigneeNo=").append(assigneeNo).append(", ")
        .append("assigneeName=").append(assigneeName).append(", ")
        .append("assigneeGroupNo=").append(assigneeGroupNo).append(", ")
        .append("assigneeGroupName=").append(assigneeGroupName).append(", ")
        .append("maxCompleteTime=").append(maxCompleteTime).append(", ")
        .append("responseTime=").append(responseTime).append(", ")
        .append("completeTime=").append(completeTime).append(", ")
        .append("servicesNo=").append(servicesNo).append(", ")
        .append("isFCR=").append(isFCR).append(", ")
        .append("optType=").append(optType).append(", ")
        .append("closeTime=").append(closeTime).append(", ")
        .append("requestCode=").append(requestCode).append(", ")
        .append("ownerNo=").append(ownerNo).append(", ")
        .append("remarkStatusNo=").append(remarkStatusNo).append(", ")
        .append("requestResolvedUserNo=").append(requestResolvedUserNo).append(", ")
        .append("requestResolvedTime=").append(requestResolvedTime).append(", ")
        .append("matchRuleName=").append(matchRuleName).append(", ")
        .append("upgradeApplySign=").append(upgradeApplySign).append(", ")
        .append("maxCompleteTimeBack=").append(maxCompleteTimeBack).append(", ")
        .append("autoUpdate=").append(autoUpdate).append(", ")
        .append("requestCategoryNo=").append(requestCategoryNo).append(", ")
        .append("requestCategoryName=").append(requestCategoryName).append(", ")
        .append("updateLevelNo=").append(updateLevelNo).append(", ")
        .append("approvalNo=").append(approvalNo).append(", ")
        .append("approvalName=").append(approvalName).append(", ")
        .append("approvalResult=").append(approvalResult).append(", ")
        .append("taskId=").append(taskId).append(", ")
        .append("currentUser=").append(currentUser).append(", ")
        .append("creator=").append(creator).append(", ")
        .append("visitRecord=").append(visitRecord).append(", ")
        .append("attachmentStr=").append(attachmentStr).append(", ")
        .append("emailMessage=").append(emailMessage).append(", ")
        .append("companyName=").append(companyName).append(", ")
        .append("companyNo=").append(companyNo).append(", ")
        .append("organizationNo=").append(organizationNo).append(", ")
        .append("actionName=").append(actionName).append(", ")
        .append("hang=").append(hang).append(", ")
        .append("requestResponseTime=").append(requestResponseTime).append(", ")
        .append("requestCompleteTime=").append(requestCompleteTime).append(", ")
        .append("attrVals=").append(attrVals).append(", ")
        .append("visitState=").append(visitState).append(", ")
        .append("visitPath=").append(visitPath).append(", ")
        .append("maxResponseTime=").append(maxResponseTime).append(", ")
        .append("slaNo=").append(slaNo).append(", ")
        .append("trueStartTime=").append(trueStartTime).append(", ")
        .append("currTime=").append(currTime).append(", ")
        .append("remark=").append(remark).append(", ")
        .append("technologyGroup=").append(technologyGroup).append(", ")
        .append("auditResult=").append(auditResult)
        .append("]");
	    
		return result.toString();
	}
	public Long getEno() {
		return eno;
	}
	public void setEno(Long eno) {
		this.eno = eno;
	}
	public Long getEcategoryNo() {
		return ecategoryNo;
	}
	public void setEcategoryNo(Long ecategoryNo) {
		this.ecategoryNo = ecategoryNo;
	}
	public String getEcategoryName() {
		return ecategoryName;
	}
	public void setEcategoryName(String ecategoryName) {
		this.ecategoryName = ecategoryName;
	}
	public String getEtitle() {
		return etitle;
	}
	public void setEtitle(String etitle) {
		this.etitle = etitle;
	}
	public String getEdesc() {
		return edesc;
	}
	public void setEdesc(String edesc) {
		this.edesc = edesc;
	}
	public Long getEffectRangeNo() {
		return effectRangeNo;
	}
	public void setEffectRangeNo(Long effectRangeNo) {
		this.effectRangeNo = effectRangeNo;
	}
	public String getEffectRangeName() {
		return effectRangeName;
	}
	public void setEffectRangeName(String effectRangeName) {
		this.effectRangeName = effectRangeName;
	}
	public String getEffectRemark() {
		return effectRemark;
	}
	public void setEffectRemark(String effectRemark) {
		this.effectRemark = effectRemark;
	}
	public Long getSeriousnessNo() {
		return seriousnessNo;
	}
	public void setSeriousnessNo(Long seriousnessNo) {
		this.seriousnessNo = seriousnessNo;
	}
	public String getSeriousnessName() {
		return seriousnessName;
	}
	public void setSeriousnessName(String seriousnessName) {
		this.seriousnessName = seriousnessName;
	}
	public Long getPriorityNo() {
		return priorityNo;
	}
	public void setPriorityNo(Long priorityNo) {
		this.priorityNo = priorityNo;
	}
	public String getPriorityName() {
		return priorityName;
	}
	public void setPriorityName(String priorityName) {
		this.priorityName = priorityName;
	}
	public Long getStatusNo() {
		return statusNo;
	}
	public void setStatusNo(Long statusNo) {
		this.statusNo = statusNo;
	}
	public String getStatusName() {
		return statusName;
	}
	public void setStatusName(String statusName) {
		this.statusName = statusName;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	public Long getCreatedByNo() {
		return createdByNo;
	}
	public void setCreatedByNo(Long createdByNo) {
		this.createdByNo = createdByNo;
	}
	public String getCreatedByName() {
		return createdByName;
	}
	public void setCreatedByName(String createdByName) {
		this.createdByName = createdByName;
	}
	public Long getTechnicianNo() {
		return technicianNo;
	}
	public void setTechnicianNo(Long technicianNo) {
		this.technicianNo = technicianNo;
	}
	public String getTechnicianName() {
		return technicianName;
	}
	public void setTechnicianName(String technicianName) {
		this.technicianName = technicianName;
	}
	public String getPid() {
		return pid;
	}
	public void setPid(String pid) {
		this.pid = pid;
	}

	public String getSolutions() {
		return solutions;
	}
	public void setSolutions(String solutions) {
		this.solutions = solutions;
	}
	public Date getSlaDoneDate() {
		return slaDoneDate;
	}
	public void setSlaDoneDate(Date slaDoneDate) {
		this.slaDoneDate = slaDoneDate;
	}
	public Date getSlaResponseDate() {
		return slaResponseDate;
	}
	public void setSlaResponseDate(Date slaResponseDate) {
		this.slaResponseDate = slaResponseDate;
	}
	public String getKeyWord() {
		return keyWord;
	}
	public void setKeyWord(String keyWord) {
		this.keyWord = keyWord;
	}
	public Long getImodeNo() {
		return imodeNo;
	}
	public void setImodeNo(Long imodeNo) {
		this.imodeNo = imodeNo;
	}
	public Long getLevelNo() {
		return levelNo;
	}
	public void setLevelNo(Long levelNo) {
		this.levelNo = levelNo;
	}
	public Long getAssigneeNo() {
		return assigneeNo;
	}
	public void setAssigneeNo(Long assigneeNo) {
		this.assigneeNo = assigneeNo;
	}
	public String getAssigneeName() {
		return assigneeName;
	}
	public void setAssigneeName(String assigneeName) {
		this.assigneeName = assigneeName;
	}
	public Long getAssigneeGroupNo() {
		return assigneeGroupNo;
	}
	public void setAssigneeGroupNo(Long assigneeGroupNo) {
		this.assigneeGroupNo = assigneeGroupNo;
	}
	public String getAssigneeGroupName() {
		return assigneeGroupName;
	}
	public void setAssigneeGroupName(String assigneeGroupName) {
		this.assigneeGroupName = assigneeGroupName;
	}
	public Long getMaxCompleteTime() {
		return maxCompleteTime;
	}
	public void setMaxCompleteTime(Long maxCompleteTime) {
		this.maxCompleteTime = maxCompleteTime;
	}
	public Long getResponseTime() {
		return responseTime;
	}
	public void setResponseTime(Long responseTime) {
		this.responseTime = responseTime;
	}
	public Long getCompleteTime() {
		return completeTime;
	}
	public void setCompleteTime(Long completeTime) {
		this.completeTime = completeTime;
	}
	public Long getServicesNo() {
		return servicesNo;
	}
	public void setServicesNo(Long servicesNo) {
		this.servicesNo = servicesNo;
	}
	public String getOptType() {
		return optType;
	}
	public void setOptType(String optType) {
		this.optType = optType;
	}
	public Date getCloseTime() {
		return closeTime;
	}
	public void setCloseTime(Date closeTime) {
		this.closeTime = closeTime;
	}
	public String getRequestCode() {
		return requestCode;
	}
	public void setRequestCode(String requestCode) {
		this.requestCode = requestCode;
	}
	public Long getOwnerNo() {
		return ownerNo;
	}
	public void setOwnerNo(Long ownerNo) {
		this.ownerNo = ownerNo;
	}
	public Long getRemarkStatusNo() {
		return remarkStatusNo;
	}
	public void setRemarkStatusNo(Long remarkStatusNo) {
		this.remarkStatusNo = remarkStatusNo;
	}
	public Long getRequestResolvedUserNo() {
		return requestResolvedUserNo;
	}
	public void setRequestResolvedUserNo(Long requestResolvedUserNo) {
		this.requestResolvedUserNo = requestResolvedUserNo;
	}
	public Date getRequestResolvedTime() {
		return requestResolvedTime;
	}
	public void setRequestResolvedTime(Date requestResolvedTime) {
		this.requestResolvedTime = requestResolvedTime;
	}
	public String getMatchRuleName() {
		return matchRuleName;
	}
	public void setMatchRuleName(String matchRuleName) {
		this.matchRuleName = matchRuleName;
	}
	public Long getUpgradeApplySign() {
		return upgradeApplySign;
	}
	public void setUpgradeApplySign(Long upgradeApplySign) {
		this.upgradeApplySign = upgradeApplySign;
	}
	public Long getMaxCompleteTimeBack() {
		return maxCompleteTimeBack;
	}
	public void setMaxCompleteTimeBack(Long maxCompleteTimeBack) {
		this.maxCompleteTimeBack = maxCompleteTimeBack;
	}
	public String getAutoUpdate() {
		return autoUpdate;
	}
	public void setAutoUpdate(String autoUpdate) {
		this.autoUpdate = autoUpdate;
	}
	public Long getRequestCategoryNo() {
		return requestCategoryNo;
	}
	public void setRequestCategoryNo(Long requestCategoryNo) {
		this.requestCategoryNo = requestCategoryNo;
	}
	public String getRequestCategoryName() {
		return requestCategoryName;
	}
	public void setRequestCategoryName(String requestCategoryName) {
		this.requestCategoryName = requestCategoryName;
	}
	public Long getUpdateLevelNo() {
		return updateLevelNo;
	}
	public void setUpdateLevelNo(Long updateLevelNo) {
		this.updateLevelNo = updateLevelNo;
	}
	public Long getApprovalNo() {
		return approvalNo;
	}
	public void setApprovalNo(Long approvalNo) {
		this.approvalNo = approvalNo;
	}
	public String getApprovalName() {
		return approvalName;
	}
	public void setApprovalName(String approvalName) {
		this.approvalName = approvalName;
	}
	public String getApprovalResult() {
		return approvalResult;
	}
	public void setApprovalResult(String approvalResult) {
		this.approvalResult = approvalResult;
	}
	public String getTaskId() {
		return taskId;
	}
	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}
	public String getCurrentUser() {
		return currentUser;
	}
	public void setCurrentUser(String currentUser) {
		this.currentUser = currentUser;
	}
	public String getCreator() {
		return creator;
	}
	public void setCreator(String creator) {
		this.creator = creator;
	}
	public String getVisitRecord() {
		return visitRecord;
	}
	public void setVisitRecord(String visitRecord) {
		this.visitRecord = visitRecord;
	}
	public String getAttachmentStr() {
		return attachmentStr;
	}
	public void setAttachmentStr(String attachmentStr) {
		this.attachmentStr = attachmentStr;
	}
	public Long getEmailMessage() {
		return emailMessage;
	}
	public void setEmailMessage(Long emailMessage) {
		this.emailMessage = emailMessage;
	}
	public String getCompanyName() {
		return companyName;
	}
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	public Long getCompanyNo() {
		return companyNo;
	}
	public void setCompanyNo(Long companyNo) {
		this.companyNo = companyNo;
	}
	public String getActionName() {
		return actionName;
	}
	public void setActionName(String actionName) {
		this.actionName = actionName;
	}
	public Date getRequestResponseTime() {
		return requestResponseTime;
	}
	public void setRequestResponseTime(Date requestResponseTime) {
		this.requestResponseTime = requestResponseTime;
	}
	public Date getRequestCompleteTime() {
		return requestCompleteTime;
	}
	public void setRequestCompleteTime(Date requestCompleteTime) {
		this.requestCompleteTime = requestCompleteTime;
	}
	public Map<String, String> getAttrVals() {
		return attrVals;
	}
	public void setAttrVals(Map<String, String> attrVals) {
		this.attrVals = attrVals;
	}
	public Long getVisitState() {
		return visitState;
	}
	public void setVisitState(Long visitState) {
		this.visitState = visitState;
	}
	public String getVisitPath() {
		return visitPath;
	}
	public void setVisitPath(String visitPath) {
		this.visitPath = visitPath;
	}
	public Long getMaxResponseTime() {
		return maxResponseTime;
	}
	public void setMaxResponseTime(Long maxResponseTime) {
		this.maxResponseTime = maxResponseTime;
	}
	public Long getSlaNo() {
		return slaNo;
	}
	public void setSlaNo(Long slaNo) {
		this.slaNo = slaNo;
	}
	public Long getTrueStartTime() {
		return trueStartTime;
	}
	public void setTrueStartTime(Long trueStartTime) {
		this.trueStartTime = trueStartTime;
	}
	public Long getCurrTime() {
		return currTime;
	}
	public void setCurrTime(Long currTime) {
		this.currTime = currTime;
	}
	public Date getLastUpdateTime() {
		return lastUpdateTime;
	}
	public void setLastUpdateTime(Date lastUpdateTime) {
		this.lastUpdateTime = lastUpdateTime;
	}
	public String getProcessNextStep() {
		return processNextStep;
	}
	public void setProcessNextStep(String processNextStep) {
		this.processNextStep = processNextStep;
	}
	public Long getSlaStatusNo() {
		return slaStatusNo;
	}
	public void setSlaStatusNo(Long slaStatusNo) {
		this.slaStatusNo = slaStatusNo;
	}
	public String getAssignType() {
		return assignType;
	}
	public void setAssignType(String assignType) {
		this.assignType = assignType;
	}
    
	public Long getCicategoryNo() {
		return cicategoryNo;
	}
	public void setCicategoryNo(Long cicategoryNo) {
		this.cicategoryNo = cicategoryNo;
	}
	
	public Long[] getOrganizationNos() {
		return organizationNos;
	}
	public void setOrganizationNos(Long[] organizationNos) {
		this.organizationNos = organizationNos;
	}
	public String getCreatedByEmail() {
		return createdByEmail;
	}
	public void setCreatedByEmail(String createdByEmail) {
		if( createdByEmail != null ){
			int start = createdByEmail.indexOf("<") + 1;
			int end = createdByEmail.indexOf(">");
			if( start > 0 && (end - start) > 4 ){//两个括号中内容长度最少为5 <a@b.c>
				createdByEmail = createdByEmail.substring(start, end).trim();
			}
		}
		this.createdByEmail = createdByEmail;
	}
	public Boolean getIsFCR() {
		return isFCR;
	}
	public void setIsFCR(Boolean isFCR) {
		this.isFCR = isFCR;
	}
	public Boolean getHang() {
		return hang;
	}
	public void setHang(Boolean hang) {
		this.hang = hang;
	}
	public Boolean getAuditResult() {
		return auditResult;
	}
	public void setAuditResult(Boolean auditResult) {
		this.auditResult = auditResult;
	}
	public Boolean getSlaExpired() {
		return slaExpired;
	}
	public void setSlaExpired(Boolean slaExpired) {
		this.slaExpired = slaExpired;
	}
	public Boolean getMailToRequest() {
		return mailToRequest;
	}
	public void setMailToRequest(Boolean mailToRequest) {
		this.mailToRequest = mailToRequest;
	}
	public Boolean getAutomaticallyAssigned() {
		return automaticallyAssigned;
	}
	public void setAutomaticallyAssigned(Boolean automaticallyAssigned) {
		this.automaticallyAssigned = automaticallyAssigned;
	}
	public Map<String, Object> getPiv() {
		piv.put("approvalNo", 0L);
		piv.put("etitle", null);
		piv.put("eventCode", null);
		piv.put("eno", null);
		piv.put("actionName", null);
		piv.put("creator", null);
		piv.put("technicianName", null);
		piv.put("approvalName", null);
		return piv;
	}
	public void setPiv(Map<String, Object> piv) {
		this.piv = piv;
	}
	public String getEventCode() {
		return eventCode;
	}
	public void setEventCode(String eventCode) {
		this.eventCode = eventCode;
	}
	public Long[] getCicategoryNos() {
		return cicategoryNos;
	}
	public void setCicategoryNos(Long[] cicategoryNos) {
		this.cicategoryNos = cicategoryNos;
	}
	public Boolean getAttendance() {
		return attendance;
	}
	public void setAttendance(Boolean attendance) {
		this.attendance = attendance;
	}
}
