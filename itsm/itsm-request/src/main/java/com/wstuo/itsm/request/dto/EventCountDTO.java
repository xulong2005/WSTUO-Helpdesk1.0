package com.wstuo.itsm.request.dto;

import java.util.Date;

import com.wstuo.common.dto.BaseDTO;

/**
 * 统计数量DTO
 * @author QXY
 *
 */
@SuppressWarnings("serial")
public class EventCountDTO extends BaseDTO{
	private Long eventCountId;
	/**创建日期*/
	private Date eventCreateDate;
	/**事件类型*/
	private String type;
	/**事件编号*/
	private Long eno;
	public Long getEventCountId() {
		return eventCountId;
	}
	public void setEventCountId(Long eventCountId) {
		this.eventCountId = eventCountId;
	}
	public Date getEventCreateDate() {
		return eventCreateDate;
	}
	public void setEventCreateDate(Date eventCreateDate) {
		this.eventCreateDate = eventCreateDate;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public Long getEno() {
		return eno;
	}
	public void setEno(Long eno) {
		this.eno = eno;
	}
	
}
