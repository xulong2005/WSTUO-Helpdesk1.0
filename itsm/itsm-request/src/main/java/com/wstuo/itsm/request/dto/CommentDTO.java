package com.wstuo.itsm.request.dto;

import java.util.List;

import com.wstuo.common.config.attachment.entity.Attachment;
import com.wstuo.common.dto.BaseDTO;

/**
 * Comment辅助类.
 * @author QXY
 *
 */
public class CommentDTO extends BaseDTO {

	private Long eno;
	private String remark;
	private String attachmentStr;
	private List<Attachment> attachments;

	public List<Attachment> getAttachments() {
		return attachments;
	}
	public void setAttachments(List<Attachment> attachments) {
		this.attachments = attachments;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public String getAttachmentStr() {
		return attachmentStr;
	}
	public void setAttachmentStr(String attachmentStr) {
		this.attachmentStr = attachmentStr;
	}
	public Long getEno() {
		return eno;
	}
	public void setEno(Long eno) {
		this.eno = eno;
	}
	
	public CommentDTO(){
		
	}
	
	public CommentDTO(Long eno, String remark, String attachmentStr,
			List<Attachment> attachments) {
		super();
		this.eno = eno;
		this.remark = remark;
		this.attachmentStr = attachmentStr;
		this.attachments = attachments;
	}
	
	
	
	
	
}
