package com.wstuo.itsm.request.dto;

import com.wstuo.common.dto.BaseDTO;
/**
 * HTTP创建请求信息DTO
 * @author will
 *
 */
@SuppressWarnings( "serial" )
public class RequestHttpDTO extends BaseDTO{

	private String phoneNumber;
	private String message;
	private String loginName;
	private String password;
	private String tenantId;
	
	public String getTenantId() {
		return tenantId;
	}
	public void setTenantId(String tenantId) {
		this.tenantId = tenantId;
	}
	public String getPhoneNumber() {
		return phoneNumber;
	}
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getLoginName() {
		return loginName;
	}
	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	
	
}
