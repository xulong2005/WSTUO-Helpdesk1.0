package com.wstuo.itsm.request.entity;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;

import com.wstuo.common.entity.BaseEntity;



/**
 * 请求草稿
 *
 */
@SuppressWarnings({ "rawtypes", "serial" })
@Entity
public class RequestDraft extends BaseEntity {
	@Id
    @GeneratedValue( strategy = GenerationType.AUTO )
	private Long requestDraftID;
	@Lob
	private String requestJson;
	@Lob
	private String edesc;
	private String requestDraftName;
	private String loginName;
	
	
	public String getLoginName() {
		return loginName;
	}
	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}
	public String getEdesc() {
		return edesc;
	}
	public void setEdesc(String edesc) {
		this.edesc = edesc;
	}
	public Long getRequestDraftID() {
		return requestDraftID;
	}
	public void setRequestDraftID(Long requestDraftID) {
		this.requestDraftID = requestDraftID;
	}
	public String getRequestJson() {
		return requestJson;
	}
	public void setRequestJson(String requestJson) {
		this.requestJson = requestJson;
	}
	public String getRequestDraftName() {
		return requestDraftName;
	}
	public void setRequestDraftName(String requestDraftName) {
		this.requestDraftName = requestDraftName;
	}
	
    
   
    
}
