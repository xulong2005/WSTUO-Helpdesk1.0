package com.wstuo.common.createmq;

import org.hibernate.internal.SessionFactoryImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.support.TransactionSynchronizationManager;

import com.wstuo.itsm.request.dto.RequestHttpDTO;
import com.wstuo.itsm.request.service.IRequestService;
import com.wstuo.multitenancy.TenantIdResolver;

/**
 * 创建请求信息消费
 * @author will
 *
 */
public class RequestCreateMessageConsumer{

	@Autowired
	private IRequestService requestService;
	
	private SessionFactoryImpl sessionFactory;
	
	
	public SessionFactoryImpl getSessionFactory() {
		return sessionFactory;
	}

	public void setSessionFactory(SessionFactoryImpl sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	/**
	 * 请求导出消费信息方法
	 * @param dto
	 */
	public void saveRequestConsumer(RequestHttpDTO httpDto) {
		TransactionSynchronizationManager.unbindResourceIfPossible(sessionFactory);
		TenantIdResolver tenantIdResolver = (TenantIdResolver)sessionFactory.getCurrentTenantIdentifierResolver();
		if(tenantIdResolver!=null){
			tenantIdResolver.setTenantId(httpDto.getTenantId());
		}
		requestService.createRequest(httpDto);
	}
}
