package com.wstuo.common.exportmq;

import org.springframework.beans.factory.annotation.Autowired;

import com.wstuo.common.activemq.consumer.IMessageConsumer;
import com.wstuo.common.tools.dto.ExportQueryDTO;
import com.wstuo.itsm.request.service.IRequestService;
/**
 * MQ请求导出
 * @author will
 *
 */
public class RequestExportConsumer implements IMessageConsumer{

	@Autowired
	private IRequestService requestService;
	
	public void messageConsumer(Object obj) {
		ExportQueryDTO dto=(ExportQueryDTO) obj;
		requestService.commonExportData(dto);
	}

}
