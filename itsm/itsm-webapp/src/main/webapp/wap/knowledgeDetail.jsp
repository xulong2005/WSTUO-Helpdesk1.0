<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%
String url="login.jsp";

 if(session.getAttribute("loginUserName")==null){
	response.sendRedirect(url);
	return;
} 
 %>
<!doctype html>
<html class="no-js">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="renderer" content="webkit">
<meta http-equiv="Cache-Control" content="no-siteapp" />
<title>WSTUO-企业一体化解决方案</title>

<link rel="icon" type="image/png" href="../images/logo2.png">
<link rel="stylesheet" href="assets/css/amazeui.min.css"/>
<link rel="stylesheet" href="assets/css/app.css">
<script type="text/javascript">
var fullName="${sessionScope.fullName}";
var userName="${sessionScope.loginUserName}";//当前登录用户

</script>

</head>
<body>
	<header data-am-widget="header" class="am-header am-header-default">
	    <h1 class="am-header-title">知识详情 </h1>  
	</header>
	<input type="hidden" value="${knowledgeDto.kid}" id="knowledge_kid" >
	<!-- Menu -->
	<nav data-am-widget="menu" class="am-menu am-menu-offcanvas1" data-am-menu-offcanvas>
		<a href="javascript:history.go(-1);" class="am-menu-left" >
	    	<i style="text-align: right;padding: 0px;" class="am-menu-toggle-icon am-icon-chevron-left"></i>
	  	</a>
	  	  <a href="javascript: location.reload();" class="am-menu-left" style="left: 30px;">
		    <i style="text-align: right;padding: 0px;" class="am-menu-toggle-icon am-icon-refresh"></i>
		  </a>
  		<a href="javascript: void(0)" class="am-menu-toggle">
	    	<i style="text-align: right;padding: 0px;" class="am-menu-toggle-icon am-icon-bars"></i>
	  	</a>
	  	<div class="am-offcanvas">
		    <div class="am-offcanvas-bar">
		    	<ul class="am-menu-nav sm-block-grid-1">      
			        <li>
			          <a href="user!findUserDetail_wap.action?userName=${sessionScope.loginUserName}">个人信息</a>        
			        </li>
			        <li >
			          <a href="pwdUpdate.jsp">密码修改</a>       
			        </li>  
			        <li>
			          <a href="index.jsp">返回首页</a>       
			        </li>   
			        <li>
			          <a href="logout.jsp">注&nbsp;&nbsp;销</a>       
			        </li>
			      </ul>
		    </div>
	  	</div>
	</nav>
	
	<header style="margin-top: 20px;text-align: center;">
  		<div class="am-u-sm-12 am-article" style="margin-bottom: 10px;">
	    	<h1 class="am-article-title">${knowledgeDto.title}</h1>
	    	<p class="am-article-meta">创建者：${knowledgeDto.creatorFullName}</p>
  		</div>
	</header>

<div class="am-g am-g-fixed">
	<div style="width: 100%;">
		<div class="am-u-sm-11 am-u-sm-centered" style="width: 98%;">
			<p style="text-align: center;margin-bottom: 2px;font-size: 1.3rem;color: #999999;">
				&nbsp;&nbsp;录入时间：${knowledgeDto.addTime}&nbsp;&nbsp; 点击：${knowledgeDto.clickRate}
			</p>
			<div class="am-cf am-article">
				<hr style="margin-top: 2px;margin-bottom: 5px;"/>
				<h3 style="margin-top: 5px;margin-left: 10px;margin-bottom: 5px;">内容：</h3>
				<p style="margin-top: 5px;margin-left: 10px;margin-right: 5px;margin-bottom: 1px;">
					&nbsp;&nbsp;&nbsp;&nbsp;${knowledgeDto.content}
				</p>
				<hr style="margin-top: 1px;"/>	
			</div>
      		<div class="am-cf am-article" id="knowledgestars" style="margin-top: 3px;">
        		<form>
					<input type="hidden" value="${sessionScope.fullName}" name="knowledgeDto.creator" id="creator"> 
					<input type="hidden" value="${knowledgeDto.kid}" id="kid" name="knowledgeDto.kid">
					<table style="width: 100%;margin-top: 3px;">
						<tr>
							<td style="width: 60px;height: 35px;">
							<!-- <fmt:message key="star" />  -->
							星级：
							</td>
							<td>
								<div style="margin-left: 2px;" id="star2"></div>
								<div style="margin-left: 18px;" id="result2"></div>
								<input type="hidden" name="knowledgeDto.stars" id="value">
							</td>
						</tr>
						<tr>
							<td>
							<!-- <fmt:message key="content" />  -->
							内容：
							</td>
							<td>
								<textarea name="knowledgeDto.remark" style="width: 100%;height: 80px;" id="reply_content" class="form-control" name="Evaluationdto.content"></textarea>
						    </td>
						</tr>
						<tr>
						  <td colspan="2" style="text-align: center;">
						  	
						     <button type="button" class="am-btn am-btn-primary am-btn-sm" id="saveEvaluation" style="margin-top: 15px;width: 30%" >
						     	评&nbsp;&nbsp;论
						     <!-- <fmt:message key="common.save" /> -->
						     </button>
						  </td>									
						</tr>
					</table>
				</form>	
      		</div>
			<hr style="margin-top: 10px;margin-bottom: 5px;"/>		
	 		<ul class="am-comments-list" id="tableContent" >
	  		
	    	</ul>
    	</div>
	</div>
</div>     
<%@ include file="include_tip.jsp"%>                   	
<script src="assets/js/jquery.min.js"></script>
<script src="assets/js/amazeui.min.js"></script>
<script src="../js/js/jquery.raty.min.js"></script>
<script src="../js/i18n/i18n_zh_CN.js"></script>
<script src="js/common.js"></script>
<script type="text/javascript">

rat('star2','result2',1);
function rat(star,result,m){
	star= '#' + star;
	result= '#' + result;
	$(result).hide();//将结果DIV隐藏

	$(star).raty({
		hints: ['一星','二星', '三星', '四星', '五星'],
		path: "${pageContext.request.contextPath}/images/img",
		starOff: 'star-off.png',
		starOn: 'star-on.png',
		size: 24,
		start: 40,
		showHalf: true,
		target: result,
		targetKeep : true,//targetKeep 属性设置为true，用户的选择值才会被保持在目标DIV中，否则只是鼠标悬停时有值，而鼠标离开后这个值就会消失
		click: function (score, evt) {
			$("#value").val(score*m);
	
		}
	});
}

initCommont();
function initCommont(){
	$.get("knowledgeInfo!findByIdCommentKnowledge.action?kid="+$("#knowledge_kid").val(),function(data){
	    //data 就是你的数据
		$.each(data,function(key,value){
			var stars=value.stars;
			if(stars=='undefined'){
				stars="无"
			}
			/* "+value.creator+" */
			$("#tableContent").append(
				"<li class='am-comment'>"+
				"<a href='#link-to-user-home'>"+
	            "<img src='assets/img/logo2.png' class='am-comment-avatar' width='48' height='48'>"+		              
	            "</a>"+		            
	            "<div class='am-comment-main' >"+
	              "<header class='am-comment-hd'>"+
	                "<div class='am-comment-meta' style='height: 35px;'>"+
	                 "<p style='color:#AAA;'>"+value.creator+"&nbsp;&nbsp;"+value.createTime+"&nbsp;"+jsimg(stars)+"</p>"+	      
	                "</div>"+
	              "</header>"+
	              "<div class='am-comment-bd'>"+
	                "<p style='font-size: 14px'>&nbsp;&nbsp;&nbsp;&nbsp;"+value.remark+"</p>"+
	              "</div>"+
	            "</div>");
		            
		});
	});
}

function jsimg(count){
	var imgsrc="";
	if(count){
		for(var i=0;i<count;i++){
			imgsrc+="<img src='${pageContext.request.contextPath}/images/img/star-on.png' style='vertical-align:middle;width: 15px;height: 15px;'/>"
		}
	}
	return imgsrc;
}

$("#saveEvaluation").click(function(){

	//var val_payPlatform = $('#radio input[name="knowledgeDto.stars"]:checked ').val();
	var content=$("#reply_content").val();
	
	if(!content){
		msgAlert(i18n.validate_notNull);
		return;
	}

	var stars=$("#value").val();
	if(stars==""){
		msgAlert(i18n.msg_stars_notnull);
		return;
	}
	var _param = $('#knowledgestars form').serialize();
	var _url='knowledgeInfo!saveCommentKnowledge.action';
	if(userName =="" || userName ==null){
		msgAlert(i18n.common_systemTimeout);
		return;	
	}
	//进程状态
	startProcess();
	$.post(_url,_param,function(){
		msgAlert(i18n['saveSuccess']);
		$("#tableContent").prepend(
		
			"<li class='am-comment'>"+
			"<a href='#link-to-user-home'>"+
	        "<img src='assets/img/logo2.png' class='am-comment-avatar' width='48' height='48'>"+		              
	        "</a>"+		            
	        "<div class='am-comment-main'>"+
	          "<header class='am-comment-hd'>"+
	            "<div class='am-comment-meta' style='height: 35px;'>"+
	            "<p style='color:#AAA;'>"+fullName+"&nbsp&nbsp;"+i18n.msg_just+"&nbsp;&nbsp;"+jsimg(stars)+"</p>"+
	            "</div>"+
	          "</header>"+
	          "<div class='am-comment-bd'>"+
	            "<p>"+content+"</p>"+
	          "</div>"+
	        "</div>");
		
		
		//进程状态
        endProcess();
        rat('star2','result2',1);
        $("#reply_content").val('');
        $("#value").val('');
	}); 
});
</script>
</body>
</html>