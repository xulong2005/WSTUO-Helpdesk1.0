var addKnowledge = function() 
{
	/**
	 * @description 提交保存请求.
	 */
	this.saveKnowledge=function(){
		var edesc=editor.$txt.html();//$("#addKnowledge_Content").val();
		var title= $('#addKnowledgeForm #addKnowledge_title').val();
		$('#addKnowledgeForm #addKnowledge_title').val(trim(title));
		var knowledge_edesc=(edesc===''|| edesc=="null" || edesc=="NULL")?false:true;
		if(!knowledge_edesc){
			msgAlert(i18n.knowTitleAndContentNotNull,'info');
		}else{
			var frm = $('#addKnowledgeForm').serialize();
			var url = 'knowledgeInfo!saveKnowledgeInfo.action';
			startProcess();
			$.post(url,frm, function(id){
				endProcess();
				msgAlert(i18n.addSuccess);
				location.href="knowledgeList.jsp";
			});
		}
	};
	this.checkKnowledgeTitle=function(){
		var title=trim($('#addKnowledge_title').val());
		var _url='knowledgeInfo!findKnowledgeByTitle.action';
		var parame = {"knowledgeDto.title":title};
		$.post(_url,parame,function(data){
			if(data != null){
				msgAlert(i18n['knowledge_title_exist']);
			}else{
				saveKnowledge();
			}
		});
	};
	this.selectTree=function (){
		showSelectTree(
			'#knowledge_category_select_window',
            '#knowledge_category_select_tree',
            'knowledge',
            '#addKnowledge_Category',
            '#addKnowledge_categoryNo',
            'knowledgeDTO');
	};
	this.showSelectTree=function(windowId,treePanel,param,namePut,idPut,dtoName){
		 $(treePanel).jstree({
				json_data: {
					ajax: {
					  type:"post",
					  url : "event!getCategoryTree.action?num=30",
					  data:function(n){
				    	  return {'types': param,'parentEventId':n.attr ? n.attr("id").replace("node_",""):0};//types is in action
				      },
	                  dataType: 'json',
	                  cache:false
				 }
				},
				plugins : ["themes", "json_data","cookies", "ui", "crrm","search"]
		})
		.bind("select_node.jstree",function(e,data){
			var obj = data.rslt.obj;
			$(namePut).val(obj.attr("cname")).focus();
			$(idPut).val(obj.attr("id"));
			$(windowId).modal('close');
		});
		$(windowId).modal({width:'220px'});
	 };

	 return{
		init: function() {
			$("#addKnowledge_Category").click(selectTree);
			$("#save_btn").click(function(){
				$('#addKnowledgeForm').submit();
			});
			$("#reset_btn").click(function(){
				$('#addKnowledgeForm')[0].reset();
			});
			//初始化附件上传控件
			setTimeout(function(){
				getUploader('#addKnowledge_uploadAttachments','#addKnowledge_attachments','','',null,false);
			},0);
			initValidate();
		}
	 };
}();
//载入
$(document).ready(addKnowledge.init);