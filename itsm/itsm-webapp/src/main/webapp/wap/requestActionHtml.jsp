<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<style>
<!--
.WSTUO-dialog{display: none}
-->
</style>
<%@ include file="../pages/language.jsp" %>        
<div class="am-modal am-modal-no-btn" tabindex="-1" id="processTracelnstanceWin">
  <div class="am-modal-dialog">
    <div class="am-modal-hd">查看流程图
      <a href="javascript: void(0)" class="am-close am-close-spin" data-am-modal-close>&times;</a>
    </div>
    <div class="am-modal-bd" id="processTracelnstance">
    </div>
  </div>
</div>

<!--请求提取-->
<div class="am-modal am-modal-no-btn" tabindex="-1" id="requestGet_win">
  <div class="am-modal-dialog">
    <div class="am-modal-hd"><fmt:message key="label.request.requestExtraction" />
      <a href="javascript: void(0)" class="am-close am-close-spin" data-am-modal-close>&times;</a>
    </div>
    <div class="am-modal-bd" id="">
    <form event="requestAction.requestGet_opt">
	<div class="lineTableBgDiv">
		<table style="width:100%" cellspacing="1">
		<tr>
			<td style="width: 15%;"><fmt:message key="common.remark" /></td>
			<td>
				<textarea class="form-control"  style="width: 98%;height: 80px" name="historyRecordDto.logDetails" id="requestGet_value" class="form-control"  minlength='1' maxlength='200'></textarea>
			</td>
		</tr>
		<tr>
			<td colspan="2">
				<input type="hidden" name="historyRecordDto.logTitle" id="requestExtraction_action" value="<fmt:message key="label.request.requestExtraction" />" />
				<input type="hidden" name="requestDTO.optType" value="requestGet" />
				<button class="btn btn-primary btn-sm" style="margin-right: 15px;margin-top: 10px;"><fmt:message key="label.request.extract" /></button>
				<br/>
			</td>
		</tr>
	</table>
	</div>
	</form>
</div></div></div>

<!--请求重新开启-->
<div id="requestReOpen_win" class="WSTUO-dialog"  shadow="false" closed="true" modal="true"  title="<fmt:message key="title.request.reStartRequest" />" minimizable="false" maximizable="false" collapsible="false" style="width:480px;height:auto">
	<form event="requestAction.requestReOpen_opt">

		<table style="width:100%" cellspacing="1">
		<tr>
			<td><fmt:message key="common.remark" /></td>
			<td>
				<textarea name="historyRecordDto.logDetails" id="requestReOpen_value" class="form-control" required="true" style="height:80px"></textarea>
			</td>
		</tr>
		<%-- <tr>
			<td><fmt:message key="label.is.request.action.notice" /></td>
			<td>
			<div id="requestReOpen_noticeEmail_div" style="display: none;">
					<input style="width: 98%;" id="requestReOpen_noticeEmail_input" ondblclick="requestDetail.selectNoticeUser('#requestReOpen_noticeEmail_input','email')"/>
					<br><span><fmt:message key="label.email.address.format" /></span>
			</div>
			<input type="checkbox" value="true" id="requestReOpen_isEmailNotice" onclick="requestDetail.requestActionNoticeSet('#requestReOpen_isEmailNotice','#requestReOpen_noticeEmail_div')" />
			
			</td>
		</tr> --%>
		<tr>
			<td colspan="2">
				<input type="hidden" name="historyRecordDto.logTitle"  id="reStartRequest_action" value="<fmt:message key="title.request.reStartRequest" />" />
				<input type="hidden" name="requestDTO.optType" value="requestReOpen" />
				<input type="submit" class="btn btn-primary btn-sm" value="<fmt:message key="label.request.open" />"/>
			</td>
		</tr>
	</table>
	</form>
</div>

<!--请求退回组-->
<div id="requestBackGroup_win" class="WSTUO-dialog" title="<fmt:message key="label.request.requestBackToGroup" />" style="width:480px;height:auto">
	<form>

	<div class="lineTableBgDiv">
		<table style="width:100%" class="lineTable" cellspacing="1">
		<tr>
			<td><fmt:message key="common.remark" /></td>
			<td>
				<textarea class="form-control" required="true" style="height:80px" name="historyRecordDto.logDetails" id="requestBackGroup_value"></textarea>
			</td>
		</tr>
		<%-- <tr>
			<td><fmt:message key="label.is.request.action.notice" /></td>
			<td>
			<div id="requestBackGroup_noticeEmail_div" style="display: none;">
					<input style="width: 98%;" id="requestBackGroup_noticeEmail_input" ondblclick="requestDetail.selectNoticeUser('#requestBackGroup_noticeEmail_input','email')"/>
					<br><span><fmt:message key="label.email.address.format" /></span>
			</div>
			<input type="checkbox" value="true" id="requestBackGroup_isEmailNotice" onclick="requestDetail.requestActionNoticeSet('#requestBackGroup_isEmailNotice','#requestBackGroup_noticeEmail_div')" />
			
			</td>
		</tr> --%>
		<tr>
			<td colspan="2">
				<input type="hidden" name="historyRecordDto.logTitle" id="requestBackToGroup_action" value="<fmt:message key="label.request.requestBackToGroup" />" />
				<input type="hidden" name="requestDTO.optType" value="requestBackGroup" />
				<a class="btn btn-primary btn-sm" onclick="requestAction.requestBackGroup_opt()"><fmt:message key="label.request.back" /></a>
			</td>
		</tr>
	</table>
	</div>
	</form>
</div>

<!-- 请求指派 -->
	<div class="am-modal am-modal-no-btn" tabindex="-1" id="requestAssign_win">
  <div class="am-modal-dialog">
    <div class="am-modal-hd"><fmt:message key="label.request.requestAssign" />
      <a href="javascript: void(0)" class="am-close am-close-spin" data-am-modal-close>&times;</a>
    </div>
    <div class="am-modal-bd" id="">
    <form event="requestAction.requestAssign_opt_s">

		<table style="width:100%" cellspacing="1">
		<tr>
			<td style="width: 25%">工作组&nbsp;<span style="color:red">*</span></td>
			<td>
			<input type="hidden" name="requestDTO.assigneeGroupNo" id="requestDetail_assigneeGroupNo" />
			<input class="form-control" name="assigneeGroupName" id="requestDetail_assigneeGroupName" class="form-control" style="width: 90%;float: left;margin-top: 10px;"
			onclick="requestDetail.selectAssginGroup('#requestAssign_win #requestDetail_assigneeGroupNo','#requestAssign_win #requestDetail_assigneeGroupName')" />
			</td>
		</tr>
		<sec:authorize url="/pages/user!find.action">
		<tr>
			<td style="width: 25%">技术员&nbsp;<span style="color:red">*</span></td>
			<td>
			<input type="hidden" name="requestDTO.assigneeNo" id="requestDetail_assigneeNo" />
			<input class="form-control" name="assigneeName" id="requestDetail_assigneeName" class="form-control" style="width: 90%;float: left;margin-top: 10px;"
			onclick="requestDetail.selectRequestUser_byOrgNo('#requestAssign_win #requestDetail_assigneeName','#requestAssign_win #requestDetail_assigneeNo')"/>
			</td>
		</tr>
		</sec:authorize>
		<tr>
			<td style="width: 25%"><fmt:message key="common.remark" /></td>
			<td>
				<textarea class="form-control" style="width:90%;height:80px;margin-top: 15px;" name="assignRemark" id="assignRemark"></textarea>
			</td>
		</tr>
		<tr>
			<td colspan="2">
				<input type="hidden" name="historyRecordDto.logTitle" id="requestAssign_action" value="<fmt:message key="label.request.requestAssign" />" />
				<input type="hidden" name="requestDTO.optType" value="requestAssgin" />
				<button class="btn btn-primary btn-sm" style="margin-right: 20px;margin-top: 10px;"><fmt:message key="title.request.assign" /></button>
				<br/>
			</td>
		</tr>
	</table>
	</form>
</div></div></div>


<!-- 请求指派二线 -->
<div id="requestAssignTwo_win" class="WSTUO-dialog" title="<fmt:message key="label.request.assignedToSecondlineTechnician" />" style="width:480px;height:auto">
	<form>
	
	<div class="lineTableBgDiv">
		<table style="width:100%" class="lineTable" cellspacing="1">
		<tr>
			<td><fmt:message key="label.request.group" />&nbsp;<span style="color:red">*</span></td>
			<td>
			<input type="hidden" name="requestDTO.assigneeGroupNo" id="requestDetail_assigneeGroupNo_two"/>
			<input class="form-control"  name="assigneeGroupName" id="requestDetail_assigneeGroupName_two" onclick="requestDetail.selectAssginGroup('#requestDetail_assigneeGroupNo_two','#requestDetail_assigneeGroupName_two')" readonly="readonly" />
			&nbsp;<a onclick="cleanIdValue('requestDetail_assigneeGroupNo_two','requestDetail_assigneeGroupName_two')" title="<fmt:message key="label.request.clear" />"><i class="glyphicon glyphicon-remove-circle form-control-feedback choose"></i></a>
			</td>
		</tr>
		<tr>
			<td><fmt:message key="label.request.technician" />&nbsp;<span style="color:red">*</span></td>
			<td>
			<input type="hidden" name="requestDTO.assigneeNo" id="requestDetail_assigneeNo_two" />
			<input class="form-control" name="assigneeName" id="requestDetail_assigneeName_two"  readonly="readonly" onclick="wstuo.user.userUtil.selectUserByRole('ROLE_SECONDLINEENGINEER','#requestDetail_assigneeName_two','#requestDetail_assigneeNo_two')" />
			&nbsp;<a onclick="cleanIdValue('requestDetail_assigneeName_two','requestDetail_assigneeNo_two')" title="<fmt:message key="label.request.clear" />"><i class="glyphicon glyphicon-remove-circle form-control-feedback choose"></i></a>
			</td>
		</tr>
		<tr>
			<td><fmt:message key="common.remark" /></td>
			<td>
				<textarea style="width: 98%;height: 80px" class="form-control" name="assignRemark" id="assignRemark_two"></textarea>
			</td>
		</tr>
<%-- 		<tr>
			<td><fmt:message key="label.is.request.action.notice" /></td>
			<td>
			<div id="requestAssignTwo_noticeEmail_div" style="display: none;">
					<input style="width: 98%;" id="requestAssignTwo_win_noticeEmail_input" ondblclick="requestDetail.selectNoticeUser('#requestAssignTwo_win_noticeEmail_input','email')"/>
					<br><span><fmt:message key="label.email.address.format" /></span>
			</div>
			<input type="checkbox" value="true" id="requestAssignTwo_win_isEmailNotice" onclick="requestDetail.requestActionNoticeSet('#requestAssignTwo_win_isEmailNotice','#requestAssignTwo_noticeEmail_div')" />
			
			</td>
		</tr> --%>
		<tr>
			<td colspan="2">
				<input type="hidden" name="historyRecordDto.logTitle" id="requestAssignTwo_win_action" value="<fmt:message key="label.request.assignedToSecondlineTechnician" />" />
				<input type="hidden" name="requestDTO.optType" value="requestAssginTwo" />
				<a class="btn btn-primary btn-sm" onclick="requestAction.requestAssign_opt('two','requestAssignTwo_win')"><fmt:message key="title.request.assign" /></a>
			</td>
		</tr>
	</table>
	</div>
	</form>
</div>


<!-- 请求指派三线 -->
<div id="requestAssignThree_win" class="WSTUO-dialog" title="<fmt:message key="label.request.assignedToThirdlineTechnician" />" style="width:480px;height:auto">
	<form>

	<div class="lineTableBgDiv">
		<table style="width:100%" class="lineTable" cellspacing="1">
		<tr>
			<td><fmt:message key="label.request.group" />&nbsp;<span style="color:red">*</span></td>
			<td>
			<input type="hidden" name="requestDTO.assigneeGroupNo" id="requestDetail_assigneeGroupNo_three" />
			<input style="width:80%" name="assigneeGroupName" id="requestDetail_assigneeGroupName_three" onclick="requestDetail.selectAssginGroup('#requestDetail_assigneeGroupNo_three','#requestDetail_assigneeGroupName_three')" readonly="readonly" />
			&nbsp;<a onclick="cleanIdValue('requestDetail_assigneeGroupNo_three','requestDetail_assigneeGroupName_three')" title="<fmt:message key="label.request.clear" />"><i class="glyphicon glyphicon-remove-circle form-control-feedback choose"></i></a>
			</td>
		</tr>
		<tr>
			<td><fmt:message key="label.request.technician" />&nbsp;<span style="color:red">*</span></td>
			<td>
			<input type="hidden" name="requestDTO.assigneeNo" id="requestDetail_assigneeNo_three" />
			<input style="width:80%" name="assigneeName" id="requestDetail_assigneeName_three"  readonly="readonly" onclick="wstuo.user.userUtil.selectUserByRole('ROLE_THIRDLINEENGINEER','#requestDetail_assigneeName_three','#requestDetail_assigneeNo_three')" />
			&nbsp;<a onclick="cleanIdValue('requestDetail_assigneeName_three','requestDetail_assigneeNo_three')" title="<fmt:message key="label.request.clear" />"><i class="glyphicon glyphicon-remove-circle form-control-feedback choose"></i></a>
			</td>
		</tr>
		<tr>
			<td><fmt:message key="common.remark" /></td>
			<td>
				<textarea style="width: 98%;height: 80px" name="assignRemark" id="assignRemark_three"></textarea>
			</td>
		</tr>
		<%-- <tr>
			<td><fmt:message key="label.is.request.action.notice" /></td>
			<td>
			<div id="requestAssignThree_noticeEmail_div" style="display: none;">
					<input style="width: 98%;" id="requestAssignThree_win_noticeEmail_input" ondblclick="requestDetail.selectNoticeUser('#requestAssignThree_win_noticeEmail_input','email')"/>
					<br><span><fmt:message key="label.email.address.format" /></span>
			</div>
			<input type="checkbox" value="true" id="requestAssignThree_win_isEmailNotice" onclick="requestDetail.requestActionNoticeSet('#requestAssignThree_win_isEmailNotice','#requestAssignThree_noticeEmail_div')" />
			
			</td>
		</tr> --%>
		<tr>
			<td colspan="2">
				<input type="hidden" name="historyRecordDto.logTitle" id="requestAssignThree_win_action" value="<fmt:message key="label.request.assignedToThirdlineTechnician" />" />
				<input type="hidden" name="requestDTO.optType" value="requestAssginThree" />
				<a class="btn btn-primary btn-sm" onclick="requestAction.requestAssign_opt('three','requestAssignThree_win')"><fmt:message key="title.request.assign" /></a>
			</td>
		</tr>
	</table>
	</div>
	</form>
</div>



<!-- 请求指派四线 -->
<div id="requestAssignFour_win" class="WSTUO-dialog" title="<fmt:message key="label.request.assignedFourthlineEngineer" />" style="width:480px;height:auto">
	<form>

	<div class="lineTableBgDiv">
		<table style="width:100%" class="lineTable" cellspacing="1">
		<tr>
			<td><fmt:message key="label.request.group" />&nbsp;<span style="color:red">*</span></td>
			<td>
			<input type="hidden" name="requestDTO.assigneeGroupNo" id="requestDetail_assigneeGroupNo_four" />
			<input style="width:80%" name="assigneeGroupName" id="requestDetail_assigneeGroupName_four" onclick="requestDetail.selectAssginGroup('#requestDetail_assigneeGroupNo_four','#requestDetail_assigneeGroupName_four')" readonly="readonly" />
			&nbsp;<a  onclick="cleanIdValue('requestDetail_assigneeGroupNo_four','requestDetail_assigneeGroupName_four')" title="<fmt:message key="label.request.clear" />"><i class="glyphicon glyphicon-remove-circle form-control-feedback choose"></i></a>
			</td>
		</tr>
		<tr>
			<td><fmt:message key="label.request.technician" />&nbsp;<span style="color:red">*</span></td>
			<td>
			<input type="hidden" name="requestDTO.assigneeNo" id="requestDetail_assigneeNo_four"  />
			<input  style="width:80%" name="assigneeName" id="requestDetail_assigneeName_four"  readonly="readonly" onclick="wstuo.user.userUtil.selectUserByRole('ROLE_FOURTHLINEENGINEER','#requestDetail_assigneeName_four','#requestDetail_assigneeNo_four')"/>
			&nbsp;<a onclick="cleanIdValue('requestDetail_assigneeName_four','requestDetail_assigneeNo_four')" title="<fmt:message key="label.request.clear" />"><i class="glyphicon glyphicon-remove-circle form-control-feedback choose"></i></a>
			</td>
		</tr>
		<tr>
			<td><fmt:message key="common.remark" /></td>
			<td>
				<textarea style="width: 98%;height: 80px" name="assignRemark" id="assignRemark_four"></textarea>
			</td>
		</tr>
		<%-- <tr>
			<td><fmt:message key="label.is.request.action.notice" /></td>
			<td>
			<div id="requestAssignFour_noticeEmail_div" style="display: none;">
					<input style="width: 98%;"  id="requestAssignFour_win_noticeEmail_input" ondblclick="requestDetail.selectNoticeUser('#requestAssignFour_win_noticeEmail_input','email')"/>
					<br><span><fmt:message key="label.email.address.format" /></span>
			</div>
			<input type="checkbox" value="true" id="requestAssignFour_win_isEmailNotice" onclick="requestDetail.requestActionNoticeSet('#requestAssignFour_win_isEmailNotice','#requestAssignFour_noticeEmail_div')" />
			
			</td>
		</tr> --%>
		<tr>
			<td colspan="2">
				<input type="hidden" name="historyRecordDto.logTitle" id="requestAssignFour_win_action" value="<fmt:message key="label.request.assignedFourthlineEngineer" />" />
				<input type="hidden" name="requestDTO.optType" value="requestAssginFour" />
				<a class="btn btn-primary btn-sm" onclick="requestAction.requestAssign_opt('four','requestAssignFour_win')"><fmt:message key="title.request.assign" /></a>
			</td>
		</tr>
	</table>
	</div>
	</form>
</div>





<!-- 请求再指派 -->
	<div class="am-modal am-modal-no-btn" tabindex="-1" id="requestAgainAssign_win">
  <div class="am-modal-dialog">
    <div class="am-modal-hd"><fmt:message key="label.request.requestAssignAgain" />
      <a href="javascript: void(0)" class="am-close am-close-spin" data-am-modal-close>&times;</a>
    </div>
    <div class="am-modal-bd" id="">
    <form event="requestAction.requestAgainAssign_opt">
		<table style="width:100%" cellspacing="1">
		<tr>
			<td><fmt:message key="label.request.technician" />&nbsp;<span style="color:red">*</span></td>
			<td>
			<input type="hidden" name="requestDTO.assigneeNo" id="requestDetail_assigneeNo_Again"  />
			<input name="requestDTO.assigneeName" id="requestDetail_assigneeName_Again" required="true" class="form-control" style="width: 90%;float: left;"  readonly="readonly" 
			onclick="wstuo.user.userUtil.againAssignSelectUser_group('#requestDetail_assigneeName_Again','#requestDetail_assigneeNo_Again','#requestDetails_assigneeLoginName','#requestDetails_assigneeGroupNo')" />
			<h4 style="float: left;padding-left: 8px;"><a onclick="cleanIdValue('requestDetail_assigneeName_Again','requestDetail_assigneeNo_Again')" title="<fmt:message key="label.request.clear" />">
			<i class=" glyphicon glyphicon-trash"></i></a></h4>
			</td>
		</tr>
		<tr>
			<td><fmt:message key="common.remark" /></td>
			<td>
				<textarea rows="8" style="width: 90%;padding-top: 10px;"  name="assignRemark" id="assignRemark_Again" class="form-control" ></textarea>
			</td>
		</tr>
		<%-- <tr>
			<td><fmt:message key="label.is.request.action.notice" /></td>
			<td>
			<div id="requestAgainAssign_noticeEmail_div" style="display: none;">
					<input style="width: 98%;" id="requestAgainAssign_noticeEmail_input" ondblclick="requestDetail.selectNoticeUser('#requestAgainAssign_noticeEmail_input','email')"/>
					<br><span><fmt:message key="label.email.address.format" /></span>
			</div>
			<input type="checkbox" value="true" id="requestAgainAssign_isEmailNotice" onclick="requestDetail.requestActionNoticeSet('#requestAgainAssign_isEmailNotice','#requestAgainAssign_noticeEmail_div')" />
			
			</td>
		</tr> --%>
		<tr>
			<td colspan="2">
				<input type="hidden" name="historyRecordDto.logTitle" id="requestAssignAgain_action" value="<fmt:message key="label.request.requestAssignAgain" />" />
				<input type="hidden" name="requestDTO.optType" value="requestAgainAssgin" />
				<button class="btn btn-primary btn-sm" style="margin-top: 10px;margin-right: 20px;"><fmt:message key="title.request.assign" /></button>
			</td>
		</tr>
	</table>
	</form>
</div></div></div>




<!-- 请求退回-->
<div id="requestBack_win" class="WSTUO-dialog" title="<fmt:message key="label.request.backToFirstLine" />" style="width:480px;height:auto">
	<form id="requestBackForm">
		<div class="lineTableBgDiv">
		<table style="width:100%" class="lineTable" cellspacing="1">
		<tr>
			<td><fmt:message key="common.remark" /></td>
			<td>
				<textarea style="width: 98%;height: 80px" name="historyRecordDto.logDetails" required="true" id="requestBackReason_value" class="form-control"  minlength='1' maxlength='200'></textarea>
			</td>
		</tr>
		<%-- <tr>
			<td><fmt:message key="label.is.request.action.notice" /></td>
			<td>
			<div id="requestBack_noticeEmail_div" style="display: none;">
					<input style="width: 98%;" id="requestBack_noticeEmail_input" ondblclick="requestDetail.selectNoticeUser('#requestBack_noticeEmail_input','email')"/>
					<br><span><fmt:message key="label.email.address.format" /></span>
			</div>
			<input type="checkbox" value="true" id="requestBack_isEmailNotice" onclick="requestDetail.requestActionNoticeSet('#requestBack_isEmailNotice','#requestBack_noticeEmail_div')" />
			
			</td>
		</tr> --%>
		<tr>
			<td colspan="2">
				<input type="hidden" name="historyRecordDto.logTitle" id="backToFirstLine_action" value="<fmt:message key="label.request.backToFirstLine" />" />
				<input type="hidden" name="requestDTO.optType" value="requestBack" />
				<input type="button" class="btn btn-primary btn-sm" onclick="requestAction.requestBack_opt()" value="<fmt:message key="label.request.back" />"/>
			</td>
		</tr>
		
		
	</table>
	</div>
	</form>
</div>

<!-- 请求升级申请-->
<div id="requestUpgradeApply_win" class="WSTUO-dialog" title="<fmt:message key="label.request.upgradeApplication" />" style="width:480px;height:auto">
	<form  event="requestAction.requestUpgradeApply_opt">
	
	<div class="lineTableBgDiv">
		<table style="width:100%" class="lineTable" cellspacing="1">
		<tr>
			<td><fmt:message key="common.remark" /></span></td>
			<td>
				<textarea style="width: 90%" rows="6" cols="45" name="historyRecordDto.logDetails" id="requestUpgradeApplyReason_value" class="form-control" minlength='1' maxlength='200'></textarea>
			</td>
		</tr>
		<%-- <tr>
			<td><fmt:message key="label.is.request.action.notice" /></td>
			<td>
			<div id="requestUpgradeApply_noticeEmail_div" style="display: none;">
					<input style="width: 98%;"  id="requestUpgradeApply_noticeEmail_input" ondblclick="requestDetail.selectNoticeUser('#requestUpgradeApply_noticeEmail_input','email')"/>
					<br><span><fmt:message key="label.email.address.format" /></span>
			</div>
			<input type="checkbox" value="true" id="requestUpgradeApply_isEmailNotice" onclick="requestDetail.requestActionNoticeSet('#requestUpgradeApply_isEmailNotice','#requestUpgradeApply_noticeEmail_div')" />
			
			</td>
		</tr> --%>
		<tr>
			<td colspan="2">
				<input type="hidden" name="historyRecordDto.logTitle" id="upgradeApplication_action" value="<fmt:message key="label.request.upgradeApplication" />" />
				<input type="hidden" name="requestDTO.optType" value="requestUpgradeApply" />
				<button class="btn btn-primary btn-sm"><fmt:message key="label.request.apply" /></button>
			</td>
		</tr>
	</table>
	</div>
	</form>
</div>


<!-- 请求升级-->
<div id="requestUpgrade_win" class="WSTUO-dialog" title="<fmt:message key="label.request.requestUpdate" />" style="width:480px;height:auto">
	<form event="requestAction.requestUpgrade_opt">
	
	<div class="lineTableBgDiv">
		<table style="width:100%" class="lineTable" cellspacing="1">
		<tr>
			<td width="30%"><fmt:message key="label.request.requestUpdateTo" />&nbsp;<span style="color:red">*</span></td>
			<td>
				<select style="width: 98%" name="requestDTO.ownerNo" id="requestUpgrade_level" ></select>
			</td>
		</tr>
		<tr>
			<td><fmt:message key="common.remark" /></td>
			<td>
				<textarea rows="8" style="width:96%" id="requestUpgradeReason" class="form-control"  minlength='1' maxlength='200'></textarea>
			</td>
		</tr>
		<tr>
			<td colspan="2">
				<input type="hidden" name="historyRecordDto.logTitle" id="requestUpdate_action" value="<fmt:message key="label.request.requestUpdate" />" />
				<input type="hidden" name="requestDTO.optType" value="requestUpgrade" />
				<button class="btn btn-primary btn-sm" ><fmt:message key="label.request.update" /></button>
			</td>
		</tr>
	</table>
	</div>
	</form>
</div>

<!-- 请求关闭-->
<div id="requestClose_win" class="WSTUO-dialog" title="<fmt:message key="label.request.requestClose" />" style="width:480px;height:auto">
	<form>
	
	<div class="lineTableBgDiv">
		<table style="width:100%" class="lineTable" cellspacing="1">


		<tr>
			<td><fmt:message key="common.remark" /></td>
			<td>
				<textarea style="width: 98%;height: 80px" name="historyRecordDto.logDetails" id="requestClose_value" class="form-control"  minlength='1' maxlength='200' required="true"></textarea>
			</td>
		</tr>
		<tr>
			<td colspan="2">
				<input name="requestDTO.closeTime" id="requestCloseTime" type="hidden" />
				<input type="hidden" name="historyRecordDto.logTitle" id="requestClose_action" value="<fmt:message key="label.request.requestClose" />" />
				<input type="hidden" name="requestDTO.optType" value="requestClose" />
				<a class="btn btn-primary btn-sm" onclick="requestAction.requestClose_opt()"><fmt:message key="label.request.close" /></a>
			</td>
		</tr>
	</table>
	</div>
	</form>
</div>
<!-- 请求处理备注-->
<div class="am-modal am-modal-no-btn" tabindex="-1" id="requestDealRemark_win">
  <div class="am-modal-dialog">
    <div class="am-modal-hd"><fmt:message key="label.request.requestProcessMrak" />
      <a href="javascript: void(0)" class="am-close am-close-spin" data-am-modal-close>&times;</a>
    </div>
    <div class="am-modal-bd" id="">
	<form>
		<div class="lineTableBgDiv">
		<table style="width:100%" cellspacing="1">
		<tr style="display:none;">
			<td style="width: 15%;"><fmt:message key="label.request.requestState" /></td>
			<td>
				<select style="width:96%" name="requestDTO.remarkStatusNo" id="requestDealRemark_statusNo"></select>
			</td>
		</tr>
		<tr>
			<td style="width: 15%;"><fmt:message key="common.remark" />&nbsp;<span style="color:red">*</span></td>
			<td>
				<textarea style="width: 98%;height: 80px" name="historyRecordDto.logDetails" id="requestDealRemark_value" class="form-control"  minlength='1' maxlength='200' required="true"></textarea>
			</td>
		</tr>
		<tr>
			<td colspan="2">
				<input type="hidden" name="historyRecordDto.logTitle" id="requestProcessMrak_action" value="<fmt:message key="label.request.requestProcessMrak" />" />
				<input type="hidden" name="requestDTO.optType" value="requestDealRemark" />
				<input type="button" style="margin-top: 10px;" class="btn btn-primary btn-sm" onclick="requestAction.requestDealRemark_opt()" value="<fmt:message key="label.request.add" />">
			</td>
		</tr>
	</table>
	</div>
	</form>
</div></div></div>

<!-- 请求处理完成-->
<div id="requestDealComplete_win" class="WSTUO-dialog" title="<fmt:message key="label.request.processComplete" />" style="width:480px;height:auto">
	<form>
	
	<div class="lineTableBgDiv">
		<table style="width:100%" class="lineTable" cellspacing="1">
		<tr>
			<td style="width:30%"><fmt:message key="common.remark" /></td>
			<td style="width:70%">
				<textarea style="width: 98%;height: 80px" name="historyRecordDto.logDetails" id="requestDealComplete_value" class="form-control"  minlength='1' maxlength='200'></textarea>
			</td>
		</tr>
<%-- 		<tr>
			<td><fmt:message key="label.is.request.action.notice" /></td>
			<td>
			<div id="requestDealComplete_noticeEmail_div" style="display: none;">
					<input style="width: 98%;" id="requestDealComplete_noticeEmail_input" ondblclick="requestDetail.selectNoticeUser('#requestDealComplete_noticeEmail_input','email')"/>
					<br><span><fmt:message key="label.email.address.format" /></span>
			</div>
			<input type="checkbox" value="true" id="requestDealComplete_isEmailNotice" onclick="requestDetail.requestActionNoticeSet('#requestDealComplete_isEmailNotice','#requestDealComplete_noticeEmail_div')" />
			
			</td>
		</tr> --%>
		<tr>
			<td colspan="2">
				<input type="hidden" name="historyRecordDto.logTitle" id="processComplete_action" value="<fmt:message key="label.request.processComplete" />" />
				<input type="hidden" name="requestDTO.optType" value="requestDealComplete" />
				<a class="btn btn-primary btn-sm" onclick="requestAction.requestDealComplete_opt()"><fmt:message key="common.submit" /></a>
			</td>
		</tr>
	</table>
	</div>
	</form>
</div>


<!-- 请求审批  -->
<div id="requestApproval_win" class="WSTUO-dialog" title="<fmt:message key="label.request.requestApproval" /> " style="width:480px;height:auto">
	<form>
	<div class="lineTableBgDiv">
		<table style="width:100%" class="lineTable" cellspacing="1">
		<tr>
			<td style="width:30%"><fmt:message key="common.remark" /></td>
			<td style="width:70%">
				<textarea style="width: 98%;height: 80px" name="historyRecordDto.logDetails" id="requestApprovalRemark" class="form-control"  minlength='1' maxlength='200' ></textarea>
			</td>
		</tr>
		<%-- <tr>
			<td><fmt:message key="label.is.request.action.notice" /></td>
			<td>
			<div id="requestApproval_noticeEmail_div" style="display: none;">
					<input style="width: 98%;" id="requestApproval_noticeEmail_input" ondblclick="requestDetail.selectNoticeUser('#requestApproval_noticeEmail_input','email')"/>
					<br><span><fmt:message key="label.email.address.format" /></span>
			</div>
			<input type="checkbox" value="true" id="requestApproval_isEmailNotice" onclick="requestDetail.requestActionNoticeSet('#requestApproval_isEmailNotice','#requestApproval_noticeEmail_div')" />
			
			</td>
		</tr> --%>
		<tr>
			<td colspan="2">
				<input type="hidden" name="historyRecordDto.logTitle" id="requestApproval_action" value="<fmt:message key="label.request.requestApproval" /> " />
				<input type="hidden" name="requestDTO.optType" value="requestApproval" />
				<input type="hidden" name="requestDTO.taskId" />
				<a class="btn btn-primary btn-sm" onclick="requestAction.requestApproval_opt('同意')"><fmt:message key="label.request.requestApprovalAgree" /></a>
				<a  class="btn btn-primary btn-sm" onclick="requestAction.requestApproval_opt('不同意')"><fmt:message key="label.request.requestApprovalDisagree" /></a>
			</td>
		</tr>
		</table>
	</div>
	</form>
</div>

<!-- 请求审批(NEW)  -->
<div id="requestApprovalResult_win" class="WSTUO-dialog" title="<fmt:message key="label.request.requestApproval" />" style="width:480px;height:auto">
	<form>
	<div class="lineTableBgDiv">
		<table style="width:100%" class="lineTable" cellspacing="1">
		<tr>
			<td style="width:30%"></td>
			<td style="width:70%">
				<textarea style="width: 98%;height: 80px" name="historyRecordDto.logDetails"></textarea>
			</td>
		</tr>
		
		<tr>
			<td colspan="2">
				<input type="hidden" name="historyRecordDto.logTitle" id="requestApprovalResult_logTitle" />
				<input type="hidden" name="requestDTO.optType" value="requestApprovalResult" />
				<a class="btn btn-primary btn-sm" onclick="requestAction.requestApprovalResult_opt('true')"><fmt:message key="label.request.requestApprovalAgree" /></a>
				<a  class="btn btn-primary btn-sm" onclick="requestAction.requestApprovalResult_opt('false')"><fmt:message key="label.request.requestApprovalDisagree" /></a>
			</td>
		</tr>
		</table>
	</div>
	</form>
</div>




<!-- 请求回访-->
<div id="requestVisit_win" class="WSTUO-dialog" title="<fmt:message key="label.request.requestVisit"/>" style="width:500px;height:auto">
<form>
	<input type="hidden" name="historyRecordDto.logTitle" value="<fmt:message key="label.request.requestVisit" />" />
	<input type="hidden" name="requestDTO.optType" id="requestVisit_optType" value="requestVisit" />
	<input type="hidden" name="requestDTO.visitRecord" id="request_visitRecord"/>
	<input type="hidden" name="historyRecordDto.logDetails" id="requestVisit_remark">
	<div class="lineTableBgDiv">
		<table style="width:100%" id="requestVisit_table" class="lineTable" cellspacing="1">
			<tr>
				<td width="35%"><fmt:message key="label.returnVisit.object" /></td>
				<td width="35%"><input class="input" name="requestDTO.createdByName" id="requestVisit_createdByName_input"  type="hidden"/>
					<span id="requestVisit_createdByName_span"></span>
				</td>
				<td style="text-align: center;">
					<input type="button" class="btn btn-primary btn-sm" onclick="requestAction.requestEmailVisit_opt()" value="<fmt:message key="common.submit" />">
				</td>
			</tr>
		</table>
	</div>
</form>
</div>


<!-- 自助解决 -->
<div class="am-modal am-modal-no-btn" tabindex="-1" id="self_help_resolve_win">
  <div class="am-modal-dialog">
    <div class="am-modal-hd"><fmt:message key="label.request.self.help.resolve" />
      <a href="javascript: void(0)" class="am-close am-close-spin" data-am-modal-close>&times;</a>
    </div>
    <div class="am-modal-bd" id="">
<form event="requestAction.self_help_resolve_opt">
	<input type="hidden" name="historyRecordDto.logTitle" value="<fmt:message key="label.request.self.help.resolve" />" />
	<input type="hidden" name="requestDTO.optType" value="requestDirectClose" />
	<div class="lineTableBgDiv">
	<table style="width:100%" class="lineTable" cellspacing="1">
		<tr>
			<td><fmt:message key="title.request.solutions" /></td>
			<td>
				<textarea style="width: 98%;height: 100px" name="requestDTO.solutions" class="form-control" required="true" id="requestSelfHelpLogDetails_solutions"></textarea>
			</td>
		</tr>		
		<tr>
			<td><fmt:message key="common.remark" /></td>
			<td><br/>
				<textarea style="width: 98%;height: 80px" id="requestSelfHelpLogDetails"  class="form-control"></textarea>
				<input type="hidden" name="historyRecordDto.logDetails" id="requestSelftHelpHiddenLog"/>
			</td>
		</tr>
		<tr><td colspan="2">
			<input type="submit" class="btn btn-primary btn-sm" style="margin-right: 15px;" value="<fmt:message key="common.submit" />"/>
		</td></tr>
	</table>
	</div>
</form>
</div></div></div>


<!-- 请求挂起 -->
<div class="am-modal am-modal-no-btn" tabindex="-1" id="requestHang_win">
  <div class="am-modal-dialog">
    <div class="am-modal-hd"><fmt:message key="request.action.hang" />
      <a href="javascript: void(0)" class="am-close am-close-spin" data-am-modal-close>&times;</a>
    </div>
    <div class="am-modal-bd" id="">
<form>
	<input type="hidden" name="historyRecordDto.logTitle" id="requestHang_action" value="<fmt:message key="request.action.hang" />" />
	<input type="hidden" name="requestDTO.optType" value="requestHangRemove" />
	<input type="hidden" name="requestDTO.hang" value="true" />
	<div class="lineTableBgDiv">
	<table style="width:100%" class="lineTable" cellspacing="1">
		<tr>
			<td><fmt:message key="common.remark" /></td>
			<td>
				<textarea style="width: 98%;height: 80px" name="historyRecordDto.logDetails" class="form-control" ></textarea>
			</td>
		</tr>
		<tr><td colspan="2">
			<input type="button" style="margin-top: 10px;" class="btn btn-primary btn-sm" onclick="requestAction.requestHang_opt()" value="<fmt:message key="common.submit" />"/>
		</td></tr>
	</table>
	</div>
</form>
</div></div></div>

<!-- 解除请求挂起 -->
<div class="am-modal am-modal-no-btn" tabindex="-1" id="requestHangRemove_win">
  <div class="am-modal-dialog">
    <div class="am-modal-hd"><fmt:message key="request.action.hangRemove" />
      <a href="javascript: void(0)" class="am-close am-close-spin" data-am-modal-close>&times;</a>
    </div>
    <div class="am-modal-bd" id="">
<form>
	<input type="hidden" name="historyRecordDto.logTitle" value="<fmt:message key="request.action.hangRemove" />" />
	<input type="hidden" name="requestDTO.optType" value="requestHangRemove" />
	<input type="hidden" name="requestDTO.hang" value="false" />
	<div class="lineTableBgDiv">
	<table style="width:100%" class="lineTable" cellspacing="1">
		<tr>
			<td><fmt:message key="common.remark" /></td>
			<td>
				<textarea style="width: 98%;height: 80px" name="historyRecordDto.logDetails" class="form-control"></textarea>
			</td>
		</tr>
		<tr><td colspan="2">
			<input type="button" class="btn btn-primary btn-sm" onclick="requestAction.requestHangRemove_opt()" value="<fmt:message key="common.submit" />"/>
		</td></tr>
	</table>
	</div>
</form>
</div></div></div>


<!-- 修改SLA时间 -->
<div id="editRequestSlaTime_win" class="WSTUO-dialog" title="<fmt:message key="request.action.editSlaTime" />" style="width:500px;height:auto">
<form>
	<input type="hidden" name="historyRecordDto.logTitle" value="<fmt:message key="request.action.editSlaTime" />" />
	<input type="hidden" name="requestDTO.optType" value="editRequestSlaTime" />
	<div class="lineTableBgDiv">
	<table style="width:100%" class="lineTable" cellspacing="1">
	
		<tr>
			<td><fmt:message key="label.request.slaReuqestTime" /></td>
			<td>
				<input type="hidden" id="requestActionHtml_responseTime_old" />
				<input name="requestDTO.requestResponseTime" id="requestActionHtml_responseTime"  class="form-control" required="true"  />
			</td>
		</tr>
		
		<tr>
			<td><fmt:message key="label.request.slaCompleteTime" /></td>
			<td>
				<input type="hidden" id="requestActionHtml_completeTime_old"  />
				<input name="requestDTO.requestCompleteTime" id="requestActionHtml_completeTime" class="form-control" required="true" validType="DateComparison['requestActionHtml_responseTime']" />
			</td>
		</tr>
		
		<tr>
			<td><fmt:message key="common.remark" /></td>
			<td>
				<textarea style="width: 98%;height: 80px" id="editRequestSlaTime_remark" class="form-control"></textarea>
			</td>
		</tr>
		<tr><td colspan="2">
			<input type="button" class="btn btn-primary btn-sm" onclick="requestAction.editRequestSlaTime_opt()" value="<fmt:message key="common.submit" />"/>
		</td></tr>
	</table>
	</div>
</form>
</div>


<!-- 请求流程重新开启 -->
<div class="am-modal am-modal-no-btn" tabindex="-1" id="request_reopen_win">
  <div class="am-modal-dialog">
    <div class="am-modal-hd"><fmt:message key="label.request.reStartRequest" />
      <a href="javascript: void(0)" class="am-close am-close-spin" data-am-modal-close>&times;</a>
    </div>
    <div class="am-modal-bd" id="">
    <form event="requestAction.reopen_opt">
	<input type="hidden" name="historyRecordDto.logTitle" value="<fmt:message key="label.flow.re.open" />" />
	<input type="hidden" name="requestDTO.optType" value="reopen" />
	<div class="lineTableBgDiv">
	<table style="width:100%" cellspacing="1">
		<tr>
			<td><fmt:message key="common.remark" /></td>
			<td>
				<textarea style="width: 98%;height: 80px" id="requestReopenLogDetails" class="form-control" required="true"  minlength='1' maxlength='200' ></textarea>
				<input name="historyRecordDto.logDetails" type="hidden" id="requestReopenLogDetailshidden"/>
			</td>
		</tr>
		<tr><td colspan="2">
			<button class="btn btn-primary btn-sm" style="margin-top: 10px;"><fmt:message key="common.submit" /></button>
		</td></tr>
	</table>
	</div>
</form>
</div></div></div>

<!-- 请求关闭审核 -->
<div id="request_close_audit_win" class="WSTUO-dialog" title='<fmt:message key="label.requestAction.closeApprove" />' style="width:500px;height:auto">
<form>
	<input type="hidden" name="historyRecordDto.logTitle" value='<fmt:message key="label.requestAction.closeApprove" />' />
	<input type="hidden" name="requestDTO.optType" value="closeAudit" />
	<div class="lineTableBgDiv">
	<table style="width:100%" class="lineTable" cellspacing="1">
		<tr>
			<td><fmt:message key="common.remark" /></td>
			<td>
				<textarea style="width: 98%;height: 80px" name="historyRecordDto.logDetails" class="form-control" required="true" ></textarea>
			</td>
		</tr>
		<tr><td colspan="2">
			<a class="btn btn-primary btn-sm" onclick="requestAction.closeAudit_opt('true')" ><fmt:message key="label_knowledge_approvalPass" /> </a>
			<a  class="btn btn-primary btn-sm" onclick="requestAction.closeAudit_opt('false')" ><fmt:message key="label_knowledge_approvalRefuse" /></a>
		</td></tr>
	</table>
	</div>
</form>
</div>
<div class="am-modal am-modal-no-btn" tabindex="-1" id="request_notComprehensiveNotSubmitted_win">
  <div class="am-modal-dialog">
    <div class="am-modal-hd"><fmt:message key="label.notComprehensiveNotSubmitted" />
      <a href="javascript: void(0)" class="am-close am-close-spin" data-am-modal-close>&times;</a>
    </div>
    <div class="am-modal-bd" id="">
    	<form>
	<input type="hidden" name="historyRecordDto.logTitle" value='<fmt:message key="label.notComprehensiveNotSubmitted" />'/>
	<input type="hidden" name="requestDTO.optType" value="notComprehensiveNotSubmitted" />
	<div class="lineTableBgDiv">
	<table style="width:100%" class="lineTable" cellspacing="1">
		<tr>
			<td><fmt:message key="common.remark" /></td>
			<td>
				<textarea style="width: 98%;height: 80px" name="historyRecordDto.logDetails" class="form-control" required="true"  minlength='1' maxlength='200'></textarea>
			</td>
		</tr>
		<tr><td colspan="2">
			<button type="button" class="btn btn-primary btn-sm" type="button" onclick="requestAction.notComprehensiveNotSubmitted_opt()" ><fmt:message key="common.submit" /></button>
		</td></tr>
	</table>
	</div>
</form>
    </div>
  </div>
</div>


<!--请求描述完全,重新提交 -->
<div class="am-modal am-modal-no-btn" tabindex="-1" id="request_resubmit_win">
  <div class="am-modal-dialog">
    <div class="am-modal-hd"><fmt:message key="label.desc.complete.resubmit" />
      <a href="javascript: void(0)" class="am-close am-close-spin" data-am-modal-close>&times;</a>
    </div>
    <div class="am-modal-bd" id="">
	<form>
		<input type="hidden" name="historyRecordDto.logTitle" value="<fmt:message key="label.desc.complete.resubmit" />" />
		<input type="hidden" name="requestDTO.optType" value="reSubmit" />
		<div class="lineTableBgDiv">
		<table style="width:100%" class="lineTable" cellspacing="1">
			<tr>
				<td><fmt:message key="common.remark" /></td>
				<td>
					<textarea style="width: 98%;height: 80px" name="historyRecordDto.logDetails" class="form-control" required="true"  minlength='1' maxlength='200'></textarea>
				</td>
			</tr>
			<tr><td colspan="2">
				<button type="button" class="btn btn-primary btn-sm" onclick="requestAction.resubmit_opt()" ><fmt:message key="common.submit" /></button>
			</td></tr>
		</table>
		</div>
	</form>
</div>
</div></div>
<!--流程处理 -->
<div id="request_process_handle_win" class="WSTUO-dialog" title="<fmt:message key="label.desc.complete.resubmit" />" style="width:500px;height:auto">
<form>
	<input type="hidden" name="historyRecordDto.logTitle" id="request_logTitle" />
	<input type="hidden" name="requestDTO.optType" value="processHandle" />
	<div class="lineTableBgDiv">
	<table style="width:100%" class="lineTable" cellspacing="1">
		<tr>
			<td><fmt:message key="common.remark" /></td>
			<td>
				<textarea style="width: 98%;height: 80px" name="historyRecordDto.logDetails" class="form-control" required="true" ></textarea>
			</td>
		</tr>
		<tr><td colspan="2">
			<a class="btn btn-primary btn-sm" onclick="requestAction.processHandle_opt()" ><fmt:message key="common.submit" /></a>
		</td></tr>
	</table>
	</div>
</form>
</div>


	<div id="processHandleDiv" class="WSTUO-dialog" shadow="false" closed="true" modal="false"  title="流程处理" minimizable="false" maximizable="false" collapsible="false" style="width:480px;height:auto" >
		<form id="processHandle_form">
			<div class="lineTableBgDiv">
				<table style="width:100%" class="lineTable" cellspacing="1">
				<tr>
					<td><fmt:message key="label.request.startRequestDealMark" />&nbsp;</td>
					<td>
						<textarea style="width: 98%;height: 80px" name="historyRecordDto.logDetails" class="form-control"  minlength='1' maxlength='200' id="requestDeal_value"></textarea>
					</td>
				</tr>
	
				<tr>
					<td colspan="2">
						<input type="hidden" name="historyRecordDto.logTitle" id="processHandle_action" value="" />
						<input type="hidden" name="requestDTO.optType" id="processHandle_optType" value="flowHandle" />
						<input type="hidden" name="requestDTO.taskId" id="processHandle_taskId" value="" />
						<input type="hidden" name="requestDTO.assigneeName" id="processHandle_assigneeName" value="" />
						<a class="btn btn-primary btn-sm" onclick="requestAction.processHandle()"><fmt:message key="label.request.startProcess" /></a>
					</td>
				</tr>
				</table>
			</div>
		</form>
	</div>
	
	<!-- 审批加签 -->
<div id="request_openGetUser" class="WSTUO-dialog" title="<fmt:message key="lable.request.appor.user" />">
	<form id="request_openGetUser_from">
	<table style="width:100%" class="lineTable" cellspacing="1">
		<tr>
			<td style="width: 20%; height: 100px;"><fmt:message key="common.remark" />:</td>
			<td>
				<textarea id="appro_User" style="width: 98%; height: 100px;" class="form-control"  required="true"  ></textarea>
			</td>
		</tr>
		<tr>
			<td><fmt:message key="lable.request.approveUser" /> :</td>
			<td>
				<input type="hidden" id="requestDetail_assigneeNo_Appro">
				<input type="text" style="width: 85%" id="appro_User_Name" onclick="wstuo.user.userUtil.selectUser('#appro_User_Name','#requestDetail_assigneeNo_Appro','','fullName',${topCompanyNo})" class="form-control" required="true">&nbsp;&nbsp;
				<a onclick="cleanIdValue('appro_User_Name','requestDetail_assigneeNo_Appro')" title="<fmt:message key="label.request.clear" />"><i class="glyphicon glyphicon-remove-circle form-control-feedback choose"></i></a>
			</td>
		</tr>
		<tr>
			<td colspan="2"><a class="btn btn-primary btn-sm" onclick="requestDetail.send_Email_appro()"><fmt:message key="common.submit" /></a></td>
		</tr>
	</table></form>
</div>



