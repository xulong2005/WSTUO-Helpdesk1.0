<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%
String url="login.jsp";

 if(session.getAttribute("loginUserName")==null){
	response.sendRedirect(url);
	return;
} 
 %>
<!DOCTYPE html>
<html>
<head lang="en">
	<meta charset="UTF-8">
	<title>WSTUO 企业一体化解决方案</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="format-detection" content="telephone=no">
	<meta name="format-detlogin.htmlection" content="telephone=no">
	<meta name="renderer" content="webkit">
	<meta http-equiv="Cache-Control" content="no-siteapp" />
	<meta name="description" content="wstuo,企业一体化解决方案">
	<meta name="author" content="WSTUO">
	<link rel="icon" type="image/png" href="../images/logo2.png">
	<link rel="stylesheet" href="assets/css/amazeui.min.css"/>
	<link rel="stylesheet" href="assets/css/app.css">
	<script src="assets/js/jquery.min.js"></script>
	<script src="assets/js/amazeui.min.js"></script>
</head>
<body>
	<header data-am-widget="header" class="am-header am-header-default">
	    <h1 class="am-header-title">个人信息 </h1>  
	</header>
	<input type="hidden" value="${knowledgeDto.kid}" id="knowledge_kid" >
	<!-- Menu -->
	<nav data-am-widget="menu" class="am-menu am-menu-offcanvas1" data-am-menu-offcanvas>
		<a href="javascript:history.go(-1);" class="am-menu-left" >
	    	<i style="text-align: right;padding: 0px;" class="am-menu-toggle-icon am-icon-chevron-left"></i>
	  	</a>
  		<a href="javascript: void(0)" class="am-menu-toggle">
	    	<i style="text-align: right;padding: 0px;" class="am-menu-toggle-icon am-icon-bars"></i>
	  	</a>
	  	<div class="am-offcanvas">
		    <div class="am-offcanvas-bar">
		    	<ul class="am-menu-nav sm-block-grid-1">      
			        <li>
			          <a href="user!findUserDetail_wap.action?userName=${sessionScope.loginUserName}">个人信息</a>        
			        </li>
			        <li >
			          <a href="pwdUpdate.jsp">密码修改</a>       
			        </li>  
			        <li>
			          <a href="index.jsp">返回首页</a>       
			        </li>   
			        <li>
			          <a href="logout.jsp">注&nbsp;&nbsp;销</a>       
			        </li>
			      </ul>
		    </div>
	  	</div>
	</nav>

	<div data-am-widget="list_news" class="am-list-news am-list-news-default am-no-layout" >	
		<div class="am-list-news-bd">
			<ul class="am-list">
			<li class="am-g am-list-item-dated">
				<a class="am-list-item-hd " style="color: black;" style="color: black;"> <b>登录名：</b>${userDto.loginName}</a>
			</li>
			<li class="am-g am-list-item-dated">
				<a class="am-list-item-hd " style="color: black;"> <b>姓&nbsp;&nbsp;名：</b>${userDto.firstName}${userDto.lastName}</a>
			</li>
			<li class="am-g am-list-item-dated">
				<a class="am-list-item-hd " style="color: black;"> <b>所属机构：</b>${userDto.orgName}</a>
			</li>
			<li class="am-g am-list-item-dated">
				<a class="am-list-item-hd " style="color: black;"> <b>所属技术组：</b>${userDto.belongsGroup}</a>
			</li>
			<li class="am-g am-list-item-dated">
				<a class="am-list-item-hd " style="color: black;"> <b>电&nbsp;&nbsp;话：</b>${userDto.phone}</a>
			</li>
			<li class="am-g am-list-item-dated">
				<a class="am-list-item-hd " style="color: black;"> <b>手&nbsp;&nbsp;机：</b>${userDto.moblie}</a>
			</li>
			<li class="am-g am-list-item-dated">
				<a class="am-list-item-hd " style="color: black;"> <b>邮&nbsp;&nbsp;箱：</b>${userDto.email}</a>
			</li>
			<li class="am-g am-list-item-dated">
				<a class="am-list-item-hd " style="color: black;"> <b>状&nbsp;&nbsp;态：</b>
				<c:if test="${userDto.userState =='false'}">
					禁用
				</c:if>
				<c:if test="${userDto.userState =='true'}">
					启用
				</c:if>
				</a>
			</li>
			<li class="am-g am-list-item-dated">
				<a class="am-list-item-hd " style="color: black;"> <b>休假状态：</b>
				<c:if test="${userDto.holidayStatus =='false'}">
					值班中
				</c:if>
				<c:if test="${userDto.holidayStatus =='true'}">
					休假中
				</c:if>
				</a>
			</li>
			</ul>
		</div>
	</div>
	
</body>
</html>
