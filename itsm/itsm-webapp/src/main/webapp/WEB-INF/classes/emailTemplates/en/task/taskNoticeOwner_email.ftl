<style>
    .table_border{width:100%}.table_border{ border-collapse:collapse; border:solid
    black; border-width:1px 0 0 1px;}.table_border caption {font-size:14px;font-weight:bolder;}.table_border
    th,.table_border td {border:solid black;border-width:0 1px 1px 0;padding-left:5px;}
</style>
<table width="550">
    <#escape x as x!>
        <tr>
            <td colspan="2">
                Hello:
                </b>
                </font>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                You have a new task, Please Check.
                <br/>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                Title：${variables.etitle}
                <br/>
                Content：${variables.edesc}
                <br/>
                Creator：${variables.createdByName}
		<br/>
                planTime：${variables.planTime}
                <br/>
                Time: ${variables.createdOn?string("yyyy-MM-dd HH:MM:ss")}
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <br>
                Thank you very much!
            </td>
        </tr>
        <tr>
            <td colspan="2">
                Wstuo Team
            </td>
        </tr>
    </#escape>
</table>
<br>
-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~
<br>
This is an automatic email by the system, please do NOT reply!
<br>
-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~