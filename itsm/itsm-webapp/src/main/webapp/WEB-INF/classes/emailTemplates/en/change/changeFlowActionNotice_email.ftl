<style>
    .table_border{width:100%}.table_border{ border-collapse:collapse; border:solid
    black; border-width:1px 0 0 1px;}.table_border caption {font-size:14px;font-weight:bolder;}.table_border
    th,.table_border td {border:solid black;border-width:0 1px 1px 0;padding-left:5px;}
</style>
<table width="550">
    <#escape x as x!>
        <tr>
            <td colspan="2">
                <#if (userName??)>
                    ${userName}，
                </#if>
                Hello:
                </b>
                </font>
            </td>
        </tr>
        <tr>
            <td colspan="2">
		Content: change process task assignment notification;
		<#if variables.activityName?exists && variables.outcome?exists>
		${variables.createdByName}To[Change][${variables.etitle}]perform actions :
                    （ ${variables.activityName} ->${variables.outcome}）
                </#if>
            </td>
        </tr>
	<tr>
            <td colspan="2">
                Change No：${variables.ecode}
            </td>
        </tr>
        <tr>
            <td colspan="2">
                Title：${variables.etitle}
            </td>
        </tr>
	<tr>
            <td colspan="2">
		<br>Change [${variables.ecode}] Related Configuration Item
		<#if (variables.ciInfos?exists && variables.ciInfos?size>0) >
                    <br>
                    <table class="table_border">
                        <tr>
                            <td>
                                Asset No
                            </td>
                            <td>
                                Asset Name
                            </td>
                            <td>
                                CI Category
                            </td>
                            <td>
                                Status
                            </td>
                        </tr>
                        <#list variables.ciInfos as ciDTO>
                            <tr>
                                <td>
                                    ${ciDTO.cino}
                                </td>
                                <td>
                                    ${ciDTO.ciname}
                                </td>
                                <td>
                                    ${ciDTO.categoryName}
                                </td>
                                <td>
                                    ${ciDTO.status}
                                </td>
                            </tr>
                        </#list>
                    </table>
                </#if>
	    </td>
        </tr>
        <tr>
            <td colspan="2">
                 Change [${variables.ecode}] HistoryRecord:
		<#if (variables.historyRecordDTO?exists && variables.historyRecordDTO?size>0) >
                <br><table class="table_border">
                        <tr>
                            <td>
                                Action
                            </td>
                            <td>
                                Details
                            </td>
                            <td>
                                Operator
                            </td>
                            <td>
                                Operation Time
                            </td>
                        </tr>
                        <#list variables.historyRecordDTO as historyRecord>
                            <tr>
                                <td width="20%">
                                    ${historyRecord.logTitle}
                                </td>
                                <td style="word-wrap:break-word;word-break:break-all;" width="40%">
                                    ${historyRecord.logDetails}
                                </td>
                                <td width="20%">
                                    ${historyRecord.operator}
                                </td>
                                <td width="20%">
                                    ${historyRecord.createdTime?string("yyyy-MM-dd HH:mm:ss")}
                                </td>
                            </tr>
                        </#list>
                 </table>
		</#if>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <br>
                Thank you very much!
            </td>
        </tr>
        <tr>
            <td colspan="2">
                Wstuo Team
            </td>
        </tr>
    </#escape>
</table>
<br>
-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~
<br>
This is an automatic email by the system, please do NOT reply!
<br>
-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~