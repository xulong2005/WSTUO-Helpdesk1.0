<style>
    .table_border{width:100%}.table_border{ border-collapse:collapse; border:solid
    black; border-width:1px 0 0 1px;}.table_border caption {font-size:14px;font-weight:bolder;}.table_border
    th,.table_border td {border:solid black;border-width:0 1px 1px 0;padding-left:5px;}
</style>
<table width="550">
    <#escape x as x!>
        <tr>
            <td colspan="2">
                Hello:
                </b>
                </font>
            </td>
        </tr>
        <tr>
            <tPlease assist us to complete following survey in order to provide better services to you. Please comment on the performance of processing request-No. 

${variables.ecode}【Title：${variables.etitle}】, thank you !
                <br>
                Comment through the URL:
                <br>
                ${variables.visitPath}&visitId=${variables.visitId}
            </td>
        </tr>
        <tr>
            <td colspan="2">
                Title :  ${variables.etitle}
            </td>
        </tr>
        <#if (historyRecordDTO?exists && historyRecordDTO?size>
            0) >
            <tr>
                <td colspan="2">
                    <table class="table_border">
                        <tr>
                            <hr>
                            <td>
                                Action
                            </td>
                            <td>
                                Details
                            </td>
                            <td>
                                Operator
                            </td>
                            <td>
                                Operation Time
                            </td>
                        </tr>
                        <#list historyRecordDTO as historyRecord>
                            <tr>
                                <td width="20%">
                                    ${historyRecord.logTitle}
                                </td>
                                <td style="word-wrap:break-word;word-break:break-all;" width="40%">
                                    ${historyRecord.logDetails}
                                </td>
                                <td width="20%">
                                    ${historyRecord.operator}
                                </td>
                                <td width="20%">
                                    ${historyRecord.createdTime?string("yyyy-MM-dd HH:MM:ss")}
                                </td>
                            </tr>
                        </#list>
                    </table>
                </td>
            </tr>
        </#if>
        <tr>
            <td colspan="2">
                <br>
                Thank you very much!
            </td>
        </tr>
        <tr>
            <td colspan="2">
                Wstuo Team
            </td>
        </tr>
    </#escape>
</table>
<br>
-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~
<br>
This is an automatic email by the system, please do NOT reply!
<br>
-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~