$package('common.jbpm') 
 /**  
 * @author amy  
 * @constructor processDefined
 * @description 流程列表
 * @date 2010-10-13
 * @since version 1.0 
 */  

common.jbpm.processDefined = function(){
	return {
		/**
		 * @description 操作项格式化
		 * @param cellValue:单元格的值；
		 * @param options:字体样式等；
		 * @param row:一行的数据
		 * */
		optFormatter:function(cellValue,options,row) {
			return '&nbsp;<a href=javascript:common.jbpm.processDefined.showProcessInstances("'+row.id+'")'+ 
			' title="'+i18n['flowExample']+'">'+i18n['flowExample']+
			'</a>&nbsp;&nbsp;|&nbsp;&nbsp;'+
			'<a href=javascript:common.jbpm.processDefined.showPicture("'+row.id+'") title="'+i18n['look']+i18n['flowChart']+'">'+i18n['look']+i18n['flowChart']+'</a>'+
			'&nbsp;&nbsp;|&nbsp;&nbsp;'+
			'<a href=javascript:common.jbpm.processDefined.openFlowPropertySettingPage(\''+row.id+'\') title="'+i18n['title_flow_property_setting']+'">'+i18n['title_flow_property_setting']+'</a>'+
			'&nbsp;&nbsp;|&nbsp;&nbsp;'+
			'<a href=javascript:common.jbpm.processDefined.processDeleteOpt(\''+row.id+'\',\''+row.deploymentId+'\') title="'+i18n['deletes']+'">'+i18n['deletes']+'</a>';
		},
		/**
		 * 加载流程列表
		 */
		load_processDefined_list:function(){
			var params = $.extend({},jqGridParams, {
				url:'jbpm!findPageProcessDefinitions.action',
				colNames:['Deployment Id','ID',i18n['name'],i18n['common_key'],i18n['version'],/*i18n['description'],*/i18n['operateItems']],
				colModel:[
				          {name:'deploymentId',width:100,sortable:false,align:'center',hidden:true},
						  {name:'id',width:100,sortable:false},
						  {name:'name',width:100,sortable:false},
						  {name:'key',width:100,sortable:false},
						  {name:'version',width:50,sortable:false},
						 /* {name:'description',width:100,sortable:false},*/
						  {name:'act',sortable:false,width:230,formatter:common.jbpm.processDefined.optFormatter}
					  ],
				jsonReader: $.extend({},jqGridJsonReader, {id: "id"}),
				sortname:'key',
				pager:'#processDefinedPager'
				});
			
				$("#processDefinedGrid").jqGrid(params);
				$("#processDefinedGrid").navGrid('#processDefinedPager',navGridParams);
				//列表操作项
				$("#t_processDefinedGrid").css(jqGridTopStyles);
				$("#t_processDefinedGrid").append($('#processToolbar').html());
				
				//自适应宽度
				setGridWidth("#processDefinedGrid","regCenter",20);

		},
		/**
		 * @description 打开流程部署
		 * */
		add_openwindow:function(){
			windows('addProcess',{width:400});
		},
		
		/**
		 * @description 关闭流程部署窗口
		 * */
		add_closewindow:function(){	
			$('#addProcess').dialog('close');
		},
		/**
		 * @descriptionopen 打开搜索窗口
		 * */
		search_openwindow:function(){		
			windows('processDefinedSearchWin',{width:400,modal: false});
		},
	
		/**
		 * @description 搜索流程
		 * */
		search_opt:function(){		
			var _params = $('#processDefinedSearchForm').getForm();
			var postData = $("#processDefinedGrid").jqGrid("getGridParam", "postData");       
			$.extend(postData, _params);  //将postData中的查询参数加上查询表单的参数		
			$('#processDefinedGrid').trigger('reloadGrid',[{"page":"1"}]);
		},
		/**
		 * 流程布署
		 * */
		add_processDefined:function(){
			startProcess();
			var jpdlFileStr=$('#processFile').val();
			var _url="jbpm!deployProcessDefinitionXmlFile.action";
			if(jpdlFileStr.substr(jpdlFileStr.length-3).toLowerCase()=='zip'){
				_url="jbpm!deployProcessDefinitionFile.action";
			}else{
				_url="jbpm!deployProcessDefinitionXmlFile.action";
			}
			var total=0
			if($('#processFile').val()!=''){
				$.post('jbpm!findPageProcessDefinitions.action',function(data){
					if(data!=null){
						total=data.totalSize;
					}
				});
				$.ajaxFileUpload({
		            url:_url,
		            secureuri:true,
		            fileElementId:'processFile',
					dataType:'json',  
		            success: function(data, status){
		            	$('#addProcess').dialog('close');
		            	$('#processDefinedGrid').trigger('reloadGrid');
		            	$.post('jbpm!findPageProcessDefinitions.action',function(data){
		    				if(data!=null){
		    					if(data.totalSize>total){
		    						msgShow(i18n['msg_process_delpoySuccess'],'show');
		    					}else{
		    						msgAlert(i18n['msg_process_delpoyFailure'],'show');
		    					}
		    				}
		    			})
		    			endProcess();
		            },
		            error:function(data){
		            	$('#addProcess').dialog('close');
		            	msgAlert(i18n['msg_process_delpoyFailure'],'info');
		            	endProcess();
		            }
		        });
			}else{
				msgAlert(i18n['msg_dc_fileNull'],'info');
				endProcess();
			}
		},
		/**
		 * @description 查看流程图
		 * @param id 流程id
		 * */
		showPicture:function(id){
			var rowsId = jQuery("#processDefinedGrid").jqGrid('getGridParam','selrow');
			if(rowsId==''){
				msgAlert(i18n['plaseSelectOptInfo'],'info');
			}else{
			    var _url = '../pages/common/jbpm/traceInstance.jsp?processDefinitionId='+id;
			    basics.tab.tabUtils.refreshTab(i18n['flowChart'],_url);
			}
		},
		/**
		 * 删除流程
		 * @param processDefinitionId 流程Id
		 * @param deploymentId 部署Id
		 * */
		processDeleteOpt:function(processDefinitionId,deploymentId){
			msgConfirm(i18n['msg_msg'],'<br/>'+i18n['msg_confirmDelete'],function(){
				$.post('jbpm!findPageProcessInstance.action','processHistoriesQueryDTO.processDefinitionId='+processDefinitionId,function(res){
					if(res.data==null || res.data==''){
						var _param = $.param({'deploymentId':deploymentId},true);
						var _url = 'jbpm!unDeployProcessDefinition.action';
					    $.post(_url,_param,function(){
					    	msgShow(i18n['msg_deleteSuccessful'],'show');
							$('#processDefinedGrid').trigger('reloadGrid');
					    });
					}else{
						msgAlert(i18n['error_jbpm_notAllowDelete'],'info');
					}
				});
			});
			

		},
		/**
		 * @description 显示流程实例
		 * */
		showProcessInstances:function(id){
			var rowIds = jQuery("#processDefinedGrid").jqGrid('getGridParam','selrow');
			if(rowIds==''){
				msgAlert(i18n['plaseSelectOptInfo'],'info');
			}
			else{
			    var _url = '../pages/common/jbpm/processInstances.jsp?processDefinitionId='+id;
			    basics.tab.tabUtils.reOpenTab(_url,i18n['flowExample']+" ");
			}
		},
		
		/**打开流程属性设置页面
		 * @param processDefinitionId 流程Id
		 * */
		openFlowPropertySettingPage:function(processDefinitionId){
			basics.tab.tabUtils.reOpenTab('../pages/common/jbpm/flowPropertyMain.jsp?processDefinitionId='+processDefinitionId,i18n['title_flow_property_setting']);
		},
		/**
		 * 初始化
		 * @private
		 */
		init: function() {
			common.jbpm.processDefined.load_processDefined_list();
			$('#link_jbpm_deploy_ok').click(common.jbpm.processDefined.add_processDefined);
			$('#processDefinedSearch_opt_but').click(common.jbpm.processDefined.search_opt);
			
		}
	}
}();
$(document).ready(common.jbpm.processDefined.init);