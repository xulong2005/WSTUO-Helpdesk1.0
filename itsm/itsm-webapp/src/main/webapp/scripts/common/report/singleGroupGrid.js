/**  
 * @author Van  
 * @constructor WSTO
 * @description 自定义报表.
 * @date 2010-11-17
 * @since version 1.0 
 */ 
$package('common.report');
$import('common.config.customFilter.filterGrid_Operation');
$import('common.report.fieldsProperties');
/**  
 * @fileOverview "单项报表管理"
 * @author Van  
 * @constructor WSTO
 * @description 单项报表管理
 * @date 2011-06-13
 * @version 1.0  
 * @since version 1.0 
 */
common.report.singleGroupGrid=function(){
	this.loadShareGroupGrid=false;
	this.module=false;
	this.itsm_report_singleGroupGrid_postUrl='sgreport!save.action';
	return {
		/**
		 * @description 显示
		 * @param cell 当前列值
		 * @param event 事件
		 * @param data 行数据
		 */
		actionFormatter:function(cell,event,data){
			if(data.customFilterName!=null && data.customFilterNo!=null){
				//加载过滤器数据
				//return "<a href=\"javascript:loadReportFilter('"+data.reportId+"','"+data.customFilterNo+"','"+data.entityClass+"','"+data.reportName+"','sgreport!show.action')\">"+i18n['label_dynamicReports_viewReport']+"</a>";	
				return "<a href=\"javascript:basics.tab.tabUtils.refreshTab('"+data.reportName+"','sgreport!show.action?reportId="+data.reportId+"')\">"+i18n['label_dynamicReports_request_show']+"</a>"+"&nbsp;&nbsp;<a href=\"javascript:basics.tab.tabUtils.refreshTab('"+data.reportName+"','sgreport!showdynamic.action?reportId="+data.reportId+"&width=600')\">"+i18n['View_Portal_effect']+"</a>";
			}else{//直接预览
				return "<a href=\"javascript:basics.tab.tabUtils.refreshTab('"+data.reportName+"','sgreport!show.action?reportId="+data.reportId+"')\">"+i18n['label_dynamicReports_request_show']+"</a>"+"&nbsp;&nbsp;<a href=\"javascript:basics.tab.tabUtils.refreshTab('"+data.reportName+"','sgreport!showdynamic.action?reportId="+data.reportId+"&width=600')\">"+i18n['View_Portal_effect']+"</a>";
			}
		},
		
		
		/**
		 * @description 显示
		 * @param cell 当前列值
		 * @param event 事件
		 * @param data 行数据
		 */
		calssNameFormatter:function(cell,event,data){
			if(cell!=null && cell!='' && cell!=undefined)
				return i18n[report_group_fields[cell]['className']];
			else
				return '';
		},
		
		
		/**
		 * 加载列表.
		 */
		showGrid:function(){
		
			var params = $.extend({},jqGridParams, {	
				url:'sgreport!findPager.action',
				colNames:[i18n['label_dynamicReports_reportName'],i18n['title'],i18n['label_dynamicReports_reportType'],
				          i18n['title_creator'],i18n['title_updator'],i18n['common_createTime'],i18n['common_updateTime'],i18n['custom_report_previewReport'],'','','','','','','','','','','',''],
				colModel:[
			   		{name:'reportName',align:'center',width:30},
			   		{name:'title',align:'center',width:30,},
			   		{name:'entityClass',align:'center',width:20,formatter:common.report.singleGroupGrid.calssNameFormatter},
			   		{name:'creator',align:'center',width:30},
			   		{name:'lastUpdater',align:'center',width:30},
			   		{name:'createTime',align:'center',width:25},
			   		{name:'lastUpdateTime',align:'center',width:25},
			   		{name:'act', align:'center',width:30,sortable:false,formatter:common.report.singleGroupGrid.actionFormatter},
			   		{name:'reportId',hidden:true},
			   		{name:'entityClass',hidden:true},
			   		{name:'xgroupField',hidden:true},
			   		{name:'xcountField',hidden:true},
			   		{name:'xcountType',hidden:true},
			   		{name:'customFilterName',hidden:true},
			   		{name:'customFilterNo',hidden:true},
			   		{name:'reportModule',hidden:true},
			   		{name:'classify',hidden:true},
			   		{name:'reportSharing',hidden:true},
			   		{name:'type',hidden:true},
			   		{name:'shareGroups',hidden:true}
			   		

			   	],		
				jsonReader: $.extend(jqGridJsonReader, {id: "reportId"}),
				sortname:'reportId',
				pager:'#singleGroupGridPager'
			});
			$("#singleGroupGrid").jqGrid(params);
			$("#singleGroupGrid").navGrid('#singleGroupGridPager',navGridParams);
			//列表操作项
			$("#t_singleGroupGrid").css(jqGridTopStyles);
			$("#t_singleGroupGrid").append($('#singleGroupGridToolbar').html());

		},
		
		/**
		 * 打开添加窗口.
		 */
		add_open_window:function(){
		    $('#add_edit_singleGroup_lastUpdater').val('');    //清除更新者
		    $('#add_edit_singleGroup_reportId').val('');
			//取消禁止选择
			$('#add_edit_singleGroup_entityClass').unbind('change');
			$('#add_edit_singleGroup_entityClass').change(common.report.singleGroupGrid.reportTypeChange);
			//清空
			$('#add_edit_singleGroup_customFilterNo').html('<option value="">-- '+i18n['label_dynamicReports_chooseFilter']+' --</option>');
			$('#add_edit_singleGroup_xgroupField').html('<option value="">-- '+i18n['label_dynamicReports_chooseGroupField']+' --</option>');
			$('#add_edit_singleGroup_xcountField').html('<option value="">-- '+i18n['label_dynamicReports_chooseCountField']+' --</option>');
			
			$('#singleEntityClass').show();
			$('#add_edit_singleGroup_entityClass').attr("disabled","");
			resetForm('#singleGroupGrid_add_edit_window form');
			//$('#add_edit_singleGroup_entityClass').attr("disabled","");
			windows('singleGroupGrid_add_edit_window',{width:450});
			$("#single_shareGroupGrid_sign").val("");//报表共享组标记 will
			$('#selectShareGroupReportSingle').val('');
			itsm_report_singleGroupGrid_postUrl='sgreport!save.action';
			$('#add_edit_singleGroup_creator').val(loginName);	//添加创建者
			//$('#add_edit_singleGroup_lastUpdater').val(loginName);	//添加更新者
			module=false;
			$('#singleGroup_module').show();
		},
		
		/**
		 * 添加自定义报表.
		 */
		add:function(){
			if($('#singleGroupGrid_add_edit_window form').form('validate')){
				//分组名称
				var xgroupFieldName=$('#add_edit_singleGroup_xgroupField').find("option:selected").text();
				$('#add_edit_singleGroup_xgroupFieldName').val(xgroupFieldName);
				var boo=$("#reportModule_checkbox input[type='checkbox']").is(':checked');
				$('#reportModule_checkbox div').html('');
				if(boo){	
					//$('#reportModule_checkbox div').append('<input name="singleGroupReportDTO.reportType" type="hidden" value="singleGroup" />');
					$('#reportModule_checkbox div').append('<input name="singleGroupReportDTO.reportModule" type="hidden" value="true" />');
				}else{
					//$('#reportModule_checkbox div').append('<input name="singleGroupReportDTO.reportType" type="hidden" value="false" />');
					$('#reportModule_checkbox div').append('<input name="singleGroupReportDTO.reportModule" type="hidden" value="" />');
				}
				if($("#singleGroupReport_Sharing_group").attr("checked")){
					var ids = $("#single_shareGroupGrid").getDataIDs();
					if(ids==""){
						msgShow(i18n['report_not_group_msg'],'show');
						return false;
					}
					$('#selectShareGroupReportSingle').val(ids);
				}else{
					$('#selectShareGroupReportSingle').val('');
				}
				var _param = $('#singleGroupGrid_add_edit_window form').serialize();
				
				startProcess();
				$.post(itsm_report_singleGroupGrid_postUrl,_param,function(data){	
					
					endProcess();
					$('#singleGroupGrid').trigger('reloadGrid');
					$('#singleGroupGrid_add_edit_window').dialog('close');
					common.report.clickEvent.loadReportModuleLeftMenu();
					msgShow(i18n['label_dynamicReports_addEditSuccessful'],'show');
				});				
			}
		},
		
		/**
		 * 编辑.
		 */
		edit_open_window:function(){
			
			checkBeforeEditGrid('#singleGroupGrid', function(data){
			
				var bo=Boolean(data.reportModule);
				$('#singleEntityClass').show();
				$('#add_edit_singleGroup_entityClass').removeClass('validatebox-invalid');
				$('#add_edit_singleGroup_entityClass').attr("disabled","true");
				$('#add_edit_singleGroup_entityClass').val($vl(data.entityClass));
				if($vl(data.entityClass)=="com.wstuo.itsm.knowledge.entity.KnowledgeInfo"){
					$('#singleGroup_module').hide();
					module=true;
				}
				if(module&&$vl(data.entityClass)!="com.wstuo.itsm.knowledge.entity.KnowledgeInfo"){
					module=false;
					$('#singleGroup_module').show();
				}
				$('#reportModule_add').attr('checked',bo);
				$('#add_edit_singleGroup_lastUpdater').val(loginName);	//添加更新者
				$('#singleGroup_classify').val($vl(data.classify));
				
				//$('#add_edit_singleGroup_entityClass').attr("disabled","disabled");
				var reportSharing=data.reportSharing;
				//var reportOrgs=data.shareGroups;
				
				if(reportSharing=="share")
					$('#singleGroupReport_Sharing').attr("checked",' ');
				else if(reportSharing.substr(0,5)=="name_")
					$('#singleGroupReport_private').attr("checked",'true');
				else
					$('#singleGroupReport_Sharing_group').attr("checked",'true');
				

				//禁止选择其它
				$('#add_edit_singleGroup_entityClass').change(function (){
					$('#add_edit_singleGroup_entityClass').val($vl(data.entityClass)); 
				});
				$('#add_edit_singleGroup_reportName').val($vl(data.reportName));
				$('#add_edit_singleGroup_title').val($vl(data.title));
				$('#add_edit_singleGroup_xcountType').val($vl(data.xcountType));
				$('#add_edit_singleGroup_reportId').val($vl(data.reportId));

				common.report.singleGroupGrid.reportTypeChange();
				
				//打开窗口
				itsm_report_singleGroupGrid_postUrl='sgreport!merge.action';
				windows('singleGroupGrid_add_edit_window',{width:450});
				$("#single_shareGroupGrid_sign").val("");//报表共享组标记 will
				setTimeout(function(){//延迟加载

					$('#add_edit_singleGroup_xgroupField').val($vl(data.xgroupField));
					$('#add_edit_singleGroup_xcountField').val($vl(data.xcountField));
					if($vl(data.customFilterNo)!=0){
						$('#add_edit_singleGroup_customFilterNo').val($vl(data.customFilterNo));
					}
				},800);
				
				
				
			});
		},
		
		/**
		 * 删除报表.
		 */
		del:function(){
			
			checkBeforeDeleteGrid('#singleGroupGrid', function(ids){
				
				var _param = $.param({'reportIds':ids},true);
				$.post("sgreport!delete.action", _param, function(){
					common.report.clickEvent.loadReportModuleLeftMenu();
					$('#singleGroupGrid').trigger('reloadGrid');
					msgShow(i18n['msg_deleteSuccessful'],'show');
					
				}, "json");
				
				
			});
			
		},
		
		/**
		 * 选择过滤器
		 */
		reportTypeChange:function(){
			
			var entityClass=$('#add_edit_singleGroup_entityClass').val();
			if(entityClass=="com.wstuo.itsm.knowledge.entity.KnowledgeInfo"){
				$('#singleGroup_module').hide();
				module=true;
			}
			if(module&&entityClass!="com.wstuo.itsm.knowledge.entity.KnowledgeInfo"){
				module=false;
				$('#singleGroup_module').show();
			}
			//清空
			$('#add_edit_singleGroup_customFilterNo').html('<option value="">-- '+i18n['label_dynamicReports_chooseFilter']+' --</option>');
			$('#add_edit_singleGroup_xgroupField').html('<option value="">-- '+i18n['label_dynamicReports_chooseGroupField']+' --</option>');
			$('#add_edit_singleGroup_xcountField').html('<option value="">-- '+i18n['label_dynamicReports_chooseGroupField']+' --</option>');
			if(entityClass!=null && entityClass!=''){
				
				var filterType=report_group_fields[entityClass]['filterType'];
				
				//加载过滤器
				common.config.customFilter.filterGrid_Operation.loadFilterByModule("#add_edit_singleGroup_customFilterNo",filterType);
			
				//加载分组字段
				//$('#add_edit_singleGroup_xgroupField').empty();
				$.each(report_group_fields[entityClass]['xgroupFields'],function(k,v){
					
					$('<option value="'+k+'">'+i18n[v]+'</option>').appendTo('#add_edit_singleGroup_xgroupField');
				});
				
				//加载统计字段
				//$('#add_edit_singleGroup_xcountField').empty();
				$.each(report_group_fields[entityClass]['xcountField'],function(k,v){
					$('<option value="'+k+'">'+i18n[v]+'</option>').appendTo('#add_edit_singleGroup_xcountField');
				});
				
			}

			
		},
		
		/**
		 * 打开搜索窗口.
		 */
		search_open_window:function(){
			windows('singleGroupGrid_search_window',{width:320,modal: false});
		},
		
		/**
		 * 执行搜索.
		 */
		search:function(){

			var _search_data = $('#singleGroupGrid_search_window form').getForm();
			var _postData = $("#singleGroupGrid").jqGrid("getGridParam", "postData");       
			$.extend(_postData, _search_data);	
			$('#singleGroupGrid').trigger('reloadGrid',[{"page":"1"}]);
			
		},
		
		/**
		 * 快速搜索.
		 */
		searchByEntityClass:function(){
			
			resetForm('#singleGroupGrid_search_window form');//重置搜索表单
			$('#singleGroupGrid_search_entityClass').val($('#singleGroupGrid_searchByEntityClass').val());
			common.report.singleGroupGrid.search();
		},
		
		/**
		 * 根据选择的报表类型弹出自定义过滤器.
		 * 
		 */
		openManagerFilter:function(){
			
			var entityClass=$("#add_edit_singleGroup_entityClass").val();
			openReportFilter(entityClass,"add_edit_singleGroup_customFilterNo");
			
		},
		/**
		 * 显示共享组列表
		 * */
		showShareGroup:function(){
			windows('single_shareGroupWindow',{width:500,height:450,close:function(){
				$("#singleGroupReport_Sharing_group").attr("checked","checked");
			}});
			var groupId=$('#add_edit_singleGroup_reportId').val();
			if(!basics.ie6.htmlIsNull("#single_shareGroupGrid")){
				common.report.singleGroupGrid.showGridAction(groupId);
				$("#single_shareGroupGrid_sign").val("single_shareGroupGrid_sign");
			}else if($("#single_shareGroupGrid_sign").val()===""){
				$("#single_shareGroupGrid_sign").val("single_shareGroupGrid_sign");
				if (groupId != null && groupId != '') {
				    var postData = $("#single_shareGroupGrid").jqGrid("getGridParam", "postData");
				    $.extend(postData, {"reportId":groupId});  //将postData中的查询参数覆盖为空值
				    $('#single_shareGroupGrid').trigger('reloadGrid');
				} else {
				    $('#single_shareGroupGrid').jqGrid("clearGridData");
				}
				
			}
			
		},
		
		/**
		 * 查询共享组列表
		 * */
		showGridAction:function(groupId){
			
			
			var _postData={};
			
			if(groupId!=null && groupId!=''){
				_postData={'reportId':groupId};
			}
			
			var params = $.extend({},jqGridParams, {	
				url:'sgreport!findShareGroups.action',
				//caption:'分享组',
				postData:_postData,
				colNames:['ID',i18n['name']],
				colModel:[{name:'orgNo',width:100,align:'center',sortable:false},
				          {name:'orgName',width:300,align:'center',sortable:false}
						  ],
				jsonReader: $.extend(jqGridJsonReader, {id: "orgNo"}),
				sortname:'orgNo',
				autowidth:true,
				rowNum:10000
			});
			
			$("#single_shareGroupGrid").jqGrid(params);
			$("#single_shareGroupGrid").navGrid('#single_shareGroupGridPager',navGridParams);
			//列表操作项
			$("#t_single_shareGroupGrid").css(jqGridTopStyles);
			$("#t_single_shareGroupGrid").html($('#single_shareGroupGridToolbar').html());
			
			//$('#single_shareGroupGrid_save').unbind().click(common.config.customFilter.single_shareGroup.save);
			$('#single_shareGroupGrid_add').unbind().click(function(){common.config.customFilter.shareGroup.showSelectWindow("single_");});
			$('#single_shareGroupGrid_remove').unbind().click(function(){common.config.customFilter.shareGroup.removeShareGroups("single_");});
		},
		
		
		
		
		init:function(){
			$('.loading').hide();
			$('.content').show();
			common.report.singleGroupGrid.showGrid();
			
			//移除事件
			$('#singleGroupGrid_add,#singleGroupGrid_edit,#singleGroupGrid_delete,#add_edit_singleGroup_entityClass,#singleGroupGrid_save,#singleGroupGrid_search,#singleGroupGrid_doSearch,#singleGroupGrid_searchByEntityClass').unbind();
			
			$('#singleGroupGrid_add').click(common.report.singleGroupGrid.add_open_window);
			$('#singleGroupGrid_edit').click(common.report.singleGroupGrid.edit_open_window);
			$('#singleGroupGrid_delete').click(common.report.singleGroupGrid.del);
			$('#singleGroupGrid_search').click(common.report.singleGroupGrid.search_open_window);
			$('#singleGroupGrid_doSearch').click(common.report.singleGroupGrid.search);
			
			$('#singleGroupGrid_searchByEntityClass').change(common.report.singleGroupGrid.searchByEntityClass);
			$('#add_edit_singleGroup_entityClass').change(common.report.singleGroupGrid.reportTypeChange);
			$('#singleGroupGrid_save').click(common.report.singleGroupGrid.add);//执行保存
			$('#add_edit_singleGroup_addCustomFilter').click(common.report.singleGroupGrid.openManagerFilter);
		
			$('#singleGroupReport_Sharing_group').click(common.report.singleGroupGrid.showShareGroup);
		}
		
	};
	
}();

$(document).ready(common.report.singleGroupGrid.init);