$package('common.config.basicConfig')
/**  
 * 货币设置
 * @author QXY  
 * @constructor WSTO
 * @description 货币设置
 * @date 2010-11-17
 * @since version 1.0 
 */  
common.config.basicConfig.currencyGrid=function(){
	return {
		/**
		 * 加载当前货币类型
		 * @private
		 */
		loadCurrency:function(){
			$.post('currency!findDefaultCurrency.action',function(data){
				$('#currency_cno').val(data.cno);
				$('#currency_sign').val(data.sign);
			});
		},
		/**
		 * 保存货币.
		 * @private
		 */
		saveCurrency:function(){
			var frm = $('#currency_form').serialize();
			var url = "currency!saveOrUpdateCurrency.action";
			$.post(url,frm, function(data){
				currency_sign = data;
				common.config.basicConfig.currencyGrid.loadCurrency();
				msgShow(i18n['msg_basicConfig_currencyAddEditSuccessful'],'show');
			});
		},
		/**
		 * 初始化加载.
		 * @private
		 */
		init:function(){
			$('#currencyMain_loading').hide();
			$('#currencyMain_content').show();
			common.config.basicConfig.currencyGrid.loadCurrency();
			$('#currency_sign_linkBut').click(common.config.basicConfig.currencyGrid.saveCurrency);
		}
	}
}();
$(document).ready(common.config.basicConfig.currencyGrid.init);
