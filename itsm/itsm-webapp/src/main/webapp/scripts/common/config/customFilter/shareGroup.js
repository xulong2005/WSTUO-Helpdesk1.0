$package('common.config.customFilter');
/**  
 * @author Van  
 * @constructor WSTO
 * @description 过滤器分享到组
 * @date 2012-06-16
 * @since version 2.0.1
 */
common.config.customFilter.shareGroup = function(){
		this.loadShareGroupGrid=false;
		return {
			/**
			 * 打开新增窗口.
			 * @param  filterId 过滤器Id
			 */
			openAppendWindow:function(filterId){
				windows('filtershareGroupWindow',{width:500,height:450});
				common.config.customFilter.shareGroup.showGrid(filterId);
			},
			/**
			 * 显示选择组树
			 * @param sign 分享到组对应模块标识
			 */
			showSelectWindow:function(sign){
				windows('selectShareGroupTree',{width:250,height:400});
				$('#selectShareGroupTreeDiv').jstree({
					"json_data":{
					    ajax: {
					    	url : "organization!findAllByUser.action?companyNo=-1",
					    	data:function(n){
						    	  return {'parentOrgNo':n.attr ? n.attr("orgNo").replace("node_",""):0};//types is in action
							},
					    	cache:false}
					},
					"plugins" : [ "themes", "json_data", "checkbox" ] 
					}).bind('dblclick.jstree',function(event){
					      $("#selectShareGroupTreeDiv ul").remove();
					      common.config.customFilter.shareGroup.showSelectWindow(sign);
				    });
				$('#shareGroupGrid_getSelectedNodes').click(function(){common.config.customFilter.shareGroup.confirmSelect(sign);});
			},
			/**
			 * 关闭分享到组的窗口
			 */
			closeSelectWindow : function() {
			    $('#filtershareGroupWindow').dialog('close');
			},
			/**
			 * 移除组.
			 * @param sign 分享到组对应模块标识
			 */
			removeShareGroups:function(sign){
				checkBeforeDeleteGrid('#'+sign+'shareGroupGrid',function(rowIds){

					for(var i=rowIds.length;i>=0;i--){
						$("#"+sign+"shareGroupGrid").delRowData(rowIds[i]);
					}
				});
				
			},
			/**
			 * 确认选择.
			 * @param sign 分享到组对应模块标识
			 */
			confirmSelect:function(sign){
				$("#selectShareGroupTreeDiv").jstree("get_checked",false,true).each(function (i,n) { 
					 var node = jQuery(this); 
					 var orgType=node.attr('orgType').replace("Panel","");
					 var orgNo=node.attr('orgNo');
					 if(orgNo==null){//校正orgNo
						 orgNo=1;
					 }
					 var url="organization!findOrganizationsByType.action?orgNo="+orgNo+"&orgType="+orgType;
					 $.post(url,function(res){
							for(var i=0;i<res.length;i++){
								var row=$("#"+sign+"shareGroupGrid").getRowData(res[i].orgNo);  //根据ID拿到一行的数据  判断是否已经存在
								if(row.orgNo==null){
									var dataRow = {orgNo:res[i].orgNo,orgName:res[i].orgName}; 
									$("#"+sign+"shareGroupGrid").jqGrid("addRowData",res[i].orgNo, dataRow, "first"); 
								}
							}
					});
				});
				$('#selectShareGroupTree').dialog('close');
			},
			/**
			 * 显示列表
			 * @param filterId 过滤器Id
			 */
			showGrid:function(filterId){
				if(filterId==null || filterId=='0'){//新增
					filterId='';
				}
				if($("#loadShareGroupGrid").val()===""){
					$("#loadShareGroupGrid").val("loadShareGroupGrid");
					common.config.customFilter.shareGroup.showGridAction(filterId);
					$("#loadShareGroupGrid_sign").val("sign");
				}else if($("#loadShareGroupGrid_sign").val()===""){
					$("#loadShareGroupGrid_sign").val("sign");
					var postData = $("#filtershareGroupGrid").jqGrid("getGridParam", "postData");       
					$.extend(postData, {"filterId":filterId});  //将postData中的查询参数覆盖为空值
					$('#filtershareGroupGrid').trigger('reloadGrid',[{"page":"1"}]);
				}
			},
			/**
			 * 保存到组的信息
			 */
			save:function(){
//				if($('#shareGroupGrid').html().indexOf('<tr')!=-1){    //已选数据
				var ids = $("#shareGroupGrid").getDataIDs();
				$('#selectShareGroupReportKPI').val(ids);
				$('#selectShareGroupReportSingle').val(ids);
				$('#selectShareGroupReportCrosstab').val(ids);
				$('#selectShareGroupReportMutil').val(ids);
				$('#shareGroupWindow').dialog('close');
				$("#singleGroupReport_Sharing_group").attr("checked","checked");
				$("#crosstabGroupReport_Sharing_group").attr("checked","checked");
				$("#multilGroupReport_Sharing_group").attr("checked","checked");
				$("#kpiGroupReport_Sharing_group").attr("checked","checked");
//				}else{     //为选择数据
//					msgAlert(i18n['msg_atLeastChooseOneData'],'info');
//				}
			},
			/**
			 * 显示列表
			 * @param filterId 过滤器Id
			 */
			showGridAction:function(filterId){
				var _postData={};
				if(filterId!=null && filterId!=''){
					_postData={'filterId':filterId}
				}
				var params = $.extend({},jqGridParams, {	
					url:'filter!findShareGroups.action',
					//caption:'分享组',
					postData:_postData,
					colNames:['ID',i18n['name']],
					colModel:[{name:'orgNo',width:80,align:'center',sortable:false},
					          {name:'orgName',width:300,align:'center',sortable:false}
							  ],
					jsonReader: $.extend(jqGridJsonReader, {id: "orgNo"}),
					sortname:'orgNo',
					autowidth:true,
					rowNum:10000
				});
				$("#filtershareGroupGrid").jqGrid(params);
				$("#filtershareGroupGrid").navGrid('#filtershareGroupGridPager',navGridParams);
				//列表操作项
				$("#t_filtershareGroupGrid").css(jqGridTopStyles);
				$("#t_filtershareGroupGrid").append($('#filtershareGroupGridToolbar').html());
				//$('#filtershareGroupGrid_save').unbind().click(common.config.customFilter.shareGroup.save);
				$('#filtershareGroupGrid_add').unbind().click(function(){common.config.customFilter.shareGroup.showSelectWindow("filter");});
				$('#filtershareGroupGrid_remove').unbind().click(function(){common.config.customFilter.shareGroup.removeShareGroups("filter");});
				$('#filtershareGroupGrid_enter').unbind().click(function(){common.config.customFilter.shareGroup.closeSelectWindow();});
			},
			/**
			 * 是否打开共享组面板
			 */
			checkShare:function(){
				var vl=$('#customFilterDTO_share').val();
				$('#customFilterDTO_share_vl').val(vl);
				if(vl=='sharegroup'){
					$('#selectShareGroup').show();
					//$('#selectShareGroup').unbind().click(function(){
						var filterId=$('#filter_Id').val();
						common.config.customFilter.shareGroup.openAppendWindow(filterId);
					//});
				}else{
					$('#selectShareGroup').hide();
				}
			},
			/**
			 * 初始化
			 * @private 
			 */
			init:function(){
				/*$('#selectShareGroupTreeDiv').unbind().dblclick(function(event){
						var trgt = $(event.target);
						alert(trgt);
						var sss=$('#selectShareGroupTreeDiv li > ins');
						if(trgt.is("ins") && event.pageY - trgt.offset().top < sss.data.core.li_height) { sss.toggle_node(trgt); }
				});*/
				$('#selectShareGroup').unbind().click(common.config.customFilter.shareGroup.checkShare);
			}
		}
}();
$(document).ready(common.config.customFilter.shareGroup.init);
