$package('common.config.cab')
$import("common.security.userUtil");
/**  
 * 时区设置
 * @author QXY  
 * @constructor WSTO
 * @description 时区设置
 * @date 2010-11-17
 * @since version 1.0 
 */  
common.config.cab.cabAdmin=function(){
	this.opt;
	var cabNumberRowData=null;
	return {
		/**
		 * CAB列表
		 * @private
		 */
		cabList:function(){
			var params = $.extend({},jqGridParams, {	
				url:'cab!findPagerCAB.action',
				caption:i18n['title_cabList'],
				colNames:[i18n['number'],i18n['name'],i18n['description'],i18n['operateItems']],
				colModel:[
			   		{name:'cabId',width:60,sortable:true},
			   		{name:'cabName',width:150},
			   		{name:'cabDesc',width:200},
			   		{name:'act',align:'center',sortable:false,formatter:common.config.cab.cabAdmin.cabActFormatter}
			   	],		
				jsonReader: $.extend(jqGridJsonReader, {id: "cabId"}),
				sortname:'cabId',
				onSelectRow:function(rowId){
				    setTimeout(function(){
                        var rowId = $('#cabGrid').getGridParam('selrow');
                        if (rowId)
                            common.config.cab.cabAdmin.cabMemberListReload(rowId);
                    }, 0);
				},
				pager:'#cabPager'
			});
			$("#cabGrid").jqGrid(params);
			$("#cabGrid").navGrid('#cabPager',navGridParams);
			//列表操作项
			$("#t_cabGrid").css(jqGridTopStyles);
			$("#t_cabGrid").append($('#cabGridToolbar').html());
			//自适应大小
			setGridWidth("#cabGrid","regCenter",20);
		},
		/**
		 * @description cab操作项
		 * @param cell 当前列值
		 * @param event 事件
		 * @param data 行数据
		 * @private
		 */
		cabActFormatter:function(cell,opt,data){ 
			return $('#cabActFormatterDiv').html().replace(/{cabId}/g,opt.rowId);
		},
		/**
		 * 打开CAB添加窗口
		 * @private
		 */
		openCabAddWin:function(){
			$("#cabName").val('');
			$("#cabDesc").val('');
			$("#cabId").val('');
			opt='saveCAB';
			windows('cabAddOrEditDiv',{width:400});
		},
		/**
		 * 打开CAB编辑窗口
		 * @param rowData   行数据
		 * @private
		 */
		openCabEditWin:function(rowData) {
			$("#cabName").val(rowData.cabName);
			$("#cabDesc").val(rowData.cabDesc);
			$("#cabId").val(rowData.cabId);
			opt='editCAB';
			windows('cabAddOrEditDiv',{width:400});
		},
		/**
		 * 编辑CAB
		 * @param rowId 列ID
		 */
		cab_edit:function(rowId){
			var rowData= $("#cabGrid").jqGrid('getRowData',rowId);  // 行数据  
			common.config.cab.cabAdmin.openCabEditWin(rowData);
		},
		/**
		 * 编辑
		 * @private
		 */
		cab_edit_aff:function(){
			checkBeforeEditGrid('#cabGrid', common.config.cab.cabAdmin.openCabEditWin);
		},
		/**
		 * CAB添加编辑保存
		 * @private
		 */
		cabAddOrEditOpt:function(){
			if($("#cabName").val().toLowerCase() == "null"){
				$("#cabName").val("");
			}
			if($('#cabAddOrEditDiv form').form('validate')){
				var _params = $('#cabAddOrEditDiv form').serialize();
				startProcess();
				$.post('cab!'+opt+'.action', _params, function(){
					$('#cabAddOrEditDiv').dialog('close');
					$('#cabGrid').trigger('reloadGrid');
					msgShow(i18n['saveSuccess'],'show');
					endProcess();
				})
			}
		},
		/**
		 * 删除CAB 
		 * @param rowId 列ID
		 */
		cab_delete:function(rowId){
			msgConfirm(i18n['msg_msg'],'<br/>'+i18n['msg_confirmDelete'],function(){
				common.config.cab.cabAdmin.deleteCab(rowId);
			});
		},
		/**
		 * 删除
		 * @private
		 */
		cab_delete_aff:function(){
			checkBeforeDeleteGrid('#cabGrid', common.config.cab.cabAdmin.deleteCab);
		},
		/**
		 * 执行删除
		 * @param rowId 列ID
		 * @private
		 */
		deleteCab:function(rowIds) {
			var _param = $.param({'cabIds':rowIds},true);
			startProcess();
			$.post("cab!deleteCAB.action", _param, function(){
				$('#cabGrid').trigger('reloadGrid');
				var _url = 'cab!findCABMember.action?cabId=';		
				$('#cabMemberGrid').jqGrid('setGridParam',{url:_url})
					.trigger('reloadGrid',[{"page":"1"}]).jqGrid('setGridParam',{url:_url});
				msgShow(i18n['deleteSuccess'],'show');
				$('#cab_cabId').val('');
				$('#cabMemberId').jqGrid('setGridParam',{url:'cab!findCABMember.action?cabId='}).trigger('reloadGrid',[{"page":"1"}]);
				endProcess();
			}, "json");
		},
		/**
		 * CAB搜索
		 * @private 
		 */
		openCabSearchWin:function(){
			$('#cabSearch_opt_but').unbind().click(common.config.cab.cabAdmin.cabSearchOpt);
			windows('cabSearchDiv',{width:400,modal: false});
			//禁止回车事件
			$("#cabQueryDtoCabName").keydown(function (e) {
	            var curKey = e.which;
	            if (curKey == 13) {
	                return false;
	            }
	        });
		},
		/**
		 * CAB搜索
		 * @private 
		 */
		cabSearchOpt:function(){
			var _params = $('#searchCabForm').getForm();
			var postData = $("#cabGrid").jqGrid("getGridParam", "postData");       
			$.extend(postData, _params);  //将postData中的查询参数加上查询表单的参数		
			$('#cabGrid').trigger('reloadGrid',[{"page":"1"}]);
		},
		/**
		 * CAB成员加载
		 * @param id ID
		 * @private
		 */
		cabMemberListReload:function(id){
			$('#cab_cabId').val(id);
			var _url = 'cab!findCABMember.action?cabId='+id;		
			$('#cabMemberGrid').jqGrid('setGridParam',{url:_url})
				.trigger('reloadGrid',[{"page":"1"}]).jqGrid('setGridParam',{url:_url});
		},
		/**
		 * CAB成员列表
		 */
		cabMemberList:function(){
			var params = $.extend({},jqGridParams, {	
				url:'cab!findCABMember.action?cabId=',
				caption:i18n['title_cabMember_list'],
				colNames:[i18n['number'],i18n['title_cab_approvalMember'],'','',i18n['description'],i18n['operateItems'],'',''],
				colModel:[
			   		{name:'cabMemberId',width:60,sortable:false},
			   		{name:'approvalMemberFullName',width:150,sortable:false},
			   		{name:'approvalMember',width:150,sortable:false,hidden:true},
			   		{name:'delegateMember',width:200,sortable:false,hidden:true},
			   		{name:'desc',width:200,sortable:false},
			   		{name:'act',hidden:true,sortable:false},
			   		{name:'approvalMemberId',hidden:true},
			   		{name:'delegateMemberId',hidden:true}
			   	],	
			   	
				jsonReader: $.extend(jqGridJsonReader, {id: "cabMemberId"}),
				sortname:'cabMemberId',
				pager:'#cabMemberPager'
			});
			$("#cabMemberGrid").jqGrid(params);
			$("#cabMemberGrid").navGrid('#cabMemberPager',navGridParams);
			//列表操作项
			$("#t_cabMemberGrid").css(jqGridTopStyles);
			$("#t_cabMemberGrid").append($('#cabMemberGridToolbar').html());
			//自适应大小
			setGridWidth("#cabMemberGrid","regCenter",20);
		},
		/**
		 * CAB成员添加窗口
		 */
		openCabMemberAddOrEditWin:function(){
			opt='addCabMember';
			cabNumberRowData=null;
			if($('#cab_cabId').val()==''){
				msgAlert(i18n['msg_msg_pleaseChooseCab'],'info');
			}else{
				$('#approvalMemberName').val('');
				$('#approvalMember').val('');
				$('#delegateMemberName').val('');
				$('#delegateMember').val('');
				$('#cabMemberDesc').val('');
				windows('cabMemberAddDiv',{width:450});
			}	
		},
		/**
		 * CAB成员添加编辑保存
		 */
		cabMemberAddOrEditOpt:function(){
			if($('#cabMemberAddDiv form').form('validate')){
				var approvalMember = $("#cabMemberAddDiv #approvalMember").val();
				if(cabNumberRowData==null){
					common.config.cab.cabAdmin.checkAddCabMember(approvalMember);
				}else{
					if(approvalMember===cabNumberRowData.approvalMemberId){
						common.config.cab.cabAdmin.executeSaveOrEdit();
					}else{
						common.config.cab.cabAdmin.checkAddCabMember(approvalMember);
					}
				}
			}
		},
		executeSaveOrEdit:function(){
			var _params = $('#cabMemberAddDiv form').serialize();
			startProcess();
			$.post('cabMember!'+opt+'.action', _params, function(){
				$('#cabMemberAddDiv').dialog('close');
				$('#cabMemberGrid').trigger('reloadGrid');
				msgShow(i18n['saveSuccess'],'show');
				endProcess();
			})
		},
		checkAddCabMember:function(approvalMember){
			var is_exist=false;
			$.each($("#cabMemberGrid td[aria-describedby=cabMemberGrid_approvalMemberId]"),function(ind,val){
				if($(this).attr("title")===approvalMember){
					is_exist=true;
					return;
				}
			});
			if(is_exist){
				msgShow("该审批成员已存在","show");
			}else{
				common.config.cab.cabAdmin.executeSaveOrEdit();
			}
		},
		/**
		 * CABMember编辑窗口
		 * @param rowData   行数据
		 * @private
		 */
		openCabMemberEditWin:function(rowData) {
			$("#cabMemberId").val(rowData.cabMemberId);
			$("#approvalMemberName").val(rowData.approvalMemberFullName);
			$("#approvalMember").val(rowData.approvalMemberId);
			$("#delegateMemberName").val(rowData.delegateMemberFullName);
			$("#delegateMember").val(rowData.delegateMemberId);
			$("#cabMemberDesc").val(rowData.desc);
			opt='editCabMember';
			cabNumberRowData=rowData;
			windows('cabMemberAddDiv',{width:450});
		},
		/**
		 * 编辑CAB用户
		 */
		cabMember_edit_aff:function(){
			checkBeforeEditGrid('#cabMemberGrid', common.config.cab.cabAdmin.openCabMemberEditWin);
		},
		/**
		 * 删除CAB用户
		 */
		cabMember_delete_aff:function(){
			checkBeforeDeleteGrid('#cabMemberGrid', common.config.cab.cabAdmin.deleteCabMember);
		},
		/**
		 * 执行删除
		 * @param rowIds   编号
		 * @private
		 */
		deleteCabMember:function(rowIds) {
			var _param = $.param({'cabMemberIds':rowIds},true);
			$.post("cabMember!deleteCABMember.action", _param+'&cabId='+$('#cab_cabId').val(), function(){
				$('#cabMemberGrid').trigger('reloadGrid');
				msgShow(i18n['deleteSuccess'],'show');
			}, "json");
		},
		/**
		 * 初始化
		 * @private
		 */
		init:function(){
			showAndHidePanel("#cabManage_panel","#cabManage_loading");
			common.config.cab.cabAdmin.cabList();
			//页面事件
			$('#cab_add_but').click(common.config.cab.cabAdmin.openCabAddWin);
			$('#cab_edit_but').click(common.config.cab.cabAdmin.cab_edit_aff);
			$('#cab_delete_but').click(common.config.cab.cabAdmin.cab_delete_aff);
			$('#cab_add_opt_but').click(common.config.cab.cabAdmin.cabAddOrEditOpt);
			
			$('#cab_search_but').click(common.config.cab.cabAdmin.openCabSearchWin);
			
			
			$('#approvalMemberName').click(function(){
				common.security.userUtil.selectUser('#approvalMemberName','#approvalMember','','fullName','-1')
			});
			$('#delegateMemberName').click(function(){
				common.security.userUtil.selectUser('#delegateMemberName','#delegateMember','','fullName','-1')
			});
		}	
	}
}();
$(document).ready(common.config.cab.cabAdmin.init);