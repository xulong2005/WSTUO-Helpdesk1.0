$package('common.config.category');
/**  
 * @author QXY  
 * @constructor WSTO
 * @description 配置项分类树
 * @date 2010-11-17
 * @since version 1.0 
 */ 
common.config.category.CICategoryTree=function(){
	return {
		/**
		 * 显示配置项分类树.
		 */
		showCICategoryTree:function(){
			$("#configureItemCategoryTree_div").jstree({
				"json_data":{
					ajax: {
						url : "ciCategory!getConfigurationCategoryTree.action",
						data:function(n){
					    	  return {'parentEventId':n.attr ? n.attr("cno").replace("node_",""):0};//types is in action
					    },
						cache: false
					}
				},
				"dnd": {
					"drag_check": function() {} 
				},
				"plugins" : [ "themes", "json_data", "ui", "crrm", "cookies","dnd","search","types","hotkeys","contextmenu"]
			})
			//.bind('loaded.jstree', function(e,data){data.inst.open_all(-1);})
			.bind("delete_node.jstree", common.config.category.CICategoryTree.removeNode)
			.bind("select_node.jstree", common.config.category.CICategoryTree.selectNode)
			.bind("move_node.jstree", common.config.category.CICategoryTree.moveNode);
		},

		/**
		 * @description 选择节点.
		 * @param event 事件
		 * @param data 数据
		 * @private
		 */
		selectNode:function(event, data){
			var obj = data.rslt.obj;
			var _parent = data.inst._get_parent();
			var _parentName = '';
			var _categoryType='';
			var _categoryCodeRule='';
			var icon=data.rslt.obj.attr('iconPath');
			
			//不允许修改父节点
			if(_parent==-1){
				$("#edit_cname").attr("disabled","true");
			}else{
				$("#edit_cname").attr("disabled","");
			}
			_categoryType=data.rslt.obj.attr('categoryType');
			
			$('#edit_cno').val(obj.attr('cno'));
			$('#edit_cno_show').text(obj.attr('cno'));
			$('#edit_cname').val(obj.attr('cname'));
			$('#edit_iconPath').val(icon);
			if(obj.attr('description')!=null && obj.attr('description')!="null"){
				$('#edit_description').val(obj.attr('description'));
			}else{
				$('#edit_description').val("");
			}
			
			$('#add_parentNo').val(obj.attr('cno'));
			
			$('#edit_category').val(_categoryType+"");
			if(_categoryType==0 || _categoryType=='undefined' || _categoryType==undefined ){
				$('#edit_category').val('');
			}
			if(obj.attr('categoryCodeRule')!=null && obj.attr('categoryCodeRule')!='null')
				$('#edit_categoryCodeRule').val(obj.attr('categoryCodeRule'));
			else
				$('#edit_categoryCodeRule').val('');
			$('#edit_showIconPanel').html("");
			if(icon!=null && icon!="" &&  icon!="null"){
				  //$('#edit_showIconPanel').html("<img src='../upload/images/"+icon+"' width='30px' height='30px' />");
				  $('#edit_showIconPanel').html("<img src='organization!getImageUrlforimageStream.action?imgFilePath="+icon+"' width='30px' height='30px' />");
			}
		},
		
		/**
		 * @description 拖拽节点
		 * @param event 事件
		 * @param data 数据
		 * @private
		 */
		moveNode:function (e, data) {
			data.rslt.o.each(function (i) {
				    var tag=data.rslt.cy;
					var _cno = data.rslt.o.attr("cno");
					var _pno = data.rslt.np.attr("cno");
					var _params = {
	                       "categoryDTO.cno" : _cno,
	                       "categoryDTO.parentNo" : _pno
	                   };
					// 判断分类是否存在    
                    $.post('ciCategory!isCategoryExisted.action', _params, function(data){
                        if(data) {
                            common.config.category.CICategoryTree.showCICategoryTree();
                            msgAlert(i18n["msg_incorrect_operation"],"info");
                        } else {
        				   if(tag==true){//复制
        					   common.config.category.CICategoryTree.moveNode_copy(_cno,_pno);
        				   }else{//剪切
        					   common.config.category.CICategoryTree.moveNode_cut(_cno,_pno);
        				   }
                        }
                    });
			});
		},
		 /**
		  * @description 复制树节点.
		  * @param _cno 配置项Id
		  * @param _pno 父节点Id
		  * @private
		  */
		moveNode_copy:function (_cno, _pno) {
			if(!isNaN(_cno)&&!isNaN(_pno)){
				var _url = "ciCategory!copyCICategory.action";
				var _params = {
					"categoryDTO.cno" : _cno,
					"categoryDTO.parentNo" : _pno
				};
				$.post(_url,_params,function (r) {
					common.config.category.CICategoryTree.showCICategoryTree();
					msgShow(i18n['msg_operationSuccessful'],'show');
				},'json');
			}else{
				common.config.category.CICategoryTree.showCICategoryTree();
				msgAlert(i18n["msg_incorrect_operation"],"info");
			}
		},
		
		/**
		 * @description 剪切树节点.
		 * @param _cno 配置项Id
		 * @param _pno 父节点Id
		 * @private
		 */
		moveNode_cut:function (_cno, _pno) {
			if(!isNaN(_cno)&&!isNaN(_pno)){
				var _url = "ciCategory!changeParent.action";
				var _params = {
					"categoryDTO.cno" : _cno,
					"categoryDTO.parentNo" : _pno
				};
				$.post(_url,_params,function (r) {
						common.config.category.CICategoryTree.showCICategoryTree();
						msgShow(i18n['msg_operationSuccessful'],'show');	
						
				},'json');
			}else{
				common.config.category.CICategoryTree.showCICategoryTree();
				msgAlert(i18n["msg_incorrect_operation"],"info");
			}
		},
		
		/**
		 * @description 删除节点.
		 * @param e 事件
		 * @param data 数据
		 * @private
		 */
		
		removeNode:function (e, data) {
			var categoryRoot = data.rslt.obj.attr("categoryRoot");
			//不允许删除父节点
			if(categoryRoot!=null && categoryRoot!='null' && categoryRoot!=''){
				msgShow(i18n['ERROR_PARENT_ORG_DONOT_DELETE'],'info');
				$.jstree.rollback(data.rlbk);
			}else{
				var _url = "ciCategory!remove.action";
				var _params = {
					"categoryDTO.cno" : data.rslt.obj.attr("cno")
				};
				$.ajax({
					type:'post',
					url:_url,
					data:_params,
					dataType:'json',
					success: function(r) {
						common.config.category.CICategoryTree.showCICategoryTree();
						msgShow(i18n['msg_operationSuccessful'],'show');
					},
					error: function(r) {
						$.jstree.rollback(data.rlbk);
					}
				});
				
			}
		},
		/**
		 * 初始化
		 * @private
		 */
		init:function(){
			common.config.category.CICategoryTree.showCICategoryTree();
		}
	}
 }();