$package('common.config.updateLevel')
$import("common.security.userUtil");
/**  
 * @author Van  
 * @constructor updateLevelMain
 * @description 自动升级函数
 * @date 2010-11-17
 * @since version 1.0 
 */  
common.config.updateLevel.updateLevelGrid = function() {
	this.operationTag;
	return{
		/**
		 * @description 动作格式化.
		 * @param cell 当前列值
		 * @param event 事件
		 * @param data 行数据
		 * @private
		 */
		updateLevelGridActionFormatter:function(cell,opt,data){
			
			return actionFormat('1','1')
			.replace('[edit]','common.config.updateLevel.updateLevelGrid.editUpdateLevel_openWindow_aff('+opt.rowId+')')
			.replace('[delete]','common.config.updateLevel.updateLevelGrid.deleteUpdateLevelInLine('+opt.rowId+')');

		},
		 /**
		  * @description 查看升级级别列表   
		  */
		showUpdateLevelGrid:function(){
			
			var params = $.extend({},jqGridParams, {	
				url:'updatelevel!findAllLevels.action',
				colNames:[i18n['levelName'],i18n['escalateTo'],i18n['operateItems'],'',''],
				colModel:[
						  {name:'ulName',width:100,align:'center'},
						  {name:'approvalName',width:80,align:'center',sortable:false},
						  {name:'act', width:80,align:'center',sortable:false,formatter:common.config.updateLevel.updateLevelGrid.updateLevelGridActionFormatter},
						  {name:'ulId',hidden:true},
						  {name:'approvalNo',hidden:true}
						  ],
				jsonReader: $.extend(jqGridJsonReader, {id: "ulId"}),
				sortname:'ulId',
				pager:'#updateLevelGridPager'
				});
				$("#updateLevelGrid").jqGrid(params);
				$("#updateLevelGrid").navGrid('#updateLevelGridPager',navGridParams);
				//列表操作项
				$("#t_updateLevelGrid").css(jqGridTopStyles);
				$("#t_updateLevelGrid").append($('#updateLevelGridToolbar').html());
				
				//自适应宽度
				setGridWidth("#updateLevelGrid","regCenter",20);
		},
		 /**
		  * @description 新增升级级别  
		  */
		addUpdateLevel_openWindow:function(){
			
			resetForm('#updateLevelWindow form');
			windows('updateLevelWindow',{width: 400});
			operationTag="save";
			
		},
		/**
		 * 保存升级级别.
		 */
		saveUpdateLevel:function(){
			if($('#updateLevelForm').form('validate')){
				var frm = $('#updateLevelWindow form').serialize();
				var url = "updatelevel!"+operationTag+".action";
				startProcess();
				$.post(url,frm, function(){
					$('#updateLevelGrid').trigger('reloadGrid');
					$('#updateLevelWindow').dialog('close');
					msgShow(i18n['msg_operationSuccessful'],'show');
					endProcess();
				});
			}
		},
		 /**
		  * @description 编辑升级级别  
		  */
		editUpdateLevel_openWindow:function(){
			checkBeforeEditGrid("#updateLevelGrid",function(rowData){
				 $('#updateLevel_uiId').val(rowData.ulId);
				 $('#updateLevel_ulName').val(rowData.ulName);
				 $('#updateLevel_approvalName').val(rowData.approvalName);
				 $('#updateLevel_approvalNo').val(rowData.approvalNo);
				 windows('updateLevelWindow',{width: 400});
				 operationTag="merge";
			});
		},
		 /**
		  * @description 编辑升级级别  
		  * @param  rowIds 行编号
		  */
		editUpdateLevel_openWindow_aff:function(rowId){
			var rowData= $("#updateLevelGrid").jqGrid('getRowData',rowId);  // 行数据  
				 $('#updateLevel_uiId').val(rowData.ulId);
				 $('#updateLevel_ulName').val(rowData.ulName);
				 $('#updateLevel_approvalName').val(rowData.approvalName);
				 $('#updateLevel_approvalNo').val(rowData.approvalNo);
				 windows('updateLevelWindow',{width: 400});
				 operationTag="merge";
		},
		 /**
		  * @description 删除升级级别
		  */
		deleteUpdateLevels:function(){
			checkBeforeDeleteGrid("#updateLevelGrid",function(rowIds){
				var param = $.param({'ulIds':rowIds},true);
				startProcess();
				$.post("updatelevel!delete.action", param, function(data){
					if(data){
						msgShow(i18n['deleteSuccess'],'show');
						$('#updateLevelGrid').trigger('reloadGrid');
					}else{
						
						msgAlert(i18n['systemDataNotDelete'],'info');
					}
					endProcess();
				}, "json");
				
			});
		},
		 /**
		  * @description 删除升级级别
		  * @param  rowIds 行编号
		  */
		deleteUpdateLevelInLine:function(rowId){
			msgConfirm(i18n['msg_msg'],'<br/>'+i18n['msg_confirmDelete'],function(){
				var param = $.param({'ulIds':rowId},true);
				$.post("updatelevel!delete.action", param, function(data){
					if(data){
						msgShow(i18n['deleteSuccess'],'show');
						$('#updateLevelGrid').trigger('reloadGrid');
					}else{
						
						msgAlert(i18n['systemDataNotDelete'],'info');
					}
					
				}, "json");
				
			});
		},
		/**
		 * @description 搜索升级级别 
		 */
		searchUpdateLevel_openWindow:function(){
			windows('searchUpdateLevelGrid',{width:400,modal: false});
		},
		
		/**
		 * 进行搜索操作.
		 */
		searchUpdateLevel:function(){
				var searchName=$('#search_updateLevelQueryDTO_ulName').val();
				var postData = $("#updateLevelGrid").jqGrid("getGridParam", "postData");       
				$.extend(postData, {'updateLevelQueryDTO.ulName':searchName});  //将postData中的查询参数加上查询表单的参数		
				$('#updateLevelGrid').trigger('reloadGrid',[{"page":"1"}]);
				return false;
		},
		/**
		 * 初始化
		 * @private
		 */
		init:function(){
			$("#updateLevelMain_loading").hide();
			$("#updateLevelMain_content").show();
			common.config.updateLevel.updateLevelGrid.showUpdateLevelGrid(); 
			$('#updateLevel_approvalName').click(function(){
				common.security.userUtil.selectUser('#updateLevel_approvalName','#updateLevel_approvalNo','','fullName',topCompanyNo);
			});
			$('#addUpdateLevelBtn').click(common.config.updateLevel.updateLevelGrid.addUpdateLevel_openWindow);//添加升级级别
			$('#saveUpdateLevelBtn').click(common.config.updateLevel.updateLevelGrid.saveUpdateLevel);//保存
			$('#editUpdateLevelBtn').click(common.config.updateLevel.updateLevelGrid.editUpdateLevel_openWindow);
			$('#deleteUpdateLevelBtn').click(common.config.updateLevel.updateLevelGrid.deleteUpdateLevels);
			$('#searchUpdateLevelBtn').click(common.config.updateLevel.updateLevelGrid.searchUpdateLevel_openWindow);
			$('#doSearchUpdateLevelBtn').click(common.config.updateLevel.updateLevelGrid.searchUpdateLevel);
		}
	}
}();
//载入
$(document).ready(common.config.updateLevel.updateLevelGrid.init);