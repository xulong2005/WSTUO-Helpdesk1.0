$package('common.config.historyData');
/**  
 * @author Van  
 * @constructor WSTO
 * @description "历史数据"
 * @date 2010-11-17
 * @since version 1.0 
 */
common.config.historyData.historyDataGrid = function () {
    return {
    	/**
    	 * 类型格式化.
    	 */
    	entityNameFormtter:function(cell,option,data){
    		var arr={'KnowledgeInfo':i18n['knowledge'],'Request':i18n['request'],'Problems':i18n['problem']};
    		return arr[data.entityName];
    		
    	},
    	/**
    	 *匹配恢复的数据的Url
    	 *@entityName 实体名 
    	 */
    	fitRestoreUrl:function(entityName){
    		var arr={
    			'KnowledgeInfo':'knowledgeInfo!restoreData.action',
    			'Request':'request!restoreData.action',
    			'Problems':'problem!restoreData.action'
    			};
    		return arr[entityName];
    	},
        /**
         * @description 加载历史数据列表
         */
        showGrid: function () {
            var params = $.extend({},jqGridParams, {
                url: 'historyData!findHistoryDataPager.action',
                caption: i18n['label_historyData_grid'],
                colNames: [i18n['common_id'],i18n['common_title'],i18n['label_historyData_dataType'],i18n['label_historyData_deleteTime'],'',''],
                colModel: [
                           {name:'entityId',width:20,align: 'center'},
                           {name:'entityTitle',width:30,align:'center'},
                           {name: 'entityNamePanel',width:20,align:'center',formatter:common.config.historyData.historyDataGrid.entityNameFormtter,sortable:false},
                           {name:'createTime',width:30,align: 'center',formatter:timeFormatter},
                           {name: 'historyNo',hidden:true},
                           {name: 'entityName',hidden:true}
                           ],
                jsonReader: $.extend(jqGridJsonReader, {
                    id: "historyNo"
                }),
                sortname: 'historyNo',
                pager: '#historyDataGridPager'
            });
            $("#historyDataGrid").jqGrid(params);
            $("#historyDataGrid").navGrid('#historyDataGridPager', navGridParams);
            //列表操作项
            $("#t_historyDataGrid").css(jqGridTopStyles);
            $("#t_historyDataGrid").append($('#historyDataGridToolbar').html());
            //自适应宽度
            setGridWidth("#historyDataGrid", "regCenter", 10);
        },
        /**
         * 根据类型过滤数据.
         */
        showGridByEntityName:function(){
        	var tvalue=$('#historyDataGrid_type').val();   
			var postData = $("#historyDataGrid").jqGrid("getGridParam", "postData");
			$.extend(postData,{"qdto.entityName":tvalue});
			$('#historyDataGrid').trigger('reloadGrid',[{"page":"1"}]); 
        },
        /**
         * 彻底删除数据.
         */
        deleteHistoryData:function(){
        	checkBeforeDeleteGrid('#historyDataGrid',function(ids){
        	    var _param = $.param({'historyDataNos': ids}, true);
        		$.post('historyData!deleteHistoryData.action',_param,function(){
        			msgShow(i18n['msg_historyData_deleteSuccess'],'show');
        			$('#historyDataGrid').trigger('reloadGrid');
        		});
        	});
        },
        /**
         * 恢复历史数据.
         */
        restoreHistoryData:function(){
        	checkBeforeMethod('#historyDataGrid',function(ids){
        		msgConfirm(i18n['msg_msg'],'<br/>'+i18n['msg_confirm_restore'],function(){
        				$.each(ids,function(k,v){
        					var data=$('#historyDataGrid').getRowData(v);
        					var url=common.config.historyData.historyDataGrid.fitRestoreUrl(data.entityName);  
        					common.config.historyData.historyDataGrid.doRestoreData(url,v);
        				});
        		});
        	});
        },
        /**
         * 进行数据恢复
         * @param _url 访问Action
         * @param _id 历史数据Id
         */
        doRestoreData:function(_url,_id){
	  		var _param = $.param({'historyDataNos':_id}, true);
    		$.post(_url,_param,function(){
    			msgShow(i18n['msg_historyData_restoreSuccess'],'show');
    			$('#historyDataGrid').trigger('reloadGrid');
    		});
        	
        },
        /**
         * 初始化
         * @private 
         */
        init:function(){
        	$(".loading").hide();
        	$(".content").show();
        	common.config.historyData.historyDataGrid.showGrid();
        	$('#historyDataGrid_delete').click(common.config.historyData.historyDataGrid.deleteHistoryData);	
        	$('#historyDataGrid_restore').click(common.config.historyData.historyDataGrid.restoreHistoryData);
        	$('#historyDataGrid_type').change(common.config.historyData.historyDataGrid.showGridByEntityName);
        }
    }
}();
$(document).ready(common.config.historyData.historyDataGrid.init);