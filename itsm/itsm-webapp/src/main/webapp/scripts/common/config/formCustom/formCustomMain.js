$package('common.config.formCustom');
$import('common.config.category.serviceCatalog');
$import('common.config.category.CICategory');
$import('basics.tab.tabUtils');
$import('common.security.base64Util');
$import('itsm.cim.includes.includes');
common.config.formCustom.formCustomMain = function() {
	this.customForm = $('#customForm');
	this.opt;
	var formCustomType = "";
	return {
		/**
		 * @description 是否是默认数据显示格式化；
		 * @param cell 列显示值
		 * @param event 事件
		 * @param data 行数据
		 */
		isDefaultFormatter:function(cell,event,data){
			if(cell){
				return "<span style='color:#ff0000'>"+i18n.label_basicConfig_deafultCurrencyYes+"</span>";
			}else{
				return i18n.label_basicConfig_deafultCurrencyNo;
			}
		},
		nameFormatter:function(cell,event,data){
			if(data.type=="request"){
				return data.formCustomName;
			}else{
				return data.formCustomName+"("+data.ciCategoryName+")";
			}
		},
		typeFormatter:function(cell,event,data){
			if(data.type=="request"){
				return i18n.request;
			}else{
				return i18n.configureitem;
			}
		},
		/**
		 * @description 自定义表单列表.
		 */
		showFormCustomGrid : function() {
			var _url = "formCustom!findPagerFormCustom.action";
			if(formCustomType!=""){
				 _url = "formCustom!findPagerFormCustom.action?formCustomQueryDTO.type="+formCustomType;
			}
			var params = $.extend({}, jqGridParams, {
				url :_url,
				colNames : ['ID','',i18n.formName,i18n.whether_the_default_form,i18n.moduleName,,,'eavNo', i18n.extendedAttribute],
				colModel : [{
					name : 'formCustomId',
					align : 'center',
					width : 45
				}, {
					name : 'formCustomName',
					align : 'left',
					width : 220,
					hidden : true
				},
				{
					name : 'formCustomName',
					align : 'left',
					width : 220,
					formatter:common.config.formCustom.formCustomMain.nameFormatter
				},
				{name:'isDefault',width:60,align:'center',formatter:common.config.formCustom.formCustomMain.isDefaultFormatter},
				{name:'typeName',width:60,align:'center',formatter:common.config.formCustom.formCustomMain.typeFormatter},
				{name:'type',width:60,align:'center',hidden : true},
				{name:'ciCategoryNo',width:60,align:'center',hidden : true},
				{
					name : 'eavNo',
					hidden : true
				},{
					name : 'eavName',
					align : 'left',
					width : 220,
					index:'eavEntity',
					formatter : function(cellvalue, options, rowObject) {
						if (rowObject.eavName == null) {
							return "null";
						} else {
							return rowObject.eavName;
						}
					}
				}],
				toolbar : [true, "top"],
				jsonReader : $.extend(jqGridJsonReader, {
					id : "formCustomId"
				}),
				sortname : 'formCustomId',
				pager : '#formCustomGridPager'
			});
			$("#formCustomGrid").jqGrid(params);
			$("#formCustomGrid").navGrid('#formCustomGridPager', navGridParams);
			// 列表操作项
			$("#t_formCustomGrid").css(jqGridTopStyles);
			$("#t_formCustomGrid").append($('#formCustomGridToolbar').html());
			// 自适应宽度
			setGridWidth("#formCustomGrid", "regCenter", 20);
		},
		/**
		 * 添加
		 */
		addEditFormCustom_openwindow : function(){
			$('#formCustom_name').val('');
			$('#formCustom_eav').val('');
			$('#formCustom_ciCategoryName').val('');
			if($('#formCustom_module').val()=="request"){
				$('#formCustom_eav_tr').show();
				$('#formCustom_category_tr').hide();
				$('#formCustom_eav').removeAttr('disabled');
				$('#formCustom_ciCategoryName').attr('disabled','true');
			}else{
				$('#formCustom_eav_tr').hide();
				$('#formCustom_category_tr').show();
				$('#formCustom_ciCategoryName').removeAttr('disabled');
				$('#formCustom_eav').attr('disabled','true');
			}
			$('#searchVisibleState').click(function() {
				if ($('#searchVisibleState').attr("checked")) {
					$('#searchVisibleState').val(1);
				} else
					$('#searchVisibleState').val(0);
			});
			windows('customForm', {
				width : 450
			});
		},
		/**
		 * 执行添加动作前的数据传递
		 */
		addEditFormCustom_do : function(){
			var formCustom_module = $('#formCustom_module').val();
			var formCustomName = $("#formCustom_name").val();
			var _url = "formCustom!isFormCustomNameExisted.action";
			var param = $.param({'formCustomQueryDTO.type':formCustom_module,"formCustomQueryDTO.formCustomName":formCustomName},true);
			$.post(_url, param, function(bool){
				if(bool){
					if($("#customForm_form").form('validate')){
						formCustomName = encodeURI(encodeURI(formCustomName));
						if(formCustom_module=="request"){
							var form_eavNo = $("#formCustom_eav").val();
							var form_eavName = encodeURI(encodeURI($("#formCustom_eav").find("option:selected").text()));
							customForm.dialog('close');
							basics.tab.tabUtils.reOpenTab("../pages/common/config/formCustom/formCustomDesign.jsp?formCustomName="
									+formCustomName+"&formCustomEavNo="+form_eavNo+"&formCustomEavName="+form_eavName+"&fromCustomType="+$("#formCustom_module").val(),i18n.formDesigner);
						}else{
							var formCustom_ciCategoryNo = $("#formCustom_ciCategoryNo").val();
							var formCustom_ciCategoryName = encodeURI(encodeURI($("#formCustom_ciCategoryName").val()));
							var url="formCustom!findFormCustomByCiCategoryNo.action";
							var param = $.param({'formCustomDTO.ciCategoryNo':formCustom_ciCategoryNo},true);
							$.post(url, param, function(res){
								if(res==null){
									customForm.dialog('close');
									basics.tab.tabUtils.reOpenTab("../pages/common/config/formCustom/formCustomDesign.jsp?formCustomName="
											+formCustomName+"&formCustom_ciCategoryNo="+formCustom_ciCategoryNo+"&formCustom_ciCategoryName="+formCustom_ciCategoryName+"&fromCustomType="+$("#formCustom_module").val(),i18n.formDesigner);
								}else{
									msgAlert(i18n.msg_formCustomCiCategoryExist,'info');
								}
							});
							
						}
					}
				}else{
					msgAlert(i18n.msg_formCustomNameExist,"info");
				}
			});
			
		},
		/**
		 * 编辑
		 */
		editFormCustom_aff : function(){
			checkBeforeEditGrid('#formCustomGrid', common.config.formCustom.formCustomMain.showEditFormCustom);
		},
		/**
		 * @description 自定义表单编辑打开
		 * @param rowData 要编辑行的数据对象
		 */
		showEditFormCustom:function(rowData){
			if(rowData.type=="request"){
				basics.tab.tabUtils.reOpenTab("../pages/common/config/formCustom/formCustomDesign.jsp?formCustomId="+rowData.formCustomId+"&formCustomEavNo="+rowData.eavNo+"&fromCustomType="+rowData.type,i18n.formDesigner);
			}else{
				basics.tab.tabUtils.reOpenTab("../pages/common/config/formCustom/formCustomDesign.jsp?formCustomId="+rowData.formCustomId+"&formCustom_ciCategoryNo="+rowData.ciCategoryNo+"&fromCustomType="+rowData.type,i18n.formDesigner);
			}
		},
		
		/**
		 * @description 删除自定义表单
		 */
		deleteFormCustom_aff : function() {
			checkBeforeDeleteGrid('#formCustomGrid', common.config.formCustom.formCustomMain.formCustom_deleteFormCustom);
		},

		/**
		 * @description 删除自定义表单
		 */
		deleteFormCustom : function(formCustomId) {
			msgConfirm(i18n['msg_msg'], '<br/>' + i18n['msg_confirmDelete'], function() {
				common.config.formCustom.formCustomMain.formCustom_deleteFormCustom(formCustomId);
			});
		},

		/**
		 * @description 执行删除.
		 * @param rowIds 任务id数组
		 */
		formCustom_deleteFormCustom : function(rowIds) {
			var url="formCustom!deleteFormCustom.action";
			var param = $.param({'formCustomIds':rowIds},true);
			$.post(url, param, function(){
				//重新统计				
				$('#formCustomGrid').trigger('reloadGrid');
				msgShow(i18n.msg_deleteSuccessful,'show');
				itsm.request.requestStats.loadRequestCount();
			}, "json");	
		},
		/**
		 * @description 打开搜索窗口.
		 */
		search_openwindow : function() {

			$('#searchVisibleState').click(function() {
				if ($('#searchVisibleState').attr("checked")) {
					$('#searchVisibleState').val(1);
				} else
					$('#searchVisibleState').val(0);
			});

			windows('searchFormCustom', {
				width : 450
			});
		},
		selectFormCustomModule:function(value){
			if(value=="request"){
				$('#formCustom_eav_tr').show();
				$('#formCustom_category_tr').hide();
				$('#formCustom_eav').removeAttr('disabled');
				$('#formCustom_ciCategoryName').attr('disabled','true');
			}else{
				$('#formCustom_eav_tr').hide();
				$('#formCustom_category_tr').show();
				$('#formCustom_ciCategoryName').removeAttr('disabled');
				$('#formCustom_eav').attr('disabled','true');
			}
		},
		/**
		 * @description 提交查询自定义表单
		 */
		search_do : function() {
			if ($('#searchFormCustom').form('validate')) {
				var sdata = $('#searchFormCustom form').getForm();
				var postData = $("#formCustomGrid").jqGrid("getGridParam", "postData");
				$.extend(postData, sdata); // 将postData中的查询参数加上查询表单的参数
				$('#formCustomGrid').trigger('reloadGrid', [{
					"page" : "1"
				}]);
				return false;
			}
		},
		searchByformCustomType:function() {
			formCustomType = $("#formCustomTypeSelect").val();
			var _url = "formCustom!findPagerFormCustom.action";
			if(formCustomType!=""){
				 _url = _url+"?formCustomQueryDTO.type="+formCustomType;
			}
			$('#formCustomGrid').jqGrid('setGridParam',{postData:null,page:1,url:_url}).trigger('reloadGrid');
			
			if(formCustomType=="ci"){
				$("#formCustomGrid").setGridParam().hideCol("isDefault").trigger("reloadGrid");
			}else{
				$("#formCustomGrid").setGridParam().showCol("isDefault").trigger("reloadGrid");
			}
		},
		init : function() {
			formCustomType = $("#formCustomTypeSelect").val();
			// 绑定日期控件
			DatePicker97(['#searchTaskStartTime', '#searchTaskEndTime']);
			$("#formCustom_loading").hide();
			$("#formCustom_content").show();
			itsm.cim.includes.includes.loadSelectCIIncludesFile();
			common.config.formCustom.formCustomMain.showFormCustomGrid();
			$('#link_formCustom_search_ok').click(common.config.formCustom.formCustomMain.search_do);
			$('#editFormCustomBtn').click(common.config.formCustom.formCustomMain.editFormCustom_aff);
			$('#save_formCustom_info').click(common.config.formCustom.formCustomMain.addEditFormCustom_do);
			common.config.category.CICategory.loadEavsToSelect("#formCustom_eav");//加载列表
			$("#formCustomTypeSelect").change(common.config.formCustom.formCustomMain.searchByformCustomType);
		}
	}
}();
$(document).ready(common.config.formCustom.formCustomMain.init);
