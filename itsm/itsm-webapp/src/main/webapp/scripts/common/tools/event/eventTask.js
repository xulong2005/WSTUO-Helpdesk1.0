/**  
 * @fileOverview eventTask
 * @author QXY
 * @version 1.0  
 */  
 /**  
 * @author QXY  
 * @constructor eventTask
 * @description eventTask
 * @date 2011-05-19
 * @since version 1.0 
 */

$import('itsm.portal.schedule');
$import('itsm.portal.costCommon');

function eventTask(eno,eventType){
	this.calendarTaskDiv = $('#calendarTaskDiv');
	this.gridId='#'+eventType+'EventTaskGrid';
	this.t_gridId='#t_'+eventType+'EventTaskGrid';
	this.pagerId='#'+eventType+'EventTaskGridPager';
	this.toolBarId='#'+eventType+'EventTaskGridToolbar';
	this.actFormatter='#'+eventType+'EventTaskActFormatterDiv';
	this.eventTaskFormId='#'+eventType+'AddOrEditEventTaskForm';
	this.eventTaskWinId='#'+eventType+'AddOrEditEventTaskDiv';
	this.eno=eno;
	this.eventType='itsm.'+eventType;
	this.eventTaskEnoId='#'+eventType+'_task_eno';
	this.opt;
	var event = this;
	this.requestTaskActFormatter=function(cell,opt,data){
		return $('#'+eventType+'EventTaskActFormatterDiv').html();
	};
	/**
	 * 状态格式化.
	 */
	this.requestTaskStateFormat=function(cellvalue, options){
		if(cellvalue=='0')
			return i18n['title_newCreate'];
		if(cellvalue=='1')
			return i18n['task_label_pending'];
		if(cellvalue=='2')
			return i18n['lable_complete'];
	};
	/**
	 * @description 任务列表
	 */
	this.eventTaskGrid=function(tool_show_hide){
		var actFormatters = this.actFormatter;
		var params ;
		if(tool_show_hide){
			params= $.extend({},jqGridParams, {	
				url:'eventTask!findPagerChangeTask.action?queryDTO.eno='+this.eno+'&queryDTO.eventType='+this.eventType,
				colNames:['ID',i18n['title'],i18n['lable_task_location'],i18n['status'],'',i18n['title_startTime'],i18n['title_endTime'],i18n['common_owner'],i18n['operateItems'],'','','',''],
				colModel:[
				    {name:'taskId',align:'left',width:45},
				    {name:'etitle',index:'title',align:'left',sortable:false,width:220,formatter:function(cellvalue, options, rowObject){
			   			if(rowObject.etitle==null){
							return "null";
						}else{
							return rowObject.etitle;
						}
					}},
			   		{name:'location',align:'left',width:110},
			   		{name:'taskStatus',align:'center',width:80,formatter:this.requestTaskStateFormat,sortable:false},
			   		{name:'taskStatus',width:150,hidden:true},
			   		{name:'startTime',align:'center',width:130,formatter:timeFormatter},
			   		{name:'endTime',align:'center',width:130,formatter:timeFormatter},
			   		{name:'owner',align:'center', width:120},
			   		{name:'act', width:80,align:'center',sortable:false,formatter:function(cell,event,data){
	             	   return $(actFormatters).html().replace(/{taskId}/g,event.rowId);
	                }},
			   		{name:'introduction',hidden:true},
			   		{name:'allDay',hidden:true},
			   		{name:'eno',hidden:true},
			   		{name:'eventType',hidden:true}
				],	
				toolbar: [tool_show_hide,"top"],
				jsonReader: $.extend(jqGridJsonReader,{id: "taskId"}),
				ondblClickRow:function(rowId){event.portalShowMyTask1(rowId)},
				sortname:'taskId',
				pager:this.pagerId
			});
		}else{
			params= $.extend({},jqGridParams, {	
				url:'eventTask!findPagerChangeTask.action?queryDTO.eno='+this.eno+'&queryDTO.eventType='+this.eventType,
				colNames:['ID',i18n['title'],i18n['lable_task_location'],i18n['status'],'',i18n['title_startTime'],i18n['title_endTime'],i18n['common_owner'],'','','',''],
				colModel:[
				    {name:'taskId',align:'left',width:45},
				    {name:'etitle',index:'title',align:'left',width:220},
			   		{name:'location',align:'left',width:110},
			   		{name:'taskStatus',align:'center',width:80,formatter:this.requestTaskStateFormat,sortable:false},
			   		{name:'taskStatus',width:150,hidden:true},
			   		{name:'startTime',align:'center',width:130,formatter:timeFormatter},
			   		{name:'endTime',align:'center',width:130,formatter:timeFormatter},
			   		{name:'owner',align:'center', width:120},
			   		{name:'introduction',hidden:true},
			   		{name:'allDay',hidden:true},
			   		{name:'eno',hidden:true},
			   		{name:'eventType',hidden:true}
				],	
				toolbar: [tool_show_hide,"top"],
				jsonReader: $.extend(jqGridJsonReader,{id: "taskId"}),
				ondblClickRow:function(rowId){event.portalShowMyTask1(rowId)},
				sortname:'taskId',
				pager:this.pagerId
			});
		}
			
		$(this.gridId).jqGrid(params);
		$(this.gridId).navGrid(this.pagerId,navGridParams);
		//列表操作项
		$(this.t_gridId).css(jqGridTopStyles);
		$(this.t_gridId).append($(this.toolBarId).html());
		if(tool_show_hide){
			$(this.gridId).jqGrid('showCol', 'cb');
		}else{
			$(this.gridId).jqGrid('hideCol', 'cb');
		}
		//自适应大小
//		setGridWidth(this.gridId,"regCenter",20);
	};
	this.portalShowMyTask1=function(id){
		var rowData = $(this.gridId).jqGrid('getRowData',id);
		itsm.portal.costCommon.scheduleShow2Init('false');
		itsm.portal.schedule.taskShow1Init( eventType );
		$('#schedule_task_edit_but').unbind().click(function(){
			event.editEventTask_win(rowData);
		});
		itsm.portal.costCommon.scheduleShow1(rowData.taskId,rowData.startTime,rowData.endTime);
	};
	/**
	 * @description 显示id
	 */
	this.portalShowMyTask=function(id){
		var postUrl="tasks!findByTaskId.action";
		var param="taskId="+id;
		$.post(postUrl,param,function(taskDto){
			$("#addcalendarTreatmentResultsTr").attr("style","display:none;");
			itsm.portal.schedule.historyShow(id);
			$('#schedule_task_process_but').show();
			$('#schedule_task_closed_but').show();
			$('#schedule_task_remark_but').show();
			$('#calendarResults_tr').hide();
			$('#calendarTask_taskId').val(id);
			if(taskDto.etitle!=null){
				$('#calendarTaskTitle').text(taskDto.etitle);
			}else{
				$('#calendarTaskTitle').text("null");
			}
			$('#calendarTaskLocation').text(taskDto.location);
			$('#calendarTaskIntroduction').text(taskDto.introduction);
			$('#calendarTaskCreator').text(taskDto.creator);
			$('#calendarTaskOwner').text(taskDto.owner);
			$('#calendarTaskStartTime').text(timeFormatter(taskDto.startTime));
			$('#calendarTaskEndTime').text(timeFormatter(taskDto.endTime));
			$('#calendarRealStartTime').text(taskDto.realStartTime);
			$('#calendarRealEndTime').text(taskDto.realEndTime);
			$('#calendarRealFree').text(Math.floor(taskDto.realFree*1/1440)+i18n.label_slaRule_days+" "+Math.floor(taskDto.realFree*1%1440/60)+i18n.label_slaRule_hours+" "+Math.floor(taskDto.realFree*1%1440%60)+i18n.label_slaRule_minutes);
			if (taskDto.taskStatus == '0') {
				$('#calendarTaskStatus').text(i18n['title_newCreate']);
				$('#schedule_task_closed_but').hide();
			}
			if (taskDto.taskStatus == '1') {
				$('#calendarTaskStatus').text(i18n['task_label_pending']);
				$('#schedule_task_process_but').hide();
			}
			if (taskDto.taskStatus == '2') {
				$('#calendarTaskStatus').text(i18n['lable_complete']);
				$('#schedule_task_process_but').hide();
				$('#schedule_task_closed_but').hide();
				$('#calendarResults_tr').show();
				$('#calendarResults').text(taskDto.treatmentResults);
			}
			if (taskDto.allDay == 'true' || taskDto.allDay) {
				$('#calendarTaskAllDay').text(i18n['label_basicConfig_deafultCurrencyYes']);
			} else {
				$('#calendarTaskAllDay').text(i18n['label_basicConfig_deafultCurrencyNo']);
			}
			
			if(taskDto.ownerLoginName!=userName){
				$('#schedule_task_process_but').hide();
				$('#schedule_task_closed_but').hide();
			}
			$('#schedule_task_delete_but').hide();
			$('#schedule_task_edit_but').hide();
			$('#schedule_task_process_but').unbind().click(function(){event.processCalendarTask(id);});
			$('#schedule_task_closed_but').unbind().click(function(){event.closedCalendarTask_opt(id);});
			$('#schedule_task_remark_but').unbind().click(function(){event.remarkCalendarTask_opt(id);});
			windows('calendarTaskDiv',{width:600,modal: true});
		});
	}
	/**
	 * @description 查询任务
	 */
	this.processCalendarTask=function(id){
		var _url="tasks!editTaskDetail.action";
		$.post(_url,'taskDto.taskId='+id+"&taskDto.taskStatus="+1+"&taskDto.operator="+userName,function(){
			calendarTaskDiv.dialog('close');
			msgShow(i18n.lable_startProcess,"show");
			$('#'+eventType+'EventTaskGrid').trigger('reloadGrid');
			$('#taskGrid').trigger('reloadGrid');
			event.portalShowMyTask(id);
		});
	}
	/**
	 * @description 打开关闭任务面板
	 */
	this.closedCalendarTask_opt=function(id){
		$.post("tasks!taskRealTimeDefaultValue.action",'taskId='+id,function(taskDto){
			calendarTaskDiv.dialog('close');
			resetForm("#realProcessTaskfm");
			$('#schedule_process_taskId').val(id);
			$('#schedule_process_realStartTime').val(taskDto.realStartTime);
			$('#schedule_process_realEndTime').val(taskDto.realEndTime);
			$('#schedule_process_taskCostDay').val(Math.floor(taskDto.realFree*1/1440));
			$('#schedule_process_taskCostHour').val(Math.floor(taskDto.realFree*1%1440/60));
			$('#schedule_process_taskCostMinute').val(Math.floor(taskDto.realFree*1%1440%60));
			$("#schedule_process_realFree").val(taskDto.realFree);
			bindControl('#schedule_process_realEndTime,#schedule_process_realStartTime');
			$('#schedule_link_task_closedTask').unbind().click(function(){event.closedCalendarTask();})
			windows('realProcessTaskDiv',{width:600});
		});
		
	}
	/**
	 * @description 关闭任务
	 */
	this.closedCalendarTask=function(){
		var _url="tasks!closedTaskDetail.action";
		if ($('#realProcessTaskfm').form('validate')) {
			var _param=$('#realProcessTaskDiv form').serialize();
			if(($('#schedule_process_realEndTime').val()!="" && $('#schedule_process_realStartTime').val() != "" && $('#schedule_process_realFree').val()>=1)){
				startProcess();
				$.post(_url,_param+"&taskDto.taskStatus="+2,function(){
					calendarTaskDiv.dialog('close');
					msgShow(i18n.lable_process_closedTask,"show");
					$('#realProcessTaskDiv').dialog('close');
					var taskId = $('#schedule_process_taskId').val();
					$('#'+eventType+'EventTaskGrid').trigger('reloadGrid');
					event.portalShowMyTask(taskId);
					$('#taskGrid').trigger('reloadGrid');
					endProcess(); 
				});
			}else{
				msgAlert(i18n.labe_taskClosedMsg,'info');
			}
		}
		
	}
	/**
	 * @description 打开任务备注
	 */
	this.remarkCalendarTask_opt=function(id){
		$("#addcalendarTreatmentResultsTr").removeAttr("style");
		$("#schedule_link_task_remarkTask").unbind().click(function(){event.remarkCalendarTask(id);});
	}
	/**
	 * @description 备注任务
	 */
	this.remarkCalendarTask=function(id){
		var _url="tasks!remarkTaskDetail.action";
		var treatmentResults = $("#addcalendarTreatmentResults").val();
		startProcess();
		$.post(_url,'taskDto.taskId='+id+"&taskDto.treatmentResults="+treatmentResults+"&taskDto.operator="+userName,function(){
			$("#addcalendarTreatmentResultsTr").attr("style","display:none;");
			$("#addcalendarTreatmentResults").val("");
			calendarTaskDiv.dialog('close');
			event.portalShowMyTask(id);
			endProcess(); 
		});
	}
	
	/**
	 * @description 保存任务
	 */
	this.saveEventTask_opt=function(){
		if($('#index_addEditTask_form').form('validate')){
			startProcess();
			if(opt=='saveEventTask'){
				$("#index_eventTaskDTO_taskId").val('');
				$('#index_eventTaskDTO_realFree').val("");
			}
			var _param = $('#index_addEditTask_form').serialize();
//			+
//			'&eventTaskDTO.startTime='+$('#index_eventTaskDTO_startTime').val()+
//			'&eventTaskDTO.endTime='+$('#index_eventTaskDTO_endTime').val();
			var _url='eventTask!'+opt+'.action';
			$.post('eventTask!timeConflict.action',_param,function(data){
				if(data){
					msgConfirm(i18n['msg_msg'],'<br/>'+i18n['timeConflictTip'],function(){
						$.post(_url,_param,function(){
							$('#index_addEditTask_window').dialog('close');
				 			$('#'+eventType+'EventTaskGrid').trigger('reloadGrid');
				 			msgShow(i18n['saveSuccess'],'show');
				 			endProcess();
						})
					});
				}else{
					$.post(_url,_param,function(){
						$('#index_addEditTask_window').dialog('close');
			 			$('#'+eventType+'EventTaskGrid').trigger('reloadGrid');
			 			msgShow(i18n['saveSuccess'],'show');
			 			endProcess();
					})
				}
			});
			/*}else{
				$.post('eventTask!timeConflict.action',_param,function(data){
					if(data){
						msgConfirm(i18n['msg_msg'],'<br/>'+i18n['timeConflictTip'],function(){
							$.post(_url,_param,function(){
								$('#index_addEditTask_window').dialog('close');
					 			$('#'+eventType+'EventTaskGrid').trigger('reloadGrid');
					 			msgShow(i18n['saveSuccess'],'show');
							})
						});
					}else{
						$.post(_url,_param,function(){
							$('#index_addEditTask_window').dialog('close');
				 			$('#'+eventType+'EventTaskGrid').trigger('reloadGrid');
				 			msgShow(i18n['saveSuccess'],'show');
						})
					}
					endProcess();
				})
			}*/
		}
	};
	
	
	/**
	 * @description 新增
	 */
	this.addEventTask_win=function(){
		opt='saveEventTask';
		$('#index_eventTaskDTO_eno').removeAttr("disabled");
		$('#index_eventTaskDTO_taskTitle').removeAttr("disabled");
		$('#index_eventTaskDTO_taskLocation').removeAttr("disabled");
		$('#index_eventTaskDTO_introduction').removeAttr("disabled");
		$('#index_eventTaskDTO_allDay').removeAttr("disabled");
		$('#index_eventTaskDTO_startTime').removeAttr("disabled");
		$('#index_eventTaskDTO_endTime').removeAttr("disabled");
		$('#index_eventTaskDTO_owner').removeAttr("disabled");
		$('#index_eventTaskDTO_owner_fullName').removeAttr("disabled");
		$('#index_eventTaskDTO_creator').removeAttr("disabled");
		$("#realStartTimeTr").attr("style","display:none;");
		$("#realEndTimeTr").attr("style","display:none;");
		$("#realFreeTr").attr("style","display:none;");
		$('#eventTaskStatusTr').attr("style","display:none;");
		$('#eventTaskTreatmentResultsTr').attr("style","display:none;");
		$("#index_eventTaskDTO_owner").val("");
		resetForm('#index_addEditTask_form');
		$('#index_eventTaskDTO_eno').val(eno);
		$('#index_eventTaskDTO_eventType').val(this.eventType);
		$('#index_eventTaskDTO_owner_fullName').unbind("click").click(function(){
			common.security.userUtil.selectUserMultiRule('#index_eventTaskDTO_owner_fullName','#index_eventTaskDTO_owner','Tesk',topCompanyNo);
		});
		
		windows('index_addEditTask_window',{width:630,close:function(){
			$('#index_addEditTask_window').click();
		}});
	};
	/**
	 * @description 编辑
	 */
	this.editEventTask_aff=function(){
		checkBeforeEditGrid(this.gridId,this.editEventTask_win);
	};
	
	/**
	 * @description 关打开编辑任务面板
	 */
	this.editEventTask_win=function(rowData){
		$.post("eventTask!findById.action",'eventTaskId='+rowData.taskId,function(res){
			if(res.creatorLoginName==userName || operationTaskRes){
				opt="editEventTask";
				$('#index_eventTaskDTO_eventType').val(res.eventType);
				$('#index_eventTaskDTO_taskId').val(res.taskId);
				$('#index_eventTaskDTO_eno').val(res.eno);
				if(res.etitle!=null){
					$('#index_eventTaskDTO_taskTitle').val(res.etitle);
				}else{
					$('#index_eventTaskDTO_taskTitle').val("null");
				}
				
				$('#index_eventTaskDTO_taskLocation').val(res.location);
				$('#index_eventTaskDTO_introduction').val(res.introduction);
				if(res.taskStatus=='0'){
					$('#index_eventTaskDTO_statusNew').attr("checked",'true')
				}
				if(res.taskStatus=='1'){
					$('#index_eventTaskDTO_statusProcessing').attr("checked",'false');
				}
				if(res.taskStatus=='2'){
					$('#index_eventTaskDTO_statusDone').attr("checked",'false');
				}
				if(res.allDay=='true' || res.allDay){
					$('#index_eventTaskDTO_allDay').attr("checked",'ture');
				}else{
					$('#index_eventTaskDTO_allDay').attr("checked",'');	
				}
				$('#index_eventTaskDTO_startTime').val(res.startTime);
				$('#index_eventTaskDTO_endTime').val(res.endTime);
				$('#index_eventTaskDTO_realStartTime').val(res.realStartTime);
				$('#index_eventTaskDTO_realEndTime').val(res.realEndTime);
				$('#index_eventTaskDTO_taskCostDay').val(Math.floor(res.realFree*1/1440));
				$('#index_eventTaskDTO_taskCostHour').val(Math.floor(res.realFree*1%1440/60));
				$("#index_eventTaskDTO_taskCostMinute").val(Math.floor(res.realFree*1%1440%60));
				$('#index_eventTaskDTO_realFree').val(res.realFree);
				$('#index_eventTaskDTO_owner').val(res.ownerLoginName);
				$('#index_eventTaskDTO_owner_fullName').val(res.owner);
				$('#index_eventTaskDTO_creator').val(res.creatorLoginName);
				$('#index_eventTaskDTO_treatmentResults').val(res.treatmentResults);
				$('#index_eventTaskDTO_owner_fullName').unbind("click");
				$('#index_eventTaskDTO_owner_fullName').click(function(){
					common.security.userUtil.selectUser('#index_eventTaskDTO_owner_fullName','#index_eventTaskDTO_owner','','EditTesk',topCompanyNo,'');
				});
				windows('index_addEditTask_window',{width:630,close:function(){
					$('#index_addEditTask_window').click();
				}});
			}else{
				msgAlert(i18n.error403,'info');
			}
			
		});
	};
	/**
	 * @description 编辑
	 */
	this.editEventTask_affs=function(rowId,et){
		var rowData= $(et.gridId).jqGrid('getRowData',rowId);  // 行数据  
		et.editEventTask_win(rowData);
	};
	
	
	/**
	 * @description 删除任务时获取id
	 */
	this.deleteEventTask_aff=function(){
		checkBeforeDeleteGrid(this.gridId,this.deleteEventTask);
	};
	/**
	 * @description 删除任务
	 */
	this.deleteEventTask_affs=function(rowId,et){
		msgConfirm(i18n['msg_msg'],'<br/>'+i18n['msg_confirmDelete'],function(){
			et.deleteEventTask(rowId);
		});
	};
	/**
	 * @description 删除
	 */
	this.deleteEventTask=function(rowIds){
		var _param = $.param({'ids':rowIds},true);
		$.post("eventTask!findTaskByIds.action", _param, function(res){
			if(res || operationTaskRes){
				$.post("eventTask!deleteEventTask.action", _param, function(){
					$('#'+eventType+'EventTaskGrid').trigger('reloadGrid');
					msgShow(i18n['deleteSuccess'],'show');
				}, "json");
			}else{
				msgAlert(i18n.error403,'info');
			}
		});
	};
	
	
	/**
	 * @description 选择关联任务列表
	 */
	this.relatedEventTaskGrid=function(){
		windows(eventType+'_relatedEventTaskGrid_win',{width:600});
		if($('#'+eventType+'_relatedEventTaskGrid').html()==''){
			var params = $.extend({},jqGridParams, {	
				url:'eventTask!findPagerChangeTask.action?queryDTO.eno='+this.eno+'&queryDTO.eventType='+this.eventType,
				caption:'',
				colNames:['ID',i18n['title'],i18n['status'],'',i18n['title_startTime'],i18n['title_endTime'],i18n['common_owner'],i18n['operateItems']],
				colModel:[
				    {name:'taskId',align:'left',width:10,align:'center'},
			   		{name:'etitle',index:'title',align:'left',width:20},
			   		{name:'taskStatus',width:10,formatter:this.relatedEventTaskStateFormat,sortable:false},
			   		{name:'taskStatus',width:10,hidden:true},
			   		{name:'startTime',align:'center',width:15,hidden:true},
			   		{name:'endTime',align:'center',width:15,hidden:true},
			   		{name:'owner',align:'center', width:15},
			   		{name:'act', width:15,sortable:false,align:'center',formatter:this.relatedEventTaskAct}
				],	
				toolbar:false,
				multiselect:false,
				jsonReader: $.extend(jqGridJsonReader,{id: "taskId"}),
				sortname:'taskId',
				ondblClickRow:function(rowId){select_eventTask_aff(eventType)},
				pager:'#'+eventType+'_relatedEventTaskGridPager'
			});
			$('#'+eventType+'_relatedEventTaskGrid').jqGrid(params);
			$('#'+eventType+'_relatedEventTaskGrid').navGrid('#'+eventType+'_relatedEventTaskGridPager',navGridParams);
		}else{
			$('#'+eventType+'_relatedEventTaskGrid').trigger('reloadGrid');
		}
		
	};
	this.relatedEventTaskStateFormat=function(cellvalue, options){
		if(cellvalue=='0')
			return i18n['title_newCreate'];
		if(cellvalue=='1')
			return i18n['task_label_pending'];
		if(cellvalue=='2')
			return i18n['lable_complete'];
	};
	
	this.relatedEventTaskAct=function(){
		return '<a href=javascript:select_eventTask_aff("'+eventType+'") title="'+i18n['check']+'"><img src="../images/icons/ok.png" /></a>';
	};
}
function select_eventTask_aff(eventTypeId){
	var rowId = $('#'+eventTypeId+'_relatedEventTaskGrid').getGridParam('selrow');
	if(rowId==null){
		msgAlert(i18n['msg_atLeastChooseOneData'],'info');
	}else{
		var rowData=$('#'+eventTypeId+'_relatedEventTaskGrid').getRowData(rowId);
		confirmSelect(eventTypeId,rowData.taskId,rowData.etitle);
	}
}

function confirmSelect(eventTypeId,taskId,title){
	$('#index_timeCost_costTaskId').val(taskId);
	$('#index_timeCost_costTaskTitle').val(title);
	$('#'+eventTypeId+'_relatedEventTaskGrid_win').dialog('close');
}

function saveEventTask(_url,_param,eventType){
	$.post(_url,_param,function(){
		$('#index_addEditTask_window').dialog('close');
		$('#'+eventType+'EventTaskGrid').trigger('reloadGrid');
		msgShow(i18n['saveSuccess'],'show');
	});
}

