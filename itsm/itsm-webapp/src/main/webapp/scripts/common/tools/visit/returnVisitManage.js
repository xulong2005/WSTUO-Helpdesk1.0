$package("common.tools.visit");
/**  
* @fileOverview 回访管理
* @author QXY
* @version 1.0
*/  
/**  
* @author QXY  
* @constructor Return Visit  
* @description 回访管理
* @since version 1.0  
*/
common.tools.visit.returnVisitManage=function(){
	return{
		
		/**
		 * @description 满意度
		 */
		satisfactionForma:function(cell){
			if(cell==5)
				return  i18n['label_returnVisit_verysatisfied']
			if(cell==4)
				return  i18n['label_returnVisit_satisfied']
			if(cell==3)
				return  i18n['label_returnVisit_general']
			if(cell==2)
				return  i18n['label_returnVisit_dissatisfied']
			if(cell==1)
				return  i18n['label_returnVisit_verydissatisfied']
			if(cell==0)
				return  ""
	    }, 
	    /**
		 * @description 状态
		 */
		stateForma:function(cell){
			if(cell==0)
				return  i18n['label_returnVisit_waitReply']
			if(cell==1)
				return  i18n['label_returnVisit_alreadyReply']
	    },
	    requestDetailForma:function(cellvalue){
	    	return '<a href="javascript:common.tools.visit.returnVisitManage.requestDetailVerification('+cellvalue+')" >['+i18n['request_detail']+']</a>';;
	    },
	    returndetailForma:function(cellvalue){
	    	return '<a href="javascript:common.tools.visit.returnVisitManage.viewUserReturnVisitDetail('+cellvalue+')" >['+i18n['return_detail']+']</a>';;
	    },
	    /**
	     * 验证请求eno是否正确
	     */
	    requestDetailVerification:function(cellvalue){
	    	var result='';
	    	$.post('request!findRequestById.action',{'requestQueryDTO.eno':cellvalue},function(data){
	    		if(data.eno!=null && data.eno !=""){
	    			basics.tab.tabUtils.reOpenTab('request!requestDetails.action?eno='+cellvalue,i18n['request_detail']);
	    		}else{
	    			msgAlert(i18n['requestNotExist'],'error');
	    		}
	    	});
	    },
		/**
		 * @description 列表
		 */
		returnVisitGrid:function(){
		
			var _url='userReturnVisit!findPagerReturnVisit.action?queryDTO.companyNo=-1'
		 	
			var _postData={};
		
			var params = $.extend({},jqGridParams, {	
				url:_url,
				postData:_postData,
				colNames:['ID',i18n['label_returnVisit_satisfaction'],i18n['label_returnVisit_state'],i18n['label_returnVisit_sendTime'],i18n['label_returnVisit_replyTime'],i18n['label_returnVisit_object'],i18n['label_returnVisit_user'],i18n['return_detail'],i18n['request_detail']],
				colModel:[
			   		{name:'visitId',align:'center',width:25,sortable:true},
			   		{name:'satisfaction',align:'center',index:'satisfaction',width:80,formatter:common.tools.visit.returnVisitManage.satisfactionForma},
			   		{name:'state',align:'center',width:150,formatter:common.tools.visit.returnVisitManage.stateForma},
			   		{name:'returnVisitSubmitTime',align:'center',width:150,formatter:timeFormatter},
			   		{name:'returnVisitTime',align:'center',width:150,formatter:timeFormatter},
			   		{name:'returnVisitUser',align:'center',width:150},
			   		{name:'returnVisitSubmitUser',align:'center',width:150},
			   		{name:'visitId',sortable:false,align:'center',width:100,formatter:common.tools.visit.returnVisitManage.returndetailForma},
			   		{name:'eno',sortable:false,align:'center',width:100,formatter:common.tools.visit.returnVisitManage.requestDetailForma}
			   	],
			   	ondblClickRow:function(rowId){common.tools.visit.returnVisitManage.viewUserReturnVisitDetail(rowId)},
				jsonReader: $.extend(jqGridJsonReader, {id: "visitId"}),
				sortname:'visitId',
				pager:'#returnVisitPager'
			});
			$("#returnVisitGrid").jqGrid(params);
			$("#returnVisitGrid").navGrid('#returnVisitPager',navGridParams);
			//列表操作项
			$("#t_returnVisitGrid").css(jqGridTopStyles);
			$("#t_returnVisitGrid").append($('#returnVisitGridToolbar').html());
		},
		/**
		 * @description 删除时获取id
		 */
		userReturnVisit_delete_aff:function(){
			checkBeforeDeleteGrid('#returnVisitGrid', common.tools.visit.returnVisitManage.deleteUserReturnVisit);
		},
		/**
		 * @description 删除
		 */
		deleteUserReturnVisit:function(rowIds){
			var _param = $.param({'ids':rowIds},true);
			startProcess();
			$.post("userReturnVisit!deleteUserReturnVisit.action", _param, function(){
				endProcess();
				$('#returnVisitGrid').trigger('reloadGrid');
				msgShow(i18n['deleteSuccess'],'show');
			}, "json");
		},
		/**
		 * @description 搜索
		 */
		openSearchWin:function(){
			windows('userReturnVisit_search_win',{width: 450});
		},
		/**
		 * @description 搜索
		 */
		doSearch:function(){
			if($('#userReturnVisit_search_win form').form('validate')){
				var _url = 'userReturnVisit!findPagerReturnVisit.action';	
				var sdata = $('#userReturnVisit_search_win form').getForm();
				var postData = $("#returnVisitGrid").jqGrid("getGridParam", "postData");     
				$.extend(postData, sdata);
				$('#returnVisitGrid').jqGrid('setGridParam',{page:1,url:_url}).trigger('reloadGrid');
			}
		},
		/**
		 * @description 导出
		 */
		exportData:function(){
			$('#userReturnVisit_search_win form').submit();
		},
		/**
		 * @description  预览
		 */
		viewUserReturnVisitDetail:function(id){
			$.post('userReturnVisit!findUserReturnVisitById.action','queryDTO.visitId='+id,function(data){
				$('#userReturnVisit_detail_satisfaction').text(common.tools.visit.returnVisitManage.satisfactionForma(data.satisfaction));
				var reg=new RegExp("<hr>","g"); //创建正则RegExp对象 
				$('#userReturnVisit_detail_returnVisitDetail').html((data.returnVisitDetail).replace(reg,''));
				$('#userReturnVisit_detail_state').text(common.tools.visit.returnVisitManage.stateForma(data.state));
				$('#userReturnVisit_detail_returnVisitSubmitTime').text(timeFormatter(data.returnVisitSubmitTime));
				$('#userReturnVisit_detail_returnVisitTime').text(timeFormatter(data.returnVisitTime));
				$('#userReturnVisit_detail_returnVisitUser').text(data.returnVisitUser);
				$('#userReturnVisit_detail_returnVisitAssignee').text(data.returnVisitTechnicianName);
				$('#userReturnVisit_detail_returnVisitSubmitUser').text(data.returnVisitSubmitUser);
				windows('userReturnVisit_detail_win',{width: 400});
			});
		},
		
		/**
		 * 初始化
		 */
		init:function(){
			//绑定日期控件
			DatePicker97(['#search_returnVisitSubmitTimeStart','#search_returnVisitSubmitTimeEnd','#search_returnVisitTimeStart','#search_returnVisitTimeEnd']);
			$("#returnVisitManage_loading").hide();
			$("#returnVisitManage_content").show();
			common.tools.visit.returnVisitManage.returnVisitGrid();
			$('#userReturnVisit_search_but').click(common.tools.visit.returnVisitManage.openSearchWin);//搜索 
			$('#doSearchReturnVisitBut').click(common.tools.visit.returnVisitManage.doSearch);//搜索
			$('#userReturnVisit_delete_but').click(common.tools.visit.returnVisitManage.userReturnVisit_delete_aff);//删除
			$('#exportUserReturnVisitData').click(common.tools.visit.returnVisitManage.exportData);//导出
			$('#search_returnVisitUser_choose').click(function(){common.security.userUtil.selectUser('#search_returnVisitUser','','','fullName','-1')});//回访对象
			$('#search_returnVisitSubmitUser_choose').click(function(){common.security.userUtil.selectUser('#search_returnVisitSubmitUser','','','fullName','-1')});//回访人
		}
	}
	
}();
$(document).ready(common.tools.visit.returnVisitManage.init);
