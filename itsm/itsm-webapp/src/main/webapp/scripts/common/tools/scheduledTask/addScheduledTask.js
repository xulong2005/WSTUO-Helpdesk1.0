$package("common.tools.scheduledTask");
$import("common.tools.scheduledTask.scheduledTask")
$import('itsm.itsop.selectCompany');
if(isCIHave){
	$import("itsm.cim.configureItemUtil");
	$import('itsm.cim.ciCategoryTree');
}
$import("common.config.dictionary.dataDictionaryUtil");
$import("common.config.category.eventCategoryTree");
$import("common.security.userUtil");
$import("common.eav.attributes");
$import("common.tools.schedule.setMonthly");
$import('common.knowledge.knowledgeTree');
$import('common.config.category.serviceCatalog');
$import('common.security.xssUtil');
/**  
 * @author QXY  
 * @constructor users
 * @description 定期任务添加
 * @date 2011-10-11
 * @since version 1.0 
 */
common.tools.scheduledTask.addScheduledTask=function(){
	
	return {
		
		/**
		 * 保存定期任务
		 */
		saveScheduledTask:function(){
			if($('#scheduledTask_add_eavAttributet_form').form('validate')){
				if($("#addScheduledTimeSettings form").form('validate')){
					if($('#addScheduledTaskForm').form('validate')){
						if($("#addScheduledTimeSettings input[name='scheduledTaskDTO.timeType']:checked").val() == "month" && $("#monthDay_select").val()==null){
							msgAlert(i18n.scheduledTask_monthPlan_monthDayIsNotNull,'info');
						}else{
							var myDate=new Date() 
				    		var month=myDate.getMonth()+1;
							if(!DateComparison($('#add_scheduledTask_endTime_input').val(),myDate.getFullYear()+'-'+month+'-'+(myDate.getDate()-1))){
								var _edesc="";
								if(CKEDITOR.instances['add_scheduledTask_edesc']){
									var oEditor = CKEDITOR.instances.add_scheduledTask_edesc;
									_edesc = oEditor.getData();
								}
								var bool=trim(_edesc)==''?false:true;
								if(!bool){
									msgAlert(i18n['titleAndContentCannotBeNull'],'info');
								}else{
									$('#add_scheduledTask_edesc').val(_edesc);
									var frm = $('#addScheduledTask_layout form').serialize();
									var url = 'scheduledTask!saveScheduledTask.action';
									//调用
									startProcess();
									$.post(url,frm, function(res){
											endProcess();
											basics.tab.tabUtils.closeTab(i18n['title_add_scheuledTask']);
											$('#scheduledTasksGrid').trigger('reloadGrid');
											basics.tab.tabUtils.addTab(i18n['title_scheduled_task_manage'],'../pages/common/tools/scheduledTask/scheduledTasksManage.jsp');
											msgShow(i18n['msg_add_successful'],'show');
										
									});
								}
							}else{
								msgAlert(i18n['tip_endTime_cannot_be_before_startTime'],'info');
								$('#add_scheduledTask').tabs('select', i18n.scheduledTask_time_setting);
							}
						}
					}
				}else{
					$('#add_scheduledTask').tabs('select', i18n.scheduledTask_time_setting);
					//为了防止验证信息不在左上角显示
					$("#addScheduledTimeSettings form").form('validate');
				}
			}else{
				msgAlert(i18n.eavAttributet_notNull,'info');
				$('#add_scheduledTask').tabs('select', i18n.config_extendedInfo);
			}
		},
		/**
		 * 选择请求分类
		 * @param showAttrId 显示div的id
		 * @param dtoName 
		 */	
		selectRequestCategory:function(showAttrId,dtoName){
			common.config.category.eventCategoryTree.showSelectTree('#request_category_select_window'
					,'#request_category_select_tree'
					,'Request'
					,'#add_scheduledTask_categoryName'
					,'#add_scheduledTask_categoryNo'
					,''
					,'',showAttrId,dtoName);
		},
		/**
		 * 初始化
		 */	
		init:function(){
			//绑定日期控件
			DatePicker97(['#add_scheduledTask_startTime_input','#add_scheduledTask_endTime_input']);
			$("#addScheduledTask_loading").hide();
			$("#addScheduledTask_panel").show();
			//设定月份总天数
			common.tools.schedule.setMonthly.setmonthDay('add_everyWhat_monthly','monthDay_select');
			common.config.dictionary.dataDictionaryUtil.loadOptionsByCode('effectRange','#add_scheduledTask_effectRange');
			common.config.dictionary.dataDictionaryUtil.loadOptionsByCode('seriousness','#add_scheduledTask_seriousness');
			common.config.dictionary.dataDictionaryUtil.loadOptionsByCode('imode','#add_scheduledTask_imode');
			common.config.dictionary.dataDictionaryUtil.loadOptionsByCode('level','#add_scheduledTask_level');
			common.config.dictionary.dataDictionaryUtil.loadOptionsByCode('priority','#add_scheduledTask_priority');
			
			setTimeout(function(){
				getUploader('#add_scheduledTask_file','#add_scheduledTask_attachmentStr','#add_scheduledTask_success_attachment','');
				initCkeditor('add_scheduledTask_edesc','Simple',function(){});
			},0)
			
//			lazyInitEditor('#add_scheduledTask_edesc','streamline');
			
			//common.eav.attributes.showAttributes('request','scheduledTask_add_eavAttributet');//加载
			
			$('#add_scheduledTask_companyName').click(function(){//选择公司
				itsm.itsop.selectCompany.openSelectCompanyWin('#add_scheduledTask_companyNo','#add_scheduledTask_companyName','#add_scheduledTask_createdName,#add_scheduledTask_createdNo');
			});
			//$('#add_scheduledTask_categoryName').click(common.tools.scheduledTask.addScheduledTask.selectRequestCategory);
			
			$('#add_scheduledTask_createdName').click(function(){
				common.security.userUtil.selectUser('#add_scheduledTask_createdName','#add_scheduledTask_createdNo','','loginName',$('#add_scheduledTask_companyNo').val());
			})
			
			//绑定选择配置项
			$('#add_scheduledTask__ref_ci_btn').click(function(){
				itsm.cim.configureItemUtil.requestSelectCI('#scheduledTaskRelatedCIShow',$('#add_scheduledTask_companyNo').val(),'');
			});
			
			
			$('#saveScheduledTaskBtn').click(common.tools.scheduledTask.addScheduledTask.saveScheduledTask);//保存定期任务
			
			$('#add_scheduledTask_backList').click(function(){
				basics.tab.tabUtils.addTab(i18n['title_scheduled_task_manage'],'../pages/common/tools/scheduledTask/scheduledTasksManage.jsp');	
			});//返回列表
			
			//加载默认公司
			common.security.defaultCompany.loadDefaultCompany('#add_scheduledTask_companyNo','#add_scheduledTask_companyName');
			
/*			$("#add_scheduledTask_ref_requestServiceDirName").click(function(){
				common.knowledge.knowledgeTree.selectKnowledgeServiceFilter('#knowledge_services_select_window','#knowledge_services_select_tree','#add_scheduledTask_ref_requestServiceDirName','#add_scheduledTask_ref_requestServiceDirNo');			
			});*/
			//服务目录
			$('#add_scheduledTask_service_add').click(function(){
				common.config.category.serviceCatalog.selectServiceDir('#add_scheduledTask_serviceDirectory_tbody');
			});
			
		}
	}
}();
$(document).ready(common.tools.scheduledTask.addScheduledTask.init);
