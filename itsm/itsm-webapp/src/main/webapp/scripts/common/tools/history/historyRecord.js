
/**  
 * @fileOverview 历史记录主函数
 * @author QXY
 * @version 1.0  
 */  
 /**  
 * @author QXY  
 * @constructor 历史记录主函数
 * @description 历史记录主函数
 * @date 2011-05-19
 * @since version 1.0 
 */
function historyRecord(historyRecordTable,eno,project){
	this.optLogHtml;
	this.showHistoryRecord=function(){
		$.post('historyRecord!findAllHistoryRecord.action','hisotryRecordDto.eno='+eno+'&hisotryRecordDto.eventType='+project,function(data){
			optLogHtml='';
			if(data!=null && data.length>0){
				$('#'+historyRecordTable+' tbody').html('');
				for(var i=0;i<data.length;i++){
					optLogHtml+="<tr >"+
					"<td style='width:10%;text-align:center;'>[step]</td>"+
					"<td style='width:20%;text-align:left'>[action]</td>"+
					"<td style='width:40%;text-align:left'><div style='white-space:normal; display:block;width:100%; word-break:break-all'>[detail]</div></td>"+
					"<td style='width:10%;text-align:center'>[operator]</td>"+
					"<td style='width:20%;text-align:center'>[time]</td>"+
					"</tr>";
					
					var _logTitle=data[i].logTitle;
					var _logDetails=data[i].logDetails;
					
					
					if(project=="itsm.problem" || project=="itsm.change"){//问题替换
						
						var temp_logTitle=i18n[_logTitle];
						var temp_logDetails=i18n[_logDetails];

						if(temp_logTitle!=null && temp_logTitle!=''){
							_logTitle=temp_logTitle;
						}

						if(temp_logDetails!=null && temp_logDetails!=''){
							_logDetails=temp_logDetails;
						}
					}
					optLogHtml=optLogHtml.replace("[step]",data.length-i)
					.replace("[action]",_logTitle)
					.replace("[detail]",_logDetails==null?'':_logDetails)
					.replace("[operator]",data[i].operator.replace('unknown',i18n['label_unknown']))
					.replace("[time]",timeFormatter(data[i].createdTime))
					.replace(/null/g,'');
					
					optLogHtml=formtDetails(optLogHtml)
					.replace(/TaskAssignGroup/g,i18n['title_request_assignToGroup'])
					.replace(/TaskAssignTechnician/g,i18n['title_request_assignToTC'])
					.replace(/AssignGroup/g,i18n['title_request_assignToGroup'])
					.replace(/AssignTechnician/g,i18n['title_request_assignToTC'])
					.replace(/Save Solution/g,i18n['problem_action_saveSolution'])
					.replace(/Task take/g,i18n['label_taskTake'])
					.replace(/Assign task/g,i18n['title_bpm_task_assign'])
					.replace(/AssignTechnician/g,i18n['title_request_assignToTC'])
					.replace(/ApproverResult/g,i18n['title_approverResult'])
					.replace(/Disagree/g,i18n['title_refused'])
					.replace(/assginTechnicalGroup/g,i18n['label_requestAssginTechnicalGroup'])
					.replace(/Agree/g,i18n['title_agree'])
					.replace(/TaskTake/g,i18n['label_taskTake'])
					.replace(/TaskReAssigne/g,i18n['assingeeTask'])
					.replace(/Assign Task/g,i18n['title_bpm_task_assign'])
					.replace(/Edit Request/g,i18n['title_request_editRequest'])
					.replace(/Remark/g,i18n['remark']);
				}
				$('#'+historyRecordTable+' tbody').append(optLogHtml);
			}
		});
	};
	
}
/**
 * 格式化历史记录内容
 * @param optLogHtml
 * @returns
 */
function formtDetails(optLogHtml){
	var returnStr=optLogHtml;
	var n1=optLogHtml.lastIndexOf('[{')+2;
	var n2=optLogHtml.lastIndexOf('}]');
	var changeNo=optLogHtml.substring(n1,n2);
	
	if(n1>0 && n2>0){
		var _href='<a href="javascript:basics.tab.tabUtils.reOpenTab({0},{1})">{2}</a>'
			_href=_href.replace('{0}',"'change!changeInfo.action?queryDTO.eno="+changeNo+"'")
			.replace('{1}',"'"+i18n['change_detail']+"'")
			.replace('{2}',"["+i18n['change_detail']+"]")
		returnStr=optLogHtml.replace("[{"+changeNo+"}]",_href)
	}
	return returnStr;
};

