$package('common.tools.workloadStatusStatistics');
/**  
* @fileOverview 技术员繁忙程度统计
* @author QXY
* @version 1.0
*/  
/**  
* @author QXY  
* @constructor Return Visit  
* @description 技术员繁忙程度统计
* @since version 1.0  
*/
common.tools.workloadStatusStatistics.workloadStatusStatistics = function() {
	return {
		/**
		 * 技术员繁忙程度统计列表
		 */
		showGrid:function(){
			var params = $.extend({},jqGridParams, {	
				url:'request!workloadStatusStatistics.action',
				colNames:[i18n.title_technician,
				          i18n.workloadStatusStatistics_countAssignedMineRquest,
				          i18n.workloadStatusStatistics_countMineNewRequest,
				          i18n.workloadStatusStatistics_countMineHandingRequest,
				          i18n.workloadStatusStatistics_servicesTotalScore],
				colModel:[
				    {name:'fullName',align:'center',width:100},
				    {name:'countAssignedMineRquest',align:'center',width:100},
				    {name:'countMineNewRequest',align:'center',width:100},
			   		{name:'countMineHandingRequest',align:'center',width:100},
			   		{name:'servicesTotalScore',align:'center',width:100},
				],	
				toolbar: [false,"top"],
				multiselect:false,
				jsonReader: $.extend(jqGridJsonReader,{id: "fullName"}),
				sortname:'fullName',
				pager:'#workStatisticsPager'
			});
			$("#workStatisticsGrid").jqGrid(params);
			$("#workStatisticsGrid").navGrid('#workStatisticsPager',navGridParams);
			//列表操作项
			$("#t_workStatisticsGrid").css(jqGridTopStyles);
			$("#t_workStatisticsGrid").append($('#workStatisticsGridToolbar').html());
			//自适应宽度
			setGridWidth("#workStatisticsGrid","regCenter",20);
		},
		/**
		 * 打开搜索面板
		 */
		search_openwindow:function(){
			windows('searchworkStatistics',{width:450,modal: false});
		},
		init:function() {
			common.tools.workloadStatusStatistics.workloadStatusStatistics.showGrid();
			$("#workStatistics_loading").hide();
			$("#workStatistics_content").show();
		}
	}
}();
$(document).ready(common.tools.workloadStatusStatistics.workloadStatusStatistics.init);
