$package('common.eav')
 
 /**  
 * @author Wstuo
 * @constructor 扩展属性分组主函数
 * @description 扩展属性分组主函数
 * @date 2013-03-11
 * @since version 1.0 
 */  
common.eav.selectAttributeGroup = function(){
	this.attributeGroupId='';
	this.attributeGroupNameId='';
	return { 
		/**
		 * @description 扩展属性分组列表
		 **/
		attrGroup_grid:function(){
			var _url="attrGroup!find.action?attrGroupQueryDTO.attrGroupId=0";
			if($("#selectAttrGroupGrid").html()!=''){
				$("#selectAttrGroupGrid").jqGrid('setGridParam',{url:_url}).trigger('reloadGrid');
			}else{
				var params = $.extend({},jqGridParams,{
					url:_url,
					colNames:[i18n['name'],i18n['sort'],'',i18n['check']],
					colModel:[{name:'attrGroupName',width:60,align:'center'},
							{name:'attrGroupSortNo',width:40,align:'center'},
							{name:'attrGroupId',hidden:true},
							{name:'act', width:20,sortable:false,align:'center',formatter:function(cell,event,data){
								return '<div style="padding:0px">'+
								'<a href="javascript:common.eav.selectAttributeGroup.confirmCheck(\''+(data.attrGroupId)+'\',\''+data.attrGroupName+'\',\''+data.attrGroupSortNo+'\')" title="'+i18n['check']+'">'+
								'<img src="../images/icons/ok.png"/></a>'+
								'</div>';
							}}
					    ],
					jsonReader: $.extend(jqGridJsonReader, {id: "attrGroupId"}),
					sortname:'attrGroupSortNo',
					multiselect: false, 
					pager:'#selectAttrGroupGridPager',
					ondblClickRow:function(rowId){
						var data=$('#selectAttrGroupGrid').getRowData(rowId);
						common.eav.selectAttributeGroup.confirmCheck(data.attrGroupId,data.attrGroupName,data.attrGroupSortNo);
					}
					});
					$("#selectAttrGroupGrid").jqGrid(params);
					$("#selectAttrGroupGrid").navGrid('#selectAttrGroupGridPager',navGridParams);
					//列表操作项
					$("#t_selectAttrGroupGrid").css(jqGridTopStyles);
					$("#t_selectAttrGroupGrid").append($('#selectAttrGroupGridToolbar').html());
			}
		},
		/**
		 * @description 选择分组后赋值并关闭面板
		 * @param attrGroupId 扩展属性分组id
		 * @param attrGroupName 扩展属性分组名称
		 * @param attrGroupSortNo 扩展属性分组排序
		 **/
		confirmCheck:function(attrGroupId,attrGroupName,attrGroupSortNo){
			$('#selectAttributeGroup_window').dialog('close');
			$('#'+attributeGroupId).val('');
			$('#'+attributeGroupNameId).val('');
			$('#'+attributeGroupId).val(attrGroupId);
			$('#'+attributeGroupNameId).val(attrGroupName).focus();
		},
		/**
		 * @description 打开选择扩展属性分组面板
		 * @param groupId 扩展属性分组id的控件id
		 * @param nameId 扩展属性分组名称的控件id
		 **/
		openGrid:function(groupId,nameId){
			attributeGroupId=groupId;
			attributeGroupNameId=nameId;
			windows('selectAttributeGroup_window',{width:500});
			common.eav.selectAttributeGroup.attrGroup_grid();
			//禁止回车事件
			$("#attributeGroupNameSearch").keydown(function (e) {
	            var curKey = e.which;
	            if (curKey == 13) {
	                return false;
	            }
	        });
		},
		/**
		 * @description 执行搜索
		 */
		doSearch:function(){
			var _params = $('#topAttrGroupForm').getForm();
			var postData = $("#selectAttrGroupGrid").jqGrid("getGridParam", "postData");       
			$.extend(postData, _params);  //将postData中的查询参数加上查询表单的参数		
			$('#selectAttrGroupGrid').trigger('reloadGrid',[{"page":"1"}]);
		},
		init: function(){	
			
		}
	}
}();
$(document).ready(common.eav.selectAttributeGroup.init);
