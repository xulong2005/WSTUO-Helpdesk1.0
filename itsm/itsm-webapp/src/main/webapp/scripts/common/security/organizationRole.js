
$package('common.security');

/**  
 * @author Van  
 * @constructor WSTO
 * @description 机构对应角色.
 * @date 2011-02-25
 * @since version 1.0 
 */
common.security.organizationRole = function() {

	return {
		
		/**
		 * 显示机构对应的角色.
		 */
		showOrganizationRole:function(orgNo,tablePanel){
			
			
			//查找机构对应角色
			$.post("organization!findRolesByOrgNo.action?orgNo="+orgNo,function(data){
				
				var roleStr="";
				
				for(var i=0;i<data.length;i++){
					roleStr+=data[i]+",";
					
				}
				
				//显示角色表格
				common.security.organizationRole.showRoleView(roleStr,tablePanel);
			});
		},
		
		
		/**
		 * @description 加载所有角色,并选择.
		 * @param roleStr  角色字符
		 * @param tablePanel 表格面板
		 */
		showRoleView:function(roleStr,tablePanel){
			$("#orgRoleDiv"+" table").html("");
			$.post("role!findByState.action",function(data){
				if(data!=null){
					var roleHtml="<tr>";
					for(var i=0;i<data.length;i++){	
						var checked='';

						if(roleStr!=null && roleStr!=''){
							var roles=roleStr.split(',')
							for(var j=0;roles.length>j;j++){
								if(roles[j]==data[i].roleId){
									checked='checked';
								}
							}
						}
						roleHtml=roleHtml+"<td align='center'><input "+checked+" type='checkbox' name='organizationDto.orgRole' class='userRoleId' value='"+data[i].roleId+"' /></td><td>"+data[i].roleName+"</td>";
						
						if(i==data.length-1 && data.length%2!=0){
							roleHtml+="<td></td><td></td>";
						}
						
						if(i!=0&&(i+1)%2==0){
							roleHtml+="</tr><tr>";
						}
					}
					roleHtml+="</tr>";
					$("#orgRoleDiv"+" table").html(roleHtml);
					orgRoleIds=null;
				}
			},"json")
		}
		
	};

}();