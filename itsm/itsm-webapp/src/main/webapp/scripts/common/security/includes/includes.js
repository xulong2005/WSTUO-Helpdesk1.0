﻿ /**  
 * @fileOverview "Includes"
 * @author QXY
 * @version 1.0  
 */  
 /**  
 * @author QXY  
 * @constructor Includes
 * @description "用于加载Includes文件JS主函数"
 * @date 2012-7-31
 * @since version 1.0 
 */ 
$package('common.security.includes');

common.security.includes.includes=function(){
	//选择所属客户
	this._loadSelectCustomerIncludesFileFlag=false;
	//选择用户
	this._loadSelectUserIncludesFileFlag=false;
	//配置向导
	this._loadGuideFileFlag = false;
	//ACL权限
	this._loadAclWinFileFlag=false;
	return {
		
		/**
		 * 加载选择外包客户窗口includes文件
		 */
		loadSelectCustomerIncludesFile:function(){
			if(!_loadSelectCustomerIncludesFileFlag){
				$('#selectCustomer_html').load('common/security/includes/includes_selectCustomer.jsp',function(){
					$.parser.parse($('#selectCustomer_html'));
				});
			}
			_loadSelectCustomerIncludesFileFlag=true;
		},
		
		/**
		 * 
		 * 加载选择用户窗口（单选）includes文件
		 */
		loadSelectUserIncludesFile:function(){
			if(!_loadSelectUserIncludesFileFlag){
				$('#selectUser_html').load('common/security/includes/includes_selectUser.jsp',function(){
					$.parser.parse($('#selectUser_html'));
				});
			}
			_loadSelectUserIncludesFileFlag=true;
		},
		/**
		 * 配置向导窗口文件
		 */
		loadGuideIncludesFile:function(){
			if(!_loadGuideFileFlag){
				$('#guide_html').load('common/security/includes/includes_guide.jsp',function(){
					$.parser.parse($('#guide_html'));
				});
			}
			_loadGuideFileFlag=true;
		},
		/**
		 * ACL权限
		 */
		loadAclWinIncludesFile:function(){
			if(!_loadAclWinFileFlag){
				$('#aclWin_html').load('common/security/includes/includes_acl.jsp',function(){
					$.parser.parse($('#aclWin_html'));
				});
			}
			_loadAclWinFileFlag=true;
		},
		init:function(){
			
		}
	}
}();
$(function(){common.security.includes.includes.init});
