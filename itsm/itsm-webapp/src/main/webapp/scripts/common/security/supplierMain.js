
$package('common.security');
$import('itsm.itsop.selectCompany');

/**  
 * @author Van  
 * @constructor WSTO
 * @description 供应商管理主函数.
 * @date 2011-02-25
 * @since version 1.0 
 */
common.security.supplierMain = function() {
	return {
		/**
		 * @description 动作格式化.
		 */
		supplierGridFormatter:function(cell,opt,data){
			
			return actionFormat('1','1')
			.replace('[edit]','common.security.supplierMain.editSupplier_openWindow_aff('+opt.rowId+')')
			.replace('[delete]','common.security.supplierMain.deleteSupplierInLine('+opt.rowId+')');
		},
		/**
		 * @description 加载供应商列表.
		 */
		showSupplierGrid:function(){
	
			var params = $.extend({},jqGridParams, {	
				url:'supplier!findByCompany.action',
				colNames:[i18n['label_belongs_client'],i18n['common_name'],i18n['common_phone'],i18n['common_email'],i18n['common_address'],i18n['common_action'],'','','','','',''],
				colModel:[
						 
				          {name:'companyName',align:'center',hidden:hideCompany,sortable:false},
						  {name:'orgName',width:100,align:'center'},			  
						  {name:'officePhone',width:80,align:'center'},
						  {name:'email',width:80,align:'center'},
						  {name:'address',width:150,align:'center'},
						  {name:'act', width:80,align:'center',sortable:false,formatter:common.security.supplierMain.supplierGridFormatter},
						  {name:'orgNo',hidden:true},
						  {name:'officeFax',hidden:true},
						  {name:'homePage',hidden:true},
						  {name:'logo',hidden:true},
						  {name:'orgType',hidden:true},
						  {name:'companyNo',hidden:true}
						  ],
				jsonReader: $.extend(jqGridJsonReader, {id: "orgNo"}),
				sortname:'orgNo',
				pager:'#supplierGridPager'
				});
				$("#supplierGrid").jqGrid(params);
				$("#supplierGrid").navGrid('#supplierGridPager',navGridParams);
				//列表操作项
				$("#t_supplierGrid").css(jqGridTopStyles);
				$("#t_supplierGrid").append($('#supplierGridToolbar').html());
				
				//自适应宽度
				setGridWidth("#supplierGrid","regCenter",20);
				
		},
	
	
		/**
		 * @description 打开新增供应商窗口.
		 */
		addSupplier_openWindow:function(){
			
			windows('addSupplier_win',{width: 400});
		},
		
		/**
		 * @description 提交新增供应商.
		 */
		addSupplier_doAdd:function(){
			
			if($('#addSupplier_form').form('validate')){
				var frm = $('#addSupplier_win form').serialize();
				var url = 'supplier!save.action';
				$.post(url,frm, function(){
					
					$('#supplierGrid').trigger('reloadGrid');
					$('#addSupplier_win').dialog('close');
					
					$('#addSupplier_orgName').val('');
					$('#addSupplier_officeFax').val('');
					$('#addSupplier_officePhone').val('');
					$('#addSupplier_email').val('');
					$('#addSupplier_address').val('');
					
					msgShow(i18n['msg_supplier_addSuccessful'],'show');
					
				});
			}
		},
		
		/**
		 * @description 编辑供应商.
		 */
		editSupplier_openWindow:function(){
			
			checkBeforeEditGrid("#supplierGrid",function(rowData){
				
				 $('#editSupplier_orgName').val(rowData.orgName);
				 $('#editSupplier_officeFax').val(rowData.officeFax);
				 $('#editSupplier_officePhone').val(rowData.officePhone);
				 $('#editSupplier_email').val(rowData.email);
				 $('#editSupplier_address').val(rowData.address);
				 $('#editSupplier_orgNo').val(rowData.orgNo);
				 
				 $('#editSupplier_companyName').val(rowData.companyName);
				windows('editSupplier_win',{width: 400});
			});
		},
		
		/**
		 * @description 编辑供应商.
		 * @param rowId  行Id
		 */
		editSupplier_openWindow_aff:function(rowId){
			var rowData= $("#supplierGrid").jqGrid('getRowData',rowId);  // 行数据  
				$('#editSupplier_orgName').val(rowData.orgName);
				 $('#editSupplier_officeFax').val(rowData.officeFax);
				 $('#editSupplier_officePhone').val(rowData.officePhone);
				 $('#editSupplier_email').val(rowData.email);
				 $('#editSupplier_address').val(rowData.address);
				 $('#editSupplier_orgNo').val(rowData.orgNo);
				 $('#editSupplier_companyName').val(rowData.companyName);
				windows('editSupplier_win',{width: 400});
		},
		
		
		/**
		 * @description 提交编辑供应商.
		 */
		editSupplier_doEdit:function(){
			
			if($('#editSupplier_form').form('validate')){
				var frm = $('#editSupplier_win form').serialize();
				var url = 'supplier!merge.action';
				$.post(url,frm, function(){
					$('#supplierGrid').trigger('reloadGrid');
					$('#editSupplier_win').dialog('close');
					msgShow(i18n['msg_supplier_editSuccessful'],'show');
				});
			}
		},
	
		/**
		 * @description 删除客户.
		 */
		deleteSupplier_tooBar:function(){
		
			checkBeforeDeleteGrid("#supplierGrid",function(rowIds){
				
				var param = $.param({'orgNos':rowIds},true);
				$.post("supplier!deleteSuppliers.action", param, function(){
					
					$('#supplierGrid').trigger('reloadGrid');
					msgShow(i18n['msg_supplier_deleteSuccessful'],'show');
					
				}, "json");
				
			});
		},
		
		
	
		/**
		 * @description 提交删除客户.
		 * @param rowid 行编号
		 */
		deleteSupplierInLine:function(rowId){
			
			msgConfirm(i18n['msg_msg'],'<br/>'+i18n['msg_confirmDelete'],function(){
				
				var url = "supplier!deleteSuppliers.action";
				var params = {"orgNos" :rowId};
				$.post(url,params,
					function (r) {
					
						$('#supplierGrid').trigger('reloadGrid');
						msgShow(i18n['msg_supplier_deleteSuccessful'],'show');
					},
					'json'
				);
			});
		},
	
		
		/**
		 * @description 搜索供应商.
		 */
		searchSupplier_openWindow:function(){
			itsm.app.autocomplete.autocomplete.bindAutoComplete('#searchSupplier_companyName','com.wstuo.itsm.itsop.itsopuser.entity.ITSOPUser','orgName','orgName','orgNo','Long','#searchSupplier_companyNo',userName,'ITSOPUser');
			windows('searchSupplierGrid',{width: 400,modal: false});
		},
		
		/**
		 * @description 搜索供应商.
		 */
		searchSupplier_doSearch:function(){
			var va=$('#searchSupplier_companyName').val().replace(/\s+/g,"");
			if(va=="")
				$('#searchSupplier_companyNo').val('');
			
			var _url = 'supplier!findByCompany.action';	
			searchGridWithForm('#supplierGrid','#searchSupplierGrid',_url);	
		},

		
		
		
		/**
		 * 导出数据.
		 */
		exportSupplier:function(){
			
			
			
			
			var _postData = $("#supplierGrid").jqGrid("getGridParam", "postData"); //列表参数
			$('#exportSupplier_param').html('');//清空参数
			
			$.each(_postData,function(k,v){
				//加入参数
				$("<input type='hidden' name='"+k+"' value='"+v+"' />").appendTo("#exportSupplier_param");
			});	
			
			
			$("#exportSupplierWindow form").submit();
		},
		
		
		/**
		 * 导入数据.
		 */
		openInportWindow:function(){			
			windows('importSupplierWindow',{width:400});
		},
		
		
		/**
		 * 执行导入.
		 */
		doImport:function(){
			startProcess();
			var path=$('#supplierFileName').val();
			if(path!=""){
				$.ajaxFileUpload({
		            url:'supplier!importSupplier.action',
		            secureuri:false,
		            fileElementId:'supplierFileName', 
		            dataType:'json',
		            success: function(data,status){
		            
		            	if(data=="-1"){//公司名称输入错误will(20130716)
							msgAlert("<br/>"+i18n['msg_dc_importFailure'],'info');
							endProcess();
		            	}else if(data=="-2"){//公司名称输入错误will(20130716)
							msgAlert("<br/>"+i18n['ERROR_COMPANY_NAME'],'error');
							endProcess();
		            	}else{
			             	$('#importSupplierWindow').dialog('close');
			            	$('#supplierGrid').trigger('reloadGrid'); 
			            	msgShow(i18n['msg_dc_dataImportSuccessful'],'show');
			            	endProcess();
		            	}
		            }
		        });
			}else{
				msgAlert(i18n['msg_dc_fileNull'],'info');
				endProcess();
			}
	},
		
		
		
		
		
				
		/**
		 * 初始化加载
		 */
		init: function(){
			//隐显加载图标
			showAndHidePanel('#supplierMain_content','#supplierMain_loading');
			
			//加载列表
			common.security.supplierMain.showSupplierGrid();

			//供应商
			$('#addSupplierBtn').click(common.security.supplierMain.addSupplier_openWindow);
			$('#addSupplierBtn_OK').click(common.security.supplierMain.addSupplier_doAdd);
			$('#editSupplierBtn').click(common.security.supplierMain.editSupplier_openWindow);
			$('#editSupplierBtn_OK').click(common.security.supplierMain.editSupplier_doEdit);
			$('#deleteSupplierBtn').click(common.security.supplierMain.deleteSupplier_tooBar);
			$('#searchSupplierBtn').click(common.security.supplierMain.searchSupplier_openWindow);
			$('#searchSupplierBtn_OK').click(common.security.supplierMain.searchSupplier_doSearch);
			
			//选择公司
			$('#search_Supplier_companyName').click(function(){
				itsm.itsop.selectCompany.openSelectCompanyWin('#addSupplier_companyNo','#addSupplier_companyName','');
			});
			itsm.app.autocomplete.autocomplete.bindAutoComplete('#addSupplier_companyName','com.wstuo.itsm.itsop.itsopuser.entity.ITSOPUser','orgName','orgName','orgNo','Long','#addSupplier_companyNo',userName,'true');
			//选择公司
			$('#Supplier_search_companyName').click(function(){
				itsm.itsop.selectCompany.openSelectCompanyWin('#searchSupplier_companyNo','#searchSupplier_companyName','');
			});
			//导入导出
			$('#supplierGrid_export').click(common.security.supplierMain.exportSupplier);
			$('#supplierGrid_import').click(common.security.supplierMain.openInportWindow);
			$('#supplierGrid_doImport').click(common.security.supplierMain.doImport);
		}
	};
}();
//载入
$(document).ready(common.security.supplierMain.init);
