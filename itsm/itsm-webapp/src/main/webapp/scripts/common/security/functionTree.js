$package('common.security');

/**
 * @author Van
 * @constructor resourceMain
 * @description 资源管理主函数.
 * @date 2011-02-25
 * @since version 1.0
 */
common.security.functionTree = function() {

	this.functionTree_DIV;

	return {
		/**
		 * 功能树，带回调函数.
		 * @param treeDIV   树div的id
		 * @param resCode   资源
		 * @param cakkback  回调函数
		 */
		functionTreeCallback : function(treeDIV, resCode, callback) {
			$(treeDIV).jstree({
				json_data : {
					ajax : {
						url : "function!findTree.action",
						cache : false,
						data : {
							'resCode' : resCode
						}
					}
				},
				plugins : [ "themes", "json_data", "ui" ]
			}).bind('loaded.jstree', function(e, data) {
				data.inst.open_all(-1);
			}).bind('select_node.jstree', function(e, data) {
				callback(e, data);
			});
		},

		/**
		 * @description 加载功能树结构. #functionTreeDIV
		 * @param treeDIV 树div的id
		 */
		showFunctionTree : function(treeDIV) {

			functionTree_DIV = treeDIV;

			$(treeDIV).jstree(
					{
						json_data : {
							ajax : {
								url : "function!findTree.action",
								cache : false
							}
						},
						plugins : [ "themes", "json_data", "ui", "crrm",
								"contextmenu", "dnd" ]

					}).bind('loaded.jstree', function(e, data) {
				data.inst.open_all(-1);
			}).bind('select_node.jstree',
					common.security.functionTree.selectTreeNode).bind(
					'delete_node.jstree',
					common.security.functionTree.removeTreeNode).bind(
					'move_node.jstree',
					common.security.functionTree.moveTreeNode);
		},

		/**
		 * @description 点击树节点
		 * @param event   事件
		 * @param data    点击数据
		 */
		selectTreeNode : function(event, data) {

			var obj = data.rslt.obj;

			var resName = obj.attr("resName");
			var icon = obj.attr("resIcon");
			if (resName != null && resName != '') {
				// 编辑面板
				$('#editCurrentFunction_resNo').val(obj.attr("resNo"));
				$('#edit_resName').val(resName);
				$('#edit_resCode').val(obj.attr("resCode"));
				$('#edit_icon').val(icon);
				$('#edit_resUrl').val(obj.attr("resUrl"));
				
				$('#show_edit_icon').html("");
				
				if(icon!=null && icon!="" &&  icon!="null"){
					$('#show_edit_icon').html(
							'<img src="organization!getImageUrlforimageStream.action?imgFilePath='+obj.attr("resIcon")+'" width="30px" height="30px" />');
				}
				// 新增
				$('#add_parentFunctionNo').val(obj.attr("resNo"));
				// 新增操作
				$('#operation_functionNo').val(obj.attr("resNo"));
				// 搜索项
				$('#search_FunctionNo').val(obj.attr("resNo"));
				// 查找当前节点下的列表
				common.security.operationGrid.showGridByFunction();
			} else {
				$('#edit_resName').val("ROOT");
				$('#edit_resCode').val("");
				$('#edit_icon').val("");
				$('#edit_resUrl').val("");
				$('#edit_resNo').val(null);
				$('#show_edit_icon').html("");
				$('#add_parentFunctionNo').val("-1");
				// 搜索项
				$('#search_FunctionNo').val("");
				// 查询对应功能的操作
				common.security.operationGrid.showGridByFunction();
			}
		},

		/**
		 * @description 删除功能节点
		 * @param e 事件
		 * @param data 数据
		 */
		removeTreeNode : function(e, data) {
			var url = "function!deleteFunction.action";
			var resNo = data.rslt.obj.attr("resNo");
			var params = {
				"functionDto.resNo" : resNo
			};
			var ajax_error = true;

			$.post(url, params, function(r) {

				ajax_error = false;

				// $('#functionGrid').jqGrid().trigger('reloadGrid');
				msgShow(i18n['msg_deleteSuccessful'], 'show');

				// 清空信息
				var resNo_base = $('#editCurrentFunction_resNo').val();

				if (resNo == resNo_base) {

					$('#edit_resName').val("ROOT");
					$('#edit_resCode').val("");
					$('#edit_icon').val("");
					$('#edit_resUrl').val("");
					$('#edit_resNo').val("");
					$('#add_parentFunctionNo').val("-1");
					$('#search_FunctionNo').val("-1");
				}

			}, 'json');

			setTimeout(function() {

				if (ajax_error == true) {
					common.security.functionTree
							.showFunctionTree(functionTree_DIV);
				}

			}, 1500);
		},

		/**
		 * @description 拖拽、复制粘贴节点
		 * @param e 事件
		 * @param data 数据
		 */
		moveTreeNode : function(e, data) {
			// 遍历节点
			data.rslt.o.each(function(i) {

				var parentFunctionNo = "-1";
				var resNo = $(this).attr('resNo').replace('node_', '');
				if (data.rslt.np.attr('resNo') != null) {
					parentFunctionNo = data.rslt.np.attr('resNo').replace(
							'node_', '');
				}
				var params = {
					"functionDto.resNo" : resNo,
					"functionDto.parentFunctionNo" : parentFunctionNo
				};
				// 复制功能
				if (data.rslt.cy == true) {
					common.security.functionTree.moveTreeNode_CopyNode(params);
					return;
				}
				// 剪切功能
				if (data.rslt.cy == null || data.rslt.cy == false) {
					common.security.functionTree.moveTreeNode_CutNode(params);
					return;
				}
			});
		},

		/**
		 * 复制功能树节点.
		 * @param params 选中节点参数
		 */
		moveTreeNode_CopyNode : function(params) {

			var url = "function!copyFunction.action";
			$.post(url, params,
					function(r) {
						// $('#functionGrid').jqGrid().trigger('reloadGrid');
						msgShow(i18n['msg_operationSuccessful'], 'show');

						common.security.functionTree
								.showFunctionTree(functionTree_DIV);
					}, 'json');
		},

		/**
		 * 剪切功能树节点.
		 * @param params 选中节点参数
		 */
		moveTreeNode_CutNode : function(params) {

			var url = "function!cutFunction.action";
			$.post(url, params,
					function(r) {

						// $('#functionGrid').jqGrid().trigger('reloadGrid');
						msgShow(i18n['msg_operationSuccessful'], 'show');
						common.security.functionTree
								.showFunctionTree(functionTree_DIV);

					}, 'json');

		},

		/**
		 * @description 选择树
		 * @param windowId
		 *            窗口ID
		 * @param functionId
		 *            选定后将功能编号赋值到 functionId
		 * @param functionName
		 *            选定后将功能名称赋值到 functionId
		 * @param treePanel
		 *            树DIV编号
		 */
		showSelectFunctionTree : function(windowId, functionId, functionName,
				treePanel) {

			// 打开窗口
			windows(windowId.replace('#', ''), {
				width : 250,
				height : 350
			});
			$(treePanel).jstree({
				json_data : {
					ajax : {
						url : "function!findTree.action",
						cache : false
					}
				},
				plugins : [ "themes", "json_data", "ui", "dnd" ]

			}).bind('loaded.jstree', function(e, data) {
				data.inst.open_all(-1);
			}).bind(
					'select_node.jstree',
					function(event, data) {

						if (data.rslt.obj.attr("resName") == null
								|| data.rslt.obj.attr("resNo") == null) {

							$(functionName).val("ROOT");
							$(functionId).val("-1");

						} else {

							$(functionName).val(data.rslt.obj.attr("resName"));
							$(functionId).val(data.rslt.obj.attr("resNo"));
						}
						$(functionName).focus()
						$(windowId).dialog('close');

					});
		}

	};

}();
