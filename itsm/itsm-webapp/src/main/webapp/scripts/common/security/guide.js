$package("common.security");

$import('common.security.includes.includes');
$import('common.config.includes.includes');

/**
 * @author Van
 * @constructor
 * @description 用户向导
 * @date 2011-02-25
 * @since version 1.0
 */
common.security.guide = function() {

	this.org_movie = '<OBJECT classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=9,0,124,0" width=455 height=300><param name= "Movie" value="../attachments/demo/guide_org.swf"><param name="allowFullScreen" value="true"><embed src="../attachments/demo/guide_org.swf" width="455" height="300" loop="0" quality="high" pluginspage="http://www.adobe.com/go/getflashplayer" type="application/x-shockwave-flash" menu="false" allowFullScreen="true"></embed></OBJECT>';
	this.user_movie = '<OBJECT classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=9,0,124,0" width=455 height=350><param name= "Movie" value="../attachments/demo/guide_user.swf"><param name="allowFullScreen" value="true"><embed src="../attachments/demo/guide_user.swf" width="455" height="350" loop="0" quality="high" pluginspage="http://www.adobe.com/go/getflashplayer" type="application/x-shockwave-flash" menu="false" allowFullScreen="true"></embed></OBJECT>';

	return {

		/**
		 * 1 初始化配置向导。
		 */
		initGuide : function() {

			var url = 'systemGuide!findGuideBoo.action';

			$.post(url, function(res) {

				if (res) {
					common.security.includes.includes.loadSelectCustomerIncludesFile();
					basics.includes.loadImportCsvIncludesFile();
					common.config.includes.includes.loadCategoryIncludesFile();
					// 加载左菜单
					/*showLeftMenu('../pages/common/security/leftMenu.jsp?random=' + Math.random() * 10 + 1, 'leftMenu');
					basics.tab.tabUtils.addTab(i18n.label_systemGuide_title, '../pages/common/config/systemGuide/SystemGuide.jsp');
					$('#main_menu_tabs li').removeClass("tabs-selected");
					$('#basicSetTopMenu_li').addClass("tabs-selected");*/
				}

			});

			/*
			 * var url = 'organization!findCompany.action?companyNo='+companyNo; $.post(url,function(res){ if(res.setup==null || !res.setup){ windows('guide_company_window',{width:500,position: [250,100]}); common.security.guide.findCompanyInfo();
			 *  } });
			 */
		},

		/**
		 * 查找公司信息.
		 */
		findCompanyInfo : function() {
			var url = 'organization!findCompany.action?companyNo=' + companyNo;
			$.post(url, function(res) {
				$('#guide_init_orgName').val($vl(res.orgName));
				$('#guide_init_officeFax').val($vl(res.officeFax));
				$('#guide_init_officePhone').val($vl(res.officePhone));
				$('#guide_init_email').val($vl(res.email));
				$('#guide_init_address').val($vl(res.address));
				$('#guide_init_homePage').val($vl(res.homePage));
				$('#guide_init_logo').val($vl(res.logo));

				if ($vl(res.logo) != '') {
					$('#guide_show_company_logo').html('<img src="../upload/images/' + res.logo + '" width="60px" height="30px" />');
				}
			});
		},
		opentohelp : function() {
			windows('common_Help_to_id', {
				width : 650,
				height : 260
			});
		},
		/**
		 * 返回配置公司信息.
		 */
		previousToCompany : function() {

			$('#guide_email_window').dialog('close');
			windows('guide_company_window', {
				width : 500,
				position : [250, 100]
			});

		},

		/**
		 * 2 保存公司信息并继续.
		 */
		saveCompanyInfoAndNext : function() {

			if ($('#guide_company_window form').form('validate')) {

				var frm = $('#guide_company_window form').serialize();
				var _url = 'organization!mergeCompany.action?companyNo=' + companyNo;
				$.post(_url, frm, function() {
					showIndexLogo();// 更新LOGO
					$('#guide_company_window').dialog('close');
					windows('guide_email_window', {
						width : 500,
						position : [250, 100]
					});

					common.security.guide.findEmail();
				});
			}
		},

		/**
		 * 查找邮箱信息.
		 */
		findEmail : function() {

			var url = 'mailServer!findServereMail.action';
			$.post(url, function(res) {

				$('#guide_mailUserName').val($vl(res.userName));
				$('#guide_mailPassword').val($vl(res.password));

				$('#guide_smtp_serverAddress').val($vl(res.smtpServerAddress));
				$('#guide_smtp_serverPort').val($vl(res.smtpServerPort));
				$('#guide_pop3_serverAddress').val($vl(res.pop3ServerAddress));
				$('#guide_pop3_serverPort').val($vl(res.pop3ServerPort));

				if (res.emailServerId != null) {
					$('#emailServerId').val($vl(res.emailServerId));
				}
			});
		},

		/**
		 * 3 保存邮箱信息并继续。
		 */
		saveMailServerAndNext : function() {

			if ($('#guide_email_window form').form('validate')) {
				var _params = $('#guide_email_window form').serialize();
				var url = 'mailServer!saveOrUpdateEmailServer.action';
				windows('guide_org_window', {
					width : 500,
					position : [250, 100]
				});
				$('#guide_email_window').dialog('close');// 配置邮件
				$.post(url, _params, function() {
					// 打开机构
					basics.tab.tabUtils.addTab(guide_org_title, '../pages/common/security/organization.jsp');
					common.security.guide.findEmail();
					$('#guide_org_movie').html(org_movie);
				});
			}
		},

		/**
		 * 4 保存邮箱信息并继续。
		 */
		jumpMailServerAndNext : function() {
			$('#guide_email_window').dialog('close');// 配置邮件
			windows('guide_org_window', {
				width : 500,
				position : [250, 100]
			});
			$('#guide_org_movie').html(org_movie);

		},

		previousToEmail : function() {

			$('#guide_org_window').dialog('close');
			windows('guide_email_window', {
				width : 500,
				position : [250, 100]
			});
		},

		nextToUser : function() {
			windows('guide_user_window', {
				width : 500,
				position : [250, 100]
			});
			$('#guide_org_window').dialog('close');

			// 打开用户
			basics.tab.tabUtils.addTab(guide_user_title, '../pages/common/security/user.jsp');

			$('#guide_user_movie').html(user_movie);

		},

		previousToOrg : function() {

			$('#guide_user_window').dialog('close');
			windows('guide_org_window', {
				width : 500,
				position : [250, 100]
			});
			$('#guide_org_movie').html(org_movie);
		},
		complete : function() {
			$('#guide_user_window').dialog('close');
			msgShow(i18n['configureComplete'], 'show');
		}
	};

}();

$(document).ready(function() {
	setTimeout(common.security.guide.initGuide, 3000);
});
