﻿ /**  
 * @fileOverview "Includes"
 * @author QXY
 * @version 1.0  
 */  
 /**  
 * @author QXY  
 * @constructor Includes
 * @description "用于加载Includes文件JS主函数"
 * @date 2012-7-31
 * @since version 1.0 
 */ 
$package('basics');

basics.includes=function(){
	//导入CSV
	this._loadImportCsvIncludesFileFlag=false;
	//T-CODE
	this._loadTCodeIncludesFileFlag=false;
	//快捷页面窗口
	this._loadWelcomeFileFlag=false;
	//语音卡弹屏页面窗口
	this._loadVoiceCardFileFlag=false;
	return {
		//加载导入数据窗口includes文件
		loadImportCsvIncludesFile:function(){
			if(!_loadImportCsvIncludesFileFlag){
				$('#importCsv_html').load('includes/includes_importExcel.jsp',function(){
					$.parser.parse($('#importCsv_html'));
				});
			}
			_loadImportCsvIncludesFileFlag=true;
		},
		//加载TCode帮助窗口includes文件
		loadTCodeIncludesFile:function(){
			if(!_loadTCodeIncludesFileFlag){
				$('#tCode_html').load('includes/includes_tcodeHelp.jsp',function(){
					$.parser.parse($('#tCode_html'));
				});
			}
			_loadTCodeIncludesFileFlag=true;
		},
		
		//快捷页面窗口文件
		loadWelcomeIncludesFile:function(){
			if(!_loadWelcomeFileFlag){
				$('#welcome_html').load('includes/includes_welcome.jsp',function(){
					$.parser.parse($('#welcome_html'));
				});
			}
			_loadWelcomeFileFlag=true;
		},
		//语音卡窗口文件
		loadVoiceCardIncludesFile:function(){
			if(!_loadVoiceCardFileFlag){
				$('#voiceCard_html').load('includes/includes_voiceCard.jsp',function(){
					$.parser.parse($('#voiceCard_html'));
				});
			}
			_loadVoiceCardFileFlag=true;
		},
		init:function(){
			
		}
	}
}();
$(function(){basics.includes.init});
