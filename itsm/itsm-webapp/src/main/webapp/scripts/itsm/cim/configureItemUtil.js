$package('itsm.cim');

 /**  
 * @author Van  
 * @constructor WSTO
 * @description 选择配置项分类公告方法.
 * @date 2011-05-21
 * @since version 1.0 
 */

itsm.cim.configureItemUtil=function(){
	
	this.windowId='';//窗口ID
	this.treeId='';//树结构ID
	this.gridId='';//列表ID
	this.tableId='';//表格ID（多选时添加到列表）
	this.multiSelectId='';//点击确认选择的按钮ID
	this.loadFlag='0';//加载状态，避免Grid重复加载
	
	this.namePut='';//将配置项名称赋值到此按钮
	this.idPut='';//将配置项编号赋值到此按钮
	this.ciselect_mutiTag=true;//多选标签
	this._creator = '';
	
	this.hiddenName='';
	this._companyNo=0;
	this._ciQueryParam='';//配置项查看参数
	var categoryNo;
	this.rowHTML='<tr id="ref_ci_CIID">'+
	'<td align="center" id="showCIno">CINO</td>'+
	'<td align="center"><input type="hidden" id="showCIId" name="changeDTO.ciIds" value="CIID" />'+
	'<a href=javascript:itsm.cim.configureItemUtil.lookConfigureItemInfo(CIID) id="showCIName">CINAME</a>'+
	'</td>'+
	'<td align="center" id="showCIcategoryName">CATEGORYNAME</td>'+
	'<td align="center" id="showCIStatus">CISTATUS</td>'+
	'<td align="center"><a onclick=itsm.cim.configureItemUtil.removeRow("#ref_ci_CIID")>DELETE</a></td>'+
	'</tr>';
	
	this.problemRowHTML='<tr id="ref_ci_CIID">'+
	'<td align="center" id="problemShowCIno">CINO</td>'+
	'<td align="center"><input id="probleShowCIId" type="hidden" name="cinos" value="CIID" />'+
	'<a href=javascript:itsm.cim.configureItemUtil.lookConfigureItemInfo(CIID) id="probleShowCIName">CINAME</a>'+
	'</td>'+
	'<td align="center" id="probleShowCIcategoryName">CATEGORYNAME</td>'+
	'<td align="center" id="probleShowCIStatus">CISTATUS</td>'+
	'<td align="center"><a onclick=itsm.cim.configureItemUtil.removeRow("#ref_ci_CIID")>DELETE</a></td>'+
	'</tr>';
	
	this.configureItemInputId='';//用户关联的配置项选择输入ID
	
	this.requestCiId='';

	return {

		/**
		 * 单选状态
		 * @param p_namePut 保存选择配置项的名称
		 * @param p_idPut 保存选择配置项的id
		 * @param p_companyNo 公司id
		 */
		selectCIS:function(p_namePut,p_idPut,p_companyNo){
			
			$('#selectCI_south_btn').hide();//隐藏多选按钮
			$('#selectCI_south_msg').show();//显示提示信息
			ciselect_mutiTag=false;
			hiddenName='';
			namePut=p_namePut;
			idPut=p_idPut;
			_companyNo=p_companyNo;
			itsm.cim.configureItemUtil.selectCI('#selectCI_Window','#selectCI_CICategoryTree','#selectCI_CIGrid','','#selectCI_ConfirmSelect');
			
		},
		
		
		/**
		 * @description 选择关联配置项（简单）.
		 * @param tableId 显示配置项列表id
		 * @param p_ciQueryParam 查询配置项参数
		 * @param p_companyNo 公司id
		 */
		selectCISM:function(tableId,p_companyNo,p_ciQueryParam){
			_companyNo=p_companyNo;
			_ciQueryParam=p_ciQueryParam;
			$('#selectCI_south_msg').hide();//隐藏提示信息
			$('#selectCI_south_btn').show();//显示多选按钮
			ciselect_mutiTag=true;
			hiddenName='';
//			if(_ciQueryParam!=undefined || _ciQueryParam!=null || _ciQueryParam!=''){
//				$('#selectCI_CICategoryTree').html('');
//				$('#selectCI_CICategoryTree').append('<div style="padding:5px"><b>'+i18n['label_change_object']+':'+p_ciQueryParam.substring(p_ciQueryParam.indexOf('changeObjectName=')+17)+'</b></div>');
//				itsm.cim.configureItemUtil.selectCI('#selectCI_Window','','#selectCI_CIGrid',tableId,'#selectCI_ConfirmSelect')
//			}else{
//				$('#selectCI_CICategoryTree').html('');
//				itsm.cim.configureItemUtil.selectCI('#selectCI_Window','#selectCI_CICategoryTree','#selectCI_CIGrid',tableId,'#selectCI_ConfirmSelect')
//			}
			itsm.cim.configureItemUtil.selectCI('#selectCI_Window','#selectCI_CICategoryTree','#selectCI_CIGrid',tableId,'#selectCI_ConfirmSelect');
		},
		
		
		/**
		 * @description 选择关联配置项（问题）
		 * @param tableId 显示配置项列表id
		 * @param p_companyNo 公司id
		 */
		problemSelectCI:function(tableId,p_companyNo){
			_companyNo=p_companyNo;
			$('#selectCI_south_msg').hide();//隐藏提示信息
			$('#selectCI_south_btn').show();//显示多选按钮
			ciselect_mutiTag=true;
			hiddenName='problem';
			itsm.cim.configureItemUtil.selectCI('#selectCI_Window','#selectCI_CICategoryTree','#selectCI_CIGrid',tableId,'#selectCI_ConfirmSelect')
			
			
		},
		
		/**
		 * @description 选择关联配置项（请求）
		 * @param tableId 显示配置项列表id
		 * @param creator 登录名
		 * @param p_companyNo 公司id
		 */
		requestSelectCI:function(tableId,p_companyNo,creator){
			_companyNo=p_companyNo;
			_creator = creator;
			$('#selectCI_south_msg').hide();//隐藏提示信息
			$('#selectCI_south_btn').show();//显示多选按钮
			ciselect_mutiTag=true;
			hiddenName='request';
			itsm.cim.configureItemUtil.selectCI('#selectCI_Window','#selectCI_CICategoryTree','#selectCI_CIGrid',tableId,'#selectCI_ConfirmSelect')
		},
		/**
		 * @description 选择关联配置项.
		 * @param p_windowID 窗口id
		 * @param p_treeID 显示树id
		 * @param p_gridId 列表id
		 * @param tableId 显示配置项列表id
		 * @param p_selectId 登录名
		 */
		selectCI:function(p_windowID,p_treeID,p_gridId,p_tableID,p_selectId){
			windowId=p_windowID;//窗口编号
			treeId=p_treeID;//树结构编号
			gridId=p_gridId;//数据列表编号
			tableId=p_tableID;//表格编号
			multiSelectId=p_selectId;//多选按钮编号

			windows(windowId.replace('#',''),{width:750,height:450});
//			$(windowId+"_Tabs").tabs();//实例化Tab页面
			itsm.cim.configureItemUtil.showCITree();//加载树结构
			$("#selectCI_Search").show();
			if(_creator!=''){
				itsm.cim.configureItemUtil.selectCIGridByCreator();//根据请求人加载配置项列表
			}else{
				itsm.cim.configureItemUtil.selectCIGrid('');//加载配置项列表
			}
			
			if(loadFlag=="0"){
				loadFlag="1";
				$(multiSelectId).click(itsm.cim.configureItemUtil.selectMultiButtonClick);//绑定
			}
			setTimeout(function(){//隐藏显示复选框
				if(ciselect_mutiTag){
					$(gridId).jqGrid('showCol', 'cb');
				}else{
					$(gridId).jqGrid('hideCol', 'cb');
				}
			},0);
			
		},
		
		/**
		 * @description 显示配置项分类树.
		 */
		showCITree:function(){
			$(treeId).jstree({
				"json_data":{
				    ajax: {
				    	url : "ciCategory!getConfigurationCategoryTree.action?findAll=false&userName="+userName,
				    	data:function(n){
					    	  return {'parentEventId':n.attr ? n.attr("cno").replace("node_",""):0};//types is in action
					    },
				    	cache:false}
				},
				plugins : ["themes", "json_data", "ui", "crrm"]
				})
				//.bind('loaded.jstree', function(e,data){data.inst.open_all(-1);})
				.bind('select_node.jstree',function(e,data){
					 categoryNo=data.rslt.obj.attr('cno');
					 itsm.cim.configureItemUtil.selectCIGrid(categoryNo);
					//$(gridId).jqGrid('setGridParam',{page:1,url:'ci!cItemsFind.action?ciQueryDTO.categoryNo='+categoryNo+'&ciQueryDTO.companyNo='+_companyNo+'&ciQueryDTO.loginName='+userName}).trigger('reloadGrid');
				});
			
		},
		
		
		
		/**
		 * 动作格式化.
		 */
		CIGridFormatter:function(cell,event,data){
	
			return '<div style="padding:0px">'+
			'<a href=javascript:itsm.cim.configureItemUtil.selectMultiButtonClick_aff('+event.rowId+') title="'+i18n['check']+'">'+
			'<img src="../images/icons/ok.png"/></a>'+
			'</div>';
		},
		/**
		 * 根据请求人加载配置项列表
		 */
		selectCIGridByCreator:function(){
			$("#selectCIUse_ciGrid_fullName").val('');
			$("#selectCIOwner_ciGrid_fullName").val('');
			$("#selectCI_Search").hide();
			if($(gridId).html()!=''){
				var ciData = $('#ciUser_searchForm').getForm();
				var postData = $(gridId).jqGrid("getGridParam", "postData");
				$.extend(postData,ciData);  //将postData中的查询参数覆盖为空值
				var ci_url ='ci!findPageConfigureItemByUser.action?ciQueryDTO.companyNo='+_companyNo+'&'+_ciQueryParam;
				$(gridId).jqGrid('setGridParam',{page:1,url:ci_url,postData:{'ciQueryDTO.loginName':_creator}}).trigger('reloadGrid');
				$(gridId).trigger("reloadGrid"); 
			}else{
				var params = $.extend({},jqGridParams,{
					url:'ci!findPageConfigureItemByUser.action?ciQueryDTO.companyNo='+_companyNo+'&'+_ciQueryParam,
					postData:{'ciQueryDTO.loginName':_creator},
					colNames:[i18n['category'],i18n['number'],i18n['name'],i18n['status'],i18n['check'],''],
					colModel:[
							  {name:'categoryName',align:'center',width:20,sortable:false},
							  {name:'cino',align:'center',width:30,formatter:function(cellvalue, options, rowObject){
									 return rowObject.cino; 
							  }},
							  {name:'ciname',align:'center',width:30,formatter:function(cellvalue, options, rowObject){
									 return rowObject.ciname; 
							  }},
							  {name:'status',align:'center',width:10,sortable:false},
							  {name:'act',align:'center',width:10,formatter:itsm.cim.configureItemUtil.CIGridFormatter,sortable:false},
							  {name:'ciId',hidden:true},
							  ],
					toolbar:false,
					jsonReader: $.extend(jqGridJsonReader,{id: "ciId"}),
					sortname:'ciId',
					pager:gridId+'Pager',
					ondblClickRow:function(rowid,iRow,iCol,e){
						itsm.cim.configureItemUtil.selectMultiButtonClick_db(rowid);
					}
				});
				$(gridId).jqGrid(params);
				$(gridId).navGrid(gridId+'Pager',navGridParams);
			
				
				$(gridId).navButtonAdd(gridId+'Pager',{caption:"",buttonicon:"ui-icon-search",onClickButton:function(){
					windows('select_configureItem_search_win',{width: 400});
					$('#selectConfigureItem_common_dosearch').click(function(){
						itsm.cim.configureItemUtil.configureItemSearch();
					});
				},position:"last"});
			}
		},
		
		
		
		
		
		/**
		 * @description 配置項列表.
		 */
		selectCIGrid:function(no){
			$("#selectCIOwner_ciGrid_fullName").val('');
			var user=$("#request_userName").val();
			if($(gridId).html()!=''){
				if(no==''){
					categoryNo='';
					$("#selectCIUse_ciGrid_fullName").val($("#request_userName").val());
				}else{
					$("#selectCIUse_ciGrid_fullName").val();
				}
				var ciData = $('#ciUser_searchForm').getForm();
				var postData = $(gridId).jqGrid("getGridParam", "postData");
				$.extend(postData,ciData);  //将postData中的查询参数覆盖为空值
				var ci_url ='ci!cItemsFind.action?ciQueryDTO.categoryNo='+categoryNo;
				$(gridId).jqGrid('setGridParam',{page:1,url:ci_url}).trigger('reloadGrid');
				$(gridId).trigger("reloadGrid"); 
			}else{
				$("#selectCIUse_ciGrid_fullName").val($("#request_userName").val());
				var _postData={};
				$.extend(_postData, {'ciQueryDTO.companyNo':_companyNo,'ciQueryDTO.loginName':userName,'ciQueryDTO.userName':user});
				var params = $.extend({},jqGridParams,{
					url:'ci!cItemsFind.action',
					postData:_postData,
					colNames:[i18n['category'],i18n['number'],i18n['name'],i18n['status'],i18n['check'],''],
					colModel:[
							  {name:'categoryName',align:'center',width:20,sortable:false},
							  {name:'cino',align:'center',width:30,formatter:function(cellvalue, options, rowObject){
									 return rowObject.cino; 
							  }},
							  {name:'ciname',align:'center',width:30,formatter:function(cellvalue, options, rowObject){
									 return rowObject.ciname; 
							  }},
							  {name:'status',align:'center',width:10,sortable:false},
							  {name:'act',align:'center',width:10,formatter:itsm.cim.configureItemUtil.CIGridFormatter,sortable:false},
							  {name:'ciId',hidden:true},
							  ],
					toolbar:false,
					jsonReader: $.extend(jqGridJsonReader,{id: "ciId"}),
					sortname:'ciId',
					pager:gridId+'Pager',
					ondblClickRow:function(rowid,iRow,iCol,e){
						itsm.cim.configureItemUtil.selectMultiButtonClick_db(rowid);
					}
				});
				$(gridId).jqGrid(params);
				$(gridId).navGrid(gridId+'Pager',navGridParams);
			
				
				$(gridId).navButtonAdd(gridId+'Pager',{caption:"",buttonicon:"ui-icon-search",onClickButton:function(){
					windows('select_configureItem_search_win',{width: 400});
					$('#selectConfigureItem_common_dosearch').click(function(){
						itsm.cim.configureItemUtil.configureItemSearch();
					});
				},position:"last"});
				
				$('#selectCI_SearchSelect').click(function(){
					var ciData = $('#ciUser_searchForm').getForm();
					var postData = $(gridId).jqGrid("getGridParam", "postData");
					$.extend(postData,ciData);  //将postData中的查询参数覆盖为空值
					$(gridId).trigger('reloadGrid',[{"page":"1"}]);
				});
			}
		},
		/**
		 * 搜索配置项
		 */
		configureItemSearch:function(){
			var _url = 'ci!cItemsFind.action';
			var sdata=$('#select_configureItem_search_win form').getForm();
			var postData = $(gridId).jqGrid("getGridParam", "postData");
			$.extend(postData,sdata);  
			
			$(gridId).jqGrid('setGridParam',{page:1,url:_url}).trigger('reloadGrid');
		},
		
		
		/**
		 * @description 移除行
		 */
		removeRow:function(rowId){
			msgConfirm(i18n['msg_msg'],'<br/>'+i18n['msg_confirmDelete'],function(){
				$(tableId+' tbody '+rowId).remove();
				$(rowId).remove();
				
				$(".hisdiv").css({"height":"auto"});
				msgShow(i18n['deleteSuccess'],'show');
			});
		},
		
		/**
		 * @description 添加行.
		 */
		addRow:function(rowData){
			
			if ( $(tableId+' #ref_ci_'+rowData.ciId).length==0 ) {//避免重复
				var newRowHTML='';
				
				if(hiddenName!=''){
					newRowHTML=problemRowHTML;
				}else{
					newRowHTML=rowHTML;
				}
				newRowHTML=newRowHTML.replace(/CIID/g,rowData.ciId)
				.replace(/CINO/g,rowData.cino)
				.replace(/CINAME/g,rowData.ciname)
				.replace(/CATEGORYNAME/g,rowData.categoryName)
				.replace(/CISTATUS/g,rowData.status)
				.replace(/DELETE/g,i18n['deletes']); 
				
				$(newRowHTML).appendTo(tableId+' tbody');
			}	
		},

		/**
		 * @description 选中的配置项（多选）
		 */
		selectMultiButtonClick:function(){
			if(tableId!=''){//多选状态
				
				var rowIds = $(gridId).getGridParam('selarrrow');
				
				if(rowIds==''){
					msgAlert(i18n['msg_atLeastChooseOneData'],'info');
				}else{
					
					for(var i=0;i<rowIds.length;i++){
						
						var rowData=$(gridId).getRowData(rowIds[i]);
						itsm.cim.configureItemUtil.addRow(rowData);//循环添加行
					}
					
					tableId='';//清空结果
					
					$(windowId).dialog('close');//关闭选择配置项窗口
				}
				
			}else{//单选状态
				var rowId = $(gridId).getGridParam('selrow');
				var rowData=$(gridId).getRowData(rowId);
				$(namePut).val(rowData.ciname);
				$(idPut).val(rowData.ciId);
				
				namePut='';//还原
				idPut='';
				
				$(windowId).dialog('close');//关闭选择配置项窗口
			}
		},
		/**
		 * @description 选中的配置项（多选）
		 */
		selectMultiButtonClick_db:function(rowId){
			if(tableId!=''){//多选状态
//				var rowIds = $(gridId).getGridParam('selarrrow');
				var rowIds = [rowId];
				if(rowIds==''){
					msgAlert(i18n['msg_atLeastChooseOneData'],'info');
				}else{
					
					for(var i=0;i<rowIds.length;i++){
						
						var rowData=$(gridId).getRowData(rowIds[i]);
						itsm.cim.configureItemUtil.addRow(rowData);//循环添加行
					}
					
					tableId='';//清空结果
					
					$(windowId).dialog('close');//关闭选择配置项窗口
					
				}
			}else{//单选状态
//				var rowId = $(gridId).getGridParam('selrow');
				var rowId = rowId;
				var rowData=$(gridId).getRowData(rowId);
				$(namePut).val(rowData.ciname);
				$(idPut).val(rowData.ciId);
				
				namePut='';//还原
				idPut='';
				
				$(windowId).dialog('close');//关闭选择配置项窗口
			}
		},
		
		/**
		 * @description 选中的配置项（多选）
		 */
		selectMultiButtonClick_aff:function(rowId){
			
			if(tableId!=''){//多选状态
				var rowIds = [rowId];				
//				var rowIds = $(gridId).getGridParam('selarrrow');
				
				if(rowIds==''){
					msgAlert(i18n['msg_atLeastChooseOneData'],'info');
				}else{
					for(var i=0;i<rowIds.length;i++){
						var rowData=$(gridId).getRowData(rowIds[i]);
						itsm.cim.configureItemUtil.addRow(rowData);//循环添加行
					}
					tableId='';//清空结果
					$(windowId).dialog('close');//关闭选择配置项窗口
				}
			}else{//单选状态
				var rowIds = rowId;
//				var rowId = $(gridId).getGridParam('selrow');
				var rowData=$(gridId).getRowData(rowId);
				$(namePut).val(rowData.ciname);
				$(idPut).val(rowData.ciId);
				
				namePut='';//还原
				idPut='';
				
				$(windowId).dialog('close');//关闭选择配置项窗口
			}
		},
		/**
		 * 查看配置项
		 */
		lookConfigureItemInfo:function(ciId){
			basics.tab.tabUtils.reOpenTab("ci!findByciId.action?ciEditId="+ciId,i18n['ci_configureItemInfo'])
		},
		/**
		 * @description 配置项关联关系
		 * */
		ciRelevanceTree:function(ciId,windowId,treeId){
			this.loadTreeFlag="0";
			$("#"+treeId).jstree({
				json_data: {
					ajax: {
					  type:"post",
					  url : "ciRelevance!ciReleTree.action",
				      data: {'relateForward': i18n['related'].replace(' ','_'),'relateBack':i18n['isAssociated'].replace(' ','_'),'ciRelevanceId' :ciId},//types is in action
	                  dataType: 'json',
	                  cache:false
					}
				},
				"plugins" : ["themes", "json_data", "ui", "crrm", "cookies", "types", "hotkeys"]
				
			})
			.bind('loaded.jstree', function(e,data){data.inst.open_all(-1);})
			.bind('select_node.jstree',function(e,data){
				var cno=data.rslt.obj.attr('ciRelevanceId');
				if(cno!=null && cno!=''){
					basics.tab.tabUtils.reOpenTab('ci!findByciId.action?ciEditId='+cno,i18n['ci_configureItemInfo']);
				}
			});
			
			
			setTimeout(function(){
				
				windows(windowId);
			},10);
		},
		
		/**
		 * 动作格式化.
		 */
		userGridFormatter:function(){
			return '<div style="padding:0px">'+
			'<a href="javascript:itsm.cim.configureItemUtil.selectConfigureItemClick(\''+requestCiId+'\')" title="'+i18n['check']+'">'+
			'<img src="../images/icons/ok.png"/></a>'+
			'</div>';
		},
		/**
		 * 根据用户名查找配置项
		 */
		findConfigureItemByLoginName:function(name,id,loginName){
			requestCiId=id;
			configureItemInputId=name;
			
			if(basics.ie6.htmlIsNull("#select_configureItem_by_userName_grid")){
				
				var ci_url ='ci!findPageConfigureItemByUser.action';
				$('#select_configureItem_by_userName_grid').jqGrid('setGridParam',{page:1,url:ci_url,postData:{'ciQueryDTO.loginName':loginName}}).trigger('reloadGrid');
				$('#select_configureItem_by_userName_grid').trigger("reloadGrid"); 
			}else{
				var params = $.extend({},jqGridParams,{
					url:'ci!findPageConfigureItemByUser.action',
					postData:{'ciQueryDTO.loginName':loginName},
					colNames:[i18n['category'],i18n['number'],i18n['name'],i18n['status'],i18n['check'],i18n['use'],i18n['check']],
					colModel:[
							  {name:'categoryName',align:'center',width:100,sortable:false},
							  {name:'cino',align:'center',width:100,formatter:itsm.cim.configureItemUtil.cinoGridFormatter},
							  {name:'ciname',align:'center',width:125,formatter:itsm.cim.configureItemUtil.cinameGridFormatter},
							  {name:'status',align:'center',width:100,sortable:false},
							  {name:'ciId',hidden:true},
							  {name:'userName',align:'center',width:100,sortable:false},
							  {name:'act', width:80,sortable:false,align:'center',formatter:itsm.cim.configureItemUtil.userGridFormatter}
							  ],
					toolbar:false,
					multiselect:false,
					jsonReader: $.extend(jqGridJsonReader,{id: "ciId"}),
					sortname:'ciId',
					pager:'#select_configureItem_by_userName_pager',
					ondblClickRow:function(){
						itsm.cim.configureItemUtil.selectConfigureItemClick(id);
					}
				});
				$('#select_configureItem_by_userName_grid').jqGrid(params);
				$('#select_configureItem_by_userName_grid').navGrid('#select_configureItem_by_userName_pager',navGridParams);
				$('#select_configureItem_by_userName_grid').navButtonAdd('#select_configureItem_by_userName_pager',{caption:"",buttonicon:"ui-icon-search",onClickButton:function(){
					windows('select_configureItem_by_userName_search_win',{width: 400});
				},position:"last"});
				$('#select_configureItem_by_userName_dosearch').click(function(){itsm.cim.configureItemUtil.configureItem_by_userName_search(loginName)});
			}
			
			setTimeout(function(){
				windows('select_configureItem_by_userName_win',{width: 630,height:370});
			},10);
		},
		/**
		 * 根据已有权限查找配置项
		 */
		findConfigureItemByPower:function(name,id,loginName){
			requestCiId=id;
			configureItemInputId=name;
			if(basics.ie6.htmlIsNull("#select_configureItem_by_userName_grid")){
				var ci_url ='ci!cItemsFind.action';
				$('#select_configureItem_by_userName_grid').jqGrid('setGridParam',{page:1,url:ci_url,postData:{'ciQueryDTO.loginName':loginName}}).trigger('reloadGrid');
				$('#select_configureItem_by_userName_grid').trigger("reloadGrid"); 
			}else{
				var params = $.extend({},jqGridParams,{
					url:'ci!cItemsFind.action',
					postData:{'ciQueryDTO.loginName':loginName},
					colNames:[i18n['category'],i18n['number'],i18n['name'],i18n['status'],i18n['check'],i18n['use'],i18n['check']],
					colModel:[
							  {name:'categoryName',align:'center',width:100,sortable:false},
							  {name:'cino',align:'center',width:100,formatter:itsm.cim.configureItemUtil.cinoGridFormatter},
							  {name:'ciname',align:'center',width:125,formatter:itsm.cim.configureItemUtil.cinameGridFormatter},
							  {name:'status',align:'center',width:100,sortable:false},
							  {name:'ciId',hidden:true},
							  {name:'userName',align:'center',width:100,sortable:false},
							  {name:'act', width:80,sortable:false,align:'center',formatter:itsm.cim.configureItemUtil.userGridFormatter}
							  ],
					toolbar:false,
					multiselect:false,
					jsonReader: $.extend(jqGridJsonReader,{id: "ciId"}),
					sortname:'ciId',
					pager:'#select_configureItem_by_userName_pager',
					ondblClickRow:function(){
						itsm.cim.configureItemUtil.selectConfigureItemClick(id);
					}
				});
				$('#select_configureItem_by_userName_grid').jqGrid(params);
				$('#select_configureItem_by_userName_grid').navGrid('#select_configureItem_by_userName_pager',navGridParams);
				$('#select_configureItem_by_userName_grid').navButtonAdd('#select_configureItem_by_userName_pager',{caption:"",buttonicon:"ui-icon-search",onClickButton:function(){
					windows('select_configureItem_by_userName_search_win',{width: 400});
				},position:"last"});
				$('#select_configureItem_by_userName_dosearch').click(function(){itsm.cim.configureItemUtil.configureItemsearch(loginName)});
			}
			
			setTimeout(function(){
				windows('select_configureItem_by_userName_win',{width: 630,height:370});
			},10);
		},
		cinoGridFormatter:function(cell,event,data){
			return data.cino;
		},
		cinameGridFormatter:function(cell,event,data){
			return data.ciname;
		},
		/**
		 * 根据用户名搜索配置项
		 */
		configureItem_by_userName_search:function(loginName){
			var _url = 'ci!findPageConfigureItemByUser.action';
			var sdata=$('#select_configureItem_by_userName_search_win form').getForm();
			var postData = $('#select_configureItem_by_userName_grid').jqGrid("getGridParam", "postData");
			$.extend(postData,sdata);
			$.extend(postData,{'ciQueryDTO.loginName':loginName});
			$('#select_configureItem_by_userName_grid').jqGrid('setGridParam',{page:1,url:_url}).trigger('reloadGrid');
		},
		/**
		 * 根据搜索配置项
		 */
		configureItemsearch:function(loginName){
			var _url = 'ci!cItemsFind.action';
			var sdata=$('#select_configureItem_by_userName_search_win form').getForm();
			var postData = $('#select_configureItem_by_userName_grid').jqGrid("getGridParam", "postData");
			$.extend(postData,sdata);
			$.extend(postData,{'ciQueryDTO.loginName':loginName});
			$('#select_configureItem_by_userName_grid').jqGrid('setGridParam',{page:1,url:_url}).trigger('reloadGrid');
		},
		/**
		 * 选择配置项
		 */
		selectConfigureItemClick:function(id){
			var rowId = $('#select_configureItem_by_userName_grid').getGridParam('selrow');
			var rowData=$('#select_configureItem_by_userName_grid').getRowData(rowId);
			$(configureItemInputId).val(rowData.ciname);
			$(id).val(rowData.ciId);
			$('#select_configureItem_by_userName_win').dialog('close');		
		},
		/**
		 * enduser 选择配置项
		 */
		enduserSelectCISM:function(tableId,p_companyNo,p_ciQueryParam){
			_companyNo=p_companyNo;
			_ciQueryParam=p_ciQueryParam;
			$('#selectCI_list_south_msg').hide();//隐藏提示信息
			$('#selectCI_list_south_btn').show();//显示多选按钮
			ciselect_mutiTag=true;
			hiddenName='request';
			itsm.cim.configureItemUtil.enduserSelectCI('#selectCI_list_window','#selectCI_list_CIGrid',tableId,'#selectCI_list_ConfirmSelect');
		},
		/**
		 * enduser 选择配置项
		 */
		enduserSelectCI:function(p_windowID,p_gridId,p_tableID,p_selectId){
			windowId=p_windowID;//窗口编号
			gridId=p_gridId;//数据列表编号
			tableId=p_tableID;//表格编号
			multiSelectId=p_selectId;//多选按钮编号
			windows(windowId.replace('#',''),{width:750,height:450});
			itsm.cim.configureItemUtil.enduserSelectCIGrid('');//加载配置项列表
			if(loadFlag=="0"){
				loadFlag="1";
				$(multiSelectId).click(itsm.cim.configureItemUtil.selectMultiButtonClick);//绑定
			}
			setTimeout(function(){//隐藏显示复选框
				if(ciselect_mutiTag){
					$(gridId).jqGrid('showCol', 'cb');
				}else{
					$(gridId).jqGrid('hideCol', 'cb');
				}
			},0);
		},
		/**
		 * enduser 查询配置项
		 */
		enduserSelectCIGrid:function(){
			if($(gridId).html()!=''){
				var ciData = {};
				var postData = $(gridId).jqGrid("getGridParam", "postData");
				$.extend(postData,ciData);  //将postData中的查询参数覆盖为空值
				var ci_url ='ci!findPageConfigureItemByUser.action?ciQueryDTO.companyNo='+_companyNo+'&'+_ciQueryParam;
				$(gridId).jqGrid('setGridParam',{page:1,url:ci_url,postData:{'ciQueryDTO.loginName':fullName}}).trigger('reloadGrid');
				$(gridId).trigger("reloadGrid"); 
			}else{
				var params = $.extend({},jqGridParams,{
					url:'ci!findPageConfigureItemByUser.action?ciQueryDTO.companyNo='+_companyNo+'&'+_ciQueryParam,
					postData:{'ciQueryDTO.loginName':fullName},
					colNames:[i18n['category'],i18n['number'],i18n['name'],i18n['status'],i18n['check'],''],
					colModel:[
							  {name:'categoryName',align:'center',width:20,sortable:false},
							  {name:'cino',align:'center',width:30,formatter:function(cellvalue, options, rowObject){
									 return rowObject.cino; 
							  }},
							  {name:'ciname',align:'center',width:30,formatter:function(cellvalue, options, rowObject){
									 return rowObject.ciname; 
							  }},
							  {name:'status',align:'center',width:10,sortable:false},
							  {name:'act',align:'center',width:10,formatter:itsm.cim.configureItemUtil.CIGridFormatter,sortable:false},
							  {name:'ciId',hidden:true},
							  ],
					toolbar:false,
					jsonReader: $.extend(jqGridJsonReader,{id: "ciId"}),
					sortname:'ciId',
					pager:gridId+'Pager',
					ondblClickRow:function(rowid,iRow,iCol,e){
						itsm.cim.configureItemUtil.selectMultiButtonClick_db(rowid);
					}
				});
				$(gridId).jqGrid(params);
				$(gridId).navGrid(gridId+'Pager',navGridParams);
			
				
				$(gridId).navButtonAdd(gridId+'Pager',{caption:"",buttonicon:"ui-icon-search",onClickButton:function(){
					windows('select_configureItem_search_win',{width: 400});
					$('#selectConfigureItem_common_dosearch').click(function(){
						itsm.cim.configureItemUtil.enduserConfigureItemSearch();
					});
				},position:"last"});
			}
		},
		/**
		 * enduser 搜索配置项
		 */
		enduserConfigureItemSearch:function(){
			var _url = 'ci!findPageConfigureItemByUser.action?ciQueryDTO.companyNo='+_companyNo+'&'+_ciQueryParam;
			var sdata=$('#select_configureItem_search_win form').getForm();
			var postData = $(gridId).jqGrid("getGridParam", "postData");
			$.extend(postData,sdata);  
			$(gridId).jqGrid('setGridParam',{page:1,url:_url}).trigger('reloadGrid');
		}
		
	}
}();