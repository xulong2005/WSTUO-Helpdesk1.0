$package('itsm.cim');

$import('itsm.cim.ciCategoryTree');//配置项分类树
$import("itsm.cim.getConfig");
$import("itsm.problem.relatedProblem");
$import("itsm.cim.leftMenu");
$import("itsm.cim.configureItem");
$import('itsm.cim.configureItemCategory');
$import("itsm.cim.configureItemAdd");
$import("itsm.cim.configureItemEdit");
$import("itsm.cim.configureItemInfo");
$import("itsm.cim.configureItemInfoFormCustom");