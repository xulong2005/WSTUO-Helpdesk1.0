$package('itsm.cim');
$import('itsm.cim.configureItemCategory');
/**  
 * @fileOverview 请求左边菜单主函数.
 * @author QXY
 * @version 1.0  
 */  
 /**  
 * @author QXY  
 * @constructor 
 * @description 请求左边菜单主函数.
 * @date 2010-4-28
 * @since version 1.0 
 * @returns
 * @param select
 */
itsm.cim.leftMenu=function(){

	return {
		
		/**
		 * 公司统计
		 */
		companyStat:function(){
			//$('#configureItemCompanyDiv').hide();
			var configureItemCompanyId =  $("#configureItemCompanyDiv a[class='statsLeftDiv_a']").attr("id");
			if(versionType=='ITSOP'){
				$.post('ci!configureItemDataCountByCompanyNo.action','loginName='+userName,function(data){
					$('#companyConfigureItemDataCount').html('');
					if(data!=null && data.length>1){
						$('#configureItemCompanyDiv').show();
						var str='';
						for(var i=0;i<data.length;i++){
							str='<a  id="configureItemCompany_{orgNo}"  style="cursor:pointer" href="javascript:itsm.cim.leftMenu.companyStatSearch(\'{orgNo}\')">{companyName}</a><span style="color: #F00">&nbsp;&nbsp;[ {total} ]</span><br>'
							str=str.replace(/{companyName}/g,data[i].orgName)
							.replace(/{total}/g,data[i].total)
							.replace(/{orgNo}/g,data[i].orgNo);
							
							$('#companyConfigureItemDataCount').append(str);
						}
						$("#companyConfigureItemDataCount a").live("click",itsm.cim.leftMenu.companyStatEffect);
					}else{
						$('#configureItemCompanyDiv').hide();
					}
					$("#configureItemCompanyDiv #"+configureItemCompanyId).attr("class","statsLeftDiv_a");
					//列表下方显示
					basics.bottomMain.resizeWithCimChart();
				});
			}else{
				$('#configureItemCompanyDiv').hide();
				basics.bottomMain.resizeWithCimChart();
			}
			
		},
		/**
		 * 根据公司查询配置项
		 */
		companyStatSearch:function(orgNo){
			var _url='ci!cItemsFind.action';
			var _postData={};
			if(orgNo!='' && orgNo!=null){
				$.extend(_postData,{'ciQueryDTO.companyNo':orgNo,'ciQueryDTO.loginName':userName});
			}
			$('#configureGrid').jqGrid('setGridParam',{postData:null});
			$('#configureGrid').jqGrid('setGridParam',{postData:_postData,page:1,url:_url}).trigger('reloadGrid');
		},
		companyStatEffect:function(){
			$("#companyConfigureItemDataCount a").removeClass("statsLeftDiv_a");
			$(this).addClass("statsLeftDiv_a");
		},
		/**
		 * @description 请求统计搜索
		 * @param type 统计类型
		 * @param id 饼图中每块对应的Id
		 * @param groupField 根据什么进行统计
		 */
		ciCountSearchByPieChart:function(id,groupField,label){
			var _url='ci!cItemsFind.action';
			var _postData={};
			$.extend(_postData,{'ciQueryDTO.loginName':userName});
			if(groupField=="status"){
				$.extend(_postData,{'ciQueryDTO.statusId':id});
			}else if(groupField=="loc"){
				$.extend(_postData,{'ciQueryDTO.locId':id});
			}
			if(id==0 &&(label=='unknown' || label == 'other')){
				$.extend(_postData,{'ciQueryDTO.label':label});
			}
			$('#configureGrid').jqGrid('setGridParam',{postData:null});
			$('#configureGrid').jqGrid('setGridParam',{postData:_postData,page:1,url:_url}).trigger('reloadGrid');
		},
		/**
		 * 初始化
		 */
		init:function(){
			
			setTimeout(function(){
				
				endLoading();
				
			},1000);
		}
	}
}();
$(document).ready(itsm.cim.leftMenu.init);


