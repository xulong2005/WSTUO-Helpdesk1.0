$package("itsm.cim") ;
$import("common.config.dictionary.dataDictionaryUtil");
$import("common.security.userUtil");
$import('itsm.itsop.selectCompany');
$import('itsm.cim.ciCategoryTree');
$import('common.config.category.ciCategoryCatalog');
$import('common.config.attachment.chooseAttachment');
$import('common.knowledge.knowledgeTree');
$import('common.security.includes.includes');
$import('common.config.includes.includes');
$import('itsm.cim.cimCommon');

$import('common.config.formCustom.formControl');
$import('common.config.formCustom.formControlImpl');
$import('common.config.category.serviceCatalog');
/**  
 * @fileOverview 配置项添加
 * @author wing
 * @version 1.0  
 */  
 /**  
 * @author QXY  
 * @constructor configureItemAdd
 * @description 配置项添加
 * @date 2015-10-17
 * @since version 1.0 
 */
itsm.cim.addCimFormCustom=function(){
	var add_cim_attrJson = "";
	var is_show_border ="0" ;
	var cimTabsName = [];
	return{
		/**
		 * 过滤字符
		 */
		stringItem:function(str){
			 var pattern=new RegExp("[`~%!@#^=?~！@#￥……&——？*]");
			 //[]内输入你要过滤的字符，这里是我的 
			 var rs=pattern.test(str);
			
			 return rs;
		},
		/**
		 * @description 保存到数据库
		 */
		saveConfigureItem:function(){
			var categoryNo = $('#ci_add_categoryNo2').val();
			if(categoryNo!=null&&categoryNo!=''){
				$.each($("#addCim_formField_from :input[attrType=Lob]"),function(index,obj){
					var oEditorObj = CKEDITOR.instances[$(obj).attr("id")];
					$(obj).val(trim(oEditorObj.getData()));
				});
				$.each($("#addCim_tabs form :input[attrType=Lob]"),function(index,obj){
					var oEditorObj = CKEDITOR.instances[$(obj).attr("id")];
					$(obj).val(trim(oEditorObj.getData()));
				});
				var validate = true;
				$.each($("#addCim_tabs form"),function(index,obj){
					 if(!$(obj).form('validate')){
						 validate = false ;
					 }
				});
			  if($('#addCim_formField_from').form('validate') && validate){
				//if($('#ci_add_eavAttributet_form').form('validate')){
					var ci_add_cino=trim($('#addCim_formField_from #ci_add_cino').val());
					var boo=itsm.cim.addCimFormCustom.stringItem(ci_add_cino);
					if(boo==true){
						$('#addCim_formField_from #ci_add_cino').focus();
						//$('#ci_add_cino').css("color","red");
						msgShow(i18n['label_string'],'show');
						return false;
					}
					itsm.cim.ciCategoryTree.getFormAttributesValue("#addCim_formField_from");
					itsm.cim.ciCategoryTree.getFormAttributesValue("#addCim_tabs form");
					$.each($("#addCim_formField_from input[attrtype='String']"),function(ind,val){
						$(this).val(common.security.xssUtil.html_encode($(this).val()));
					});
					$.each($("#addCim_tabs form input[attrtype='String']"),function(ind,val){
						$(this).val(common.security.xssUtil.html_encode($(this).val()));
					});
					var _params = $('#configureItemAdd_panel form,#addCim_tabs form').serialize();
					startProcess();
					$.post('ci!existCIByCiNo.action','ciQueryDTO.cino='+ci_add_cino,function(data){
						if(data){
							msgAlert(i18n['err_ciNoExist'],'info');
							endProcess();
						}else{
							var url='ci!cItemSave.action?ciDto.isShowBorder='+is_show_border+'&ciDto.isNewForm=true';
							
							$.post(url, _params, function(){
								endProcess();
								basics.tab.tabUtils.closeTab(i18n['ci_addConfigureItem']);	
								basics.tab.tabUtils.selectTab(i18n['ci_configureItemAdmin'],function(){
									$('#configureGrid').trigger('reloadGrid');
								});
								msgShow(i18n['saveSuccess'],'show');
								
								if ($('#itsmMainTab').tabs('exists',i18n.ci_configureItemAdmin)){
									itsm.cim.leftMenu.companyStat();
									basics.showChart.cimShowChart();
								}
							});
							
						}
					});
				/*}else{
					msgAlert(i18n.eavAttributet_notNull,'info');
					$('#addCim_tabs').tabs('select', ci_add_eavAttributet);
				}*/
			}
		  }else{
			  msgAlert(i18n.err_ciCategoryNoNotNull,'info');
		  }
		},
		
		/**
		 * @description 配置项分类选择窗口打开
		 **/
		focusInit:function(){
			configureItemTree();
			windows('configureItemAddCategory',{width:400,height:400});
		},

		
		/**
		 * @description 选择请求者.
		 */
		selectCICreator:function(userName){
			common.security.userUtil.selectUser(userName,'','','fullName');

		},
		/**
		 * 返回列表
		 */
		returnConfigureItemList:function(){
			basics.tab.tabUtils.addTab(i18n['ci_configureItemAdmin'],'../pages/itsm/cim/configureItem.jsp');
		},
		/**
		 * 加载模板
		 */
		selectConfigureItemTemplate:function(){
			$("#configureItemTemplate").html("");
			var categoryNo = $('#ci_add_categoryNo2').val();
			if(!categoryNo){
				categoryNo=0;
			}
			$('<option value="">-- '+i18n["common_pleaseChoose"]+' --</option>').appendTo("#configureItemTemplate");
			$.post("template!findAllTemplate.action",{"templateDTO.templateType":"configureItem","templateDTO.categoryNo":categoryNo},function(data){
				if(data!=null && data.length>0){
					for(var i=0;i<data.length;i++){
						$('<option value="'+data[i].templateId+'">'+data[i].templateName+'</option>').appendTo("#configureItemTemplate");
					}
				}
			});
		},
		/**
		 * 选择模板
		 */
		getTemplateValue:function(templateId){
		    $('#addConfigureItem_uploadedAttachments').html('');
		    $('#addConfigureItem_attachments').val('');
			if(templateId!=null && templateId!=""){
				$('#saveconfigureItemTemplateBtn').hide();
				$('#editconfigureItemTemplateBtn').show();
				$.post("template!findByTemplateId.action",{"templateDTO.templateId":templateId},function(data){
					if(data!=null){
						var formCustomId="#addCimFormCustom_setting";
						if(page=="main"){
							formCustomId="#addCimFormCustom";
						}
						if(data.isShowBorder==="1"){
							$(formCustomId).attr("href","../styles/common/addCimFormCustom.css");
						}else{
							$(formCustomId).attr("href","../styles/common/addCimFormCustom-no.css");
						}	
						$('#configureItemTemplate').attr('value',data.templateId);
						$('#configureItemTemplateNameInput').val(data.templateName);
						$('#configureItemTemplateId').val(data.templateId);
						var _param = {"formCustomDTO.ciCategoryNo" : data.dto.categoryNo};
						
						$.post('formCustom!findFormCustomByCiCategoryNo.action',_param,function(formData){
							if(formData!=null&&formData.formCustomContents!==""){
								var formCustomContents = common.config.formCustom.formControlImpl.editHtml(formData.formCustomContents,'addCim_formField');
								$('#addCim_formField').html(formCustomContents);
								$.parser.parse($('#addCim_formField'));
								common.config.formCustom.formControlImpl.formCustomInit('#addCim_formField');
								$.post('ciCategory!findByCategoryId.action','categoryNo='+data.dto.categoryNo,function(res){
									itsm.cim.cimCommon.setCimFormValuesByScheduledTask(data.dto,'#addCim_formField',data.dto.attrVals,res);
									itsm.cim.addCimFormCustom.showBorderCss(data,'#addCim_formField',formCustomId);
									itsm.cim.cimCommon.removeRequiredField(page);
									if(belongsClient=="0"){
										//设置所属客户不可编辑,并且移除图标
										$("#addCim_formField #ci_add_companyNo").next().remove();
										$("#addCim_formField #ci_add_companyName").attr("disabled",true).css("width","90%");							}
									if(pageType=="template"){
										$("#addCim_formField").find("div[class='fieldName']").css("margin-right","0px");
									}
								
								});
							}else{
								itsm.cim.addCimFormCustom.initDefault(data.dto,data,formCustomId);
							}
						});
					}else{
						
					}
					$("#add_ci_serviceDirectory_tbody").html('');
					var serviceNos = data.dto.serviceNos;
					if(serviceNos!=null){
						var html="";
						for(var key in serviceNos){
							html+="<tr id=add_ci_"+key+"><td>"+serviceNos[key]+"</td><td><a onclick=common.knowledge.knowledgeTree.serviceRm("+key+")>"+i18n['deletes']+"</a><input type=hidden name=ciDto.serviceDirectoryNos value="+key+"></td></tr>";
					    }
						$("#add_ci_serviceDirectory_tbody").html(html);
					}
					/*$('#addci_softSetingParam').val(data.dto.softSetingParam);
					$('#addci_softConfigureAm').val(data.dto.softConfigureAm);
					$('#addci_softRemark1').val(data.dto.softRemark1);
					$('#addci_softRemark2').val(data.dto.softRemark2);
					$('#addci_softRemark3').val(data.dto.softRemark3);*/
					$('#depreciationIsZeroYears').val(data.dto.depreciationIsZeroYears);
					$('#ci_systemPlatform').val(data.dto.systemPlatformId);
					//附件
					$('#addConfigureItem_attachments').val(data.dto.attachmentStr);
					var attrArr=data.dto.attachmentStr.split("-s-");
		    		for(var i=0;i<attrArr.length;i++){
		    			var url=attrArr[i].replace("\\", "/");
						if(url!=""){
		        			var attrArrs=url.split("==");
		        			var name=attrArrs[0];
		        			uploadSuccess(name.substr(name.lastIndexOf(".")),attrArrs[1],name,'#addConfigureItem_attachments','#addConfigureItem_uploadedAttachments',"");
						}
		    		}
				});
			}else{
				$('#saveconfigureItemTemplateBtn').show();
				$('#ci_add_categoryNo').val("");
				$('#ci_add_categoryName').val("");
				$('#ci_add_cino').val("");
				$('#ciname').val("");
				$('#model').val("");
				$('#serialNumber').val("");
				$('#barcode').val("");
				$('.control').val("");
				$('#ci_statusAdd').val("");
				$('#ci_brandNameAdd').val("");
				$('#ci_providerAdd').val("");
				$('#ci_locid').val("");
				$('#ci_loc').val("");
				$('#configureItem_add_buyDate').val("");
				$('#configureItem_add_arrivalDate').val("");
				$('#warningDate').val("");
				$('#poNo').val("");
				
				$('#assetsOriginalValue').val("");
				$('#configureItem_add_department').val("");
				$('#workNumber').val("");
				$('#project').val("");
				$('#sourceUnits').val("");
				$('#lifeCycle').val("");
				$('#warranty').val("");
				$('#wasteTime').val("");
				$('#borrowedTime').val("");
				$('#expectedRecoverTime').val("");
				$('#recoverTime').val("");
				$('#usePermissions').val("");
				$('#ci_add_originalUser').val("");
				$('#ci_add_useName').val("");
				$('#ci_add_owner').val("");
				$('#CDI').val("");
				$('#ci_add_eavAttributet table tbody').html("<tr><td colspan=\"2\" style=\"color: red\"><b>"+i18n['label_notExtendedInfo_Ci']+"</b></td></tr>");
				$('#add_ci_serviceDirectory_tbody').html("");
				$('#editconfigureItemTemplateBtn').hide();
				$('#addci_softSetingParam').val("");
				$('#addci_softConfigureAm').val("");
				$('#addci_softRemark1').val("");
				$('#addci_softRemark2').val("");
				$('#addci_softRemark3').val("");
				$('#depreciationIsZeroYears').val("");
				$('#ci_systemPlatform').val("");
			}
		},
		/**
		 * 打开保存模板面板
		 */
		configureItemTemplateNameWin:function(){
			$('#configureItemTemplateNameInput').val("");
			windows('configureItemTemplateName',{close:function(){
				$("#ci_add_categoryName").blur();
			}});
		},
		/**
		 * 保存模板
		 */
		saveConfigureItemTemplate:function(type){
			if(type=="add"){
				$('#configureItemTemplateId').val("");
			}
			var categoryNo = $('#ci_add_categoryNo2').val();
			if( $('#addCim_formField_from').form('validate')&&categoryNo!=null&&categoryNo!=''){
				itsm.cim.ciCategoryTree.getFormAttributesValue("#ci_add_eavAttributet_form");
				var frm = $('#configureItemAdd_panel form,#configureItemTemplateName form').serialize();
				var url = 'ci!saveConfigureItemTemplate.action?templateDTO.isShowBorder='+is_show_border+'&templateDTO.isNewForm=true&templateDTO.categoryNo='+categoryNo;
				//调用
				startProcess();
				$.post(url,frm, function(res){
						endProcess();
						$('#configureItemTemplateName').dialog('close');
						if(type=="add"){
							$('#configureItemTemplate').html("");
							itsm.cim.addCimFormCustom.selectConfigureItemTemplate();
							$('#templateGrid').trigger('reloadGrid');
							msgShow(i18n['saveSuccess'],'show');
						}else{
							$('#templateGrid').trigger('reloadGrid');
							msgShow(i18n['editSuccess'],'show');
						}
				});
			}
		},
		/**
		 * 加载默认的表单
		 */
		initAddCim_formField:function(htmlDivId,initCss){
			$("#"+htmlDivId).load('common/config/formCustom/ciDefaultField.jsp',function(){
				itsm.cim.cimCommon.removeRequiredField(page);
				if(initCss!=null&&initCss!=''){
					$("#"+htmlDivId+" .input").attr("disabled",true);
					 $("#"+htmlDivId+" .control").attr("disabled",true);
					 $("#"+htmlDivId+" .control-img").attr("disabled",true);
					 $("#"+htmlDivId+" #add_search_cim_companyName").hide();
				}else{
					 $("#"+htmlDivId+" .input").attr("disabled",false);
					 $("#"+htmlDivId+" .control").attr("disabled",false);
					 $("#"+htmlDivId+" .control-img").attr("disabled",false);
					 $("#"+htmlDivId+" #add_search_cim_companyName").show();
				}
				 
				itsm.cim.cimCommon.initDefaultForm(htmlDivId);
				common.config.formCustom.formControlImpl.formCustomInitByDataDictionaray('#'+htmlDivId);
				itsm.cim.cimCommon.changeCompany();
				
				/*$.each($("#"+htmlDivId+" :input[attrType=Lob]"),function(ind,val){
					$(val).parent().parent().attr("class","field_options field_options_lob");
				});*/
				
				//$("#"+htmlDivId+" :input[attrType=Lob]").parent().attr("class","field_lob");
				//$("#addCim_formField").find("div[class=field_options] div[class=label]:odd").css("border-left","none");

				if(page=="main"){
		        	$("#addCimFormCustom").attr("href","../styles/common/addCimFormCustom-no.css");
				}else{
					$("#addCimFormCustom_setting").attr("href","../styles/common/addCimFormCustom-no.css");
				}
				if(belongsClient=="0"){
					//设置所属客户不可编辑,并且移除图标
					$("#addCim_formField #ci_add_companyNo").next().remove();
					$("#addCim_formField #ci_add_companyName").attr("disabled",true).css("width","90%");
				}else
					itsm.cim.cimCommon.bindAutoCompleteToCim(htmlDivId);
				
				if(pageType=="template"){
					$("#addCim_formField").find("div[class='fieldName']").css("margin-right","0px");
				}
				//$("#"+htmlDivId+" :input").not(".control").addClass("margintop10");
				$.parser.parse($('#'+htmlDivId));//加上这句，必填才有用	

			});
		},
		/**
		 * 根据分类查询自定义表单
		 */
		showCimFormCustomDesign:function(){
			
			common.config.category.ciCategoryCatalog.selectSingleServiceDirCallback('#ci_add_categoryName2','#ci_add_categoryNo2',function(){
				var categoryNo = $('#ci_add_categoryNo2').val();
				$("#cim_ciCategoryName").html(": &nbsp;"+$("#ci_add_categoryName2").val());
			    $("#c_no").val(categoryNo);
				$.post("formCustom!findFormCustomByCiCategoryNo.action","formCustomDTO.ciCategoryNo="+categoryNo,function(data){
					if(data!=null&&data.formCustomContents!=""){
						itsm.cim.addCimFormCustom.loadCimFormHtmlByFormId(data,'addCim_formField');
						itsm.cim.addCimFormCustom.initAddTabs(data);
					}else{
						itsm.cim.addCimFormCustom.initAddCim_formField('addCim_formField','');
					}
				});
				itsm.cim.addCimFormCustom.selectConfigureItemTemplate();
				$('#cleanCim_ciCategoryName').css("display","");
			},"#addcim_formId");
			
			$("#selectCiSingleDirDiv").dialog({
				close: function(event, ui) {
					setTimeout(function(){
						var categoryNo = $('#ci_add_categoryNo2').val();
						if(categoryNo==''){
							msgAlert(i18n.msg_ci_category_null,'info');
						}
						$("#selectCiSingleDirDiv").dialog('destroy');//销毁
					},0);
				} 
			});
		},
		/**
		 * 根据分类查询加载自定义表单
		 */
		loadCimFormHtmlByFormId:function(data,htmlDivId){
			var formCustomContents = common.config.formCustom.formControlImpl.editHtml(data.formCustomContents,'addCim_formField');
			$('#'+htmlDivId).html(formCustomContents);
			common.config.formCustom.formControlImpl.formCustomInitByDataDictionaray('#'+htmlDivId);//加载下拉框
			$.parser.parse($('#'+htmlDivId));//加上这句，必填才有用
			$("#addCim_formField").find("div[class=field_options] div[class=label]:odd").css("border-left","none");
			is_show_border = data.isShowBorder;
			var formCustomId="#addCimFormCustom_setting";
			if(page=="main"){
				formCustomId="#addCimFormCustom";
			}
			itsm.cim.addCimFormCustom.showBorderCss(data,"#addCim_formField",formCustomId);
			itsm.cim.cimCommon.setDefaultParamValue(htmlDivId);
			common.config.formCustom.formControlImpl.formCustomInit('#'+htmlDivId);
			if(belongsClient=="0"){
				//设置所属客户不可编辑,并且移除图标#
				$("#addCim_formField #ci_add_companyNo").next().remove();
				$("#addCim_formField #ci_add_companyName").attr("disabled",true).css("width","90%");
			}
			/*if(requestUser=="0"){
				//设置请求用户不可编辑,并且移除图标
				$("#addCim_formField #request_userId").next().remove();
				$("#addCim_formField #request_userName").attr("disabled",true).css("width","90%");
			}*/
			itsm.cim.cimCommon.removeRequiredField(page);
			if(pageType=="template"){
				$("#addCim_formField").find("div[class='fieldName']").css("margin-right","0px");
			}
		},
		/**
		 * 线框的样式
		 */
		showBorderCss:function(data,htmlDIvId,cssId){
			if(data.isShowBorder==="1"){
				$(cssId).attr("href","../styles/common/addCimFormCustom.css");
				$(htmlDIvId+" :input").css("margin-top","0px");
				var len = $("#addCim_formField").find("div[class='field_options_2Column field_options']").length;
				if(len > 0){
					$("#addCim_formField > div[class='field_options_lob field_options'] div[class='label']").css("margin-left","5px");
					
				}else{
					$.each($("#addCim_formField").find("div[class='field_options_lob field_options']"),function(ind,obj){
						$(obj).find("div[class='field_lob']").css("margin-left","5px");
					});
					
				}
				/*if($("#addRequest_formField > div[class='field_options']").size()==0){
					$("#addRequest_formField > div[class='field_options_lob field_options'] div[class='label']").css("margin-left","5px");
				}*/
			}else{
				$(cssId).attr("href","../styles/common/addCimFormCustom-no.css");
				$(htmlDIvId+" :input").not(".control").css("margin-top","10px");
				var len = $("#addCim_formField").find("div[class='field_options_2Column field_options']").length;
				if(len > 0){
					$("#request_userName").add("#request_companyName").css("margin-top","0px");
				}else{
					$.each($("#addCim_formField").find("div[class='field_options_lob field_options']"),function(ind,obj){
						$(obj).find("div[class='field_lob']").css("margin-left","5px");
					});
				}
			}
		},
		cleanCimCatalogNavigation:function(){
			$("#ci_add_categoryNo2").val('');
			$("#ci_add_categoryName2").val('');
			$("#addcim_formId").val(0);
			$("#cim_ciCategoryName").html('');
			$('#cleanCim_ciCategoryName').hide();
			itsm.cim.addCimFormCustom.initAddCim_formField('addCim_formField','initCss');
			itsm.cim.addCimFormCustom.selectConfigureItemTemplate();
		},
		
		initDefault:function(res,data,formCustomId){
			$("#addCim_formField").load('common/config/formCustom/ciDefaultField.jsp',function(){
				
				itsm.cim.cimCommon.initDefaultForm('addCim_formField');
				itsm.cim.cimCommon.setCimParamValueToOldForm('#addCim_formField',res);
				itsm.cim.cimCommon.setCimParamValueToOldFormByDataDictionaray('#addCim_formField',res);

				
				
//				itsm.cim.cimCommon.initDefaultFormByAttr(htmlDivId);
//				
//				itsm.cim.cimCommon.changeCompany();
//				itsm.cim.editCimFormCustom.setEditCimCss(res);
//				itsm.cim.cimCommon.setfieldOptionsLobCss('#'+htmlDivId);
//				itsm.cim.cimCommon.setCimParamValueToOldFormByDataDictionaray('#'+htmlDivId,res);
//				$.parser.parse($('#'+htmlDivId));
//				
//				itsm.cim.cimCommon.setCimParamValueToOldForm('#'+htmlDivId,res);

				
				if(!data.isNewForm){
					common.eav.attributes.findAttributeByScheduledTask(res.attrVals,res.requestCategoryNo,'itsm.request','requestDTO',function(json){
						if(json){
							var jsonStr=common.security.base64Util.encode(JSON.stringify(json));
							$("#addCim_formField").append(common.config.formCustom.formControlImpl.editHtml(jsonStr,'addRequest_formField'));
						}
						common.config.formCustom.formControlImpl.formCustomInit('#addCim_formField');
						common.config.formCustom.formControlImpl.formCustomInitByDataDictionaray('#addCim_formField');
					});
				}
				itsm.cim.addCimFormCustom.showBorderCss(data,'#addCim_formField',formCustomId);
				if(belongsClient=="0"){
					//设置所属客户不可编辑,并且移除图标
					$("#addRequest_formField #request_companyNo").next().remove();
					$("#addRequest_formField #request_companyName").attr("disabled",true).css("width","90%");
				}
				
				if(pageType=="template"){
					$("#addRequest_formField").find("div[class='fieldName']").css("margin-right","0px");
				}
				itsm.cim.cimCommon.removeRequiredField(page);
				if(data.serviceDirId>0){
					$("#servicesCatalogNavigationName").html(": &nbsp;"+res.serviceNos[data.serviceDirId]);
					$('#addRequest_serviceDirIds').val(data.serviceDirId);
					$('#cleanServicesCatalogNavigation').css("display","");
				}
			});
		},
		initAddTabs:function(res){
			var data = res.formCustomTabsDtos;
			$(cimTabsName).each(function(index, name) {
				if ($('#addCim_tabs').tabs('exists',name)){
					$('#addCim_tabs').tabs('close',name);
				}
			});
			var divElements =new Array(data.length);//存储需要初始化时间的控件
			for ( var i = 0; i < data.length; i++) {
				var tabId =data[i].tabId;
				var name =data[i].tabName;
				var htmlDivId = 'addCim_tabs_formField_'+tabId;
				var formCustomContents = common.config.formCustom.formControlImpl.editHtml(data[i].tabAttrsConent,htmlDivId);
				$('#addCim_tabs').tabs('add',{
					title:name,
					cache:"true",
					content:"<form><div id='"+htmlDivId+"' style='width: 90%;margin:0 auto;' >"+formCustomContents+"</div></form>",
					active: 1
				});
				divElements[i] = "#" + htmlDivId + " :input[attrtype='Date']";
				cimTabsName.push(name);
				common.config.formCustom.formControlImpl.formCustomInit('#'+htmlDivId);
				common.config.formCustom.formControlImpl.formCustomInitByDataDictionaray('#'+htmlDivId);//加载下拉框
				itsm.cim.addCimFormCustom.showTabsCss('#'+htmlDivId);
				$.parser.parse($('#'+htmlDivId));//加上这句，必填才有用
			}
			//面板初始化之后，加载时间控件
			DatePicker97(divElements,'yyyy-MM-dd HH:mm:ss');
		},
		showTabsCss:function(htmlDIvId){
			$(htmlDIvId).find("div[class=field_options] div[class=label]:odd").css("border-left","none");
			$(htmlDIvId+" :input").css("margin-top","0px");
			var len = $(htmlDIvId).find("div[class='field_options_2Column field_options']").length;
			if(len > 0){
				$(htmlDIvId+" > div[class='field_options_lob field_options'] div[class='label']").css("margin-left","5px");
			}else{
				$.each($(htmlDIvId).find("div[class='field_options_lob field_options']"),function(ind,obj){
					$(obj).find("div[class='field_lob']").css("margin-left","5px");
				});
			}
		},
		loadCimForm:function(data){
			$.post("formCustom!findFormCustomById.action","formCustomId="+data,function(res){
				itsm.cim.addCimFormCustom.loadCimFormHtmlByFormId(res,'addCim_formField');
				itsm.cim.addCimFormCustom.initAddTabs(res);
			});
		},
		/**
		 * 初始化数据
		 */
		init:function(){
			if(pageType=="template"){
				$('#itemAddSave').hide();
			}
			itsm.cim.includes.includes.loadSelectCIIncludesFile();
			common.security.includes.includes.loadSelectCustomerIncludesFile();
			common.config.includes.includes.loadCategoryIncludesFile();
			common.config.includes.includes.loadCustomFilterIncludesFile();
			$("#configureItemAdd_loading").hide();
			$("#configureItemAdd_panel").show();
			itsm.cim.addCimFormCustom.selectConfigureItemTemplate();
			$('#add_configureItem_backList').click(function(){
				if(pageType=='template'){
					basics.tab.tabUtils.refreshTab(i18n['title_dashboard_template'],'../pages/common/config/template/templateMain.jsp');
				}else{
					showLeftMenu('../pages/itsm/cim/leftMenu.jsp','leftMenu');		
					basics.tab.tabUtils.refreshTab(i18n['ci_configureItemAdmin'],'../pages/itsm/cim/configureItem.jsp');	
				}
			});
			$('#ci_loc').click(function(){
				itsm.cim.cimCommon.selectCimLocation('#ci_loc','#ci_locid',this);			
			});
			//新增
			$('#itemAddSave').click(function(){
				itsm.cim.addCimFormCustom.saveConfigureItem();
			});
			itsm.cim.ciCategoryTree.defaultShowAttributet($('#ci_add_categoryNo').val(),'ci_add_eavAttributet');
			
			//公司选择
			$('#search_ci_companyName').click(function(){
				itsm.itsop.selectCompany.openSelectCompanyWin('#ci_add_companyNo','#ci_add_companyName','#configureItem_add_department,#ci_add_useName,#ci_add_owner,#ci_add_originalUser');
			});
			
			$('#saveconfigureItemTemplateBtn').click(itsm.cim.addCimFormCustom.configureItemTemplateNameWin);
			$('#addTemplateOk').click(function(){
				itsm.cim.addCimFormCustom.saveConfigureItemTemplate("add");
			});
			$('#editconfigureItemTemplateBtn').click(function(){
				itsm.cim.addCimFormCustom.saveConfigureItemTemplate("edit");
			});
			if(editTemplateId){
				$('#configureItemTemplate').val(editTemplateId);
				itsm.cim.addCimFormCustom.getTemplateValue(editTemplateId);
			}else{
				//$("#addCim_formField").html("请选择配置项分类！");
				itsm.cim.addCimFormCustom.initAddCim_formField('addCim_formField','initCss');
			}
			
			$('#add_ci_service_add').click(function(){//关联服务目录
				common.config.category.serviceCatalog.selectServiceDir('#add_ci_serviceDirectory_tbody');
    		});
			
			$('#add_ci_relatedService_checked').click(function(){checkAll('add_ci_relatedService_checked','add_ci_relatedService','ciDto.serviceDirectoryNos');});
			itsm.app.autocomplete.autocomplete.bindAutoComplete('#ci_add_companyName','com.wstuo.common.security.entity.Organization','orgName','orgName','orgNo','Long','#ci_add_companyNo',userName,'true');//所属客户
			
			setTimeout(function(){
				//加载默认公司
				common.security.defaultCompany.loadDefaultCompany('#ci_add_companyNo','#ci_add_companyName');
			},300);
			
			$('#cim_categoryName').click(function(){
				itsm.cim.addCimFormCustom.showCimFormCustomDesign();
			});
			
			//实例化附加上传
			setTimeout(function(){
				getUploader('#addConfigureItem_uploadAttachments','#addConfigureItem_attachments','#addConfigureItem_uploadedAttachments','addConfigureItem_fileQueue');
				//getUploader('#addConfigureItem_uploadSoftAttachments','#addConfigureItem_Softattachments','#add_uploadedSoftAttachments','addConfigureItem_SoftfileQueue');
				//initFileUpload("_CiAdd","","","","","#addConfigureItem_attachments","#addConfigureItem_fileQueue");
				//initFileUpload("_CiAddSoft","","","","","#editConfigureItem_Softattachments","#edit_uploadedSoftAttachments");
				//配置项分类目录导航
				itsm.cim.addCimFormCustom.showCimFormCustomDesign();
			},0);
		}
	};
 }(); 
 //加载初始化数据
 $(document).ready(itsm.cim.addCimFormCustom.init);