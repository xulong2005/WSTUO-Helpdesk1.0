$package("itsm.request");
$import("common.config.dictionary.dataDictionaryUtil");
$import("common.config.category.eventCategoryTree");
$import("common.security.userUtil");
if(isCIHave){
	$import("itsm.cim.configureItemUtil");
	$import('itsm.cim.ciCategoryTree');
}
$import("common.eav.attributes");
$import('itsm.itsop.selectCompany');
$import('itsm.request.relatedRequestAndKnowledge');
$import('common.config.attachment.chooseAttachment');
$import('common.knowledge.knowledgeTree');
$import('common.config.category.serviceCatalog');
$import('common.config.includes.includes');
$import('common.security.includes.includes');
$import('common.security.base64Util');
$import('common.config.formCustom.formControl');
$import('common.config.formCustom.formControlImpl');
$import('itsm.request.requestCommon');
$import('common.security.xssUtil');
/**  
 * @author QXY  
 * @constructor WSTO
 * @description 新增请求主函数.
 * @date 2010-11-17
 * @since version 1.0 
 * @returns
 * @param select
 */
itsm.request.addRequestFormCustom = function(){
	var templateId='';
	this.addRequestGrids=[];
	var addRequest_hidden_formField = '';
	var add_request_attrJson = "";
	var is_show_border ="0" ;
	return {
		
		/**
		 * 根据语音卡用户Id赋值选择请求人
		 */
		selectRequestvoiceCard:function(){
			$.post('user!findUserDetail.action','userDto.userId='+voiceCarduserId,function(data){
				$('#addRequestForm #request_userName').val(data.fullName);
				$('#addRequestForm #request_userId').val(data.userId);
			});
		},
		
		/**
		 * @description 选择请求子分类.
		 * @param showAttrId 扩展属性Id
		 * @param dtoName 实体对象名
		 */
		selectRequestCategoryZi:function(showAttrId,dtoName){
			var categoryNo= $('#addRequestCategoryNo').val();
			if(categoryNo==="")
				msgShow(i18n.request_categorysub_alert,'show');
			else{
			common.config.category.eventCategoryTree.showSelectTreeZi('#request_category_select_window',
                                                                      '#request_category_select_tree',
                                                                      'Request',
                                                                      '#addRequestCategoryName_zi',
                                                                      '#addRequestCategoryNo_zi',
                                                                      '#request_etitle',
                                                                      '#request_edesc',
                                                                      showAttrId,
                                                                      dtoName,
                                                                      categoryNo
                                                                     );
			}
		},
		/**
		 * @description 提交保存请求.
		 */
		saveRequest:function(){
			var oEditor = CKEDITOR.instances.request_edesc;
			var edesc=trim(oEditor.getData());
			var uid=$('#request_userId').val();
			var username=$('#request_userName').val();
			var title= $('#addRequestForm #request_etitle').val();
			$('#addRequestForm #request_etitle').val(trim(title));
			$.each($("#addRequestForm :input[attrType=Lob]"),function(index,obj){
				var oEditorObj = CKEDITOR.instances[$(obj).attr("id")];
				$(obj).val(trim(oEditorObj.getData()));
			});
			var request_edesc=(edesc===''|| edesc=="null" || edesc=="NULL")?false:true;
			if(!request_edesc){
				msgAlert(i18n.titleAndContentCannotBeNull,'info');
			}else if(username==='' || uid===''){
				$('#request_userId').val('');
				$('#request_userName').val('');
				msgAlert(i18n.ERROR_CREATE_BY_NULL,'info');
			}else{
				if($('#addRequestForm').form('validate')){
					itsm.cim.ciCategoryTree.getFormAttributesValue("#addRequestForm");
					$('#addRequestForm #request_edesc').val(edesc);
					var rNo=$('#addRequestCategoryNo_zi').val();
					var categoryNo=$('#addRequestCategoryNo').val();
					if(rNo!=="" && rNo!==undefined){
						$('#addRequestCategoryNo').val(rNo);
					}
					//var frm = $('#addRequest_layout form').serializeObject();
					
					$.each($("#addRequest_formField input[attrtype='String']"),function(ind,val){
						$(this).val(common.security.xssUtil.html_encode($(this).val()));
					});
					
					var frm = $('#addRequest_layout form').serialize();
					var url = 'request!saveRequest.action?requestDTO.isShowBorder='+is_show_border+'&requestDTO.isNewForm=true';
					//调用
					startProcess();
					$.post(url,frm, function(eno){
							basics.tab.tabUtils.closeTab(i18n.title_request_addRequest);
							showRequestIndex();
							basics.tab.tabUtils.reOpenTab("request!requestDetails.action?eno="+eno,i18n.request_detail);
							if($('#itsmMainTab').tabs('exists',i18n.title_request_requestGrid)){
								$('#requestGrid').trigger('reloadGrid');
								itsm.request.requestStats.countAllRquest();
							}
							msgShow(i18n.msg_request_addSuccessful,'show');
							endProcess();
					});
				}
				
			}
			
		},
		/**
		 * 查询相类似的请求和知识
		 * @param id 标识
		 */
		findLikeData:function(id){
			var _keyword=$(id).val();
			if(_keyword!=null && _keyword!=='' && _keyword!=' '){
				itsm.request.relatedRequestAndKnowledge.findKnowledge('#findLikeKnowledgeGrid','#findLikeKnowledgePager',_keyword);
				itsm.request.relatedRequestAndKnowledge.findLikeRequest('#findLikeRequestGrid','#findLikeRequestPager',_keyword);
				$('#addRequest_keyword').val(_keyword);
			}else{
				$("#findLikeKnowledgeGrid").jqGrid("clearGridData");
				$("#findLikeRequestGrid").jqGrid("clearGridData");
			}
		},
		/**
		 * 数据列表伸展
		 */
		addRequestPanelCollapseAndExpand:function(){
			 $(addRequestGrids).each(function(i, g) {
			      setGridWidth(g,'addRequest_tab', 10);
			 });
		},
		/**
		 * 数据列表自动伸展
		 */
		fitRequestAddGrids:function(){
			$('#addRequest_west,#addRequest_center').panel({
				onCollapse:itsm.request.addRequestFormCustom.addRequestPanelCollapseAndExpand,
				onExpand:itsm.request.addRequestFormCustom.addRequestPanelCollapseAndExpand,
				onResize:function(width, height){
					setTimeout(function(){
						itsm.request.addRequestFormCustom.addRequestPanelCollapseAndExpand();
					},0);
				}
			});
		},
		/**
		 * 选择请求模板
		 */
		selectRequestTemplate:function(){
			//var template_formId = $('#addRequest_formId').val();
			var serviceDirId = $('#addRequest_serviceDirIds').val();
			if(!serviceDirId){
				serviceDirId=0;
			}
			$("#requestTemplate").html("");
			$('<option value="">-- '+i18n.common_pleaseChoose+' --</option>').appendTo("#requestTemplate");
			$.post("template!findAllTemplate.action",{"templateDTO.templateType":"request","templateDTO.serviceDirId":serviceDirId},function(data){
				if(data!=null && data.length>0){
					for(var i=0;i<data.length;i++){
						$('<option value="'+data[i].templateId+'">'+data[i].templateName+'</option>').appendTo("#requestTemplate");
					}
				}
			});
		},
		/**
		 * 线框的样式
		 */
		showBorderCss:function(data,htmlDIvId,cssId){
			if(data.isShowBorder==="1"){
				$(cssId).attr("href","../styles/common/addRequestFormCustom.css");
				$(htmlDIvId+" :input").css("margin-top","0px");
				var len = $("#addRequest_formField").find("div[class='field_options_2Column field_options']").length;
				if(len > 0){
					$("#addRequest_formField > div[class='field_options_lob field_options'] div[class='label']").css("margin-left","5px");
					
				}else{
					$.each($("#addRequest_formField").find("div[class='field_options_lob field_options']"),function(ind,obj){
						$(obj).find("div[class='field_lob']").css("margin-left","5px");
					});
					
				}
				/*if($("#addRequest_formField > div[class='field_options']").size()==0){
					$("#addRequest_formField > div[class='field_options_lob field_options'] div[class='label']").css("margin-left","5px");
				}*/
			}else{
				$(cssId).attr("href","../styles/common/add_Edit_Detail_RequestFormCustom-no.css");
				$(htmlDIvId+" :input").not(".control").css("margin-top","10px");
				var len = $("#addRequest_formField").find("div[class='field_options_2Column field_options']").length;
				if(len > 0){
					$("#request_userName").add("#request_companyName").css("margin-top","0px");
				}else{
					$.each($("#addRequest_formField").find("div[class='field_options_lob field_options']"),function(ind,obj){
						$(obj).find("div[class='field_lob']").css("margin-left","5px");
					});
				}
			}
		},
		/**
		 * 获取模板值
		 * @param templateId 模板Id
		 */
		getTemplateValue:function(templateId){
		    $('#add_request_success_attachment').html('');
		    $('#add_request_attachmentStr').val('');
		    $('#requestRelatedCIShow tbody').html('');
			if(templateId!=null && templateId!==""){
				$('#saveRequestTemplateBtn').hide();
				$('#editRequestTemplateBtn').show();
				$('#add_request_serviceDirectory_tbody').html("");
				$.post("template!findByTemplateId.action",{"templateDTO.templateId":templateId},function(data){
					if(data!=null){
						var formCustomId="#addRequestFormCustom_setting";
						if(page=="main"){
							formCustomId="#addRequestFormCustom";
						}
						if(data.isShowBorder==="1"){
							$(formCustomId).attr("href","../styles/common/addRequestFormCustom.css");
						}else{
							$(formCustomId).attr("href","../styles/common/addRequestFormCustom-no.css");
						}
						$('#requestTemplate').attr('value',data.templateId);
						$('#requestTemplateNameInput').val(data.templateName);
						$('#requestTemplateId').val(data.templateId);
						var res = data.dto;
						if(data.formId>0){
							var _param = {"formCustomId" : data.formId};
							$.post('formCustom!findFormCustomById.action',_param,function(formData){
								if(formData.formCustomContents!==""){
									var formCustomContents = common.config.formCustom.formControlImpl.editHtml(formData.formCustomContents,'addRequest_formField');
									$('#addRequest_formField').html(formCustomContents);
									$.parser.parse($('#addRequest_formField'));
									common.config.formCustom.formControlImpl.formCustomInit('#addRequest_formField');
									itsm.request.requestCommon.setRequestFormValuesByScheduledTask(res,'#addRequest_formField',res.attrVals,formData.eavNo);
									itsm.request.addRequestFormCustom.showBorderCss(data,'#addRequest_formField',formCustomId);
								
									itsm.request.requestCommon.removeRequiredField(page);
								
									if(belongsClient=="0"){
										//设置所属客户不可编辑,并且移除图标
										$("#addRequest_formField #request_companyNo").next().remove();
										$("#addRequest_formField #request_companyName").attr("disabled",true).css("width","90%");
									}
									if(requestUser=="0"){
										//设置请求用户不可编辑,并且移除图标
										$("#addRequest_formField #request_userId").next().remove();
										$("#addRequest_formField #request_userName").attr("disabled",true).css("width","90%");
										$("#addRequest_formField #request_userName").val(fullName);
										$("#addRequest_formField #request_userId").val(userId);
									}
									if(pageType=="template"){
										$("#addRequest_formField").find("div[class='fieldName']").css("margin-right","0px");
									}
								}else
									itsm.request.addRequestFormCustom.initDefault(res,data,formCustomId);
							});
						}else{
							itsm.request.addRequestFormCustom.initDefault(res,data,formCustomId);
						}
						var ciNos = res.relatedCiNos;
						if(ciNos!=null && ciNos.length>0){
							var param = $.param({"ids":ciNos},true);
							$.post("ci!findByIds.action",param,function(res){
								for(var i=0;i<res.length;i++){
									var ci=res[i];
									$('<tr id="ref_ci_'+ci.ciId+'">'+
											'<td align="center" id="problemShowCIno">'+ci.cino+'</td>'+
											'<td align="center"><input id="probleShowCIId" type="hidden" name="cinos" value="'+ci.ciId+'" />'+
											'<a href=javascript:itsm.cim.configureItemUtil.lookConfigureItemInfo('+ci.ciId+') id="probleShowCIName">'+ci.ciname+'</a>'+
											'</td>'+
											'<td align="center" id="probleShowCIcategoryName">'+ci.categoryName+'</td>'+
											'<td align="center" id="probleShowCIStatus">'+ci.status+'</td>'+
											'<td align="center"><a onclick=itsm.cim.configureItemUtil.removeRow("#ref_ci_'+ci.ciId+'")>'+i18n.deletes+'</a></td>'+
											'</tr>').appendTo('#requestRelatedCIShow tbody');
								}
								
							});
						}
						$('#add_request_attachmentStr').val(data.dto.attachmentStr);
						var attrArr=data.dto.attachmentStr.split("-s-");
			    		for(var i=0;i<attrArr.length;i++){
			    			var url=attrArr[i].replace("\\", "/");
							if(url!=""){
			        			var attrArrs=url.split("==");
			        			var name=attrArrs[0];
			        			uploadSuccess(name.substr(name.lastIndexOf(".")),attrArrs[1],name,'#add_request_attachmentStr','#add_request_success_attachment',"");
							}
			    		}
					}
				});
			}else{
				$('#saveRequestTemplateBtn').show();
				$('#editRequestTemplateBtn').hide();
				var template_formId = $('#addRequest_formId').val();
				if(template_formId!=null && template_formId != 0){
					itsm.request.addRequestFormCustom.loadRequestFormHtmlByFormId(template_formId,'addRequest_formField');
				}else{
					itsm.request.addRequestFormCustom.initAddRequest_formField('addRequest_formField');
				}
			}
		},
		initDefault:function(res,data,formCustomId){
			$("#addRequest_formField").load('common/config/formCustom/defaultField.jsp',function(){
				itsm.request.requestCommon.initDefaultForm('addRequest_formField');
				itsm.request.requestCommon.setRequestParamValueToOldForm('#addRequest_formField',res);
				itsm.request.requestCommon.setRequestParamValueToOldFormByDataDictionaray('#addRequest_formField',res);
				if(!data.isNewForm){
					common.eav.attributes.findAttributeByScheduledTask(res.attrVals,res.requestCategoryNo,'itsm.request','requestDTO',function(json){
						if(json){
							var jsonStr=common.security.base64Util.encode(JSON.stringify(json));
							$("#addRequest_formField").append(common.config.formCustom.formControlImpl.editHtml(jsonStr,'addRequest_formField'));
						}
						common.config.formCustom.formControlImpl.formCustomInit('#addRequest_formField');
						common.config.formCustom.formControlImpl.formCustomInitByDataDictionaray('#addRequest_formField');
					});
				}
				itsm.request.addRequestFormCustom.showBorderCss(data,'#addRequest_formField',formCustomId);
			
				if(belongsClient=="0"){
					//设置所属客户不可编辑,并且移除图标
					$("#addRequest_formField #request_companyNo").next().remove();
					$("#addRequest_formField #request_companyName").attr("disabled",true).css("width","90%");
				}
				if(requestUser=="0"){
					//设置请求用户不可编辑,并且移除图标
					$("#addRequest_formField #request_userId").next().remove();
					$("#addRequest_formField #request_userName").attr("disabled",true).css("width","90%");
					$("#addRequest_formField #request_userName").val(fullName);
					$("#addRequest_formField #request_userId").val(userId);
				}
				if(pageType=="template"){
					$("#addRequest_formField").find("div[class='fieldName']").css("margin-right","0px");
				}
				
				itsm.request.requestCommon.removeRequiredField(page);
				if(data.serviceDirId>0){
					$("#servicesCatalogNavigationName").html(": &nbsp;"+res.serviceNos[data.serviceDirId]);
					$('#addRequest_serviceDirIds').val(data.serviceDirId);
					$('#cleanServicesCatalogNavigation').css("display","");
				}
			});
		},
		/**
		 * 保存请求内容模板
		 * @param type 模板类型，add为新增
		 */
		saveRequestTemplate:function(type){
			if(type=="add"){
				$('#requestTemplateId').val("");
			}
			
			if($("#request_companyName").val()===""){
				$("#request_companyNo").val("");
			}
			if($("#request_userName").val()===""){
				$("#request_userId").val("");
			}
			
			var oEditor = CKEDITOR.instances.request_edesc;
			var edesc=trim(oEditor.getData());
			edesc = edesc.replace(/\s*<\/p>\s*|\s*<\/p>\s*/, '').replace(/\s*<p>\s*|\s*<\/p>\s*/, '').replace(/&nbsp;/gi, '');
			$('#addRequestForm #request_edesc').val(edesc);
			$.each($("#addRequestForm :input[attrType=Lob]"),function(index,obj){
				var oEditorObj = CKEDITOR.instances[$(obj).attr("id")];
				$(obj).val(trim(oEditorObj.getData()));
			});
			var request_edesc=(edesc===''|| edesc=="null" || edesc=="NULL")?false:true;
			if(!request_edesc){
				msgAlert(i18n.titleAndContentCannotBeNull,'info');
				return false;
			}
			if($('#requestTemplateForm').form('validate') && $('#addRequestForm').form('validate')){
				itsm.cim.ciCategoryTree.getFormAttributesValue("#addRequestForm");
				$.each($("#addRequest_formField input[attrtype='String']"),function(ind,val){
					$(this).val(common.security.xssUtil.html_encode($(this).val()));
				});
				$("#requestTemplateNameInput").val(common.security.xssUtil.html_encode($("#requestTemplateNameInput").val()));
				var frm = $('#addRequestForm,#addRequest_center form,#requestTemplateName form').serializeObject();
				var url = 'request!saveRequestTemplate.action';
				var serviceDirId = $('#addRequest_serviceDirIds').val();
				if(!serviceDirId){
					serviceDirId=0;
				}
				$.extend(frm,{"templateDTO.isShowBorder":is_show_border,"templateDTO.isNewForm":true,"templateDTO.serviceDirId":serviceDirId});
				//调用
				startProcess();
				$.post(url,frm, function(res){
						endProcess();
						$('#requestTemplateName').dialog('close');
						if(type=="add"){
							itsm.request.addRequestFormCustom.selectRequestTemplate();
							$('#templateGrid').trigger('reloadGrid');
							msgShow(i18n.saveSuccess,'show');
						}else{
							$('#templateGrid').trigger('reloadGrid');
							msgShow(i18n.editSuccess,'show');
						}
					
				});
			}
			
		},
		/**
		 * 新增请求模板框
		 */
		requestTemplateNameWin:function(){
			$('#requestTemplateNameInput').val("");
			windows('requestTemplateName');
		},
		/**
		 * 请求新增上传
		 * @param locimage 文件名对象
		 */
		addrequestupload:function(locimage){
			var FileType = "jpg,png,gif"; 
	        var FileName = locimage.value;
		    FileName = FileName.substring(FileName.lastIndexOf('.')+1, FileName.length).toLowerCase(); 
		    if (FileType.indexOf(FileName) == -1){
		    	$('#request_upload_file').val("");
		    	msgAlert(i18n.lable_addRequest_upload_file,'info');
		     }else{
		    	 itsm.request.addRequestFormCustom.uploadrequestimage(FileName);
		     }
		},
		/**
		 * 上传请求图片
		 * @param FileName 文件名
		 */
		uploadrequestimage:function(FileName){
			var imageFileNametoreuqest=new Date().getTime()+"."+FileName;
			$.ajaxFileUpload({
				url:'fileUpload!fileUpload.action?imageFileNametoreuqest='+imageFileNametoreuqest,
				secureuri:false,
	            fileElementId:'request_upload_file', 
	            dataType:'String',
				success: function (data, status){
					$('#request_upload_file').val("");
					$('#request_upload_opt_info').text("../upload/request/"+imageFileNametoreuqest);
					msgShow(i18n.lable_addRequest_upload_success,'show');
				},
				error: function (data, status){
					msgShow(i18n.lable_addRequest_upload_errors,'show');
	            }
			});
		},
		/**
		 * 呼叫中心集成打开的新增请求页面处理函数(韵达语音系统集成)
		 */
		callCenterAddRequest:function(){
			$("#phones").val(callNumber);
			var partten = /^0(([1,2]\d)|([3-9]\d{2}))\d{7,8}$/;
			var url="user!updateUserByPhone.action?userDto.moblie="+callNumber;
			if(partten.test(callNumber)){
				url="user!updateUserByPhone.action?userDto.phone="+callNumber;
		     }
			//在这里要判断一下电话是手机还是座机
			$.post(url,function(data){
				if(data != null){
					$("#selectCreator").attr("value",data.fullName);
				}
			});
			//输入用户修改电话号码
			$("#edit_User_By_LoginName").bind("click",function(){
				windows('updateUserWindow');
			});
			//新增一个用户
			$("#add_User_By_Phone").bind("click",function(){
				windows('addUserByPhone');
			});
			//输入用户名修改用户手机号码
			$("#update_user_by_loginName").bind("click",function(){
				if(callNumber == null || callNumber === ""){
					msgAlert(i18n.No_incoming_number,'info');
				}else{
					var loginNames=$("#bindByLoginName").val();
					if(loginNames != null || loginNames !== ""){
						var url="user!findUserUpdatePhone.action?userDto.loginName="+loginNames+"&userDto.moblie="+callNumber;
						var partten = /^0(([1,2]\d)|([3-9]\d{2}))\d{7,8}$/;
						if(partten.test(callNumber)){
							url="user!findUserUpdatePhone.action?userDto.loginName="+loginNames+"&userDto.phone="+callNumber;
					     }
						$.post(url,function(data){
							if(data){
								var urls="user!getUserDetailByLoginName.action?userName="+loginNames;
								$.post(urls,function(datas){
									$("#selectCreator").val(datas.fullName);
									$("#addRequestUserId").val(datas.userId);
									msgShow(i18n.editSuccess,'show');
									$('#updateUserWindow').dialog('close');
								});
							}else{
								msgAlert(i18n.user_phone_is_not_find,'info');
							}
						});
					}
				}
			});
			//添加操作
			$("#add_User_Phone").bind("click",function(){
				if(callNumber == null || callNumber === ""){
					msgAlert(i18n.No_incoming_number,'info');
				}else{
					var fullNames= $("#add_loginName").val();
					var orgNo=$("#addUserOroNo").val();
					var partten = /^0(([1,2]\d)|([3-9]\d{2}))\d{7,8}$/;
					var url="user!saveuserByPhone.action";
					var frm="userDto.fullName="+fullNames+"&userDto.orgNo="+orgNo+"&userDto.moblie="+callNumber;
					if(partten.test(callNumber)){
						frm="userDto.fullName="+fullNames+"&userDto.orgNo="+orgNo+"&userDto.phone="+callNumber;
				     }
					if(orgNo != null && orgNo !== ""){
						$.post(url,frm,function(data){
							if(data != null){
								msgShow(i18n.addSuccess,'show');
								$("#selectCreator").val($("#add_loginName").val());
								$("#add_loginName").val("");
								$("#addUserOroNo").val("");
								$("#addRequestUserId").val(data.userId);
							}
						});
					}else{
						msgAlert(i18n.title_user_org+i18n.err_nameNotNull,'info');
					}
				}
			});
		},
		//根据表单Id加载表单内容
		loadRequestFormHtmlByFormId:function(formId,htmlDivId){
			$("#addRequest_formId").val(formId);
			$.post("formCustom!findFormCustomById.action","formCustomId="+formId,function(data){
				if(data.formCustomContents!=""){
					//add_request_attrJson = common.security.base64Util.decode(data.formCustomContents);
					var formCustomContents = common.config.formCustom.formControlImpl.editHtml(data.formCustomContents,'addRequest_formField');
					$('#'+htmlDivId).html(formCustomContents);
					$.parser.parse($('#'+htmlDivId));//加上这句，必填才有用
					
					$("#addRequest_formField").find("div[class=field_options] div[class=label]:odd").css("border-left","none");
					is_show_border = data.isShowBorder;
					var formCustomId="#addRequestFormCustom_setting";
					if(page=="main"){
						formCustomId="#addRequestFormCustom";
					}
					itsm.request.addRequestFormCustom.showBorderCss(data,"#addRequest_formField",formCustomId);
					
					itsm.request.requestCommon.setDefaultParamValue(htmlDivId);
					common.config.formCustom.formControlImpl.formCustomInit('#'+htmlDivId);
					common.config.formCustom.formControlImpl.formCustomInitByDataDictionaray('#'+htmlDivId);
					itsm.request.requestCommon.changeCompany('#'+htmlDivId);
					itsm.request.requestCommon.bindAutoCompleteToRequest(htmlDivId);
					if(fixRequestAndKnowledge=="1"){
						$('#addRequest_formField #request_etitle').keyup(function(){
							itsm.request.addRequestFormCustom.findLikeData('#addRequest_formField #request_etitle');
							});
						$('#addRequest_keyword_search_ok').click(function(){itsm.request.addRequestFormCustom.findLikeData('#addRequest_keyword');});
					}
					if(belongsClient=="0"){
						//设置所属客户不可编辑,并且移除图标
						$("#addRequest_formField #request_companyNo").next().remove();
						$("#addRequest_formField #request_companyName").attr("disabled",true).css("width","90%");
					}
					if(requestUser=="0"){
						//设置请求用户不可编辑,并且移除图标
						$("#addRequest_formField #request_userId").next().remove();
						$("#addRequest_formField #request_userName").attr("disabled",true).css("width","90%");
						$("#addRequest_formField #request_userName").val(fullName);
						$("#addRequest_formField #request_userId").val(userId);
					}
					
					itsm.request.requestCommon.removeRequiredField(page);
					if(pageType=="template"){
						$("#addRequest_formField").find("div[class='fieldName']").css("margin-right","0px");
					}
				}else{
					itsm.request.addRequestFormCustom.initAddRequest_formField(htmlDivId);
				}
			});
		},
		showFormCustomDesign:function(){
			common.config.category.serviceCatalog.selectSingleServiceDirCallback('#addRequest_serviceDirName','#addRequest_serviceDirIds',function(){
				$("#servicesCatalogNavigationName").html(": &nbsp;"+$("#addRequest_serviceDirName").val());
				var formId = $('#addRequest_formId').val();
				if(formId!=null && formId!="null" & formId!=""){
					itsm.request.addRequestFormCustom.loadRequestFormHtmlByFormId(formId,'addRequest_formField');
				}else{
					itsm.request.addRequestFormCustom.initAddRequest_formField('addRequest_formField');
				}
				itsm.request.addRequestFormCustom.selectRequestTemplate();
				$('#cleanServicesCatalogNavigation').css("display","");
				/*if(voiceCarduserId!==""){
					if(callNumber== null || callNumber ===""){
						itsm.request.addRequestFormCustom.selectRequestvoiceCard();
					}
				}*/
			},"#addRequest_formId");
		},
		initAddRequest_formField:function(htmlDivId){
			$("#"+htmlDivId).load('common/config/formCustom/defaultField.jsp',function(){
				
				itsm.request.requestCommon.removeRequiredField(page);
				
				add_request_attrJson = itsm.request.requestCommon.initDefaultForm(htmlDivId);
				common.config.formCustom.formControlImpl.formCustomInitByDataDictionaray('#'+htmlDivId);
				itsm.request.requestCommon.changeCompany();

				$.each($("#"+htmlDivId+" :input[attrType=Lob]"),function(ind,val){
					$(val).parent().parent().attr("class","field_options field_options_lob");
				});
				$("#"+htmlDivId+" :input[attrType=Lob]").parent().attr("class","field_lob");
				$("#addRequest_formField").find("div[class=field_options] div[class=label]:odd").css("border-left","none");
				if(page=="main"){
		        	$("#addRequestFormCustom").attr("href","../styles/common/addRequestFormCustom-no.css");
				}else{
					$("#addRequestFormCustom_setting").attr("href","../styles/common/addRequestFormCustom-no.css");
				}
				$("#"+htmlDivId+" :input").not(".control").css("margin-top","10px");
				$("#addRequest_formField").find("div[attrtype='Lob']").find("div[class='field_lob']").css("margin-left","5px");
				itsm.request.requestCommon.bindAutoCompleteToRequest(htmlDivId);
				$.parser.parse($('#'+htmlDivId));//加上这句，必填才有用				
				if(fixRequestAndKnowledge=="1"){
					$('#addRequest_formField #request_etitle').keyup(function(){
						itsm.request.addRequestFormCustom.findLikeData('#addRequest_formField #request_etitle');
						});
					$('#addRequest_keyword_search_ok').click(function(){itsm.request.addRequestFormCustom.findLikeData('#addRequest_keyword');});
				}
				if(belongsClient=="0"){
					//设置所属客户不可编辑,并且移除图标
					$("#addRequest_formField #request_companyNo").next().remove();
					$("#addRequest_formField #request_companyName").attr("disabled",true).css("width","90%");
				}
				if(requestUser=="0"){
					//设置请求用户不可编辑,并且移除图标
					$("#addRequest_formField #request_userId").next().remove();
					$("#addRequest_formField #request_userName").attr("disabled",true).css("width","90%");
					$("#addRequest_formField #request_userName").val(fullName);
					$("#addRequest_formField #request_userId").val(userId);
				}
				if(pageType=="template"){
					$("#addRequest_formField").find("div[class='fieldName']").css("margin-right","0px");
				}
			});
		},
		cleanServicesCatalogNavigation:function(){
			$("#addRequest_serviceDirIds").val('');
			$("#addRequest_serviceDirName").val('');
			$("#addRequest_formId").val(0);
			$("#servicesCatalogNavigationName").html('');
			$('#cleanServicesCatalogNavigation').hide();
			itsm.request.addRequestFormCustom.initAddRequest_formField('addRequest_formField');
			itsm.request.addRequestFormCustom.selectRequestTemplate();
		},
		/**
		 * 初始化
		 * @private
		 */
		init:function() {
			$("#addRequest_loading").hide();
			$("#addRequest_layout").show();
			if(page=="main"){
	        	$("#addRequestFormCustom").attr("href","../styles/common/addRequestFormCustom-no.css");
			}else{
				$("#addRequestFormCustom_setting").attr("href","../styles/common/addRequestFormCustom-no.css");
			}
			
			//判断是否有传来电号码(韵达语音系统集成)
			if(callNumber!=="" && callNumber != null || callNumber.length !== 0){
				itsm.request.addRequestFormCustom.callCenterAddRequest();
			}
			//如果是内容模板，则隐藏请求保存按钮
			if(pageType=="template"){
				$('#saveRequestBtn').hide();
				$("#addRequest_serviceDirIds").val(serviceDirId);
			}
			//加载请求模板includes文件
			itsm.request.includes.includes.loadRequestActionIncludesFile();
			common.security.includes.includes.loadSelectCustomerIncludesFile();
			common.config.includes.includes.loadCategoryIncludesFile();
			common.config.includes.includes.loadCustomFilterIncludesFile();
			
			//点击附件Tab执行清空
			$('#addRequest_tab').tabs({
				onSelect:function(title){
					if(title==title_add_request_attr){
						$('#addRequestQueId').empty();
					}
				}
	        });
			
			/*$('#servicesCatalogNavigation').combotree({  
		        //获取数据URL  
		        url :"event!getCategoryCombotree.action?types=Service", 
		        valueField: 'id',
                textField: 'text',
                required: true,
                editable: false,
		        //选择树节点触发事件  
		        onSelect : function(node) {  
		            //返回树对象  
		            var tree = $(this).tree;  
		            //选中的节点是否为叶子节点,如果不是叶子节点,清除选中  
		            var isLeaf = tree('isLeaf', node.target);  
		            if (!isLeaf) {  
		                //清除选中  
		                $('#servicesCatalogNavigation').combotree('clear');  
		            }  
		        }  
		    }); */
			
			//绑定请求保存按钮
			$('#saveRequestBtn').click(itsm.request.addRequestFormCustom.saveRequest);
			
			//绑定选择指派信息按钮
			$('#selectAssigneeInfo').click(itsm.request.addRequestFormCustom.selectRequestAssignee);
			
			//初始化附件上传控件
			setTimeout(function(){
				getUploader('#add_request_file','#add_request_attachmentStr','#add_request_success_attachment','addRequestQueId');
			},0);
			
			//绑定退回请求列表页面按钮
			$('#add_request_backList').click(function(){
				if(pageType=='template'){
					basics.tab.tabUtils.refreshTab(i18n.title_dashboard_template,'../pages/common/config/template/templateMain.jsp');
				}else{
					showLeftMenu('../pages/itsm/request/leftMenu.jsp','leftMenu');		
					basics.tab.tabUtils.refreshTab(i18n.title_request_requestGrid,'../pages/itsm/request/requestMain.jsp');
				}
			});
			
			setTimeout(function(){//设置请求下拉树的宽度
				$('#add_request_select_category_panel').css('width',$('#addRequestCategoryName').css('width'));
			},0);
			
			//服务目录
			$('#add_request_service_add').click(function(){
				common.config.category.serviceCatalog.selectServiceDir('#add_request_serviceDirectory_tbody');
			});
			
			
			if(fixRequestAndKnowledge=="1"){
				addRequestGrids.push('#findLikeKnowledgeGrid');
				addRequestGrids.push('#findLikeRequestGrid');
				setTimeout(function(){
					$("#phoneNums").val(phoneNum);
					itsm.request.relatedRequestAndKnowledge.findKnowledge('#findLikeKnowledgeGrid','#findLikeKnowledgePager','');
					itsm.request.relatedRequestAndKnowledge.findLikeRequest('#findLikeRequestGrid','#findLikeRequestPager','');
				},500);
			}
			setTimeout(itsm.request.addRequestFormCustom.fitRequestAddGrids,100);
			//加载请求内容模板
			itsm.request.addRequestFormCustom.selectRequestTemplate();
			
			$('#saveRequestTemplateBtn').click(function(){
				itsm.request.addRequestFormCustom.requestTemplateNameWin();
			});
			$('#addRequestTemplateOk').click(function(){
				itsm.request.addRequestFormCustom.saveRequestTemplate("add");
			});
			$('#editRequestTemplateBtn').click(function(){
				itsm.request.addRequestFormCustom.saveRequestTemplate("edit");
			});
			if(editTemplateId!==""){
				$('#requestTemplate').val(editTemplateId);
				setTimeout(function(){
					itsm.request.addRequestFormCustom.getTemplateValue(editTemplateId);
				},500);
			}else{
				$.post("formCustom!findIsDefault.action?type=request",function(data){
					if(data!=null){
						itsm.request.addRequestFormCustom.loadRequestFormHtmlByFormId(data.formCustomId,'addRequest_formField');
					}else{
						itsm.request.addRequestFormCustom.initAddRequest_formField('addRequest_formField');
					}
					if(voiceCarduserId!==""){
						if(callNumber== null || callNumber ===""){
							itsm.request.addRequestFormCustom.selectRequestvoiceCard();
						}
					}
				});
			}
			$('#addrequestRelatedCIBtn').click(function(){
				if(userRoleCode =="ROLE_ENDUSER,"){//如果是终端用户，根据请求人去搜索，不需要分类权限控制； 
					itsm.cim.configureItemUtil.enduserSelectCISM('#requestRelatedCIShow',$("#addRequest_formField #request_companyNo").val());
				}else{
					itsm.cim.configureItemUtil.requestSelectCI('#requestRelatedCIShow',$("#addRequest_formField #request_companyNo").val(),'');
				}
			});
			//服务目录导航
			$('#addRequest_ServicesCatalogNavigation').click(function(){
				itsm.request.addRequestFormCustom.showFormCustomDesign();
			});
			//服务目录导航清除按钮
			$('#cleanServicesCatalogNavigation').click(function(){
				msgConfirm(i18n.msg_msg,'<br/>'+i18n.msg_changeForm,function(){
					itsm.request.addRequestFormCustom.cleanServicesCatalogNavigation();
				});
			});
		}
	}
}();
//载入
$(document).ready(itsm.request.addRequestFormCustom.init);