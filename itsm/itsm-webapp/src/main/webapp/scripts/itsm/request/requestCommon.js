$package("itsm.request");
$import('common.config.formCustom.formControlImpl');
$import('itsm.itsop.selectCompany');
$import('common.security.userUtil');
$import('common.config.category.eventCategoryTree');
$import('common.security.xssUtil');
/**  
 * @author ciel  
 * @constructor requestCommon
 * @description 请求公共函数
 * @date 2010-11-17
 * @since version 1.0 
 */
itsm.request.requestCommon=function(){
	var loadAutoUserName=true;
	var loadAutoCompany=true;
	//载入
	return {
		
		/**
		 * 初始化默认表单
		 */
		initDefaultForm:function(htmlDivId){
			
			$("#"+htmlDivId+" .field_opt").remove();
			$("#"+htmlDivId+" .field_options").css('cursor','auto');
			var formCustomContents = $("#"+htmlDivId).html();
			formCustomContents = common.config.formCustom.formControlImpl.replaceSpecialAttr(formCustomContents,htmlDivId);
			$("#"+htmlDivId).html(formCustomContents);
			if(htmlDivId == "addRequest_formField" || htmlDivId == "addScheduledTaskRequest_formField" ){
				itsm.request.requestCommon.setDefaultParamValue(htmlDivId);
			}
			common.config.formCustom.formControlImpl.formCustomInit('#'+htmlDivId);
			
			/*var attrJson = common.config.formCustom.formControlImpl.setFormCustomContentJson("#"+htmlDivId);
						
			return common.security.base64Util.decode(attrJson);*/
		},
		/**
		 * 初始化默认表单
		 */
		initDefaultFormByAttr:function(htmlDivId){
			
			$("#"+htmlDivId+" .field_opt").remove();
			$("#"+htmlDivId+" .field_options").css('cursor','auto');
			var formCustomContents = $("#"+htmlDivId).html();
			formCustomContents = common.config.formCustom.formControlImpl.replaceSpecialAttr(formCustomContents,htmlDivId);
			$("#"+htmlDivId).html(formCustomContents);
			if(htmlDivId == "addRequest_formField" || htmlDivId == "addScheduledTaskRequest_formField" ){
				itsm.request.requestCommon.setDefaultParamValue(htmlDivId);
			}
		},
		setDefaultParamValue:function(htmlDivId){
			$("#"+htmlDivId+" #request_companyName").val(companyName);
			$("#"+htmlDivId+" #request_userName").val(fullName);
			$("#"+htmlDivId+" #request_companyNo").val(companyNo);
			$("#"+htmlDivId+" #request_userId").val(userId);
		},
		/**
		 * 选择公司
		 */
		chooseCompany :function(companyNo,companyName,event){
			var formId=$(event).closest('form').attr("id");
			if(formId)
				itsm.itsop.selectCompany.openSelectCompanyWin(
						'#'+formId+' '+companyNo,
						'#'+formId+' '+companyName,
						'#request_userName,#request_userId,#selectCreator,#addRequestUserId,#add_request_ref_ciname,#add_request_ref_ciid');
		},
		/**
		 * @description 选择请求者.
		 */
		selectRequestCreator:function(userName,userId,companyNo,event){
			var formId=$(event).closest('form').attr("id");
			if(formId)
				common.security.userUtil.selectUser(
						'#'+formId+' '+userName,
						'#'+formId+' '+userId,
						'','fullName',
						$('#'+formId+' '+companyNo).val(),function(){});
		},
		/**
		 * @description 选择请求分类.
		 * @param showAttrId 扩展属性Id
		 * @param dtoName 实体对象名
		 */
		selectRequestCategory:function(categoryName,categoryNo,etitle,edesc,event){
			var formId=$(event).closest('form').attr("id");
			if(formId)
				common.config.category.eventCategoryTree.showSelectTree(
						'#request_category_select_window',
                        '#request_category_select_tree',
                        'Request',
                        '#'+formId+' '+categoryName,
                        '#'+formId+' '+categoryNo,
                        '#'+formId+' '+etitle,
                        '#'+formId+' '+edesc,
                        null,
                        'requestDTO');
		},
		
		/**
		 * @description 选择位置.
		 * @param showAttrId 扩展属性Id
		 * @param dtoName 实体对象名
		 */
		selectRequestLocation:function(categoryName,categoryNo,event){
			var formId=$(event).closest('form').attr("id");
			if(formId)
				common.config.category.eventCategoryTree.showSelectTree(
						'#ci_loc_select_window',
                        '#ci_loc_select_tree',
                        'Location',
                        '#'+formId+' '+categoryName,
                        '#'+formId+' '+categoryNo,
                        null,
                        null,
                        null,
                        'requestDTO');
		},
		/**
		 * 对旧表单的值进行赋值
		 * @param htmlDivId 表单Id
		 * @param data 数据源
		 */
		setRequestParamValueToOldFormByDataDictionaray:function(htmlDivId,data,callback){
			var num = 0;
			$(htmlDivId+" :input[attrtype=DataDictionaray]").each(function(i,obj){
				var attrId=$(obj).attr("id");
				common.config.dictionary.dataDictionaryUtil.loadOptionsByCode($(obj).attr("attrdatadictionary"),htmlDivId+' #'+attrId,function(){
					if(attrId=="request_imode"){
						var imodeNo = (data.imodeNo == 0 ? "":data.imodeNo);
						$(htmlDivId+" #request_imode").val(imodeNo);
						num++;
					}else if(attrId=="request_level"){
						var levelNo = (data.levelNo == 0 ? "":data.levelNo);
						$(htmlDivId+" #request_level").val(levelNo);
						num++;
					}else if(attrId=="request_effectRange"){
						var effectRangeNo = (data.effectRangeNo == 0 ? "":data.effectRangeNo);
						$(htmlDivId+" #request_effectRange").val(effectRangeNo);
						num++;
					}else if(attrId=="request_priority"){
						var priorityNo = (data.priorityNo == 0 ? "":data.priorityNo);
						$(htmlDivId+" #request_priority").val(priorityNo);
						num++;
					}else if(attrId=="request_seriousness"){
						var seriousnessNo = (data.seriousnessNo == 0 ? "":data.seriousnessNo);
						$(htmlDivId+" #request_seriousness").val(seriousnessNo);
						num++;
					}else{
						var _value = (($(obj).attr('val') == 0 || $(obj).attr('val') ==undefined) ? "" : $(obj).attr('val'));
						$(obj).val(_value);
					}
					if(num==5 && callback!=null && callback!="" ){
						callback();
					}
				});
			});
		},
		setRequestDataDictionarayValueByDetail :function(htmlDivId,data){
			$(htmlDivId+" :input[attrtype=DataDictionaray]").each(function(i,obj){
				var attrId=$(obj).attr("id");
				//common.config.dictionary.dataDictionaryUtil.loadOptionsByCode($(obj).attr("attrdatadictionary"),htmlDivId+' #'+attrId,function(){
					$(obj).val($(obj).attr('val'));
					if(attrId=="request_imode"){
						$(htmlDivId+" #request_imode").closest('.field,.field_lob').html(data.imodeName);
					}else if(attrId=="request_level"){
						$(htmlDivId+" #request_level").closest('.field,.field_lob').html(data.levelName);
					}else if(attrId=="request_effectRange"){
						$(htmlDivId+" #request_effectRange").closest('.field,.field_lob').html(data.effectRangeName);
					}else if(attrId=="request_priority"){
						$(htmlDivId+" #request_priority").closest('.field,.field_lob').html(data.priorityName);
					}else if(attrId=="request_seriousness"){
						$(htmlDivId+" #request_seriousness").closest('.field,.field_lob').html(data.seriousnessName);
					}

				//});
			});
		},
		/**
		 * 对旧表单的值进行赋值
		 * @param htmlDivId 表单Id
		 * @param data 数据源
		 */
		setRequestParamValueToOldForm:function(htmlDivId,data){
			$(htmlDivId+" #request_companyName").val(data.companyName);
			$(htmlDivId+" #request_companyNo").val(data.companyNo);
			$(htmlDivId+" #request_userName").val(data.createdByName);
			$(htmlDivId+" #request_userId").val(data.createdByNo);
			$(htmlDivId+" #request_edesc").val(data.edesc);
			$(htmlDivId+" #request_locationName").val(data.locationName);
			$(htmlDivId+" #request_locationNos").val(data.locationNos);
			
			if(htmlDivId=="#edit_formField"){
				$(htmlDivId+" #editrequest_edesc").val(data.edesc);
			}
			$(htmlDivId+" #request_etitle").val(common.security.xssUtil.html_code(data.etitle));
			if(data.requestCategoryName!=null && data.requestCategoryName!=""){
				$(htmlDivId+" #request_categoryName").val(data.requestCategoryName);
			}else{
				$(htmlDivId+" #request_categoryName").val(data.ecategoryName);
			}
			if(data.requestCategoryNo!=null && data.requestCategoryNo!=""){
				$(htmlDivId+" #request_categoryNo").val(data.requestCategoryNo);
			}else{
				$(htmlDivId+" #request_categoryNo").val(data.ecategoryNo);
			}
		},
		/**
		 * 对旧表单的值进行赋值
		 * @param htmlDivId 表单Id
		 * @param data 数据源
		 */
		setRequestParamValueToOldFormByDetail:function(htmlDivId,data){
			$(htmlDivId+" #request_companyName").closest('.field,.field_lob').html(data.companyName);
			$(htmlDivId+" #request_userName").closest('.field,.field_lob').html(data.createdByName+'<a title="'+i18n.label_contactInfo+'" onclick="$(\'#requesterContactInfo_win\').dialog()"><img src="../images/icons/user.gif" style="vertical-align:middle;"></a>');
			$(htmlDivId+" #request_edesc").closest('.field_lob').html(data.edesc);
			$(htmlDivId+" #request_etitle").closest('.field,.field_lob').html(data.etitle);
			$(htmlDivId+" #request_categoryName").closest('.field,.field_lob').html(data.requestCategoryName);
			$(htmlDivId+" #request_locationName").closest('.field,.field_lob').html(data.locationName);
		},
		/**
		 * 自动补全
		 */
		bindAutoCompleteToRequest:function(htmlDivId){
			loadAutoUserName=true;//请求用户
			$('#'+htmlDivId+' #request_userName').focus(function(){
				if(loadAutoUserName){
					//请求用户
					itsm.app.autocomplete.autocomplete.bindAutoComplete('#'+htmlDivId+' #request_userName','com.wstuo.common.security.entity.User','fullName','fullName','userId','Long','#'+htmlDivId+' #request_userId','#'+htmlDivId+' #request_companyNo','true');
					loadAutoUserName=false;
				}
			});
			loadAutoCompany=true;//所属客户
			if(versionType==="ITSOP"){
				$('#'+htmlDivId+' #request_companyName').focus(function(){
					if(loadAutoCompany){
						loadAutoCompany=false;//所属客户
						itsm.app.autocomplete.autocomplete.bindAutoComplete('#'+htmlDivId+' #request_companyName','com.wstuo.common.security.entity.Organization','orgName','orgName','orgNo','Long','#'+htmlDivId+' #request_companyNo',userName,'false');
					}
				});
			}
		},
		changeCompany:function(htmlDivId){
			var temp_companyName = $(htmlDivId+' #request_companyName').val();
			$(htmlDivId+' #request_companyName').focus(function(){
				temp_companyName = $(htmlDivId+' #request_companyName').val();
			});
			$(htmlDivId+' #request_companyName').blur(function(){
				if(temp_companyName!==$(htmlDivId+' #request_companyName').val())
					$(htmlDivId+' #request_userName').val('');
			});
		},
		/**
		 * 给请求编辑form赋值
		 * @data 数据对象
		 * @formId 表单id
		 */
		setRequestFormValuesByScheduledTask:function(eventdata,formId,eventEavVals,eavNo,callback){
			itsm.request.requestCommon.setRequestParamValueToOldForm(formId,eventdata);
			itsm.request.requestCommon.setRequestParamValueToOldFormByDataDictionaray(formId,eventdata,callback);
			itsm.request.requestCommon.setAttributesValue(eventdata,formId,eventEavVals,eavNo,callback);
		},
		/**
		 * 赋值扩展属性
		 */
		setAttributesValue:function(eventdata,formId,eventEavVals,eavNo,callback){
			$.post('attrs!findAttributesByEavNo.action','attrsQueryDTO.eavNo='+eavNo,function(data){
				if(data!==null && data.length>0 && data!==''){
					for(var i=0;i<data.length;i++){
						var value='';
						var attrName=data[i].attrName;
						if(eventEavVals && eval('eventEavVals.'+attrName)!==undefined && eval('eventEavVals.'+attrName)!==''){
							value = eval('eventEavVals.'+attrName);
						}
						if(data[i].attrType=="Radio"){
							$(formId+" input[name*='"+attrName+"'][value='"+value+"']").attr("checked","checked");
						}else if(data[i].attrType=="Checkbox"){
							var values=value.split(',');
							for ( var _int = 0; _int < values.length; _int++) {
								$(formId+" input[name*='"+attrName+"'][value='"+values[_int]+"']").attr("checked","checked");
							}
						}else if(data[i].attrType=="DataDictionaray"){
							var selectId = $(formId+" select[name*='"+attrName+"']").attr("id");
							var selectVal = value;
							common.config.dictionary.dataDictionaryUtil.setSelectOptionsByCode(data[i].attrdataDictionary,formId+' #'+selectId,selectVal);
						}else{
							$(formId+" :input[name*='"+attrName+"']").val(value);
						}
					}
				}
				
			});
		},
		/**
		 * 给请求编辑form赋值
		 * @data 数据对象
		 * @formId 表单id
		 */
		setRequestFormValues:function(eventdata,formId,eventType,eavId,callback){
			itsm.request.requestCommon.setRequestParamValueToOldForm(formId,eventdata);
			itsm.request.requestCommon.setRequestParamValueToOldFormByDataDictionaray(formId,eventdata);
			$.post('eventEav!findEventEav.action','eventEavDTO.entityType='+eventType+'&eventEavDTO.eno='+eventdata.eno,function(eventEavVals){
				if(eavId!="null" && eavId!==null && eavId!=0){
					$.post('attrs!findAttributesByEavNo.action','attrsQueryDTO.eavNo='+eavId,function(data){
						if(data!==null && data.length>0 && data!==''){
							for(var i=0;i<data.length;i++){
								var value='';
								var attrName=data[i].attrName;
								if(eventEavVals && eval('eventEavVals.'+attrName)!==undefined && eval('eventEavVals.'+attrName)!==''){
									value = eval('eventEavVals.'+attrName);
								}
								if(data[i].attrType=="Radio"){
									$(formId+" input[name*='"+attrName+"'][value='"+value+"']").attr("checked","checked");
								}else if(data[i].attrType=="Checkbox"){
									var values=value.split(',');
									for ( var _int = 0; _int < values.length; _int++) {
										$(formId+" input[name*='"+attrName+"'][value='"+values[_int]+"']").attr("checked","checked");
									}
								}else{
									value = common.security.xssUtil.html_code(value);
									$(formId+" :input[name*='"+attrName+"']").val(value);
								}
								if(i==(data.length-1) && callback!=null && callback!="" ){
									callback();
								}
							}
						}else{
							callback();
						}
					});
				}else{
					callback();
				}
			});
		},
		/**
		 * 给请求详情form赋值
		 * @data 数据对象
		 * @formId 表单id
		 */
		setRequestFormValuesByDetail:function(eventdata,formId,eventType,eavId,callback){
			itsm.request.requestCommon.setRequestParamValueToOldFormByDetail(formId,eventdata);
			$.post('eventEav!findEventEav.action','eventEavDTO.entityType='+eventType+'&eventEavDTO.eno='+eventdata.eno,function(eventEavVals){
				if(eavId!="null" && eavId!==null && eavId!=0){
					$.post('attrs!findAttributesByEavNo.action','attrsQueryDTO.eavNo='+eavId,function(data){
						if(data!==null && data.length>0 && data!==''){
							for(var i=0;i<data.length;i++){
								var value='';
								var attrName=data[i].attrName;
								if(eventEavVals && eval('eventEavVals.'+attrName)!==undefined && eval('eventEavVals.'+attrName)!==''){
									value = eval('eventEavVals.'+attrName);
								}
								if(value!='' && data[i].attrType=='DataDictionaray'){
									itsm.request.requestCommon.showDataDicInfo(value,formId,attrName);
								}else
									$(formId+" :input[name*='"+attrName+"']").closest('.field,.field_lob').html(value);
							}
						}
						callback();
						itsm.request.requestCommon.setRequestDataDictionarayValueByDetail(formId,eventdata);
					});
				}else
					callback();
			});
		},
		/**
		 * @description 根据扩展属性id查询数据字典名称
		 * @param attrValNo
		 * @param showAttrId 显示控件id
		 */
		showDataDicInfo:function(attrValNo,formId,attrName){
			$.post('dataDictionaryItems!findByDcode.action?groupNo='+attrValNo,function(res){
				$(formId+" :input[name*='"+attrName+"']").closest('.field,.field_lob').html(res.dname);
			});
		},
		setfieldOptionsLobCss:function(htmlDivId){
			var len=$(htmlDivId+" .field_options").not('.field_options_2Column,.field_options_lob').length;
			if(len%2!=0){
				$(htmlDivId).append('<div class="field_options"><div class="label"></div><div class="field"></div></div>');
			}
		},
		showFormBorder:function(isShowBorder,htmlDivId,isCssId,noCssId){
			if(isShowBorder==="1"){
				$(isCssId).attr("rel","stylesheet");
				$(noCssId).attr("rel","");
			}else{
				$(isCssId).attr("rel","");
				$(noCssId).attr("rel","stylesheet");
				$(htmlDivId+" :input").not(".control").css("margin-top","10px");
			}
		},
		removeRequiredField:function(page){
			if(page==="setting"){
				$("#request_userName").attr("class","control-img").removeAttr("required");
				$("#request_userName").parent().prev().children("div[class=required_div]").remove();
				$("#request_companyName").attr("class","control-img").removeAttr("required");
				$("#request_companyName").parent().prev().children("div[class=required_div]").remove();
			}
		}
	}
}();