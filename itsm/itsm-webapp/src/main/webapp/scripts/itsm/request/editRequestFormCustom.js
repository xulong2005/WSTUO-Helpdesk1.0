$package("itsm.request");
$import("common.jbpm.processCommon");
$import("common.config.dictionary.dataDictionaryUtil");
$import("common.config.category.eventCategoryTree");
$import("common.security.organizationTreeUtil");
$import("common.security.userUtil");
$import("common.tools.event.eventAttachment");
if(isCIHave){
	$import("itsm.cim.configureItemUtil");
}
$import("common.eav.attributes");
$import('itsm.itsop.selectCompany');
$import('common.config.attachment.chooseAttachment');
$import('itsm.cim.ciCategoryTree');
$import('common.knowledge.knowledgeTree');
$import('common.config.category.serviceCatalog');
$import('common.security.includes.includes');
$import('common.security.base64Util');
$import('common.config.formCustom.formControl');
$import('common.config.formCustom.formControlImpl');
$import('itsm.request.requestCommon');
/**  
 * @author QXY  
 * @constructor requestEdit
 * @description 编辑请求函数
 * @date 2010-11-17
 * @since version 1.0 
 */
itsm.request.editRequestFormCustom = function(){
	this.rowHTML='<tr id="ref_ci_CIID">'+
	'<td align="center">CINO</td>'+
	'<td align="center"><input type="hidden" name="cinos" value="CIID" />'+
	'<a href=javascript:itsm.request.editRequestFormCustom.lookConfigureItemInfo(CIID)>CINAME</a>'+
	'</td>'+
	'<td align="center">CATEGORYNAME</td>'+
	'<td align="center">CISTATUS</td>'+
	'<td align="center"><a onclick=itsm.cim.configureItemUtil.removeRow("#ref_ci_CIID")>DELETE</a></td>'+
	'</tr>';
	var edit_request_attrJson="";
	return {
		/**
		 * @description 获取配置项信息
		 * @param ciId 配置项Id
		 */
		lookConfigureItemInfo:function(ciId){
			basics.tab.tabUtils.reOpenTab("ci!findByciId.action?ciEditId="+ciId,i18n.ci_configureItemInfo);
		},
		setEditRequestParamValue:function(res){
			$('#editField_requestCode').val(res.requestCode);
			var edit_requestServiceDirs = "";
			if(res.serviceDirectory.length > 0 && res.serviceDirectory != null){
				for ( var i = 0; i < res.serviceDirectory.length; i++) {
					edit_requestServiceDirs += res.serviceDirectory[i].eventName + ",";
				}
				edit_requestServiceDirs = edit_requestServiceDirs.substring(0,edit_requestServiceDirs.length-1);
			}
			$('#editField_requestServiceDirs').val(edit_requestServiceDirs);
			//基本信息
			$('#requestEdit_pid').val(res.pid);
			if(res.pid!=null){
				$('#traceRequestEditBtn').attr('style','margin-right:15px;');
			}
			if(res.formId!=null){
				$('#requestEdit_formId').val(res.formId);
			}else{
				$('#requestEdit_formId').val(0);
			}
			$('#requestEdit_eno').val(res.eno);
			$('#requestEdit_status').val(res.statusNo);
			if(res.createdByPhone!=null)
				$('#RequestEdit_UserPhone').val(res.createdByPhone);
			else
				$('#RequestEdit_UserPhone').val('');
			//指派
			if(res.assigneeGroupName!=null)
				$('#requestEdit_assigneeGroupName').val(res.assigneeGroupName);
			else
				$('#requestEdit_assigneeGroupName').val('');
			if(res.assigneeGroupNo!=null)
				$('#requestEdit_assigneeGroupNo').val(res.assigneeGroupNo);
			else
				$('#requestEdit_assigneeGroupNo').val('');
				
			if(res.assigneeName!=null)
				$('#requestEdit_assigneeName').val(res.assigneeName);
			else
				$('#requestEdit_assigneeName').val('');
			if(res.assigneeNo!=null)
				$('#requestEdit_assigneeNo').val(res.assigneeNo);
			else
				$('#requestEdit_assigneeNo').val('');
			
			//关系配置项
			if(res.cigDTO!=null){
				var cigDTO=res.cigDTO;
				for(var i=0;i<cigDTO.length;i++){
					var status='';
					if(cigDTO[i].status!=null)
						status=cigDTO[i].status;
					var newRowHTML=rowHTML.replace(/CIID/g,cigDTO[i].ciId)
					.replace(/CINO/g,cigDTO[i].cino)
					.replace(/CIDETAIL_TITLE/g,i18n.ci_configureItemInfo)
					.replace(/CINAME/g,cigDTO[i].ciname)
					.replace(/CATEGORYNAME/g,cigDTO[i].categoryName)
					.replace(/CISTATUS/g,status)
					.replace(/DELETE/g,i18n.deletes); 	
					$(newRowHTML).appendTo('#edit_request_relatedCIShow table');
				}
			}
			//解决方案
			if(res.solutions!=null){
				$('#requestEditSolutions').val(res.solutions);
			}
			$('#requestEdit_requestCode,#requestEdit_requestCode_show').val(res.requestCode);
			
			itsm.app.autocomplete.autocomplete.bindAutoComplete('#RequestEdit_UserName','com.wstuo.common.security.entity.User','fullName','fullName','userId','Long','#RequestEdit_UserId',companyNoReqEdit,'true');
		
		},
		
		setEditRequestCss:function(res){
			itsm.request.requestCommon.bindAutoCompleteToRequest('edit_formField');
			itsm.request.editRequestFormCustom.setEditRequestParamValue(res);
			
			$("#edit_formField").find("div[class=field_options] div[class=label]:odd").css("border-left","none");
			if(res.isShowBorder==="1"){
				$("#editRequestFormCustom").attr("href","../styles/common/editRequestFormCustom.css");
				$("#edit_formField :input").css("margin-top","0px");
			}else{
				$("#editRequestFormCustom").attr("href","../styles/common/addRequestFormCustom-no.css");
				$("#edit_formField").find(":input").not(".control").css("margin-top","10px"); 
			}
			if(res.isNewForm){
				$("#requestEdit_isNewForm").val(true);
			}else{
				$("#requestEdit_isNewForm").val(false);
			}
			//设置所属客户不可编辑,并且移除图标
			$("#edit_formField #request_companyNo").next().remove();
			$("#edit_formField #request_companyName").attr("disabled",true).css("width","90%");
			
			itsm.request.editRequestFormCustom.oneRowCss(res);
			
		},
		oneRowCss:function(res){
			if(res.isShowBorder=="1"){
				var leng = $("#edit_formField").find("div[class='field_options_2Column field_options']").length;
				if(leng>0){
					$.each($("#edit_formField").find("div[class='field_options']"),function(ind,obj){
						$(obj).attr("class","field_options_2Column field_options");
						$(obj).find("div[class='field']").attr("class","field_lob");
					});
					$("#editField_requestServiceDirs").parent().prev().attr("style","");
					$.each($("#edit_formField").find("div[class='field_options_lob field_options']"),function(ind,obj){
						$(obj).children(":first").css("height","292px");
					});
					$.each($("div[class='field_options_2Column field_options']"),function(ind,obj){
						$(obj).find("div[class='field_lob']").css("margin-top","10px");
					});
				}else{
					$.each($("#edit_formField").find("div[class='field_options_lob field_options']"),function(ind,obj){
						$(obj).children(":first").css("height","302px");
					});
					$.each($("#edit_formField").find("div[class='field_options_lob field_options']"),function(ind,obj){
						$(obj).find("div[class='field_lob']").css("margin-left","5px");
					});
				}
			}else{
				var leng = $("#edit_formField").find("div[class='field_options_2Column field_options']").length;
				if(leng>0){
					$.each($("#edit_formField").find("div[class='field_options']"),function(ind,obj){
						$(obj).attr("class","field_options_2Column field_options");
						$(obj).find("div[class='field']").attr("class","field_lob");
					});
					$.each($("#edit_formField").find("div[class='field_options_lob field_options']"),function(ind,obj){
						$(obj).find("div[class='field_lob']").css("margin-left","7px");
					});
				}else{
					$.each($("#edit_formField").find("div[class='field_options_lob field_options']"),function(ind,obj){
						$(obj).find("div[class='field_lob']").css("margin-left","4px");
					});
				}
				$.each($("div[class='field_options_2Column field_options']"),function(ind,obj){
					$(obj).find("div[class='field_lob'] :input").css("margin-top","0px");
				});
			}
		},
		/**
		 * 查询请求信息
		 */
		findRequestById:function(){
			var htmlDivId = "edit_formField";
			$('#'+htmlDivId).html("");
			var url = 'request!findRequestById.action?requestQueryDTO.eno='+eno;
			$.post(url, function(res){
				if(res.formId){
					var _param = {"formCustomId" : res.formId};
					$.post('formCustom!findFormCustomById.action',_param,function(data){
						var formCustomContents = common.config.formCustom.formControlImpl.editHtml(data.formCustomContents,htmlDivId);
						$('#'+htmlDivId).html(formCustomContents);
						$('#'+htmlDivId).prepend($('#edit_codeAndServices').html());
						$('#'+htmlDivId+' #request_edesc').attr("id","editrequest_edesc");//将编辑器的id改为editrequest_edesc
						itsm.request.requestCommon.setRequestFormValues(res,'#'+htmlDivId,'itsm.request',data.eavNo,function(){
							common.config.formCustom.formControlImpl.formCustomInit('#'+htmlDivId);
							endProcess();
						});
						$('#'+htmlDivId+' #request_edesc').attr("id","editrequest_edesc");//将编辑器的id改为editrequest_edesc
						$.parser.parse($('#'+htmlDivId));
						if(editRequestUser=="0"){
							//设置请求用户不可编辑,并且移除图标
							$("#edit_formField #request_userId").next().remove();
							$("#edit_formField #request_userName").attr("disabled",true).css("width","90%");
							$("#edit_formField #request_userName").val(fullName);
							$("#edit_formField #request_userId").val(userId);
						}
						itsm.request.editRequestFormCustom.setEditRequestCss(res);
					});
				}else{
					$('#'+htmlDivId).load('common/config/formCustom/defaultField.jsp',function(){
						itsm.request.requestCommon.initDefaultFormByAttr(htmlDivId);
						$('#'+htmlDivId).prepend($('#edit_codeAndServices').html());
						itsm.request.requestCommon.setRequestParamValueToOldForm('#'+htmlDivId,res);

						itsm.request.editRequestFormCustom.setEditRequestCss(res);
						$('#'+htmlDivId+' #request_edesc').attr("id","editrequest_edesc");//将编辑器的id改为editrequest_edesc
						itsm.request.requestCommon.setfieldOptionsLobCss('#'+htmlDivId);
						$.parser.parse($('#'+htmlDivId));
						if(editRequestUser=="0"){
							//设置请求用户不可编辑,并且移除图标
							$("#edit_formField #request_userId").next().remove();
							$("#edit_formField #request_userName").attr("disabled",true).css("width","90%");
							$("#edit_formField #request_userName").val(fullName);
							$("#edit_formField #request_userId").val(userId);
						}
						if(!res.isNewForm){
							common.eav.attributes.findAttributeByEno_New(eno,'itsm.request',res.categoryEavId,'requestDTO','',function(json){
								if(json!=undefined){
									var jsonStr=common.security.base64Util.encode(JSON.stringify(json));
									$('#'+htmlDivId).append(common.config.formCustom.formControlImpl.editHtml(jsonStr,htmlDivId));
								}
								common.config.formCustom.formControlImpl.formCustomInit('#'+htmlDivId);
								itsm.request.requestCommon.setRequestParamValueToOldFormByDataDictionaray('#'+htmlDivId,res);
								$.parser.parse($('#'+htmlDivId));
								endProcess();
							});
						}else{
							itsm.request.requestCommon.setRequestParamValueToOldFormByDataDictionaray('#'+htmlDivId,res);
							common.config.formCustom.formControlImpl.formCustomInit('#'+htmlDivId);
							endProcess();
						}
					});
				}
			});
		},
		
		/**
		 * @description 保存请求修改
		 * */
		saveRequestEdit:function(){
			var oEditor = CKEDITOR.instances.editrequest_edesc;
			var edesc=trim(oEditor.getData());
			edesc = edesc.replace(/\s*<\/p>\s*|\s*<\/p>\s*/, '').replace(/\s*<p>\s*|\s*<\/p>\s*/, '').replace(/&nbsp;/gi, ''); 
			
			var request_edesc=(edesc===''|| edesc=="null" || edesc=="NULL")?false:true;
			if(!request_edesc){
				msgAlert(i18n.titleAndContentCannotBeNull,'info');
				return false;
			}
			$.each($("#editRequest_from :input[attrType=Lob]"),function(index,obj){
				var oEditorObj = CKEDITOR.instances[$(obj).attr("id")];
				$(obj).val(trim(oEditorObj.getData()));
			});
			if($('#editRequest_from').form('validate')){
				$('#editRequest_layout #editrequest_edesc').val(edesc);
				var rNo=$('#editRequestCategoryNoSub').val();
				if(rNo!=="" && rNo!==undefined){
					$('#editRequestCategoryNo').val(rNo);
				}
				itsm.cim.ciCategoryTree.getFormAttributesValue("#editRequest_from");
				$.each($("#editRequest_from input[attrtype='String']"),function(ind,val){
					$(this).val(common.security.xssUtil.html_encode($(this).val()));
				});
				var frm = $('#editRequest_layout form').serialize();
				var url = 'request!updateRequest.action';
				startProcess();
				$.post(url,frm, function(){
					endProcess();
					showRequestIndex();
					if($('#itsmMainTab').tabs('exists',i18n.title_request_requestGrid)){
						$('#requestGrid').trigger('reloadGrid');
						itsm.request.requestStats.countAllRquest();
					}
					//关闭请求详情TAB
					basics.tab.tabUtils.closeTab(i18n.title_request_editRequest);
					msgShow(i18n.editSuccess,'show');
		
				});
			}
		},
		/**
		 * @description 创建人信息
		 * */
		selectCreator_openWindow:function(){
			
			var _RequestEdit_UserId=$('#RequestEdit_UserId').val();
			common.security.userUtil.selectUser('#RequestEdit_UserName','#RequestEdit_UserId','','fullName',$('#edit_request_companyNo').val(),function(){
				/*if(_RequestEdit_UserId!=$('#RequestEdit_UserId').val()){//如果更改了请求人，则清空相关联的配置项需要重新选择
					$('#edit_request_ref_ciname,#edit_request_ref_ciid').val('');
				}*/
			});
		},
		/**
		 * @description 删除请求附件
		 * @param eno 编号eno
		 * @param aid 附件Id
		 */
		deleteRequestAttachement:function(eno,aid){
			
			msgConfirm(i18n.msg_msg,'<br/>'+i18n.msg_confirmDelete,function(){
				var _url = "request!deleteRequestAttachement.action";
				$.post(_url,'eno='+eno+'&aid='+aid,function(){
					$('#show_edit_request_attachment #att_'+aid).remove();
					msgShow(i18n.deleteSuccess,'show');
				});
			});
		},
		/**
		 * 初始化
		 * @private
		 */
		init: function(){
			itsm.cim.includes.includes.loadSelectCIIncludesFile();//加载选择配置项
			common.security.includes.includes.loadSelectUserIncludesFile();//加载用户选择
			$("#requestEdit_loading").hide();
			$("#editRequest_layout").show();
			//绑定退回请求列表页面按钮
			$('#edit_request_backList').click(function(){
				showLeftMenu('../pages/itsm/request/leftMenu.jsp','leftMenu');		
				basics.tab.tabUtils.refreshTab(i18n.title_request_requestGrid,'../pages/itsm/request/requestMain.jsp');
			});
			//lazyInitEditor('#requestEdit_edesc','streamline');
			//页面点击
			 $('#requestEditsTab').tabs({
	                onSelect:function(title){
					  	if(title=title_edit_request_attr){
					  		$('#editRequestQueId').empty();
						}
			  		}
	         });

			//流程跟踪
			$('#traceRequestEditBtn').click(function(){common.jbpm.processCommon.showFlowChart( $('#requestEdit_pid').val(),i18n.request);});
			$('#search_Edit_UserName').click(itsm.request.editRequestFormCustom.selectCreator_openWindow);
			setTimeout(function(){
				//getUploader('上传文件文本ID','上传后返回的信息字符串','显示上传成功的附件','');
				getUploader('#edit_request_file','#edit_request_attachmentStr','#show_edit_request_attachment_success','editRequestQueId',function(){
				    common.tools.event.eventAttachment.saveEventAttachment($('#attachmentCreator').val(),'show_edit_request_attachment',eno,'itsm.request','edit_request_attachmentStr',true);
				});
				//initFileUpload("_RequestEdit",eno,"itsm.request",userName,"show_edit_request_attachment","edit_request_attachmentStr");
			},0);
			common.tools.event.eventAttachment.showEventAttachment('show_edit_request_attachment',eno,'itsm.request',true);
			//设置请求下拉树的宽度
			setTimeout(function(){
				$('#edit_request_select_category_panel').css('width',$('#editRequestCategoryName').css('width'));
			},0);

			$('#edit_request_ref_ci').click(function(){//选择配置项
				itsm.cim.configureItemUtil.selectCIS('#edit_request_ref_ci','#requestEditCIId');
			});
			//服务目录
			$('#edit_request_service_edit').click(function(){
				common.config.category.serviceCatalog.selectServiceDir('#edit_request_serviceDirectory_tbody');
			});
			//绑定
			$('#edit_request_ref_ciname').click(function(){
				itsm.cim.configureItemUtil.findConfigureItemByPower('#edit_request_ref_ciname','#edit_request_ref_ciid',userName);
			});
			$('#Request_edit_ref_ci_btn').click(function(){
				if(userRoleCode =="ROLE_ENDUSER,"){//如果是终端用户，根据请求人去搜索，不需要分类权限控制； 
					itsm.cim.configureItemUtil.enduserSelectCISM('#edit_request_relatedCIShow',$("#edit_formField #request_companyNo").val());
				}else{
					itsm.cim.configureItemUtil.requestSelectCI('#edit_request_relatedCIShow',$("#edit_formField #request_companyNo").val(),'');
				}
			});
			
			$("#edit_request_ref_requestServiceDirName").click(function(){
				common.knowledge.knowledgeTree.selectKnowledgeServiceFilter('#knowledge_services_select_window','#knowledge_services_select_tree','#edit_request_ref_requestServiceDirName','#edit_request_ref_requestServiceDirNo');			
			});
			
			$('#editRequest_serviceDirName').click(function(){
				common.config.category.serviceCatalog.selectSingleServiceDir('#editRequest_serviceDirName','#editRequest_serviceDirIds');
			});
		}
	};
}();
//载入
$(document).ready(itsm.request.editRequestFormCustom.init);