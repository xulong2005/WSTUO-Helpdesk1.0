$package('wstuo.rules');
/**  
 * @author WSTUO 
 * @description 规则包列表主函数.WSTUO/rules/packageMain.jsp

 */
wstuo.rules.packageMain=function(){
	return {
		/**
		 * 
		 */
		isToFlagName:function(cellvalue, options, rowObject){
			  if(cellvalue=="saveRequest"){
				  return "<span style='color:#ff0000'>[System]</span>&nbsp;"+i18n.lable_Added_triggered_request;
			  }else if(cellvalue=="saveChange"){
				  return "<span style='color:#ff0000'>[System]</span>&nbsp;"+i18n.lable_When_the_change_is_triggered_new;
			  }else if(cellvalue=="requestProce"){
				  return i18n.lable_Request_process_flow_trigger;
			  }else if(cellvalue=="changeProce"){
				  return i18n.lable_Circulation_trigger_change_process;
			  }
		},
		/**
		 * @description 规则包列表显示
		 */
		showPackageGrid:function(){
			var params = $.extend({},jqGridParams, {
				url:'rulePackage!findRulePackageByPage.action',
				colNames:['ID',
				          i18n['lable_created_drl_processing'],
				          i18n['type'],
				          '',
				          i18n['lable_Rules_package_name']
				],
			 	colModel:[
			 	          {name:'rulePackageNo',align:'center',width:20,hidden:true},
			 	          {name:'packageName',align:'center',width:50},
			 	          {name:'flagName',align:'center',width:50,formatter:wstuo.rules.packageMain.isToFlagName},
			 	          {name:'dataFlag',hidden:true},
			 	          {name:'rulePackageRemarks',align:'center',width:80}
			 	],
				jsonReader: $.extend(jqGridJsonReader, {id: "rulePackageNo"}),
				sortname:'rulePackageNo',
				pager:'#packagesGridPager'
				});
				$("#packagesGrid").jqGrid(params);
				$("#packagesGrid").navGrid('#packagesGridPager',navGridParams);
				//列表操作项
				$("#t_packagesGrid").css(jqGridTopStyles);
				$("#t_packagesGrid").append($('#packagesGridToolbar').html());
				
				//自适应宽度
				//setGridWidth("#packagesGrid",20);
		},
		/**
		 * @description 打开添加规则包窗口
		 */
		package_add:function(){
			$('#add_packageName').val('');
			$('#add_rulePackageRemarks').val('');
			$('#ruleTypeAdd').val('');
			windows('packageAddWin',{width:450});
		},
		/**
		 * @description 规则包保存
		 */
		package_save:function(){
				startProcess();
				var _packageName= $('#add_packageName').val();
				$.post('rulePackage!findRulePackageBoolean.action?addpackageName='+_packageName,function(data){
					if(data){
						var _frm = $('#packageAddWin form').serialize();
						var url = 'rulePackage!saveRulePackage.action';
						//调用
						$.post(url,_frm, function(){
							msgShow(i18n['saveSuccess'],'show');
							$('#packageAddWin').dialog('close');
							$('#packagesGrid').trigger('reloadGrid');
							endProcess();
						});
					}else{
						endProcess();
						msgAlert(i18n['lable_Rule_package_name_already_exists'],'info');
					}
				});
		},
		/**
		 * @description 打开规则包编辑窗口
		 * @param rowsId 选中行的ID
		 */
		package_edit:function(rowId){
			if(rowId==null){
				msgAlert(i18n['msg_atLeastChooseOneData'],'info');
			}else{
				var data = $("#packagesGrid").getRowData(rowId);
				$('#packageMain_rulePackageNo').val(data.rulePackageNo);
				$('#rulePackage_rulePackageRemarks').val(data.rulePackageRemarks);
				windows('packageEditWin',{width:400});
			}
		},
		/**
		 * @description 规则包编辑保存
		 */
		package_saveEdit:function(){
			startProcess();
			var _frm = $('#packageEditWin form').serialize();
			var url = 'rulePackage!editRulePackage.action';
			//调用
			$.post(url,_frm, function(){
				msgShow(i18n['saveSuccess'],'show');
				$('#packageEditWin').dialog('close');
				$('#packagesGrid').trigger('reloadGrid');
				endProcess();
			});
		},
		/**
		 * @description 判断是否选择要删除
		 */
		deletepackage_aff:function(){
			var rowIds = $('#packagesGrid').getGridParam('selarrrow');
			if(rowIds==''){
				msgAlert(i18n['msg_atLeastChooseOneData'],'info');
			}
			else
			{
				var isdel=true;
				for(var i=rowIds.length;i>=0;i--){
					var arr = $("#packagesGrid").jqGrid("getRowData",rowIds[i]);
					if(arr.dataFlag === "1"){
						isdel=false;
					}
				}
				if(isdel){
					msgConfirm(i18n['msg_msg'],'<br/>'+i18n['lable_Rule_package_msg_msg'],function(){
						wstuo.rules.packageMain.deleteproxOpt(rowIds);
					});
				}else{
					msgShow(i18n['msg_canNotDeleteSystemData'],'warning');
				}
			}
		},
		/**
		 * @description 删除操作
		 * @param rowsId 选中行的ID
		 */
		deleteproxOpt:function(rowsId){
			var url="rulePackage!deleteRulePackage.action";
			var param = $.param({'ids':rowsId},true);
			$.post(url, param, function()
			{
				msgShow(i18n['msg_deleteSuccessful'],'show');
				$('#packagesGrid').trigger('reloadGrid');
				$('#rulesGrid').trigger('reloadGrid');
			}, "json");	
		},
		/**
		 * @description 搜索窗口打开
		 */
		SearchBtnWin:function(){
			$('#SearchpackageName').val('');
			$('#SearchrulePackageRemarks').val('');
			windows('packageSearchWin',{width:450});
		},
		/**
		 * @description 提交搜索
		 */
		SearchBtn:function(){
			var sdata=$('#packageSearchWin form').getForm();
			var postData = $("#packagesGrid").jqGrid("getGridParam", "postData");
			$.extend(postData,sdata);
			var _url = 'rulePackage!findRulePackageByPage.action';
			$('#packagesGrid').jqGrid('setGridParam',{url:_url}).trigger('reloadGrid',[{"page":"1"}]);
		},
		/**
		 * @description 初始化
		 */
		init:function(){
			$("#packageMain_loading").hide();
			$("#packageMain_content").show();
			wstuo.rules.packageMain.showPackageGrid();
			$('#packagesGrid_add').click(wstuo.rules.packageMain.package_add);
			$('#packagesGrid_edit').click(function(){wstuo.rules.packageMain.package_edit($("#packagesGrid").getGridParam("selrow"))});
			$('#savepackageBtn').click(wstuo.rules.packageMain.package_save);
			$('#EditpackageBtn').click(wstuo.rules.packageMain.package_saveEdit);
			$('#packagesGrid_delete').click(wstuo.rules.packageMain.deletepackage_aff);
			$('#packagesGrid_search').click(wstuo.rules.packageMain.SearchBtnWin);
			$('#SearchpackageBtn').click(wstuo.rules.packageMain.SearchBtn);
		}
	};
}();
$(document).ready(wstuo.rules.packageMain.init);

