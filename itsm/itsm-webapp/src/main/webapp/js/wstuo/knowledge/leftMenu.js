$package('wstuo.knowledge');
/**  
 * @author WSTUO 
 * @constructor knowledgeLeftMenu
 * @description 知识库左边二级菜单

 */
wstuo.knowledge.leftMenu = function(){	
	
	//公开方法调用
	return {
		
		/**
		 *@description 显示热门，最新知识
		 */
		showHotKnowledges:function(){
			
			var url = 'knowledgeInfo1!findPopularKnowledge.action';
			var hotHTML="";
			$.post(url,'loginName='+userName,function(res){
				var tableWidth = $('#hotKnowledge table').width();
				if(res.list!=null && res.list.length>0){
					$.each(res.list,function(k,v){
						var titleStr =  wstuo.knowledge.leftMenu.shortTitle(v.title,tableWidth,11);
						hotHTML+="<tr><td style='width:5%'><img src='../images/numbers/"+(k+1)+".gif' /></td><td style='padding-left:10px;width:80%'>&nbsp;<a href=javascript:wstuo.knowledge.knowledgeDetail.showKnowledgeDetail('"+v.kid+"')>"+titleStr+"</a></td><td style='width:15%;'>"+v.clickRate+"</td></tr>";
					});					
				}else{
					hotHTML+="<center><span style='color:red;height:100px;l line-height:20px; text-align:center; display:block; width:"+$('#hotKnowledge').width()+"px'>"+i18n.noData+"</span></center>";
				}
				$('#hotKnowledge table').html(hotHTML);
				
				hotHTML=null;
				
			});
			
		},
		/**
		 * @description 显示热门，最新知识
		 */
		showNewKnowledges:function(){
			
			var url = 'knowledgeInfo1!findNewKnowledge.action';
			var hotHTML="";
			$.post(url,'loginName='+userName,function(res){
				var tableWidth = $('#newKnowledge table').width();
				var molecule = 19;
				if(tableWidth<255){
					molecule = 30;
				}
				if(res.list!=null && res.list.length>0){
					$.each(res.list,function(k,v){
						var titleStr =  wstuo.knowledge.leftMenu.shortTitle(v.title,tableWidth,molecule);
						hotHTML+="<tr><td style='width:5%'><img src='../images/numbers/"+(k+1)+".gif' /></td><td style='padding-left:10px;width:55%'>&nbsp;<a href=javascript:wstuo.knowledge.knowledgeDetail.showKnowledgeDetail('"+v.kid+"')>"+titleStr+"</a></td><td style='width:45%;white-space:nowrap'>"+v.addTime+"</td></tr>";
					});					
				}else{
					hotHTML+="<center><span style='color:red;height:100px;l line-height:100px;'>"+i18n.noData+"</span></center>";
				}

				$('#newKnowledge table').html(hotHTML);
				hotHTML=null;
			});
			
		},
		
		
		 /**
		  * @description 加载知识库首页.
		  */
		showKnowledgeIndex:function(type){
			
			//showLeftMenu('../pages/common/knowledge/leftMenu.jsp','leftMenu');
			basics.index.initContent('wstuo/knowledge/knowledgeMain.jsp?type='+type,function(){
				startLoading();
				setTimeout(function(){  
					endLoading();
				},800);
				wstuo.includes.loadServiceDirectoryItemsIncludesFile();
				wstuo.includes.loadCustomFilterIncludesFile();
				wstuo.includes.loadCategoryIncludesFile();
			});
		},
		
		
		/**
		 * @description 门户首页-热门知识.
		 * @param tableID 表格编号	
		 * @param knowledgeType 知识类型
		 */
		showKnowledgePortal:function(tableID,knowledgeType){
			var url='';
			if(knowledgeType=='PopularKnowledge'){  //查询热门知识
				url = "knowledgeInfo1!findPopularKnowledge.action";
			}else{   //查询最新知识
				url = "knowledgeInfo1!findNewKnowledge.action";
			}
			$.post(url,'loginName='+userName,function(data){
				if(data!=null && data.list!=null && data.list.length>0){
					$(tableID).empty();
					var count=0;
					var tableWidth = $(tableID).width();
					$.each(data.list,function(i,item){
						var titleStr =  wstuo.knowledge.leftMenu.shortTitle(item.title,tableWidth,7.5);
						$(tableID).append("<tr><td style='width:5px'><img src='../images/numbers/"+(i+1)+".gif' /></td><td >" +
								"&nbsp;<a class='div_ellipsis' style='width:"+$(tableID).width()+"px' href=javascript:wstuo.knowledge.knowledgeDetail.showKnowledgeDetail('"+item.kid+"')>"+titleStr+"</a></td></tr>");
						count++;
						if(count>7){
							return false;
						}
					});
				}else{
					$(tableID).append("<tr><td align='center'><span style='color:#999999;'>"+i18n.emptyrecords+"...</span></td></tr>");
				}
			});
		},
		/**
		 * 知识库 title 根据容器大小截取
		 * @param titileStr title
		 * @param tableWidth 容器长度
		 * @param molecule 分子
		 */
		shortTitle:function(titileStr,tableWidth,molecule){
			var len = tableWidth/molecule;
			var title = wstuo.knowledge.leftMenu.cutstr(titileStr, Math.ceil(len));
			return title;
		},
		/**
		 * 知识库数量统计
		 * @param flag  标记
		 * @param divId 统计结果显示位置
		 */
		countKnowledge:function(){
			$.post('knowledgeInfo!countKnowledge.action',function(data){
				$("#share_kw").text('[ '+data.shareCount+' ]');
				$("#share_my").text('[ '+data.myCount+' ]');
				$("#share_myapp").text('[ '+data.myapCount+' ]');
				$("#share_allapp").text('[ '+data.allapCount+' ]');
				$("#share_myappf").text('[ '+data.myapfCount+' ]');
				$("#share_allappf").text('[ '+data.allapfCount+' ]');
			});
		},
		
		/**
		 * 获得字符串实际长度，中文2，英文1
		 * @param str 要获得长度的字符串
		 */
		getRelLength:function(str){
	        var realLength = 0, len = str.length, charCode = -1;
	        for (var i = 0; i < len; i++) {
	            charCode = str.charCodeAt(i);
	            if (charCode >= 0 && charCode <= 128) realLength += 1;
	            else realLength += 2;
	        }
	        return realLength;
		},
		 /** 
	     * js截取字符串，中英文都能用 
	     * @param str：需要截取的字符串 
	     * @param len: 需要截取的长度 
	     */
		cutstr:function(str, len) {
	        var str_length = 0;
	        var str_len = 0;
	        str_cut = new String();
	        str_len = str.length;
	        for (var i = 0; i < str_len; i++) {
	            a = str.charAt(i);
	            str_length++;
	            if (escape(a).length > 4) {
	                //中文字符的长度经编码之后大于4  
	                str_length++;
	            }
	            str_cut = str_cut.concat(a);
		        if (str_length > len+1) {
	                str_cut = str_cut.concat("...");
	                return str_cut;
	            }
	        }

	        
	        return str;
	        
	    }
	}

}();



