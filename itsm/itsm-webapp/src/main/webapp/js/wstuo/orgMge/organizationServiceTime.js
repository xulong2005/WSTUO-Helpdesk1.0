$package('wstuo.orgMge');

/**  
 * @author WSTUO 
 */
wstuo.orgMge.organizationServiceTime = function() {

	
	return {
		
		/**
		 * @description 根据机构编号查找服务时间.
		 */
		findServiceByOrganizationNo:function(orgNo){
			
			
			var url="serviceTime!findByOrgNo.action?serviceTimeDTO.orgNo="+orgNo;
			
			$.post(url,function(res){
					
					if(res.sid!=null){
						$('#serviceTimeDTO_sid').val(res.sid);
					}else{
						$('#serviceTimeDTO_sid').val('');
					}
				 	if(res.allday){
				 		
				 		$('#setTime').hide();
				 		$('#diyday').removeAttr("checked");
				 		$('#fullday')[0].checked=true;
				 	}else{
				 		
				 		$('#setTime').show();
				 		$('#fullday').removeAttr("checked");
				 		$('#diyday')[0].checked=true;
				 	}
				    $('#serviceTimeDTO_startHour').val(res.startHour);
				    $('#serviceTimeDTO_startMinute').val(res.startMinute);
				    $('#serviceTimeDTO_startNoonHour').val(res.startNoonHour);
				    $('#serviceTimeDTO_startNoonMinute').val(res.startNoonMinute);
				    $('#serviceTimeDTO_endNoonHour').val(res.endNoonHour);
				    $('#serviceTimeDTO_endNoonMinute').val(res.endNoonMinute);
				    $('#serviceTimeDTO_endHour').val(res.endHour);
				    $('#serviceTimeDTO_endMinute').val(res.endMinute);
				    
				    
				    
				    if(res.monday){
				    	$('#serviceTimeDTO_monday')[0].checked=true;
				    }else{
				    	$('#serviceTimeDTO_monday').removeAttr("checked");
				    }
				    
				    if(res.tuesday){
				    	$('#serviceTimeDTO_tuesday')[0].checked=true;
				    }else{
				    	$('#serviceTimeDTO_tuesday').removeAttr("checked");
				    }

				    if(res.wednesday){
				    	$('#serviceTimeDTO_wednesday')[0].checked=true;
				    }else{
				    	$('#serviceTimeDTO_wednesday').removeAttr("checked");
				    }

				    if(res.thursday){
				    	$('#serviceTimeDTO_thursday')[0].checked=true;
				    }else{
				    	$('#serviceTimeDTO_thursday').removeAttr("checked");
				    }

				    if(res.friday){
				    	$('#serviceTimeDTO_friday')[0].checked=true;
				    }else{
				    	$('#serviceTimeDTO_friday').removeAttr("checked");
				    }
				    
				    if(res.saturday){
				    	$('#serviceTimeDTO_saturday')[0].checked=true;
				    }else{
				    	$('#serviceTimeDTO_saturday').removeAttr("checked");
				    }
				    
				    if(res.sunday){
				    	$('#serviceTimeDTO_sunday')[0].checked=true;
				    }else{
				    	$('#serviceTimeDTO_sunday').removeAttr("checked");
				    }
				    var isSelectAll=true;
				    $("#serviceTimeDiv [type='checkbox']").each(function(){
				    	if($(this).is(":checked")==false)
					    {
					    	isSelectAll=false;
					    }
				    });
				    if($("#serviceTimeDiv [type='checkbox']").is(':checked')){
				    	if(isSelectAll){
					    	$("#workDay_reverseSelect")[0].checked=true;
					    	$("#workDay_normalSelect").removeAttr("checked");
					    }else{
					    	$("#workDay_normalSelect")[0].checked=true;
					    	$("#workDay_reverseSelect").removeAttr("checked");
					    }
				    }else{
				    	$("#workDay_reverseSelect").removeAttr("checked");
				    	$("#workDay_normalSelect").removeAttr("checked");
				    }
				    
				});
		},
		
		
		/**
		 * @description 保存服务时间.
		 */
		saveServiceTime:function(){
			startProcess();
			var serviceTime_orgNo=$('#serviceTime_orgNo').val();
			
			if(serviceTime_orgNo==''){
				endProcess();
				msgAlert(i18n['msg_chooseOrg'],'info');
			}else{
			
				wstuo.orgMge.organizationServiceTime.setFormValue();
					
				
				var startHour=($('#serviceTimeDTO_startHour').val())*1;
				var startMinute=($('#serviceTimeDTO_startMinute').val())*1;
				var startNoonHour=($('#serviceTimeDTO_startNoonHour').val())*1;
				var startNoonMinute=($('#serviceTimeDTO_startNoonMinute').val())*1;
							
				var startworkminutes=((startNoonHour*60)+startNoonMinute)-((startHour*60)+startMinute);
				
				var endNoonHour=($('#serviceTimeDTO_endNoonHour').val())*1;
				var endNoonMinute=($('#serviceTimeDTO_endNoonMinute').val())*1;
				var endHour=($('#serviceTimeDTO_endHour').val())*1;
				var endMinute=($('#serviceTimeDTO_endMinute').val())*1;
							
				var endworkminutes=((endHour*60)+endMinute)-((endNoonHour*60)+endNoonMinute);
				
				var Noonworkminutes=((endNoonHour*60)+endNoonMinute)-((startNoonHour*60)+startNoonMinute);
				
				if($('#serviceTimeDTO_allday').val()=="true" || (startworkminutes>=0 && endworkminutes>=0 && Noonworkminutes>=0)){	
					
					var url="serviceTime!saveOrgUpdate.action";
					var frm = $('#serviceTimeDiv form').serialize();
					$.post(url,frm, function(){
						endProcess();
						msgShow(i18n['msg_serviceTime_updateSuccessful'],'show')
					});
				}else if(startworkminutes<0){
					endProcess();
					msgAlert(i18n['msg_startworkminutes_not_correct'],'info')
				}else if(Noonworkminutes<0){
					endProcess();
					msgAlert(i18n['msg_Noonworkminutes_not_correct'],'info')
				}else if(endworkminutes<0){
					endProcess();
					msgAlert(i18n['msg_endworkminutes_not_correct'],'info')
				}
			}
		},
		
		
		
		/**
		 * @description 设置表单值.
		 * @param method 方法
		 */
		setFormValue:function(method){
			
			var fullday=$('#fullday').is(":checked");
			if(fullday){
				$('#serviceTimeDTO_allday').val("true");
			}else{
				$('#serviceTimeDTO_allday').val("false");
			}
			
			if($('#serviceTimeDTO_monday').is(":checked")){
				$('#serviceTimeDTO_monday').val("true");
			}
			if($('#serviceTimeDTO_tuesday').is(":checked")){
				$('#serviceTimeDTO_tuesday').val("true");
			}
			if($('#serviceTimeDTO_wednesday').is(":checked")){
				$('#serviceTimeDTO_wednesday').val("true");
			}
			if($('#serviceTimeDTO_thursday').is(":checked")){
				$('#serviceTimeDTO_thursday').val("true");
			}
			if($('#serviceTimeDTO_friday').is(":checked")){
				$('#serviceTimeDTO_friday').val("true");
			}
			if($('#serviceTimeDTO_saturday').is(":checked")){
				$('#serviceTimeDTO_saturday').val("true");
			}
			if($('#serviceTimeDTO_sunday').is(":checked")){
				$('#serviceTimeDTO_sunday').val("true");
			}
			
			
			
			
			
			
		},
		
		/**
		 * 初始化加载
		 */
		init:function(){
			$('#serviceTime_save').click(wstuo.orgMge.organizationServiceTime.saveServiceTime);
			
			
			$('#fullday').click(function(){//全天工作
				
				$('#setTime').hide();
				wstuo.orgMge.organizationServiceTime.setFormValue();
			});
			

			$('#diyday').click(function(){//自定义工作时间
				
				$('#setTime').show();
				wstuo.orgMge.organizationServiceTime.setFormValue();
			});
			
			
			//全选
			$('#workDay_reverseSelect').click(function(){
				
				var checkFlag=false;
				
				if(this.checked){ 
					checkFlag=true;				
				}

				$("#workDay_workTime input,#workDay_weekEnd input").each(function(){
					this.checked=checkFlag;	
				});
			});
			
			//常用时间
			$('#workDay_normalSelect').click(function(){

				
				$("#workDay_weekEnd input").each(function(){
					this.checked=false;
				});
				
				
				$("#workDay_workTime input").each(function(){
					this.checked=true;				
				});
				
			});
		}
	};
}();
