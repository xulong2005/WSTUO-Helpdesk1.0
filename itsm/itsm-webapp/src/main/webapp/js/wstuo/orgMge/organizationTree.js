$package('wstuo.orgMge');
/** 
 * @author WSTUO
 */
wstuo.orgMge.organizationTree = function() {
	this._treeDIV='';
	this.treeData;
	return {
		/**
		 * @description 加载右边树菜单
		 * @param treeDIV  树div的id
		 */
		loadOrganizationTreeView:function(treeDIV){
			$.post('organization!findAll.action','parentOrgNo=0&companyNo='+companyNo,function(data){
				if(treeDIV==''){
					treeDIV='#orgTreeDIV';				
				}
				_treeDIV=treeDIV;
				
				$(treeDIV).jstree({
					json_data:{
						ajax: {
							url : "organization!findAll.action?companyNo="+companyNo,
							data:function(n){
						    	  return {'parentOrgNo':n.attr ? n.attr("orgNo").replace("node_",""):0};//types is in action
							},
			                dataType: 'json',
							cache: false
						}
					},
					plugins : ["themes", "json_data", "ui", "crrm", "contextmenu", "dnd"]
				})
				//.bind('loaded.jstree', function(e,data){data.inst.open_all(-1);})
				.bind('select_node.jstree',wstuo.orgMge.organizationTree.selectTreeNode)
				.bind('delete_node.jstree',wstuo.orgMge.organizationTree.deleteTreeNode)
				.bind('move_node.jstree', wstuo.orgMge.organizationTree.moveTreeNode);
			})
			
		},
		
		/**
		 * @description 选择节点事件.
		 * @param event 事件
		 * @param data  选中数据
		 */
		selectTreeNode:function(event, data){	
			var orgNo=data.rslt.obj.attr("orgNo");
			var orgName=data.rslt.obj.attr("orgName");
			//var companyNo=data.rslt.obj.attr("companyNo");
			var orgType=data.rslt.obj.attr("orgType");
			if(orgNo!=null && orgNo!='' && orgName!='ROOT'){
				
				//加载机构角色			
				//wstuo.orgMge.organizationRole.showOrganizationRole(orgNo);
				
				//加载节假日列表
				$('#hoilday_orgNo').val(orgNo);
				$('#hoilday_orgType').val(orgType);
				wstuo.orgMge.organizationHoliday.showHolidayByOrganization(orgNo);
				
				//服务机构
				$('#serviceTime_orgNo').val(orgNo);
				wstuo.orgMge.organizationServiceTime.findServiceByOrganizationNo(orgNo);

				$('#add_parentOrgNo').val(orgNo);
				
				//显示当前机构信息
				wstuo.orgMge.organizationTree.findOrganizationByTree(data);
			}
					

			if(orgType!=null && orgType!=''){
				
				wstuo.orgMge.organizationTree.enableOrDisableOrgType(orgType);
			}
			
		},

		
		/**
		 * 改变机构类型.
		 * @param orgType 类型
		 */
		enableOrDisableOrgType:function(orgType){
			$('#edit_orgType').val(orgType);
			if(orgType=="itsop" || orgType=="ROOT"){ //外包客户编辑
			
				$("#edit_actionType").empty();
				
				$("<option value='company'>"+i18n['label_org_companyInfo']+"</option>").appendTo("#edit_actionType");
				
			}else if(orgType=="inner"  || orgType=="innerPanel" ){//添加时禁用或启用机构
				
				//添加
				$("#add_actionType,#edit_actionType").empty();
				$("<option value='inner'>"+i18n['title_sla_byServiceOrg']+"</option>").appendTo("#add_actionType,#edit_actionType");
			
			}else if(orgType=="services"  || orgType=="servicePanel"){
				
				$("#add_actionType option,#edit_actionType option").remove();
				$("<option value='services'>"+i18n['msg_org_serviceOrg']+"</option>").appendTo("#add_actionType,#edit_actionType");
				
				
			}else{
				//添加
				$("#edit_actionType").empty();
				$("#add_actionType").append("<option value='inner'>"+i18n['title_sla_byServiceOrg']+"</option>" +
						"<option value='services'>"+i18n['msg_org_serviceOrg']+"</option>");
				
				//编辑
				$("#edit_actionType").append("<option value='company'>"+i18n['label_org_companyInfo']+"</option>");
				
				
				$('#add_parentOrgNo').val("1");
			}
			
		},
		
		/**
		 * @description 删除节点.
		 * @param event 事件
		 * @param data  选中数据
		 */
		deleteTreeNode:function (e, data) {
			
			
			if(viewOrglist_delete=="1"){
				
				
				
				var url = "organization!remove.action";
				var params = {"orgNo" : data.rslt.obj.attr("orgNo")};
				$.post(url,params,function (r) {
					

					if(r=='0'){
						
						$.jstree.rollback(data.rlbk);  
						msgShow(i18n['ERROR_DATA_CAN_NOT_DELETE'],"show");
						
					}else{
						
						
						wstuo.orgMge.organizationTree.loadOrganizationTreeView(_treeDIV);						
						
						$('#orgGrid').trigger('reloadGrid');
						msgShow(i18n['msg_operationSuccessful'],"show");
					}
					
					},
					'json'
				);	
			}else{
				
				$.jstree.rollback(data.rlbk);  
				msgShow(i18n['error403'],"show");
			}		
		},
		
		/**
		 * 拖拽机构（复制、剪切、粘贴）
		 * @param event 事件
		 * @param data  选中数据
		 */
		moveTreeNode:function(e, data){
			var parent_no=data.rslt.np.attr('orgNo');
			if(parent_no==null){
				$.jstree.rollback(data.rlbk);  
				msgShow(i18n["msg_incorrect_operation"],"show");
			}else{	
				data.rslt.o.each(function (i) {	
					   var tag=data.rslt.cy;
					   var cate_type=$(this).attr('orgType').replace('node_','');
					   var cate_ptype=data.rslt.np.attr('orgType').replace('node_','');
					   //标题机构
					   if(cate_type=="servicePanel"||cate_type=="innerPanel"||cate_type=="company"||cate_type=="ROOT"){
						   msgShow(i18n['msg_org_disMove'],"show");
						   $.jstree.rollback(data.rlbk);  
					   }else{		   	   
							   var msg=wstuo.orgMge.organizationTree.switchMessage(cate_type,cate_ptype);   
							   if(msg!=''){
								   $.jstree.rollback(data.rlbk);  
								   msgShow(msg,"show");
							   }else{					 
								   
								   //可以移动
								   var cate_id=$(this).attr('orgNo').replace('node_','');
								   var cate_parid=1;
			
								   if(cate_ptype!="servicePanel"&&cate_ptype!="innerPanel"&&cate_ptype!="company"&&cate_ptype!="ROOT"){
									   cate_parid=parent_no.replace('node_','');
								   }
								   var params = {"organizationDto.orgNo" : cate_id,"organizationDto.parentOrgNo":cate_parid};
								   
								   // 判断分类是否存在    
			                       $.post('organization!isCategoryExisted.action', params, function(data){
			                           if(data) {
			                               wstuo.orgMge.organizationTree.loadOrganizationTreeView(_treeDIV);
			                               msgShow(i18n["msg_incorrect_operation"],"show");
			                           } else {
			                             //复制节点
		                                   if(tag==true){
		                                       wstuo.orgMge.organizationTree.moveTreeNode_Copy(params);
		                                   }
		                                   //剪切节点
		                                   if(tag==null||tag==false){
		                                       wstuo.orgMge.organizationTree.moveTreeNode_Cut(params);
		                                   }
			                           }
			                       });
								   
							 }
					   }

				});
				
			}
	
			
		},
		
		/**
		 * 匹配消息
		 * @param cate_type 类型
		 * @param cate_type 类型
		 */
		switchMessage:function(cate_type,cate_ptype){
			
			  var msg='';
			 if(cate_type=="services"&&cate_ptype=="innerPanel"){
				   msg=i18n['msg_org_serviceMoveToInner'];
			   }
			   if(cate_type=="services"&&cate_ptype=="inner"){
				   msg=i18n['msg_org_serviceMoveToInner'];
			   }
			   if(cate_type=="inner"&&cate_ptype=="services"){
				   msg=i18n['msg_org_innerMoveToService'];
			   }
			   if(cate_type=="inner"&&cate_ptype=="servicePanel"){
				   msg=i18n['msg_org_innerMoveToService'];
			   }
			   return msg;
		},
		
		/**
		 * 复制节点.
		 * @param params  参数
		 */
		moveTreeNode_Copy:function(params){
			
			  var url = "organization!copyOrg.action";
				$.post(url,params,function (r){
						if(r && !r.status){
							$.jstree.rollback(data.rlbk);  
						}else{
							
							wstuo.orgMge.organizationTree.loadOrganizationTreeView(_treeDIV);
							msgShow(i18n['msg_operationSuccessful']);
						}
					},
					'json'
				);
		},
		
		/**
		 * 剪切节点.
		 * @param params  参数
		 */
		moveTreeNode_Cut:function(params){
			
		    var url = "organization!moveOrg.action";
			$.post(url,params,function (r) {
					if(r && !r.status){
						$.jstree.rollback(data.rlbk);  
					}else{
						wstuo.orgMge.organizationTree.loadOrganizationTreeView(_treeDIV);
						msgShow(i18n['msg_operationSuccessful']);
					}
				},
				'json'
			);
		},

		/**
		 * 从树数据中获取机构信息（用于编辑）.
		 * @param data  数据
		 */
		findOrganizationByTree:function(data){
			//清空
			$('#edit_officeFax').val("");
			$('#edit_officePhone').val("");
			$('#edit_email').val("");
			$('#edit_address').val("");
			$('#edit_orgNo').val("");
			$('#parentOrgNo').val("1");
			$('#organizationDto_personInChargeName').val("");
			$('#organizationDto_personInChargeNo').val("");
			
			$('#edit_orgName').val(data.rslt.obj.attr("orgName"));
			if(data.rslt.obj.attr("officeFax") !=null && data.rslt.obj.attr("officeFax")!="" && data.rslt.obj.attr("officeFax")!="null"){
				$('#edit_officeFax').val(data.rslt.obj.attr("officeFax"));
			}else{
				$('#edit_officeFax').val("");
			}
			if(data.rslt.obj.attr("officePhone") !=null && data.rslt.obj.attr("officePhone")!="" && data.rslt.obj.attr("officePhone")!="null"){
				$('#edit_officePhone').val(data.rslt.obj.attr("officePhone"));
			}else{
				$('#edit_officePhone').val("");
			}
			if(data.rslt.obj.attr("email") !=null && data.rslt.obj.attr("email")!="" && data.rslt.obj.attr("email")!="null"){
				$('#edit_email').val(data.rslt.obj.attr("email"));
			}else{
				$('#edit_email').val("");
			}
			if(data.rslt.obj.attr("address") !=null && data.rslt.obj.attr("address")!="" && data.rslt.obj.attr("address")!="null"){
				$('#edit_address').val(data.rslt.obj.attr("address"));
			}else{
				$('#edit_address').val("");
			}
			
			$('#edit_orgNo').val(data.rslt.obj.attr("orgNo"));
			//add panel
			
			var _parentNo=data.rslt.obj.attr("parentNo");
			
			if(_parentNo!=null && _parentNo!=''){
				
				$('#edit_parentOrgNo').val(data.rslt.obj.attr("parentNo"));
				
			}else{
				$('#edit_parentOrgNo').val("");
			}
			
			
			var personInChargeName = data.rslt.obj.attr("personInChargeName");
			
			if(personInChargeName!=null&&personInChargeName!=''){
				$('#organizationDto_personInChargeName').val(personInChargeName);
				$('#organizationDto_personInChargeNo').val(data.rslt.obj.attr("personInChargeNo"));
			}else{		
				$('#organizationDto_personInChargeName').val('');
				$('#organizationDto_personInChargeNo').val('');
			}
		}
		
	};

}();