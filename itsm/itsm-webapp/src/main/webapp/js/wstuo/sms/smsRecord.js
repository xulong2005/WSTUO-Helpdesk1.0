$package('wstuo.sms');

$import('wstuo.user.userUtil');

 /**  
 * @author coney  
 * @constructor affiche
 * @description tools/Affiche.jsp
 * @date 2010-11-17
 * @since version 1.0 
 */
wstuo.sms.smsRecord = function(){

	
	this._gridId='';
	
	return {
		/**
		 * money格式化
		 */
		moneyFormatter:function(cell,event,data){
			return "￥"+cell;
		},
		/**
		 * 加载列表.
		 * @param gridId 列表id
		 */
		showGrid:function(gridId) {

			_gridId=gridId;
			
			var params = $.extend({},jqGridParams, {	
				url:'smsRecord!findPager.action',
				colNames:[i18n['label_sms_receivingMobile'],i18n['label_sms_content'],i18n['label_sms_sender'],i18n['label_sms_sendTime'],i18n['label_sms_cost'],'',''],
				colModel:[
			   		{name:'mobile',width:15,align:'center'},
			   		{name:'contentString',sortable:false,width:35,formatter:function(cell,event,data){
		
			   			return '<a href=javascript:wstuo.sms.smsRecord.showSMS("'+data.id+'")>'+data.content+'</a>';

			   		}},
			   		{name:'sender',align:'center',width:15},
			   		{name:'sendTime',align:'center',width:20,formatter:timeFormatter},
			   		{name:'cost',hidden:true,align:'center',width:15,formatter:wstuo.sms.smsRecord.moneyFormatter},
			   		{name:'id',hidden:true},
			   		{name:'content',hidden:true}
			   	
			   	],		
				jsonReader: $.extend(jqGridJsonReader, {id: "id"}),
				sortname:'id',
				pager:gridId+'Pager',
				ondblClickRow:wstuo.sms.smsRecord.showSMS
			});
			$(gridId).jqGrid(params);
			$(gridId).navGrid(gridId+'Pager',navGridParams);
			//列表操作项
			$(gridId.replace('#','#t_')).css(jqGridTopStyles);
			$(gridId.replace('#','#t_')).append($(gridId+'Toolbar').html());
			//自适应大小
			//setGridWidth(gridId,20);
		},
		
		/**
		 * 打开短信记录详情
		 * @param rowId 列表行对象id
		 */
		showSMS:function(rowId){
			windows('sms_record_detail_window',{width:450,height:230});
			var rowData=$(_gridId).getRowData(rowId);
			
			$('#sms_detail_mobile').html(rowData.mobile);
			$('#sms_detail_content').html(rowData.content);
			$('#sms_detail_sender').html(rowData.sender);
			$('#sms_detail_sendTime').html(rowData.sendTime);
			$('#sms_detail_cost').html(rowData.cost);
			
		},
		/**
		 * 删除
		 */
		delRecord:function(){
			
			checkBeforeDeleteGrid(_gridId,function(ids){
				
				var _param=$.param({'ids':ids},true);
				
				$.post('smsRecord!delete.action',_param,function(){
					
					$(_gridId).trigger('reloadGrid');	
					msgShow(i18n['label_sms_deleteSuccessful'],'show');
				});
				
			});
		},
		/**
		 * 打开搜索面板
		 */
		search:function(){
			windows('search_sms_record_window',{width:450});
		},
		/**
		 * 执行搜索
		 */
		doSearch:function(){
			if($('#search_sms_record_window form').form('validate')){
				var sdata = $('#search_sms_record_window form').getForm();
				var postData = $(_gridId).jqGrid("getGridParam", "postData");       
				$.extend(postData, sdata);	
				$(_gridId).trigger('reloadGrid',[{"page":"1"}]);
				return false;
			}
		}

	};	
}();
