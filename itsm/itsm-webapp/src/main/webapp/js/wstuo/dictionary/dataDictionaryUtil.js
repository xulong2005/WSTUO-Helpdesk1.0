$package('wstuo.dictionary');
/**  
 * @author QXY  
 * @constructor dictionary
 * @description 数据字典工具类
 * @date 2010-11-17
 * @since version 1.0 
 */  
wstuo.dictionary.dataDictionaryUtil = function(){
	return{
		/**
		 * @description 加载数据字典.
		 * @param groupCode 编码
		 * @param select 对应下拉元素
		 */
		loadOptionsByCode:function(groupCode,select,callback){
			setTimeout(function(){//延迟加载
				$(select).empty();
				if(select!='#leftop')
				$('<option value="">-- '+i18n["common_pleaseChoose"]+' --</option>').appendTo(select);
				if(groupCode=="week"){
					HTML="<option value='Monday'>"+i18n["label_calendar_Monday"]+"</option>"
						+"<option value='Tuesday'>"+i18n["label_calendar_Tuesday"]+"</option>"
						+"<option value='Wednesday'>"+i18n["label_calendar_Wednesday"]+"</option>"
						+"<option value='Thursday'>"+i18n["label_calendar_Thursday"]+"</option>"
						+"<option value='Friday'>"+i18n["label_calendar_Friday"]+"</option>"
						+"<option value='Saturday'>"+i18n["label_calendar_Saturday"]+"</option>"
						+"<option value='Sunday'>"+i18n["label_calendar_Sunday"]+"</option>";
					$(HTML).appendTo(select);
				}else{
					var url = 'dataDictionaryItems!findByCode.action?dataDictionaryQueryDto.groupCode='+groupCode;
					$.post(url,function(res){
						if(res!=null && res.length>0){
							for(var i=0;i<res.length;i++){
								$('<option value="'+res[i].dcode+'">'+res[i].dname+'</option>').appendTo(select);
							}
						}
						if(typeof(callback) == 'function'){
							callback();
						}
						
					});	
				}
				
			},0);
			
		},
		loadCheckboxOptionsByCode:function(groupCode,select,type,id,callback){
			setTimeout(function(){//延迟加载
				$(select).siblings("."+type+"-inline").remove();
				var url = 'dataDictionaryItems!findByCode.action?dataDictionaryQueryDto.groupCode='+groupCode;
				$.post(url,function(res){
					if(res!=null && res.length>0){
						for(var i=0;i<res.length;i++){
							$(select).before('&nbsp;&nbsp;<label class="'+type+'-inline"><input type="'+type+'" value="'+res[i].dcode+'" name="'+id+'" title="'+res[i].dname+'">'+res[i].dname+'</label>&nbsp;&nbsp;');
						}
					}
					if(typeof(callback) == 'function'){
						callback();
					}
				});	
			},0);
			
		},
		setSelectOptionsByCode:function(groupCode,select,selectVal){
			$(select).empty();
			if(select!='#leftop')
			$('<option value="">-- '+i18n["common_pleaseChoose"]+' --</option>').appendTo(select);
			var url = 'dataDictionaryItems!findByCode.action?dataDictionaryQueryDto.groupCode='+groupCode;
			$.post(url,function(res){
				if(res!=null && res.length>0){
					for(var i=0;i<res.length;i++){
						$('<option value="'+res[i].dcode+'">'+res[i].dname+'</option>').appendTo(select);
					}
				}
				$(select).val(selectVal);
			});	
		},
		/**
		 * @description 加载数据字典.
		 * @param groupNo 数据字典No
		 * @param select 对应下拉元素
		 */
		loadOptionsByGroupNo:function(groupNo,select){
			setTimeout(function(){//延迟加载
				$(select).html('<option value="">-- '+i18n["common_pleaseChoose"]+' --</option>');
				var url = 'dataDictionaryItems!findByGroupNo.action?dataDictionaryQueryDto.groupNo='+groupNo;
				$.post(url,function(res){
					if(res!=null && res.length>0){
						for(var i=0;i<res.length;i++){
							$('<option value="'+res[i].dcode+'">'+res[i].dname+'</option>').appendTo(select);
						}
					}
				});	
			},0);
		}
	}
}();