$package('wstuo.category');
$import('wstuo.category.CICategoryTree');
$import('wstuo.category.CICategoryUtil');
/**  
 * @author WSTUO  
 * @description 配置项分类
 * @since version 1.0 
 */ 
wstuo.category.CICategory=function(){
	return {
		/**
		 * @description 添加配置项分类.
		 * */
		saveConfigItemCategory:function() {
			var parentNo=$('#add_parentNo').val();
			var rootId = $('#configureItemCategoryTree_div').find('li').length;			
			if(parentNo===null || parentNo==='' && rootId > 0){
				msgAlert(i18n.msg_msg_pleaseChooseParent,'info');
			}else{
				if($("#addCI_cname").val().toLowerCase() == "null"){
					$("#addCI_cname").val("");
					msgAlert(i18n['err_categoryNameNotNull'],'info');
				}
					var _params = $('#addConfigurationItemForm').serialize();
					startProcess();
					$.post('ciCategory!isCategoryExisted.action', _params, function(data){
                        if(data) {
                            msgAlert(i18n['err_categoryNameIsExisted'],'info');
                            endProcess();
                        } else {
                            var url = 'ciCategory!save.action';
                            if(rootId>0){
                                if($('#addConfigurationItemForm').form('validate')){
                                    $.post(url, _params,function (r) {
                                      wstuo.category.CICategoryTree.showCICategoryTree();
                                      $('#add_parentNo').val(parentNo);
                                      $('#add_category,#add_description,#addCMUploadIcon,#addCI_cname,#add_categoryCodeRule').val('');
                                      $('#add_showIconPanel').html("");msgShow(i18n.msg_add_successful,'show');
                                    endProcess();
                                    },'json');
                                }
                            }else{
                                if($('#addConfigurationItemForm').form('validate')){
                                    $.post(url, _params+"&configurationItemsDto.categoryRoot=CICategory",function (r) {
                                    wstuo.category.CICategoryTree.showCICategoryTree();
                                      $('#add_parentNo').val(parentNo);
                                      $('#add_category,#add_description,#addCMUploadIcon,#addCI_cname,#add_categoryCodeRule').val('');
                                      $('#add_showIconPanel').html("");
                                      msgShow(i18n.msg_add_successful,'show');
                                    endProcess();
                                    },'json');
                                }   
                            }
                        }
					});
			}
		},
		/**
        * @description 编辑配置项分类.
        */
		updateConfigItemCategory:function() {
			var cno=$('#edit_cno').val();
			if(cno===null || cno===''){
				msgAlert(i18n.msg_msg_pleaseChooseCategory,'info');
			}else{
			    var _params = $('#editConfigurationItemForm').serialize();
			    var _url = 'ciCategory!update.action';
			    startProcess();
			    $.post('ciCategory!isCategoryExistdOnEdit.action', _params, function(data){
                    if(data) {
                    	endProcess();
                        msgAlert(i18n['err_categoryNameIsExisted'],'info');
                    } else {
                        if($('#editConfigurationItemForm').form('validate')){
                            $.post(_url,_params,function (r) {
                                wstuo.category.CICategoryTree.showCICategoryTree();
                                msgShow(i18n.msg_edit_successful,'show');
                                endProcess();
                            },'json');
                        }
                    }
			    });
			}
		},
		/**
		 * @description  加载扩展属性下拉列表.
		 * @param select 扩展属性下拉列表Id
		 */
		loadEavsToSelect:function(select){
			wstuo.category.CICategoryUtil.loadEavsToSelect(select);
		},
		/**
		 * 导出配置项分类
		 */
		exportCiCategory:function(){
			$("#exportCiCategoryWindow form").submit();
		},
		/**
		 * 打开导入文件框
		 */
		importCiCategory:function(){
			windows('importCiCategoryWindow',{width:400});
		},
		/**
		 * @description 导入数据库
		 */
		importCiCategoryCsv:function(){
			$.ajaxFileUpload({
              url:'ciCategory!importCiCategory.action',
              secureuri:false,
              fileElementId:'importCiCategoryFile', 
              dataType:'json',
              success: function(data,status){
                if(data=="FileNotFound"){
                  msgAlert(i18n.msg_dc_fileNotExists,'info');
                }else if(data=="IOError"){
                  msgAlert(i18n.msg_dc_importFailure,'info');
                }else{
                  msgAlert(i18n.msg_dc_importSuccess.replace('N','<br>['+data+']<br>'),'show');
                  wstuo.category.CICategoryTree.showCICategoryTree();
                  $('#importEventCategoryWindow_'+_categoryCode).dialog('close');
                }
              }
            });
		},
		/**
		 * @description  加载自定义表单下拉列表.
		 * @param select 自定义表单下拉列表Id
		 */
		loadFormToSelect:function(select){
			var url = "formCustom!findAllFormCustom.action";
			$.post(url, function(res){
				$(select).html('');
				$('<option value="">--'+i18n.pleaseSelect+'--</option>').appendTo(select);
				$.each(res,function(index,formCustom){
					$("<option value='"+formCustom.formCustomId+"'>"+formCustom.formCustomName+"</option>").appendTo(select);
				});
			});
		},
		/**
		 * 初始化
		 * @private
		 */
		init:function(){
			showAndHidePanel('#configItemCategoryContent','#configItemCategoryLoading');//隐显面板
			wstuo.category.CICategoryTree.init();//加载树结构
			$('#cicategory_save_data').click(wstuo.category.CICategory.updateConfigItemCategory);//绑定事件
			$('#cicategory_save_sub').click(wstuo.category.CICategory.saveConfigItemCategory);//绑定事件
			//wstuo.category.CICategory.loadEavsToSelect("#edit_category,#add_category");//加载列表
			//wstuo.category.CICategory.loadFormToSelect("#add_ciformId,#edit_ciformId");//加载列表
		}
	}
 }();
 $(document).ready(wstuo.category.CICategory.init);
