$package('wstuo.category');
/**  
 * @author wstuo  
 * @description 分类树
 * @since version 1.0 
 */ 
wstuo.category.eventCategoryTree=function(){
	this._cname='';
	this.treeID='';
	this.param='';
	this.ff=function(){};
	return {
		 /**
		  * @description 加载分类树.
		  * @param treeID 树Id
		  */
		  showCategoryTree:function(treeID,param,ff) {
			  this.treeID=treeID;this.param=param;this.ff=ff;
	          $(treeID).jstree({
					json_data: {
						ajax: {
						  type:"post",
						  url : "event!getCategoryTree.action?flag=dataDic&num="+category_num+"",
					      data:function(n){
					    	  return {'types': param,'parentEventId':n.attr ? n.attr("id").replace("node_",""):0};//types is in action
					      },
		                  dataType: 'json',
		                  cache:false
						}
					},
				"plugins" : ["themes","unique", "json_data", "ui", "crrm", "cookies", "dnd", "search", "types", "hotkeys", "contextmenu"]
			})
			.bind("delete_node.jstree",wstuo.category.eventCategoryTree.removeNode)
			.bind("select_node.jstree",function(e,data){
				wstuo.category.eventCategoryTree.selectNode(e,data,treeID);
			})
			.bind("move_node.jstree", function(e,data){
				wstuo.category.eventCategoryTree.moveNode(e,data,treeID);
			})
			.bind("loaded.jstree",function(e,data){
				if(ff){
					$('body').popover({
						html:true,
						placement:'right',
						trigger:'click',
						selector:treeID+' li a',
						content:'&nbsp;<button class="btn btn-default btn-xs tree_opt" opt="add"><i class="glyphicon glyphicon-plus"></i> '+i18n.add+'</button>'+
							'<button class="btn btn-default btn-xs tree_opt" opt="edit"><i class="glyphicon glyphicon-edit"></i> '+i18n.edit+'</button>'
					});
					$('body').on('show.bs.popover', function () {
						 $('.tree_opt').parent().parent().remove();
					});
					$('body').on('shown.bs.popover', function () {
						 $('.tree_opt').unbind('click');
						 $(".tree_opt").click(function(e){
							ff($(this).attr('opt'));
						 });
					});
				}
			});
	       
		 },
		 
		 /**
			 * @description 删除节点.
			 * @param e  事件
			 * @param data 行数据
			 * @private
			 */
		  removeNode:function (e, data) {
			 var categoryRoot = data.rslt.obj.attr("categoryRoot");
				//不允许删除父节点
				if(categoryRoot!=null && categoryRoot!='null' && categoryRoot!=''){
					msgShow(i18n['ERROR_PARENT_ORG_DONOT_DELETE']+"=="+categoryRoot,'info');
					$.jstree.rollback(data.rlbk);
				}else{
					var _url = "event!remove.action";
					var _params = {
						"eventCategoryDto.eventId" : data.rslt.obj.attr("id")
					};
					$.post(_url,_params,function (r) {
						if(r=="1"){
							msgShow(i18n['msg_operationSuccessful'],'show');
							wstuo.category.eventCategoryTree.showCategoryTree(treeID,param,ff);
						}else{
							msgAlert(i18n['ERROR_DATA_CAN_NOT_DELETE'],'info');
							$.jstree.rollback(data.rlbk);
						}
					});
				}
			},
		  /**
		   * @description 拖拽节点.
		   * @param treeId 树节点
		   */
			moveNode :function (e, data,treeId) {
				data.rslt.o.each(function (i) {
					   var tag=data.rslt.cy;
					   var _cno = data.rslt.o.attr("id");
					   var _pno = data.rslt.np.attr("id");
					   
					   var _params = {
		                        "eventCategoryDto.eventId" : _cno,
		                        "eventCategoryDto.parentEventId" : _pno
		                    };
					   
					   // 判断分类是否存在    
                       $.post('event!isCategoryExisted.action', _params, function(data){
                           if(data) {
                               wstuo.category.eventCategoryTree.showCategoryTree(treeId);
                               msgAlert(i18n["msg_incorrect_operation"],"info");
                           } else {
        					   if(tag==true){
        						   //复制
        						   wstuo.category.eventCategoryTree.moveNode_copy(_cno,_pno,treeId);
        					   }else{
        						   //剪切
        						   wstuo.category.eventCategoryTree.moveNode_cut(_cno,_pno,treeId);
        					   }
                           }
                       });
				
				});
	
			},
			/**
			 * @description 复制节点方法.
			 * @param _cno 分类Id
			 * @param _pno 父节点Id
			 * @param treeID 树Id
			 */
			moveNode_copy:function(_cno,_pno,treeId){
				
				if(!isNaN(_cno)&&!isNaN(_pno)){
					
					var _url = "event!copyCategory.action";					 
					var _params = {
						"eventCategoryDto.eventId" : _cno,
						"eventCategoryDto.parentEventId" : _pno
					};
					$.post(_url,_params,
						function (r) {							
							wstuo.category.eventCategoryTree.showCategoryTree(treeId);
							msgShow(i18n['msg_operationSuccessful'],'show');
						},
						'json'
					);
				}else{
					wstuo.category.eventCategoryTree.showCategoryTree(treeId);
					msgAlert(i18n["msg_incorrect_operation"],"info");
				}
				
				
				 	
			},
			 /**
			  * @description 剪切节点方法.
			  */
			moveNode_cut:function(_cno,_pno,treeId){
				if(!isNaN(_cno)&&!isNaN(_pno)){
					var _url = "event!changeParents.action";
					var _params = {
						"eventCategoryDto.eventId" : _cno,
						"eventCategoryDto.parentEventId" : _pno
					};
					$.post(_url,_params,
						function (r) {								
							wstuo.category.eventCategoryTree.showCategoryTree(treeId);
							msgShow(i18n['msg_operationSuccessful'],'show');
						},
						'json'
					);
					
				}else{
					wstuo.category.eventCategoryTree.showCategoryTree(treeId);
					msgAlert(i18n["msg_incorrect_operation"],"info");
				}
				
			},
			 /**
			  * @description 单击节点.
			  */
			selectNode :function (e, data,treeID) {
				startProcess();
				var obj = data.rslt.obj;
				var _parent = data.inst._get_parent();
				if($vl(obj.attr('cname'))==="More..."&& obj.attr('start')!=undefined){
					$.ajax({
						type:"post",
						url :"event!getCategoryTree.action?flag=dataDic&num="+category_num+"&start="+obj.attr('start')+"&types="+param+"&parentEventId="+(_parent.attr ? _parent.attr("id").replace("node_",""):0),
		                dataType: 'json',
		                cache:false,
						success:function(data0){
							var bool=false;
							$.each(data0, function(s, n) {
								$(treeID).jstree("create_node", obj, "before", n
								).bind("select_node.jstree",function(e,data1){
									wstuo.category.eventCategoryTree.selectNode(e,data1,treeID);
								});
								bool=n.attr.bool;
							});
							obj.attr('start',parseInt(obj.attr('start'))+1);
							if(!bool){
								msgShow("没有更多了...",'show');
								obj.attr('cname',"NoMore");
							}
							endProcess();
					    }
					});
				}else if($vl(obj.attr('cname'))==="NoMore"){
					endProcess();
					msgShow("没有更多了...",'show');
				}else{
					var _parentNo = null;
					//不允许修改父节点
					/*if(_parent==-1){
						$('#eventName').attr("disabled","true");
					}else{
						$('#eventName').attr("disabled","");
					}*/
					
					if (_parent!=-1 && _parent.attr('eventId')) {
						_parentNo = _parent.attr('id');
					}
					$('#eventId').val($vl(obj.attr('id')));
					$('#eventName').val($vl(obj.attr('cname')));
					if(obj.attr('eavId')==0){
						$('#edit_category').val("");
					}else{
						$('#edit_category').val($vl(obj.attr('eavId')));
					}
					if(obj.attr('formId')==0){
						$('#edit_form').val("");
					}else{
						$('#edit_form').val($vl(obj.attr('formId')));
					}
					$('#edit_scores').val($vl(obj.attr('scores')));
					$('#eventDescription').val($vl(obj.attr('description')));
					$('#parentEventId').val($vl(obj.attr('id')));
					$('#categoryCodeRule').val($vl(obj.attr('categoryCodeRule')));
					endProcess();
				}
		 },
		 /**
		  * 公共模块，选择树
		  * @param windowId 窗口元素
		  * @param treePanel 树元素
		  * @param param 类别
		  * @param namePut 名称元素
		  * @param idPut Id元素
		  * @param otherPut1 标题输入框ID
		  * @param otherPut2 描述输入框ID
		  * @param showAttrId 扩展属性Id
		  * @param dtoName 关联模块
		  */
		 showSelectTree:function(windowId,treePanel,param,namePut,idPut,otherPut1,otherPut2,showAttrId,dtoName,method){
			 var bool=false;
			 $(treePanel).jstree({
					json_data: {
						ajax: {
						  type:"post",
						  url : "event!getCategoryTree.action?num="+category_num+"",
						  data:function(n){
					    	  return {'types': param,'parentEventId':n.attr ? n.attr("id").replace("node_",""):0};//types is in action
					      },
		                  dataType: 'json',
		                  cache:false
					 }
					},
					plugins : ["themes", "json_data","cookies", "ui", "crrm","search"]
			})
			.bind("select_node.jstree",function(e,data){
				wstuo.category.eventCategoryTree.selectNode_index(e,data,windowId,treePanel,param,namePut,idPut,otherPut1,otherPut2,showAttrId,dtoName,method);
			});
			windows(windowId.replace('#',''),{width:240,close:function(){
				$(namePut).blur();
			}});
	 
		 },
		 selectNode_index:function(e,data,windowId,treePanel,param,namePut,idPut,otherPut1,otherPut2,showAttrId,dtoName,method){
			var obj = data.rslt.obj;
			var _parent = data.inst._get_parent();
			if($vl(obj.attr('cname'))==="More..." && obj.attr('start')!=undefined){
				startProcess();
				$.ajax({
					type:"post",
					url :"event!getCategoryTree.action?flag=dataDic&num="+category_num+"&start="+obj.attr('start')+"&types="+param+"&parentEventId="+(_parent.attr ? _parent.attr("id").replace("node_",""):0),
	                dataType: 'json',
	                cache:false,
					success:function(data0){
						//$(treePanel).jstree("remove", obj);
						var bool=false;
						$.each(data0, function(s, n) {
							$(treePanel).jstree("create_node", obj, "before", n
							).bind("select_node.jstree",function(e,data1){
								wstuo.category.eventCategoryTree.selectNode_index(e,data1,windowId,treePanel,param,namePut,idPut,otherPut1,otherPut2,showAttrId,dtoName,method);
							});
							bool=n.attr.bool;
						});
						obj.attr('start',parseInt(obj.attr('start'))+1);
						endProcess();
						if(!bool){
							msgShow("没有更多了...",'show');
							obj.attr('cname',"NoMore");
						}
						
				    }
				});
			}else if($vl(obj.attr('cname'))==="NoMore"){
				msgShow("没有更多了...",'show');
			}else{
				catName=obj.attr("cname");
				
				if(($(idPut).attr("id")=="request_locationNos" && $(namePut).attr("id")=="request_locationName") ||
						($(idPut).attr("id")=="ci_locid" && $(namePut).attr("id")=="ci_loc") || 
						($(idPut).attr("id")=="ci_locid_edit" && $(namePut).attr("id")=="ci_loc_edit")){
					var _url = "event!findLocationNameById.action?eventId="+obj.attr("id");
					$.post(_url,function(data){
						$(namePut).val(data.categoryLocation);
						$(idPut).val(obj.attr("id"));
					});
				}else{
					$(namePut).val(obj.attr("cname")).focus();
					$(idPut).val(obj.attr("id"));
				}
				
				if(otherPut1!=undefined && otherPut1!='' && otherPut2!=undefined && otherPut2!=''){
					if($(otherPut1).val().indexOf(_cname)>-1){
						$(otherPut1).val($(otherPut1).val().replace(_cname,data.rslt.obj.attr("cname")))
					}else{
						$(otherPut1).val($(otherPut1).val().replace(_cname,'')+data.rslt.obj.attr("cname"))
					}
					if( $(otherPut2).val()!=undefined ){
						if(otherPut2!='' && $(otherPut2).val().indexOf(_cname)>-1){
							$(otherPut2).val($(otherPut2).val().replace(_cname,data.rslt.obj.attr("cname")))
						}else{
							$(otherPut2).val($(otherPut2).val().replace(_cname,'')+data.rslt.obj.attr("cname"))
						}
					}
				}
				if( typeof method === 'function' )
					method(data.rslt.obj.attr('formId'));
				//_cname=data.rslt.obj.attr("cname");
				//itsm.cim.ciCategoryTree.showAttributet(data.rslt.obj.attr('eavId'),showAttrId,dtoName);
				$(windowId).dialog('close');//关闭窗口
			}
		 },
		 /**
		  * 选择变更分类.
		  * @param namePut 名称元素
		  * @param idPut Id元素
		  */
		 changeCategory:function(namePut,idPut){
			 wstuo.category.eventCategoryTree.showSelectTree('#change_category_select_window','#change_category_select_tree','Change',namePut,idPut);
		 },
		 
		 /**
		  * 选择问题分类.
		  * @param namePut 名称元素
		  * @param idPut Id元素
		  * @param showAttrId 扩展属性Id
		  * @param dtoName 关联模块
		  */
		 problemCategory:function(namePut,idPut,showAttrId,dtoName){
			 wstuo.category.eventCategoryTree.showSelectTree('#problem_category_select_window','#problem_category_select_tree','Problem',namePut,idPut,'','',showAttrId,dtoName);
		 },
		 /**
		  * 选择问题子分类.
		  * @param namePut 名称元素
		  * @param idPut Id元素
		  * @param showAttrId 扩展属性Id
		  * @param dtoName 关联模块
		  * @param categoryNo 分类Id
		  */
		 problemCategory_sub:function(namePut,idPut,showAttrId,dtoName,categoryNo){
			 wstuo.category.eventCategoryTree.showSelectTreeZi('#problem_category_select_window','#problem_category_select_tree','Problem',namePut,idPut,'','',showAttrId,dtoName,categoryNo);
		 },
		 
		 /**
		  * 选择请求分类.
		  * @param namePut 名称元素
		  * @param idPut Id元素
		  */
		 requestCategory:function(namePut,idPut){
			 wstuo.category.eventCategoryTree.showSelectTree('#request_category_select_window','#request_category_select_tree','Request',namePut,idPut);
		 },
		 /**
		  * 关联变更选择变更分类.
		  * @param namePut 名称元素
		  * @param idPut Id元素
		  * @param showAttrId 扩展属性Id
		  * @param dtoName 关联模块
		  */
		 selectChangeCategory:function(namePut,idPut,showAttrId,dtoName){
			 wstuo.category.eventCategoryTree.showSelectTree('#change_category_select_window','#change_category_select_tree','Change',namePut,idPut,'','',showAttrId,dtoName);
		 },
		 /**
		  * 选择发布分类.
		  * @param namePut 名称元素
		  * @param idPut Id元素
		  */
		 releaseCategory:function(namePut,idPut){
			 wstuo.category.eventCategoryTree.showSelectTree('#release_category_select_window','#release_category_select_tree','Release',namePut,idPut);
		 },
		 /**
		  * 选择发布子分类.
		  * @param namePut 名称元素
		  * @param idPut Id元素
		  * @param categoryNo 分类Id
		  */
		 releaseCategorySub:function(namePut,idPut,categoryNo){
			 wstuo.category.eventCategoryTree.showSelectTreeZi('#release_category_select_window','#release_category_select_tree','Release',namePut,idPut,'','','','',categoryNo);
		 },
		 /**
		  * 选择显示配置项所在位置.
		  * @param namePut 名称元素
		  * @param idPut Id元素
		  */
		 selectlocation:function(namePut,idPut){
			 wstuo.category.eventCategoryTree.showSelectTree('#ci_loc_select_window','#ci_loc_select_tree','Location',namePut,idPut);
		 },
		 /**
		  * 公共模块，选择树
		  * @param windowId   窗体编号
		  * @param treePanel  树面板
		  * @param param      参数
		  * @param namePut    名称元素
		  * @param idPut      Id元素
		  * @param otherPut1: 标题输入框ID
		  * @param otherPut2: 描述输入框ID
		  * @param showAttrId 显示AttrId  
		  * @param dtoName    dto名称
		  * @param CategoryNo  分类编号
		  */
		 showSelectTreeZi:function(windowId,treePanel,param,namePut,idPut,otherPut1,otherPut2,showAttrId,dtoName,CategoryNo){
			   $(treePanel).jstree({
					json_data: {
						ajax: {
						  type:"post",
						  url : "event!getCategoryTree.action?num="+category_num+"",
						  data:function(n){
					    	  return {'types': param,'parentEventId':n.attr ? n.attr("id").replace("node_",""):CategoryNo};//types is in action
					      },
		                  dataType: 'json',
		                  cache:false
					 }
					},
					plugins : ["themes", "json_data","cookies", "ui", "crrm"]
			})
			.bind("select_node.jstree",function(e,data){
				
				if(data.rslt.obj.attr('state')==null || data.rslt.obj.attr('state')=='' || data.rslt.obj.attr('state')==undefined){
					$(namePut).val(data.rslt.obj.attr("cname"));
					$(idPut).val(data.rslt.obj.attr("id"));
					if(otherPut1!=undefined && otherPut1!='' && otherPut2!=undefined && otherPut2!=''){
						if($(otherPut1).val().indexOf(_cname)>-1){
							$(otherPut1).val($(otherPut1).val().replace(_cname,data.rslt.obj.attr("cname")))
						}else{
							$(otherPut1).val($(otherPut1).val().replace(_cname,'')+data.rslt.obj.attr("cname"))
						}
						
						if(otherPut2!='' && $(otherPut2).val().indexOf(_cname)>-1){
							$(otherPut2).val($(otherPut2).val().replace(_cname,data.rslt.obj.attr("cname")))
						}else{
							$(otherPut2).val($(otherPut2).val().replace(_cname,'')+data.rslt.obj.attr("cname"))
						}
					}
					_cname=data.rslt.obj.attr("cname");
					
					/*if("Release"!=param)
						itsm.cim.ciCategoryTree.showAttributet(data.rslt.obj.attr('eavId'),showAttrId,dtoName);*/
					$(windowId).dialog('close');//关闭窗口
				 }else{
					msgShow(i18n.tip_please_select_sub_node,'show');
				 }
			});
			windows(windowId.replace('#',''),{width:240});
	 
		 },
		 /**
		  * 初始化
		  * @private
		  */
		 init:function(){
			 //wstuo.category.eventCategoryTree.showCategoryTree();
		 }
	}
 }();