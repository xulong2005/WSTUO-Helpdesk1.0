$package('wstuo.sysMge')
$import('wstuo.sysMge.base64Util');
$import('wstuo.sysMge.pagesSet');

/**  
 * @fileOverview 公司信息设置主函数.
 * @author Van
 * @version 1.0  
 */  

var company = function() 
{
	/**
	 * @description 查找公司信息
	 */
	this.findCompanyInfo=function (){
		this._flag = true;
		var refreshMessage;
		var url = 'organization!findCompany.action?companyNo='+companyNo;
		$.post(url,function(res){
			
			$('#init_orgName').val(res.orgName);
			$('#init_officeFax').val(res.officeFax);
			$('#init_officePhone').val(res.officePhone);
			$('#init_email').val(res.email);
			$('#init_address').val(res.address);
			$('#init_homePage').val(res.homePage);
			
			$('#init_logo').val(res.logo);
			
			if(res.logo!=''){
				$('#showCompanyCompanyLogo').html('<img id="imgshowCompanyCompanyLogo" src="'+$("#index_logo").attr("src")+'" height="30px" />');
			}
			
			//showIndexLogo();//更新LOGO
		});
	}
	
	/**
	 * @description 保存信息
	 */
	this.saveCompanyInfo=function (){	
			var frm = $('#companyInfoForm').serialize();
			var _url = 'organization!mergeCompany.action?companyNo='+companyNo;
		
			if($('#init_orgName').val()=="" || $('#init_orgName').val()==null){
				msgShow(i18n['companyNameNotNull'],'show');
			}else if($('#pagesSysName').val()=="" || $('#pagesSysName').val()==null){
				msgShow(i18n['systemNameNotNull'],'show');
			}else{
			
			$.post(_url,frm, function(){
			
				$("#index_logo").attr("src",$("#showCompanyCompanyLogo").attr("src"));
				$("#index_companyName").html($("#init_orgName").val()); 
				
				var str_gsm=$("#init_orgName").val();
			
				if($("#init_homePage").val()!= ''){
					$('#index_logo_href').attr({'href':$("#init_homePage").val()});
				}else{
					
					$('#index_logo_href').attr({'onClick':'javascript:basics.tab.tabUtils.addTab("'+i18n.companyInformation+'","../pages/wstuo/baseData/soft_setting.jsp")'});
				}
				
				companyName = $('#init_orgName').val();//当前用户所在公司名称
				

			});
	
			//保存系统名称
			if($('#pagesSysName').val()!=undefined  && $('#pagesSysName').val().length>0){
				wstuo.sysMge.pagesSet.update();
			}
			
			//保存登录logo
			if($("#Login_logo").val()!=undefined  && $("#Login_logo").val().length>0){				
				var url="pagesSet!updatePagesSet.action";
				$.post(url,{'entity.imgname':$("#Login_logo").val(),'entity.id':1},function(data){
				});
			}
		
			msgShow(i18n['saveSuccess'],'show');
		}
	}
	/**
	 * @description 发送邮件连接测试.
	 * @author mars
	 */
	this.emailConnTest=function(){	
		startProcess();
		wstuo.sysMge.base64Util.encodePassword('#company_password','#company_password_reality','#company_password_hidden');
		var frmp="emailConnectionDto.smtphost="+$('#smtp_serverAddress').val()+"&emailConnectionDto.smtpport="+$('#smtp_serverPort').val()
		+"&emailConnectionDto.username="+$('#userName').val()+"&emailConnectionDto.password="+$('#company_password_reality').val()
		
		$.post('email!emailConnTest.action',frmp,function(testErrorEncode){
			var emailTestResult=i18n['msg_smtp_test_result']+"[SMTP-RESULT]"
			endProcess();
			switch(testErrorEncode){
				case 0: msgShow(emailTestResult.replace('SMTP-RESULT',i18n['success']),'show'); break;//服务发送邮箱测试成功
				case 1: msgShow(emailTestResult.replace('SMTP-RESULT',i18n['success']),'show'); break;//服务邮箱测试失败
				case 2:	msgShow(emailTestResult.replace('SMTP-RESULT',i18n['failure']),'show'); break;//发送邮箱测试失败
				default: msgShow(emailTestResult.replace('SMTP-RESULT',i18n['failure']),'show');//服务和发送邮箱测试失败
			}	
		})
	}
	
	/**
	 * @description 接收邮件连接测试.
	 * @author mars
	 */
	this.ReceiveEmailConnTest=function(){

			startProcess();
			wstuo.sysMge.base64Util.encodePassword('#company_receive_password','#company_receive_password_reality','#password_hidden');
			
			var frmp="emailConnectionDto.username="+$('#ReceiveUserName').val()+"&emailConnectionDto.password="+$('#company_receive_password_reality').val()
			+"&emailConnectionDto.pop3host="+$('#pop3_ReceiveServerAddress').val()+"&emailConnectionDto.pop3port="+$('#pop3_ReceiveServerPort').val()
			
			$.post('email!emailConnTest.action',frmp,function(testErrorEncode){
				var emailTestResult=i18n['msg_pop_test_result']+"[POP-RESULT]<br><br>"
				endProcess();
				switch(testErrorEncode){
					case 0: msgShow(emailTestResult.replace('POP-RESULT',i18n['success']),'show'); break;//服务发送邮箱测试成功
					case 1: msgShow(emailTestResult.replace('POP-RESULT',i18n['failure']),'show'); break;//服务邮箱测试失败
					case 2:	msgShow(emailTestResult.replace('POP-RESULT',i18n['success']),'show'); break;//发送邮箱测试失败
					default: msgShow(emailTestResult.replace('POP-RESULT',i18n['failure']),'show');//服务和发送邮箱测试失败
				}	
			})
		
	}
	
	/**
	 * @description Exchange邮件连接测试.
	 * @author mars
	 */
	this.exchangeEmailConnTest=function(type){
		startProcess();
		var frmp='';
		if(type=='server'){
			 wstuo.sysMge.base64Util.encodePassword('#exchangePassword','#exchangePassword_reality','#exchangePassword_hidden');//编码密码
			 frmp="emailServerDTO.exchangeHostName="+$('#exchangeHostName').val()+"&emailServerDTO.exchangeUserName="+$('#exchangeUserName').val()
			+"&emailServerDTO.exchangePassword="+$('#exchangePassword_reality').val()+"&emailServerDTO.domain="+$('#exchangeDomain').val()
			+"&emailServerDTO.exchangeEmailAccount="+$('#exchangeEmailAccount').val()+"&emailServerDTO.emailVersion="+$('#exchangeVersionSelect').val()
		}
		if(type=='receive'){
			 wstuo.sysMge.base64Util.encodePassword('#ReceiveExchangePassword','#ReceiveExchangePassword_reality','#ReceiveExchangePassword_hidden');//编码密码
			 
			 frmp="emailServerDTO.exchangeHostName="+$('#ReceiveExchangeHostName').val()+"&emailServerDTO.exchangeUserName="+$('#ReceiveExchangeUserName').val()
			+"&emailServerDTO.exchangePassword="+$('#ReceiveExchangePassword_reality').val()+"&emailServerDTO.domain="+$('#ReceiveExchangeDomain').val()
			+"&emailServerDTO.exchangeEmailAccount="+$('#ReceiveExchangeEmailAccount').val()+"&emailServerDTO.emailVersion="+$('#ReceiveExchangeVersionSelect').val()
		}
		$.post('email!exchangeEmailConnTest.action',frmp,function(res){
			endProcess();
			if(res)
				msgShow(i18n['msg_company_connectionSuccessful'],'show');
			else
				msgShow(i18n['ERROR_CONN_WMI_FAILURE'],'show');
		})
	}
	
	/**
	 * 发邮件邮件设置面板切换
	 */
	this.emailServerChange=function(value){
			$('#radio_value').val(value);
		if(value=='exchange'){
			$('#normalEmailSetting').hide();
			$('#exchangeEmailSetting').show();
			$('#emailConnTestLink').unbind();
			//$('#company_mailserver_save').unbind();
			//$('#company_mailserver_save').click(function(){mailserver('exchange')});
			$('#emailConnTestLink').click(function(){exchangeEmailConnTest('server')});
		}else{
			$('#normalEmailSetting').show();
			$('#exchangeEmailSetting').hide();
			$('#emailConnTestLink').unbind();
			//$('#company_mailserver_save').unbind();
			//$('#company_mailserver_save').click(function(){mailserver('normalEmail')});
			$('#emailConnTestLink').click(function(){emailConnTest()});
		}
	}
	/**
	 * 发送
	 */

	this.mailserver=function(){

		wstuo.sysMge.mailServer.saveOrUpdateMailServer(mailserver);
	}
	
	/**
	 * 接收
	 */
	this.Receive=function(receive){
		wstuo.sysMge.mailServer.saveOrUpdateReceiveMailServer(receive);
	}
	/**
	 * 接收邮件设置面板切换
	 */
	this.emailReceiveServerChange=function(value){
		$('#ReceivelRadio').val(value);
		if(value=='Receive_exchange'){
			$('#ReceiveEmailSetting').hide();
			$('#ReceiveExchangeEmailSetting').show();
			$('#ReceiveEmailConnTestLink').unbind();
			//$('#company_receive_mailserver_save').unbind();
			//$('#company_receive_mailserver_save').click(function(){Receive('Receive_exchange')});
			$('#ReceiveEmailConnTestLink').click(function(){exchangeEmailConnTest('receive')});
		}else{
			$('#ReceiveEmailSetting').show();
			$('#ReceiveExchangeEmailSetting').hide();
			$('#ReceiveEmailConnTestLink').unbind();
			//$('#company_receive_mailserver_save').unbind();
			//$('#company_receive_mailserver_save').click(function(){Receive('Receive')});
			$('#ReceiveEmailConnTestLink').click(function(){ReceiveEmailConnTest()});
		}
		
	}
	
	/**
	 * 接收邮件设置服务端口切换
	 */
	this.checkPop3Change=function(value){

		if(value=='check_IMAP'){	
			$('#Pop3Radio').attr('checked',false);
			$('#pop3_ReceiveServerPort').val('143');
			$('#span_port').html('143');
			
		}else if(value=='check_POP3'){
			$('#ImapRadio').attr('checked',false);
			$('#pop3_ReceiveServerPort').val('110');
			$('#span_port').html('110');
			
		}
		
	}
	
	//从发送箱复制给接收箱
	this.toVal=function(){
		var receive_Personal=$("#Receive_Personal").val();
		var personalEmailAddress=$("#ReceivePersonalEmailAddress").val();
		var receiveUserName=$("#ReceiveUserName").val();
		var company_receive_password=$("#company_receive_password").val();
		var company_receive_password_reality=$("#company_receive_password_reality").val();
		if(receive_Personal==''){
			$("#Receive_Personal").val($("#personal").val());
		}
		if(personalEmailAddress==''){
			$("#ReceivePersonalEmailAddress").val($("#personalEmailAddress").val());
		}
		if(receiveUserName==''){
			$("#ReceiveUserName").val($("#userName").val());
		}
	    if(company_receive_password==''){
	    	$("#company_receive_password").val($("#company_password").val());
	    	$("#company_receive_password_reality").val($("#company_password_reality").val());
	    }
	}
	//加载
	  return{
			init: function() {
				findCompanyInfo();
				$('#import_company_btn').click(function(){
					if(language=="en_US"){
						$('#index_import_href').attr('href',"../importFile/en/CompanyInfo.zip");
					}else{
						$('#index_import_href').attr('href',"../importFile/CompanyInfo.zip");
					}
					windows('index_import_excel_window',{width:400});
					$("#index_import_confirm").unbind(); //清空事件      
					$('#index_import_confirm').click(function(){
						startProcess();
						var path=$('#importFile').val();
						if(path!=""){
							$.ajaxFileUpload({
					            url:'company!importCompanyInfo.action',
					            secureuri:false,
					            fileElementId:'importFile', 
					            dataType:'json',
					            success: function(data,status){
									if(data=="Error"){
										msgAlert(i18n['msg_dc_fileNotExists'],'error');
										endProcess();
									}else{
										$('#index_import_excel_window').dialog('close');
										findCompanyInfo();										
						            	msgShow(i18n['msg_company_dataImportSuccessful'],'show');
						            	resetForm('#index_import_excel_window form');
						            	endProcess();
									}
									
					            }
					        });
						}else{
							msgAlert(i18n['msg_dc_fileNull'],'info');
							endProcess();
						}
					});
				});
				
				$('#export_company_btn').click(function(){
					var url='pagesSet!showPagesSet.action';
					$.post(url,function(data){
						location.href="company!exportCompanyInfo.action?paName="+encodeURI(encodeURI(data.paName))+"&paLogo="+data.imgname;
					});
				});
				
				//当密码框发生按键事件时，说明用户正在修改密码，清空已保存的编码后密码
				$('#company_password').keyup(function(e){
					$('#company_password_reality').val("");
				});
				$('#exchangePassword').keyup(function(e){
					$('#exchangePassword_reality').val("");
				});
				$('#smsAccountDTO_pwd').keyup(function(e){
					$('#smsAccountDTO_pwd_reality').val("");
				});
			}
	  }
	}();

//载入
$(document).ready(company.init);
