(function( factory ) {
	if ( typeof define === "function" && define.amd ) {
		define( ["jquery", "../jquery.validate"], factory );
	} else {
		factory( jQuery );
	}
}(function( $ ) {

/*
 * Translated default messages for the jQuery validation plugin.
 * Locale: ZH (Chinese, 中文 (Zhōngwén), 汉语, 漢語)
 */
$.extend($.validator.messages, {
	required: "输入有误",
	remote: "请修正此字段",
	email: "请输入有效的电子邮件地址",
	url: "请输入有效的网址",
	date: "请输入有效的日期",
	dateISO: "请输入有效的日期 (YYYY-MM-DD)",
	number: "请输入有效的数字",
	digits: "只能输入数字",
	creditcard: "请输入有效的信用卡号码",
	equalTo: "你的输入不相同",
	extension: "请输入有效的后缀",
	maxlength: $.validator.format("最多可以输入 {0} 个字符"),
	minlength: $.validator.format("最少要输入 {0} 个字符"),
	rangelength: $.validator.format("请输入长度在 {0} 到 {1} 之间的字符串"),
	range: $.validator.format("请输入范围在 {0} 到 {1} 之间的数值"),
	max: $.validator.format("请输入不大于 {0} 的数值"),
	min: $.validator.format("请输入不小于 {0} 的数值")
});
//中文字两个字节
jQuery.validator.addMethod(
    "byteRangeLength", 
    function(value, element, param) {
        var length = value.length;
        for(var i = 0; i < value.length; i++){
            if(value.charCodeAt(i) > 127){
                length++;
            }
        }
        return this.optional(element) || (length >= param[0] && length <= param[1]);   
    }, 
    $.validator.format("请确保输入的值在{0}-{1}个字节之间(一个中文字算2个字节)")
);
$.validator.addMethod("phone",function(value,element){  
    	//var ck  =/^((\+?[0-9]{2,4}\-[0-9]{3,4}\-)|([0-9]{3,4}\-))?([0-9]{5,8})(\-[0-9]+)?$/;
		//mars add @20110705
		if(value==undefined || trim(value).length==0) return true; //为空不验证
		var ck=/([0-9]{3,4}\-)?[0-9]{7,8}$|[0-9]{3,4}\-[0-9]{7,8}\-[0-9]{3,4}$|(\+086-)[0-9]{3,4}\-[0-9]{7,8}$/;
		//var ck = /^(\(\d{3,4}\)|\d{3,4}-)?\d{7,8}$/;
		//var ck=/(^(/d{2,4}[-_－—]?)?/d{3,8}([-_－—]?/d{3,8})?([-_－—]?/d{1,7})?$)|(^0?1[35]/d{9}$)/;
    	return ck.test(value);
    	//return true;
    }, 
    i18n['validate_mobile_fax']
);
$.validator.addMethod("mobile",function(value,element){ 
		if(value==undefined || trim(value).length==0) return true; //为空不验证
		var ck = /^1[3,5,8,9,7]\d{9}$/;
		return ck.test(value);
	}, 
    i18n['validate_mobile_no']//message: i18n['error_mobile']
);
$.validator.addMethod("ip",function(value,element){
		if(value==undefined || trim(value).length==0) return true; //为空不验证
		var ck = /^(\d{1,2}|1\d\d|2[0-4]\d|25[0-5])\.(\d{1,2}|1\d\d|2[0-4]\d|25[0-5])\.(\d{1,2}|1\d\d|2[0-4]\d|25[0-5])\.(\d{1,2}|1\d\d|2[0-4]\d|25[0-5])$/;
		return ck.test(value);
	}, 
	i18n['common_ip']  
);
$.validator.addMethod("money",function(value,element){
		if(value==undefined || trim(value).length==0) return true; //为空不验证
		var ck = /^(([0-9]+\\.[0-9]*[1-9][0-9]*)|([0-9]*[1-9][0-9]*\\.[0-9]+)|([0-9]*[1-9][0-9]*))$/;
		return ck.test(value);
	}, 
    i18n['validate_price']
);
$.validator.addMethod("count",function(value,element){
		if(value==undefined || trim(value).length==0) return true; //为空不验证
		var ck = /^[0-9]*[1-9][0-9]*$/;
		return ck.test(value);
	}, 
	i18n['validate_integer'] 
);

/**验证两个日期比较*/
$.validator.addMethod("DateComparison",function(value,element,param){ 
    	var date1=$('#'+param[0]).val();
    	var date2=value;
    	if(date1==""){
    		return true;
    	}
    	return DateComparison(date1,date2);
    },
    i18n['error_dateComparison']
);

/**验证两个整数比较*/

$.validator.addMethod("NumberComparison",function(value,element,param){ 
    	var num1=$('#'+param[0]).val();
    	var num2=value;
    	if(num1==""){
    		return true;
    	}
    	return NumberComparison(num1,num2);
    },
    i18n['error_NumberComparison']
);

$.validator.addMethod("DateComparisonCI",function(value,element,param){ 
    	var date1=$('#'+param[0]).val();
    	var date2=value;
    	if(date1==""){
    		return true;
    	}
    	return DateComparison(date1,date2);
    },
    i18n['error_dateComparisonCI']
);

$.validator.addMethod("DateComparisonHoliday",function(value,element,param){ 
        var date1=$('#'+param[0]).val();
        var date2=value;
        if(date1==""){
            return true;
        }
        return DateComparison(date1,date2);
    },
    i18n['error_dateComparisonHoliday']
);

$.validator.addMethod("CHS",function(value,element){ 
		if(value==undefined || trim(value).length==0) return true; //为空不验证
        return /^[\u0391-\uFFE5]+$/.test(value);
    },
     i18n['validate_chinese_letters']
);

$.validator.addMethod("ZIP",function(value,element){ 
		if(value==undefined || trim(value).length==0) return true; //为空不验证
        return /^[1-9]\d{5}$/.test(value);
    },
    i18n['validate_ZIPCode']
);

$.validator.addMethod("QQ",function(value,element){
		if(value==undefined || trim(value).length==0) return true; //为空不验证
        return /^[1-9]\d{4,10}$/.test(value);
    },
    i18n['validate_qq']
);

$.validator.addMethod("loginName",function(value,element){ 
        return /^[a-zA-Z0-9][a-zA-Z0-9_/@.-]*$/.test(value);
    },
    i18n['error_loginName']
);

$.validator.addMethod("safepass",function(value,element){ 
        return safePassword(value);
    },
     i18n['validate_password']
);

$.validator.addMethod("chars",function(value,element){ 
		if(value==undefined || trim(value).length==0) return true; //为空不验证
        return /^[A-Za-z]+$/.test(value);
    },
    i18n['validate_chars']
);

$.validator.addMethod("persent",function(value,element){ 
		if(value==undefined || trim(value).length==0) return true; //为空不验证
        return value<=100;
    },
    i18n['validate_persent']
);

$.validator.addMethod("reDataTime",function(value,element){ 
        if(value!=null && value!=''){
        	if(strDateTime(value) || strDateTimeLeng(value))
        		return true;
	        else
	        	return false;
        }else{
        	return true;
        }
    },
    i18n['msg_date_format']
);
/**
 * 只能输入数字和字母
 */

$.validator.addMethod("numLetters",function(value,element){
	if(value==undefined || trim(value).length==0) return true; //为空不验证
    	 return /^[a-zA-Z0-9]*$/.test(value);
    },
    i18n['label_onlyNumALetters']
);
/**验证两个日期比较*/
$.validator.addMethod("DateComparisonAndDateReg",function(value,element,param){ 
    	var date1=$('#'+param[0]).val();
    	var date2=value;
    	
    	if(date1==""){
    		return true;
    	}
    	if(strDateTime(value) || strDateTimeLeng(value)){
    		if(DateComparison(date1,date2)){
    			return true;
    		}else{
    			return false;
    		}
    	}else{
    		return false;
    	}
    	//return DateComparison(date1,date2);
    },
    i18n['error_dateComparison']+i18n['msg_date_format']
);
/***验证是否大于当前系统时间**/
$.validator.addMethod("gtNowTimeReg",function(value,element){ 
		if(value==""){
    		return true;
    	}
		var myDate=new Date() ;
		var month=myDate.getMonth()+1;
    	return DateComparison(value,myDate.getFullYear()+'-'+month+'-'+myDate.getDate());
	},
	i18n['msg_gtNowTimeReg']
);
/**实际响应时间不能为空或计划开始时间不能早于实际响应时间*/
$.validator.addMethod("ResponseDateReg",function(value,element,param){ 
    	var date1=$('#'+param[0]).val();
    	var date2=value;
    	if(date1==""){
    		return false;
    	}
    	if(DateComparison(date1,date2)){
			return true;
		}else{
			return false;
		}
    	//return DateComparison(date1,date2);
    },
    i18n['error_responTimeNotNullandBigThanPlanStartTime!']
);
/*** 两时间大小比较*/
$.validator.addMethod("twoTimeEq",function(value,element,param){ 
		var m=$('#'+param[1]).val()*60+$('#'+param[2]).val()*1;
		var tm=$('#'+param[0]).val()*1;
		if(tm<m){
			return false
		}else{
			return true;
		}
	},
	i18n['error_processingTimeEarlierStartToEndTime']
);
/**名称不能使用特殊符号*/
$.validator.addMethod("nameSpecialSymbolVerification",function(value,element){ 
    	var pattern=RegExp(/[(\ )(\~)(\!)(\@)(\#)(\$)(\%)(\^)(\&)(\*)(\()(\))(\+)(\=)(\[)(\])(\{)(\})(\|)(\\)(\;)(\:)(\')(\")(\,)(\.)(\/)(\<)(\>)(\?)(\)]+/);
        return !pattern.test(value);
    },
     i18n['ERROR_SLA_RULENAME']
);
/**密码安全策略验证*/
$.validator.addMethod("passwordsecurity",function(value,element){ 
    	var ls = 0;
		if(value.match(/([a-z])+/)){  
		    ls++;  
		}  
		if(value.match(/([0-9])+/)){  
		    ls++;    
		}  
		if(value.match(/([A-Z])+/)){  
		    ls++;  
		}
		if(value.length>38){
			return true;
		}else if(value.length<8){
			return false;
		}else if(ls!=3){
			return false;
		}else{
			return true;
		}
    },
     i18n['validate_password']
);
$.validator.addMethod("nullValueValid",function(value,element){ 
		if(trim(value)=='null' || trim(value)=='NULL' || trim(value)=='' ){
			return false;
		}else{
			return true;
		}
    },
    i18n.validate_notNull
);
$.validator.addMethod("useEnCharPwd",function(value,element){
        var exp= /^[\u0021-\u007e]{1,}$/g;
        return exp.test(value);
    },
    i18n.error_pwdSpecial
);
$.validator.addMethod("radioAndchekboxValid",function(value,element,name){
		return $("#"+name[0]+" input[name*="+name[1]+"]:checked").length>0?true:false;
    },
    i18n.common_pleaseChoose
);


// 邮政编码验证   
jQuery.validator.addMethod("isZipCode", function(value, element) {   
    var tel = /^[0-9]{6}$/;
    return this.optional(element) || (tel.test(value));
}, "请正确填写您的邮政编码");


}));