$package('itsm.app.license')

/**
 * @author Mark Tao  
 * @constructor license管理函数
 * @description license管理函数
 * @date 2012-08-27
 * @since version 1.0 
 */

itsm.app.license.overdue_license=function(){
	var url;
	var param;
	return {
		/**
		 * 许可码写入
		 */
		rereadTxt:function(){
			if($('#over_license_code_form').form('validate')){
				//将许可码写入，进行判断是否可用
				url = "license!checkLicenseCode.action";
				param = 'licenseCoed='+$('#over_licenseCode').val();
				$.post(url,param,function(result){
					if(result=='true'){
						msgAlert(i18n['label_license_rightCode_restartService'],'info');
						$('#over_licenseCode').val('');
						$('#over_license_code').dialog('close');
					}else{
						msgAlert(i18n['label_license_errorCode'],'error');
					}
				});
			}
		},
		openUploadWindow:function(){
			windows('over_license_file',{width:350});
		},
		openLicenseLoadWin:function(){
			windows('over_license_code',{width:600});
		},
		/**
		 * 上传许可文件
		 */
		uploadLicense:function(){
			if($('#over_importLicenseFile').val()!=''){
				$.ajaxFileUpload({
		            url:'license!uploadLicense.action',
		            secureuri:true,
		            fileElementId:'over_importLicenseFile',    //文件输入框的ID名
		            dataType:'json',
		            success: function(data){
		             	if(data || data=="true"){
		             		$('#over_importLicenseFile').val('');
		             		$('#over_license_file').dialog('close');
							msgAlert(i18n['label_license_rightCode_restartService'],'info');
		             	}else{
		             		msgAlert(i18n['label_license_errorFile'],'error');
		             	}
		            }
		        });
			}else{
				msgAlert(i18n['label_license_nullFile'],'warning');
			}
		},
		init:function(){
			$('#over_licenseCode_input').button();
			$('#over_upload_license_window').button();
			$('#over_licenseCode_submit').button();
			$('#over_uploadFile_license_window_submit').button();
			$('#over_licenseCode_input').click(itsm.app.license.overdue_license.openLicenseLoadWin);
			$('#over_upload_license_window').click(itsm.app.license.overdue_license.openUploadWindow);
			$('#over_licenseCode_submit').click(itsm.app.license.overdue_license.rereadTxt);
			$('#over_uploadFile_license_window_submit').click(itsm.app.license.overdue_license.uploadLicense);
		}
	};
}();
$(function(){itsm.app.license.overdue_license.init()});
$(document).ajaxError(function(e, xhr, settings, exception) {
	var msg=xhr.responseText;
	if(msg!=null && msg.indexOf("ERROR_LICENSECODEEXCEPTION")!=-1){
		msgAlert(i18n['label_license_errorFile'],'info');
		return;
	}
});