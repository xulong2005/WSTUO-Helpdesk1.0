﻿ $package('itsm.cim.includes');
/**  
 * @fileOverview "Includes"
 * @author QXY
 * @version 1.0  
 */  
 /**  
 * @author QXY  
 * @constructor Includes
 * @description "用于加载Includes文件JS主函数"
 * @date 2012-7-31
 * @since version 1.0 
 */ 
itsm.cim.includes.includes=function(){
	
	/**
	 * 选择CI
	 */
	this._loadSelectCIIncludesFileFlag=false;
	
	return {
		
		/**
		 * 加载选择关联配置项窗口（多选）includes文件
		 */
		loadSelectCIIncludesFile:function(){
			if(!_loadSelectCIIncludesFileFlag){
				$('#selectCI_html').load('itsm/cim/selectCI.jsp',function(){
					$.parser.parse($('#selectCI_html'));
				});
			}
			_loadSelectCIIncludesFileFlag=true;
		},
		init:function(){
			
		}
	}
}();
$(function(){itsm.cim.includes.includes.init});
