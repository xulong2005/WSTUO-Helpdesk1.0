 /**  
 * @author Will  
 * @constructor 
 * @description 事件打印工单主函数
 * @date 2013-11-17
 * @since version 1.0 
 * @returns
 * @param select
 */
var requestPrint=function(){
	/**
	 * @description  打印工单显示
	 */
	this.preview=function(){
		bdhtml=window.document.body.innerHTML;
		sprnstr="<!--startprint-->";
		eprnstr="<!--endprint-->";
		prnhtml=bdhtml.substr(bdhtml.indexOf(sprnstr)+17);
		prnhtml=prnhtml.substring(0,prnhtml.indexOf(eprnstr));
		window.document.body.innerHTML=prnhtml;
		window.print();
	};
	return {
		/**
		 * @description 打印工单显示初始化
		 */
		init:function(){
			$.post('request!showRequestInfo.action','eno='+eno,function(data){
				$('#request_eventNo').html(data.requestCode);
				$('#request_requestorName').html(data.fullName);
				$('#request_requestorMode').html(data.createdByPhone);
				$('#request_eventTitle').html(data.etitle);
				$('#request_requestorUnit').html(data.companyName);
				$('#request_assigneeName').html(data.assigneeName);//受理人姓名
				$('#request_businessName').html(data.requestServiceDirName);
				$('#request_eventOccurrenceTime').html(data.createdOn);
				$('#request_eventSource').html(data.imodeName);////事件来源
				$('#request_sourceReleNo').html();//来源关联单号
				$('#request_faultClassification').html(data.requestCategoryName);
				$('#request_faultLevel').html(data.levelName);//11
				$('#request_desOfPhen').html(data.edesc);//描述
				$('#request_reportTime').html(timeFormatter(data.maxResponsesTime));
				$('#request_withAssets').html(data.ciName);
				$('#request_lineSupport').html();
				$('#request_secondTime').html();
				$('#request_changeOrderNo').html();
				$('#request_backlevel').html();
				$('#request_threeLineSupport').html();
				$('#request_processingResults').html(data.solutions);
				$('#request_feedback').html();
				$('#request_confirmTime').html();
				$('#request_requestCloseTime').html(timeFormatter(data.closeTime));//
				
				
				if(data.isConvertdToProblem){
					$('#request_questionWhether').html(i18n.label_basicConfig_deafultCurrencyYes);
					$.post('problem!findRelatedProblemPager.action','queryDTO.relatedEno='+eno,function(pdata){
						$.each(pdata.data,function(i,v){
							$('#request_problemsNumber').html($('#request_problemsNumber').html()+"&nbsp;"+v.problemNo);
						});
					});
				}
				else
					$('#request_questionWhether').html(i18n.label_basicConfig_deafultCurrencyNo);
				
				if(data.isConvertdToChange){
					$('#request_whetherChange').html(i18n.label_basicConfig_deafultCurrencyYes);
					$.post('change!findRelatedChangePager.action?queryDTO.relatedEno='+eno+'&queryDTO.relatedType=request',function(pdata){
						$.each(pdata.data,function(i,v){
							$('#request_changeOrderNo').html($('#request_changeOrderNo').html()+"&nbsp;"+v.changeNo);
						});
					});
				}
				else
					$('#request_whetherChange').html(i18n.label_basicConfig_deafultCurrencyNo);
			});
		}
	};
}();
$(document).ready(requestPrint.init);