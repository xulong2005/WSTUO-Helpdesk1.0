$package('itsm.request'); 
$import("basics.tab.tabUtils");
 /**  
 * @author QXY  
 * @constructor myRequestTask
 * @description 我的任务（请求）
 * @date 2010-02-26
 * @since version 1.0 
 */
itsm.request.myRequestTask = function() {
	return {
		/**
		 * @description 数据列表行格式化.
		 */
		titleUrlFrm:function(cellvalue, options, rowObject){
//			var executionId=rowObject.executionId;
//			if(executionId.indexOf('Changes')>=0 || executionId.indexOf('Change')>=0 || executionId.indexOf('change')>=0){
//				return '<a href="javascript:itsm.request.myRequestTask.changeDetails('+rowObject.variables.dto.eno+')">'+cellvalue+':'+rowObject.variables.dto.etitle+'</a>'
//			}	
//			else if(executionId.indexOf('request')>=0 || executionId.indexOf('Request')>=0){	
//				return '<a href="JavaScript:itsm.request.myRequestTask.requestDetails('+rowObject.variables.dto.eno+','+rowObject.id+')">'+cellvalue+":"+rowObject.variables.dto.etitle+'</a>'
//			}else{
//				return cellvalue;
//			}
			return '';
		},
		/**
		 * 转到变更详情页面
		 * @param changeId 变更eno
		 */
		changeDetails:function(changeId){
			basics.tab.tabUtils.reOpenTab('change!changeInfo.action?queryDTO.eno='+changeId,i18n.change_detail);
		},
		
		/**
		 * @description 转到请求详细页面.
		 * @param eno 编号eno
		 * @param taskId 任务Id
		 */
		requestDetails:function(eno,taskId){
			basics.tab.tabUtils.reOpenTab('request!requestDetails.action?eno='+eno+'&taskId='+taskId,i18n.request_detail);
		},
		
		/**
		 * @description 加载我的任务列表.
		 */
		myRequestTaskGrid:function(){
			var params=$.extend({},jqGridParams,{
//				caption:i18n['caption_request_taskGrid'],
				url:'jbpm!findPagePersonalTasks.action?assignee='+userName,
				colNames:['ID',i18n.common_name,i18n.title_request_requestTitle,i18n.common_desc,i18n.common_createTime],
				colModel:[
	  					  {name:'id',sortable:false},
						  {name:'name',width:150,formatter:this.titleUrlFrm,sortable:false},
						  {name:'variables',hidden:true,sortable:false},
						  {name:'description',index:'description',width:100,sortable:false},
						  {name:'createTime',index:'createTime',width:100,formatter:timeFormatter,sortable:false}
					  ],
				toolbar:false,
				multiselect:false,
				jsonReader: $.extend(jqGridJsonReader, {id: "id"}),
				sortname:'id',
				pager:'#MyRequestTaskPager'
			});
			$("#MyRequestTaskGrid").jqGrid(params);
			$("#MyRequestTaskGrid").navGrid('#MyRequestTaskPager',navGridParams);
			//自适应大小
			setGridWidth("#requestOwnerGrid","regCenter",20);
		},
		/**
		 * 初始化
		 * @private
		 */
		init: function(){
			itsm.request.myRequestTask.myRequestTaskGrid();
		}
	};
}();
$(document).ready(itsm.request.myRequestTask.init);