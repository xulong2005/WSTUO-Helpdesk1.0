$package("itsm.request");
/**  
 * @fileOverview 请求任务主函数
 * @author QXY
 * @version 1.0  
 */  
itsm.request.requestTask = function() {
	return {
		/**
		 * @description 保存任务
		 */
		saveEventTask:function(){
			var titleTask=$('#index_eventTaskDTO_taskTitle').val();
			$('#index_eventTaskDTO_taskTitle').val(titleTask.replace(/\s+/g,""));
			var et=new eventTask($('#requestDetails_requestNo').val(),'request');
			et.saveEventTask_opt();
		},
		
		/**
		 * @description 打开新增任务窗口
		 */
		addEventTask:function(){
			DateBox97('#index_eventTaskDTO_endTime,#index_eventTaskDTO_startTime');
			DateBox97('#index_eventTaskDTO_realEndTime,#index_eventTaskDTO_realStartTime');
			var et=new eventTask($('#requestDetails_requestNo').val(),'request');
			$('#index_addEditTask_save').unbind("click");
			$('#index_addEditTask_save').click(itsm.request.requestTask.saveEventTask);
			et.addEventTask_win();
			
		},
		
		/**
		 * @description 编辑事件任务
		 */
		editEventTask:function(){
			DateBox97('#index_eventTaskDTO_endTime,#index_eventTaskDTO_startTime');
			DateBox97('#index_eventTaskDTO_realEndTime,#index_eventTaskDTO_realStartTime');
			var et=new eventTask($('#requestDetails_requestNo').val(),'request');
			$('#index_addEditTask_save').unbind("click");
			$('#index_addEditTask_save').click(itsm.request.requestTask.saveEventTask);
			et.editEventTask_aff();
		},
		/**
		 * @description 编辑
		 * @param rowId 选中要编辑行ID
		 */
		editEventTask_affs:function(rowId){
			DateBox97('#index_eventTaskDTO_endTime,#index_eventTaskDTO_startTime');
			DateBox97('#index_eventTaskDTO_realEndTime,#index_eventTaskDTO_realStartTime');
			var et=new eventTask($('#requestDetails_requestNo').val(),'request');
			$('#index_addEditTask_save').unbind("click").click(itsm.request.requestTask.saveEventTask);
			et.editEventTask_affs(rowId,et);
		},
		/**
		 * @description 删除任务
		 */
		deleteEventTask:function(){
			var et=new eventTask($('#requestDetails_requestNo').val(),'request');
			et.deleteEventTask_aff();
		},
		/**
		 * @description 删除任务提示
		 * @param rowId 要删除的行ID
		 */
		deleteEventTask_aff:function(rowId){
			var et=new eventTask($('#requestDetails_requestNo').val(),'request');
			et.deleteEventTask_affs(rowId,et);
		}
	};
}();