/**  
 * @author QXY  
 * @constructor tan
 * @description 表格常用操作方法
 * @date 2013-08-01
 * @since version 1.0 
 */

$package('basics');

basics.tableOpt = function(){
	
	return {
		//规则集删除
		trDel:function(trId){
			$(trId).parent().parent().remove();
		},
	
		//动作集删除
		trDelRule:function(trId){

			$(trId).remove();
		},
		
		//上移
		trUp:function(trId){
			var t = $(trId);
			var i=$(t).index();//当前行的id
			if(i+1>1){//不是表头，也不是第一行，则可以上移
				 t.fadeOut().fadeIn(); 
		         t.prev().before(t); 
				var tem0=$(t).parent().parent().html();
				var tem1=$(t).parent().parent().prev().html();
				$(t).parent().parent().prev().html(tem0);
				$(t).parent().parent().html(tem1);
			}
		},
		//置顶
		trTop:function(trId){
			var t = $(trId);
	        t.fadeOut().fadeIn(); 
	        $(t).parent().prepend(t); 
		},
		//下移
		trDown:function(tableId,trId){
			
			var t = $(trId);
			var l=$(tableId+" tbody tr").length;//总行数
			var i=$(t).index();//当前行的id
			if(i<l-1){//不是最后一行，则可以下移
				t.fadeOut().fadeIn(); 
	            t.next().after(t);
				//var tem0=$(t).parent().eq(i).html();
				//alert(tem0+"1111");
				//var tem1=$(t).next().html();
				//alert(tem1+"222");
				//$(t).parent().parent().next().html(tem0);
				//$(t).parent().parent().html(tem1);
			}
		},
		//添加
		trAdd:function(trId){
			var t = $(trId);
			var tem0=$(t).parent().parent().html();
			$(t).parent().parent().append("<tr>"+tem0+"</tr>");
		},
		init:function(){
			
		}
	};
	
}();