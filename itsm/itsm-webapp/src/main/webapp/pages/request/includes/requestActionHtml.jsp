<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<%@ include file="../../language.jsp" %>        
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>请求includes文件</title>
</head>
<body>


<!-- 开始请求处理-->
<div id="requestDeal_win" class="WSTUO-dialog" title="<fmt:message key="label.sla.updateBase.response" />" style="width:480px;height:auto">
	<form id="startRequestDeal_form" event="itsm.request.requestAction.requestDeal_opt">
		<div class="lineTableBgDiv">
			<table style="width:100%" class="lineTable" cellspacing="1">
			<tr>
				<td><fmt:message key="common.remark" />&nbsp;</td>
				<td>
					<textarea rows="8" cols="45" name="historyRecordDto.logDetails" class="form-control"  minlength='1' maxlength='200' id="requestDeal_value"></textarea>
				</td>
			</tr>
		<%-- 	<tr>
				<td><fmt:message key="label.is.request.action.notice" /></td>
				<td>
				<div id="requestDeal_noticeEmail_div" style="display: none;">
					<input style="width: 98%;" id="requestDeal_noticeEmail_input" ondblclick="itsm.request.requestDetail.selectNoticeUser('#requestDeal_noticeEmail_input','email')"/>
					<br><span><fmt:message key="label.email.address.format" /></span>
				</div>
				<input type="checkbox" value="true" id="startRequestDeal_isEmailNotice" onclick="itsm.request.requestDetail.requestActionNoticeSet('#startRequestDeal_isEmailNotice','#requestDeal_noticeEmail_div')" />
					
				</td>
			</tr> --%>
			<tr>
				<td colspan="2">
					<input type="hidden" name="historyRecordDto.logTitle" id="startRequestDeal_action" value="<fmt:message key="label.request.startRequestDeal" />" />
					<input type="hidden" name="requestDTO.optType" value="requestDeal" />
					<button class="btn btn-primary btn-sm"><fmt:message key="label.request.startProcess" /></button>
				</td>
			</tr>
			</table>
		</div>
	</form>
</div>


<!--请求提取-->
<div id="requestGet_win" class="WSTUO-dialog" title="<fmt:message key="label.request.requestExtraction" />" style="width:480px;height:auto">
	<form event="itsm.request.requestAction.requestGet_opt">
	<div class="lineTableBgDiv">
		<table style="width:100%" cellspacing="1">
		<tr>
			<td style="width: 15%;"><fmt:message key="common.remark" /></td>
			<td>
				<textarea class="form-control" style="width: 96%;" rows="8" cols="45" name="historyRecordDto.logDetails" id="requestGet_value" class="form-control"  minlength='1' maxlength='200'></textarea>
			</td>
		</tr>
		<%-- <tr>
			<td><fmt:message key="label.is.request.action.notice" /></td>
			<td>
			<div id="requestGet_noticeEmail_div" style="display: none;">
					<input style="width: 98%;" id="requestGet_noticeEmail_input" ondblclick="itsm.request.requestDetail.selectNoticeUser('#requestGet_noticeEmail_input','email')"/>
					<br><span><fmt:message key="label.email.address.format" /></span>
			</div>
			<input type="checkbox" value="true" id="requestGet_isEmailNotice" onclick="itsm.request.requestDetail.requestActionNoticeSet('#requestGet_isEmailNotice','#requestGet_noticeEmail_div')" />
			
			</td>
		</tr> --%>
		<tr>
			<td colspan="2">
				<input type="hidden" name="historyRecordDto.logTitle" id="requestExtraction_action" value="<fmt:message key="label.request.requestExtraction" />" />
				<input type="hidden" name="requestDTO.optType" value="requestGet" />
				<button class="btn btn-primary btn-sm" style="margin-right: 15px;margin-top: 10px;"><fmt:message key="label.request.extract" /></button>
				<br/>
			</td>
		</tr>
	</table>
	</div>
	</form>
</div>

<!--请求重新开启-->
<div id="requestReOpen_win" class="WSTUO-dialog"  shadow="false" closed="true" modal="true"  title="<fmt:message key="title.request.reStartRequest" />" minimizable="false" maximizable="false" collapsible="false" style="width:480px;height:auto">
	<form event="itsm.request.requestAction.requestReOpen_opt">

		<table style="width:100%" cellspacing="1">
		<tr>
			<td><fmt:message key="common.remark" /></td>
			<td>
				<textarea name="historyRecordDto.logDetails" id="requestReOpen_value" class="form-control" required="true" style="height:80px"></textarea>
			</td>
		</tr>
		<%-- <tr>
			<td><fmt:message key="label.is.request.action.notice" /></td>
			<td>
			<div id="requestReOpen_noticeEmail_div" style="display: none;">
					<input style="width: 98%;" id="requestReOpen_noticeEmail_input" ondblclick="itsm.request.requestDetail.selectNoticeUser('#requestReOpen_noticeEmail_input','email')"/>
					<br><span><fmt:message key="label.email.address.format" /></span>
			</div>
			<input type="checkbox" value="true" id="requestReOpen_isEmailNotice" onclick="itsm.request.requestDetail.requestActionNoticeSet('#requestReOpen_isEmailNotice','#requestReOpen_noticeEmail_div')" />
			
			</td>
		</tr> --%>
		<tr>
			<td colspan="2">
				<input type="hidden" name="historyRecordDto.logTitle"  id="reStartRequest_action" value="<fmt:message key="title.request.reStartRequest" />" />
				<input type="hidden" name="requestDTO.optType" value="requestReOpen" />
				<input type="submit" class="btn btn-primary btn-sm" value="<fmt:message key="label.request.open" />"/>
			</td>
		</tr>
	</table>
	</form>
</div>

<!--请求退回组-->
<div id="requestBackGroup_win" class="WSTUO-dialog" title="<fmt:message key="label.request.requestBackToGroup" />" style="width:480px;height:auto">
	<form>

	<div class="lineTableBgDiv">
		<table style="width:100%" class="lineTable" cellspacing="1">
		<tr>
			<td><fmt:message key="common.remark" /></td>
			<td>
				<textarea class="form-control" required="true" style="height:80px" name="historyRecordDto.logDetails" id="requestBackGroup_value"></textarea>
			</td>
		</tr>
		<%-- <tr>
			<td><fmt:message key="label.is.request.action.notice" /></td>
			<td>
			<div id="requestBackGroup_noticeEmail_div" style="display: none;">
					<input style="width: 98%;" id="requestBackGroup_noticeEmail_input" ondblclick="itsm.request.requestDetail.selectNoticeUser('#requestBackGroup_noticeEmail_input','email')"/>
					<br><span><fmt:message key="label.email.address.format" /></span>
			</div>
			<input type="checkbox" value="true" id="requestBackGroup_isEmailNotice" onclick="itsm.request.requestDetail.requestActionNoticeSet('#requestBackGroup_isEmailNotice','#requestBackGroup_noticeEmail_div')" />
			
			</td>
		</tr> --%>
		<tr>
			<td colspan="2">
				<input type="hidden" name="historyRecordDto.logTitle" id="requestBackToGroup_action" value="<fmt:message key="label.request.requestBackToGroup" />" />
				<input type="hidden" name="requestDTO.optType" value="requestBackGroup" />
				<a class="btn btn-primary btn-sm" onclick="itsm.request.requestAction.requestBackGroup_opt()"><fmt:message key="label.request.back" /></a>
			</td>
		</tr>
	</table>
	</div>
	</form>
</div>

<!-- 请求指派 -->
<div id="requestAssign_win" class="WSTUO-dialog" title="<fmt:message key="label.request.requestAssign" />" style="width:520px;height:auto">
	<form event="itsm.request.requestAction.requestAssign_opt_s">

		<table style="width:100%" cellspacing="1">
		<tr>
			<td style="width: 25%"><fmt:message key="label.request.assignToGroup" />&nbsp;<span style="color:red">*</span></td>
			<td>
			<input type="hidden" name="requestDTO.assigneeGroupNo" id="requestDetail_assigneeGroupNo" />
			<input class="form-control" name="assigneeGroupName" id="requestDetail_assigneeGroupName" class="form-control" style="width: 90%;float: left;"
			onclick="itsm.request.requestDetail.selectAssginGroup('#requestDetail_assigneeGroupNo','#requestDetail_assigneeGroupName')" readonly="readonly" />
			<h5 style="float: left;padding-left: 8px;"><a onclick="cleanIdValue('requestDetail_assigneeGroupNo','requestDetail_assigneeGroupName')" title="<fmt:message key="label.request.clear" />">
			<i class=" glyphicon glyphicon-trash"></i></a></h5>
			</td>
		</tr>
		<sec:authorize url="/pages/user!find.action">
		<tr>
			<td style="width: 25%"><fmt:message key="title.change.assigneeTechnician" /> &nbsp;<span style="color:red">*</span></td>
			<td>
			<input type="hidden" name="requestDTO.assigneeNo" id="requestDetail_assigneeNo" />
			<input class="form-control" name="assigneeName" id="requestDetail_assigneeName"  readonly="readonly" class="form-control" style="width: 90%;float: left;margin-top: 10px;"
			onclick="itsm.request.requestDetail.selectRequestUser_byOrgNo('#requestDetail_assigneeName','#requestDetail_assigneeNo')"/>
			<h5 style="float: left;padding-left: 8px;text-align: center;"><a onclick="cleanIdValue('requestDetail_assigneeName','requestDetail_assigneeNo','requestDetail_assigneeGroupNo')" title="<fmt:message key="label.request.clear" />">
			<i class=" glyphicon glyphicon-trash"></i></a></h5>
			</td>
		</tr>
		</sec:authorize>
		<tr>
			<td style="width: 25%"><fmt:message key="common.remark" /></td>
			<td>
				<textarea class="form-control" style="width:90%;height:80px;margin-top: 15px;" name="assignRemark" id="assignRemark"></textarea>
			</td>
		</tr>
		<%-- <tr>
			<td><fmt:message key="label.is.request.action.notice" /></td>
			<td>
			<div id="requestAssign_noticeEmail_div" style="display: none;">
					<input style="width: 98%;" id="requestAssign_noticeEmail_input" ondblclick="itsm.request.requestDetail.selectNoticeUser('#requestAssign_noticeEmail_input','email')"/>
					<br><span><fmt:message key="label.email.address.format" /></span>
			</div>
			<input type="checkbox" value="true" id="requestAssign_isEmailNotice" onclick="itsm.request.requestDetail.requestActionNoticeSet('#requestAssign_isEmailNotice','#requestAssign_noticeEmail_div')" />
			
			</td>
		</tr> --%>
		<tr>
			<td colspan="2">
				<input type="hidden" name="historyRecordDto.logTitle" id="requestAssign_action" value="<fmt:message key="label.request.requestAssign" />" />
				<input type="hidden" name="requestDTO.optType" value="requestAssgin" />
				<button class="btn btn-primary btn-sm" style="margin-right: 20px;margin-top: 10px;"><fmt:message key="title.request.assign" /></button>
				<br/>
			</td>
		</tr>
	</table>
	</form>
</div>


<!-- 请求指派二线 -->
<div id="requestAssignTwo_win" class="WSTUO-dialog" title="<fmt:message key="label.request.assignedToSecondlineTechnician" />" style="width:480px;height:auto">
	<form>
	
	<div class="lineTableBgDiv">
		<table style="width:100%" class="lineTable" cellspacing="1">
		<tr>
			<td><fmt:message key="label.request.group" />&nbsp;<span style="color:red">*</span></td>
			<td>
			<input type="hidden" name="requestDTO.assigneeGroupNo" id="requestDetail_assigneeGroupNo_two"/>
			<input class="form-control"  name="assigneeGroupName" id="requestDetail_assigneeGroupName_two" onclick="itsm.request.requestDetail.selectAssginGroup('#requestDetail_assigneeGroupNo_two','#requestDetail_assigneeGroupName_two')" readonly="readonly" />
			&nbsp;<a onclick="cleanIdValue('requestDetail_assigneeGroupNo_two','requestDetail_assigneeGroupName_two')" title="<fmt:message key="label.request.clear" />"><i class="glyphicon glyphicon-remove-circle form-control-feedback choose"></i></a>
			</td>
		</tr>
		<tr>
			<td><fmt:message key="label.request.technician" />&nbsp;<span style="color:red">*</span></td>
			<td>
			<input type="hidden" name="requestDTO.assigneeNo" id="requestDetail_assigneeNo_two" />
			<input class="form-control" name="assigneeName" id="requestDetail_assigneeName_two"  readonly="readonly" onclick="wstuo.user.userUtil.selectUserByRole('ROLE_SECONDLINEENGINEER','#requestDetail_assigneeName_two','#requestDetail_assigneeNo_two')" />
			&nbsp;<a onclick="cleanIdValue('requestDetail_assigneeName_two','requestDetail_assigneeNo_two')" title="<fmt:message key="label.request.clear" />"><i class="glyphicon glyphicon-remove-circle form-control-feedback choose"></i></a>
			</td>
		</tr>
		<tr>
			<td><fmt:message key="common.remark" /></td>
			<td>
				<textarea rows="8" cols="45" class="form-control" name="assignRemark" id="assignRemark_two"></textarea>
			</td>
		</tr>
<%-- 		<tr>
			<td><fmt:message key="label.is.request.action.notice" /></td>
			<td>
			<div id="requestAssignTwo_noticeEmail_div" style="display: none;">
					<input style="width: 98%;" id="requestAssignTwo_win_noticeEmail_input" ondblclick="itsm.request.requestDetail.selectNoticeUser('#requestAssignTwo_win_noticeEmail_input','email')"/>
					<br><span><fmt:message key="label.email.address.format" /></span>
			</div>
			<input type="checkbox" value="true" id="requestAssignTwo_win_isEmailNotice" onclick="itsm.request.requestDetail.requestActionNoticeSet('#requestAssignTwo_win_isEmailNotice','#requestAssignTwo_noticeEmail_div')" />
			
			</td>
		</tr> --%>
		<tr>
			<td colspan="2">
				<input type="hidden" name="historyRecordDto.logTitle" id="requestAssignTwo_win_action" value="<fmt:message key="label.request.assignedToSecondlineTechnician" />" />
				<input type="hidden" name="requestDTO.optType" value="requestAssginTwo" />
				<a class="btn btn-primary btn-sm" onclick="itsm.request.requestAction.requestAssign_opt('two','requestAssignTwo_win')"><fmt:message key="title.request.assign" /></a>
			</td>
		</tr>
	</table>
	</div>
	</form>
</div>


<!-- 请求指派三线 -->
<div id="requestAssignThree_win" class="WSTUO-dialog" title="<fmt:message key="label.request.assignedToThirdlineTechnician" />" style="width:480px;height:auto">
	<form>

	<div class="lineTableBgDiv">
		<table style="width:100%" class="lineTable" cellspacing="1">
		<tr>
			<td><fmt:message key="label.request.group" />&nbsp;<span style="color:red">*</span></td>
			<td>
			<input type="hidden" name="requestDTO.assigneeGroupNo" id="requestDetail_assigneeGroupNo_three" />
			<input style="width:80%" name="assigneeGroupName" id="requestDetail_assigneeGroupName_three" onclick="itsm.request.requestDetail.selectAssginGroup('#requestDetail_assigneeGroupNo_three','#requestDetail_assigneeGroupName_three')" readonly="readonly" />
			&nbsp;<a onclick="cleanIdValue('requestDetail_assigneeGroupNo_three','requestDetail_assigneeGroupName_three')" title="<fmt:message key="label.request.clear" />"><i class="glyphicon glyphicon-remove-circle form-control-feedback choose"></i></a>
			</td>
		</tr>
		<tr>
			<td><fmt:message key="label.request.technician" />&nbsp;<span style="color:red">*</span></td>
			<td>
			<input type="hidden" name="requestDTO.assigneeNo" id="requestDetail_assigneeNo_three" />
			<input style="width:80%" name="assigneeName" id="requestDetail_assigneeName_three"  readonly="readonly" onclick="wstuo.user.userUtil.selectUserByRole('ROLE_THIRDLINEENGINEER','#requestDetail_assigneeName_three','#requestDetail_assigneeNo_three')" />
			&nbsp;<a onclick="cleanIdValue('requestDetail_assigneeName_three','requestDetail_assigneeNo_three')" title="<fmt:message key="label.request.clear" />"><i class="glyphicon glyphicon-remove-circle form-control-feedback choose"></i></a>
			</td>
		</tr>
		<tr>
			<td><fmt:message key="common.remark" /></td>
			<td>
				<textarea rows="8" cols="45" name="assignRemark" id="assignRemark_three"></textarea>
			</td>
		</tr>
		<%-- <tr>
			<td><fmt:message key="label.is.request.action.notice" /></td>
			<td>
			<div id="requestAssignThree_noticeEmail_div" style="display: none;">
					<input style="width: 98%;" id="requestAssignThree_win_noticeEmail_input" ondblclick="itsm.request.requestDetail.selectNoticeUser('#requestAssignThree_win_noticeEmail_input','email')"/>
					<br><span><fmt:message key="label.email.address.format" /></span>
			</div>
			<input type="checkbox" value="true" id="requestAssignThree_win_isEmailNotice" onclick="itsm.request.requestDetail.requestActionNoticeSet('#requestAssignThree_win_isEmailNotice','#requestAssignThree_noticeEmail_div')" />
			
			</td>
		</tr> --%>
		<tr>
			<td colspan="2">
				<input type="hidden" name="historyRecordDto.logTitle" id="requestAssignThree_win_action" value="<fmt:message key="label.request.assignedToThirdlineTechnician" />" />
				<input type="hidden" name="requestDTO.optType" value="requestAssginThree" />
				<a class="btn btn-primary btn-sm" onclick="itsm.request.requestAction.requestAssign_opt('three','requestAssignThree_win')"><fmt:message key="title.request.assign" /></a>
			</td>
		</tr>
	</table>
	</div>
	</form>
</div>



<!-- 请求指派四线 -->
<div id="requestAssignFour_win" class="WSTUO-dialog" title="<fmt:message key="label.request.assignedFourthlineEngineer" />" style="width:480px;height:auto">
	<form>

	<div class="lineTableBgDiv">
		<table style="width:100%" class="lineTable" cellspacing="1">
		<tr>
			<td><fmt:message key="label.request.group" />&nbsp;<span style="color:red">*</span></td>
			<td>
			<input type="hidden" name="requestDTO.assigneeGroupNo" id="requestDetail_assigneeGroupNo_four" />
			<input style="width:80%" name="assigneeGroupName" id="requestDetail_assigneeGroupName_four" onclick="itsm.request.requestDetail.selectAssginGroup('#requestDetail_assigneeGroupNo_four','#requestDetail_assigneeGroupName_four')" readonly="readonly" />
			&nbsp;<a  onclick="cleanIdValue('requestDetail_assigneeGroupNo_four','requestDetail_assigneeGroupName_four')" title="<fmt:message key="label.request.clear" />"><i class="glyphicon glyphicon-remove-circle form-control-feedback choose"></i></a>
			</td>
		</tr>
		<tr>
			<td><fmt:message key="label.request.technician" />&nbsp;<span style="color:red">*</span></td>
			<td>
			<input type="hidden" name="requestDTO.assigneeNo" id="requestDetail_assigneeNo_four"  />
			<input  style="width:80%" name="assigneeName" id="requestDetail_assigneeName_four"  readonly="readonly" onclick="wstuo.user.userUtil.selectUserByRole('ROLE_FOURTHLINEENGINEER','#requestDetail_assigneeName_four','#requestDetail_assigneeNo_four')"/>
			&nbsp;<a onclick="cleanIdValue('requestDetail_assigneeName_four','requestDetail_assigneeNo_four')" title="<fmt:message key="label.request.clear" />"><i class="glyphicon glyphicon-remove-circle form-control-feedback choose"></i></a>
			</td>
		</tr>
		<tr>
			<td><fmt:message key="common.remark" /></td>
			<td>
				<textarea rows="8" cols="45" name="assignRemark" id="assignRemark_four"></textarea>
			</td>
		</tr>
		<%-- <tr>
			<td><fmt:message key="label.is.request.action.notice" /></td>
			<td>
			<div id="requestAssignFour_noticeEmail_div" style="display: none;">
					<input style="width: 98%;"  id="requestAssignFour_win_noticeEmail_input" ondblclick="itsm.request.requestDetail.selectNoticeUser('#requestAssignFour_win_noticeEmail_input','email')"/>
					<br><span><fmt:message key="label.email.address.format" /></span>
			</div>
			<input type="checkbox" value="true" id="requestAssignFour_win_isEmailNotice" onclick="itsm.request.requestDetail.requestActionNoticeSet('#requestAssignFour_win_isEmailNotice','#requestAssignFour_noticeEmail_div')" />
			
			</td>
		</tr> --%>
		<tr>
			<td colspan="2">
				<input type="hidden" name="historyRecordDto.logTitle" id="requestAssignFour_win_action" value="<fmt:message key="label.request.assignedFourthlineEngineer" />" />
				<input type="hidden" name="requestDTO.optType" value="requestAssginFour" />
				<a class="btn btn-primary btn-sm" onclick="itsm.request.requestAction.requestAssign_opt('four','requestAssignFour_win')"><fmt:message key="title.request.assign" /></a>
			</td>
		</tr>
	</table>
	</div>
	</form>
</div>





<!-- 请求再指派 -->
<div id="requestAgainAssign_win" class="WSTUO-dialog" title="<fmt:message key="label.request.requestAssignAgain" /> " style="width:520px;height:auto">
	<form event="itsm.request.requestAction.requestAgainAssign_opt">
		<table style="width:100%" cellspacing="1">
		<tr>
			<td><fmt:message key="label.request.technician" />&nbsp;<span style="color:red">*</span></td>
			<td>
			<input type="hidden" name="requestDTO.assigneeNo" id="requestDetail_assigneeNo_Again"  />
			<input name="requestDTO.assigneeName" id="requestDetail_assigneeName_Again" required="true" class="form-control" style="width: 90%;float: left;"  readonly="readonly" 
			onclick="wstuo.user.userUtil.againAssignSelectUser_group('#requestDetail_assigneeName_Again','#requestDetail_assigneeNo_Again','#requestDetails_assigneeLoginName','#requestDetails_assigneeGroupNo')" />
			<h4 style="float: left;padding-left: 8px;"><a onclick="cleanIdValue('requestDetail_assigneeName_Again','requestDetail_assigneeNo_Again')" title="<fmt:message key="label.request.clear" />">
			<i class=" glyphicon glyphicon-trash"></i></a></h4>
			</td>
		</tr>
		<tr>
			<td><fmt:message key="common.remark" /></td>
			<td>
				<textarea rows="8" style="width: 90%;padding-top: 10px;"  name="assignRemark" id="assignRemark_Again" class="form-control" ></textarea>
			</td>
		</tr>
		<%-- <tr>
			<td><fmt:message key="label.is.request.action.notice" /></td>
			<td>
			<div id="requestAgainAssign_noticeEmail_div" style="display: none;">
					<input style="width: 98%;" id="requestAgainAssign_noticeEmail_input" ondblclick="itsm.request.requestDetail.selectNoticeUser('#requestAgainAssign_noticeEmail_input','email')"/>
					<br><span><fmt:message key="label.email.address.format" /></span>
			</div>
			<input type="checkbox" value="true" id="requestAgainAssign_isEmailNotice" onclick="itsm.request.requestDetail.requestActionNoticeSet('#requestAgainAssign_isEmailNotice','#requestAgainAssign_noticeEmail_div')" />
			
			</td>
		</tr> --%>
		<tr>
			<td colspan="2">
				<input type="hidden" name="historyRecordDto.logTitle" id="requestAssignAgain_action" value="<fmt:message key="label.request.requestAssignAgain" />" />
				<input type="hidden" name="requestDTO.optType" value="requestAgainAssgin" />
				<button class="btn btn-primary btn-sm" style="margin-top: 10px;margin-right: 20px;"><fmt:message key="title.request.assign" /></button>
			</td>
		</tr>
	</table>
	</form>
</div>




<!-- 请求退回-->
<div id="requestBack_win" class="WSTUO-dialog" title="<fmt:message key="label.request.backToFirstLine" />" style="width:480px;height:auto">
	<form id="requestBackForm">
		<div class="lineTableBgDiv">
		<table style="width:100%" class="lineTable" cellspacing="1">
		<tr>
			<td><fmt:message key="common.remark" /></td>
			<td>
				<textarea rows="8" cols="45" name="historyRecordDto.logDetails" required="true" id="requestBackReason_value" class="form-control"  minlength='1' maxlength='200'></textarea>
			</td>
		</tr>
		<%-- <tr>
			<td><fmt:message key="label.is.request.action.notice" /></td>
			<td>
			<div id="requestBack_noticeEmail_div" style="display: none;">
					<input style="width: 98%;" id="requestBack_noticeEmail_input" ondblclick="itsm.request.requestDetail.selectNoticeUser('#requestBack_noticeEmail_input','email')"/>
					<br><span><fmt:message key="label.email.address.format" /></span>
			</div>
			<input type="checkbox" value="true" id="requestBack_isEmailNotice" onclick="itsm.request.requestDetail.requestActionNoticeSet('#requestBack_isEmailNotice','#requestBack_noticeEmail_div')" />
			
			</td>
		</tr> --%>
		<tr>
			<td colspan="2">
				<input type="hidden" name="historyRecordDto.logTitle" id="backToFirstLine_action" value="<fmt:message key="label.request.backToFirstLine" />" />
				<input type="hidden" name="requestDTO.optType" value="requestBack" />
				<input type="button" class="btn btn-primary btn-sm" onclick="itsm.request.requestAction.requestBack_opt()" value="<fmt:message key="label.request.back" />"/>
			</td>
		</tr>
		
		
	</table>
	</div>
	</form>
</div>

<!-- 请求升级申请-->
<div id="requestUpgradeApply_win" class="WSTUO-dialog" title="<fmt:message key="label.request.upgradeApplication" />" style="width:480px;height:auto">
	<form  event="itsm.request.requestAction.requestUpgradeApply_opt">
	
	<div class="lineTableBgDiv">
		<table style="width:100%" class="lineTable" cellspacing="1">
		<tr>
			<td><fmt:message key="common.remark" /></span></td>
			<td>
				<textarea style="width: 90%" rows="6" cols="45" name="historyRecordDto.logDetails" id="requestUpgradeApplyReason_value" class="form-control" minlength='1' maxlength='200'></textarea>
			</td>
		</tr>
		<%-- <tr>
			<td><fmt:message key="label.is.request.action.notice" /></td>
			<td>
			<div id="requestUpgradeApply_noticeEmail_div" style="display: none;">
					<input style="width: 98%;"  id="requestUpgradeApply_noticeEmail_input" ondblclick="itsm.request.requestDetail.selectNoticeUser('#requestUpgradeApply_noticeEmail_input','email')"/>
					<br><span><fmt:message key="label.email.address.format" /></span>
			</div>
			<input type="checkbox" value="true" id="requestUpgradeApply_isEmailNotice" onclick="itsm.request.requestDetail.requestActionNoticeSet('#requestUpgradeApply_isEmailNotice','#requestUpgradeApply_noticeEmail_div')" />
			
			</td>
		</tr> --%>
		<tr>
			<td colspan="2">
				<input type="hidden" name="historyRecordDto.logTitle" id="upgradeApplication_action" value="<fmt:message key="label.request.upgradeApplication" />" />
				<input type="hidden" name="requestDTO.optType" value="requestUpgradeApply" />
				<button class="btn btn-primary btn-sm"><fmt:message key="label.request.apply" /></button>
			</td>
		</tr>
	</table>
	</div>
	</form>
</div>


<!-- 请求升级-->
<div id="requestUpgrade_win" class="WSTUO-dialog" title="<fmt:message key="label.request.requestUpdate" />" style="width:480px;height:auto">
	<form event="itsm.request.requestAction.requestUpgrade_opt">
	
	<div class="lineTableBgDiv">
		<table style="width:100%" class="lineTable" cellspacing="1">
		<tr>
			<td width="30%"><fmt:message key="label.request.requestUpdateTo" />&nbsp;<span style="color:red">*</span></td>
			<td>
				<select style="width: 98%" name="requestDTO.ownerNo" id="requestUpgrade_level" ></select>
			</td>
		</tr>
		<tr>
			<td><fmt:message key="common.remark" /></td>
			<td>
				<textarea rows="8" style="width:96%" id="requestUpgradeReason" class="form-control"  minlength='1' maxlength='200'></textarea>
			</td>
		</tr>
		<%-- <tr>
			<td><fmt:message key="label.is.request.action.notice" /></td>
			<td>
			<div id="requestUpgrade_noticeEmail_div" style="display: none;">
					<input style="width: 98%;" id="requestUpgrade_noticeEmail_input" ondblclick="itsm.request.requestDetail.selectNoticeUser('#requestUpgrade_noticeEmail_input','email')"/>
					<br><span><fmt:message key="label.email.address.format" /></span>
			</div>
			<input type="checkbox" value="true" id="requestUpgrade_isEmailNotice" onclick="itsm.request.requestDetail.requestActionNoticeSet('#requestUpgrade_isEmailNotice','#requestUpgrade_noticeEmail_div')" />
			
			</td>
		</tr> --%>
		<tr>
			<td colspan="2">
				<input type="hidden" name="historyRecordDto.logTitle" id="requestUpdate_action" value="<fmt:message key="label.request.requestUpdate" />" />
				<input type="hidden" name="requestDTO.optType" value="requestUpgrade" />
				<button class="btn btn-primary btn-sm" ><fmt:message key="label.request.update" /></button>
			</td>
		</tr>
	</table>
	</div>
	</form>
</div>

<!-- 请求关闭-->
<div id="requestClose_win" class="WSTUO-dialog" title="<fmt:message key="label.request.requestClose" />" style="width:480px;height:auto">
	<form>
	
	<div class="lineTableBgDiv">
		<table style="width:100%" class="lineTable" cellspacing="1">


		<tr>
			<td><fmt:message key="common.remark" /></td>
			<td>
				<textarea rows="8" cols="45" name="historyRecordDto.logDetails" id="requestClose_value" class="form-control"  minlength='1' maxlength='200' required="true"></textarea>
			</td>
		</tr>
		<%-- <tr>
			<td><fmt:message key="label.is.request.action.notice" /></td>
			<td>
			<div id="requestClose_noticeEmail_div" style="display: none;">
					<input style="width: 98%;" id="requestClose_noticeEmail_input" ondblclick="itsm.request.requestDetail.selectNoticeUser('#requestClose_noticeEmail_input','email')"/>
					<br><span><fmt:message key="label.email.address.format" /></span>
			</div>
			<input type="checkbox" value="true" id="requestClose_isEmailNotice" onclick="itsm.request.requestDetail.requestActionNoticeSet('#requestClose_isEmailNotice','#requestClose_noticeEmail_div')" />
			
			</td>
		</tr> --%>
		<tr>
			<td colspan="2">
				<input name="requestDTO.closeTime" id="requestCloseTime" type="hidden" />
				<input type="hidden" name="historyRecordDto.logTitle" id="requestClose_action" value="<fmt:message key="label.request.requestClose" />" />
				<input type="hidden" name="requestDTO.optType" value="requestClose" />
				<a class="btn btn-primary btn-sm" onclick="itsm.request.requestAction.requestClose_opt()"><fmt:message key="label.request.close" /></a>
			</td>
		</tr>
	</table>
	</div>
	</form>
</div>

<!-- 请求处理备注-->
<div id="requestDealRemark_win" class="WSTUO-dialog" title="<fmt:message key="label.request.requestProcessMrak" />" style="width:480px;height:auto">
	<form>
		<div class="lineTableBgDiv">
		<table style="width:100%" cellspacing="1">
		<tr style="display:none;">
			<td style="width: 15%;"><fmt:message key="label.request.requestState" /></td>
			<td>
				<select style="width:96%" name="requestDTO.remarkStatusNo" id="requestDealRemark_statusNo"></select>
			</td>
		</tr>
		<tr>
			<td style="width: 15%;"><fmt:message key="common.remark" />&nbsp;<span style="color:red">*</span></td>
			<td>
				<textarea rows="8" cols="45" name="historyRecordDto.logDetails" id="requestDealRemark_value" class="form-control"  minlength='1' maxlength='200' required="true"></textarea>
			</td>
		</tr>
		<!-- 
		<tr>
			<td><fmt:message key="label.is.request.action.notice" /></td>
			<td>
			<div id="requestDealRemark_noticeEmail_div" style="display: none;">
					<input style="width: 98%;" value="${requestDetailDTO.createdByEmail}" id="requestDealRemark_noticeEmail_input" ondblclick="itsm.request.requestDetail.selectNoticeUser('#requestDealRemark_noticeEmail_input','email')"/>
					<br><span><fmt:message key="label.email.address.format" /></span>
			</div>
			<input type="checkbox" value="true" id="requestDealRemark_isEmailNotice" onclick="itsm.request.requestDetail.requestActionNoticeSet('#requestDealRemark_isEmailNotice','#requestDealRemark_noticeEmail_div')" />
			
			</td>
		</tr>
		 -->
		<tr>
			<td colspan="2">
				<input type="hidden" name="historyRecordDto.logTitle" id="requestProcessMrak_action" value="<fmt:message key="label.request.requestProcessMrak" />" />
				<input type="hidden" name="requestDTO.optType" value="requestDealRemark" />
				<input type="button" style="margin-top: 10px;" class="btn btn-primary btn-sm" onclick="itsm.request.requestAction.requestDealRemark_opt()" value="<fmt:message key="label.request.add" />">
			</td>
		</tr>
	</table>
	</div>
	</form>
</div>

<!-- 请求处理完成-->
<div id="requestDealComplete_win" class="WSTUO-dialog" title="<fmt:message key="label.request.processComplete" />" style="width:480px;height:auto">
	<form>
	
	<div class="lineTableBgDiv">
		<table style="width:100%" class="lineTable" cellspacing="1">
		<tr>
			<td style="width:30%"><fmt:message key="common.remark" /></td>
			<td style="width:70%">
				<textarea rows="8" cols="45" name="historyRecordDto.logDetails" id="requestDealComplete_value" class="form-control"  minlength='1' maxlength='200'></textarea>
			</td>
		</tr>
<%-- 		<tr>
			<td><fmt:message key="label.is.request.action.notice" /></td>
			<td>
			<div id="requestDealComplete_noticeEmail_div" style="display: none;">
					<input style="width: 98%;" id="requestDealComplete_noticeEmail_input" ondblclick="itsm.request.requestDetail.selectNoticeUser('#requestDealComplete_noticeEmail_input','email')"/>
					<br><span><fmt:message key="label.email.address.format" /></span>
			</div>
			<input type="checkbox" value="true" id="requestDealComplete_isEmailNotice" onclick="itsm.request.requestDetail.requestActionNoticeSet('#requestDealComplete_isEmailNotice','#requestDealComplete_noticeEmail_div')" />
			
			</td>
		</tr> --%>
		<tr>
			<td colspan="2">
				<input type="hidden" name="historyRecordDto.logTitle" id="processComplete_action" value="<fmt:message key="label.request.processComplete" />" />
				<input type="hidden" name="requestDTO.optType" value="requestDealComplete" />
				<a class="btn btn-primary btn-sm" onclick="itsm.request.requestAction.requestDealComplete_opt()"><fmt:message key="common.submit" /></a>
			</td>
		</tr>
	</table>
	</div>
	</form>
</div>


<!-- 请求审批  -->
<div id="requestApproval_win" class="WSTUO-dialog" title="<fmt:message key="label.request.requestApproval" /> " style="width:480px;height:auto">
	<form>
	<div class="lineTableBgDiv">
		<table style="width:100%" class="lineTable" cellspacing="1">
		<tr>
			<td style="width:30%"><fmt:message key="common.remark" /></td>
			<td style="width:70%">
				<textarea rows="8" cols="45" name="historyRecordDto.logDetails" id="requestApprovalRemark" class="form-control"  minlength='1' maxlength='200' ></textarea>
			</td>
		</tr>
		<%-- <tr>
			<td><fmt:message key="label.is.request.action.notice" /></td>
			<td>
			<div id="requestApproval_noticeEmail_div" style="display: none;">
					<input style="width: 98%;" id="requestApproval_noticeEmail_input" ondblclick="itsm.request.requestDetail.selectNoticeUser('#requestApproval_noticeEmail_input','email')"/>
					<br><span><fmt:message key="label.email.address.format" /></span>
			</div>
			<input type="checkbox" value="true" id="requestApproval_isEmailNotice" onclick="itsm.request.requestDetail.requestActionNoticeSet('#requestApproval_isEmailNotice','#requestApproval_noticeEmail_div')" />
			
			</td>
		</tr> --%>
		<tr>
			<td colspan="2">
				<input type="hidden" name="historyRecordDto.logTitle" id="requestApproval_action" value="<fmt:message key="label.request.requestApproval" /> " />
				<input type="hidden" name="requestDTO.optType" value="requestApproval" />
				<input type="hidden" name="requestDTO.taskId" />
				<a class="btn btn-primary btn-sm" onclick="itsm.request.requestAction.requestApproval_opt('同意')"><fmt:message key="label.request.requestApprovalAgree" /></a>
				<a  class="btn btn-primary btn-sm" onclick="itsm.request.requestAction.requestApproval_opt('不同意')"><fmt:message key="label.request.requestApprovalDisagree" /></a>
			</td>
		</tr>
		</table>
	</div>
	</form>
</div>

<!-- 请求审批(NEW)  -->
<div id="requestApprovalResult_win" class="WSTUO-dialog" title="<fmt:message key="label.request.requestApproval" />" style="width:480px;height:auto">
	<form>
	<div class="lineTableBgDiv">
		<table style="width:100%" class="lineTable" cellspacing="1">
		<tr>
			<td style="width:30%"></td>
			<td style="width:70%">
				<textarea rows="8" cols="45" name="historyRecordDto.logDetails"></textarea>
			</td>
		</tr>
		
		<tr>
			<td colspan="2">
				<input type="hidden" name="historyRecordDto.logTitle" id="requestApprovalResult_logTitle" />
				<input type="hidden" name="requestDTO.optType" value="requestApprovalResult" />
				<a class="btn btn-primary btn-sm" onclick="itsm.request.requestAction.requestApprovalResult_opt('true')"><fmt:message key="label.request.requestApprovalAgree" /></a>
				<a  class="btn btn-primary btn-sm" onclick="itsm.request.requestAction.requestApprovalResult_opt('false')"><fmt:message key="label.request.requestApprovalDisagree" /></a>
			</td>
		</tr>
		</table>
	</div>
	</form>
</div>




<!-- 请求回访-->
<div id="requestVisit_win" class="WSTUO-dialog" title="<fmt:message key="label.request.requestVisit"/>" style="width:500px;height:auto">
<form>
	<input type="hidden" name="historyRecordDto.logTitle" value="<fmt:message key="label.request.requestVisit" />" />
	<input type="hidden" name="requestDTO.optType" id="requestVisit_optType" value="requestVisit" />
	<input type="hidden" name="requestDTO.visitRecord" id="request_visitRecord"/>
	<input type="hidden" name="historyRecordDto.logDetails" id="requestVisit_remark">
	<div class="lineTableBgDiv">
		<table style="width:100%" id="requestVisit_table" class="lineTable" cellspacing="1">
			<tr>
				<td width="35%"><fmt:message key="label.returnVisit.object" /></td>
				<td width="35%"><input class="input" name="requestDTO.createdByName" id="requestVisit_createdByName_input"  type="hidden"/>
					<span id="requestVisit_createdByName_span"></span>
				</td>
				<td style="text-align: center;">
					<input type="button" class="btn btn-primary btn-sm" onclick="itsm.request.requestAction.requestEmailVisit_opt()" value="<fmt:message key="common.submit" />">
				</td>
			</tr>
		</table>
	</div>
</form>
</div>


<!-- 自助解决 -->
<div id="self_help_resolve_win" class="WSTUO-dialog" title="<fmt:message key="label.request.self.help.resolve" />" style="width:500px;height:auto">
<form event="itsm.request.requestAction.self_help_resolve_opt">
	<input type="hidden" name="historyRecordDto.logTitle" value="<fmt:message key="label.request.self.help.resolve" />" />
	<input type="hidden" name="requestDTO.optType" value="requestDirectClose" />
	<div class="lineTableBgDiv">
	<table style="width:100%" class="lineTable" cellspacing="1">
		<tr>
			<td><fmt:message key="title.request.solutions" /></td>
			<td>
				<textarea style="width: 98%;height: 100px" name="requestDTO.solutions" class="form-control" required="true" id="requestSelfHelpLogDetails_solutions"></textarea>
			</td>
		</tr>		
		<tr>
			<td><fmt:message key="common.remark" /></td>
			<td><br/>
				<textarea style="width: 98%;height: 80px"  id="requestSelfHelpLogDetails"  class="form-control"></textarea>
				<input type="hidden" name="historyRecordDto.logDetails" id="requestSelftHelpHiddenLog"/>
			</td>
		</tr>
		<tr><td colspan="2">
			<input  type="submit" class="btn btn-primary btn-sm" style="margin-right: 15px;" value="<fmt:message key="common.submit" />"/>
		</td></tr>
	</table>
	</div>
</form>
</div>


<!-- 请求挂起 -->
<div id="requestHang_win" class="WSTUO-dialog" title="<fmt:message key="request.action.hang" />" style="width:500px;height:auto">
<form>
	<input type="hidden" name="historyRecordDto.logTitle" id="requestHang_action" value="<fmt:message key="request.action.hang" />" />
	<input type="hidden" name="requestDTO.optType" value="requestHangRemove" />
	<input type="hidden" name="requestDTO.hang" value="true" />
	<div class="lineTableBgDiv">
	<table style="width:100%" class="lineTable" cellspacing="1">
		<tr>
			<td><fmt:message key="common.remark" /></td>
			<td>
				<textarea style="width: 98%;height: 80px" name="historyRecordDto.logDetails" class="form-control" ></textarea>
			</td>
		</tr>
		
		<%-- <tr>
				<td><fmt:message key="label.is.request.action.notice" /></td>
				<td>
				<input type="checkbox" value="true" id="requestHang_isEmailNotice" onclick="itsm.request.requestDetail.requestActionNoticeSet('#requestHang_isEmailNotice','#requestHang_noticeEmail_div')" />
				<div id="requestHang_noticeEmail_div" style="display: none;">
					<input style="width: 98%;" id="requestHang_noticeEmail_input" ondblclick="itsm.request.requestDetail.selectNoticeUser('#requestHang_noticeEmail_input','email')"/>
					<br><span><fmt:message key="label.email.address.format" /></span>
				</div>
					
				</td>
		</tr> --%>
		
		<tr><td colspan="2">
			<input type="button" style="margin-top: 10px;" class="btn btn-primary btn-sm" onclick="itsm.request.requestAction.requestHang_opt()" value="<fmt:message key="common.submit" />"/>
		</td></tr>
	</table>
	</div>
</form>
</div>

<!-- 解除请求挂起 -->
<div id="requestHangRemove_win" class="WSTUO-dialog" title="<fmt:message key="request.action.hangRemove" />" style="width:500px;height:auto">
<form>
	<input type="hidden" name="historyRecordDto.logTitle" value="<fmt:message key="request.action.hangRemove" />" />
	<input type="hidden" name="requestDTO.optType" value="requestHangRemove" />
	<input type="hidden" name="requestDTO.hang" value="false" />
	<div class="lineTableBgDiv">
	<table style="width:100%" class="lineTable" cellspacing="1">
		<tr>
			<td><fmt:message key="common.remark" /></td>
			<td>
				<textarea style="width: 98%;height: 80px" name="historyRecordDto.logDetails" class="form-control"></textarea>
			</td>
		</tr>
		
<%-- 		<tr>
				<td><fmt:message key="label.is.request.action.notice" /> </td>
				<td>
				<input type="checkbox" value="true" id="requestHangRemove_isEmailNotice" onclick="itsm.request.requestDetail.requestActionNoticeSet('#requestHangRemove_isEmailNotice','#requestHangRemove_noticeEmail_div')" />
				<div id="requestHangRemove_noticeEmail_div" style="display: none;">
					<input style="width: 98%;" id="requestHangRemove_noticeEmail_input" ondblclick="itsm.request.requestDetail.selectNoticeUser('#requestHangRemove_noticeEmail_input','email')"/>
					<br><span><fmt:message key="label.email.address.format" /></span>
				</div>
				</td>
		</tr> --%>
		
		<tr><td colspan="2">
			<input type="button" class="btn btn-primary btn-sm" onclick="itsm.request.requestAction.requestHangRemove_opt()" value="<fmt:message key="common.submit" />"/>
		</td></tr>
	</table>
	</div>
</form>
</div>


<!-- 修改SLA时间 -->
<div id="editRequestSlaTime_win" class="WSTUO-dialog" title="<fmt:message key="request.action.editSlaTime" />" style="width:500px;height:auto">
<form>
	<input type="hidden" name="historyRecordDto.logTitle" value="<fmt:message key="request.action.editSlaTime" />" />
	<input type="hidden" name="requestDTO.optType" value="editRequestSlaTime" />
	<div class="lineTableBgDiv">
	<table style="width:100%" class="lineTable" cellspacing="1">
	
		<tr>
			<td><fmt:message key="label.request.slaReuqestTime" /></td>
			<td>
				<input type="hidden" id="requestActionHtml_responseTime_old" />
				<input name="requestDTO.requestResponseTime" id="requestActionHtml_responseTime"  class="form-control" required="true"  />
			</td>
		</tr>
		
		<tr>
			<td><fmt:message key="label.request.slaCompleteTime" /></td>
			<td>
				<input type="hidden" id="requestActionHtml_completeTime_old"  />
				<input name="requestDTO.requestCompleteTime" id="requestActionHtml_completeTime" class="form-control" required="true" validType="DateComparison['requestActionHtml_responseTime']" />
			</td>
		</tr>
		
		<tr>
			<td><fmt:message key="common.remark" /></td>
			<td>
				<textarea style="width: 98%;height: 80px" id="editRequestSlaTime_remark" class="form-control"></textarea>
			</td>
		</tr>
		<tr><td colspan="2">
			<input type="button" class="btn btn-primary btn-sm" onclick="itsm.request.requestAction.editRequestSlaTime_opt()" value="<fmt:message key="common.submit" />"/>
		</td></tr>
	</table>
	</div>
</form>
</div>


<!-- 请求流程重新开启 -->
<div id="request_reopen_win" class="WSTUO-dialog" title="<fmt:message key="label.request.reStartRequest" />" style="width:500px;height:auto">
<form event="itsm.request.requestAction.reopen_opt">
	<input type="hidden" name="historyRecordDto.logTitle" value="<fmt:message key="label.flow.re.open" />" />
	<input type="hidden" name="requestDTO.optType" value="reopen" />
	<div class="lineTableBgDiv">
	<table style="width:100%" cellspacing="1">
		<tr>
			<td><fmt:message key="common.remark" /></td>
			<td>
				<textarea style="width: 98%;height: 80px" id="requestReopenLogDetails" class="form-control" required="true"  minlength='1' maxlength='200' ></textarea>
				<input name="historyRecordDto.logDetails" type="hidden" id="requestReopenLogDetailshidden"/>
			</td>
		</tr>
		<tr><td colspan="2">
			<button class="btn btn-primary btn-sm" style="margin-top: 10px;"><fmt:message key="common.submit" /></button>
		</td></tr>
	</table>
	</div>
</form>
</div>

<!-- 请求关闭审核 -->
<div id="request_close_audit_win" class="WSTUO-dialog" title='<fmt:message key="label.requestAction.closeApprove" />' style="width:500px;height:auto">
<form>
	<input type="hidden" name="historyRecordDto.logTitle" value='<fmt:message key="label.requestAction.closeApprove" />' />
	<input type="hidden" name="requestDTO.optType" value="closeAudit" />
	<div class="lineTableBgDiv">
	<table style="width:100%" class="lineTable" cellspacing="1">
		<tr>
			<td><fmt:message key="common.remark" /></td>
			<td>
				<textarea style="width: 98%;height: 80px" name="historyRecordDto.logDetails" class="form-control" required="true" ></textarea>
			</td>
		</tr>
		<tr><td colspan="2">
			<a class="btn btn-primary btn-sm" onclick="itsm.request.requestAction.closeAudit_opt('true')" ><fmt:message key="label_knowledge_approvalPass" /> </a>
			<a  class="btn btn-primary btn-sm" onclick="itsm.request.requestAction.closeAudit_opt('false')" ><fmt:message key="label_knowledge_approvalRefuse" /></a>
		</td></tr>
	</table>
	</div>
</form>
</div>

<!--请求描述不全未提交 -->
<div id="request_notComprehensiveNotSubmitted_win" class="WSTUO-dialog" title="<fmt:message key="label.notComprehensiveNotSubmitted" />" style="width:500px;height:auto">
<form>
	<input type="hidden" name="historyRecordDto.logTitle" value='<fmt:message key="label.notComprehensiveNotSubmitted" />'/>
	<input type="hidden" name="requestDTO.optType" value="notComprehensiveNotSubmitted" />
	<div class="lineTableBgDiv">
	<table style="width:100%" class="lineTable" cellspacing="1">
		<tr>
			<td><fmt:message key="common.remark" /></td>
			<td>
				<textarea style="width: 98%;height: 80px" name="historyRecordDto.logDetails" class="form-control" required="true"  minlength='1' maxlength='200'></textarea>
			</td>
		</tr>
		<%-- <tr>
			<td><fmt:message key="label.is.request.action.notice" /></td>
			<td>
			<div id="notComprehensiveNotSubmitted_noticeEmail_div" style="display: none;">
				<input style="width: 98%;" id="notComprehensiveNotSubmitted_noticeEmail_input" ondblclick="itsm.request.requestDetail.selectNoticeUser('#notComprehensiveNotSubmitted_noticeEmail_input','email')"/>
				<br><span><fmt:message key="label.email.address.format" /></span>
			</div>
			<input type="checkbox" value="true" id="notComprehensiveNotSubmitted_isEmailNotice" onclick="itsm.request.requestDetail.requestActionNoticeSet('#notComprehensiveNotSubmitted_isEmailNotice','#notComprehensiveNotSubmitted_noticeEmail_div')" />
				
			</td> 
		</tr>--%>
		<tr><td colspan="2">
			<span class="btn btn-primary btn-sm" onclick="itsm.request.requestAction.notComprehensiveNotSubmitted_opt()" ><fmt:message key="common.submit" /></span>
		</td></tr>
	</table>
	</div>
</form>
</div>

<!--请求描述完全,重新提交 -->
<div id="request_resubmit_win" class="WSTUO-dialog" title="<fmt:message key="label.desc.complete.resubmit" />" style="width:500px;height:auto">
<form>
	<input type="hidden" name="historyRecordDto.logTitle" value="<fmt:message key="label.desc.complete.resubmit" />" />
	<input type="hidden" name="requestDTO.optType" value="reSubmit" />
	<div class="lineTableBgDiv">
	<table style="width:100%" class="lineTable" cellspacing="1">
		<tr>
			<td><fmt:message key="common.remark" /></td>
			<td>
				<textarea style="width: 98%;height: 80px" name="historyRecordDto.logDetails" class="form-control" required="true"  minlength='1' maxlength='200'></textarea>
			</td>
		</tr>
		<tr><td colspan="2">
			<span class="btn btn-primary btn-sm" onclick="itsm.request.requestAction.resubmit_opt()" ><fmt:message key="common.submit" /></span>
		</td></tr>
	</table>
	</div>
</form>
</div>

<!--流程处理 -->
<div id="request_process_handle_win" class="WSTUO-dialog" title="<fmt:message key="label.desc.complete.resubmit" />" style="width:500px;height:auto">
<form>
	<input type="hidden" name="historyRecordDto.logTitle" id="request_logTitle" />
	<input type="hidden" name="requestDTO.optType" value="processHandle" />
	<div class="lineTableBgDiv">
	<table style="width:100%" class="lineTable" cellspacing="1">
		<tr>
			<td><fmt:message key="common.remark" /></td>
			<td>
				<textarea style="width: 98%;height: 80px" name="historyRecordDto.logDetails" class="form-control" required="true" ></textarea>
			</td>
		</tr>
		<tr><td colspan="2">
			<a class="btn btn-primary btn-sm" onclick="itsm.request.requestAction.processHandle_opt()" ><fmt:message key="common.submit" /></a>
		</td></tr>
	</table>
	</div>
</form>
</div>


	<div id="processHandleDiv" class="easyui-window" shadow="false" closed="true" modal="false"  title="流程处理" minimizable="false" maximizable="false" collapsible="false" style="width:480px;height:auto" >
		<form id="processHandle_form">
			<div class="lineTableBgDiv">
				<table style="width:100%" class="lineTable" cellspacing="1">
				<tr>
					<td><fmt:message key="label.request.startRequestDealMark" />&nbsp;</td>
					<td>
						<textarea rows="8" cols="45" name="historyRecordDto.logDetails" class="form-control"  minlength='1' maxlength='200' id="requestDeal_value"></textarea>
					</td>
				</tr>
	
				<tr>
					<td colspan="2">
						<input type="hidden" name="historyRecordDto.logTitle" id="processHandle_action" value="" />
						<input type="hidden" name="requestDTO.optType" id="processHandle_optType" value="flowHandle" />
						<input type="hidden" name="requestDTO.taskId" id="processHandle_taskId" value="" />
						<input type="hidden" name="requestDTO.assigneeName" id="processHandle_assigneeName" value="" />
						<a class="btn btn-primary btn-sm" onclick="itsm.request.requestAction.processHandle()"><fmt:message key="label.request.startProcess" /></a>
					</td>
				</tr>
				</table>
			</div>
		</form>
	</div>
	
	<!-- 审批加签 -->
<div id="request_openGetUser" class="WSTUO-dialog" title="<fmt:message key="lable.request.appor.user" />">
	<form id="request_openGetUser_from">
	<table style="width:100%" class="lineTable" cellspacing="1">
		<tr>
			<td style="width: 20%; height: 100px;"><fmt:message key="common.remark" />:</td>
			<td>
				<textarea id="appro_User" style="width: 98%; height: 100px;" class="form-control"  required="true"  ></textarea>
			</td>
		</tr>
		<tr>
			<td><fmt:message key="lable.request.approveUser" /> :</td>
			<td>
				<input type="hidden" id="requestDetail_assigneeNo_Appro">
				<input type="text" style="width: 85%" id="appro_User_Name" onclick="wstuo.user.userUtil.selectUser('#appro_User_Name','#requestDetail_assigneeNo_Appro','','fullName',${topCompanyNo})" class="form-control" required="true">&nbsp;&nbsp;
				<a onclick="cleanIdValue('appro_User_Name','requestDetail_assigneeNo_Appro')" title="<fmt:message key="label.request.clear" />"><i class="glyphicon glyphicon-remove-circle form-control-feedback choose"></i></a>
			</td>
		</tr>
		<tr>
			<td colspan="2"><a class="btn btn-primary btn-sm" onclick="itsm.request.requestDetail.send_Email_appro()"><fmt:message key="common.submit" /></a></td>
		</tr>
	</table></form>
</div>

</body>
</html>



