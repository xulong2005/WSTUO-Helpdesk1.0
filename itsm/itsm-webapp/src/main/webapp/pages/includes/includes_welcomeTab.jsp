<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ include file="../language.jsp" %>
<!-- <style>
.welcome-div{
	width:145px;
	float:left;
	line-height:25px;
	text-align:center;
	cursor:pointer;
	margin-top:15px;
	margin-left:0px;
}
.welcome-div img{
	width:58px;
	height:58px;
}
</style> -->
<script src="../scripts/common/compass/fullSearch.js?random=<%=new java.util.Date().getTime()%>"></script>
<script src="../scripts/common/config/customFilter/filterGrid_Operation.js?random=<%=new java.util.Date().getTime()%>"></script>
<script src="${pageContext.request.contextPath}/scripts/common/knowledge/leftMenu.js?random=<%=new java.util.Date().getTime()%>"></script>
<script src="../scripts/common/report/processingTime.js?random=<%=new java.util.Date().getTime()%>"></script>
<c:if test="${cimHave eq true}">
<script src="../scripts/common/report/clickEvent.js?random=<%=new java.util.Date().getTime()%>"></script>
</c:if>
<script>
var title_security_roleManager='<fmt:message key="title.security.roleManager"/>';
var title_security_userManager='<fmt:message key="title.security.userManager"/>';
var systemVersion = '${sessionScope.systemVersion }';//系统版本
/**
 * 快速创建
 */
function doFastCreate(){
	 var title= $('#welcome_etitle').val();
	 $('#welcome_etitle').val(trim(title));
	 var content= $('#welcome_content').val();
	 $('#welcome_content').val(trim(content));
	 if($('#welcome_tab_fastcreate_window form').form('validate')){
			var frm = $('#welcome_tab_fastcreate_window form').serialize();
			var url = 'request!saveRequest.action';
			startLoading();
			$.post(url,frm, function(eno){
				endLoading();
				//重新统计请求
				msgShow(i18n['createSuccess'],'show');
				$('#welcome_etitle').val('');
				$('#welcome_content').val('');
				$('#welcome_tab_fastcreate_window').dialog('close');
				basics.tab.tabUtils.reOpenTab("request!requestDetails.action?eno="+eno,i18n["request_detail"]);
			});
	 }
}
/**
 * 选择语言版本
 */
function selectlanguage(){
	setCookie('language',$("#language_Select").val(),300);
	location.href='index.jsp';
}

/**
 * 创建请求
 */
function welcome_createRequest(){
	common.config.includes.includes.loadCategoryIncludesFile();
	basics.tab.tabUtils.refreshTab(i18n["title_request_addRequest"],'../pages/itsm/request/addRequestFormCustom.jsp');
	
}

/**
 * 打开搜索知识窗口.
 */
function open_search_kw_window(){
	 windows('welcome_tab_search_kw_window',{width:350});
}


/**
 * 查看我的请求。
 */
function view_my_request(){
	var _url='../pages/itsm/request/requestMain.jsp?countQueryType=myProposedRequest';
	basics.tab.tabUtils.addTab(i18n['title_request_requestGrid'],_url);
}
var options={};
if(isITSOPUser){		
	$.extend(options,{'companyNo_companyNo': 'label_belongs_client'});
}

$.extend(options,{
	//变量名_类型:字段名
	'requestCode_String':'number',
	'etitle_String':'common_title',
	'requestCategory.eventId_ReventId' : 'category',
	'slaState.dcode_dcode':'title_request_SLAState',
	'requestStatus.dcode_dcode':'common_state',
	'imode.dcode_dcode':'label_sla_imode',
	'level.dcode_dcode':'label_sla_level',
	'priority.dcode_dcode' : 'priority',
	'seriousness.dcode_dcode':'label_sla_seriousness',
	'effectRange.dcode_dcode':'label_sla_effectRange',
	'createdBy.loginName_loginName':'requester',
	//请求人所在组
	'createdBy.orgnization.orgNo':'title_requester_group',
	'assigneeGroup.orgNo_orgNo' :  'title_request_assignToGroup',
	'technician.loginName_loginName' : 'title_request_assignToTC',
	'owner.loginName_loginName':'common_owner',
	'createdOn_Data' : 'common_createTime'
});



function doImportCI(){
		if($('#welcome_tab_import_ci_file').val()==""){
			msgAlert(i18n['msg_dc_fileNull'],'info');
		}else{
			startProcess();
			$.ajaxFileUpload({
	            url:'ci!importConfigureItem.action?ciDto.companyNo='+$('#welcome_tab_csv_import_companyNo').val(),
	            secureuri:false,
	            fileElementId:'welcome_tab_import_ci_file', 
	            dataType:'json',
	            success: function(data,status){
	            	if(data=="ERROR_CSV_FILE_NOT_EXISTS"){
						msgAlert(i18n['msg_dc_fileNotExists'],'info');
					}else if(data=="ERROR_CSV_FILE_IO"){
						msgAlert(i18n['msg_dc_importFailure'],'info');
					}else{
						$('#welcome_tab_import_ci_window').dialog('colse');
						msgAlert(data.replace('Total',i18n['opertionTotal']).replace('Insert',i18n['newAdd']).replace('Update',i18n['update']).replace('Failure',i18n['failure']),'show');
					}
	            	endProcess();
	            }
	        });
		}
}


$(document).ready(function(){
	itsm.request.includes.includes.loadRequestActionIncludesFile();
	common.security.includes.includes.loadSelectCustomerIncludesFile();
	common.config.includes.includes.loadCategoryIncludesFile();
	common.config.includes.includes.loadCustomFilterIncludesFile();
	basics.includes.loadWelcomeIncludesFile();
	basics.includes.loadImportCsvIncludesFile();
	/* $('#wel_myHomePage').click(function(){//我的首页
		if ($('#itsmMainTab').tabs('exists', i18n['title_my_home'])){
			$('#itsmMainTab').tabs('select', i18n['title_my_home']);
		} else {
			
			$('#itsmMainTab').tabs('add',{
				title:i18n['title_my_home'],
				cache:"true",
				href:'userCustom!portalDashboard.action?queryDTO.customType=1&queryDTO.loginName='+userName,
				closable:false
			});
		}
	}); */

	//服务台
	$('#wel_helpdesk').click(function(){
		$('#itsmMainTab').tabs('select',i18n['title_request_requestGrid']);
		var _url='../pages/itsm/request/requestMain.jsp?'
		if(!isAllRequest_Res){//是否允许查看全部权限
			_url=_url+'&countQueryType=myProposedRequest&currentUser='+userName
		}
		
		basics.tab.tabUtils.addTab(i18n['title_request_requestGrid'],_url);
	});
	//门户面板配置
	/* $('#wel_dashboardSet').click(function(){
		var _url="userCustom!portalDashboardPreview.action?queryDTO.customType=1&queryDTO.loginName="+userName;
		var dashboardSet ='<fmt:message key="title.dashboard.setting"/>';
		basics.tab.tabUtils.addTab(dashboardSet,_url);
	}); */
	
	//知识库
	$('#wel_knowledgeBase').click(function(){
		    common.knowledge.leftMenu.showKnowledgeIndex("knowledge");
	});
	
	//快速创建请求
	$('#wel_addRequest_sp').click(function(){
		windows('welcome_tab_fastcreate_window',{width:350});
		$('#welcome_tab_fastcreate_window_submit').unbind().click(doFastCreate);
	});
	
	
	//添加请求，详情
	$('#wel_addRequest_dt').click(welcome_createRequest);
	
	
	//角色管理
	$('#wel_role').click(function(){
	    window.open('setting.jsp?firstMenu=visit&secondMenu=roleManager', '_blank');
	});
	
	//新增配置项
	$('#wel_addCI').click(function(){
		startLoading();
		basics.tab.tabUtils.refreshTab(i18n['ci_addConfigureItem'],'ci!configureItemAdd.action?categoryName=&categoryType=&categoryNo=&random=1338615712015');
		setTimeout(function(){  
			endLoading();
		},300); 
	});
	
	
	
	//关于WSTUO
	$('#wel_about').click(function(){
		
		window.open('note.jsp');
	});
	
	
	//问题管理
	$('#wel_problem').click(function(){
		basics.tab.tabUtils.addTab(i18n['problem_grid'],'../pages/itsm/problem/problemMain.jsp');
	});
	
	//添加变更
	$('#wel_log_change').click(function(){
		basics.tab.tabUtils.refreshTab(i18n['titie_change_add'],'../pages/itsm/change/addChange.jsp');
	});
	
	//添加问题
	$('#wel_log_poblem').click(function(){
		basics.tab.tabUtils.refreshTab(i18n['problem_add'],'../pages/itsm/problem/addProblem.jsp');
	});
	
	//查看报表
	$('#wel_vl_report').click(function(){
		var reporti18n=$('#reportLang').val();
		var requestFRC_line_week_week='<fmt:message key="requestFRC_line_week_week" />';
		var FIRSTDATES=common.report.processingTime.showWeekFirstDay(currentMonth).format('yyyy-MM-dd');
    	var LASTDATES=common.report.processingTime.showWeekLastDay(currentMonth).format('yyyy-MM-dd'); 
		basics.tab.tabUtils.addTab(requestFRC_line_week_week,"../pages/reports!statReportRead.action?dto.fileName=requestFRC_line_week&dto.firstDate="+FIRSTDATES+"&dto.lastDate="+LASTDATES+"&dto.showHide=WEEKS&dto.reporti18n="+reporti18n+"&dto.confirm=true");
	});

	//全文搜索
	$('#wel_vl_search').click(function(){
		common.compass.fullSearch.openFullsearchWindow('','request');
	});
	
	
	//用户管理
	$('#wel_user_mgr').click(function(){
	    window.open('setting.jsp?firstMenu=visit&secondMenu=userManager', '_blank');
	});
	
	
	//流程
	$('#wel_process_mgr').click(function(){
	    window.open('setting.jsp?firstMenu=workflow&secondMenu=securityProcessManage', '_blank');
	});
	
	
	//添加知识
	$('#wel_add_knowledge').click(function(){
		basics.tab.tabUtils.addTab(i18n['title_request_newKnowledge'],'../pages/common/knowledge/addKnowledge.jsp');
	});
	
	//系统设置
	$('#wel_sys_mgr').click(function(){
	    window.open('setting.jsp', '_blank');
	});
	
	//修改密码
	$('#wel_change_pwd').click(function(){
		window.open('setting.jsp?firstMenu=visit&secondMenu=password', '_blank');
	});
	
	//配置项管理
	$('#wel_ci_mgr').click(function(){
		
		startLoading();
		basics.tab.tabUtils.addTab(i18n['ci_configureItemAdmin'],'../pages/itsm/cim/configureItem.jsp');
		endLoading();
	});

	
	//变更委员会
	$('#wel_cab_mgr').click(function(){
	    window.open('setting.jsp?firstMenu=dataDictionary&secondMenu=cab', '_blank');
	});
	
	//机构设置
	$('#wel_org_mgr,#wel_org_tree').click(function(){
	    window.open('setting.jsp?firstMenu=org&secondMenu=orgSet', '_blank');
	});
	
	//重新登录
	$('#wel_re_login').click(function(){
		windows('welcome_tab_language_window',{width:350});
		$('#selectlanguageBtn').unbind().click(selectlanguage);
	});
	
	
	//用户回访
	$('#wel_user_retrospect').click(function(){
		basics.tab.tabUtils.addTab('<fmt:message key="label.returnVisit.manage" />','../pages/common/tools/visit/returnVisitManage.jsp')
	});
	
	
	//用户手册
	$('#wel_user_manual').click(function(){
		window.open('http://www.bangzhutai.com/pages/newResources.jsp?type=28&_page=1');
	});

	//辅助工具
	$('#wel_tools_mgr').click(function(){
		
		startLoading();
		basics.tab.tabUtils.addTab(default_page_tools,'../pages/common/tools/task/task.jsp');   
		$('#main_menu_tabs li').removeClass("tabs-selected");
		$('#toolsTopMenu_li').addClass("tabs-selected");
		setTimeout(function(){  
			endLoading();
		},300); 
	});

	//本地化设置
	$('#wel_locale_mgr').click(function(){
	    window.open('setting.jsp?firstMenu=locale&secondMenu=currencySet', '_blank');
	});

	//帮助卡
	$('#wel_Help').click(openHelp);
	
	//搜索知识
	$('#wel_search_knowledgeBase').click(open_search_kw_window);
	
	
	//自助服务
	$('#wel_self_service').click(function(){
		
		windows('welcome_tab_self_service_window',{width:350});
	});
	
	
	//事件查看器
	$('#wel_filter_mgr').click(function(){
		common.config.customFilter.filterGrid_Operation.openCustomFilterWin(options,"request","com.wstuo.itsm.request.entity.Request");
	});
	
	
	//SLA状态统计
	$('#wel_sla_status').click(function(){
		//打开报表-SLA状态统计
		var requestSLA_response_line='<fmt:message key="requestSLA_response_line_week_week" />';
		var reporti18n=$('#reportLang').val();
		var FIRSTDATES=common.report.processingTime.showWeekFirstDay(currentMonth).format('yyyy-MM-dd');
	    var LASTDATES=common.report.processingTime.showWeekLastDay(currentMonth).format('yyyy-MM-dd');
	    basics.tab.tabUtils.addTab(requestSLA_response_line,"../pages/reports!statReportRead.action?dto.fileName=requestSLA_response_line_week&dto.firstDate="+FIRSTDATES+"&dto.lastDate="+LASTDATES+"&dto.showHide=WEEKS&dto.reporti18n="+reporti18n+"&dto.confirm=true");
		});
	
	//配置项的导入
	$('#wel_ci_import').click(function(){
		//加载默认公司
		common.security.defaultCompany.loadDefaultCompany('#welcome_tab_csv_import_companyNo','#welcome_tab_csv_import_companyName');
		//公司选择
		$('#welcome_tab_csv_import_companyName').click(function(){
			itsm.itsop.selectCompany.openSelectCompanyWin('#welcome_tab_csv_import_companyNo','#welcome_tab_csv_import_companyName','');
		});
		windows('welcome_tab_import_ci_window',{width:400});
	});

	//$('#welcome_tab_contract_admin_window').unbind();
	
	//联系管理员
	$('#wel_contract_admin').click(function(){
		windows('welcome_tab_contract_admin_window',{width:400});
		$.post('user!find.action?page=1&rows=1&userQueryDto.roleCode=ROLE_SYSADMIN',function(data){
			if(data!=null && data.data.length>0){
				$('#contract_fullName').text(data.data[0].fullName);
				$('#contract_phone').text(data.data[0].phone);
				$('#contract_mobile').text(data.data[0].moblie);
				$('#contract_email').text(data.data[0].email);
			}
		});
	});
	$('#wel_news').click(function(){
		window.open('http://www.bangzhutai.com/pages/newResources.jsp?type=28&_page=1');
	});
	
	//添加报表（报表列表）
	$('#wel_report_list').click(function(){

		startLoading();
		common.security.includes.includes.loadSelectCustomerIncludesFile();
		common.report.includes.includes.loadReportViewIncludesFile();
		common.config.includes.includes.loadCategoryIncludesFile();
		common.config.includes.includes.loadCustomFilterIncludesFile();
		$('#main_menu_tabs li').removeClass("tabs-selected");
		$('#reportTopMenu_li').addClass("tabs-selected");
		
		basics.tab.tabUtils.refreshTab('<fmt:message key="title.dynamicReports.singleGroupReport" />','../pages/common/report/singleGroupGrid.jsp');
		setTimeout(function(){  
			endLoading();
		},300); 
	});
	
	
	
	//日志
	$('#wel_opt_log').click(function(){
	    window.open('setting.jsp?firstMenu=logManage&secondMenu=securityUserOptLog', '_blank'); 
	});
	
	
	//升级级别
	$('#wel_esc_level').click(function(){
	    window.open('setting.jsp?firstMenu=dataDictionary&secondMenu=escalateLevel', '_blank');
	});
	
	
	//升级级别
	$('#wel_next_page').click(function(){

		startLoading();
		basics.tab.tabUtils.addTab(i18n['title_my_home'],'userCustom!portalDashboard.action?queryDTO.customType=1&queryDTO.loginName='+userName)
	});
});

</script>

</head>

<div style="overflow:auto;background-image:url(../images/welcome/bg2.jpg);background-repeat:repeat-x;margin:40px 0px 0px 0px;">


<%-- <div class="welcome-div" id="wel_myHomePage">
	<img src="../images/welcome/My_Page.png"/><br/>
	<fmt:message key="label_welcome_page_MyPage"/>
</div> --%>
<sec:authorize url="PORTAL_DASHBOARD_RES">
<div class="welcome-div" id="wel_dashboardSet" onclick="basics.tab.tabUtils.addTab('<fmt:message key="title.dashboard.setting"/>','userCustom!portalDashboardPreview.action?queryDTO.customType=1&queryDTO.loginName=${sessionScope.loginUserName}')" style="width:145px;float:left;line-height:25px;text-align:center;cursor:pointer;margin-top:15px;margin-left:0px;">
	<img src="../images/welcome/Dashboard_Set.png"/><br/>
	<fmt:message key="title.dashboard.setting"/>
</div>
</sec:authorize>

<c:if test="${requestHave eq true}">
<sec:authorize url="REQUEST_MAIN">
<div class="welcome-div" id="wel_helpdesk" style="width:145px;float:left;line-height:25px;text-align:center;cursor:pointer;margin-top:15px;margin-left:0px;">
	<img src="../images/welcome/Helpdesk.png"/ style="width:58px;height: 58px;"><br/>
	<fmt:message key="label_welcome_page_Helpdesk"/>
</div>
</sec:authorize>
</c:if>

<c:if test="${requestHave eq true}">
<sec:authorize url="/pages/request!saveRequest.action">
<div class="welcome-div" id="wel_addRequest_sp" style="width:145px;float:left;line-height:25px;text-align:center;cursor:pointer;margin-top:15px;margin-left:0px;">
	<img src="../images/welcome/Login a Quick Call .png" style="width:58px;height: 58px;" /><br/>
	<fmt:message key="label_welcome_page_LoginQuickCall"/>
</div>
</sec:authorize>
</c:if>

<c:if test="${requestHave eq true}">
<sec:authorize url="/pages/request!saveRequest.action">
<div class="welcome-div" id="wel_addRequest_dt" style="width:145px;float:left;line-height:25px;text-align:center;cursor:pointer;margin-top:15px;margin-left:0px;">
	<img src="../images/welcome/Log Call with details.png" style="width:58px;height: 58px;"/><br/>
	<fmt:message key="label_welcome_page_LogCallwithdetails"/>
</div>
</sec:authorize>
</c:if>

<sec:authorize url="/pages/userReturnVisit!findPagerReturnVisit.action">
<div class="welcome-div" id="wel_user_retrospect" style="width:145px;float:left;line-height:25px;text-align:center;cursor:pointer;margin-top:15px;margin-left:0px;">
	<img src="../images/welcome/User Retrospect.png" style="width:58px;height: 58px;"/><br/>
	<fmt:message key="label_welcome_page_UserRetrospect"/>
</div>
</sec:authorize>

<c:if test="${problemHave eq true}">
<sec:authorize url="PROBLEM_MAIN">
<sec:authorize url="/pages/problem!findProblemPager.action">
<div class="welcome-div" id="wel_problem" style="width:145px;float:left;line-height:25px;text-align:center;cursor:pointer;margin-top:15px;margin-left:0px;">
	<img src="../images/welcome/Log an Incident.png" style="width:58px;height: 58px;"/><br/>
	<fmt:message key="label_welcome_page_LoganIncident"/>
</div>
</sec:authorize>
</sec:authorize>
</c:if>

<c:if test="${problemHave eq true}">
<sec:authorize url="/pages/problem!saveProblem.action">
<div class="welcome-div" id="wel_log_poblem" style="width:145px;float:left;line-height:25px;text-align:center;cursor:pointer;margin-top:15px;margin-left:0px;">
	<img src="../images/welcome/Login a Problem.png" style="width:58px;height: 58px;"/><br/>
	<fmt:message key="label_welcome_page_LogaProblem"/>
</div>
</sec:authorize>
</c:if>

<c:if test="${changeHave eq true}">
<sec:authorize url="/pages/change!saveChange.action">
<div class="welcome-div" id="wel_log_change" style="width:145px;float:left;line-height:25px;text-align:center;cursor:pointer;margin-top:15px;margin-left:0px;">
	<img src="../images/welcome/Log a Change Request.png" style="width:58px;height: 58px;"/><br/>
	<fmt:message key="label_welcome_page_LogaChangeRequest"/>
</div>
</sec:authorize>
</c:if>

<c:if test="${cimHave eq true}">
<sec:authorize url="CIM_MAIN">
<div class="welcome-div" id="wel_ci_mgr" style="width:145px;float:left;line-height:25px;text-align:center;cursor:pointer;margin-top:15px;margin-left:0px;">
	<img src="../images/welcome/CI_Mgmt.png" style="width:58px;height: 58px;"/><br/>
	<fmt:message key="label_welcome_page_CI_Mgmt"/> 
</div>
</sec:authorize>
</c:if>

<c:if test="${cimHave eq true}">
<sec:authorize url="/pages/ci!cItemSave.action">
<div class="welcome-div" id="wel_addCI" style="width:145px;float:left;line-height:25px;text-align:center;cursor:pointer;margin-top:15px;margin-left:0px;">
	<img src="../images/welcome/Add a CI.png" style="width:58px;height: 58px;"/><br/>
	<fmt:message key="label_welcome_page_AddaCI"/>
</div>
</sec:authorize>
</c:if>

<c:if test="${cimHave eq true}">
<sec:authorize url="/pages/ci!cItemSave.action">
<div class="welcome-div" id="wel_ci_import" style="width:145px;float:left;line-height:25px;text-align:center;cursor:pointer;margin-top:15px;margin-left:0px;">
	<img src="../images/welcome/Import or Export.png" style="width:58px;height: 58px;"/><br/>
	<fmt:message key="label_welcome_page_CIImporting"/>
</div>
</sec:authorize>
</c:if>

<sec:authorize url="KNOWLEDGEBASE_MAIN">
<div class="welcome-div" id="wel_knowledgeBase" style="width:145px;float:left;line-height:25px;text-align:center;cursor:pointer;margin-top:15px;margin-left:0px;">
	<img src="../images/welcome/Explore Knowledge Base.png" style="width:58px;height: 58px;"/><br/>
	<fmt:message key="label_welcome_page_KnowledgeBase"/>
</div>
</sec:authorize>

<sec:authorize url="KNOWLEDGEINFO_ADDKNOWLEDGE">
<div class="welcome-div" id="wel_add_knowledge" style="width:145px;float:left;line-height:25px;text-align:center;cursor:pointer;margin-top:15px;margin-left:0px;">
	<img src="../images/welcome/Write Down Knowledge.png" style="width:58px;height: 58px;"/><br/>
	<fmt:message key="label_welcome_page_WriteDownKnowledge"/>
</div>
</sec:authorize>

<sec:authorize url="KNOWLEGEINFO_VIEWKNOWLEGE">
<div class="welcome-div" id="wel_search_knowledgeBase" style="width:145px;float:left;line-height:25px;text-align:center;cursor:pointer;margin-top:15px;margin-left:0px;"> 
	<img src="../images/welcome/View Knowledge_Base.png" style="width:58px;height: 58px;"/><br/>
	<fmt:message key="label_welcome_page_SearchKnowledge"/>
</div>
</sec:authorize>

<c:if test="${requestHave eq true}">
<sec:authorize url="REPORT_MAIN">
<div class="welcome-div" id="wel_vl_report" style="width:145px;float:left;line-height:25px;text-align:center;cursor:pointer;margin-top:15px;margin-left:0px;">
	<img src="../images/welcome/Check Reports.png" style="width:58px;height: 58px;"/><br/>
	<fmt:message key="title.report.FRCRate"/>
</div>
</sec:authorize>
</c:if>

<c:if test="${requestHave eq true}">
<sec:authorize url="request_report_slaResponse">
<div class="welcome-div" id="wel_sla_status" style="width:145px;float:left;line-height:25px;text-align:center;cursor:pointer;margin-top:15px;margin-left:0px;">
	<img src="../images/welcome/SLA Status Statistics.png" style="width:58px;height: 58px;"/><br/>
	<fmt:message key="title.report.slaResponseRate"/>
</div>
</sec:authorize>
</c:if>

<sec:authorize url="REPORT_MAIN">
<div class="welcome-div" id="wel_report_list" style="width:145px;float:left;line-height:25px;text-align:center;cursor:pointer;margin-top:15px;margin-left:0px;">
	<img src="../images/welcome/Create a New Report.png" style="width:58px;height: 58px;"/><br/>
	<fmt:message key="title.dynamicReports.diyReport"/>
</div>
</sec:authorize>

<sec:authorize url="TOOLS_MAIN"><sec:authorize url="VIEWALLTASKRES">
<div class="welcome-div" id="wel_tools_mgr" style="width:145px;float:left;line-height:25px;text-align:center;cursor:pointer;margin-top:15px;margin-left:0px;">
	<img src="../images/welcome/Accesories.png" style="width:58px;height: 58px;"/><br/>
	<fmt:message key="common.myTask"/>
</div>
</sec:authorize></sec:authorize>

<sec:authorize url="BASICSET_MAIN">
<div class="welcome-div" id="wel_sys_mgr" style="width:145px;float:left;line-height:25px;text-align:center;cursor:pointer;margin-top:15px;margin-left:0px;">
	<img src="../images/welcome/Settings.png" style="width:58px;height: 58px;"/><br/>
	<fmt:message key="label_welcome_page_Settings"/>
</div>
</sec:authorize>

<sec:authorize url="ORGMG">
<div class="welcome-div" id="wel_org_mgr" style="width:145px;float:left;line-height:25px;text-align:center;cursor:pointer;margin-top:15px;margin-left:0px;">
	<img src="../images/welcome/Org Settings.png" style="width:58px;height: 58px;"/><br/>
	<fmt:message key="label_welcome_page_OrgSettings"/>
</div>
</sec:authorize>

<sec:authorize url="MODIFY_PASSWORD_MODIFY">
<div class="welcome-div" id="wel_change_pwd" style="width:145px;float:left;line-height:25px;text-align:center;cursor:pointer;margin-top:15px;margin-left:0px;">
	<img src="../images/welcome/Change Password.png" style="width:58px;height: 58px;"/><br/>
	<fmt:message key="label_welcome_page_ChangePassword"/>
</div>
</sec:authorize>

<sec:authorize url="USER">
<sec:authorize url="/pages/user!find.action">
<div class="welcome-div" id="wel_user_mgr" style="width:145px;float:left;line-height:25px;text-align:center;cursor:pointer;margin-top:15px;margin-left:0px;">
	<img src="../images/welcome/Users Admin.png" style="width:58px;height: 58px;"/><br/>
	<fmt:message key="label_welcome_page_UsersAdmin"/>
</div>
</sec:authorize>
</sec:authorize>

<sec:authorize url="ROLE">
<sec:authorize url="/pages/role!find.action">
<div class="welcome-div" id="wel_role" style="width:145px;float:left;line-height:25px;text-align:center;cursor:pointer;margin-top:15px;margin-left:0px;">
	<img src="../images/welcome/Roles.png" style="width:58px;height: 58px;"/><br/>
	<fmt:message key="label_welcome_page_Roles"/>
</div>
</sec:authorize>
</sec:authorize>

<sec:authorize url="SEC_DICTIONARY">
<div class="welcome-div" id="wel_cab_mgr" style="width:145px;float:left;line-height:25px;text-align:center;cursor:pointer;margin-top:15px;margin-left:0px;">
	<img src="../images/welcome/Change Mgmt Committee.png" style="width:58px;height: 58px;"/><br/>
	<fmt:message key="label_welcome_page_ChangeMgmtCommittee"/>
</div>
</sec:authorize>

<sec:authorize url="SEC_DICTIONARY">
<div class="welcome-div" id="wel_esc_level" style="width:145px;float:left;line-height:25px;text-align:center;cursor:pointer;margin-top:15px;margin-left:0px;">
	<img src="../images/welcome/Escalate a Call.png" style="width:58px;height: 58px;"/><br/>
	<fmt:message key="label_welcome_page_EscalateaCall"/>
</div>
</sec:authorize>

<sec:authorize url="/pages/useroptlog!findPager.action">
<div class="welcome-div" id="wel_opt_log" style="width:145px;float:left;line-height:25px;text-align:center;cursor:pointer;margin-top:15px;margin-left:0px;">
	<img src="../images/welcome/Track System Logs.png" style="width:58px;height: 58px;"/><br/>
	<fmt:message key="label_welcome_page_TrackSystemLogs"/>
</div>
</sec:authorize>

<sec:authorize url="/pages/jbpm!findPageProcessDefinitions.action">
<div class="welcome-div" id="wel_process_mgr" style="width:145px;float:left;line-height:25px;text-align:center;cursor:pointer;margin-top:15px;margin-left:0px;">
	<img src="../images/welcome/Workflows.png" style="width:58px;height: 58px;"/><br/>
	<fmt:message key="title.security.processManage"/>
</div>
</sec:authorize>

<sec:authorize url="CURRENCYSET_RES">
<div class="welcome-div" id="wel_locale_mgr" style="width:145px;float:left;line-height:25px;text-align:center;cursor:pointer;margin-top:15px;margin-left:0px;">
	<img src="../images/welcome/Locale.png" style="width:58px;height: 58px;"/><br/>
	<fmt:message key="label_welcome_page_Locale"/>
</div>
</sec:authorize>

<%-- <div class="welcome-div" id="wel_Help">
	<img src="../images/welcome/Help Cards.png"/><br/>
	<fmt:message key="label_welcome_page_HelpCards"/>
</div> --%>

<div class="welcome-div" id="wel_contract_admin" style="width:145px;float:left;line-height:25px;text-align:center;cursor:pointer;margin-top:15px;margin-left:0px;">
	<img src="../images/welcome/Contact System Admin.png" style="width:58px;height: 58px;"/><br/>
	<fmt:message key="label_welcome_page_ContactSystemAdmin"/>
</div>

<div class="welcome-div" id="wel_re_login" style="width:145px;float:left;line-height:25px;text-align:center;cursor:pointer;margin-top:15px;margin-left:0px;">
	<img src="../images/welcome/Login for other languages.png" style="width:58px;height: 58px;"/><br/>
	<fmt:message key="label_welcome_page_Loginforotherlanguages"/>
</div>

<div class="welcome-div" id="wel_news" style="width:145px;float:left;line-height:25px;text-align:center;cursor:pointer;margin-top:15px;margin-left:0px;"  <c:if test="${systemVersion!='WSTUO'}"><c:out value="style=display:none"></c:out></c:if> >
	<img src="../images/welcome/news.png" style="width:58px;height: 58px;"/><br/>
	<fmt:message key="welcome.news"/>
</div>

<div class="welcome-div" id="wel_about" style="width:145px;float:left;line-height:25px;text-align:center;cursor:pointer;margin-top:15px;margin-left:0px;"  <c:if test="${systemVersion!='WSTUO'}"><c:out value="style=display:none"></c:out></c:if> >
	<img src="../images/welcome/Help.png" style="width:58px;height: 58px;"/><br/>
	<fmt:message key="label_welcome_page_AboutWSTUO"/>
</div>

<%-- <div class="welcome-div" id="wel_self_service">
	<img src="../images/welcome/Self Service Portal3.png"/><br/>
	<fmt:message key="label_welcome_page_SelfService"/>
</div> 
<sec:authorize url="FNULLTEXTSEARCH_RES">
<div class="welcome-div" id="wel_vl_search">
	<img src="../images/welcome/Search.png"/><br/>
	<fmt:message key="label_welcome_page_Search"/>
</div>
</sec:authorize>
<sec:authorize url="itsm_request_requestMain_getDataByFilterSearch">
<div class="welcome-div" id="wel_filter_mgr">
	<img src="../images/welcome/Event Monitor.png"/><br/>
	<fmt:message key="label_welcome_page_EventMonitor"/>
</div>
</sec:authorize>
<sec:authorize url="/pages/serviceTime!saveOrgUpdate.action">
<div class="welcome-div" id="wel_org_tree">
	<img src="../images/welcome/Category-tree.png"/><br/>
	<fmt:message key="label_welcome_page_OrganizationTree"/>
</div>
</sec:authorize>
<div class="welcome-div" id="wel_user_manual">
	<img src="../images/welcome/User Manual.png"/><br/>
	<fmt:message key="label_welcome_page_UserManual"/>
</div>
--%>
<div class="welcome-div" style="width:145px;float:left;line-height:25px;text-align:center;margin-top:15px;margin-left:0px;" >
<br/><br/><br/><br/><br/><br/><br/><br/><br/>
<br/><br/><br/><br/><br/><br/><br/><br/><br/>
</div>
</div>
