<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ include file="../../language.jsp" %>
<script>var technicianNumber="<%=com.wstuo.common.config.register.service.RegisterService.licenseNumber%>";</script>
<script src="../scripts/itsm/itsop/ITSOPUserMain.js?random=<%=new java.util.Date().getTime()%>"></script>

<div class="loading" id="ITSOPUserMain_loading"><img src="../images/icons/loading.gif" /></div>

<div class="content" id="ITSOPUserMain_panel">

<!-- 外包客户列表 -->
<table id="ITSOPUserGrid"></table>
<div id="ITSOPUserGridPager"></div>

<div id="ITSOPUserGridToolbar" style="display: none">

	<sec:authorize url="/pages/itsopUser!addITSOPUser.action">
		<a class="easyui-linkbutton" plain="true" icon="icon-add" id="ITSOPUserGrid_add" title="<fmt:message key="common.add"/>"></a>
	</sec:authorize>
	
	<sec:authorize url="/pages/itsopUser!updateITSOPUser.action">
		<a class="easyui-linkbutton" plain="true" icon="icon-edit" id="ITSOPUserGrid_edit" title="<fmt:message key="common.edit"/>"></a>
	</sec:authorize>
	
	<sec:authorize url="/pages/itsopUser!deleteITSOPUser.action">
	<a class="easyui-linkbutton" plain="true" icon="icon-cancel" id="ITSOPUserGrid_delete" title="<fmt:message key="common.delete"/>"></a>
	</sec:authorize>
	
	<a class="easyui-linkbutton" plain="true" icon="icon-search" id="ITSOPUserGrid_search" title="<fmt:message key="common.search"/>"></a>
	
	<span id="itsop_license" style="color:#ff0000" ></span>
	
		<sec:authorize url="/pages/user!importITSOPUser.action"> 
	<a id="importITSOPUser" class="easyui-linkbutton" plain="true" icon="icon-import" title="<fmt:message key="label.dc.import"/>"></a>
	</sec:authorize>
	
	<sec:authorize url="/pages/user!exportITSOPUser.action"> 
	<a id="exportITSOPUser" class="easyui-linkbutton" plain="true" icon="icon-export" title="<fmt:message key="label.dc.export"/>"></a>
 	</sec:authorize> 
	<%-- 
	&nbsp;|&nbsp;<a href="javascript:showHelpFilm('itsop_customer_manage_workflow.flv')" plain="true" class="easyui-linkbutton" icon="icon-help"></a>
 	--%>
 	<fmt:message key="label.license.outCustomerNum"/>：${itsopNum }，<fmt:message key="itsop.itsopNumUserd"/>：<span id="itsopAlreadyUsed">0</span>
</div>

<div style="height:3px"></div>

<!-- 外包客户用户列表 -->
<table id="ITSOPCustomer_userGrid"></table>
<div id="ITSOPCustomer_userGridPager"></div>

<div id="ITSOPCustomer_userGridToolbar" style="background-color: #EEEEEE; height: 28px;display: none;">
	<sec:authorize url="/pages/user!save.action">
		<a class="easyui-linkbutton" plain="true" icon="icon-add" id="ITSOPCustomer_userGrid_add" title="<fmt:message key="common.add" />"></a> 
	</sec:authorize>
	
	<sec:authorize url="/pages/user!merge.action">
		<a class="easyui-linkbutton" plain="true" icon="icon-edit" id="ITSOPCustomer_userGrid_edit" title="<fmt:message key="common.edit" />"></a>
	</sec:authorize> 
	
	<sec:authorize url="/pages/user!delete.action">
	<a class="easyui-linkbutton" plain="true" icon="icon-cancel" id="ITSOPCustomer_userGrid_delete" title="<fmt:message key="common.delete" />"></a> 
	</sec:authorize>
	
	<sec:authorize url="/pages/user!find.action">
	<a class="easyui-linkbutton" plain="true" icon="icon-search" id="ITSOPCustomer_userGrid_search" title="<fmt:message key="common.search" />"></a>
	</sec:authorize>
	
	<sec:authorize url="/pages/user!updateRoleByUserId.action"> 
		<a class="easyui-linkbutton" plain="true" icon="icon-user" id="ITSOPCustomer_userGrid_role" title="<fmt:message key="label.user.roleSettings" />"></a>
	</sec:authorize>
	
</div>

<div id="ITSOPCustomer_userGrid_action" style="display:none"><sec:authorize url="/pages/user!updateRoleByUserId.action"> <a href="javascript:common.security.user.opt_setRole_aff()" title="<fmt:message key="label.user.roleSettings" />"><img src="../images/icons/user.gif" border="0" /></a></sec:authorize>&nbsp;&nbsp;&nbsp;&nbsp;<sec:authorize url="/pages/user!merge.action"><a href="javascript:common.security.user.editUser_aff()" title="<fmt:message key="common.edit" />"><img src="../skin/default/images/grid_edit.png" border="0"/></a></sec:authorize>&nbsp;&nbsp;&nbsp;<sec:authorize url="/pages/user!delete.action"><a href="javascript:common.security.user.opt_delete()" title="<fmt:message key="common.delete" />"><img src="../skin/default/images/grid_edit.png" border="0"/></a></sec:authorize></div>


<!-- 添加/编辑用户 -->
<div id="add_edit_itsop_user_window" class="WSTUO-dialog" title="<fmt:message key="title.user.addOrEditUser" />" style="width:700px; height:auto;padding:3px">

<div class="easyui-layout" fit="true" id="add_edit_itsop_user_layout" style="width:100%;height:430px">
	<div region="west" split="true" title="<fmt:message key="common.basicInfo" />" style="width:350px">
	<div class="lineTableBgDiv">
		<form id="itsop_user_basicInfo_form">
		<input type="hidden" name="userDto.userId" value="0" id="itsop_user_userId" />
		<input type="hidden" name="userDto.companyNo" id="itsop_user_companyNo" />
		<table style="width:100%" class="lineTable" cellspacing="1">
	
		<tr>
			<td><fmt:message key="label.user.loginName" /></td>
			<td><input name="userDto.loginName" id="itsop_user_loginName" class="input easyui-validatebox" required="true" /></td>
		</tr>
		<tr id="itsop_reset_user_password">
			<td><fmt:message key="label.user.loginPassword" /></td>
			<td>[<a id="itsop_reset_user_password_link" ><fmt:message key="common.reset" /></a>]
			[<a id="itsop_edit_pwd" ><fmt:message key="label.edit.user.password" /></a>]</td>
		</tr>
		
		<tr id="itsop_user_password">
			<td><fmt:message key="label.user.loginPassword" /></td>
			<td><input type="password" name="userDto.password" validType="useEnCharPwd" id="itsopUserPassword" class="input easyui-validatebox" required="true"/></td>
		</tr>
		<tr id="itsop_user_confirm_password">
			<td><fmt:message key="label.user.comfirmLoginPassword" /></td>
			<td><input type="password" id="itsopUserPasswordConfirm" class="input easyui-validatebox" validType="equalTo['itsopUserPassword']" required="true"/></td>
		</tr>
		<tr>
			<td><fmt:message key="label.user.userLastName" /></td>
			<td><input name="userDto.firstName" id="itsop_user_firstName" class="input easyui-validatebox" required="true"/></td>
		</tr>
		<tr>
			<td><fmt:message key="label.user.userFirstName" /></td>
			<td><input name="userDto.lastName" id="itsop_user_lastName" class="input easyui-validatebox" required="true"/></td>
		</tr>
		<tr>
			<td><fmt:message key="label.user.userOwnerOrg" /></td>
			<td>
				<input id="itsop_user_orgName" class="input easyui-validatebox" style="cursor:pointer" required="true" readonly/>
				<input type="hidden" name="userDto.orgNo" id="itsop_user_orgNo" />
			</td>
		</tr>
		<tr>
			<td><fmt:message key="label.user.userTitle" /></td>
			<td><input name="userDto.job" id="itsop_user_job" class="input"/></td>
		</tr>
		
		
		<tr>
			<td><fmt:message key="label.user.position" /></td>
			<td><input name="userDto.position" id="itsop_user_position" class="input"/></td>
		</tr>
		
		<tr>
			<td><fmt:message key="label.user.userState" /></td>
			<td>
				<input type="radio" name="userDto.userState" id="itsop_user_userState" value="true" checked />
				<fmt:message key="common.enable" /> 
				<input type="radio"name="userDto.userState" id="itsop_user_userState1" value="false" />
				<fmt:message key="common.disable" />
			</td>
		</tr>
		
		<tr>
			<td><fmt:message key="label.user.sex" /></td>
			<td>
			<input type="radio" name="userDto.sex" id="itsop_user_sex_man" value="true" checked="checked" /><fmt:message key="label.user.man" /> 
			<input type="radio" name="userDto.sex" id="itsop_user_sex_woman" value="false" /><fmt:message key="label.user.woman" /></td>
		</tr>
		
		</table>
		</form>
	</div>
	
	</div>
	<div region="center" title="<fmt:message key="common.detailInfo" />">
		<div id="ITSOP_User_Detail" class="easyui-tabs" fit="true" border="none">
			<div title="<fmt:message key="title.user.contactInfo" />">
				<div class="lineTableBgDiv" style="width:92%" >
					<form id="itsop_user_detail_form">
					<table style="width:100%" class="lineTable" cellspacing="1">
						<tr>
							<td><fmt:message key="label.user.icCard"/></td>
							<td><input name="userDto.icCard" id="itsop_user_icCard" class="input" /></td>
						</tr>
						<tr>
							<td><fmt:message key="label.user.pinyin"/></td>
							<td><input name="userDto.pinyin" id="itsop_user_pinyin" class="input" /></td>
						</tr>
						
						<tr>
							<td><fmt:message key="label.user.birthday"/></td>
							<td><input name="userDto.birthday" id="itsop_user_birthday" class="choose" readonly /></td>
						</tr>
		
		
						<tr>
							<td><fmt:message key="label.user.email"  /></td>
							<td><input name="userDto.email" id="itsop_user_email" class="easyui-validatebox input" validType="email"/></td>
						</tr>
						<tr>
							<td><fmt:message key="label.user.mobile"/></td>
							<td><input name="userDto.moblie" id="itsop_user_moblie" class="easyui-validatebox input" validType="mob"/></td>
						</tr>
						<tr>
							<td><fmt:message key="label.user.phone" /></td>
							<td><input name="userDto.phone" id="itsop_user_phone" class="easyui-validatebox input" validType="phone"/></td>
						</tr>
						
						<tr>
							<td><fmt:message key="label.user.fax"/></td>
							<td><input name="userDto.fax" id="itsop_user_fax" class="input"/></td>
						</tr>
						<tr>
							<td><fmt:message key="label.user.qqOrMsn" /></td>
							<td><input name="userDto.msn" id="itsop_user_msn" class="input"/></td>
						</tr>
						<tr>
							<td><fmt:message key="label.user.workAddress" /></td>
							<td><input name="userDto.officeAddress" id="itsop_user_officeAddress" class="input"/></td>
						</tr>
						<tr>
							<td><fmt:message key="label.user.description" /></td>
							<td><textarea name="userDto.description" id="itsop_user_description"  style="height:50px" class="input"/></textarea></td>	
						</tr>
					
					</table>
					</form>
				</div>
			</div>
			<div title="<fmt:message key="title.user.userRole" />">
				<div id="itsop_user_roles" class="lineTableBgDiv" style="width:92%" >
					<form id="itsop_user_role_form">
					<table style="width:100%" class="lineTable" cellspacing="1"></table>
					</form>
				</div>
			</div>
			
			
		</div>
	</div>
	
	<div region="south" style="height:60px;padding:10px">
		<a class="easyui-linkbutton" icon="icon-save" id="itsop_user_save_btn" ><fmt:message key="common.save" /></a>
	</div>
	
	
</div>
</div>


<!-- 设置用户角色 -->
<div id="set_itsop_user_role_window" title="<fmt:message key="label.user.roleSettings" />" class="WSTUO-dialog" style="width:400px;height:auto">
<form>
	<div class="lineTableBgDiv">
		<table style="width:100%" class="lineTable" cellspacing="1"></table>
		<div style="background:#fff;padding-top:10px;padding-bottom:10px">
		<a class="easyui-linkbutton" plain="false" icon="icon-save" id="set_itsop_user_role_save"><fmt:message key="common.save" /></a>
		</div>
	</div>
</form>

</div>




<!-- 搜索用户 -->
<div id="search_itsop_user_window" class="WSTUO-dialog" title="<fmt:message key="title.user.searchUser" />" style="width:400px;height:auto">
	<form>
		<div class="lineTableBgDiv">
			<table style="width:100%" class="lineTable" cellspacing="1">
				<tr>
					<td><fmt:message key="label.orgSettings.ownerOrg" /></td>
					<td>
						<input id="itsop_user_search_orgName" style="width:96%" readonly />
						<input type="hidden" name="userQueryDto.orgNo" id="itsop_user_search_orgNo" />
					</td>
				</tr>
				<tr>
					<td><fmt:message key="label.user.loginName" /></td>
					<td><input name="userQueryDto.loginName" style="width:96%" /></td>
				</tr>
				<tr>
					<td><fmt:message key="label.user.userLastName" /></td>
					<td><input name="userQueryDto.firstName" style="width:96%" /></td>
					
				</tr>
				<tr>
					<td><fmt:message key="label.user.userFirstName" /></td>
					<td><input name="userQueryDto.lastName" style="width:96%" /></td>
				</tr>
				<tr>
					<td><fmt:message key="label.user.email" /></td>
					<td><input name="userQueryDto.email" style="width:96%" /></td>
				</tr>
				<tr>
					<td colspan="2">
					<input type="hidden" name="userQueryDto.limit" id="limit" value="45" />
					<a class="easyui-linkbutton" icon="icon-search" id="search_itsop_user_search"><fmt:message key="common.search" /></a>
					</td>
				</tr>
			</table>
		</div>
	</form>
</div>









<!-- 搜索外包客户 -->
<div id="Window_Search_ITSOPUserGrid" class="WSTUO-dialog" title='<fmt:message key="title.customer.searchCustomer" />' style="width: 400px; height:auto">

<form>
<div class="lineTableBgDiv">
	<table style="width:100%" class="lineTable" cellspacing="1">
	
	<tr>
		<td><fmt:message key="label.itsop.customerName" /></td>
		<td><input name="itsopUserQueryDTO.orgName" class="input"/></td>
	</tr>
	
	<tr>
		<td><fmt:message key="common.category" /></td>
		<td><select name="itsopUserQueryDTO.typeNo" id="search_ITSOPUser_typeNo" class="input"></select></td>
	</tr>
	
	<tr>
		<td><fmt:message key="label.user.phone" /></td>
		<td><input name="itsopUserQueryDTO.officePhone" class="input"/></td>
	</tr>
	
	<tr>
		<td><fmt:message key="label.e-mail" /></td>
		<td><input name="itsopUserQueryDTO.email" class="input"/></td>
	</tr>
	
	
	<tr>
		<td colspan="2">
		<a id="SEARCH_ITSOPUSER_SUBMIT" class="easyui-linkbutton" icon="icon-search"><fmt:message key="common.search" /></a>
		</td>
	</tr>
</table>
</div>

</form>
</div>

<!-- 导入数据 -->
<div id="importITSOPUserWindow"  class="WSTUO-dialog" title="<fmt:message key="label.dc.import"/>" style="width:400px;height:auto">
	<form>
   <div class="lineTableBgDiv" >
    <table  class="lineTable"  width="100%" cellspacing="1"> 
        <tr>
            <td width="100px">
            <fmt:message key="label.dc.filePath"/>
        
            </td>
            <td>
           	<input type="file" class="easyui-validatebox input" required="true" name="itsopUserDTO.importFile" id="itsopUserImportFile" onchange="checkFileType(this,'csv')"/>
           	
            </td>
        </tr>
		<tr>
		       <td colspan="2" align="center">
		      		 <c:choose>  
						   <c:when test="${lang eq 'en_US'}">
						   		<a href="../importFile/en/ITSOPOrganization.zip" target="_blank">[<fmt:message key="label.csv.template.download"></fmt:message>]</a>
						   </c:when>
						     
						   <c:otherwise>
						   		<a href="../importFile/ITSOPOrganization.zip" target="_blank">[<fmt:message key="label.csv.template.download"></fmt:message>]</a>
						   </c:otherwise>
						</c:choose>
		        </td>
		</tr>
        <tr>
            <td colspan="2">
                <div style="padding-top:8px;">
                	<a id="importITSOPUserBtn" class="easyui-linkbutton" icon="icon-import" onclick="itsm.itsop.ITSOPUserMain.importExcel(0)"><fmt:message key="label.dc.import"/></a>
                </div>
        </td>
        </tr>
    </table>
    </div>
    </form>
</div>

<!-- 新增、编辑外包客户 -->
<div id="Window_Add_Edit_ITSOPUser" class="WSTUO-dialog" title='<fmt:message key="label.add.edit.customer" />' style="width:450px; height:auto">
	<div class="lineTableBgDiv">
		<form>
			<table style="width:100%" class="lineTable" cellspacing="1">
			
			<tr>
				<td><fmt:message key="label.itsop.technicianInCharge" /></td>
				<td><textarea name="itsopUserDTO.technicianNamesStr" id="ITSOPUser_technicianNamesStr" class="easyui-validatebox" required="true" style="width:80%"  ></textarea>
				<a id="add_edit_itsop_technician" class="easyui-linkbutton" plain="true" icon="icon-user" title="<fmt:message key="common.select"/>"></a>
				</td>
			</tr>
			
			<tr>
				<td><fmt:message key="label.itsop.customerName" /></td>
				<td><input name="itsopUserDTO.orgName" id="ITSOPUser_orgName" class="easyui-validatebox input" required="true" validtype="length[1,200]" style="width:80%"/></td>
			</tr>
		
			<tr>
				<td><fmt:message key="common.category" /></td>
				<td><select name="itsopUserDTO.typeNo" id="ITSOPUser_typeNo" class="easyui-validatebox" style="width:82%"></select></td>
			</tr>
			<tr>
				<td><fmt:message key="label.telephone" /></td>
				<td>
				<input name="itsopUserDTO.officePhone" id="ITSOPUser_officePhone" class="easyui-validatebox input" validType="phone" style="width:80%"/>
				</td>
			</tr>
			<tr>
				<td><fmt:message key="label.fax" /></td>
				<td><input name="itsopUserDTO.officeFax" id="ITSOPUser_officeFax" class="easyui-validatebox input" validType="phone" style="width:80%"/></td>
			</tr>
			
			<tr>
				<td><fmt:message key="label.itsop.corporate" /></td>
				<td><input name="itsopUserDTO.corporate" id="ITSOPUser_corporate" class="easyui-validatebox input" style="width:80%"/></td>
			</tr>
			
			<tr>
				<td><fmt:message key="label.itsop.companySize" /></td>
				<td>
					<select name="itsopUserDTO.companySize" id="ITSOPUser_companySize" style="width:80%">
						<option value="1"><fmt:message key="label.itsop.moreThan50" /></option>
						<option value="2"><fmt:message key="label.itsop.moreThan100" /></option>
						<option value="3"><fmt:message key="label.itsop.moreThan200" /></option>
						<option value="4"><fmt:message key="label.itsop.moreThan500" /></option>
						<option value="5"><fmt:message key="label.itsop.moreThan1000" /></option>
					</select>
				</td>
			</tr>
			
			<tr>
				<td><fmt:message key="label.itsop.regNumber" /></td>
				<td><input name="itsopUserDTO.regNumber" id="ITSOPUser_regNumber" class="easyui-validatebox input" style="width:80%"/></td>
			</tr>
			
			<tr>
				<td><fmt:message key="label.itsop.regCapital" /></td>
				<td><input name="itsopUserDTO.regCapital" id="ITSOPUser_regCapital" class="easyui-numberbox" style="width:80%"/>&nbsp;&nbsp;<fmt:message key="label.itsop.wan" /></td>
			</tr>
			<tr>
				<td><fmt:message key="label.e-mail" /></td>
				<td><input name="itsopUserDTO.email" id="ITSOPUser_email" class="easyui-validatebox input" validType="email"  style="width:80%"/>
				</td>
			</tr>
		
			<tr>
				<td><fmt:message key="label.address" /></td>
				<td><input name="itsopUserDTO.address" id="ITSOPUser_address" class="input"  style="width:80%"/></td>
			</tr>
		
		
			<tr>
				<td colspan="2">
				<input name="itsopUserDTO.orgNo" id="ITSOPUser_orgNo" type="hidden" />
				<a id="Add_Edit_ITSOPUser_Submit" class="easyui-linkbutton" icon="icon-save"><fmt:message key="common.save" /></a>
				</td>
			</tr>
	
	</table>
	</form>
	</div>
</div>

<div id="editITSOPUserPwd_win" class="WSTUO-dialog" title="<fmt:message key="label.edit.user.password" />" style="width:380px;height:190px;padding:3px;">
	<form>
	
	<div class="lineTableBgDiv" >
		<table  class="lineTable"  width="100%" cellspacing="1" >
			<tr>
				<td><fmt:message key="label.user.loginName" /></td>
				<td><span id="editITSOPUserPwd_loginName_span"></span>
				<input type="hidden" id="editITSOPUserPwd_loginName" name="editUserPasswordDTO.loginName" />
				<input type="hidden" id="editITSOPUserPwd_oldPassword" name="editUserPasswordDTO.oldPassword" />
				</td>
			</tr>
			<tr>
				<td ><fmt:message key="label.new.password" /></td>
				<td><input type="password" id="editITSOPUserPwd_newPassword" name="editUserPasswordDTO.newPassword" class="easyui-validatebox input" validType="useEnCharPwd"  required="true"/></td>
			</tr>
			<tr>
				<td ><fmt:message key="label.user.comfirmLoginPassword" /></td>
				<td><input type="password" id="editITSOPUserPwd_repeatPassword" class="easyui-validatebox input" validType="equalTo['editITSOPUserPwd_newPassword']" required="true"/></td>
			</tr>
			
			<tr>
				<td colspan="2" align="center" ><a class="easyui-linkbutton" icon="icon-save" id="edit_ITSOPUser_pwd_but" ><fmt:message key="common.update" /></a></td>
			</tr>
		</table>
	</div>
	</form>
</div>
</div>