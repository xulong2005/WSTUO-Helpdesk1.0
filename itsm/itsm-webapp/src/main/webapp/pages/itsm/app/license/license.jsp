<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ include file="../../../language.jsp" %>
<%
String versionTry = com.wstuo.common.config.register.service.RegisterService.versionTry;
request.setAttribute("versionTry",versionTry);
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<script src="../scripts/itsm/app/license/license.js"></script>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title><fmt:message key="title.dashboard.license" />
</title>
<style type="text/css">
.progressbar{
	width: 200px;
	height: 20px;
	float: left;
}
#license_table{
	background-color:#DCE7FB;
}
#lineTableBgDiv table tr{
	height: 30px;
}
</style>

</head>
<body>
<div class="loading" id="license_loading"><img src="../images/icons/loading.gif" /></div>
<div class="content" id="license_content">
		<!-- 许可信息 -->
		<div id="license_info">
			<div class="lineTableBgDiv">
				<table style="width:100%" class="lineTable" cellspacing="1">
				<tr>
					<td><fmt:message key="label.license.category" />:</td>
					<td id="license_category" colspan="2"></td>
					<%-- <td><fmt:message key="label_software_type" />:</td>
					<td id="customerType_value" colspan="2" ></td> --%>
					<td><fmt:message key="label.license.thLicenseNum" />:</td>
					<td id="thLicense_value"></td>
					<td>
						<div class="thLicense_div">
							<div id="thLicense_progressbar" class="progressbar"></div>
							 <span id="thLicense_value_span" style="float: left;line-height: 20px;margin-left: 10px;"></span> 
						</div>
					</td>
				</tr>
				<tr>
					<td><fmt:message key="label.license.startUseDate" />:</td>
					<td id="startDate_value" colspan="2"></td>
					<td><fmt:message key="label.license.endUseDate" />:</td>
					<td id="endDate_value"></td>
					<td>
					</td>
				</tr>
				<tr>
					<td><fmt:message key="label.license.alreadyUsed" />:</td>
					<td id="used_value" colspan="2"></td>
					<td><fmt:message key="label.license.overDay" />:</td>
					<td id="surplus_value"></td>
					<td>
						<div class="surplus_div">
							<div id="surplus_progressbar" class="progressbar"></div>
							<span id="surplus_value_span" style="float: left;line-height: 20px;margin-left: 10px;"></span>
						</div>
					</td>
				</tr>
				<tr>
					
					<td><fmt:message key="label_software_version" />:</td>
					<td colspan="2" id="WSTUO_version"></td>
					<td><span id="concurrent_label"><fmt:message key="label.concurrent.license.number"/>:</span></td>
					<td id="concurrent_value"></td>
					<td>
					</td>
				</tr>
			</table>
			</div>
		</div>
	
	</div>
	
	<!-- 许可列表 -->
		<div id="gridToolbar" style="padding: 0px;display: none;">
			<a class="easyui-linkbutton" id="licenseCode_input" onclick="itsm.app.license.license.openLicenseLoadWin()" plain="true">
			<fmt:message key="button.license.code.import" /></a>
			<a class="easyui-linkbutton" id="upload_license_window" onclick="itsm.app.license.license.openUploadWindow()" plain="true">
			<fmt:message key="button.license.file.import" /></a>
			<c:if test="${versionTry eq 'Official'}">
			<a class="easyui-linkbutton" id="disable_license_window" onclick="itsm.app.license.license.disableLicenseKey()" plain="true">
			<fmt:message key="action.disable.licenseKey" /></a>
			</c:if>
		</div>
		
		<table id='licenseTable'></table>
		<div id='license_pager'></div>
		
		<!-- 许可码输入激活 -->
		<div id="license_code" title="<fmt:message key="title.dashboard.activate.license" />" class="WSTUO-dialog" style="width:600px; height:auto;padding:2px">	
			<form id="license_code_form">
				<p><fmt:message key="title.dashboard.license.code" />:</p>
				<textarea id="licenseCode" name="licenseCoed" style="height:250px;width: 98%;"  class="easyui-validatebox input" required="true" ></textarea><br>
				<a class="easyui-linkbutton" id="licenseCode_submit" icon="icon-ok" plain="true" style="margin-left: 48%;" >
				<fmt:message key="title.dashboard.activate" /></a>
			</form>
		</div>
		
		
		
		<div id="search_dialog" class="WSTUO-dialog" title="<fmt:message key="title.license.search" />" style="padding: 20px;width: 400px;height: auto;" >
			<form id="data_form">
			<input type="hidden" id="licenseid" name="license.Id"/>
			<fmt:message key="label_software_type" />:
				<select name="licensedto.customerType" required="true" id="Type">
					<option value="ITSOPT"><fmt:message key="label.license.itsopt" /></option>
					<option value="INTRANET"><fmt:message key="label.license.intranet" /></option>
				</select><br><br>
			<fmt:message key="label.license.thLicenseNum" />:<input class="easyui-numberbox" name="licensedto.thLicenseNum" id="thNum"/><br><br>
			<fmt:message key="label.license.outCustomerNum" />:<input class="easyui-numberbox" name="licensedto.outCustomerNum" id="outNum"/><br><br>
			<fmt:message key="label.license.startUseDate" />:<input class="input" name="licensedto.startDate" id="MMOA"/><br><br>
			<fmt:message key="label.license.endUseDate" />:<input class="input" name="licensedto.endDate" id="MMMW" /><br><br>
		    <a class="easyui-linkbutton" id="search_license_window_submit" icon="icon-search"><fmt:message key="common.search" /></a>
		    </form>
		</div>
		
		
		<!-- 许可文件导入激活 -->
		<div id="license_file" title="<fmt:message key="button.license.file.import" />" class="WSTUO-dialog" style="width:350px; height:auto;padding:15px">	
			<form id="uploadFile">
				<div style="float: left;"><fmt:message key="label.license.file" />:&nbsp;&nbsp;&nbsp;&nbsp;</div>
				<div style="float: left;">
					<input type="file" class="easyui-validatebox input" required="true" id="importLicenseFile" name="fileName" onchange="checkFileType(this,'txt')"/>
				</div><br><br>
				<a class="easyui-linkbutton" id="uploadFile_license_window_submit" icon="icon-ok">
				<fmt:message key="label.attachment.upload" /></a>
			</form>
		</div>
	
</body>
</html>