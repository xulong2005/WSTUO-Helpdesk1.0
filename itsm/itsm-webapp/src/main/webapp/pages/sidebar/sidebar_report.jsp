<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<li class="nav-header">报表统计</li>
<li><a class=ajax-link href="wstuo/report/requestCatagory.jsp"><span>请求分类统计</span></a></li>
<li><a class="ajax-link" href="wstuo/report/requestStatus.jsp"><span>请求状态统计</span></a></li>
<li><a class="ajax-link" href="wstuo/report/requestSLAStatus.jsp"><span>SLA响应率统计</span></a></li>
<li><a class="ajax-link" href="wstuo/report/requestSLAComplete.jsp"><span>SLA完成率统计</span></a></li>
<li><a class="ajax-link" href="wstuo/report/engineerWorkload.jsp" ><span>技术员工作量统计</span></a></li>
<li><a class="ajax-link" href="wstuo/report/workloadStatusStatistics.jsp" ><span>技术员忙碌程度统计</span></a></li>
<li><a class="ajax-link" href="wstuo/report/tctakeTimeperRequest.jsp" ><span>技术员处理请求耗时</span></a></li>
<li><a class="ajax-link" href="wstuo/report/technicianCostTime.jsp" ><span>技术员工时统计报表</span></a></li>