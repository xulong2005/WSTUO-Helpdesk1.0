<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="com.opensymphony.xwork2.ActionContext" %>    
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ include file="../../language.jsp" %>
<script src="${pageContext.request.contextPath}/js/wstuo/infoMge/infoMge.js?random=<%=new java.util.Date().getTime()%>"></script>
<script>
	var immanageReply="0";
	var immanageDelete="0";
	
	$(document).ready(function(){
		 $("#InstantMessage_loading").hide();
		 $("#InstantMessage_content").show();
	});
	
</script>
<sec:authorize url="/pages/immanage!save.action">
	<script>immanageReply="1";</script>
</sec:authorize>
<sec:authorize url="IMMANAGE_DELETE">
	<script>immanageDelete="1";</script>
</sec:authorize>
			
<div class="tab-pane active" >
	<div class="box-inner">
		<div class="box-header well" >
	        <h2><i class="glyphicon glyphicon-list"></i>&nbsp;消息管理</h2>
        </div>
		<div id="InstantMessage_content" style="padding: 3px;">
			<table id="IMjqGrid" ></table>
			<div id="IMpager"></div>	
			<div id="imgridact" style="display:none"><a id="imjqgridact_details" href="javascript:wstuo.infoMge.infoMge.im_look_win({imid})" title="<fmt:message key="common.detailInfo" />"><i class="glyphicon glyphicon-list"></i></a><span style='display:none'>&nbsp;&nbsp;&nbsp;<sec:authorize url="/pages/immanage!save.action"><a id="imjqgridact_replay" href="javascript:wstuo.infoMge.infoMge.im_reply_imgridact({imid});" title="<fmt:message key="tool.im.replayIM"/>" ><i class="glyphicon glyphicon-edit"></i></a></sec:authorize></span><sec:authorize url="IMMANAGE_DELETE">&nbsp;&nbsp;&nbsp;<a id="imjqgridact_delete" href="javascript:wstuo.infoMge.infoMge.im_delete_inline({imid})" title="<fmt:message key="common.delete" />" ><i class="glyphicon glyphicon-trash"></i></a></sec:authorize></div>
	
			<div id="IMgridToolbar"  style="display: none">
				<div class="panelBar">	
				
					<sec:authorize url="IMAD">
						<a class="btn btn-default btn-xs"  onClick="wstuo.infoMge.infoMge.add_im_openwindow()" title="<fmt:message key="tool.im.writeIM"/>"><i class="glyphicon glyphicon-plus"></i>&nbsp;<fmt:message key="tool.im.writeIM" /></a>
					</sec:authorize> 
				
					<sec:authorize url="EMDL">
						<a class="btn btn-default btn-xs" onClick="wstuo.infoMge.infoMge.im_reply_win()" title="<fmt:message key="tool.im.replayIM"/>"><i class="glyphicon glyphicon-share-alt"></i>&nbsp;<fmt:message key="tool.im.replayIM" /></a>
					</sec:authorize> 
				
					<sec:authorize url="IMMANAGE_DELETE">
						<a class="btn btn-default btn-xs" onClick="wstuo.infoMge.infoMge.im_delete()" title="<fmt:message key="common.delete"/>"><i class="glyphicon glyphicon-trash"></i>&nbsp;<fmt:message key="common.delete" /></a>
					</sec:authorize> 
				
					<a class="btn btn-default btn-xs" onClick="wstuo.infoMge.infoMge.search_openwindow()" title="<fmt:message key="common.search"/>"><i class="glyphicon glyphicon-search"></i>&nbsp;<fmt:message key="common.search" /></a>
					
				</div>
			</div>
		</div>
	</div>	
	
	
	
	<!-- 写消息 -->
	<div id="addMessage" class="WSTUO-dialog" title="<fmt:message key="tool.im.writeIM"/>" style="width:500px;height:auto">
	 <form id="addMessageForm" event="wstuo.infoMge.infoMge.add_instanceMessage">
	 
	 	<input type="hidden" id="IM_sendUserName" name="imDTO.sendUsers" value="${sessionScope.loginUserName}"/>
	 	<input type="hidden" name="imDTO.status" value="0"/>
	 	
			<table style="width:100%" class="lineTable" cellspacing="1">
				<tr>
					<td style="width:20%"><fmt:message key="tool.im.receiveUser"/>&nbsp;<span style="color:red">*</span></td>
					<td style="width:80%">
						<span style="float: left;width: 80%">
							<input id="receiveUsers" style="color:#555;" class="form-control" required="true" readonly/>						
						</span>
						<span style="float: left;margin-top: 5px;" >
						<sec:authorize url="/pages/user!find.action">							
							&nbsp;<a href="#" onclick="cleanIdValue('receiveUsers')" title="<fmt:message key="label.request.clear" />"><i class="glyphicon glyphicon-trash"></i></a>
							&nbsp;<a href="#"  id="write_im_sendUser"  title="<fmt:message key="tool.im.selectUser"/>"><i class="glyphicon glyphicon-user" ></i></a>
				
						</sec:authorize>
						</span>				
					</td>
				</tr>
				<tr>
	            	<td><fmt:message key="tool.im.imTitle"/>&nbsp;<span style="color:red">*</span></td>
	             	<td>
	             		<input id="title" name="imDTO.title" class="form-control" required="true" style="width: 96%"/>
	             	</td>
	        	</tr>
				<tr>
	            	<td><fmt:message key="tool.im.imContent"/>&nbsp;<span style="color:red">*</span></td>
	             	<td>
	             		<textarea id="add_content" name="imDTO.content" class="form-control" required="true"  style="width:96%;height:180px"></textarea>
	           		</td>
	        	</tr>
				<tr>
	            	<td colspan="2" style="height:50px">    
	        		<button type="submit" class="btn btn-primary btn-sm" style="margin-right: 15px;margin-top: 8px;margin-bottom: 10px;"><fmt:message key="tool.im.confirmSend"/></button>					
					</td>
	        	</tr>			
			</table>
		</form>
	</div>
	

	<!-- 回复消息 -->
	<div id="editMessage" class="WSTUO-dialog" title="<fmt:message key="tool.im.replayIM"/>" style="width:500px;height:auto"> 
		<form id="editMessageForm" >
		<table style="width:100%" class="lineTable" cellspacing="1">
		  
    		<tr>
        		<td><fmt:message key="tool.im.receiveUser"/>&nbsp;<span style="color:red">*</span></td>
        		<td>
        		<span style="float: left;width: 90%">
					<input id="rep_toUser" style="color:#555;" class="form-control" required="true" readonly/>
				</span>
				<span style="float: left;margin-top: 5px;" >
				<sec:authorize url="/pages/user!find.action">
					&nbsp;<a href="#"  id="write_im_reply"  title="<fmt:message key="tool.im.selectUser"/>"><i class=" glyphicon glyphicon-user"></i></a>
				</sec:authorize>
				</span>		
        		
        		</td>
    		</tr>
    		<tr>
        		<td><fmt:message key="tool.im.imTitle"/>&nbsp;<span style="color:red">*</span></td>
        		<td>
         			<input id="rep_title" name="imDTO.title" style="width:96%" class="form-control" required="true" />
        		</td>
   	 		</tr>
    		<tr>
        		<td><fmt:message key="tool.im.imContent"/>&nbsp;<span style="color:red">*</span></td>
        		<td style="word-break:break-all; word-wrap:break-all;">
        			<textarea id="rep_content" name="imDTO.content" style="width:96%;height:180px" class="form-control" required="true" ></textarea>
        		</td>
    		</tr>   
    		<tr>
        		<td colspan="2">
        			<input type="hidden"  name="imDTO.senderFullName" value="${fullName}"/>
	 				<input type="hidden" name="imDTO.status" value="0" />
            		<span id="link_im_reply_ok"  class="btn btn-primary btn-sm" style="margin-right: 15px;margin-top: 8px;margin-bottom: 10px;" ><fmt:message key="tool.im.confirmReply"/></span>					
		
        		</td>
    		</tr>
		</table>
		</form>
	</div>
	
	<!--查看消息 -->
	<div id="lookMessage" class="WSTUO-dialog" title="<fmt:message key="tool.im.imDetail"/>" style="width:500px;height:auto"> 
		<form id="lookMessage_form" >
		<input type="hidden"  name="imDTO.senderFullName" value="${fullName}"/>
	 	<input type="hidden" name="imDTO.status" value="0"/>
		<table style="width:100%" class="lineTable" cellspacing="1">	
			<tr>
				<td style="width:20%"><fmt:message key="tool.im.sendUser"/></td>
				<td style="width:80%"><span id="look_im_sendUser"></span></td>
			</tr>  
			<tr>
				<td><fmt:message key="tool.im.sendTime"/></td>
				<td><span id="look_im_sendTime"></span></td>
			</tr>
			<tr>
				<td><fmt:message key="tool.im.imTitle"/></td>
				<td><span id="look_im_title"></span></td>
			</tr>   
			<tr>
				<td><fmt:message key="tool.im.imContent"/></td>
				<td style="word-break:break-all; word-wrap:break-all;"><span id="look_im_content"></span></td>
			</tr>   
			<tbody id="im_save_detail"> 
			<sec:authorize url="/pages/immanage!save.action">  
    		<tr id="im_save_detail_1">
        		<td><fmt:message key="tool.im.receiveUser"/></td>
        		<td>
        			<span style="float: left;">	     				
       				<input id="lookMessage_toUser" style="width:330px;color:#555" class="form-control" required="true"/>
       				</span>
       				<sec:authorize url="/pages/user!find.action">
       				<h5 style="float: left;margin-left: 5px;margin-bottom: 3px;">
					<a href="#" id="write_im_reply2" title="<fmt:message key="tool.im.selectUser" />"><i class="glyphicon glyphicon-user"></i></a>				
					</h5>
       				</sec:authorize>
        		</td>
    		</tr>
    		<tr id="im_save_detail_2">
        		<td><fmt:message key="tool.im.imTitle"/></td>
        		<td>
         			<input id="lookMessage_title" name="imDTO.title" style="width:96%;" class="form-control" maxlength="200" required="true"/>
        		</td>
   	 		</tr>
    		<tr id="im_save_detail_3">
        		<td><fmt:message key="tool.im.imContent"/></td>
        		<td>
        			<textarea id="lookMessage_content" name="imDTO.content" style="width:96%;height:180px" class="form-control" maxlength="200" required="true"></textarea>
        		</td>
    		</tr>   
    		<tr>
        		<td colspan="2" style="padding-top:8px;">          		
            		<span id="read_im_reply_send" class="btn btn-primary btn-sm" style="margin-right: 15px;"><fmt:message key="tool.im.confirmReply"/></span>
        		</td>
    		</tr>
    		</sec:authorize>
    		</tbody>
		</table>
		</form>
	</div>

	<!-- 搜索 -->
	<div id="searchImmanage" class="WSTUO-dialog" title="<fmt:message key="title.im.search"/>" style="width:400px;height:auto;line-height:20px;">
		<form event="wstuo.infoMge.infoMge.search_do">
			<table style="width:100%" class="lineTable" cellspacing="1">
        		<tr>
            		<td><fmt:message key="tool.im.state"/></td>   
            		<td>
            			<select name="manageQueryDTO.status" class="form-control" class="form-control">
            				<option value="">--<fmt:message key="common.pleaseSelect"/>--</option>
            				<option value="0"><fmt:message key="tool.im.state.notRead"/></option>
            				<option value="1"><fmt:message key="tool.im.state.readed"/></option>
            				<option value="2"><fmt:message key="tool.im.state.replayed"/></option>
            			</select>
            		</td>         		
        		</tr>
        		<tr>
       				<td>
       					<fmt:message key="tool.im.imTitle"/>
       				</td>
       				<td><input name="manageQueryDTO.title" id="manageQueryDTO_keyWord" class="form-control" style="width:96%"/></td>
       			</tr>
       			<tr>
       				<td>
       					<fmt:message key="tool.im.imContent"/>
       				</td>
       				<td><input name="manageQueryDTO.content" id="manageQueryDTO_keyWord" class="form-control" style="width:96%"/></td>
       			</tr>
       			
       			<tr>
       				<td><fmt:message key="label.sms.sender"/></td>
       				<td>
       				<span style="float: left;">	
       				<input name="manageQueryDTO.senderFullName" id="manageQueryDTO_sendUsers" class="form-control" style="width:260px;"/>
       				</span>
       				<sec:authorize url="/pages/user!find.action">
       				
       				<h5 style="float: left;margin-left: 5px;">
					<a href="#" id="im_search_sendUsers" title="<fmt:message key="tool.im.selectUser" />"><i class="glyphicon glyphicon-user"></i></a>				
					</h5>
					</sec:authorize></td>
       			</tr>
       			
       			<tr>
       				<td><fmt:message key="tool.mail.sendTime"/></td>
       				<td>
       			
					<span style="float: left;">	
       				<input name="manageQueryDTO.sendSatrt" id="manageQueryDTO_sendSatrt" style="width:120px" class="form-control" validType="date" readonly/>
       				</span>
					<h5 style="float: left;margin-left: 3px;margin-right: 3px;">	
       				<fmt:message key="tool.affiche.to"/>
       				</h5>
					<span style="float: left;">	
       				<input name="manageQueryDTO.sendEnd" id="manageQueryDTO_sendEnd" style="width:120px" class="form-control" validType="DateComparison['manageQueryDTO_sendSatrt']" readonly/>
       				</span>
       				<h5 style="float: left;margin-left: 5px;">
					<a href="#" onclick="cleanIdValue('manageQueryDTO_sendSatrt','manageQueryDTO_sendEnd')" title="<fmt:message key="label.request.clear" />"><i class="glyphicon glyphicon-trash"></i></a>				
					</h5>
       				</td>
       			</tr>
       			
        		<tr>
            		<td colspan="2">               		
						<button type="submit" class="btn btn-primary btn-sm" style="margin-right:15px;margin-top: 8px;margin-bottom: 10px;"><fmt:message key="common.search"/></button>						
       				</td>
        		</tr>
    		</table>
    	</form>
	</div>

</div>	
	
	