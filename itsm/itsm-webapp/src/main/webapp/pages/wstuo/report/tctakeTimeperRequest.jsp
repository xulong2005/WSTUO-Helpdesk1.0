<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ include file="../../language.jsp" %>
<script type="text/javascript">
$(function(){
	var html="";
	var spendtime="";
    var responsesdatetime,closedatetime;
	$.post("report!tctakeTimeperRequest.action",function(data){
		$.each(data,function(i,o){
			if(o[1]==null){
				o[1]="";
			}
			if(o[2]==null){
				responsesdatetime="";
			}else{
				var responsesTime = new Date(o[2].time);
				responsesdatetime = responsesTime.getFullYear()
                + "-"// "年"
                + ((responsesTime.getMonth() + 1) > 10 ? (responsesTime.getMonth() + 1) : "0"
                        + (responsesTime.getMonth() + 1))
                + "-"// "月"
                + (responsesTime.getDate() < 10 ? "0" + responsesTime.getDate() : responsesTime
                        .getDate())
                + " "
                + (responsesTime.getHours() < 10 ? "0" + responsesTime.getHours() : responsesTime
                        .getHours())
                + ":"
                + (responsesTime.getMinutes() < 10 ? "0" + responsesTime.getMinutes() : responsesTime
                        .getMinutes())
                + ":"
                + (responsesTime.getSeconds() < 10 ? "0" + responsesTime.getSeconds() : responsesTime
                        .getSeconds());
			}
            if(o[3]==null){
            	closedatetime="";
            	spendtime="0天0小时0分钟";
			}else{
				
				var closeTime = new Date(o[3].time);
				closedatetime = closeTime.getFullYear()
                + "-"// "年"
                + ((closeTime.getMonth() + 1) > 10 ? (closeTime.getMonth() + 1) : "0"
                        + (closeTime.getMonth() + 1))
                + "-"// "月"
                + (closeTime.getDate() < 10 ? "0" + closeTime.getDate() : closeTime
                        .getDate())
                + " "
                + (closeTime.getHours() < 10 ? "0" + closeTime.getHours() : closeTime
                        .getHours())
                + ":"
                + (closeTime.getMinutes() < 10 ? "0" + closeTime.getMinutes() : closeTime
                        .getMinutes())
                + ":"
                + (closeTime.getSeconds() < 10 ? "0" + closeTime.getSeconds() : closeTime
                        .getSeconds());
			}
            if(o[2]!=null&&o[3]!=null){
            	
            	//alert( o[2].time);
            	 var date3=o[3].time-o[2].time;  //时间差的毫秒数
                 
            	//计算出相差天数
            	var days=Math.floor(date3/(24*3600*1000));

            	//计算出小时数
            	var leave1=date3%(24*3600*1000);   //计算天数后剩余的毫秒数
            	var hours=Math.floor(leave1/(3600*1000));
            	//计算相差分钟数
            	var leave2=leave1%(3600*1000);        //计算小时数后剩余的毫秒数
            	var minutes=Math.floor(leave2/(60*1000));
            	//计算相差秒数
            	var leave3=leave2%(60*1000) ;     //计算分钟数后剩余的毫秒数
            	var seconds=Math.round(leave3/1000) ;
            	spendtime=days+"天 "+hours+"小时 "+minutes+" 分钟"+seconds+" 秒";

            }
			html=html+"<tr><td>"+o[0]+"</td><td>"+o[1]+"</td><td>"+responsesdatetime+"</td><td>"+closedatetime+"</td><td>"+spendtime+"</td></tr>";
		});
		$("#tctakeTimeperRequestinfo tbody").append(html);
	});
	
});

</script>
<div class="row">
		<div class="box col-md-12">
			<div class="box-inner">
				<div class="box-header well" data-original-title="">
					<h2>
						<i class="glyphicon glyphicon-list-alt"></i>&nbsp;技术员处理请求耗时
					</h2>
					<div class="box-icon">
					</div>
				</div>
				<div class="box-content buttons" >
				 <table id="tctakeTimeperRequestinfo" class="table table-striped table-bordered responsive">
                        <thead>
                        <tr>
                            <th>请求标题</th>
                            <th>技术员</th>
                            <th>响应时间</th>
                            <th>完成时间</th>
                            <th>花费时间</th>
                        </tr>
                        </thead>
                        <tbody>
                           
                        </tbody>
                    </table>
				</div>
			</div>
		</div>
 </div>