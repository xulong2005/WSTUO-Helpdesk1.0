<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<%@ include file="../../language.jsp" %>     

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>常用分类includes文件</title>
</head>
<body>

<!-- 请求分类 -->
<div id="request_category_select_window" class="WSTUO-dialog" title="<fmt:message key="common.category" /><fmt:message key="common.select" />" style="max-height:400px;overflow:auto;width:240px;height:auto;padding:3px">
	<div id="request_category_select_tree"></div>
</div>

<!-- 变更分类 -->
<div id="change_category_select_window" class="WSTUO-dialog" title="<fmt:message key="change.changeCategory" />" style="max-height:400px;overflow:auto;width:240px;height:auto;padding:3px">
	<div id="change_category_select_tree"></div>
</div>


<!-- 问题分类 -->
<div id="problem_category_select_window" class="WSTUO-dialog" title="<fmt:message key="setting.problemCategory" />" style="max-height:400px;overflow:auto;width:240px;height:auto;padding:3px">
	<div id="problem_category_select_tree"></div>
</div>

<!-- 知识分类 -->
<div id="knowledge_category_select_window" class="WSTUO-dialog" title="<fmt:message key="knowledge.knowledgeCategory" />" style="max-height:400px;overflow:auto;width:240px;padding:3px;min-height:200px;max-height:400">
	<div id="knowledge_category_select_tree"></div>
</div>

<!-- 知识库服务目录 -->
<div id="knowledge_services_select_window" class="WSTUO-dialog" title="<fmt:message key="setting.serviceDirectory"/>" style="max-height:500px;overflow:auto;width:320px;padding:3px;min-height:300px;max-height:500">
	<div id="knowledge_services_select_tree"></div>
</div>
<!-- 软件 -->
<div id="software_category_select_window" class="WSTUO-dialog" title="<fmt:message key="label_software_category" />" style="max-height:400px;overflow:auto;width:240px;height:auto;padding:3px">
	<div id="software_category_select_tree"></div>
</div>

<!-- 指派组 -->
<div id="index_assignGroup_window" class="WSTUO-dialog" title="<fmt:message key="title.request.chooseGroup" />/<fmt:message key="label.orgSettings.ownerOrg" />" style="max-height:400px;overflow:auto;width:400px;height:auto;padding:3px;">
<div id="index_assignGroup_tree"></div>
</div>

<!-- 选择机构 -->
<div id="index_selectORG_window" class="WSTUO-dialog" title="<fmt:message key="title.user.selectOrg" />" style="max-height:400px;overflow:auto;width:380px;height:auto;padding:3px;">
<div id="index_selectORG_tree"></div>
</div>


<!-- 选择服务机构 -->
<div id="index_selectServiceORG_window" title='<fmt:message key="title.sla.chooseServiceOrg"/>' class="WSTUO-dialog" style="max-height:400px;overflow:auto;width:280px;height:auto;padding:3px;">
	<div id="index_selectServiceORG_window_tree"></div>
</div>


<!-- 发布分类 -->
<div id="release_category_select_window" class="WSTUO-dialog" title="<fmt:message key="setting.releaseCategory" />" style="max-height:400px;overflow:auto;width:240px;height:auto;padding:3px">
	<div id="release_category_select_tree"></div>
</div>



<%--请求分类树多选 --%>
<div id="selectClassifyDirDiv" title='<fmt:message key="setting.requestCategory"/>' class="WSTUO-dialog" style="width:250px;height:270px; padding:10px; line-height:20px;" >
	
	<div id="selectClassifyDirTreeDiv"></div>
	<br/>
	<a class="btn btn-primary btn-sm" plain="true" id="selectClassifyDir_getClassifydNodes" ><fmt:message key="label.sla.confirmChoose"/></a>

</div>

<%--请求所属机构 多选--%>
<div id="selectOrgDirDiv" title='<fmt:message key="label.sla.ownerOrg"/>' class="WSTUO-dialog" style="width:250px;height:270px; padding:10px; line-height:20px;" >
	
	<div id="selectOrgDirTreeDiv"></div>
	<br/>
	<a class="btn btn-primary btn-sm" plain="true" id="selectOrgDir_getOrgdNodes"><fmt:message key="label.sla.confirmChoose"/></a>

</div>
<%--关联服务目录单选--%>
<div id="selectServicesSingleDirDiv" title='<fmt:message key="request.servicesCatalogNavigation"/>' class="WSTUO-dialog" style="width:250px;height:270px; padding:10px; line-height:20px;" >
	<div id="selectServicesSingleDirTreeDiv"></div>
</div>
<%--关联服务目录 多选--%>
<div id="selectServicesDirDiv" title='<fmt:message key="setting.serviceDirectory"/>' class="WSTUO-dialog" style="width:250px;height:270px; padding:10px; line-height:20px;" >
	
	<div id="selectServicesDirTreeDiv"></div>
	<br/>

	<span class="btn btn-primary btn-sm" plain="true" id="selectServicesDir_getServicesdNodes" title="<fmt:message key="label.sla.confirmChoose"/>"><fmt:message key="label.sla.confirmChoose"/></span>

</div>
<%--关联配置项分类单选--%>
<div id="selectCiSingleDirDiv" title='<fmt:message key="report.Configuration.class"/>' class="WSTUO-dialog" style="width:250px;height:270px; padding:10px; line-height:20px;" >
	<div id="selectCiSingleDirTreeDiv"></div>
</div>

<%--配置项分类树  多选--%>
<div id="selectCIsDirDiv" title='<fmt:message key="report.Configuration.class"/>' class="WSTUO-dialog" style="width:250px;height:auto; padding:10px; line-height:20px;" >
	
	<div id="selectCIsDirTreeDiv"></div>
	<br/>

	<a class="btn btn-primary btn-sm" plain="true" id="selectCIsDir_getCIsdNodes" ><fmt:message key="label.sla.confirmChoose"/></a>

</div>

<!-- 配置项所在位置 -->
<div id="ci_loc_select_window" class="WSTUO-dialog" title="<fmt:message key="ci.location" />" style="max-height:400px;overflow:auto;width:240px;padding:3px;min-height:200px;max-height:400">
	<div id="ci_loc_select_tree"></div>
</div>

<%--变更分类  多选--%>
<div id="selectChangeDirDiv" title='<fmt:message key="report.Change.class"/>' class="WSTUO-dialog" style="width:250px;height:570px; padding:10px; line-height:20px;" >
	
	<div id="selectChangeDirTreeDiv"></div>
	<br/>

	<a class="btn btn-primary btn-sm" plain="true" id="selectChangeDir_getChangedNodes"><fmt:message key="label.sla.confirmChoose"/></a>
	
</div>

</body>
</html>