<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ include file="../../language.jsp" %>   
<script src="${pageContext.request.contextPath}/js/wstuo/customForm/customFormMain.js?random=<%=new java.util.Date().getTime()%>"></script>

<div class="row">
    <div class="box col-md-12">
        <div class="box-inner">
            <div class="box-header well" data-original-title="">
                <h2><i class="glyphicon glyphicon-list-alt"></i> 自定义表单列表</h2>
            </div>
            <div class="box-content buttons" id="formCustom_content">
                <table id="formCustomGrid"></table>
				<div id="formCustomGridPager"></div>
            </div>
        </div>
    </div>
</div>



<div id="formCustomGridToolbar" style="display: none">
	<button class="btn btn-default btn-xs" onClick="wstuo.customForm.customFormMain.addEditFormCustom_openwindow()"><i class="glyphicon glyphicon-plus"></i> <fmt:message key="common.add"/></button>
	<button class="btn btn-default btn-xs" id="editFormCustomBtn"><i class="glyphicon glyphicon-edit"></i> <fmt:message key="common.edit"/></button>
	<button class="btn btn-default btn-xs" onclick="wstuo.customForm.customFormMain.deleteFormCustom_aff();"><i class="glyphicon glyphicon-trash"></i> <fmt:message key="common.delete"/></button>
	<button class="btn btn-default btn-xs"  id="organizationGrid_search" onClick="wstuo.customForm.customFormMain.search_openwindow()">
	<i class="glyphicon glyphicon-search"></i> <fmt:message key="common.search" />
	</button>
	<%-- &nbsp;|&nbsp;&nbsp;
	<select id="formCustomTypeSelect">
		<c:if test="${requestHave eq true}">
			<option value="request"><fmt:message key="label.formCustom.request"/></option>
		</c:if>
		<c:if test="${cimHave eq true}">
			<option value="ci"><fmt:message key="label.formCustom.ci"/></option>
		</c:if>
	</select> --%>
</div>

<!-- search formCustom -->
<div id="searchFormCustom" class="WSTUO-dialog" title="<fmt:message key="common.search"/>" style="width:400px;height:auto">
	<form>
	<div class="lineTableBgDiv">
	<table style="width: 100%" class="lineTable" cellspacing="1">
		<tr>
			<td width="100px"><fmt:message key="formCustom.formName"/></td>
			<td><input class="form-control" id="searchName" name="formCustomQueryDTO.formCustomName" style="width:96%" /></td>
		</tr>
		<tr>
			<td colspan="2" style="padding-top: 8px;">
			<button  type="button" id="link_formCustom_search_ok" class="btn btn-primary btn-sm"><fmt:message key="common.save" /></button>
			</td>
		</tr>
	</table>
	</div>
	</form>
</div>

<!-- 选择服务目录以及填写表单名称 -->
<div id="customForm" class="WSTUO-dialog" title='<fmt:message key="formCustom"/>' style="width:400px;height:auto">
	<form id="customForm_form" event="wstuo.customForm.customFormMain.addEditFormCustom_do">
	<div class="lineTableBgDiv">
	<table style="width: 100%" class="lineTable" cellspacing="1">
		<tr>
			<td width="100px"><fmt:message key="formCustom.formName"/>&nbsp;<span style="color:red">*</span></td>
			<td><input id="formCustom_name" class="form-control" style="width:96%" required="true" /></td>
		</tr>
		<tr>
			<td width="100px"><fmt:message key="label.moduleManage.moduleName"/>&nbsp;<span style="color:red">*</span></td>
			<td><select id="formCustom_module" class="form-control" style="width:96%" required="true" onchange="wstuo.customForm.customFormMain.selectFormCustomModule(this.value)">
				<option value="request"><fmt:message key="title.mainTab.request"/></option>
				</select>
			</td>
		</tr>
		<tr>
			<td colspan="2" style="padding-top: 8px;">
			<button type="submit" class="btn btn-primary btn-sm"><fmt:message key="common.save" /></button>
			</td>
		</tr>
	</table>
	</div>
	</form>
</div>
