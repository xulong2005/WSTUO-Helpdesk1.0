<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ include file="../../language.jsp" %>

<script src="../js/wstuo/sysMge/currencyGrid.js?random=<%=new java.util.Date().getTime()%>"></script>

<!-- 树形主面板开始-->
<div class="content" id="currencyMain_content">
<div align="center" class="lineTableBgDiv">
	<div class="box-header well" data-original-title="">
          <h2><i class="glyphicon glyphicon-list"></i>&nbsp;<fmt:message key="label.basicConfig.currencySet" /></h2>
          <div class="box-icon">
            
          </div>
     </div>
		<form id=currency_form>
			<input type="hidden" name="currencyDTO.cno" id="currency_cno" />
			<table style="width:100%; padding-top: 10px;"  cellspacing="1"  class="table table-bordered">
				<tr style="height: 30px;">
					<td width="40%" style="text-align: right;">
						<h5 style="color: black;">		
						<fmt:message key="laber.basicConfig.currencyUnit"/>：
						</h5>
					</td>
					<td align="left" >
						<select name="currencyDTO.sign" id="currency_sign" class="form-control"  style="width: 50%">
							<option value="CNY" selected="selected">人民币(CNY)</option>
							<option value="HKD">港元(HKD)</option>
							<option value="TWD">新台币(TWD)</option>
							<option value="USD">美元(USD)</option>
							<option value="EUR">欧元(EUR)</option>
							<option value="JPY">日元(JPY)</option>
							<option value="GBP">英镑(GBP)</option>
							<option value="CAD">加拿大元(CAD)</option>
							<option value="RUB">俄国卢布(RUB)</option>
							<option value="AUD">澳大利亚元(AUD)</option>
							<option value="KRW">韩圆(KRW)</option>
							<option value="MOP">澳门元(MOP)</option>
							<option value="UZS">乌兹别克斯苏姆(UZS)</option>
							<option value="INR">印度卢比(INR)</option>
							<option value="YER">也门里亚尔(YER)</option>
							<option value="KWD">科威特第纳尔(KWD)</option>
							<option value="KZT">哈萨克斯坦坚戈(KZT)</option>
							<option value="HUF">匈牙利福林(HUF)</option>
							<option value="SCR">塞舌尔卢比(SCR)</option>
							<option value="MUR">毛里求斯卢比(MUR)</option>
							<option value="BGN">保加利亚新列弗(BGN)</option>
							<option value="PYG">巴拉圭瓜拉尼(PYG)</option>
							<option value="COP">哥伦比亚比索(COP)</option>
							<option value="LKR">斯里兰卡卢比(LKR)</option>
							<option value="UYU">乌拉圭比索(UYU)</option>
							<option value="TTD">特立尼达和多巴哥元(TTD)</option>
							<option value="LVL">拉脱维亚拉特(LVL)</option>
							<option value="VND">越南盾(VND)</option>
							<option value="NGN">尼日利亚奈拉(NGN)</option>
							<option value="RSD">塞尔维亚第纳尔(RSD)</option>
							<option value="EGP">埃及镑(EGP)</option>
							<option value="CRC">哥斯达黎加科朗(CRC)</option>
							<option value="AED">阿联酋迪拉姆(AED)</option>
							<option value="UGX">乌干达先令(UGX)</option>
							<option value="EEK">爱沙尼亚克朗(EEK)</option>
							<option value="LAK">老挝基普(LAK)</option>
							<option value="MMK">缅甸缅元(MMK)</option>
							<option value="KHR">柬埔寨瑞尔(KHR)</option>
							<option value="BYR">白俄罗斯卢布(BYR)</option>
							<option value="BZD">伯利兹元(BZD)</option>
							<option value="ETB">埃塞俄比亚比尔(ETB)</option>
							<option value="GTQ">危地马拉格查尔(GTQ)</option>
							<option value="IQD">伊拉克第纳尔(IQD)</option>
							<option value="IRR">伊朗里尔斯(IRR)</option>
							<option value="MYR">马来西亚林吉特(MYR)</option>
							<option value="HRK">克罗地亚库纳(HRK)</option>
							<option value="BRL">巴西雷亚尔(BRL)</option>
							<option value="UAH">乌克兰格里夫尼亚(UAH)</option>
							<option value="THB">泰铢(THB)</option>
							<option value="ZAR">南非兰特(ZAR)</option>
							<option value="PGK">巴布亚新几内亚基那(PGK)</option>
							<option value="CLP">智利比索(CLP)</option>
							<option value="MAD">摩洛哥迪拉姆(MAD)</option>
							<option value="SVC">萨尔瓦多科朗(SVC)</option>
							<option value="PLN">波兰兹罗提(PLN)</option>
							<option value="SGD">新加坡元(SGD)</option>
							<option value="SYP">叙利亚镑(SYP)</option>
							<option value="LBP">黎巴嫩镑(LBP)</option>
							<option value="ANG">荷兰安替兰盾(ANG)</option>
							<option value="TND">突尼斯第纳尔(TND)</option>
							<option value="XOF">非洲金融共同体法郎(XOF)</option>
							<option value="JOD">约旦第纳尔(JOD)</option>
							<option value="IDR">印度尼西亚盾(IDR)</option>
							<option value="KES">肯尼亚先令(KES)</option>
							<option value="SEK">瑞典克朗(SEK)</option>
							<option value="MDL">摩尔多瓦列伊(MDL)</option>
							<option value="QAR">卡塔尔里亚尔(QAR)</option>
							<option value="PKR">巴基斯坦卢比(PKR)</option>
							<option value="RON">罗马尼亚列伊(RON)</option>
							<option value="SKK">斯洛伐克克朗(SKK)</option>
							<option value="HNL">洪都拉斯拉伦皮拉(HNL)</option>
							<option value="VEF">委内瑞拉强势玻利瓦(VEF)</option>
							<option value="BHD">巴林第纳尔(BHD)</option>
							<option value="NPR">尼泊尔卢比(NPR)</option>
							<option value="JMD">牙买加元(JMD)</option>
							<option value="ILS">以色列新谢克尔(ILS)</option>
							<option value="OMR">阿曼里亚尔(OMR)</option>
							<option value="NAD">纳米比亚元(NAD)</option>
							<option value="DZD">阿尔及利亚第纳尔(DZD)</option>
							<option value="ISK">冰岛克朗(ISK)</option>
							<option value="BDT">孟加拉塔卡(BDT)</option>
							<option value="BOB">玻利维亚诺(BOB)</option>
							<option value="BND">文莱元(BND)</option>
							<option value="DKK">丹麦克朗(DKK)</option>
							<option value="ARS">阿根廷比索(ARS)</option>
							<option value="NIO">尼加拉瓜金科多巴(NIO)</option>
							<option value="CZK">捷克克郎(CZK)</option>
							<option value="KYD">开曼元(KYD)</option>
							<option value="FJD">斐济元(FJD)</option>
							<option value="MVR">马尔代夫拉菲亚(MVR)</option>
							<option value="SAR">沙特里亚尔(SAR)</option>
							<option value="PHP">菲律宾比索(PHP)</option>
							<option value="CHF">瑞士法郎(CHF)</option>
							<option value="NOK">挪威克朗(NOK)</option>
							<option value="LTL">立陶宛立特(LTL)</option>
							<option value="TRY">新土耳其里拉(TRY)</option>
							<option value="SLL">塞拉利昂利昂(SLL)</option>
							<option value="MKD">马其顿戴代纳尔(MKD)</option>
							<option value="BWP">博茨瓦纳普拉(BWP)</option>
							<option value="MXN">墨西哥比索(MXN)</option>
							<option value="PEN">秘鲁新索尔(PEN)</option>
							<option value="DOP">多米尼加比索(DOP)</option>
							<option value="NZD">新西兰元(NZD)</option>
							<option value="TZS">坦桑尼亚先令(TZS)</option>
							<option value="ZMK">赞比亚克瓦查(ZMK)</option>
							
						</select>
					</td>
				</tr>
				<tr style="height: 30px;">
					<td  align="center" colspan="2" >
					
						<input type="button" id="currency_sign_linkBut" class="btn btn-primary btn-sm"  value="<fmt:message key="common.save"/>"/>						
					</td>
				</tr>
			</table>
		</form>
</div>
</div>