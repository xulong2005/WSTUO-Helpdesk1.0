<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ include file="../../language.jsp" %>

<script type="text/javascript" src="${pageContext.request.contextPath}/js/basics/htmlformat.js"></script>

<script type="text/javascript">
$(document).ready(function(){
	do_js_beautify("emailAdd_templateContent");
});
$('#smsAdd_templateContent,#imAdd_templateContent,#pushAdd_templateContent,#titleAdd_templateContent,#emailAdd_templateContent').focus(function(obj){
	$("#eventObjAdd").val(obj.target.id);
});

//initCkeditor('emailAdd_templateContent','Simple',function(){});
</script>

 <div class="row">
    <div class="box col-md-12">
        <div class="box-inner">
            <div class="box-header well" data-original-title="">
                <h2><fmt:message key="lable.notice.addNoticeMain"/></h2>
            </div>
            <div class="box-content" style="height: 1000px;">
                <div id="addNotice_layout" >				
				<!-- 编辑通知規則窗口 -->
				<div region="center" fit="true" title="" id="addNotice_center" >
				<input type="button" class="btn btn-primary btn-sm" id="addNotice" onclick="javascript:wstuo.noticeRule.noticeGrid.addNoticeRule()" value="<fmt:message key="common.save" />">
					<!-- 新增通知規則窗口 -->
					<div id="index_add_notice_window" style="width:auto;height:auto;">
						<div style="float: left;width: 75%;" >						
						<div id="noticeMessage" style="width:100%;">
							<b style="margin-top: 10px;margin-left: 10px;"><fmt:message key="common.basicInfo" /></b>	
							<hr style="margin-top: 3px;margin-bottom: 5px;width: 95%;margin-left: 2px;">									
						<form id="addNoticesForm" >
						<table style="width:100%;" cellspacing="1">
							<tr>
								<td style="width: 15%;padding-left: 30px;"><fmt:message key="lable.notice.name" /></td>
								<td style="width: 60%;">
									<input type="text" class="form-control" id="noticeNameAdd" name="noticeRuleDTO.noticeRuleName" required="true" style="width: 90%"/>
								</td>								
							</tr>
							<tr>
								<td style="width: 15%;padding-left: 30px;"><br/><fmt:message key="notice.enableThisRule" /></td>
								<td style="width: 60%;"><br/>
									<label class="radio-inline">
					                    <input type="radio" name="noticeRuleDTO.useStatus" value="true" checked="checked"/><fmt:message key="common.enable" />
					                </label>&nbsp;&nbsp;
									<label class="radio-inline" >
					                   <input type="radio" name="noticeRuleDTO.useStatus" value="false"/><fmt:message key="common.disable" />
					                </label>								
								</td>								
							</tr>
							<tr>
								<td style="width: 15%;padding-left: 30px;"><br/><fmt:message key="notice.module" /></td>
								<td style="width: 60%;"><br/>
									<select id="ruleTypeAdd" name="noticeRuleDTO.module" style="width: 90%" onchange="javascript:wstuo.noticeRule.noticeGrid.changeNoticeObject(this.value,'Add')" class="form-control" required="true">
										<option value=""><fmt:message key="common.pleaseSelect" /></option>
										<c:if test="${requestHave eq true}">
										<option value="request"><fmt:message key="itsm.request" /></option>
										</c:if>
										<c:if test="${problemHave eq true}">
										<option value="problem"><fmt:message key="title.mainTab.problem.manage" /></option>
										</c:if>
										<c:if test="${changeHave eq true}">
										<option value="change"><fmt:message key="title.mainTab.change.manage" /></option>
										</c:if>
										<c:if test="${releaseHave eq true}">
										<option value="release"><fmt:message key="title.mainTab.release.manage" /></option>
										</c:if>
										<c:if test="${cimHave eq true}">
										<option value="cim"><fmt:message key="title.mainTab.cim.manage" /></option>
										</c:if>
									</select>
								</td>
							</tr>
							<tr id="nrtrAdd" style="display: none;" >
								<td style="width: 15%;padding-left: 30px;"><br/><fmt:message key="notice.noticeTarget" /></td>
								<td style="width: 60%;"><br/>
									<label class="checkbox-inline">
										<span id="nrRequestAdd" style="display: none;"><input type="checkbox" name="noticeRuleTypeAdd" value="requester"/><fmt:message key="notice.requesterOrReporter" /></span>
									</label>
									<label class="checkbox-inline">
										<span id="nrTechnicianAdd" style="display: none;"><input type="checkbox" name="noticeRuleTypeAdd" value="technician"/><fmt:message key="notice.assignTechnician" /></span>
									</label>
									<label class="checkbox-inline">
										<span id="nrTechnicalGroupAdd" style="display: none;"><input type="checkbox" name="noticeRuleTypeAdd" value="technicalGroup"/><fmt:message key="notice.assignTechnicalGroup" /></span>
									</label>
									<label class="checkbox-inline">
										<span id="nrOwnerAdd" style="display: none;"><input type="checkbox" name="noticeRuleTypeAdd" value="owner"/><fmt:message key="task.ownerName" /></span>
									</label>
									<label class="checkbox-inline">
										<span id="nrTaskGroupAndTechnicianAdd" style="display: none;"><input type="checkbox" name="noticeRuleTypeAdd" value="taskGroupAndTechnician"/><fmt:message key="notice.taskAssignObjectNotice" /></span>
									</label>
									<label class="checkbox-inline">
										<span id="nrTechnicalGroupLeaderAdd" style="display: none;"><input type="checkbox" name="noticeRuleTypeAdd" value="taskTechnicalGroupLeader"/><fmt:message key="technologyGroupLeader" /></span>
									</label>
									<label class="checkbox-inline">
										<span id="nrCiOwnerAdd" style="display: none;"><input type="checkbox" name="noticeRuleTypeAdd" value="ciOwner"/><fmt:message key="ci.owner" /></span>
									</label>
									<label class="checkbox-inline">
										<span id="nrCiUseAdd" style="display: none;"><input type="checkbox" name="noticeRuleTypeAdd" value="ciUse"/><fmt:message key="ci.use" /></span>
									</label>
									</td>
							</tr>
							<tr>
								<td style="width: 15%;padding-left: 30px;"><br/><fmt:message key="sla.request.NotificationWay" /></td>
								<td style="width: 60%;"><br/>
									<label class="checkbox-inline">
										<input type="checkbox" name="noticeRuleDTO.mailNotice" value="true"/><fmt:message key="lable.notice.emialNoticeMethod" />
									</label>
									<label class="checkbox-inline">	
										<input type="checkbox" name="noticeRuleDTO.smsNotice" value="true"/><fmt:message key="lable.notice.smsNotice" />
									</label>
									<label class="checkbox-inline">	
										<input type="checkbox" name="noticeRuleDTO.imNotice" value="true"/><fmt:message key="notice.IMnotice" />
									</label>
									<%-- <label class="checkbox-inline">	
										<input type="checkbox" name="noticeRuleDTO.pushNotice" value="true"/><fmt:message key="notice.pushNotice"/>
									</label> --%>
								</td>
							</tr>
							<tr>
								<td style="width: 15%;padding-left: 30px;"><br/><fmt:message key="lable.notice.noticeTechnician" /></td>
								<td style="width: 60%;"><br/>
									<input type="text" id="addTechnicianMethodInput" style="width: 90%;float: left;" class="form-control"/>
									<h5><a class="glyphicon glyphicon-user" style="float: left;padding-left: 8px;" href="javascript:wstuo.noticeRule.noticeGrid.selectNoticeUser('#addTechnicianMethodInput')">
									</a></h5>
								</td>
							</tr>
							<tr>
								<td style="width: 15%;padding-left: 30px;"><br/><fmt:message key="lable.notice.noticeAssignMial" /></td>
								<td style="width: 60%;"><br/>
									<input type="text" id="addEmailMethodInput" class="form-control" style="width: 90%;float: left;" />
									<h5><a class="glyphicon glyphicon-user" style="float: left;padding-left: 8px;"  href="javascript:wstuo.noticeRule.noticeGrid.selectNoticeUser('#addEmailMethodInput','email')">
									</a></h5>
								</td>
							</tr>
						</table>
						</form>
						</div>
						<br/>
						<div style="width:100%;">												
	                       <b style="margin-left: 10px;"><fmt:message key="notice.noticeContentModule" /> </b>		                              
	                       <hr style="margin-top: 3px;margin-bottom: 5px;width: 95%;margin-left: 2px;">							
						<div style="margin-bottom: 30px">
							<form id="noticeMailFrom">
								<table style="width:100%;" cellspacing="1">
									<tr >
										<td style="width: 15%;padding-left: 30px;"><fmt:message key="notice.smsModule" /></td> 
										<td style="width: 60%;">
											<input  id="smsAdd_templateContent" name="noticeRuleDTO.smsTemp" style=" width:90%;float: left;" class="form-control" validType="noticeContent" required="true"  value="${noticeRuleDTO.smsTemp }"/>
											<h5><a style="float: left;padding-left: 8px;" class="glyphicon glyphicon-eye-open" plain="true"  id="seeNotice_but" href="javascript:wstuo.noticeRule.noticeGrid.templatePreview('sms','Add')" title="<fmt:message key="lable.notice.see" />">
											</a></h5>
										</td>
							
									</tr>
									<tr>
										<td style="width: 15%;padding-left: 30px;"><br/><fmt:message key="notice.IMModule" /></td> 
										<td style="width: 60%;"><br/>
											<input id="imAdd_templateContent" name="noticeRuleDTO.imTemp" style=" width:90%;float: left;" class="form-control" validType="noticeContent" required="true" value="${noticeRuleDTO.imTemp }"/>
											<h5><a style="float: left;padding-left: 8px;" class="glyphicon glyphicon-eye-open" plain="true" href="javascript:wstuo.noticeRule.noticeGrid.templatePreview('im','Add')" title="<fmt:message key="lable.notice.see" />">
											</a></h5>
										</td>
										
									</tr>
									<%-- <tr>
										<td style="width: 15%;padding-left: 30px;"><br/><fmt:message key="notice.pushNoticeTemp" /></td> 
										<td style="width: 60%;"><br/>
											<input id="pushAdd_templateContent" name="noticeRuleDTO.pushTemp" style=" width:90%;float: left;" class="form-control" validType="noticeContent" required="true" value="${noticeRuleDTO.pushTemp }"/>
											<h5><a style="float: left;padding-left: 8px;" class="glyphicon glyphicon-eye-open" plain="true" href="javascript:wstuo.noticeRule.noticeGrid.templatePreview('push','Add')" title="<fmt:message key="lable.notice.see" />">
											</a></h5>
										</td>
									</tr> --%>
									<tr>								
										<td colspan="2" class="dashboard-list">
											<br/>
											<b style="margin-left: 20px;margin-top: 10px;"><fmt:message key="notice.mailModule" /></b>
				                        	<hr style="margin-left: 10px;margin-top: 3px;width: 95%;margin-left: 2px;">
										</td>
									</tr>
									<tr>
										<td style="width: 15%;padding-left: 30px;"><fmt:message key="common.title" /></td>
										<td style="width: 60%;">
											<input name="noticeRuleDTO.emailTitleTemp" id="titleAdd_templateContent" class="form-control" validType="noticeTitle" required="true"  style=" width:90%;float: left;" value="${noticeRuleDTO.emailTitleTemp }"/>
											<h5><a style="float: left;padding-left: 8px;" class="glyphicon glyphicon-eye-open" plain="true" id="seeNotice_but" href="javascript:wstuo.noticeRule.noticeGrid.templatePreview('title','Add')" title="<fmt:message key="lable.notice.see" />">
											</a></h5>
										</td>
									</tr>
									<tr>
										<td style="width: 15%;padding-left: 30px;"><br/><fmt:message key="common.content" /></td> 
										<td ><br/>
											<div style="width: 90%;float: left;">
											<textarea id="emailAdd_templateContent" name="noticeRuleDTO.emailTemp" style=" width:100%; height: 200px;" >
												${noticeRuleDTO.emailTemp }</textarea>																					
											</div>
											<h5><a style="float: left;padding-left: 8px;" class="glyphicon glyphicon-eye-open" plain="true" id="seeNotice_but" href="javascript:wstuo.noticeRule.noticeGrid.templatePreview('email','Add')" title="<fmt:message key="lable.notice.see" />">
											</a></h5>
										</td>
									</tr>
								</table>
								<br/>
									<input type="button" class="btn btn-primary btn-sm" id="addNotice" onclick="javascript:wstuo.noticeRule.noticeGrid.addNoticeRule()" value="<fmt:message key="common.save" />">
							</form>
						</div>
					</div>	
				
					</div>					
					
					<div style="width: 25%;float: left;margin-top: 20px;">
							<table>
								<tr>
									<td>
										<b><fmt:message key="lable.notice.variableValue" /></b>
										<input type="hidden" id="eventObjAdd">
									</td>
								</tr>
								<tr>
									<td>
									<select id="variableValueAdd" multiple="true" style="width:160px;;height:370px" onclick="wstuo.noticeRule.noticeGrid.getValue('Add')" class="form-control">
									</select>
									<label id="labelValueAdd"></label>
								</td>
								</tr>						
							</table>						
						</div>
					</div>
				</div>
				
				<div id="historyRecord_winAdd" class="WSTUO-dialog" title="<fmt:message key="label.noticeHistoryRecordHtml" />" style="width:auto;height:auto">
					<div style="margin-left: 10px;margin-top: 10px;">
						<span><fmt:message key="label.noticeHistoryRecord" /></span>
					</div>
					<div>
						<textarea id="historyRecord_htmlAdd" class="autogrow" style="width: 400px;height: 150px;"></textarea>
					</div>
				</div>
				<div id="templatePreviewDivAdd" class="WSTUO-dialog" title="<fmt:message key="notice.seeModule" />" style="width:auto;height:auto">

				</div>	
				</div>

            </div>
        </div>
    </div>
</div>