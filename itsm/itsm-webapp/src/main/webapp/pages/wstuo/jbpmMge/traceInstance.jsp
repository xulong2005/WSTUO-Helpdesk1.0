<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ include file="../../language.jsp"%>

<div class="tab-pane active" >
	<c:if test="${!empty param.processDefinitionId || !empty processDefinitionId}">
		<input type="hidden" id="id" name="id" value="${param.processDefinitionId}" />
		<c:if test="${
			activityCoordinatesDTO.x eq 0 
			and activityCoordinatesDTO.y eq 0 
			and activityCoordinatesDTO.width eq 0 
			and activityCoordinatesDTO.height eq 0 }">
			<div class="traceInstance" style="width: 99%;text-align: center;">
				<fmt:message key="title.jbppm.processEnd" />
			</div>
		</c:if>
		<c:if test="${! empty activityCoordinatesDTO or ! empty activityHistoryCoordinatesDTO }">
			<center>
				<div style="margin-top:5px">
				<span style="border:1px dashed #FF6600;border-radius: 4px;text-align: center;color: red;padding: 0px 10px 0px 10px;" >
				<fmt:message key="label.flow.chart.hisActivityCoordinatesTip" />
				</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				<span style="border:1px solid #FF6600;border-radius: 4px;text-align: center;color: red;padding: 0px 10px 0px 10px;">
				<fmt:message key="label.flow.chart.activityCoordinatesTip" />
				</span></div>
			</center>
		</c:if>
		<div style="boverflow: auto; height:90%;position:relative;top:3px;bottom: 3px" >
			<!-- 当前坐标 -->
			<c:if test="${! empty activityCoordinatesDTO }">
				<div style="position:absolute;border:1px solid #FF6600;z-index:999px;border-radius: 4px;
					left:${activityCoordinatesDTO.x}px;top:${activityCoordinatesDTO.y}px;
					width:${activityCoordinatesDTO.width}px;height:${activityCoordinatesDTO.height}px;">
				</div>
			</c:if>
			
			<!-- 历史坐标 -->
			<c:if test="${! empty activityHistoryCoordinatesDTO }">
			<c:forEach items="${activityHistoryCoordinatesDTO }" var="hisacDTO"> 
			<div style="position:absolute;border:1px dashed #FF6600;z-index:999px;border-radius: 4px;
				left:${hisacDTO.x }px;top:${hisacDTO.y }px;
				width:${hisacDTO.width }px;
				height:${hisacDTO.height}px;"></div>
			</c:forEach>
			</c:if>
			<!-- 历史坐标 -->
			<img style="" id="flow_img" src="jbpm!getProcessImage.action?processDefinitionId=${empty processDefinitionId ? param.processDefinitionId : processDefinitionId}" />
		</div></c:if>
</div>


