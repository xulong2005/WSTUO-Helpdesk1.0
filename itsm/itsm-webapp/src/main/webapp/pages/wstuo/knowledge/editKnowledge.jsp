<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="../../language.jsp" %>

<script>var id='${param.id}';var editkw_opt='${param.opt}';</script>
<script src="${pageContext.request.contextPath}/js/wstuo/knowledge/editKnowledge.js?random=<%=new java.util.Date().getTime()%>"></script>

<form id="editKnowledgeForm" event="wstuo.knowledge.editKnowledge.checkKnowledgeValidate">
<div class="row" id="editKnowledge_contentPanel">
   	<div class="box col-md-12">
		<div class="box-inner">
			<div class="box-header well" data-original-title="">
				<h2>
					<i class="glyphicon glyphicon-pencil"></i> 编辑知识
				</h2>
				<div class="box-icon"></div>
			</div>
			<div class="box-content">
					<button  class="btn btn-default btn-xs" style="margin-right:15px"  ><i class="glyphicon glyphicon-floppy-disk"></i>&nbsp;<fmt:message key="common.save" /></button>
		            <button class="btn btn-default btn-xs"  style="margin-right:15px" id="edit_backToKnowledgeIndex" ><i class=" glyphicon glyphicon-share-alt"></i>&nbsp;<fmt:message key="common.returnToList" /></button>
					<sec:authorize url="/pages/knowledgeInfo!passKW.action"><input type="checkbox"  id="editKnowledge_knowledgeStatus_check">&nbsp;&nbsp;<label><fmt:message key="label_knowledge_approved" /></label>
              		</sec:authorize><input type="hidden"  name="knowledgeDto.knowledgeStatus" id="editKnowledge_knowledgeStatus">
			
					<input type="hidden" name="knowledgeDto.kid" id="editKnowledge_kid">
					<input type="hidden"  id="editKnowledge_aid">
					<input type="hidden" name="knowledgeDto.addTime" id="editKnowledge_addTime">
		           <div class="lineTableBgDiv" style="width: 98%">
				<table style="width:100%" class="lineTable" cellspacing="1">
		            <tr>
		              <td style="width:10%;padding-left: 15px;"><fmt:message key="knowledge.label.knowledgeTitle" />&nbsp;<span style="color:red">*</span></td>
		              <td><input attrtype='String' name="knowledgeDto.title" id="editKnowledge_title" class="form-control" required="true" /></td>
		              <td style="width:10%;padding-left: 15px;">
		                <fmt:message key="knowledge.label.knowledgeSort" />&nbsp;<span style="color:red">*</span>
		              </td>
		              <td >
		                <input type=hidden name="knowledgeDto.categoryNo" id="editKnowledge_categoryNo">
		              	<input id="editKnowledge_Category"  class="form-control" readonly/>
		              	<div id="editKnowledge_CategoryDiv"  class="easyui-panel" closed="true"  style="position:absolute;float:left;width:260px;height:300px;background-color:#FFF;display: none;"></div>
		              </td>
		            </tr>
		            <tr>
		              <td style="width:10%;padding-left: 15px;"><fmt:message key="label.keywords" /></td>
		              <td><input attrtype='String' id="editKnowledge_keyWords" name="knowledgeDto.keyWords"  class="form-control" /></td>
		             <td style="width:10%;padding-left: 15px;" ><fmt:message key="label.knowledge.relatedService" /></td> <td>
		                  <div style="padding-top:3px;">
			               <a href="#" id="editKnowledge_service_dir_items_name"><fmt:message key="lable.knowled.serviceTitle"/></a>
			              </div>
			              <div style="color:#ff0000;" id="editKnowledge_service_name">
		                   <input type=hidden name='knowledgeDto.knowledServiceNo' value="0">
		                  </div>
		              </td>
		            </tr>
		             <tr>
		                <td colspan="4"><br/>
		                  <textarea id="editKnowledgeCon" style="width:100%;height:440px"></textarea>
		                </td>
		             </tr>
		          </table>
		          </div>
		          	<textarea id="editKnowledge_Content" name="knowledgeDto.content" style="display:none"></textarea>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="row">
   				<div class="box col-md-12">
   				    <div class="box-inner" >	
						<div class="box-content" >
	   				 <ul class="nav nav-tabs" id="myTab">
	                    <li><a href="#attachment"><fmt:message key="common.attachment" /></a></li>
	                </ul>
	
	                <div id="myTabContent" class="tab-content">
						<div class="tab-pane" id="attachment">
							 <input type="hidden" name="knowledgeDto.attachmentStr" id="editKnowledge_attachments"/>
		                  <div id="editKnowledge_oldAttachments" style="line-height:25px;color:#555;"></div>
						  <div class="form-group" >
			                 <input type="file" name="filedata" id="editKnowledge_uploadAttachments" multiple class="file" data-overwrite-initial="false" data-min-file-count="1">
			              </div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div></form>