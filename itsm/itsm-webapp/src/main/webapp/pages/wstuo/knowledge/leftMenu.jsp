<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ include file="../../language.jsp" %>

<script>
setTimeout(function(){
	$.parser.parse($('#lf_kw_knowledge_fullSearch'));
	setTimeout(function(){  
		common.knowledge.leftMenu.showHotKnowledges();
		common.knowledge.leftMenu.showNewKnowledges();
		common.knowledge.leftMenu.countKnowledge('share','#share_kw');
		common.knowledge.leftMenu.countKnowledge('my','#share_my');
		common.knowledge.leftMenu.countKnowledge('myap','#share_myapp');
		common.knowledge.leftMenu.countKnowledge('allap','#share_allapp');
		common.knowledge.leftMenu.countKnowledge('myapf','#share_myappf');
		common.knowledge.leftMenu.countKnowledge('allapf','#share_allappf');
		endLoading();
		
	},500);  
},10);

</script>


<!-- 搜索知识库
<div class="musp"><fmt:message key="label.compass.searchKnowledge" /></div> -->

<!-- 全文搜索 -->
<sec:authorize url="FNULLTEXTSEARCH_RES">
<div class="musp"><fmt:message key="label.fullsearch.fullsearch" /></div>


<table width="96%" border="0" align="center" style="margin-top:5px;margin-bottom:5px">
<tr>
<td>
<sec:authorize url="/pages/request!findRequestPager.action">
<input type="radio" value="request" name="lf_knowledge_fullSearch_type"><fmt:message key="title.mainTab.request" />
</sec:authorize>
<c:if test="${problemHave eq true}">
<sec:authorize url="/pages/problem!findProblemPager.action">
<input type="radio" value="problem" name="lf_knowledge_fullSearch_type"><fmt:message key="title.mainTab.problem" />
</sec:authorize>
</c:if>
<c:if test="${changeHave eq true}">
<sec:authorize url="/pages/change!findChangePager.action">
<input type="radio" value="change" name="lf_knowledge_fullSearch_type"><fmt:message key="title.mainTab.change" />
</sec:authorize>
</c:if>
<sec:authorize url="KNOWLEGEINFO_VIEWKNOWLEGE">
<input type="radio" value="knowledge" name="lf_knowledge_fullSearch_type" checked><fmt:message key="knowledge.knowledge" />
</sec:authorize>

</td>
</tr>

<tr> 
<td><input class="input" id="lf_knowledge_fullSearchKeyWord" onfocus="javascript:common.compass.fullSearch.setSearchFlag()" onblur="javascript:common.compass.fullSearch.onblurEvent()"></td>
</tr>

<tr>
<td>
<a class="easyui-linkbutton" id="lf_knowledge_fullSearch_submit" onclick="common.compass.fullSearch.setFullsearchValue('#lf_knowledge_fullSearchKeyWord','lf_knowledge_fullSearch_type')" style="border:#99bbe8 1px solid" plain="true" icon="icon-search"><fmt:message key="common.search" /></a></td>
</tr>
</table>

</sec:authorize>

<!--<div id="lf_kw_knowledge_fullSearch">
<table width="96%" border="0" align="center" style="margin-top:5px;margin-bottom:5px">

	<tr>
		<td><input class="input" id="lf_kw_knowledge_fullSearchKeyWord" onfocus="javascript:common.compass.fullSearch.setFullSearchId('#lf_kw_knowledge_fullSearchKeyWord')"></td>
	</tr>
	<tr>
		<td>
		<a class="easyui-linkbutton" onclick="common.compass.fullSearch.searchKnowledge('#lf_kw_knowledge_fullSearchKeyWord')" style="border:#99bbe8 1px solid" plain="true" icon="icon-search"><fmt:message key="common.search" /></a></td>
	</tr>
</table>

</div>
-->

<div class="musp" ><fmt:message key="label_knowledge_menu" /></div>
<div style="padding:10px;line-height:20px;color:#ff0000">
<sec:authorize url="pages/knowledgeInfo!sharedKnowledge.action">
<a id="shared_kw"><fmt:message key="label_knowledge_sharedKnowledge" /></a>&nbsp;&nbsp;<span id="share_kw"></span><br/>
</sec:authorize>
<!-- 我发布的知识 -->
<sec:authorize url="KNOWLEDGEINFO_MYKNOWLEDGE">
<a id="my_kw"><fmt:message key="label_knowledge_myKnowledge" /></a>&nbsp;&nbsp;<span id="share_my"></span><br/>
</sec:authorize>
<!-- 我的待审的知识 -->
<sec:authorize url="KNOWLEDGEINFO_MYSTAYCHECKKNOWLEDGE">
<a id="my_ap_kw"><fmt:message key="label_knowledge_myApKnowledge" /></a>&nbsp;&nbsp;<span id="share_myapp"></span><br/>
</sec:authorize>
<sec:authorize url="knowledge_menu_allStayCheckKnowledge">
<a id="all_ap_kw"><fmt:message key="label_knowledge_allApKnowledge" /></a>&nbsp;&nbsp;<span id="share_allapp"></span><br/>
</sec:authorize>
<!-- 我审核未通过的知识 -->
<sec:authorize url="KNOWLEDGEINFO_MYNONCHECKKNOWLEDGE">
<a id="my_apf_kw"><fmt:message key="label_knowledge_page_myuntreated"></fmt:message></a>&nbsp;&nbsp;<span id="share_myappf"></span><br/>
</sec:authorize>
<sec:authorize url="pages/knowledgeInfo!alluntreated.action">
<a id="all_apf_kw"><fmt:message key="label_knowledge_page_alluntreated"></fmt:message></a>&nbsp;&nbsp;<span id="share_allappf"></span><br/>
</sec:authorize>



<script type="text/javascript">


function showKw(_param){
	basics.tab.tabUtils.refreshTab(i18n['title_request_knowledgeGrid'],'${pageContext.request.contextPath}/pages/common/knowledge/knowledgeMain.jsp'+_param);
}
$('#request_fastCreate_companyNo').val(companyNo);
$(document).ready(function(){


	$('#shared_kw').click(function(){showKw('?opt=share');});
	$('#my_kw').click(function(){showKw('?opt=my');});
	$('#my_ap_kw').click(function(){showKw('?opt=myap');});
	$('#all_ap_kw').click(function(){showKw('?opt=allap');});

	$('#my_apf_kw').click(function(){showKw('?opt=myapf');});
	$('#all_apf_kw').click(function(){showKw('?opt=allapf');});
	
	$('#lf_rq_knowledge_fullSearch_submit').linkbutton();
	$('#lf_knowledge_fullSearch_submit').linkbutton();
	$('#lf_knowledge_fast_create_request_submit').linkbutton();
	
	$('#fastCreateEtitle').validatebox({'required':'true','validType':'length[1,200]'});
	$('#fastCreateEdesc').validatebox({'required':'true','validType':'length[1,1200]'});

	
	common.compass.fullSearch.setEnterFullSearch('#lf_knowledge_fullSearchKeyWord','lf_knowledge_fullSearch_type');
	itsm.request.requestStats.loadRequestCount();

});




</script>

</div>


<sec:authorize url="knowledge_menu_hostKnowledge">
<div class="musp" ><fmt:message key="knowledge.hotKnowledge" /></div>
<div id="hotKnowledge" style="padding:10px; line-height:20px">
</div>
</sec:authorize>
<sec:authorize url="knowledge_menu_newKnowledge">
<div class="musp"><fmt:message key="knowledge.topKnowledge" /></div>
<div id="newKnowledge" style="padding:10px; line-height:20px">
</div>
</sec:authorize>