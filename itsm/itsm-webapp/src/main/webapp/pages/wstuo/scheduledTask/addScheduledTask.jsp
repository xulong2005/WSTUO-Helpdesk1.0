<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="com.opensymphony.xwork2.ActionContext" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ include file="../../language.jsp" %>
<script src="${pageContext.request.contextPath}/js/wstuo/scheduledTask/addScheduledTask.js?random=<%=new java.util.Date().getTime()%>"></script>
 <!--    
<div class="loading" id="addScheduledTask_loading"><img src="../images/icons/loading.gif" /></div>
 -->
 <div id="addScheduledTask_panel" class="content" border="none" fit="true" style="height: 99%">
	
	<div class="easyui-layout" fit="true" id="addScheduledTask_layout">
		<div region="north" border="false" class="panelTop">
			<a id="saveScheduledTaskBtn" class="easyui-linkbutton" plain="true" icon="icon-save" style="margin-right:15px" title="<fmt:message key="common.save" />"></a>
			<a id="add_scheduledTask_backList" class="easyui-linkbutton" plain="true" icon="icon-undo" style="margin-right:15px" title="<fmt:message key="common.returnToList" />"></a>
		</div>
		<div region="west" split="true" title="<fmt:message key="common.basicInfo" />"style="width:450px;height: auto;">
			<form id="addScheduledTaskForm">
			<input type="hidden" name="scheduledTaskDTO.beanId" value="createRequestJob"  />
			<input type="hidden" name="scheduledTaskDTO.scheduledTaskType" value="request"  />
			<div class="lineTableBgDiv">
				<table style="width:100%" cellspacing="1">
				<!-- 所属客户  -->
				<tr <c:if test="${versionType!='ITSOP'}"><c:out value="style=display:none"></c:out></c:if>>
					<td class="lineTableTd"><fmt:message key="label.belongs.client" />&nbsp;<span style="color:red">*</span></td>
					<td class="lineTableTd">
						<input type="hidden" name="scheduledTaskDTO.requestDTO.companyNo" id="add_scheduledTask_companyNo" />
						<input id="add_scheduledTask_companyName" name="scheduledTaskDTO.requestDTO.companyName" class="easyui-validatebox choose" required="true"  readonly />
					</td>
				</tr>
				<tr>	
					<td class="lineTableTd" width="20%"><fmt:message key="setting.requestCategory" />&nbsp;<span style="color:red">*</span></td>
						<td class="lineTableTd">
						<input type="hidden" name="scheduledTaskDTO.requestDTO.ecategoryNo" id="add_scheduledTask_categoryNo" />
						<input name="scheduledTaskDTO.requestDTO.ecategoryName"  id="add_scheduledTask_categoryName" onclick="common.tools.scheduledTask.addScheduledTask.selectRequestCategory('scheduledTask_add_eavAttributet','requestDTO')" class="easyui-validatebox choose" required="true" validType="length[1,200]"  readonly/>
					</td>
				</tr>
				
				
				<tr>
					<td class="lineTableTd"><fmt:message key="common.title" />&nbsp;<span style="color:red">*</span></td>
					<td class="lineTableTd"><input name="scheduledTaskDTO.requestDTO.etitle" id="add_scheduledTask_etitle"  class="easyui-validatebox input" validType="length[1,200]" required="true" /></td>
				</tr>

				<tr>
					<td class="lineTableTd"><fmt:message key="common.content" />&nbsp;<span style="color:red">*</span></td>
					<td class="lineTableTd">
					<textarea style="width: 98%;height: 200px;" name="scheduledTaskDTO.requestDTO.edesc" id="add_scheduledTask_edesc" ></textarea>
					</td>
				</tr>
				<tr>
					<td class="lineTableTd"><fmt:message key="label.request.requestUser" />&nbsp;<span style="color:red">*</span></td>
					<td class="lineTableTd">
					<input  id="add_scheduledTask_createdName" class="easyui-validatebox input"  name="scheduledTaskDTO.requestDTO.createdByName" value="${loginUserName}" required="true" style="color:#555;" />
					<input  id="add_scheduledTask_createdNo" type="hidden" name="scheduledTaskDTO.requestDTO.createdByNo" /></td>
				</tr>
<%-- 				<c:if test="${cimHave eq true}">
				<tr>
					<td class="lineTableTd"><fmt:message key="label.request.withAssets" /></td>
					<td class="lineTableTd">
						<input class="choose" style="width: 85%" id="add_scheduledTask_ref_ciname" name="scheduledTaskDTO.requestDTO.ciName" />
						<input  id="add_scheduledTask_ref_ciid" type="hidden" name="scheduledTaskDTO.requestDTO.ciId" />
						<a onclick="cleanIdValue('add_scheduledTask_ref_ciid','add_scheduledTask_ref_ciname')"><fmt:message key="label.request.clear" /></a>
					</td>
				</tr>
				</c:if> --%>
<%-- 				<tr>
					<td class="lineTableTd" width="20%"><fmt:message key="label.ci.ciServiceDir" /></td>
					<td class="lineTableTd">
						<input class="choose" style="width: 85%" name="scheduledTaskDTO.requestDTO.requestServiceDirName" id="add_scheduledTask_ref_requestServiceDirName" readonly="" required="true" />
						<input  id="add_scheduledTask_ref_requestServiceDirNo" type="hidden" name="scheduledTaskDTO.requestDTO.requestServiceDirNo" />
						<a onclick="cleanIdValue('add_scheduledTask_ref_requestServiceDirNo','add_scheduledTask_ref_requestServiceDirName')"><fmt:message key="label.request.clear" /></a>
					</td>
				</tr> --%>
				<tr>	
					<td class="lineTableTd"><fmt:message key="label.request.form" /></td>
					<td class="lineTableTd">
					<select id="add_scheduledTask_imode" name="scheduledTaskDTO.requestDTO.imodeNo" class="input"></select>
					</td>
				</tr>
				<tr>
					<td class="lineTableTd"><fmt:message key="label.request.priority" /></td>
					<td class="lineTableTd">
						<select id="add_scheduledTask_priority" name="scheduledTaskDTO.requestDTO.priorityNo" class="input"></select>
					</td>
				</tr>
				<tr>
					<td class="lineTableTd"><fmt:message key="label.request.complexity" /></td>
					<td class="lineTableTd">
					<select id="add_scheduledTask_level" name="scheduledTaskDTO.requestDTO.levelNo" class="input"></select>
					</td>
				</tr>
				<tr>
					<td class="lineTableTd"><fmt:message key="label.request.effect" /></td>
					<td class="lineTableTd">
						<select id="add_scheduledTask_effectRange" name="scheduledTaskDTO.requestDTO.effectRangeNo" class="input"></select>
					</td>
				</tr>
				<tr>	
					<td class="lineTableTd"><fmt:message key="label.request.seriousness" /></td>
					<td class="lineTableTd">
						<select id="add_scheduledTask_seriousness" name="scheduledTaskDTO.requestDTO.seriousnessNo" class="input"></select>
					</td>
				</tr>

			</table>
			
			</div>
			</form>
			
			<%--高度占位 --%>
			<div style="height: 30px"></div>
			
		</div>
		
		<!-- 扩展信息 start -->
		<div region="center" title="<fmt:message key="common.detailInfo" />"  style="background:#eee;">
			<div class="easyui-tabs" fit="true" border="false" id="add_scheduledTask">
			<%-- 关联配置项 --%>
				<c:if test="${cimHave eq true}">
				
				<div title="<fmt:message key="ci.relatedCI" />" style="padding: 2px;">
				<form>
					<div  id="scheduledTaskRelatedCIShow"  class="hisdiv" >
						<table class="histable" style="width:100%" cellspacing="1">
							<thead>
							<tr>
								<td colspan="5" style="text-align:left">
									<a id="add_scheduledTask__ref_ci_btn" class="easyui-linkbutton" plain="true" icon="icon-add" title="<fmt:message key="common.add" />"></a>
								</td>
							</tr>
							<tr height="20px">
								<th align="center"><fmt:message key="lable.ci.assetNo" /></th>
								<th align="center"><fmt:message key="label.name"/></th>
								<th align="center"><fmt:message key="common.category" /></th>
								<th align="center"><fmt:message key="common.state" /></th>
								<th align="center"><fmt:message key="ci.operateItems" /></th>
							</tr>
							</thead>
							<tbody></tbody>
						</table>
					</div>
					</form>
				</div>
				
				</c:if>
			<div title='<fmt:message key="label.ci.ciServiceDir" />'>
				<form>
					<div class="hisdiv">
						<table class="histable" id="add_scheduledTask_serviceDirectory" style="width:100%" cellspacing="1">
						<thead>
						<tr>
							<td colspan="3" style="text-align:left">
								<a id="add_scheduledTask_service_add" class="easyui-linkbutton" plain="true" icon="icon-add" title="<fmt:message key="common.add" />"></a>
							</td>
						</tr>
						<tr height="20px">
							<%-- <th align="center"><fmt:message key="common.id"/></th> --%>
							<th align="center"><fmt:message key="relevance.service.name"/> </th>
							<th align="center"><fmt:message key="label.service.scores"/> </th>
							<th align="center"><fmt:message key="label.rule.operation"/> </th>
						</tr>
						</thead>
						<!-- 服务目录显示 -->
						<tbody id="add_scheduledTask_serviceDirectory_tbody">
							
						</tbody>
						
						</table>
					</div>
					</form>
				</div>
				<div title="<fmt:message key="config.extendedInfo"/>" style="overflow:hidden;">
				<form id="scheduledTask_add_eavAttributet_form">
					<div class="hisdiv" id="scheduledTask_add_eavAttributet">
						<table style="width:100%" class="histable" cellspacing="1">
							<thead>
								<tr>
									<!-- <th><fmt:message key="common.attributeName" /></th><th><fmt:message key="common.attributeValue" /></th> -->
								</tr>
							</thead>
							<tbody>
								<tr><td colspan="2" style="color: red"><b><fmt:message key="label.notExtendedInfo.Request" /></b></td></tr>
							</tbody>
              			</table>
                	</div>
                	</form>
				</div>
			
			
				<div title="<fmt:message key="common.attachment" />" style="padding:3px;">
				<form>
					<div class="lineTableBgDiv diyLinkbutton" id="add_scheduledTask_attachment_div" >
							<input type="hidden" name="scheduledTaskDTO.requestDTO.attachmentStr" id="add_scheduledTask_attachmentStr"/>
							<table style="width:100%" cellspacing="1" class="lineTable">
								<tr>
									<td> 
									<div style="color:#ff0000;line-height:28px"><fmt:message key="msg.attachment.maxsize" /></div>
										<input type="file"  name="filedata" id="add_scheduledTask_file">
										<div style="height:5px"></div>
										<a class="easyui-linkbutton" icon="icon-upload" href="javascript:if($('#add_scheduledTask_fileQueue').html()!='')$('#add_scheduledTask_file').uploadifyUpload();else msgShow(i18n['msg_add_attachments'],'show');" ><fmt:message key="label.attachment.upload" /></a>
									</td>
								</tr>
								<tr>
									<td>
										<div style="padding-top:3px">
							              <div id="add_scheduledTask_success_attachment" style="line-height:25px;color:#555"></div>
							            </div>
									</td>
								</tr>
							</table>
					</div>
					</form>
				</div>
				<div title="<fmt:message key="title.scheduled.time.settings" />" style="padding:0px;" id="addScheduledTimeSettings">
				<form>
					<div class="lineTableBgDiv">
						<table style="width:100%" class="lineTable" cellspacing="1">
							<tr>
								<td>
									<div class="lineTableBgDiv" class="lineTable" cellspacing="1">
									<table style="width:100%">
										<tr>
											<td>
												<input type="radio" name="scheduledTaskDTO.timeType" value="day" onclick="common.tools.scheduledTask.scheduledTask.everyWhatChange(this.value,'add_')"  checked="checked">
												<fmt:message key="title.scheduled.day.plan" />
												<input type="radio" name="scheduledTaskDTO.timeType" value="weekly" onclick="common.tools.scheduledTask.scheduledTask.everyWhatChange(this.value,'add_')">
												<fmt:message key="title.scheduled.week.plan" />
												<input type="radio" name="scheduledTaskDTO.timeType" value="month" onclick="common.tools.scheduledTask.scheduledTask.everyWhatChange(this.value,'add_')">
												<fmt:message key="title.scheduled.month.plan" />
												<%-- 周期性计划(天) --%>
												<input type="radio" name="scheduledTaskDTO.timeType" value="cycle" onclick="common.tools.scheduledTask.scheduledTask.everyWhatChange(this.value,'add_')">
												<fmt:message key="title.scheduled.cycle.plan" />(<fmt:message key="label.sla.day" />)
												<%-- 周期性计划(分钟) --%>
												<input type="radio" name="scheduledTaskDTO.timeType" value="cycleMinute" onclick="common.tools.scheduledTask.scheduledTask.everyWhatChange(this.value,'add_')">
												<fmt:message key="title.scheduled.cycle.plan" />(<fmt:message key="label.sla.minute" />)
												<input type="radio" name="scheduledTaskDTO.timeType" value="on_off" onclick="common.tools.scheduledTask.scheduledTask.everyWhatChange(this.value,'add_')">
												<fmt:message key="title.scheduled.on-off.plan" />
											</td>
										</tr>
										<tr>
											<td>
												<b>[<span id="add_everyWhat_show"><fmt:message key="label.scheduledTask.day" /></span>]</b><br>
												
												<div id="add_everyWhat_weekly" style="display: none;" class="lineTableBgDiv">
													<!--每周定时维护： -->
													<table style="width:100%" class="lineTableBgDiv" class="lineTable" cellspacing="1">
														<tr><td colspan="4"><input type="checkbox" id="add_scheduledTask_weekWeeks_all" value="1" onclick="common.tools.scheduledTask.scheduledTask.checkAll('add_scheduledTask_weekWeeks_all','add_everyWhat_weekly','scheduledTaskDTO.weekWeeks')"/>
														<fmt:message key="label.date.weekly" />：</td></tr>
														<tr>
															<td><input type="checkbox" value="SUN" name="scheduledTaskDTO.weekWeeks" /><fmt:message key="label.orgSettings.sunday" /></td>
															<td><input type="checkbox" value="MON" name="scheduledTaskDTO.weekWeeks" /><fmt:message key="label.orgSettings.monday" /></td>
															<td><input type="checkbox" value="TUE" name="scheduledTaskDTO.weekWeeks" /><fmt:message key="label.orgSettings.tuesday" /></td>
															<td><input type="checkbox" value="WED" name="scheduledTaskDTO.weekWeeks" /><fmt:message key="label.orgSettings.wednesday" /></td>
														</tr>
														<tr>
															<td><input type="checkbox" value="THU" name="scheduledTaskDTO.weekWeeks" /><fmt:message key="label.orgSettings.thursday" /></td>
															<td><input type="checkbox" value="FRI" name="scheduledTaskDTO.weekWeeks" /><fmt:message key="label.orgSettings.friday" /></td>
															<td><input type="checkbox" value="SAT" name="scheduledTaskDTO.weekWeeks" /><fmt:message key="label.orgSettings.saturday" /></td>
															<td></td>
														</tr>
													</table>
												</div>
												
												<div id="add_everyWhat_monthly" style="display: none;"  class="lineTableBgDiv">
													<!-- 每月定时维护：  -->
													
													<table style="width:100%" class="lineTableBgDiv" class="lineTable" cellspacing="1">
														<tr><td colspan="4"><input type="checkbox" id="add_scheduledTask_monthMonths_all" value="1" onclick="common.tools.scheduledTask.scheduledTask.checkAll('add_scheduledTask_monthMonths_all','add_everyWhat_monthly','scheduledTaskDTO.monthMonths')"/><fmt:message key="label.date.month" />：</td></tr>
														<tr>
															<td><input type="checkbox" value="JAN" name="scheduledTaskDTO.monthMonths" /><fmt:message key="label.date.Jan" /></td>
															<td><input type="checkbox" value="FEB" name="scheduledTaskDTO.monthMonths" /><fmt:message key="label.date.Feb" /></td>
															<td><input type="checkbox" value="MAR" name="scheduledTaskDTO.monthMonths" /><fmt:message key="label.date.Mar" /></td>
															<td><input type="checkbox" value="APR" name="scheduledTaskDTO.monthMonths" /><fmt:message key="label.date.Apr" /></td>
														</tr>
														<tr>
															<td><input type="checkbox" value="MAY" name="scheduledTaskDTO.monthMonths" /><fmt:message key="label.date.May" /></td>
															<td><input type="checkbox" value="JUN" name="scheduledTaskDTO.monthMonths" /><fmt:message key="label.date.Jun" /></td>
															<td><input type="checkbox" value="JUL" name="scheduledTaskDTO.monthMonths" /><fmt:message key="label.date.Jul" /></td>
															<td><input type="checkbox" value="AUG" name="scheduledTaskDTO.monthMonths" /><fmt:message key="label.date.Aug" /></td>
														</tr>
														<tr>
															<td><input type="checkbox" value="SEP" name="scheduledTaskDTO.monthMonths" /><fmt:message key="label.date.Sep" /></td>
															<td><input type="checkbox" value="OCT" name="scheduledTaskDTO.monthMonths" /><fmt:message key="label.date.Oct" /></td>
															<td><input type="checkbox" value="NOV" name="scheduledTaskDTO.monthMonths" /><fmt:message key="label.date.Nov" /></td>
															<td><input type="checkbox" value="DEC" name="scheduledTaskDTO.monthMonths" /><fmt:message key="label.date.Dec" /></td>
														</tr>
														<tr><td colspan="4">
															<fmt:message key="common.date"></fmt:message>：
															<select id="monthDay_select" name="scheduledTaskDTO.monthDay">
															</select>
														</td></tr>
													</table>

													
												</div>
												
												<div id="add_everyWhat_cycle" style="display: none;">
													<!-- 周期性地维护： ：  -->
													<hr>
													<fmt:message key="label.scheduledTask.every"></fmt:message>&nbsp;&nbsp;&nbsp;<input type="text" class="easyui-numberbox" value="1" width="12px" name="scheduledTaskDTO.cyclicalDay">
													<fmt:message key="label.scheduledTask.day.one"></fmt:message>
												</div>
												
												<%-- 周期性执行(分钟) --%>
												<div id="add_everyWhat_cycleMinute" style="display: none;">
													
													<hr>
													<fmt:message key="label.scheduledTask.every"></fmt:message>&nbsp;&nbsp;
													<input type="text" width="12px" class="easyui-numberbox" value="1" id="add_scheduledTask_cyclicalMinute" name="scheduledTaskDTO.cyclicalMinute">
													<fmt:message key="label.sla.minute" />
													
												</div>
												
											</td>
										</tr>
										<tr>
											<td>
													<div id="add_scheduledTask_taskDate" style="width: 100%">
													<span id="add_scheduledTask_startTime"><fmt:message key="label.sla.slaStartTime" /></span>&nbsp;&nbsp;
													<input type="text" style="width:75%" name="scheduledTaskDTO.taskDate" id="add_scheduledTask_startTime_input" class="input" readonly>
													
													<div id="add_scheduledTask_endTime_div">
													<br>
													<span id="add_scheduledTask_endTime"><fmt:message key="label.sla.slaEndTime" /></span>&nbsp;&nbsp;
													<input type="text" style="width:75%" id="add_scheduledTask_endTime_input" name="scheduledTaskDTO.taskEndDate" class="input easyui-validatebox" validType="DateComparison['add_scheduledTask_startTime_input']" readonly></div>
													<hr></div>
													
													<div id="add_scheduledTask_taskHour_taskMinute">
														<fmt:message key="label.scheduledTask.specific.time" />：
														<select name="scheduledTaskDTO.taskHour" id="timeType_hours">
															<c:forEach var="i" begin="0" end="23" step="1"> 
																<option value="${i}">${i}</option>
															</c:forEach>
														</select>
														<fmt:message key="label.orgSettings.hour" />
														<!-- 分 -->
														
														<select name="scheduledTaskDTO.taskMinute" >
															<c:forEach var="i" begin="0" end="59" step="1"> 
																<option value="${i}">${i}</option>
															</c:forEach>
														</select>
														<fmt:message key="label.orgSettings.minute" />
													</div>
											</td>
										</tr>
									</table>
									</div>
								</td>
							</tr>
						</table>
					</div>	</form>
				</div>
			</div>
		</div>	
		<!-- 扩展信息 end -->

	</div>
</div>