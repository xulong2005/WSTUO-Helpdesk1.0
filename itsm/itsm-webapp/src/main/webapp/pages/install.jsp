﻿<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ include file="language.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<title><fmt:message key="lable.install.title"/></title>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="wstuo.com">
<meta name="author" content="WSTUO">

<link rel="stylesheet" href="../js/jquery/jquery-ui-1.11.4.custom/jquery-ui.min.css" />
<link rel="stylesheet" href="../js/jquery/jquery-ui-1.11.4.custom/jquery-ui.structure.min.css" />
<link rel="stylesheet" href="../css/easyui.css">
<!-- The styles -->
<link id="bs-css" href="../bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">

<link href="../css/charisma-app.css" rel="stylesheet">
<link href='../bower_components/fullcalendar/dist/fullcalendar.css' rel='stylesheet'>
<link href='../bower_components/fullcalendar/dist/fullcalendar.print.css' rel='stylesheet' media='print'>
<link href='../bower_components/chosen/chosen.min.css' rel='stylesheet'>
<link href='../bower_components/bootstrap-tour/build/css/bootstrap-tour.min.css' rel='stylesheet'>
<link href='../css/jquery.noty.css' rel='stylesheet'>
<link href='../css/noty_theme_default.css' rel='stylesheet'>
<link href='../css/elfinder.min.css' rel='stylesheet'>
<link href='../css/elfinder.theme.css' rel='stylesheet'>
<link href='../css/jquery.iphone.toggle.css' rel='stylesheet'>
<link href='../css/animate.min.css' rel='stylesheet'>
<link href='../css/css.css' rel='stylesheet'>
<link rel="stylesheet" type="text/css" href="../css/upload/default.css">
<link href="../css/upload/fileinput.css" media="all" rel="stylesheet" type="text/css" />	
<!-- jQuery -->
<script src="../bower_components/jquery/jquery.min.js"></script>

<!-- The HTML5 shim, for IE6-8 support of HTML5 elements -->
<!--[if lt IE 9]>
<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->

<!-- The fav icon -->
<link rel="shortcut icon" href="../images/logo2.png">

<!-- external javascript -->

<script src="../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

<!-- library for cookie management -->
<script src="../js/js/jquery.cookie.js"></script>

<!-- calender plugin -->
<script src='../bower_components/moment/min/moment.min.js'></script>

<!-- select or dropdown enhancer -->
<script src="../bower_components/chosen/chosen.jquery.min.js"></script>
<!-- notification plugin -->
<script src="../js/js/jquery.noty.js"></script>
<!-- tour plugin -->
<script src="../bower_components/bootstrap-tour/build/js/bootstrap-tour.min.js"></script>
<!-- star rating plugin -->
<script src="../js/js/jquery.raty.min.js"></script>
<!-- for iOS style toggle switch -->
<script src="../js/js/jquery.iphone.toggle.js"></script>
<!-- history.js for cross-browser state change on ajax -->
<script src="../js/js/jquery.history.js"></script>


<script src="../js/jquery/jquery-ui-1.11.4.custom/jquery-ui.min.js"></script>

<script src="../js/ckeditor/ckeditor.js"></script>
<!-- <script src="../scripts/jquery/jquery.floatingmessage.js"></script> -->

<!-- <script src='../dwr/engine.js'></script>  
<script src='../dwr/util.js'></script>
<script src='../dwr/interface/getuserpagedwr.js'></script> -->
<script type="text/javascript" src="../js/basics/package.js"></script>
<script type="text/javascript" src="../js/basics/import.js"></script>

<script src="../js/i18n/i18n_${lang}.js"></script>

<script src="../js/jquery/jquery-json.js"></script> 
<script type="text/javascript" src="../js/basics/easyui_common.js"></script>
<script type="text/javascript" src="../js/basics/jquery_common.js"></script>
<script type="text/javascript" src="../js/basics/jqgrid_common.js"></script>
<script type="text/javascript" src="../js/basics/browserProp.js"></script>
<script type="text/javascript" src="../js/jquery/ajaxfileupload.js"></script>
<script type="text/javascript" src="../js/basics/ie6.js"></script>
	<script>var isAllRequest_Res=false;var allRequest_Res;</script>
<sec:authorize url="AllRequest_Res">
<script>isAllRequest_Res=true</script>
</sec:authorize>
	<script type="text/javascript">
	
	var browserLanguage = navigator.browserLanguage?navigator.browserLanguage:navigator.language;
	function importConfigData(){//升级操作
		var language;
		var loginLang;
		if($('#languageVersion').val()=='en_US'){
			language="en";
			loginLang="en_US";
		}else if($('#languageVersion').val()=='zh_CN'){
			language="cn";
			loginLang="zh_CN";
		}else if($('#languageVersion').val()=='ja_JP'){
			language="jp";
			loginLang="ja_JP";
		}else if($('#languageVersion').val()=='zh_TW'){
			language="tw";
			loginLang="zh_TW";
		}
		var installDomeData=$('#installChecked').attr('checked');
		var _url='configData!importConfigData.action?languageVersion='+language+'&insatllDomeData='+installDomeData;
		startProcess();
		$.post(_url,function(res){
			if(res=='success'){
				msgAlert(i18n['install_Congratulations'],'success');
				//同步知识库分类资源
				$.post('event!getCategoryTree.action?flag=dataDic&types=Knowledge');
				setTimeout(function(){
					
					window.location="login.jsp?language="+loginLang;
				},3000);
			}else if(res=='failure'){
				msgAlert(i18n['install_Failure'],'error');
			}else if(res=='configed'){
				msgAlert(i18n['install_Repeat'],'warning');
			}
			endProcess();
		});
	}
	$(document).ready(function(){
		
		windows('installData',{width:600,top:'300px',resizable:false,open:function(){
			$('.ui-dialog #installData').parent().find('.ui-dialog-titlebar-close').remove();
			$('.ui-dialog #installData').parent().css('top','300px');
		}});
		$('#upgrade_btn').button().click(importConfigData);
		
		var strHref  =  location.href; 
		var i=strHref.length;
		var st=strHref.substring(i-5,i)
	
		if(st=="en_US"){
			 st="en_US";
			
	    	 setCookie('language',st,300);
		     location.href='install.jsp';}
	    if(st=="zh_CN"){
			 st="zh_CN";
			
	    	 setCookie('language',st,300);
		     location.href='install.jsp';}
	    if(st=="ja_JP"){
	    	 st="ja_JP";
	    	 setCookie('language',st,300);
		     location.href='install.jsp';}
	    if(st=="zh_TW"){
	    	 st="zh_TW";
	    	 setCookie('language',st,300);
		     location.href='install.jsp';}
	});
	function setCookie(c_name,value,exdays){
		var exdate=new Date();
		exdate.setDate(exdate.getDate() + exdays);
		var c_value=escape(value) + ((exdays==null) ? "" : "; expires="+exdate.toUTCString());
		document.cookie=c_name + "=" + c_value;
	}
	
	</script>
	
	<style type="text/css">
		td{line-height:22px;}
	</style>
</head>
<body style="margin:0px;background-repeat:no-repeat; background-position:top;font-size:12px;overflow: hidden;" >
<div style="overflow: hidden;width: 0px;height: 0px;position:relative">
<div id="installData" title='<fmt:message key="lable.install.title"/>' style="width:600px;height:auto;" >
<form>
 	<div class="lineTableBgDiv" id="companyInfoDiv">
       		<table style="width:auto;height: auto;" class="lineTable" cellspacing="1">
       		<tr style="display: none">
       		<td style="height:45px;display: none">
       			<fmt:message key="lable.install.update.lang"/>&nbsp;&nbsp;
       			<select onchange="setLanguage(this.value)" name="languageVersion" id="languageVersion">
	       			<option id="cn1" value="zh_CN">简体中文</option>
	       			<option id="en1" value="en_US">English</option>
	       			<option id="jp1" value="ja_JP">日文</option>
	       			<option id="tw1" value="zh_TW">繁體中文</option>
       			</select>
       			 <script>
       				function setLanguage(lang){
		        		 //alert(lang);
		        		 setCookie('language',lang,300);
		        		// alert(lang);
		        		 location.href='index.jsp'
		        			// alert(lang);
		        	 }
       			
		       		 var lan="${cookie['language'].value}";
			           if('${param.language}'==''){
				           if(lan!=null && lan!=''){
				        	   $('#languageVersion').val(lan);
				           }else{
							
				        	   $('#languageVersion').val(browserLanguage);
				           }
			           }else{
			        	   $('#languageVersion').val('${param.language}');
			           }
        		 </script>
       		</td>
       		
       		</tr>
       		<tr>
       		<td style="height:80px">
       		
       			<fmt:message key="lable.install.careful"/><br/>
       			<fmt:message key="lable.install.careful.zone"/><br/>
       			<fmt:message key="lable.install.careful.one"/><br/>
       			<fmt:message key="lable.install.careful.two"/><br/>
       			<fmt:message key="lable.install.careful.three"/><br/>
       		</td>
       		
       		</tr>

       		<tr style="display: none">
       				<td>
       				<fmt:message key="lable.install.system"/>(<span style="color:#FF0000"><fmt:message key="lable.install.SetUp"/> </span>)<br/>
       				<input name="dataKind" type="checkbox" id="installChecked"><fmt:message key="label.install.domeData"/>(<span style="color:#FF0000"><fmt:message key="label.install.domeDataContent"/></span>)<br/>
       				</td>
       				
       		</tr>
   
       		<tr>
       		<td style="height:45px">
       			<a class="easyui-linkbutton"  id="upgrade_btn" icon="icon-ok" ><fmt:message key="lable.install.title"/></a>
       		</td>
       		</tr>
       		
       		</table>
       		
     </div>
 
</form>


</div>



<%--显示操作进程 --%>
<%@ include file="includes/includes_process.jsp"%>
</div>
</body>
</html>