package com.wstuo.itsm.scheduled.action;

import java.io.File;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.opensymphony.xwork2.ActionSupport;
import com.wstuo.common.scheduled.dto.SchedulePreviewDTO;
import com.wstuo.common.scheduled.dto.ScheduleQueryDTO;
import com.wstuo.common.scheduled.dto.ScheduleViewDTO;
import com.wstuo.itsm.scheduled.service.IScheduleService;
/**
 * 人员行程Action类
 * @author Ciel
 *
 */
@SuppressWarnings("serial")
public class ScheduledAction extends ActionSupport{
	@Autowired
    private IScheduleService scheduleService;
	private ScheduleQueryDTO scheduleQueryDTO;
    private List<SchedulePreviewDTO> schedulePreviewDTO = new ArrayList<SchedulePreviewDTO>();
    private ScheduleViewDTO scheduleViewDTO = new ScheduleViewDTO();
    private File file;
    private String sidx;
    private String sord;
    private String fileName="";
    private InputStream exportStream;
    
	public ScheduleQueryDTO getScheduleQueryDTO() {
		return scheduleQueryDTO;
	}

	public void setScheduleQueryDTO(ScheduleQueryDTO scheduleQueryDTO) {
		this.scheduleQueryDTO = scheduleQueryDTO;
	}

	public List<SchedulePreviewDTO> getSchedulePreviewDTO() {
		return schedulePreviewDTO;
	}

	public void setSchedulePreviewDTO(List<SchedulePreviewDTO> schedulePreviewDTO) {
		this.schedulePreviewDTO = schedulePreviewDTO;
	}

	public ScheduleViewDTO getScheduleViewDTO() {
		return scheduleViewDTO;
	}

	public void setScheduleViewDTO(ScheduleViewDTO scheduleViewDTO) {
		this.scheduleViewDTO = scheduleViewDTO;
	}

	public File getFile() {
		return file;
	}

	public void setFile(File file) {
		this.file = file;
	}

	public String getSidx() {
		return sidx;
	}

	public void setSidx(String sidx) {
		this.sidx = sidx;
	}

	public String getSord() {
		return sord;
	}

	public void setSord(String sord) {
		this.sord = sord;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public InputStream getExportStream() {
		return exportStream;
	}

	public void setExportStream(InputStream exportStream) {
		this.exportStream = exportStream;
	}

	/**
	 * 获取行程显示数据
	 */
	public String findScheduleRequest(){
    	scheduleViewDTO = scheduleService.findScheduleShowData(scheduleQueryDTO,sidx,sord);
    	return "scheduleViewDTO";
    }
    /**
     * 当前行程视图导出
     */
    public String exportScheduleData(){
    	fileName="ScheduleViewData.csv";
    	exportStream = scheduleService.exportScheduleData(scheduleQueryDTO, sidx, sord);
    	return "exportFileSuccessful";
    }
    /**
     * 人员行程主界面导出
     */
    public String exportScheduleView(){
    	fileName="ScheduleView.xls";
    	exportStream = scheduleService.exportScheduleView(scheduleQueryDTO, sidx, sord);
    	return "exportFileSuccessful";
    }
    /**
     * 预览导出效果
     */
    public String exportSchedulePreview(){
    	schedulePreviewDTO = scheduleService.exportSchedulePreview(scheduleQueryDTO, sidx, sord);
    	return "schedulePreviewDTO";
    }
}
