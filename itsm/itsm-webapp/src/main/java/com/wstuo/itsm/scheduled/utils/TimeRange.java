package com.wstuo.itsm.scheduled.utils;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.wstuo.common.scheduled.dto.ScheduleDTO;
import com.wstuo.common.util.TimeUtils;

public class TimeRange {
	/**
	 * 判断是否在指定时间范围内
	 * @param startTime1
	 * @param endTime1
	 * @param startTime
	 * @param endTime
	 * @return
	 */
	public static boolean beForSpecifiedTime(Date startTime1, Date endTime1, Date startTime,Date endTime) {
		if( !( startTime1 != null && endTime1 != null && startTime != null && endTime != null ) ){
			return false;
		}
		startTime1 = TimeUtils.dateFormat(startTime1,TimeUtils.DATE_PATTERN);
		endTime1 = TimeUtils.dateFormat(endTime1,TimeUtils.DATE_PATTERN);
		startTime = TimeUtils.dateFormat(startTime,TimeUtils.DATE_PATTERN);
		endTime = TimeUtils.dateFormat(endTime,TimeUtils.DATE_PATTERN);
		// 如果开始时间及结束时间都在指定的时间范围内，那么说明在时间有有冲突
		if ((startTime1.getTime() >= startTime.getTime() && startTime1.getTime() <= endTime.getTime())
			|| (endTime1.getTime() >= startTime.getTime() && endTime1.getTime() <= endTime.getTime())
			|| (startTime.getTime() >= startTime1.getTime() && startTime.getTime() <= endTime1.getTime())
			|| (endTime.getTime() >= startTime1.getTime() && endTime.getTime() <= endTime1.getTime())){
			return true;
		}
		return false;
	}
	
	/**
	 * 取两时间段的天数
	 * 
	 * @param startTime
	 * @param endTime
	 * @return List<Date>
	 */
	@Transactional
	public static List<Date> getTimeBetweenDays(String startTime, String endTime) {

		Date startDate = new Date();
		startDate = TimeUtils.parse(startTime,TimeUtils.DATE_PATTERN);
		Date endDate = new Date();
		endDate = TimeUtils.parse(endTime,TimeUtils.DATE_PATTERN);
		Long days = (endDate.getTime() - startDate.getTime())
				/ (1000 * 3600 * 24);
		int betweenDay = days.intValue() + 1;

		List<Date> dates = new ArrayList<Date>(betweenDay);

		Calendar cl = new GregorianCalendar();
		cl.setTime(startDate);

		for (int i = 0; i < betweenDay; i++) {
			Calendar hiloday = new GregorianCalendar(cl.get(Calendar.YEAR),
					cl.get(Calendar.MONTH), cl.get(Calendar.DATE) + i);
			dates.add(TimeUtils.dateFormat(hiloday.getTime(),TimeUtils.DATE_PATTERN));
		}
		return dates;
	}
	/**
	 * 判断是否全是任务并且时间段不在行程时间内
	 * @param data
	 * @param minTime
	 * @param maxTime
	 * @return
	 */
	@Transactional
	public static boolean scheduleIsAllTask(List<ScheduleDTO> data, String minTime,String maxTime) {
		boolean returnResult = false;
			List<Date> dates = TimeRange.getTimeBetweenDays(minTime, maxTime);
			if (!scheduleTaskJudge(data)){// 如果全部是任务，则判断全部的任务是否都在当前指定时间内
				for (ScheduleDTO dto : data) {
					for (Date d : dates) {
						if (d.getTime() >= TimeUtils.dateFormat(new Date(),TimeUtils.DATE_PATTERN)
								.getTime()
								&& dto.getStartTime().getTime() <= d.getTime()
								&& dto.getEndTime().getTime() >= d.getTime()) {
							returnResult = true;
							break;
						}
					}
				}
			}else{
				returnResult = true;
			}
		return returnResult;
	}
	/**
	 * 判断类型是否是任务，如果不是，则直接跳出循环
	 * @param data
	 * @return
	 */
	private static boolean scheduleTaskJudge(List<ScheduleDTO> data){
		boolean result = false;
		for (ScheduleDTO dto : data) {
			if (!"Task".equals(dto.getType())) {// 判断类型是否是任务，如果不是，则直接跳出循环
				result = true;
				break;
			}
		}
		return result;
	}
}
