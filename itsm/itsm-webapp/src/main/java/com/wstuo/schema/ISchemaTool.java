package com.wstuo.schema;

import java.sql.SQLException;

import com.wstuo.common.security.dto.UserDTO;


public interface ISchemaTool {
	/**
	 * 创建数据库
	 * @param tenantIdentifier
	 * @throws SQLException
	 */
	void createDatabase(String tenantIdentifier) throws SQLException;
	/**
	 * 创建表结构
	 * @param tenantIdentifier
	 * @throws SQLException
	 */
	void createSchema(String tenantIdentifier)throws SQLException;
	/**
	 * 更新表结构
	 * @param tenantIdentifier
	 * @throws SQLException
	 */
	void updateSchema(String tenantIdentifier)throws SQLException;
	/**
	 * 加载基础数据
	 * @param tenantIdentifier
	 * @param language
	 * @throws SQLException
	 */
	void configDataLoader(String tenantIdentifier,String language,UserDTO userDTO)throws SQLException ;
	/**
	 * 配置文件初始化
	 * @param tenantIdentifier
	 */
	void configureFileInit(String tenantIdentifier);
	/**
	 * schema初始化
	 * @param tenantIdentifier
	 * @param language
	 * @throws SQLException
	 */
	void tenantDataInit(String tenantIdentifier,String language,UserDTO userDTO)throws SQLException ;
}
