package com.wstuo.upgrade.service;

public interface IUpgradeService {

	/**
	 * 升级
	 * 
	 * @return String
	 */
	public abstract String upgradePermission();

	/**
	 * 公共方法，取得CSV文件路径.
	 * 
	 * @param fileName
	 * @return String
	 */
	public abstract String getFilePath(String fileName);

	/**
	 * 所在位置的更新
	 * 
	 * @return String
	 */
	public abstract String upgradeLocation();
	/**
	 * 
	 * @return boolean
	 */
	public boolean subServiceList();
	/**
	 * 
	 * @return boolean
	 */
	public boolean serviceDirectoryList();
	/**
	 * 帐号密码升级
	 */
	public void passwordUpgrade(); 
	
	/**
	 * 对已关闭的请求进行计算处理时长
	 */
	void closeRequestHanldTimeLong();
	/**
	 * 重建索引
	 * @return
	 */
	String reindexAll();
	/**
	 * 初始化索引库
	 * @return
	 */
	String initIndexs( );
	String delIndexs( );
	/**
	 * 更新知识库权限
	 * @return
	 */
	boolean upgradeKnowledgeRes();

}