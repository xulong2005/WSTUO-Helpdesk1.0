package com.wstuo.weixin;
import java.io.IOException;
import java.util.Arrays;

import javax.servlet.http.HttpServletResponse;

import org.apache.struts2.ServletActionContext;
import com.wstuo.weixin.WeixinMessageDigestUtil;

public class WeixinAction {

    private static final String TOKEN = "weixin_wstuo";
    /**
     * 
		AppID(应用ID)
		wx4cf2d5dde8351571
		AppSecret(应用密钥)
		6e828f2f7efe6d07d5f83c54bd728c02
     */
    
    private String signature;
    private String timestamp;
    private String nonce;
    private String echostr;
    /**
     * 微信回调方法、包括doGet & doPost 
     * @return
     */
    public String msg(){
    	String[] arrTmp = { TOKEN, timestamp, nonce }; 
        Arrays.sort(arrTmp); 
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < arrTmp.length; i++) {
            sb.append(arrTmp[i]);
        }
        String pwd = WeixinMessageDigestUtil.getInstance().encipher(sb.toString());  
        if(pwd.equals(signature)){
            if(!"".equals(echostr) && echostr != null){
                try {
                    HttpServletResponse response = ServletActionContext.getResponse();
                	response.getWriter().write(echostr);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return null;
    }
        //一下代码不用看的.
    //getter && setter 
    public String getSignature() {
        return signature;
    }

    public void setSignature(String signature) {
        this.signature = signature;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public String getNonce() {
        return nonce;
    }

    public void setNonce(String nonce) {
        this.nonce = nonce;
    }

    public String getEchostr() {
        return echostr;
    }

    public void setEchostr(String echostr) {
        this.echostr = echostr;
    }
    
}