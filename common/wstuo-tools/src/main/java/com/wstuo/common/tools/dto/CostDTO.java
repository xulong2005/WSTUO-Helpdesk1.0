package com.wstuo.common.tools.dto;

import java.util.Date;

import com.wstuo.common.dto.BaseDTO;

/**
 * 成本DTO
 * @author WSTUO
 * 
 */
@SuppressWarnings("serial")
public class CostDTO extends BaseDTO {
	private Long progressId;
	private String stage;
	private Long status;
	private String description;
	private Long taskId;
	private String title;
	private Long userId;
	private String userName;
	private String fullName;
	private Date opertionTime;
	private Double perHourFees;
	private Long useTime;
	private Double skillFees;
	private Double othersFees;
	private Double totalFees;
	private Long eno;
	private String eventType;
	private Byte dataFlag;
	private Date startTime;// 执行开始时间
	private Date endTime;// 执行结束时间
	private Long startToEedTime;// 开始到结束时间(分钟)
	private Long actualTime;// 实际处理时间(分钟)
	private String type = "NOHANG";// 进展类型(挂起、工程师填写)
	private Double materialCost;// 物料成本
	private Long scheduleTaskId;
	private Date scheduleTaskTime;

	public Long getProgressId() {
		return progressId;
	}
	public boolean checkProgressId() {
		if ( this.progressId != null && this.progressId > 0) {
			return true;
		}
		return false;
	}
	public void setProgressId(Long progressId) {
		this.progressId = progressId;
	}

	public String getStage() {
		return stage;
	}

	public void setStage(String stage) {
		this.stage = stage;
	}

	public Long getStatus() {
		return status;
	}

	public void setStatus(Long status) {
		this.status = status;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Long getTaskId() {
		return taskId;
	}

	public void setTaskId(Long taskId) {
		this.taskId = taskId;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public Date getOpertionTime() {
		return opertionTime;
	}

	public void setOpertionTime(Date opertionTime) {
		this.opertionTime = opertionTime;
	}

	public Double getPerHourFees() {
		return perHourFees;
	}

	public void setPerHourFees(Double perHourFees) {
		this.perHourFees = perHourFees;
	}

	public Long getUseTime() {
		return useTime;
	}

	public void setUseTime(Long useTime) {
		this.useTime = useTime;
	}

	public Double getSkillFees() {
		return skillFees;
	}

	public void setSkillFees(Double skillFees) {
		this.skillFees = skillFees;
	}

	public Double getOthersFees() {
		return othersFees;
	}

	public void setOthersFees(Double othersFees) {
		this.othersFees = othersFees;
	}

	public Double getTotalFees() {
		return totalFees;
	}

	public void setTotalFees(Double totalFees) {
		this.totalFees = totalFees;
	}

	public Long getEno() {
		return eno;
	}

	public void setEno(Long eno) {
		this.eno = eno;
	}

	public String getEventType() {
		return eventType;
	}

	public void setEventType(String eventType) {
		this.eventType = eventType;
	}

	public Byte getDataFlag() {
		return dataFlag;
	}

	public void setDataFlag(Byte dataFlag) {
		this.dataFlag = dataFlag;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((actualTime == null) ? 0 : actualTime.hashCode());
		result = prime * result
				+ ((dataFlag == null) ? 0 : dataFlag.hashCode());
		result = prime * result
				+ ((description == null) ? 0 : description.hashCode());
		result = prime * result + ((endTime == null) ? 0 : endTime.hashCode());
		result = prime * result + ((eno == null) ? 0 : eno.hashCode());
		result = prime * result
				+ ((eventType == null) ? 0 : eventType.hashCode());
		result = prime * result
				+ ((fullName == null) ? 0 : fullName.hashCode());
		result = prime * result
				+ ((materialCost == null) ? 0 : materialCost.hashCode());
		result = prime * result
				+ ((opertionTime == null) ? 0 : opertionTime.hashCode());
		result = prime * result
				+ ((othersFees == null) ? 0 : othersFees.hashCode());
		result = prime * result
				+ ((perHourFees == null) ? 0 : perHourFees.hashCode());
		result = prime * result
				+ ((progressId == null) ? 0 : progressId.hashCode());
		result = prime * result
				+ ((skillFees == null) ? 0 : skillFees.hashCode());
		result = prime * result + ((stage == null) ? 0 : stage.hashCode());
		result = prime * result
				+ ((startTime == null) ? 0 : startTime.hashCode());
		result = prime * result
				+ ((startToEedTime == null) ? 0 : startToEedTime.hashCode());
		result = prime * result + ((status == null) ? 0 : status.hashCode());
		result = prime * result + ((taskId == null) ? 0 : taskId.hashCode());
		result = prime * result + ((title == null) ? 0 : title.hashCode());
		result = prime * result
				+ ((totalFees == null) ? 0 : totalFees.hashCode());
		result = prime * result + ((type == null) ? 0 : type.hashCode());
		result = prime * result + ((useTime == null) ? 0 : useTime.hashCode());
		result = prime * result + ((userId == null) ? 0 : userId.hashCode());
		result = prime * result
				+ ((userName == null) ? 0 : userName.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CostDTO other = (CostDTO) obj;
		if (actualTime == null) {
			if (other.actualTime != null)
				return false;
		} else if (!actualTime.equals(other.actualTime))
			return false;
		if (dataFlag == null) {
			if (other.dataFlag != null)
				return false;
		} else if (!dataFlag.equals(other.dataFlag))
			return false;
		if (description == null) {
			if (other.description != null)
				return false;
		} else if (!description.equals(other.description))
			return false;
		if (endTime == null) {
			if (other.endTime != null)
				return false;
		} else if (!endTime.equals(other.endTime))
			return false;
		if (eno == null) {
			if (other.eno != null)
				return false;
		} else if (!eno.equals(other.eno))
			return false;
		if (eventType == null) {
			if (other.eventType != null)
				return false;
		} else if (!eventType.equals(other.eventType))
			return false;
		if (fullName == null) {
			if (other.fullName != null)
				return false;
		} else if (!fullName.equals(other.fullName))
			return false;
		if (materialCost == null) {
			if (other.materialCost != null)
				return false;
		} else if (!materialCost.equals(other.materialCost))
			return false;
		if (opertionTime == null) {
			if (other.opertionTime != null)
				return false;
		} else if (!opertionTime.equals(other.opertionTime))
			return false;
		if (othersFees == null) {
			if (other.othersFees != null)
				return false;
		} else if (!othersFees.equals(other.othersFees))
			return false;
		if (perHourFees == null) {
			if (other.perHourFees != null)
				return false;
		} else if (!perHourFees.equals(other.perHourFees))
			return false;
		if (progressId == null) {
			if (other.progressId != null)
				return false;
		} else if (!progressId.equals(other.progressId))
			return false;
		if (skillFees == null) {
			if (other.skillFees != null)
				return false;
		} else if (!skillFees.equals(other.skillFees))
			return false;
		if (stage == null) {
			if (other.stage != null)
				return false;
		} else if (!stage.equals(other.stage))
			return false;
		if (startTime == null) {
			if (other.startTime != null)
				return false;
		} else if (!startTime.equals(other.startTime))
			return false;
		if (startToEedTime == null) {
			if (other.startToEedTime != null)
				return false;
		} else if (!startToEedTime.equals(other.startToEedTime))
			return false;
		if (status == null) {
			if (other.status != null)
				return false;
		} else if (!status.equals(other.status))
			return false;
		if (taskId == null) {
			if (other.taskId != null)
				return false;
		} else if (!taskId.equals(other.taskId))
			return false;
		if (title == null) {
			if (other.title != null)
				return false;
		} else if (!title.equals(other.title))
			return false;
		if (totalFees == null) {
			if (other.totalFees != null)
				return false;
		} else if (!totalFees.equals(other.totalFees))
			return false;
		if (type == null) {
			if (other.type != null)
				return false;
		} else if (!type.equals(other.type))
			return false;
		if (useTime == null) {
			if (other.useTime != null)
				return false;
		} else if (!useTime.equals(other.useTime))
			return false;
		if (userId == null) {
			if (other.userId != null)
				return false;
		} else if (!userId.equals(other.userId))
			return false;
		if (userName == null) {
			if (other.userName != null)
				return false;
		} else if (!userName.equals(other.userName))
			return false;
		return true;
	}

	public Date getStartTime() {
		return startTime;
	}

	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}

	public Date getEndTime() {
		return endTime;
	}

	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}

	public Long getStartToEedTime() {
		return startToEedTime;
	}

	public void setStartToEedTime(Long startToEedTime) {
		this.startToEedTime = startToEedTime;
	}

	public Long getActualTime() {
		return actualTime;
	}

	public void setActualTime(Long actualTime) {
		this.actualTime = actualTime;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Double getMaterialCost() {
		return materialCost;
	}
	public void setMaterialCost(Double materialCost) {
		this.materialCost = materialCost;
	}
	public Long getScheduleTaskId() {
		return scheduleTaskId;
	}
	public void setScheduleTaskId(Long scheduleTaskId) {
		this.scheduleTaskId = scheduleTaskId;
	}

	@Override
	public String toString() {
		return "CostDTO [progressId=" + progressId + ", stage=" + stage
				+ ", status=" + status + ", description=" + description
				+ ", taskId=" + taskId + ", title=" + title + ", userId="
				+ userId + ", userName=" + userName + ", fullName=" + fullName
				+ ", opertionTime=" + opertionTime + ", perHourFees="
				+ perHourFees + ", useTime=" + useTime + ", skillFees="
				+ skillFees + ", othersFees=" + othersFees + ", totalFees="
				+ totalFees + ", eno=" + eno + ", eventType=" + eventType
				+ ", dataFlag=" + dataFlag + ", startTime=" + startTime
				+ ", endTime=" + endTime + ", startToEedTime=" + startToEedTime
				+ ", actualTime=" + actualTime + ", type=" + type
				+ ", materialCost=" + materialCost + ", scheduleTaskId="
				+ scheduleTaskId + "]";
	}
	public Date getScheduleTaskTime() {
		return scheduleTaskTime;
	}
	public void setScheduleTaskTime(Date scheduleTaskTime) {
		this.scheduleTaskTime = scheduleTaskTime;
	}
}
