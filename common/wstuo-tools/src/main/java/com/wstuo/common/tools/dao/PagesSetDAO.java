package com.wstuo.common.tools.dao;

import com.wstuo.common.tools.entity.PagesSet;
import com.wstuo.common.dao.BaseDAOImplHibernate;

/**
 * 页面设置DAO
 * @author Candy
 *
 */
public class PagesSetDAO extends BaseDAOImplHibernate<PagesSet> implements IPagesSetDAO{

}
