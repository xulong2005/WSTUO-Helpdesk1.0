package com.wstuo.common.scheduledTask.service;

import java.io.File;

import com.wstuo.common.scheduledTask.dto.ScheduledTaskDTO;
import com.wstuo.common.scheduledTask.dto.ScheduledTaskQueryDTO;
import com.wstuo.common.scheduledTask.dto.ScheduledTaskRequestDTO;
import com.wstuo.common.dto.PageDTO;

/**
 * 定期任务Service接口在类
 * @author WSTUO
 *
 */
public interface IScheduledTaskService {
	/**
	 * 一次性计划
	 */
	public final static String ONOFF="on_off";
	/**
	 * 编辑定期任务
	 */
	public final static String EDIT_SCHEDULEDTASK="edit";
	/**
	 * 新增定期任务
	 */
	public final static String NEW_SCHEDULEDTASK="new";
	/**
	 * 时间格式化
	 */
	public final static String SUCCESS="success";
	public final static String FALL="fall";
	public final static String LASTTIME=" 23:59:59";
	public final static String INITTIME=" 00:00:00";
	public String JOB_NAME = "JOB_NAME_";
	public String JOB_GROUP_NAME = "JOB_GROUP_NAME_";
	public String TRIGGER_NAME = "TRIGGER_NAME_";
	public String TRIGGER_GROUP_NAME = "TRIGGER_GROUP_NAME_";
	/**
	 * 分页查询
	 * @param queryDTO
	 * @return PageDTO
	 */
	PageDTO findPageScheduledTask(final ScheduledTaskQueryDTO queryDTO);
	/**
	 * 添加定期任务
	 * @param dto
	 */
	void saveScheduledTask(final ScheduledTaskDTO dto);
	/**
	 * 编辑定期任务
	 * @param dto ScheduledTaskDTO
	 */
	void editScheduledTask(final ScheduledTaskDTO dto);
	/**
	 * 删除定期任务
	 * @param ids
	 */
	void deleteScheduledTask(final Long[] ids);
	
	/**
	 * 移除任务
	 * @param ids
	 */
	void removeJob(final Long[] ids);
	
	/**
	 * 根据ID获取定期任务详细
	 * @param id
	 * @return ScheduledTaskDTO
	 */
	ScheduledTaskDTO findScheduledTaskById(final Long id);
	
	/**
	 * 对所有任务生成定期
	 */
	void generateScheduledTask();
	
	/**
	 * 人员行程任务导入
	 * @param importFile
	 * @return import result
	 */
	String importScheduledTask(File importFile);
	/**
	 * 查询关联表单的数量
	 * @param formId
	 * @return
	 */
	Integer findScheduledTaskNumByFormId(Long[] formIds);
	/**
	 * 转换ScheduledTaskDTO的值
	 * @param scheduledTaskDTO
	 * @param scheduledTaskRequestDTO
	 * @param cinos
	 * @param serviceDirNos
	 * @return ScheduledTaskDTO
	 */
	ScheduledTaskDTO convertScheduledTaskDTOValue(ScheduledTaskDTO scheduledTaskDTO ,ScheduledTaskRequestDTO scheduledTaskRequestDTO ,Long[] cinos,Long[] serviceDirNos);
}
