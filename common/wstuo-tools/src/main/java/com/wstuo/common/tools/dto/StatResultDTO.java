package com.wstuo.common.tools.dto;

/**
 * 统计结果DTO
 * @author WSTUO
 *
 */
public class StatResultDTO {
	private String name;
	private Long quantity;
	private String busy;
	private String bgColor;
	private Long id;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Long getQuantity() {
		return quantity;
	}
	public void setQuantity(Long quantity) {
		this.quantity = quantity;
	}
	public StatResultDTO(String name, Long quantity) {
		super();
		this.name = name;
		this.quantity = quantity;
	}
	public StatResultDTO(){
		super();
	}
	
	public String getBusy() {
		return busy;
	}
	public void setBusy(String busy) {
		this.busy = busy;
	}
	public String getBgColor() {
		return bgColor;
	}
	public void setBgColor(String bgColor) {
		this.bgColor = bgColor;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	@Override
	public String toString() {
	    StringBuffer result = new StringBuffer();
        result.append("StatResultDTO ")
        .append("[")
        .append("StatResultDTO=").append(name).append(", ")
        .append("quantity=").append(quantity)
        .append("]");
        
        return result.toString();
	}
	
}
