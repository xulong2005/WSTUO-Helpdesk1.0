package com.wstuo.common.tools.dto;

/**
 * 发送短信信息DTO类
 * @author QXY
 * date 2010-10-11
 */
public class SMSMessageDTO {

	
	
	private String id;
	private String stateMsg;
	private Integer sendCount;
	private String costMoney;
	private String totalMoney;
	private String sid;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getStateMsg() {
		return stateMsg;
	}
	public void setStateMsg(String stateMsg) {
		this.stateMsg = stateMsg;
	}
	public Integer getSendCount() {
		return sendCount;
	}
	public void setSendCount(Integer sendCount) {
		this.sendCount = sendCount;
	}
	
	public String getCostMoney() {
		return costMoney;
	}
	public void setCostMoney(String costMoney) {
		this.costMoney = costMoney;
	}
	public String getTotalMoney() {
		return totalMoney;
	}
	public void setTotalMoney(String totalMoney) {
		this.totalMoney = totalMoney;
	}
	public String getSid() {
		return sid;
	}
	public void setSid(String sid) {
		this.sid = sid;
	}

	
	@Override
	public String toString() {
	    
	    StringBuffer result = new StringBuffer();
        result.append("SMSMessageDTO ")
        .append("[")
        .append("id=").append(id).append(", ")
        .append("stateMsg=").append(stateMsg).append(", ")
        .append("sendCount=").append(sendCount).append(", ")
        .append("costMoney=").append(costMoney).append(", ")
        .append("totalMoney=").append(totalMoney).append(", ")
        .append("sid=").append(sid)
        .append("]");
        
        return result.toString();
	}
	
	
	
}
