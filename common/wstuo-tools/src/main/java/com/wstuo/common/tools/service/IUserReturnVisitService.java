package com.wstuo.common.tools.service;

import java.io.InputStream;

import com.wstuo.common.tools.dto.UserReturnVisitDTO;
import com.wstuo.common.tools.dto.UserReturnVisitDetailDTO;
import com.wstuo.common.tools.dto.UserReturnVisitQueryDTO;
import com.wstuo.common.dto.PageDTO;

/**
 * 用户回访Service interface
 * @author WSTUO
 *
 */
public interface IUserReturnVisitService {
	
	/**
	 * 保存用户回访
	 * @param returnVisitDTO
	 */
	void saveUserReturnVisit(UserReturnVisitDTO returnVisitDTO);
	/**
	 * 编辑用户回访
	 * @param returnVisitDTO
	 */
	void editUserReturnVisit(UserReturnVisitDTO returnVisitDTO);
	
	/**
	 * 用户回访回复
	 * @param returnVisitDTO
	 */
	boolean replyUserReturnVisit(UserReturnVisitDTO returnVisitDTO);
	
	/**
	 * 删除用户回访
	 * @param ids
	 */
	void deleteUserReturnVisit(final Long[] ids);
	/**
	 * 分页查询用户回访记录
	 * @param queryDTO
	 * @param sidx
	 * @param sord
	 * @return PageDTO
	 */
	PageDTO findPagerReturnVisit(UserReturnVisitQueryDTO queryDTO,String sidx, String sord);
	
	/**
	 * 根据ID获取用户回访详细记录
	 * @param id
	 * @return UserReturnVisitDetailDTO
	 */
	UserReturnVisitDetailDTO findUserReturnVisitById(final Long id);
	
	/**
	 * 导出用户记录
	 * @param queryDTO
	 * @param sidx
	 * @param sord
	 * @return InputStream
	 */
	InputStream exportReturnVisit(UserReturnVisitQueryDTO queryDTO,String sidx, String sord);
}
