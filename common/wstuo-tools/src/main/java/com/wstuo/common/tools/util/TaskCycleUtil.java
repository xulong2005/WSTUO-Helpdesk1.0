package com.wstuo.common.tools.util;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import com.wstuo.common.tools.dto.TaskQueryDTO;
import com.wstuo.common.tools.entity.Task;
import com.wstuo.common.tools.entity.TaskCycle;
import com.wstuo.common.util.TimeUtils;
/**
 * 循环任务工具类
 * @author gs60
 *
 */
public class TaskCycleUtil {


	/**
	 * 通过从数据库获取的Task实体，在对应的时间内把循环任务解析成单条普通任务
	 * @param tasks
	 * @param taskQueryDto
	 * @return
	 */
	public static List<Task> taskCycleResolve(List<Task> tasks,TaskQueryDTO taskQueryDto) {
		List<Task> results = new ArrayList<Task>();
		if( tasks != null ){
			for (Task task : tasks) {
				List<Task> temp = TaskCycleUtil.taskCycleResolve(task, taskQueryDto);
				if ( temp != null ) {
					results.addAll( temp );
				}else{
					results.add( task );
				}
			}
		}
		return results;
	}
	
	/**
	 * 循环任务解析
	 * @param task
	 * @param taskQueryDto
	 * @return 如果返回结果为null 说明是普通任务，没有进行解析；
	 */
	public static List<Task> taskCycleResolve(Task task,TaskQueryDTO taskQueryDto) {
		List<Task> list = null;
		TaskCycle cycle = task.getTaskCycle();
		taskQueryDto = TaskQueryDTO.chenkDTO(taskQueryDto);
		if (cycle != null 
				&& ToolsConstant.TASK_TYPE_CYCLE_WEEK.equals(cycle.getType())) {
			list = taskCycleResolveWithWeek(task,taskQueryDto);
		}
		return list;
	}
	/**
	 * 根据任务获取指定时间内的第一条任务；
	 * @param task
	 * @param startDate
	 * @param endDate
	 * @return
	 */
	public static Task taskCycleResolve(Task task,Date startDate,Date endDate) {
		TaskQueryDTO taskQueryDto = new TaskQueryDTO();
		taskQueryDto.setStartTime( startDate );
		taskQueryDto.setEndTime( endDate );
		List<Task> list = taskCycleResolve(task, taskQueryDto);
		if (list != null && list.size() > 0) {
			return list.get(0);
		}
		return task;
	}
	
	//==============按周循环=========================
	/**
	 * 按周循环的解析
	 * @param task
	 * @param taskQueryDto
	 * @return
	 * <hr>由于循环任务存在无线循环,所以防止性能问题，请设置TaskQueryDTO的时间范围；
	 */
	public static List<Task> taskCycleResolveWithWeek(Task task,TaskQueryDTO taskQueryDto) {
		List<Task> list = new ArrayList<Task>();
		TaskCycle cycle = task.getTaskCycle();

		Calendar startDate = TimeUtils.getMaxDate(task.getStartTime(), taskQueryDto.getStartTime());
		Calendar endDate = TimeUtils.getMinDate(task.getEndTime(), taskQueryDto.getEndTime());

		List<Date> dates = taskCycleResolveWithWeekDate(startDate, endDate,cycle);
		if (dates != null ) {
			for (Date date : dates) {
				Task task2 = task.copy();
				task2.setTaskCycle( cycle );
				task2.setStartTime( TimeUtils.dateTransform(date, cycle.getStartDate()) );
				task2.setEndTime( TimeUtils.dateTransform(date, cycle.getEndDate()) );
				list.add(task2);
			}
		}
		return list;
	}
	
	/**
	 * 获取在指定时间段（startDate至endDate）符合taskCycle的循环规则的日期；从而用来生成任务信息
	 * @param startDate
	 * @param endDate
	 * @param taskCycle
	 * @return
	 */
	public static List<Date> taskCycleResolveWithWeekDate(Calendar startDate,Calendar endDate
			,TaskCycle taskCycle) {
		List<Date> list = new ArrayList<Date>();
		int[] result = taskCycle.getDateIndexVerify();
		if (result != null) {
			int start = startDate.get(Calendar.DAY_OF_YEAR);
			int end = TimeUtils.calcDateDayCount(startDate, endDate);
			while ( start <= end ) {
				for ( int weekIndex : result ) {
					if ( startDate.get(Calendar.DAY_OF_WEEK) == weekIndex ){
						list.add( startDate.getTime() );
						break;
					}
				}
				startDate.add (Calendar.DAY_OF_YEAR, 1);
				start++;
			}
		}
		return list;
	}
}
