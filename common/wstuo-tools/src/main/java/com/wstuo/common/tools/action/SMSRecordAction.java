package com.wstuo.common.tools.action;

import org.springframework.beans.factory.annotation.Autowired;

import com.opensymphony.xwork2.ActionSupport;
import com.wstuo.common.tools.dto.SMSRecordDTO;
import com.wstuo.common.tools.service.ISMSRecordService;
import com.wstuo.common.dto.PageDTO;

/**
 * 短信发送记录Action
 * @author Administrator
 *
 */
@SuppressWarnings("serial")
public class SMSRecordAction extends ActionSupport{

	
	@Autowired
	private ISMSRecordService smsRecordService;
	private SMSRecordDTO smsRecordDTO;
	private String sord;
	private String sidx;
	private int page;
	private int rows;
	private Long [] ids;
	private PageDTO pageDTO;
	
	
	
	
	public SMSRecordDTO getSmsRecordDTO() {
		return smsRecordDTO;
	}

	public void setSmsRecordDTO(SMSRecordDTO smsRecordDTO) {
		this.smsRecordDTO = smsRecordDTO;
	}

	public String getSord() {
		return sord;
	}

	public void setSord(String sord) {
		this.sord = sord;
	}

	public String getSidx() {
		return sidx;
	}

	public void setSidx(String sidx) {
		this.sidx = sidx;
	}

	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public int getRows() {
		return rows;
	}

	public void setRows(int rows) {
		this.rows = rows;
	}

	public Long[] getIds() {
		return ids;
	}

	public void setIds(Long[] ids) {
		this.ids = ids;
	}

	public PageDTO getPageDTO() {
		return pageDTO;
	}

	public void setPageDTO(PageDTO pageDTO) {
		this.pageDTO = pageDTO;
	}

	/**
	 * 分页查找 
	 * @return pageDTO
	 */
	public String findPager(){
		int start = ( page - 1 ) * rows;
		pageDTO=smsRecordService.findPager(smsRecordDTO, start, rows, sidx, sord);
		pageDTO.setPage(page);
		pageDTO.setRows(rows);
		return SUCCESS;
	}
	
	/**
	 * 保存.
	 * @return null
	 */
	public String save(){
		smsRecordService.save(smsRecordDTO);
		return SUCCESS;
	}
	
	/**
	 * 批量删除.
	 * @return null
	 */
	public String delete(){
		smsRecordService.delete(ids);
		return SUCCESS;
	}
}
