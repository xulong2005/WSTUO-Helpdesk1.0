package com.wstuo.common.tools.dto;

/**
 * 繁忙程度自动指派DTO
 * @author QXY
 *
 */
public class AutomaticallyAssignedDTO {
	
	private String loginName;
	
	private String fullName;
	/**
	 * 我已关闭的请求
	 */
	private Long countMineClosedRequest=0L;
	/**
	 * 我已完成的请求
	 */
	private Long countMineCompletedRequest=0L;
	/**
	 * 指派给我的请求
	 */
	private Long countAssignedMineRquest=0L;
	/**
	 * 我正在处理的请求
	 */
	private Long countMineHandingRequest=0L;
	/**
	 * 我待处理的请求
	 */
	private Long countMineNewRequest=0L;
	/**
	 * 服务总分值
	 */
	private Long servicesTotalScore;
	/**
	 * 排序规则
	 */
	private String sidx;
	
	public String getSidx() {
		return sidx;
	}
	public void setSidx(String sidx) {
		this.sidx = sidx;
	}
	public String getLoginName() {
		return loginName;
	}
	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}
	public String getFullName() {
		return fullName;
	}
	public void setFullName(String fullName) {
		this.fullName = fullName;
	}
	public Long getCountMineClosedRequest() {
		return countMineClosedRequest;
	}
	public void setCountMineClosedRequest(Long countMineClosedRequest) {
		this.countMineClosedRequest = countMineClosedRequest;
	}
	public Long getCountMineCompletedRequest() {
		return countMineCompletedRequest;
	}
	public void setCountMineCompletedRequest(Long countMineCompletedRequest) {
		this.countMineCompletedRequest = countMineCompletedRequest;
	}
	public Long getCountAssignedMineRquest() {
		return countAssignedMineRquest;
	}
	public void setCountAssignedMineRquest(Long countAssignedMineRquest) {
		this.countAssignedMineRquest = countAssignedMineRquest;
	}
	public Long getCountMineHandingRequest() {
		return countMineHandingRequest;
	}
	public void setCountMineHandingRequest(Long countMineHandingRequest) {
		this.countMineHandingRequest = countMineHandingRequest;
	}
	public Long getServicesTotalScore() {
		return servicesTotalScore;
	}
	public void setServicesTotalScore(Long servicesTotalScore) {
		this.servicesTotalScore = servicesTotalScore;
	}
	public Long getCountMineNewRequest() {
		return countMineNewRequest;
	}
	public void setCountMineNewRequest(Long countMineNewRequest) {
		this.countMineNewRequest = countMineNewRequest;
	}

}
