package com.wstuo.common.tools.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import com.wstuo.common.entity.BaseEntity;


/**
 * 历史操作记录
 * @author WSTUO
 *
 */
@SuppressWarnings({ "serial", "rawtypes" })
@Entity
public class HistoryRecord extends BaseEntity {
	@Id
    @GeneratedValue( strategy = GenerationType.AUTO )
	private Long logNo;
	private String operator;
	private String logTitle;
	private String logDetails;
	private Long eno;
	private String eventType;
	
	public Long getLogNo() {
		return logNo;
	}
	public void setLogNo(Long logNo) {
		this.logNo = logNo;
	}
	public String getOperator() {
		return operator;
	}
	public void setOperator(String operator) {
		this.operator = operator;
	}
	public String getLogTitle() {
		return logTitle;
	}
	public void setLogTitle(String logTitle) {
		this.logTitle = logTitle;
	}
	public String getLogDetails() {
		return logDetails;
	}
	public void setLogDetails(String logDetails) {
		this.logDetails = logDetails;
	}
	public Long getEno() {
		return eno;
	}
	public void setEno(Long eno) {
		this.eno = eno;
	}
	public String getEventType() {
		return eventType;
	}
	public void setEventType(String eventType) {
		this.eventType = eventType;
	}
	
}
