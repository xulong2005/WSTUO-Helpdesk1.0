package com.wstuo.common.customForm.action;

import org.springframework.beans.factory.annotation.Autowired;

import com.opensymphony.xwork2.ActionSupport;
import com.wstuo.common.customForm.dto.FormCustomTabsDTO;
import com.wstuo.common.customForm.service.IFormCustomTabsService;

public class FormCustomTabsAction extends ActionSupport{
	
	@Autowired
	private IFormCustomTabsService formCustomTabsService;
	
	private FormCustomTabsDTO formCustomTabsDTO;
	private Long tabsId;
	private Long[] tabAttrsId;

	public Long getTabsId() {
		return tabsId;
	}

	public void setTabsId(Long tabsId) {
		this.tabsId = tabsId;
	}

	public Long[] getTabAttrsId() {
		return tabAttrsId;
	}

	public void setTabAttrsId(Long[] tabAttrsId) {
		this.tabAttrsId = tabAttrsId;
	}

	public FormCustomTabsDTO getFormCustomTabsDTO() {
		return formCustomTabsDTO;
	}

	public void setFormCustomTabsDTO(FormCustomTabsDTO formCustomTabsDTO) {
		this.formCustomTabsDTO = formCustomTabsDTO;
	}

	public String saveFormCustomTabs(){
		StringBuffer sb = new StringBuffer();
		for (int i = 0; i < tabAttrsId.length; i++) {
			sb.append(tabAttrsId[i]);
			if((tabAttrsId.length-1)!=i){
				sb.append(',');
			}
		}
		formCustomTabsDTO.setTabAttrNos(sb.toString());
		tabsId = formCustomTabsService.saveFormCustomTabs(formCustomTabsDTO);
		return "tabsId";
	}
	
	
	public String findFormCustomTabsByTabsId(){
		formCustomTabsDTO = formCustomTabsService.findFormCustomTabsByTabsId(tabsId);
		return SUCCESS;
	}
	
	public String updateFormCustomTabs(){
		StringBuffer sb = new StringBuffer();
		for (int i = 0; i < tabAttrsId.length; i++) {
			sb.append(tabAttrsId[i]);
			if((tabAttrsId.length-1)!=i){
				sb.append(',');
			}
		}
		formCustomTabsDTO.setTabAttrNos(sb.toString());
		tabsId = formCustomTabsService.updateFormCustomTabs(formCustomTabsDTO);
		return  "tabsId";
	}

}
