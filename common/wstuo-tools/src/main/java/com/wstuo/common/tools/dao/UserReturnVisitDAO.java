package com.wstuo.common.tools.dao;

import java.util.Calendar;
import java.util.GregorianCalendar;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;

import com.wstuo.common.tools.dto.UserReturnVisitQueryDTO;
import com.wstuo.common.tools.entity.UserReturnVisit;
import com.wstuo.common.dao.BaseDAOImplHibernate;
import com.wstuo.common.dto.PageDTO;
import com.wstuo.common.util.DaoUtils;
import com.wstuo.common.util.StringUtils;

/**
 * 用户回访DAO
 * @author WSTUO
 *
 */
public class UserReturnVisitDAO extends BaseDAOImplHibernate<UserReturnVisit> implements IUserReturnVisitDAO {
	/**
	 * 分页查询回访结果
	 * @param queryDTO
	 * @param sidx
	 * @param sord
	 * @return PageDTO
	 */
	public PageDTO findPagerUserReturnVisit(UserReturnVisitQueryDTO queryDTO, String sidx, String sord) {
		DetachedCriteria dc = DetachedCriteria.forClass(UserReturnVisit.class);
		Integer start = 0;
		Integer limit = 10;
		if (queryDTO != null) {
			start = queryDTO.getStart();
			limit = queryDTO.getLimit();
			// 回访用户
			if (StringUtils.hasText(queryDTO.getReturnVisitUser())) {
				dc.add(Restrictions.like("returnVisitUser", queryDTO.getReturnVisitUser(), MatchMode.ANYWHERE));
			}
			// 提交回访用户
			if (StringUtils.hasText(queryDTO.getReturnVisitSubmitUser())) {
				dc.add(Restrictions.like("returnVisitSubmitUser", queryDTO.getReturnVisitSubmitUser(), MatchMode.ANYWHERE));
			}
			// 满意度
			if (queryDTO.getSatisfaction() != null) {
				dc.add(Restrictions.eq("satisfaction", queryDTO.getSatisfaction()));
			}
			// 状态
			if (queryDTO.getState() != null) {
				dc.add(Restrictions.eq("state", queryDTO.getState()));
			}
			// 回访方式
			if (queryDTO.getReturnVisitMode() != null) {
				dc.add(Restrictions.eq("returnVisitMode", queryDTO.getReturnVisitMode()));
			}
			// 回访提交时间
			if (queryDTO.getReturnVisitSubmitTimeStart() != null && queryDTO.getReturnVisitSubmitTimeEnd() == null) {
				dc.add(Restrictions.ge("returnVisitSubmitTime", queryDTO.getReturnVisitSubmitTimeStart()));
			}
			if (queryDTO.getReturnVisitSubmitTimeStart() == null && queryDTO.getReturnVisitSubmitTimeEnd() != null) {
				dc.add(Restrictions.le("returnVisitSubmitTime", queryDTO.getReturnVisitSubmitTimeEnd()));
			}

			if (queryDTO.getReturnVisitSubmitTimeStart() != null && queryDTO.getReturnVisitSubmitTimeEnd() != null) {
				Calendar endTimeCl = new GregorianCalendar();
				endTimeCl.setTime(queryDTO.getReturnVisitSubmitTimeEnd());
				endTimeCl.set(Calendar.DATE, endTimeCl.get(Calendar.DATE) + 1);
				dc.add(Restrictions.and(Restrictions.le("returnVisitSubmitTime", endTimeCl.getTime()), Restrictions.ge("returnVisitSubmitTime", queryDTO.getReturnVisitSubmitTimeStart())));
			}

			// 回访时间
			if (queryDTO.getReturnVisitTimeStart() != null && queryDTO.getReturnVisitTimeEnd() == null) {
				dc.add(Restrictions.ge("returnVisitTime", queryDTO.getReturnVisitTimeStart()));
			}
			if (queryDTO.getReturnVisitTimeStart() == null && queryDTO.getReturnVisitTimeEnd() != null) {
				dc.add(Restrictions.le("returnVisitTime", queryDTO.getReturnVisitTimeEnd()));
			}

			if (queryDTO.getReturnVisitTimeStart() != null && queryDTO.getReturnVisitTimeEnd() != null) {

				Calendar endTimeCl = new GregorianCalendar();
				endTimeCl.setTime(queryDTO.getReturnVisitTimeEnd());
				endTimeCl.set(Calendar.DATE, endTimeCl.get(Calendar.DATE) + 1);

				dc.add(Restrictions.and(Restrictions.le("returnVisitTime", endTimeCl.getTime()), Restrictions.ge("returnVisitTime", queryDTO.getReturnVisitTimeStart())));
			}

			if (queryDTO.getCompanyNo() != null && !queryDTO.getCompanyNo().toString().equals("-1")) {
				dc.add(Restrictions.eq("companyNo", queryDTO.getCompanyNo()));
			}
			// 请求详细回访事项
			if (queryDTO.getEno() != null && !queryDTO.getEno().toString().equals("-1")) {
				dc.add(Restrictions.eq("eno", queryDTO.getEno()));
			}
			// 排序
			dc = DaoUtils.orderBy(sidx, sord, dc);
		}
		return super.findPageByCriteria(dc, start, limit);
	}
}
