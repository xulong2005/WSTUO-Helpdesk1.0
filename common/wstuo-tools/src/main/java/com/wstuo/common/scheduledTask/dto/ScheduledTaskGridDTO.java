package com.wstuo.common.scheduledTask.dto;

import java.util.Date;

/**
 * 定期任务列表DTO类
 * @author WSTUO
 *
 */
public class ScheduledTaskGridDTO {
	private Long scheduledTaskId;//ID
	private String title;//标题
	private String description;//描述
	private Date lastUpdateTime;
	private Date createTime;
	private String companyName;
	private Date taskDate;//具体时间(开始时间)
	private Date taskEndDate;//结束时间
	private String scheduledTaskType;
	private String timeType;
	public Long getScheduledTaskId() {
		return scheduledTaskId;
	}
	public void setScheduledTaskId(Long scheduledTaskId) {
		this.scheduledTaskId = scheduledTaskId;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Date getLastUpdateTime() {
		return lastUpdateTime;
	}
	public void setLastUpdateTime(Date lastUpdateTime) {
		this.lastUpdateTime = lastUpdateTime;
	}
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	public String getCompanyName() {
		return companyName;
	}
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	public Date getTaskDate() {
		return taskDate;
	}
	public void setTaskDate(Date taskDate) {
		this.taskDate = taskDate;
	}
	public Date getTaskEndDate() {
		return taskEndDate;
	}
	public void setTaskEndDate(Date taskEndDate) {
		this.taskEndDate = taskEndDate;
	}
	public String getScheduledTaskType() {
		return scheduledTaskType;
	}
	public void setScheduledTaskType(String scheduledTaskType) {
		this.scheduledTaskType = scheduledTaskType;
	}
	public String getTimeType() {
		return timeType;
	}
	public void setTimeType(String timeType) {
		this.timeType = timeType;
	}

	
}
