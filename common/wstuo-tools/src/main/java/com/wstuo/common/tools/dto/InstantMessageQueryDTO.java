package com.wstuo.common.tools.dto;

import java.util.Date;

import com.wstuo.common.dto.AbstractValueObject;

/**
 * 即时信息查询DTO类
 * 
 * @author brain date 2010-10-11
 */
public class InstantMessageQueryDTO extends AbstractValueObject {
	/**
	 * keyWord
	 */
	private String keyWord;
	private String sendUsers;
	private String content;
	private String title;
	private String status;
	private String receiveUsers;
	private String receiverFullName;
	private String senderFullName;
	/**
	 * start
	 */
	private Integer start;
	/**
	 * limit
	 */
	private Integer limit;

	private Date sendSatrt;
	private Date sendEnd;
	private String imType;
	private Long requestEno;
	private String isReadRequest;// 0为未查看请求详细 1未已查看请求详细

	public String getIsReadRequest() {
		return isReadRequest;
	}

	public void setIsReadRequest(String isReadRequest) {
		this.isReadRequest = isReadRequest;
	}

	public String getImType() {
		return imType;
	}

	public void setImType(String imType) {
		this.imType = imType;
	}

	public Long getRequestEno() {
		return requestEno;
	}

	public void setRequestEno(Long requestEno) {
		this.requestEno = requestEno;
	}

	public String getKeyWord() {
		return keyWord;
	}

	public void setKeyWord(String keyWord) {
		this.keyWord = keyWord;
	}

	public Integer getStart() {
		return start;
	}

	public void setStart(Integer start) {
		this.start = start;
	}

	public Integer getLimit() {
		return limit;
	}

	public void setLimit(Integer limit) {
		this.limit = limit;
	}

	public String getSendUsers() {
		return sendUsers;
	}

	public void setSendUsers(String sendUsers) {
		this.sendUsers = sendUsers;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getReceiveUsers() {
		return receiveUsers;
	}

	public void setReceiveUsers(String receiveUsers) {
		this.receiveUsers = receiveUsers;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Date getSendSatrt() {
		return sendSatrt;
	}

	public void setSendSatrt(Date sendSatrt) {
		this.sendSatrt = sendSatrt;
	}

	public Date getSendEnd() {
		return sendEnd;
	}

	public void setSendEnd(Date sendEnd) {
		this.sendEnd = sendEnd;
	}

	public String getReceiverFullName() {
		return receiverFullName;
	}

	public void setReceiverFullName(String receiverFullName) {
		this.receiverFullName = receiverFullName;
	}

	public String getSenderFullName() {
		return senderFullName;
	}

	public void setSenderFullName(String senderFullName) {
		this.senderFullName = senderFullName;
	}

}
