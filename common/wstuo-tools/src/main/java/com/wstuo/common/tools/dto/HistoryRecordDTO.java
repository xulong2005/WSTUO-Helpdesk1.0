package com.wstuo.common.tools.dto;


import java.util.Date;

import com.wstuo.common.dto.BaseDTO;

/**
 * 历史操作DTO
 * @author WSTUO
 *
 */

@SuppressWarnings("serial")
public class HistoryRecordDTO extends BaseDTO {
	private Long logNo;
	private String operator;
	private String logTitle;
	private String logDetails;
	private Date createdTime;
	private Long eno;
	private String eventType;
	public Long getLogNo() {
		return logNo;
	}
	public void setLogNo(Long logNo) {
		this.logNo = logNo;
	}
	public String getOperator() {
		return operator;
	}
	public void setOperator(String operator) {
		this.operator = operator;
	}
	public String getLogTitle() {
		return logTitle;
	}
	public void setLogTitle(String logTitle) {
		this.logTitle = logTitle;
	}
	public String getLogDetails() {
		return logDetails;
	}
	public void setLogDetails(String logDetails) {
		this.logDetails = logDetails;
	}
	public Date getCreatedTime() {
		return createdTime;
	}
	public void setCreatedTime(Date createdTime) {
		this.createdTime = createdTime;
	}
	public Long getEno() {
		return eno;
	}
	public void setEno(Long eno) {
		this.eno = eno;
	}
	public String getEventType() {
		return eventType;
	}
	public void setEventType(String eventType) {
		this.eventType = eventType;
	}
}
