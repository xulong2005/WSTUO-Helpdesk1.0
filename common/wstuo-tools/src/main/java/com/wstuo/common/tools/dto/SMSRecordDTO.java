package com.wstuo.common.tools.dto;

import java.util.Date;

import com.wstuo.common.dto.BaseDTO;

/**
 * 短信历史记录DTO
 * @author Administrator
 *
 */
public class SMSRecordDTO extends BaseDTO {
	
	private Long id;
	private String sender;
	private String mobile;
	private String content;
	private Date sendTime;
	private Double cost;
	
	private Date sendTime_end;
	private Date sendTime_start;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getSender() {
		return sender;
	}
	public void setSender(String sender) {
		this.sender = sender;
	}
	public String getMobile() {
		return mobile;
	}
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public Date getSendTime() {
		return sendTime;
	}
	public void setSendTime(Date sendTime) {
		this.sendTime = sendTime;
	}
	public Double getCost() {
		return cost;
	}
	public void setCost(Double cost) {
		this.cost = cost;
	}
	public Date getSendTime_end() {
		return sendTime_end;
	}
	public void setSendTime_end(Date sendTime_end) {
		this.sendTime_end = sendTime_end;
	}
	public Date getSendTime_start() {
		return sendTime_start;
	}
	public void setSendTime_start(Date sendTime_start) {
		this.sendTime_start = sendTime_start;
	}
	

}
