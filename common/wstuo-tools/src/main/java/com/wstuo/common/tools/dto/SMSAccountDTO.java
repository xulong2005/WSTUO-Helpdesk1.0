package com.wstuo.common.tools.dto;

import com.wstuo.common.dto.BaseDTO;

/**
 * 短信连接信息DTO类
 * @author QXY
 * date 2010-10-11
 */
@SuppressWarnings("serial")
public class SMSAccountDTO extends BaseDTO
{

	private Long said;
	private String orgId;
	private String userName;
	private String pwd;
	private String smsInstance;
	private Integer smsLength;
	private Integer moblieCount;
	
	public Long getSaid() {
		return said;
	}
	public void setSaid(Long said) {
		this.said = said;
	}
	public String getOrgId() {
		return orgId;
	}
	public void setOrgId(String orgId) {
		this.orgId = orgId;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getPwd() {
		return pwd;
	}
	public void setPwd(String pwd) {
		this.pwd = pwd;
	}
	public String getSmsInstance() {
		return smsInstance;
	}
	public void setSmsInstance(String smsInstance) {
		this.smsInstance = smsInstance;
	}
	public Integer getSmsLength() {
		return smsLength;
	}
	public void setSmsLength(Integer smsLength) {
		this.smsLength = smsLength;
	}
	public Integer getMoblieCount() {
		return moblieCount;
	}
	public void setMoblieCount(Integer moblieCount) {
		this.moblieCount = moblieCount;
	}
	
	@Override
	public String toString() {
	    
	    StringBuffer result = new StringBuffer();
	    result.append("SMSAccountDTO ")
	    .append("[")
	    .append("said=").append(said).append(", ")
	    .append("orgId=").append(orgId).append(", ")
	    .append("userName=").append(userName).append(", ")
	    .append("pwd=").append(pwd).append(", ")
	    .append("smsInstance=").append(smsInstance).append(", ")
	    .append("smsLength=").append(smsLength).append(", ")
	    .append("moblieCount=").append(moblieCount)
	    .append("]");
	    
		return result.toString();
	}
	
	
}
