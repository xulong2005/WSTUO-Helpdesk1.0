package com.wstuo.common.noticeRule.dao;

import com.wstuo.common.noticeRule.dto.NoticeRuleDTO;
import com.wstuo.common.noticeRule.entity.NoticeRule;
import com.wstuo.common.dao.IEntityDAO;
import com.wstuo.common.dto.PageDTO;

/**
 * 通知规则DAO接口类
 * @author QXY 
 *
 */
public interface INoticeRuleDAO extends IEntityDAO<NoticeRule> {
	/**
	 * 分页查询通知规则
	 * @param noticeRuleDTO
	 * @param start
	 * @param limit
	 * @param sord
	 * @param sidx
	 * @return PageDTO
	 */
	PageDTO findPager(NoticeRuleDTO noticeRuleDTO, int start, int limit,String sord, String sidx);
}
