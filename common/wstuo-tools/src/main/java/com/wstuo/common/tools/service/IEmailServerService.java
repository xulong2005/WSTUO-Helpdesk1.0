package com.wstuo.common.tools.service;

import com.wstuo.common.tools.dto.EmailServerDTO;

/***
 * ServicesEmail Service interface class.
 * @author QXY
 */
public interface IEmailServerService
{
    /**
     * Find  Email
     *
     * */
    EmailServerDTO findEmail();
    /**
     * 接收邮箱
     * @return
     */
    EmailServerDTO findReceiveEmail();
   
    /**
     * preservation Email server
     * */
    void saveOrUpdateEmailServer( EmailServerDTO emailServerDTO );
}
