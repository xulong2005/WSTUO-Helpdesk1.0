package com.wstuo.common.customForm.service;

import java.util.List;

import com.wstuo.common.customForm.dto.FieldDTO;
import com.wstuo.common.customForm.entity.Field;
import com.wstuo.common.dto.PageDTO;

public interface IFieldService {
	
	PageDTO findPageField(Field field,int start,int limit,String sidx,String  sord);

	boolean add(Field field);
	
	boolean update(Field field);
	
	boolean del(Long[] ids);
	
	Field findfieldById(Long id);
	
	PageDTO findByCustom(String table,int start,int limit,String sidx,String  sord);
	
	List<FieldDTO> findAll(String module);
}
