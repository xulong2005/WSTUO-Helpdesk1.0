package com.wstuo.common.tools.dto;

/**
 * Email 帐号连接信息DTO
 * 
 * @author brain date 2010/11/8 remark Reference is someone else's mail server
 * **/
public class EmailConnectionDTO {
	private String pop3host; // 连接的接收服务器地址
	private String smtphost; // 连接的发送服务器地址
	private String username; // 用户�?
	private String password; // 用户密码
	private Integer pop3port; // 连接接收的端口号
	private Integer smtpport; // 连接发�?的端口号
	private String mailPath; // 附件本地路径

	public String getPop3host() {
		return pop3host;
	}

	public void setPop3host(String pop3host) {
		this.pop3host = pop3host;
	}

	public String getSmtphost() {
		return smtphost;
	}

	public String getMailPath() {
		return mailPath;
	}

	public void setMailPath(String mailPath) {
		this.mailPath = mailPath;
	}

	public void setSmtphost(String smtphost) {
		this.smtphost = smtphost;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Integer getPop3port() {
		return pop3port;
	}

	public void setPop3port(Integer pop3port) {
		this.pop3port = pop3port;
	}

	public Integer getSmtpport() {
		return smtpport;
	}

	public void setSmtpport(Integer smtpport) {
		this.smtpport = smtpport;
	}

}
