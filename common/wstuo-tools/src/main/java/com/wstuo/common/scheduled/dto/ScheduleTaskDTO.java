package com.wstuo.common.scheduled.dto;

import java.util.Date;

/**
 * 人员行程任务DTO
 * @author WSTUO
 *
 */
public class ScheduleTaskDTO {
	//标题
	private String title;
	//状态
	private String status;
	//计划开始时间
	private Date startTime;
	//计划结束时间
	private Date endTime;
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Date getStartTime() {
		return startTime;
	}
	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}
	public Date getEndTime() {
		return endTime;
	}
	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}
	
}
