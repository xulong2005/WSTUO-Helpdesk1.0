package com.wstuo.common.tools.dao;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.util.StringUtils;
import com.wstuo.common.tools.dto.EventTaskDTO;
import com.wstuo.common.tools.dto.EventTaskQueryDTO;
import com.wstuo.common.tools.entity.EventTask;
import com.wstuo.common.dao.BaseDAOImplHibernate;
import com.wstuo.common.dto.PageDTO;

/**
 * EventTaskDAO DAO Class
 * 
 * @author WSTUO
 * 
 */
public class EventTaskDAO extends BaseDAOImplHibernate<EventTask> implements
		IEventTaskDAO {
	/**
	 * 分页查询任务
	 * @param queryDto
	 * @param start
	 * @param limit
	 * @return PageDTO
	 */
	public PageDTO findPagerEventTask(EventTaskQueryDTO queryDto, int start,
			int limit) {
		final DetachedCriteria dc = DetachedCriteria.forClass(EventTask.class);
		if (queryDto != null) {
			if (queryDto.getEno() != null) {
				dc.add(Restrictions.eq("eno", queryDto.getEno()));
			}

			if (StringUtils.hasText(queryDto.getEventType())) {
				dc.add(Restrictions.eq("eventType", queryDto.getEventType()));
			}
			if (StringUtils.hasText(queryDto.getOwner())) {
				dc.createAlias("owner", "on").add(
						Restrictions.eq("on.loginName", queryDto.getOwner()));
			}

			// SimpleDateFormat sdf = new
			// 开始时间
			if (queryDto.getStartTime() != null
					&& queryDto.getEndTime() != null) {
				// dc.add(Restrictions.ge("startTime",
				// queryDto.getStartTime()));
				Calendar endTimeCl = new GregorianCalendar();
				endTimeCl.setTime(queryDto.getEndTime());
				endTimeCl.set(Calendar.DATE, endTimeCl.get(Calendar.DATE) + 1);
				dc.add(Restrictions.or(
						Restrictions.or(
								Restrictions.between("startTime",
										queryDto.getStartTime(),
										queryDto.getEndTime()),
								Restrictions.between("endTime",
										queryDto.getStartTime(),
										endTimeCl.getTime())),
						Restrictions.and(
								Restrictions.le("startTime",
										queryDto.getStartTime()),
								Restrictions.ge("endTime",
										queryDto.getEndTime()))));

			}
		}
		// 排序
		if (queryDto != null && StringUtils.hasText(queryDto.getSord())
				&& StringUtils.hasText(queryDto.getSidx())) {
			if (queryDto.getSord().equals("desc"))
				dc.addOrder(Order.desc(queryDto.getSidx()));
			else
				dc.addOrder(Order.asc(queryDto.getSidx()));
		}

		return super.findPageByCriteria(dc, start, limit);
	}

	/**
	 * 查询指定时间段任务
	 * @param eventTaskDTO
	 * @return boolean
	 */
	public boolean findSameTimeEventTask(EventTaskDTO eventTaskDTO) {
		boolean bl;
		if (eventTaskDTO.getStartTime() != null
				&& eventTaskDTO.getEndTime() != null
				&& eventTaskDTO.getOwner() != null) {
			/*
			 * if(eventTaskDTO.isAllDay()){ return false; }else{}
			 */
			if (eventTaskDTO.getTaskStatus()!=null&&eventTaskDTO.getTaskStatus() == 2) {// 完成状态
				bl= false;
			} else {
				String hql = " from EventTask tk where tk.owner.loginName=? and (tk.startTime between ? and ? or tk.endTime between ? and ? )and tk.taskStatus!=2";
				if (eventTaskDTO.getTaskId() != null)// will
					hql += " and taskId!=" + eventTaskDTO.getTaskId();
				List<EventTask> tasks = super.getHibernateTemplate().find(hql,
						eventTaskDTO.getOwner(), eventTaskDTO.getStartTime(),
						eventTaskDTO.getEndTime(), eventTaskDTO.getStartTime(),
						eventTaskDTO.getEndTime());
				if (tasks != null && tasks.size() > 0) {
					bl= true;
				} else {
					bl= false;
				}
			}
		} else {
			bl= false;
		}
		return bl;
	}
	
	/**
	 * 查询人员行程管理任务
	 * @param queryDto
	 */
	public List<EventTask> findScheduleEventTask(EventTaskQueryDTO queryDto) {
		final DetachedCriteria dc = DetachedCriteria.forClass(EventTask.class);
//		dc.add(Restrictions.eq("eventType", "itsm.request"));
		if (queryDto != null) {
			if (queryDto.getEno() != null) {
				dc.add(Restrictions.eq("eno", queryDto.getEno()));
			}
			if (StringUtils.hasText(queryDto.getOwner())) {
				dc.createAlias("owner", "on").add(
						Restrictions.eq("on.loginName", queryDto.getOwner()));
			}

			// SimpleDateFormat sdf = new
			// 开始时间
			if (queryDto.getStartTime() != null
					&& queryDto.getEndTime() != null) {
				// dc.add(Restrictions.ge("startTime",
				// queryDto.getStartTime()));
				Calendar endTimeCl = new GregorianCalendar();
				endTimeCl.setTime(queryDto.getEndTime());
				endTimeCl.set(Calendar.DATE, endTimeCl.get(Calendar.DATE) + 1);
				dc.add(Restrictions.or(
						Restrictions.or(
								Restrictions.between("startTime",
										queryDto.getStartTime(),
										queryDto.getEndTime()),
								Restrictions.between("endTime",
										queryDto.getStartTime(),
										endTimeCl.getTime())),
						Restrictions.and(
								Restrictions.le("startTime",
										queryDto.getStartTime()),
								Restrictions.ge("endTime",
										queryDto.getEndTime()))));

			}
		}
		// 排序
		if (queryDto != null && StringUtils.hasText(queryDto.getSord())
				&& StringUtils.hasText(queryDto.getSidx())) {
			if (queryDto.getSord().equals("desc"))
				dc.addOrder(Order.desc(queryDto.getSidx()));
			else
				dc.addOrder(Order.asc(queryDto.getSidx()));
		}

		return super.getHibernateTemplate().findByCriteria(dc);
	}

}
