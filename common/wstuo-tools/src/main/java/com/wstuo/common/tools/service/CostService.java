package com.wstuo.common.tools.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.wstuo.common.tools.dao.ICostDAO;
import com.wstuo.common.tools.dao.IEventTaskDAO;
import com.wstuo.common.tools.dao.ITaskDAO;
import com.wstuo.common.tools.dto.CostDTO;
import com.wstuo.common.tools.dto.TaskDTO;
import com.wstuo.common.tools.entity.Cost;
import com.wstuo.common.tools.entity.EventTask;
import com.wstuo.common.tools.entity.Task;
import com.wstuo.common.dto.PageDTO;
import com.wstuo.common.security.dao.IUserDAO;

/**
 * cost service class
 * @author WSTUO
 *
 */
public class CostService implements ICostService{
	@Autowired
	private ICostDAO costDAO;
	@Autowired
	private IUserDAO userDAO;
	@Autowired
	private IEventTaskDAO eventTaskDAO;
	@Autowired
	private ITaskDAO taskDao;
	
	/**
	 * 成本分页查询
	 * @param dto
	 * @param start
	 * @param limit
	 * @param sidx
	 * @param sord
	 * @return PageDTO
	 */
	@SuppressWarnings("unchecked")
	@Transactional
    public PageDTO findPagerCost( CostDTO dto, int start, int limit,String sidx,String sord ){
		PageDTO p = costDAO.findPagerCost(dto, start, limit, sidx, sord);
        List<Cost> cost = (List<Cost>) p.getData();
        List<CostDTO> costDtos = new ArrayList<CostDTO>( cost.size(  ) );
        for ( Cost entity : cost ){
        	CostDTO costDTO=new CostDTO();
        	CostDTO.entity2dto(entity, costDTO);
        	if(entity.getUsers()!=null){
        		costDTO.setUserId(entity.getUsers().getUserId());
        		costDTO.setUserName(entity.getUsers().getLoginName());
        		costDTO.setFullName(entity.getUsers().getFullName());
        	}
        	if(entity.getEventTask()!=null){
        		costDTO.setTaskId(entity.getEventTask().getTaskId());
        		costDTO.setTitle(entity.getEventTask().getTitle());
        	}
        	if(entity.getScheduleTask() !=null){
        		costDTO.setScheduleTaskId( entity.getScheduleTask().getTaskId());
        	}
        	costDtos.add(costDTO);
        }
        p.setData( costDtos );
        return p;
    }
    /**
     * 保存成本
     * @param dto
     */
    @Transactional
    public void saveCost(CostDTO dto){
    	Cost cost =new Cost();
    	dto2entity(dto, cost);
    	costDAO.save(cost);
    	dto.setProgressId(cost.getProgressId());
    	
    }
    /**
     * 编辑成本
     * @param dto
     */
    @Transactional
    public void editCost(CostDTO dto){
    	Cost cost =costDAO.findById(dto.getProgressId());
    	dto2entity(dto, cost);
    	costDAO.merge(cost);
    }
    /**
     * 删除成本
     * @param ids
     */
    @Transactional
    public void deleteCost(final Long[] ids){
    	costDAO.deleteByIds(ids);
    }
    /**
     *根据任务ID删除进展及成本
     * @param ids
     *///final Long[] scheduleTaskIds){
    @Transactional
    public void deleteCostBySchedule(final List<Long> scheduleTaskIds){
    	if (scheduleTaskIds != null && scheduleTaskIds.size() > 0) {
    		Long[] ids = scheduleTaskIds.toArray(new Long[scheduleTaskIds.size()]);
			costDAO.deleteCostBySchedule( ids );
		}
    }
    /**
     * 删除成本
     * @param ids
     */
    @Transactional
    public void deleteCost(final List<CostDTO> dtos){
    	if (dtos != null && dtos.size() > 0) {
    		List<Long> ids = new ArrayList<Long>();
			for (CostDTO dto : dtos) {
				if (dto != null && dto.checkProgressId()) {
					ids.add(dto.getProgressId());
				}
			}
			if (ids.size() > 0) {
				costDAO.deleteByIds( ids.toArray(new Long[ids.size()]));
			}
		}
    }
    /**
     * 根据ID查找进展及成本
     * @param id
     * @return CostDTO
     */
    @Transactional
    public CostDTO findCostById(Long id){
    	CostDTO costDTO=new CostDTO();
    	Cost cost =costDAO.findById(id);
    	CostDTO.entity2dto(cost, costDTO);
    	if(cost != null){
	    	if(cost.getUsers()!=null){
	    		costDTO.setUserId(cost.getUsers().getUserId());
	    		costDTO.setUserName(cost.getUsers().getLoginName());
	    		costDTO.setFullName(cost.getUsers().getFullName());
	    	}
	    	if(cost.getEventTask()!=null){
	    		costDTO.setTaskId(cost.getEventTask().getTaskId());
	    		costDTO.setTitle(cost.getEventTask().getTitle());
	    	}
    	}
    	return costDTO;
    }
    public CostDTO findScheduleCost(TaskDTO dto){
    	CostDTO costDTO = new CostDTO();
    	if (dto != null && dto.getTaskId() != null) {
    		List<Cost> costs = costDAO.findScheduleCost( dto.getTaskId(), dto.getStartTime() 
    				, dto.getEndTime() , dto.getTaskCycle() );
    		if(costs != null && costs.size() > 0){
				entity2dto(costs.get(0), costDTO);
			}
		}
    	return costDTO;
    }
    
    public List<CostDTO> findTaskCostWithTaskId(Long taskId){
    	List<CostDTO> costDTOs = new ArrayList<CostDTO>();
    	if (taskId != null) {
    		List<Cost> costs = costDAO.findScheduleCost( taskId,null,null,null );
    		if(costs != null){
    			for (Cost cost : costs) {
    				CostDTO costDTO = new CostDTO();
    				entity2dto(cost, costDTO);
    				costDTOs.add(costDTO);
				}
			}
		}
    	return costDTOs;
    }
    
    public void dto2entity(CostDTO dto,Cost entity){
    	if (dto != null && entity != null) {
			CostDTO.dto2entity(dto, entity);
			if(dto.getUserId()!=null){
				entity.setUsers(userDAO.findById(dto.getUserId()));
	    	}else if( dto.getUserName() != null ){
	    		entity.setUsers(userDAO.findUserByLoginName(dto.getUserName()));
	    	}
	    	if(dto.getTaskId()!=null){
	    		EventTask eventTask=eventTaskDAO.findById(dto.getTaskId());
	    		entity.setEventTask(eventTask);
	    	}else{
	    		entity.setEventTask(null);
	    	}
	    	if (dto.getScheduleTaskId() != null) {
	    		Task task = taskDao.findById(dto.getScheduleTaskId());
	    		entity.setScheduleTask(task);
			}else{
				entity.setScheduleTask(null);
			}
		}
    }
    public void entity2dto(Cost entity,CostDTO dto){
    	if (dto != null && entity != null) {
			CostDTO.entity2dto( entity,dto);
			if(entity.getUsers()!=null){
				dto.setUserId(entity.getUsers().getUserId());
				dto.setUserName(entity.getUsers().getLoginName());
				dto.setFullName(entity.getUsers().getFullName());
	    	}
	    	if(entity.getEventTask()!=null){
	    		dto.setTaskId(entity.getEventTask().getTaskId());
	    		dto.setTitle(entity.getEventTask().getTitle());
	    	}
	    	if(entity.getScheduleTask() !=null){
	    		dto.setScheduleTaskId(entity.getScheduleTask().getTaskId());
	    	}
		}
    }
}
