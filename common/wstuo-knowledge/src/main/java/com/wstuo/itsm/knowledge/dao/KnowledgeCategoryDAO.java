package com.wstuo.itsm.knowledge.dao;


import com.wstuo.itsm.knowledge.entity.KnowledgeCategory;
import com.wstuo.common.dao.BaseDAOImplHibernate;

import java.util.List;

/**
 * KnowledgeDAO Bean 2010.9.13
 *
 * @author spring
 *
 */
public class KnowledgeCategoryDAO
    extends BaseDAOImplHibernate<KnowledgeCategory>
    implements IKnowledgeCategoryDAO
{
    /**
     * find all Knowledge
     *
     * @return List<Knowledge>
     */
    @SuppressWarnings( "unchecked" )
    public List<KnowledgeCategory> findKnowledge( String dtype )
    {
        String hql =
            "from KnowledgeCategory kn where kn.parent IS NULL and DTYPE = '" + dtype + "' order by kn.cno asc";

        return getHibernateTemplate(  ).find( hql );
    }
}
