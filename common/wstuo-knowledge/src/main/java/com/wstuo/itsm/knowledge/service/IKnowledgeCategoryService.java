package com.wstuo.itsm.knowledge.service;

import com.wstuo.common.config.category.dto.CategoryDTO;
import com.wstuo.common.config.category.dto.CategoryTreeViewDTO;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

/**
 * KnowledgeCategoryService Interface 2010.9.13
 *
 * @author spring
 *
 */
public interface IKnowledgeCategoryService
{
    /**
     * Check all KnowledgeCategoryTree
     *
     * @return List<CategoryTreeViewDTO>
     */
    public List<CategoryTreeViewDTO> findKnowledgeCategoryTreeDtos( String name );

    /**
     * Save KnowledgeCategory
     * @param dto
     */
    public void saveKnowledgeCategory( CategoryDTO dto );

    /**
     * Remove KnowledgeCategory
     * @param kno
     */
    public void removeKnowledgeCategory( Long kno );

    /**
     * Update KnowledgeCategory
     * @param dto
     */
    public void updateKnowledgeCategory( CategoryDTO dto );

    /**
     * Change KnowledgeCategory
     * @param dto
     */
    public void changeKnowledgeCategory( CategoryDTO dto );
    
    /**
     * find KnowledgeCategory
     *
     * @param cno
     */
    @Transactional
    public CategoryDTO findKnowledgeCategoryById(long cno );
}
