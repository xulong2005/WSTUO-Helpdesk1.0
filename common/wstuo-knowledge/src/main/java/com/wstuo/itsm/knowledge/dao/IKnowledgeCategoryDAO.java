package com.wstuo.itsm.knowledge.dao;


import com.wstuo.itsm.knowledge.entity.KnowledgeCategory;
import com.wstuo.common.dao.IEntityDAO;

import java.util.List;

/**
 * KnowledgeDAO Interface 2010.9.13
 *
 * @author spring
 *
 */
public interface IKnowledgeCategoryDAO
    extends IEntityDAO<KnowledgeCategory>
{
    /**
     * find all Knowledge
     *
     * @return List<Knowledge>
     */
    List<KnowledgeCategory> findKnowledge( String dtype );
}
