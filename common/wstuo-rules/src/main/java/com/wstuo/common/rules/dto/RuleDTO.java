package com.wstuo.common.rules.dto;

import org.apache.commons.beanutils.BeanUtils;

import com.wstuo.common.rules.entity.Rule;
import com.wstuo.common.dto.BaseDTO;
import com.wstuo.common.exception.ApplicationException;

/**
 * dto of rule
 * 
 * @author <a href="mailto:376890523@qq.com">jeff</a>
 * @version 0.1 2010.9.28
 * */
public class RuleDTO extends BaseDTO {
	/**
	 * rule no
	 */
	private Long ruleNo;
	/**
	 * rule name
	 */
	private String ruleName;
	/**
	 * dialect
	 */
	private String dialect;
	/**
	 * description
	 */
	private String description;
	/**
	 * rule package no
	 */
	private Long rulePackageNo;
	/**
	 * rule package name
	 */
	private String packageName;
	private Long salience;
	private String flagName;

	
	public String getFlagName() {
		return flagName;
	}

	public void setFlagName(String flagName) {
		this.flagName = flagName;
	}

	public Long getRuleNo() {
		return ruleNo;
	}

	public void setRuleNo(Long ruleNo) {
		this.ruleNo = ruleNo;
	}

	public String getRuleName() {
		return ruleName;
	}

	public void setRuleName(String ruleName) {
		this.ruleName = ruleName;
	}

	public String getDialect() {
		return dialect;
	}

	public void setDialect(String dialect) {
		this.dialect = dialect;
	}

	public Long getRulePackageNo() {
		return rulePackageNo;
	}

	public void setRulePackageNo(Long rulePackageNo) {
		this.rulePackageNo = rulePackageNo;
	}

	public String getPackageName() {
		return packageName;
	}

	public void setPackageName(String packageName) {
		this.packageName = packageName;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Long getSalience() {
		return salience;
	}

	public void setSalience(Long salience) {
		this.salience = salience;
	}

	public RuleDTO() {
		super();
	}

	public RuleDTO(Long ruleNo, String ruleName, String dialect, String description, Long rulePackageNo, String packageName) {
		super();
		this.ruleNo = ruleNo;
		this.ruleName = ruleName;
		this.dialect = dialect;
		this.description = description;
		this.rulePackageNo = rulePackageNo;
		this.packageName = packageName;
	}
	
	/**
	 * dto2entity Method
	 * 
	 * @param dto
	 * @param entity
	 */
	public static void dto2entity(RuleDTO dto, Rule entity) {
		try {
			BeanUtils.copyProperties(entity, dto);
		} catch (Exception ex) {
			throw new ApplicationException(ex);
		}
	}

	/**
	 * entity2dto Method
	 * 
	 * @param entity
	 * @param dto
	 */
	public static void entity2dto(Rule entity, RuleDTO dto) {
		try {
			BeanUtils.copyProperties(dto, entity);
		} catch (Exception ex) {
			throw new ApplicationException(ex);
		}
	}

}
