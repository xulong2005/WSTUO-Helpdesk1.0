package com.wstuo.common.rules.service;

import com.wstuo.common.rules.dto.RuleConstraintDTO;

import java.util.List;

/**
 * interface of RuleConstraintService
 *
 * @author <a href="mailto:376890523@qq.com">jeff</a>
 * @version 0.1 2010-9-30
 * */
public interface IRuleConstraintService
{
    /**
     * --------------save,remove,update Method--------------
     * */

    /**
     * save constraint
     *
     * @param dto
     */
    public void saveRuleConstraint( RuleConstraintDTO dto );

    /**
     * remove constraint
     *
     * @param no
     */
    public void removeRuleConstraint( Long no );

    /**
     * remove constraints
     *
     * @param nos
     */
    public void removeRuleConstraints( Long[] nos );

    /**
     * merge constraint
     *
     * @param dto
     * @return RuleConstraintDTO
     */
    public RuleConstraintDTO mergeRuleConstraint( RuleConstraintDTO dto );

    /**
     * merge constraints
     *
     * @param dtos
     */
    public void mergeAllRuleConstraint( List<RuleConstraintDTO> dtos );

    /**
     * --------------find Method--------------
     * */

    /**
     * find all constraint
     *
     * @return List<RuleConstraintDTO>
     */
    public List<RuleConstraintDTO> findRuleConstraints(  );

    /**
     * find constraint by id
     *
     * @param no
     * @return RuleConstraintDTO
     */
    public RuleConstraintDTO findRuleConstraintById( Long no );

    /**
     * find constraint by PatternNo
     *
     * @param patternNo
     * @return List<RuleConstraintDTO>
     */
    public List<RuleConstraintDTO> findRuleConstraintByPatternNo( Long patternNo );
}
