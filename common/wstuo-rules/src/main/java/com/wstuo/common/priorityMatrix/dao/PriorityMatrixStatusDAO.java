package com.wstuo.common.priorityMatrix.dao;

import com.wstuo.common.priorityMatrix.entity.PriorityMatrixStatus;
import com.wstuo.common.dao.BaseDAOImplHibernate;

/**
 * 优先级矩阵状态DAO类
 * @author WSTUO
 *
 */
public class PriorityMatrixStatusDAO extends BaseDAOImplHibernate<PriorityMatrixStatus> implements IPriorityMatrixStatusDAO {

}
