package com.wstuo.common.rules.service;

import com.wstuo.common.rules.dto.RuleActionDTO;

import java.util.List;

/**
 * interface of RuleActionService
 * @author <a href="mailto:376890523@qq.com">jeff</a>
 * @version 0.1  2010-9-30
 * */
public interface IRuleActionService{

    /**
     * save action
     * @param dto
     */
    void saveRuleAction( RuleActionDTO dto );

    /**
     * remove action
     * @param no
     */
    void removeRuleAction( Long no );

    /**
     * remove actions
     * @param nos
     */
    void removeRuleActions( Long[] nos );

    /**
     * merge action
     * @param dto
     * @return RuleActionDTO
     */
    RuleActionDTO mergeRuleAction( RuleActionDTO dto );

    /**
     * merge actions
     * @param dtos
     */
    void mergeAllRuleAction( List<RuleActionDTO> dtos );

    /**
     * find all actions
     * @return List<RuleActionDTO>
     */
    List<RuleActionDTO> findRuleActions();

    /**
     * find action by id
     * @param no
     * @return RuleActionDTO
     */
    RuleActionDTO findRuleActionById( Long no );

    /**
     * find action by ruleNo
     * @param ruleNo
     * @return List<RuleActionDTO>
     */
    List<RuleActionDTO> findRuleActionByRuleNo( Long ruleNo );
}
