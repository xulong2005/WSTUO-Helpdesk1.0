package com.wstuo.common.priorityMatrix.action;

import org.springframework.beans.factory.annotation.Autowired;

import com.opensymphony.xwork2.ActionSupport;
import com.wstuo.common.priorityMatrix.dto.PriorityMatrixDTO;
import com.wstuo.common.priorityMatrix.dto.PriorityMatrixDataShowDTO;
import com.wstuo.common.priorityMatrix.dto.PriorityMatrixStatusDTO;
import com.wstuo.common.priorityMatrix.service.IPriorityMatrixService;
import com.wstuo.common.priorityMatrix.service.IPriorityMatrixStatusService;

/**
 * 优先级矩阵Action类
 * @author WSTUO
 *
 */
@SuppressWarnings("serial")
public class PriorityMatrixAction extends ActionSupport {
	@Autowired
	private IPriorityMatrixService priorityMatrixService;
	@Autowired
	private IPriorityMatrixStatusService priorityMatrixStatusService;
	private PriorityMatrixDataShowDTO dataShowDTO=new PriorityMatrixDataShowDTO();
	private PriorityMatrixDTO priorityMatrixDTO=new PriorityMatrixDTO();
	private PriorityMatrixStatusDTO priorityMatrixStatusDTO=new PriorityMatrixStatusDTO();
	
	
	public PriorityMatrixDataShowDTO getDataShowDTO() {
		return dataShowDTO;
	}
	public void setDataShowDTO(PriorityMatrixDataShowDTO dataShowDTO) {
		this.dataShowDTO = dataShowDTO;
	}
	public PriorityMatrixDTO getPriorityMatrixDTO() {
		return priorityMatrixDTO;
	}
	public void setPriorityMatrixDTO(PriorityMatrixDTO priorityMatrixDTO) {
		this.priorityMatrixDTO = priorityMatrixDTO;
	}
	
	public PriorityMatrixStatusDTO getPriorityMatrixStatusDTO() {
		return priorityMatrixStatusDTO;
	}
	public void setPriorityMatrixStatusDTO(
			PriorityMatrixStatusDTO priorityMatrixStatusDTO) {
		this.priorityMatrixStatusDTO = priorityMatrixStatusDTO;
	}
	
	/**
	 * 保存优先级矩阵
	 * @return String
	 */
	public String savePriorityMatrix(){
		
		priorityMatrixService.savePriorityMatrix(priorityMatrixDTO);
		dataShowDTO=null;
		return SUCCESS;
	}
	
	/**
	 * 获取优先级矩阵显示数据
	 * @return String
	 */
	public String findPriorityMatrixDataShow(){
		dataShowDTO=priorityMatrixService.findPriorityMatrixDataShow();
		return SUCCESS;
	}
	
	/**
	 * 保存优先级矩阵状态
	 * @return String
	 */
	public String savePriorityMatrixStatus(){
		priorityMatrixStatusService.savePriorityMatrixStatus(priorityMatrixStatusDTO);
		return SUCCESS;
	}
	
	/**
	 * 查询优先级矩阵状态
	 * @return String
	 */
	public String findPriorityMatrixStatus(){
		priorityMatrixStatusDTO=priorityMatrixStatusService.findPriorityMatrixStatus();
		return "priorityMatrixStatusDTO";
	}
	
}
