package com.wstuo.common.rules.dto;

import com.wstuo.common.dto.BaseDTO;

/**
 * dto of rulepattern
 * 
 * @author <a href="mailto:376890523@qq.com">jeff</a>
 * @version 0.1 2010.9.28
 * */
public class RulePatternDTO extends BaseDTO {
	/**
	 * rule pattern no
	 */
	private Long rulePatternNo;

	/**
	 * pattern type
	 */
	private String patternType;

	/**
	 * pattern binding
	 */
	private String patternBinding;

	/**
	 * sequence
	 */
	private Integer sequence = 0;

	/**
	 * rule no
	 */
	private Long ruleNo;

	/**
	 * rule name
	 */
	private String ruleName;

	public Long getRulePatternNo() {
		return rulePatternNo;
	}

	public void setRulePatternNo(Long rulePatternNo) {
		this.rulePatternNo = rulePatternNo;
	}

	public String getPatternType() {
		return patternType;
	}

	public void setPatternType(String patternType) {
		this.patternType = patternType;
	}

	public String getPatternBinding() {
		return patternBinding;
	}

	public void setPatternBinding(String patternBinding) {
		this.patternBinding = patternBinding;
	}

	public Integer getSequence() {
		return sequence;
	}

	public void setSequence(Integer sequence) {
		this.sequence = sequence;
	}

	public Long getRuleNo() {
		return ruleNo;
	}

	public void setRuleNo(Long ruleNo) {
		this.ruleNo = ruleNo;
	}

	public String getRuleName() {
		return ruleName;
	}

	public void setRuleName(String ruleName) {
		this.ruleName = ruleName;
	}

	public RulePatternDTO() {
		super();
	}

	public RulePatternDTO(Long rulePatternNo, String patternType, String patternBinding, Integer sequence, Long ruleNo, String ruleName) {
		super();
		this.rulePatternNo = rulePatternNo;
		this.patternType = patternType;
		this.patternBinding = patternBinding;
		this.sequence = sequence;
		this.ruleNo = ruleNo;
		this.ruleName = ruleName;
	}

}
