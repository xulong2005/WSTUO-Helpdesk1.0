package com.wstuo.common.rules.drool;

public enum RulePathType {
	Request("request"),
	RequestMail("requestMail"),
	Change("change"),
	RequestWorkFlow("requestWorkFlow"),
	ChangeWorkFlow("changeWorkFlow"),
	Sla("sla");
	
	// 定义私有变量
	private String nCode;

	// 构造函数，枚举类型只能为私有
	private RulePathType(String _nCode) {
		this.nCode = _nCode;
	}

	public String getValue() {
		return nCode;
	}
}
