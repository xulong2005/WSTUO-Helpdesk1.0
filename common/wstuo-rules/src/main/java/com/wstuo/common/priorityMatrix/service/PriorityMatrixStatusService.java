package com.wstuo.common.priorityMatrix.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.wstuo.common.priorityMatrix.dao.IPriorityMatrixStatusDAO;
import com.wstuo.common.priorityMatrix.dto.PriorityMatrixStatusDTO;
import com.wstuo.common.priorityMatrix.entity.PriorityMatrixStatus;

/**
 * 优先级矩阵状态Service类
 * @author WSTUO
 *
 */
public class PriorityMatrixStatusService implements IPriorityMatrixStatusService{

	@Autowired
	private IPriorityMatrixStatusDAO priorityMatrixStatusDAO;
	
	/**
	 * 保存优先级状态
	 * @param dto 优先级矩阵DTO参数
	 */
	@Transactional
	public void savePriorityMatrixStatus(PriorityMatrixStatusDTO dto){
		PriorityMatrixStatus entity=priorityMatrixStatusDAO.findUniqueBy("type",dto.getType());
		if(entity!=null){
			entity.setPriorityMatrixStatus(dto.getPriorityMatrixStatus());
		}else{
			entity=new PriorityMatrixStatus();
			entity.setType(dto.getType());
			entity.setPriorityMatrixStatus(dto.getPriorityMatrixStatus());
		}
		
		priorityMatrixStatusDAO.save(entity);
		
	};
	/**
	 * 查询优先级状态
	 * @return PriorityMatrixStatusDTO
	 */
	@Transactional
	public PriorityMatrixStatusDTO findPriorityMatrixStatus(){
		PriorityMatrixStatusDTO dto=new PriorityMatrixStatusDTO();
		PriorityMatrixStatus entity=priorityMatrixStatusDAO.findUniqueBy("type","priorityMatrixStatus");
		PriorityMatrixStatusDTO.entity2dto(entity, dto);
		return dto;
		
	}
	
}
