package com.wstuo.common.rules.parser;

import com.wstuo.common.rules.entity.Rule;
import com.wstuo.common.rules.entity.RuleAction;
import com.wstuo.common.rules.entity.RuleConstraint;
import com.wstuo.common.rules.entity.RulePackage;
import com.wstuo.common.rules.entity.RulePattern;

/**
 * interface of RuleHandler
 * @author QXY
 * @version 0.1                2010.9.17
 * */
public interface IRuleHandler
{
    /**
     * onPackage Method
     * @param packageName
     * @return RulePackage
     */
    RulePackage onPackage( String packageName );

    /**
     * onImport Method
     * @param imports
     * @return RulePackage
     */
    RulePackage onImport( String imports );

    /**
     * onRule Method
     * @param ruleName
     * @param dialect
     * @return Rule
     */
    Rule onRule( String ruleName, String dialect );

    /**
     * onPattern Method
     * @param patternBinding
     * @param patternType
     * @return RulePattern
     */
    RulePattern onPattern( String patternBinding, String patternType );

    /**
     * onConstraint Method
     * @param propertyName
     * @param propertyValue
     * @param propertyType
     * @param sequence
     * @param andOr
     * @return RuleConstraint
     */
    RuleConstraint onConstraint( String propertyName, String propertyValue, Boolean propertyType, int sequence,
                                        String andOr );

    /**
     * onAction Method
     * @param propertyName
     * @param givenValue
     * @param sequence
     * @param andOr
     * @return RuleAction
     */
    RuleAction onAction( String propertyName, String givenValue, int sequence, String andOr );
}
