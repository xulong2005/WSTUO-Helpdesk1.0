package com.wstuo.common.priorityMatrix.dao;

import com.wstuo.common.priorityMatrix.dto.PriorityMatrixDTO;
import com.wstuo.common.priorityMatrix.entity.PriorityMatrix;
import com.wstuo.common.dao.IEntityDAO;

/**
 * 优先级矩阵DAO接口类
 * @author WSTUO
 */
public interface IPriorityMatrixDAO extends IEntityDAO<PriorityMatrix> {
	/**
	 * 根据紧急度、影响范围查找优先级矩阵
	 * @param dto PriorityMatrixDTO
	 * @return PriorityMatrix 返回优先级矩阵结果
	 */
	PriorityMatrix findPriorityMatrixByUrgencyAffect(PriorityMatrixDTO dto);
}