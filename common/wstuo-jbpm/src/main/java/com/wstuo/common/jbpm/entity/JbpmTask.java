package com.wstuo.common.jbpm.entity;

import java.util.Date;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity(name="JBPM4_TASK")
@Cacheable
public class JbpmTask {
	@Id
	@Column(name="DBID_")
	private Long id;
	@Column(name="ASSIGNEE_")
	private String assignee;
	@Column(name="CREATE_")
	private Date createTime;
	@Column(name="ACTIVITY_NAME_")
	private String activityName;
	@Column(name="NAME_")
	private String name;
	@Column(name="PRIORITY_")
	private int priority;
	@Column(name="DUEDATE_")
	private Date duedate;
	@Column(name="EXECUTION_ID_")
	private String executionId;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getPriority() {
		return priority;
	}
	public void setPriority(int priority) {
		this.priority = priority;
	}
	public Date getDuedate() {
		return duedate;
	}
	public void setDuedate(Date duedate) {
		this.duedate = duedate;
	}
	public String getExecutionId() {
		return executionId;
	}
	public void setExecutionId(String executionId) {
		this.executionId = executionId;
	}
	public String getActivityName() {
		return activityName;
	}
	public void setActivityName(String activityName) {
		this.activityName = activityName;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getAssignee() {
		return assignee;
	}
	public void setAssignee(String assignee) {
		this.assignee = assignee;
	}
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
}
