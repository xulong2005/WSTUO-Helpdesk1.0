package com.wstuo.common.jbpm.dao;

import org.jbpm.pvm.internal.history.model.HistoryTaskInstanceImpl;

import com.wstuo.common.dao.IEntityDAO;

public interface IJbpmHistoryTaskInstanceDAO extends IEntityDAO<HistoryTaskInstanceImpl>{
	/**
	 * 根据TaskId获取历史任务 
	 * @param taskId
	 */
	HistoryTaskInstanceImpl getHistoryTaskInstanceByTaskId(final String taskId);
	
}
