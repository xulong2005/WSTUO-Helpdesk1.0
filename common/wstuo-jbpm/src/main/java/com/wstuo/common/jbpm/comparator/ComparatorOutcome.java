package com.wstuo.common.jbpm.comparator;

import java.util.Comparator;

/**
 * 流程出口排序
 * @author wstuo
 *
 */
@SuppressWarnings("rawtypes")
public class ComparatorOutcome implements Comparator{
	public int compare(Object arg0, Object arg1) {
		  String set0=(String)arg0;
		  String set1=(String)arg1;
		  
		  if(set0.indexOf("一线")!=-1){
			  set0 = "1"+set0;
		  }
		  if(set0.indexOf("二线")!=-1){
			  set0 = "2"+set0;
		  }
		  if(set0.indexOf("三线")!=-1){
			  set0 = "3"+set0;
		  }
		  if(set0.indexOf("四线")!=-1){
			  set0 = "4"+set0;
		  }
		  
		  if(set1.indexOf("一线")!=-1){
			  set1 = "1"+set1;
		  }
		  if(set1.indexOf("二线")!=-1){
			  set1 = "2"+set1;
		  }
		  if(set1.indexOf("三线")!=-1){
			  set1 = "3"+set1;
		  }
		  if(set1.indexOf("四线")!=-1){
			  set1 = "4"+set1;
		  }
		  
		  return set0.compareTo(set1);
	}
}
