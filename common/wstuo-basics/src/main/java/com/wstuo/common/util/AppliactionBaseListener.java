package com.wstuo.common.util;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.springframework.context.ApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

public class AppliactionBaseListener implements ServletContextListener {
	
	public static ApplicationContext ctx=null;
	//@Override
	public void contextDestroyed(ServletContextEvent arg0) {
	
	}
	//@Override
	public void contextInitialized(ServletContextEvent event) {
		ctx = WebApplicationContextUtils.getWebApplicationContext(event.getServletContext());
	}
}
