package com.wstuo.common.security.utils;

public interface AppContext {
	/**
	 * get attribute
	 * @param attrName
	 * @return
	 */
	Object getAttribute(String attrName);
	
	/**
	 * set attribute
	 * @param attrName
	 * @param value
	 */
	void setAttribute(String attrName,Object value);
	
	/**
	 * 获取当前登录的登录账号
	 * @return 当前登录的登录账号
	 */
	String getCurrentLoginName(); 
	
	
	/**
	 * get attribute value by name
	 * @param attrName
	 * @return attribute value
	 */
	String getAttributeByName(String attrName);
	
	
	/**
	 * 获取当前语言版本(Default 'zh_CN')
	 * @return 当前语言版本
	 */
	String getCurrentLanguage();
	
	
	/**
	 * session invalidate
	 */
	void invalidate();

	String getCurrentFullName();
	
	
	String getCurrentTenantId();
	
	String getCurrentTenantIdNotDefaultValue();
	
}
