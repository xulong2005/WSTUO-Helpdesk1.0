package com.wstuo.common.security.entity;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrimaryKeyJoinColumn;

import org.hibernate.annotations.ForeignKey;

/**
 * Entity class Operation.
 * 
 */
@SuppressWarnings("serial")
@Entity
@PrimaryKeyJoinColumn(name = "resNo")
public class Operation extends Resource {
	@ManyToOne
	@JoinColumn(name = "functionNo")
	@ForeignKey(name = "functionNo")
	private Function function;

	public Function getFunction() {
		return function;
	}

	public void setFunction(Function function) {
		this.function = function;
	}

	public Operation() {

	}

	public Operation(String resName, String resCode, String resUrl, Function function) {

		super.setResName(resName);
		super.setResCode(resCode);
		super.setResUrl(resUrl);
		this.function = function;
	}
}
