package com.wstuo.common.proxy.service;


import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.wstuo.common.dto.PageDTO;
import com.wstuo.common.proxy.dao.IProxyDAO;
import com.wstuo.common.proxy.dto.ProxyDTO;
import com.wstuo.common.proxy.entity.AuthorizeProxy;
import com.wstuo.common.security.dao.IUserDAO;
import com.wstuo.common.security.entity.User;

/**
 * 代理Service类
 * @author Administrator
 *
 */
public class ProxyService implements IProxyService{
	@Autowired
	private IProxyDAO proxyDAO;
	@Autowired
	private IUserDAO userDAO;
	
	/**
	 * 代理分页查询
	 * @param proxyDTO
	 * @param sidx
	 * @param sord
	 * @param start
	 * @param rows
	 * @return PageDTO
	 */
	@SuppressWarnings("unchecked")
	@Transactional
    public PageDTO findProxyByPage(ProxyDTO proxyDTO,String sidx,String sord,int start,int rows) {
		PageDTO p = proxyDAO.findPager(proxyDTO,sidx,sord,start,rows);
		List<AuthorizeProxy> listEntity=p.getData();
		List<ProxyDTO> listDto = new ArrayList<ProxyDTO>();
		for(AuthorizeProxy entity:listEntity){
			ProxyDTO dto = new ProxyDTO();
			proxyEntity2Dto(entity,dto);
			listDto.add(dto);
		}
		p.setData(listDto);
       return p;
    }
	/**
	 * entity 2 dto
	 * @param entity
	 * @param dto
	 */
	
	private void proxyEntity2Dto(AuthorizeProxy entity,ProxyDTO dto){
		ProxyDTO.entity2dto(entity, dto);
		if(entity.getUserAgent()!=null){//代理人
			dto.setUserId(entity.getUserAgent().getUserId());
			dto.setUserName(entity.getUserAgent().getFullName());
			dto.setProxyState(entity.getUserAgent().getHolidayStatus());
		}
		if(entity.getProxieduser()!=null){//代理人
			dto.setProxieduserId(entity.getProxieduser().getUserId());
		}
	}
	/**
	 * 保存代理人
	 * @param dto
	 */
	@Transactional
	public void saveProxy(final ProxyDTO dto){
		AuthorizeProxy entity=new AuthorizeProxy();
		proxyDto2Entity(dto,entity);
		proxyDAO.save(entity);
		dto.setProxyId(entity.getProxyId());
	};
	/**
	 * 编辑代理人
	 * @param dto
	 */
	@Transactional
	public void editProxy(final ProxyDTO dto){
		AuthorizeProxy entity=new AuthorizeProxy();
		proxyDto2Entity(dto,entity);
		proxyDAO.merge(entity);
	};
	/**
	 * entity 2 dto
	 * @param entity
	 * @param dto
	 */
	private void proxyDto2Entity(ProxyDTO dto,AuthorizeProxy entity){
		ProxyDTO.dto2entity(dto,entity);
		if(dto.getUserId()!=null&&dto.getUserId()!=0){
			User us=userDAO.findById(dto.getUserId());
			entity.setUserAgent(us);
		}
		if(dto.getProxieduserId()!=null&&dto.getProxieduserId()!=0){
			User pu=userDAO.findById(dto.getProxieduserId());
			entity.setProxieduser(pu);
		}
	}
	/**
	 * 删除代理人
	 * @param ids
	 */
	@Transactional
	public void deleteProxy(final Long[] ids){
		proxyDAO.deleteByIds(ids);
	};
	/**
	 * 查询数据
	 * @param loginName
	 * @return String[]
	 */
	@Transactional
	public String[] findProxyOrpersonal(String loginName){
		String[] ligins = null;
		List<AuthorizeProxy> list=proxyDAO.findProxyOrpersonal(loginName,null);
		if(list!=null&&list.size()>0){
			ligins=new String[list.size()];
			 int i=0;
			 for(AuthorizeProxy ap:list){
				 ligins[i]=ap.getProxieduser().getLoginName();
				 i++;
			 }
	   }
	   return ligins;
	}
	/**
	 * 查询显示状态
	 * @param loginName
	 * @param technicalloginName
	 * @return true or false
	 */
	@Transactional
	public boolean findProxyOrlogin(String loginName,String technicalloginName){
		boolean result = false;
		List<AuthorizeProxy> list=proxyDAO.findProxyOrpersonal(loginName,technicalloginName);
		if(list!=null&&list.size()>0){
			result = true;
		}
		return result;
	}
}
