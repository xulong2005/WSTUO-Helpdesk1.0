package com.wstuo.common.security.action;

import com.opensymphony.xwork2.ActionSupport;

import com.wstuo.common.dto.PageDTO;
import com.wstuo.common.exception.ApplicationException;
import com.wstuo.common.security.dto.RoleDTO;
import com.wstuo.common.security.dto.RoleQueryDTO;
import com.wstuo.common.security.dto.UserRoleDTO;
import com.wstuo.common.security.service.IRoleService;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

/**
 * 角色Action
 * 
 * @author will
 * 
 */
@SuppressWarnings("serial")
public class RoleAction extends ActionSupport {
	@Autowired
	private IRoleService roleService;
	private RoleDTO roleDto = new RoleDTO();
	private RoleQueryDTO roleQueryDto = new RoleQueryDTO();
	private List<UserRoleDTO> userRoleDtos = new ArrayList<UserRoleDTO>();
	private Long[] ids;
	private String str;
	private PageDTO roles;
	private Boolean result = false;
	private int page = 1;
	private int rows = 10;
	private long roleId;
	private String effect;
	private InputStream exportStream;
	private String fileName = "";
	private String sidx;
	private String sord;
	private Long res;

	public Long getRes() {
		return res;
	}

	public void setRes(Long res) {
		this.res = res;
	}

	public String getSidx() {
		return sidx;
	}

	public void setSidx(String sidx) {
		this.sidx = sidx;
	}

	public String getSord() {
		return sord;
	}

	public void setSord(String sord) {
		this.sord = sord;
	}

	public String getEffect() {
		return effect;
	}

	public void setEffect(String effect) {
		this.effect = effect;
	}

	public InputStream getExportStream() {
		return exportStream;
	}

	public void setExportStream(InputStream exportStream) {
		this.exportStream = exportStream;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public IRoleService getRoleService() {
		return roleService;
	}

	public void setRoleService(IRoleService roleService) {
		this.roleService = roleService;
	}

	public RoleDTO getRoleDto() {
		return roleDto;
	}

	public void setRoleDto(RoleDTO roleDto) {
		this.roleDto = roleDto;
	}

	public RoleQueryDTO getRoleQueryDto() {
		return roleQueryDto;
	}

	public void setRoleQueryDto(RoleQueryDTO roleQueryDto) {
		this.roleQueryDto = roleQueryDto;
	}

	public Long[] getIds() {
		return ids;
	}

	public void setIds(Long[] ids) {
		this.ids = ids;
	}

	public String getStr() {
		return str;
	}

	public void setStr(String str) {
		this.str = str;
	}

	public List<UserRoleDTO> getUserRoleDtos() {
		return userRoleDtos;
	}

	public void setUserRoleDtos(List<UserRoleDTO> userRoleDtos) {
		this.userRoleDtos = userRoleDtos;
	}

	public PageDTO getRoles() {
		return roles;
	}

	public void setRoles(PageDTO roles) {
		this.roles = roles;
	}

	public Boolean getResult() {
		return result;
	}

	public void setResult(Boolean result) {
		this.result = result;
	}

	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public int getRows() {
		return rows;
	}

	public void setRows(int rows) {
		this.rows = rows;
	}

	public long getRoleId() {
		return roleId;
	}

	public void setRoleId(long roleId) {
		this.roleId = roleId;
	}

	/**
	 * query role
	 * 
	 * @return String
	 */
	public String find() {
		int start = (page - 1) * rows;

		roleQueryDto.setStart(start);
		roleQueryDto.setLimit(rows);
		roles = roleService.findPagerRole(roleQueryDto, sidx, sord);
		roles.setPage(page);
		roles.setRows(rows);

		return SUCCESS;
	}

	/**
	 * save role
	 * 
	 * @return String
	 */
	public String save() {
		roleService.saveRole(roleDto);
		return SUCCESS;
	}

	/**
	 * delete role
	 * 
	 * @return String
	 */
	public String deleteRole() {
		try {
			res = roleService.deleteRole(ids);
		} catch (Exception ex) {
			throw new ApplicationException("ERROR_DATA_CAN_NOT_DELETE", ex);
		}
		return "res";
	}

	/**
	 * edit role
	 * 
	 * @return String
	 */
	public String editRole() {
		roleService.editRole(roleDto);
		return SUCCESS;
	}

	/**
	 * get all role to select
	 * 
	 * @return String
	 */
	public String findByState() {
		userRoleDtos = roleService.findRoleByPropertyName("roleState", true);
		return "userRoleDtos";
	}

	/**
	 * existByRoleCode
	 * 
	 * @return String
	 */
	public String existByRoleCode() {
		if (roleDto.getRoleId()!=null && roleDto.getRoleId() > 0) {
			result = true;
		} else {
			result = roleService.existByRoleCode(roleDto.getRoleCode());
		}
		return "result";
	}

	/**
	 * existByRoleName
	 * 
	 * @return String
	 */
	public String existByRoleName() {
		if (roleDto.getRoleId()!=null&&roleDto.getRoleId() > 0) {
			result = true;
		} else {
			result = roleService.existByRoleName(roleDto.getRoleName());
		}

		return "result";
	}

	/**
	 * setResourceByRoleIds
	 * 
	 * @return String
	 */
	public String setResourceByRoleIds() {
		roleService.setResourceByRoleIds(str);
		result = true;
		return "result";
	}

	/**
	 * getResourceByRoleId
	 * 
	 * @return String
	 */
	public String getResourceByRoleId() {
		str = roleService.getResourceRoleByRoleId(roleId);
		return "str";
	}

	/**
	 * 导出数据.
	 * 
	 * @return inputStream
	 */
	public String exportRole() {
		roleQueryDto.setStart(0);
		roleQueryDto.setLimit(10000);
		exportStream = roleService.exportRoleItems(roleQueryDto, sidx, sord);
		return "exportFileSuccessful";
	}

	/**
	 * 导入数据
	 * 
	 * @return import result
	 */
	public String importRoleData() {
		effect = roleService.importRoleItems(roleQueryDto.getImportFile());
		return "importResult";
	}

	/**
	 * 导入角色权限数据
	 * 
	 * @return import result
	 */
	public String importRoleResource() {
		effect = roleService.importRoleResource(roleQueryDto.getImportFile());
		return "importResult";
	}

	/**
	 * 导出所有角色资源
	 * 
	 * @return inputStream
	 */
	public String findAllRoleResource() {
		exportStream = roleService.findAllRoleResource();
		return "exportFileSuccessful";
	}

}
