package com.wstuo.common.security.service;

import java.util.Comparator;

import com.wstuo.common.security.entity.Function;

/**
 * 功能比较排序
 * @author Administrator
 *
 */
@SuppressWarnings("rawtypes")
public class ComparatorFunction implements Comparator {
	public int compare(Object arg0, Object arg1) {
		  Function function=(Function)arg0;
		  Function function1=(Function)arg1;

		  //首先比较编号，如果编号相同，则比较名字
		  int flag=function.getResNo().compareTo(function1.getResNo());
		  if(flag==0){
			  flag= function.getResName().compareTo(function1.getResName());
		  }
		  return flag;  
	}
}
