package com.wstuo.common.security.dto;

import com.wstuo.common.sha.JDKMessageDigest;

/**
 * edit user password dto
 * @author will
 *
 */
public class EditUserPasswordDTO {
	private String loginName;
	private String oldPassword;
	private String newPassword;
	private String resetPassword;
	public String getLoginName() {
		return loginName;
	}
	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}

	public String getOldPassword() {
		return oldPassword;
	}
	public void setOldPassword(String oldPassword) {
		JDKMessageDigest sha = new JDKMessageDigest();
		if(oldPassword.length() != 40){
			oldPassword = sha.Encryption(oldPassword);
		}
		this.oldPassword = oldPassword;
	}
	public String getNewPassword() {
		return newPassword;
	}
	public void setNewPassword(String newPassword) {
		this.newPassword = newPassword;
	}
	public String getResetPassword() {
		return resetPassword;
	}
	public void setResetPassword(String resetPassword) {
		
		this.resetPassword = resetPassword;
	}
	
}
