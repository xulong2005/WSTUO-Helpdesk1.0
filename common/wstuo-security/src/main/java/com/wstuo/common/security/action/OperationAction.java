package com.wstuo.common.security.action;

import com.opensymphony.xwork2.ActionSupport;

import com.wstuo.common.dto.PageDTO;
import com.wstuo.common.exception.ApplicationException;
import com.wstuo.common.security.dto.OperationDTO;
import com.wstuo.common.security.dto.OperationQueryDTO;
import com.wstuo.common.security.service.IOperationService;

import java.io.File;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;

import org.apache.log4j.Logger;

/**
 * 操作Action类
 * 
 * @author will
 */
@SuppressWarnings("serial")
public class OperationAction extends ActionSupport {
	private static final Logger LOGGER = Logger.getLogger(OperationAction.class);
	private IOperationService operationService;
	private OperationQueryDTO operationQueryDto = new OperationQueryDTO();
	private OperationDTO operationDto = new OperationDTO();
	private PageDTO operations;
	private int page = 1;
	private int rows = 10;
	private Long functionNo;
	private Long[] resNos;
	private String resName;
	private Boolean result = false;
	private File importFile;
	private InputStream exportFile;
	private String effect = "failure";
	private String sidx;
	private String sord;
	private String functionResCode;

	public String getSidx() {
		return sidx;
	}

	public void setSidx(String sidx) {
		this.sidx = sidx;
	}

	public String getSord() {
		return sord;
	}

	public void setSord(String sord) {
		this.sord = sord;
	}

	public File getImportFile() {
		return importFile;
	}

	public void setImportFile(File importFile) {
		this.importFile = importFile;
	}

	public InputStream getExportFile() {
		return exportFile;
	}

	public void setExportFile(InputStream exportFile) {
		this.exportFile = exportFile;
	}

	public String getEffect() {
		return effect;
	}

	public void setEffect(String effect) {
		this.effect = effect;
	}

	public Boolean getResult() {
		return result;
	}

	public void setResult(Boolean result) {
		this.result = result;
	}

	public String getResName() {
		return resName;
	}

	public void setResName(String resName) {
		this.resName = resName;
	}

	private Boolean isExist = false;

	public Boolean getIsExist() {
		return isExist;
	}

	public void setIsExist(Boolean isExist) {
		this.isExist = isExist;
	}

	public Long[] getResNos() {
		return resNos;
	}

	public void setResNos(Long[] resNos) {
		this.resNos = resNos;
	}

	public OperationDTO getOperationDto() {
		return operationDto;
	}

	public void setOperationDto(OperationDTO operationDto) {
		this.operationDto = operationDto;
	}

	public Long getFunctionNo() {
		return functionNo;
	}

	public void setFunctionNo(Long functionNo) {
		this.functionNo = functionNo;
	}

	public IOperationService getOperationService() {
		return operationService;
	}

	public void setOperationService(IOperationService operationService) {
		this.operationService = operationService;
	}

	public OperationQueryDTO getOperationQueryDto() {
		return operationQueryDto;
	}

	public void setOperationQueryDto(OperationQueryDTO operationQueryDto) {
		this.operationQueryDto = operationQueryDto;
	}

	public PageDTO getOperations() {
		return operations;
	}

	public void setOperations(PageDTO operations) {
		this.operations = operations;
	}

	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public int getRows() {
		return rows;
	}

	public void setRows(int rows) {
		this.rows = rows;
	}

	public String getFunctionResCode() {
		return functionResCode;
	}

	public void setFunctionResCode(String functionResCode) {
		this.functionResCode = functionResCode;
	}

	/**
	 * 分页查找操作列表.
	 * 
	 * @return String
	 */
	public String find() {
		operationQueryDto.setSidx(sidx);
		operationQueryDto.setSord(sord);
		int start = (page - 1) * rows;
		operationQueryDto.setFunctionNo(functionNo);
		operationQueryDto.setFunctionResCode(functionResCode);
		operationQueryDto.setStart(start);
		operationQueryDto.setLimit(rows);
		operations = operationService.findPagerOperation(operationQueryDto);
		operations.setPage(page);
		operations.setRows(rows);
		return SUCCESS;
	}

	/**
	 * 新增操作.
	 * 
	 * @return String
	 */
	public String addOperation() {
		operationService.addOperation(operationDto);

		return SUCCESS;
	}

	/**
	 * 修改操作.
	 * 
	 * @return String
	 */
	public String updateOperation() {
		operationService.upadteOperation(operationDto);

		return SUCCESS;
	}

	/**
	 * 删除操作.
	 * 
	 * @return String
	 */
	public String deleteOperation() {

		try {
			result = operationService.deleteOperation(operationDto.getResNo());
		} catch (Exception ex) {
			throw new ApplicationException("ERROR_DATA_CAN_NOT_DELETE", ex);
		}
		return "result";
	}

	/**
	 * 批量删除操作.
	 * 
	 * @return String
	 */
	public String deleteOperations() {
		try {
			result = operationService.deleteOperations(resNos);
		} catch (Exception ex) {
			throw new ApplicationException("ERROR_DATA_CAN_NOT_DELETE", ex);
		}

		return "result";
	}

	/**
	 * 数据验证.
	 * 
	 * @throws UnsupportedEncodingException
	 * @return String
	 */
	public String findOperationExist() throws UnsupportedEncodingException {
		if (operationQueryDto.getResName() != null) {
			String resName = new String(operationQueryDto.getResName().getBytes("ISO-8859-1"), "UTF-8");
			operationQueryDto.setResName(resName);
		}
		if (operationQueryDto.getResCode() != null) {
			String resCode = new String(operationQueryDto.getResCode().getBytes("ISO-8859-1"), "UTF-8");
			operationQueryDto.setResCode(resCode);
		}
		if (operationQueryDto.getResUrl() != null) {
			String resUrl = new String(operationQueryDto.getResUrl().getBytes("ISO-8859-1"), "UTF-8");
			operationQueryDto.setResUrl(resUrl);
		}
		isExist = operationService.findOperationExist(operationQueryDto);
		return "isExist";
	}

	/**
	 * 导出CSV.
	 * 
	 * @return inputStream
	 */
	public String exportOperation() {
		operationQueryDto.setFunctionNo(functionNo);
		operationQueryDto.setStart(0);
		operationQueryDto.setLimit(10000);
		exportFile = operationService.exportOperation(operationQueryDto);
		return "exportFile";
	}

	/**
	 * 导入CSV
	 * 
	 * @return import result
	 */
	public String importOperation() {
		try {
			effect = operationService.importOperation(importFile.getAbsolutePath());
		} catch (Exception ex) {
			LOGGER.error(ex.getMessage());
		}
		return "importResult";
	}

}
