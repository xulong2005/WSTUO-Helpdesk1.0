package com.wstuo.common.bpm.dto;

import java.util.List;

import com.wstuo.common.bpm.dto.FlowActivityDTO;
import com.wstuo.common.dto.BaseDTO;

/**
 * Flow Property DTO Class
 * @author wstuo
 *
 */
@SuppressWarnings("serial")
public class FlowPropertyDTO extends BaseDTO {
	private Long id;
	private String deploymentId;
	private String processDefinitionId;
	private List<FlowActivityDTO> flowActivityDTO;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getDeploymentId() {
		return deploymentId;
	}
	public void setDeploymentId(String deploymentId) {
		this.deploymentId = deploymentId;
	}
	
	public String getProcessDefinitionId() {
		return processDefinitionId;
	}
	public void setProcessDefinitionId(String processDefinitionId) {
		this.processDefinitionId = processDefinitionId;
	}
	public List<FlowActivityDTO> getFlowActivityDTO() {
		return flowActivityDTO;
	}
	public void setFlowActivityDTO(List<FlowActivityDTO> flowActivityDTO) {
		this.flowActivityDTO = flowActivityDTO;
	}
	
}
