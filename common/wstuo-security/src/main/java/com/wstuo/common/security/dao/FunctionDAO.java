package com.wstuo.common.security.dao;

import java.util.List;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;

import com.wstuo.common.dao.BaseDAOImplHibernate;
import com.wstuo.common.dto.PageDTO;
import com.wstuo.common.security.dto.FunctionQueryDTO;
import com.wstuo.common.security.entity.Function;
import com.wstuo.common.util.StringUtils;

/**
 * 资源中功能的DAO.
 * 
 * @author will
 */
@SuppressWarnings("unchecked")
public class FunctionDAO extends BaseDAOImplHibernate<Function> implements IFunctionDAO {

	@Autowired
	private IResourceDAO resourceDAO;

	/**
	 * 分页查找功能.
	 * 
	 * @param qdto
	 *            查询DTO FunctionQueryDTO
	 * @return 分页数据 PageDTO
	 */
	public PageDTO findPager(FunctionQueryDTO qdto) {
		final DetachedCriteria dc = DetachedCriteria.forClass(Function.class);
		int start = 0;
		int limit = 1;

		if (qdto != null) {
			start = qdto.getStart();
			limit = qdto.getLimit();

			if (StringUtils.hasText(qdto.getResName())) {
				dc.add(Restrictions.like("resName", qdto.getResName(), MatchMode.ANYWHERE));
			}

			if (StringUtils.hasText(qdto.getResCode())) {
				dc.add(Restrictions.like("resCode", qdto.getResCode(), MatchMode.ANYWHERE));
			}

			if (StringUtils.hasText(qdto.getResUrl())) {
				dc.add(Restrictions.like("resUrl", qdto.getResUrl(), MatchMode.ANYWHERE));
			}
			// 排序
			if (StringUtils.hasText(qdto.getSord()) && StringUtils.hasText(qdto.getSidx())) {
				if ("desc".equals(qdto.getSord())) {
					dc.addOrder(Order.desc(qdto.getSidx()));
				} else {
					dc.addOrder(Order.asc(qdto.getSidx()));
				}
			} else {
				dc.addOrder(Order.desc("resNo"));
			}
		}
		return super.findPageByCriteria(dc, start, limit);
	}

	/**
	 * 验证功能相关属�?是否存在.
	 * 
	 * @param qdto
	 *            FunctionQueryDTO
	 * @return Boolean true表示存在，false表示不存�?
	 */
	public Boolean findExist(FunctionQueryDTO qdto) {
		boolean result = false;
		if (qdto.getResName() != null) {
			List<Object> obj = super.getHibernateTemplate().find(" from Function fc where fc.resName='" + qdto.getResName() + "'");
			if (!obj.isEmpty()) {
				result = true;
			}
		} else if (qdto.getResCode() != null) {
			List<Object> obj = super.getHibernateTemplate().find(" from Function fc where fc.resCode='" + qdto.getResCode() + "'");
			if (!obj.isEmpty()) {
				result = true;
			}
		} else if (qdto.getResUrl() != null) {
			List<Object> obj = super.getHibernateTemplate().find(" from Function fc where fc.resUrl='" + qdto.getResUrl() + "'");

			if (!obj.isEmpty()) {
				result = true;
			}
		}

		return result;
	}

	/**
	 * 根据父功能查找关联的子功能列�?
	 * 
	 * @param parentNo
	 *            父功能编号Long parentNo
	 * @return 子功能列表 List<Function>
	 */
	public List<Function> findByParentNo(Long parentNo) {
		String hql = " from Function fc where fc.parentFunction.resNo=" + parentNo;

		return super.getHibernateTemplate().find(hql);
	}

	public List<Function> findRoots(String resCode) {
		String hql = " from Function fc where fc.parentFunction is null and fc.dataFlag!=99";
		if (StringUtils.hasText(resCode)) {
			if ("Knowledge_Category_View".equals(resCode)) {
				hql = " from Function fc where (fc.resCode='" + resCode + "' or fc.resCode='Request_Category_View') and fc.dataFlag!=99";
			} else {
				hql = " from Function fc where fc.resCode='" + resCode + "' and fc.dataFlag!=99";
			}
		}
		return super.getHibernateTemplate().find(hql);
	}

	/**
	 * 取得下一个编号.
	 * 
	 * @return next res no
	 */
	public Long getNextResNo() {
		Long v = 1L;
		final String hql = "select max(f.resNo) from Resource f";
		@SuppressWarnings("rawtypes")
		List list = getHibernateTemplate().find(hql);

		if (list != null && list.size() > 0) {
			Number n = (Number) list.get(0);
			if (n != null) {
				v = n.longValue();
			}
		}
		return v;
	}

	/**
	 * 重写SAVE方法.
	 */
	@Override
	public void save(Function entity) {

		if (entity.getResNo() == null || entity.getResNo() == 0) {
			entity.setResNo(resourceDAO.getLatesResNo());
		} else {

			if (entity.getResNo() > resourceDAO.getLatesResNo()) {

				resourceDAO.setLatesResNo(entity.getResNo());
			}

		}
		super.save(entity);
		resourceDAO.increment();
	}

	@Override
	public Function merge(Function entity) {

		if (entity.getResNo() != null) {
			if (entity.getResNo() >= resourceDAO.getLatesResNo()) {
				resourceDAO.setLatesResNo(entity.getResNo() + 1);
			}
		} else {
			resourceDAO.increment();// 自增1.
		}

		return super.merge(entity);
	}

}
