package com.wstuo.common.security.service;

import com.wstuo.common.security.dto.OrganizationDTO;
import com.wstuo.common.security.entity.OrganizationServices;

/**
 * 服务机构业务类接口
 */
public interface IOrganizationServicesService
{
    /**
     * 保存服务机构.
     * @param dto 机构DTO：OrganizationDTO
     */
    void saveOrganizationServices( OrganizationDTO dto );

    /**
     * 修改服务机构.
     * @param dto 机构数据DTO：OrganizationDTO
     */
    void mergeOrganizationServices( OrganizationDTO dto );

    /**
     * 批量删除服务机构.
     * @param orgNos 机构编号数组：Long [] orgNos
     */
    void deleteOrganizationServices( Long[] orgNos );

    /**
     * 根据ID查找服务机构.
     * @param id 服务机构编号：Long id
     * @return 服务机构 OrganizationServices
     */
    OrganizationServices findOrganizationServicesById( Long id );
    
    /**
     * 根据编号查找机构DTO.
     * @param id 机构编号：Long id
     * @return OrganizationDTO
     */
    OrganizationDTO findById(Long id);
}
