package com.wstuo.common.bpm.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import com.wstuo.common.entity.BaseEntity;
/**
 * Process Use Entity CLASS(默认流程设置)
 * @author wstuo
 *
 */
@SuppressWarnings({ "serial", "rawtypes" })
public class ProcessUse extends BaseEntity {
	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long processUseNo;
	@Column(unique=true)
	private String useName;
	@Column(nullable=true)
	private String requestProcessKey;//请求
	@Column(name="requestPid")
	private String requestProcessDefinitionId;//请求流程定义ID
	@Column(nullable=true)
	private String changeProcessKey;//变更
	@Column(name="changePid")
	private String changeProcessDefinitionId;//变更流程定义ID
	@Column(nullable=true)
	private String problemProcessKey;//问题
	@Column(name="problemPid")
	private String problemProcessDefinitionId;//问题流程定义ID
	@Column(nullable=true)
	private String knowledgeProcessKey;//知识库
	@Column(name="knowledgePid")
	private String knowledgeProcessDefinitionId;//知识库流程定义ID
	@Column(nullable=true)
	private String configureItemProcessKey;//配置项
	@Column(name="ciPid")
	private String configureItemProcessDefinitionId;//配置项流程定义ID
	
	@Column(nullable=true)
	private String releaseProcessKey;//发布
	@Column(name="releasePid")
	private String releaseProcessDefinitionId;//发布流程定义ID
	
	public Long getProcessUseNo(){
		return processUseNo;
	}
	public void setProcessUseNo(Long processUseNo) {
		this.processUseNo = processUseNo;
	}
	public String getUseName() {
		return useName;
	}
	public void setUseName(String useName) {
		this.useName = useName;
	}
	public String getRequestProcessKey() {
		return requestProcessKey;
	}
	public void setRequestProcessKey(String requestProcessKey) {
		this.requestProcessKey = requestProcessKey;
	}
	public String getChangeProcessKey() {
		return changeProcessKey;
	}
	public void setChangeProcessKey(String changeProcessKey) {
		this.changeProcessKey = changeProcessKey;
	}
	public String getProblemProcessKey() {
		return problemProcessKey;
	}
	public void setProblemProcessKey(String problemProcessKey) {
		this.problemProcessKey = problemProcessKey;
	}
	public String getKnowledgeProcessKey() {
		return knowledgeProcessKey;
	}
	public void setKnowledgeProcessKey(String knowledgeProcessKey) {
		this.knowledgeProcessKey = knowledgeProcessKey;
	}
	public String getConfigureItemProcessKey() {
		return configureItemProcessKey;
	}
	public void setConfigureItemProcessKey(String configureItemProcessKey) {
		this.configureItemProcessKey = configureItemProcessKey;
	}
	public String getRequestProcessDefinitionId() {
		return requestProcessDefinitionId;
	}
	public void setRequestProcessDefinitionId(String requestProcessDefinitionId) {
		this.requestProcessDefinitionId = requestProcessDefinitionId;
	}
	public String getChangeProcessDefinitionId() {
		return changeProcessDefinitionId;
	}
	public void setChangeProcessDefinitionId(String changeProcessDefinitionId) {
		this.changeProcessDefinitionId = changeProcessDefinitionId;
	}
	public String getProblemProcessDefinitionId() {
		return problemProcessDefinitionId;
	}
	public void setProblemProcessDefinitionId(String problemProcessDefinitionId) {
		this.problemProcessDefinitionId = problemProcessDefinitionId;
	}
	public String getKnowledgeProcessDefinitionId() {
		return knowledgeProcessDefinitionId;
	}
	public void setKnowledgeProcessDefinitionId(String knowledgeProcessDefinitionId) {
		this.knowledgeProcessDefinitionId = knowledgeProcessDefinitionId;
	}
	public String getConfigureItemProcessDefinitionId() {
		return configureItemProcessDefinitionId;
	}
	public void setConfigureItemProcessDefinitionId(
			String configureItemProcessDefinitionId) {
		this.configureItemProcessDefinitionId = configureItemProcessDefinitionId;
	}
	public String getReleaseProcessKey() {
		return releaseProcessKey;
	}
	public void setReleaseProcessKey(String releaseProcessKey) {
		this.releaseProcessKey = releaseProcessKey;
	}
	public String getReleaseProcessDefinitionId() {
		return releaseProcessDefinitionId;
	}
	public void setReleaseProcessDefinitionId(String releaseProcessDefinitionId) {
		this.releaseProcessDefinitionId = releaseProcessDefinitionId;
	}
	
	
}
