package com.wstuo.common.security.dto;

import com.wstuo.common.dto.AbstractValueObject;

/**
 * class OrganizationQueryDTO.
 * @author will
 */
@SuppressWarnings( "serial" )
public class OrganizationQueryDTO
    extends AbstractValueObject
{
    private Long orgNo;
    private String orgName;
    private String address;
    private String email;
    private String officePhone;
    private String officeFax;
    private Integer start;
    private Integer limit;
    private Long[] orgNos;
    private String sord;
    private String sidx;
    private Long companyNo;
    private Long parentNo;
    
    private Long personInChargeNo;
    private String personInChargeName;
    
    
	public Long getParentNo() {
		return parentNo;
	}

	public void setParentNo(Long parentNo) {
		this.parentNo = parentNo;
	}

	public Long getPersonInChargeNo() {
		return personInChargeNo;
	}

	public void setPersonInChargeNo(Long personInChargeNo) {
		this.personInChargeNo = personInChargeNo;
	}

	public String getPersonInChargeName() {
		return personInChargeName;
	}

	public void setPersonInChargeName(String personInChargeName) {
		this.personInChargeName = personInChargeName;
	}

	public Long getCompanyNo() {
		return companyNo;
	}

	public void setCompanyNo(Long companyNo) {
		this.companyNo = companyNo;
	}

	public String getSord() {
		return sord;
	}

	public void setSord(String sord) {
		this.sord = sord;
	}

	public String getSidx() {
		return sidx;
	}

	public void setSidx(String sidx) {
		this.sidx = sidx;
	}

	public Long[] getOrgNos(  )
    {
        return orgNos;
    }

    public void setOrgNos( Long[] orgNos )
    {
        this.orgNos = orgNos;
    }

    public OrganizationQueryDTO(  )
    {
        super(  );
    }

    public Long getOrgNo(  )
    {
        return orgNo;
    }

    public void setOrgNo( Long orgNo )
    {
        this.orgNo = orgNo;
    }

    public String getEmail(  )
    {
        return email;
    }

    public void setEmail( String email )
    {
        this.email = email;
    }

    public String getOfficeFax(  )
    {
        return officeFax;
    }

    public void setOfficeFax( String officeFax )
    {
        this.officeFax = officeFax;
    }

    public String getOrgName(  )
    {
        return orgName;
    }

    public void setOrgName( String orgName )
    {
        this.orgName = orgName;
    }

    public String getAddress(  )
    {
        return address;
    }

    public void setAddress( String address )
    {
        this.address = address;
    }

    public String getOfficePhone(  )
    {
        return officePhone;
    }

    public void setOfficePhone( String officePhone )
    {
        this.officePhone = officePhone;
    }

	public Integer getStart() {
		return start;
	}

	public void setStart(Integer start) {
		this.start = start;
	}

	public Integer getLimit() {
		return limit;
	}

	public void setLimit(Integer limit) {
		this.limit = limit;
	}

   
}
