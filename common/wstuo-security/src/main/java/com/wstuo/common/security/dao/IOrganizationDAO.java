package com.wstuo.common.security.dao;

import com.wstuo.common.dao.IEntityDAO;
import com.wstuo.common.dto.PageDTO;
import com.wstuo.common.security.dto.OrganizationDTO;
import com.wstuo.common.security.dto.OrganizationQueryDTO;
import com.wstuo.common.security.entity.Organization;

import java.util.List;

/**
 * 机构DAO接口.
 * @author will
 */
public interface IOrganizationDAO
    extends IEntityDAO<Organization>
{
    /**
     * 分页查找机构列表.
     * @param  qdto 查询DTO OrganizationQueryDTO
     * @return 分页数据 PageDTO
     */
    PageDTO findPager( OrganizationQueryDTO qdto );

    /**
     * 根据传入的编号数据查询机构分页
     * @param qdto 查询DTO OrganizationQueryDTO
     * @return 分页数据 PageDTO
     */
    PageDTO findPagerByIds( OrganizationQueryDTO qdto );

    /**
     * 根据父机构查找子机构列表.
     * @param parentNo 父机构编号Long
     * @return 子机构列表List<Organization>
     */
    List<Organization> findByParent( Long parentNo );
    
    /**
     * 取得最新的编号.
     * @return next org no
     */
    Long getNextOrgNo();
    
    /**
     * 直接设置.
     * @param no
     */
    void setLatesOrganizationNo(Long no);
    
    /**
     * 累加机构编号.
     */
    void increment();
    
    /**
     * 取得最新的编号.
     * @return long
     */
     Long getLatestOrganizationNo();
    
    /**
     * 查询最上层公司(机构)
     * @return Organization
     */
    Organization findTopCompany();
    
    /**
     * 查询指定的OrgId
     * @param ids
     * @return List<Organization>
     */
    List<Organization> findOrganizationsByIds(Long [] ids);
    
    /**
     * 查询子机构
     * @param parentOrgNo
     * @return List<Organization>
     */
    List<Organization> findSubOrg(Long parentOrgNo);
	/**
     * 根据机构名称查询
     * @param orgName
     * @return List<Organization>
     */
    List<Organization> findOrgByName(String orgName);
    /**
	 * 根据机构名称和父节点名称查询
	 * 
	 * @param orgName
	 * @return List<Organization>
	 */
	List<Organization> findOrgByOrgNameOrparentOrgName(String orgNames,String parentOrgName);
    /**
	 * 根据Path查询机构下所有子机构ID
	 * @return List<Long>
	 */
	List findOrgChildsIdsByPath(String orgPath);
	
	/**
     * 判断分类是否存在
     * @param ec
     */
    boolean isCategoryExisted(OrganizationDTO dto);
}
