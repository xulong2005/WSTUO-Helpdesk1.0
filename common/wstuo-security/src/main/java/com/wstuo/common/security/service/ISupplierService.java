package com.wstuo.common.security.service;


import java.io.InputStream;

import com.wstuo.common.dto.PageDTO;
import com.wstuo.common.security.dto.OrganizationDTO;
import com.wstuo.common.security.dto.SupplierDTO;
import com.wstuo.common.security.dto.SupplierQueryDTO;
import com.wstuo.common.security.entity.Supplier;

/**
 * 供应商Service interface class
 */
public interface ISupplierService
{
    /**
     * 分页查询供应商
     * @param supplierrQueryDTO
     * @param sidx
     * @param sord
     * @return PageDTO
     */
    PageDTO findPagerSupplier( SupplierQueryDTO supplierrQueryDTO , String sidx, String sord);

    /**
     * 保存（添加） 供应商信息
     * @param dto 机构DTO：OrganizationDTO
     */
    void saveSupplier( OrganizationDTO dto );

    /**
     * 保存（添加） 供应商信息
     * @param dto 供应商DTO：SupplierDTO
     */
    void saveSupplier( SupplierDTO dto );

    /**
     * 修改供应商信息
     * @param dto 机构DTO：SupplierDTO
     */
    void mergeSupplier( OrganizationDTO dto );

    /**
     * 修改供应商信息
     * @param dto 供应商DTO：SupplierDTO
     */
    void mergeSupplier( SupplierDTO dto );

    /**
     * 根据ID删除供应商
     * @param orgNo 供应商编号：Long orgNo
     */
    void deleteSupplier( Long orgNo );

    /**
     * 批量删除供应商
     * @param orgNo 长整型数组：Long[] orgNo
     */
    void deleteSupplier( Long[] orgNo );

    /**
     * 根据ID查找供应商信息
     * @param id 供应商ID:Long id
     */
    Supplier findSupplierById( Long id );
    

    /**
     * 根据ID查找供应商DTO
     * @param id 供应商ID:Long id
     * return SupplierDTO
     */
    SupplierDTO findById(Long id);
    
    /**
     * 导出供应商
     * @param queryDTO
     * @param sidx
     * @param sord
     * @return InputStream
     */
    InputStream exportSupplier(SupplierQueryDTO queryDTO,String sidx, String sord);
    
    /**
     * 导入供应商
     * @param fileName
     * @return import quantity
     */
    Integer importSupplier(String fileName);
    
}
