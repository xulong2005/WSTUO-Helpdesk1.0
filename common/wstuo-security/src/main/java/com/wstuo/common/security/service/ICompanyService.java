package com.wstuo.common.security.service;

import com.wstuo.common.security.dto.CompanyDTO;
import com.wstuo.common.security.dto.OrganizationDTO;
import com.wstuo.common.security.entity.Company;

import java.io.InputStream;
import java.util.*;

/**
 * 公司信息业务类接口
 */
public interface ICompanyService
{
    /**
     * 查询顶级公司信息.
     * @return 公司集合：List<CompanyDTO>
     */
    List<CompanyDTO> findCompanyATree(  );

    /**
     * 查询公司信息.
     * @return 公司信息DTO:CompanyDTO
     */
    CompanyDTO findCompany(Long companyNo);

    /**
     * 保存公司信息.
     * @param dto 公司DTO:OrganizationDTO
     */
    void saveCompany( OrganizationDTO dto );

    /**
     * 修改公司信息.
     * @param dto 公司DTO:OrganizationDTO
     */
    void mergeCompany( OrganizationDTO dto );

    /**
     * 根据ID删除公司信息.
     * @param orgNo 公司ID：Long orgNo
     */
    void deleteCompany( Long orgNo );

    /**
     * 根据ID查找公司信息.
     * @param id 公司编号：Long id
     * @return Company
     */
    Company findCompanyById( Long id );
    
    /**
     * 根据编号查找机构DTO.
     * @param id 机构编号：Long id
     * @return OrganizationDTO
     */
    OrganizationDTO findById(Long id);

    /**
     * 导出公司信息.
     * @param paName
     * @param paLogo
     * @return InputStream
     */
    InputStream exportCompanyInfo(String paName,String paLogo);
    
    /**
     * 导入公司信息.
     * @param importFile
     * @return import result
     */
    String importCompanyInfo(String importFile);
    /**
     * 获取LOGO图片流
     * @param companyNo
     * @return InputStream
     */
    InputStream getImageStream(Long companyNo);
    /**
     * 根据上传图片名称获取LOGO图片流
     * @param companyNo
     * @return InputStream
     */
	InputStream getImageUrlforimageStream(String imgFilePath);
}
