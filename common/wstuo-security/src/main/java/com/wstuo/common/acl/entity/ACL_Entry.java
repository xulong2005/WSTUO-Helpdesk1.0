package com.wstuo.common.acl.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

/**
 * ACL ENTRY ENTITY CLASS
 * @author Administrator
 *
 */
//@Entity(name="acl_entry")
public class ACL_Entry {
	@Id
    @GeneratedValue( strategy = GenerationType.AUTO )
	private Long id;
	@ManyToOne
	@JoinColumn(name="acl_object_identity")
	private ACL_Object_Identity acl_object_identity;
	private Integer ace_order;
	@ManyToOne
	@JoinColumn(name="sid")
	private ACL_SID sid;	
	private Integer mask;
	private Byte granting;
	private Byte audit_success;
	private Byte audit_failure;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public ACL_Object_Identity getAcl_object_identity() {
		return acl_object_identity;
	}
	public void setAcl_object_identity(ACL_Object_Identity acl_object_identity) {
		this.acl_object_identity = acl_object_identity;
	}
	public ACL_SID getSid() {
		return sid;
	}
	public void setSid(ACL_SID sid) {
		this.sid = sid;
	}
	public Integer getAce_order() {
		return ace_order;
	}
	public void setAce_order(Integer ace_order) {
		this.ace_order = ace_order;
	}
	public Integer getMask() {
		return mask;
	}
	public void setMask(Integer mask) {
		this.mask = mask;
	}
	public Byte getGranting() {
		return granting;
	}
	public void setGranting(Byte granting) {
		this.granting = granting;
	}
	public Byte getAudit_success() {
		return audit_success;
	}
	public void setAudit_success(Byte audit_success) {
		this.audit_success = audit_success;
	}
	public Byte getAudit_failure() {
		return audit_failure;
	}
	public void setAudit_failure(Byte audit_failure) {
		this.audit_failure = audit_failure;
	}
	
}
