package com.wstuo.common.bpm.entity;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

import com.wstuo.common.bpm.entity.FlowActivity;
import com.wstuo.common.entity.BaseEntity;

/**
 * Flow Property Entity Class
 * @author wstuo
 *
 */
@SuppressWarnings({ "rawtypes", "serial" })
@Entity
public class FlowProperty extends BaseEntity {
	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	@Column(nullable=false,unique=true)
	private String deploymentId;
	private String processDefinitionId;
	@ManyToMany
	@Cascade(CascadeType.DELETE)
	private List<FlowActivity> flowActivity;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getDeploymentId() {
		return deploymentId;
	}
	public void setDeploymentId(String deploymentId) {
		this.deploymentId = deploymentId;
	}

	public String getProcessDefinitionId() {
		return processDefinitionId;
	}
	public void setProcessDefinitionId(String processDefinitionId) {
		this.processDefinitionId = processDefinitionId;
	}
	public List<FlowActivity> getFlowActivity() {
		return flowActivity;
	}
	public void setFlowActivity(List<FlowActivity> flowActivity) {
		this.flowActivity = flowActivity;
	}
	
	
}
