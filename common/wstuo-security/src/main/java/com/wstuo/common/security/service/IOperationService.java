package com.wstuo.common.security.service;


import java.io.InputStream;

import com.wstuo.common.dto.PageDTO;
import com.wstuo.common.security.dto.OperationDTO;
import com.wstuo.common.security.dto.OperationQueryDTO;

/**
 * 操作业务类接口
 *
 */
public interface IOperationService
{
    /**
    * 分页查找操作.
    * @param operationQueryDTO 分页查询DTO：OperationQueryDTO
    * @return 分页数据：PageDTO
    */
    PageDTO findPagerOperation( OperationQueryDTO operationQueryDTO );

    /**
     * 新增操作.
     * @param operationDTO 操作DTO:OperationDTO
     */
    void addOperation( OperationDTO operationDTO );

    /**
     * 编辑操作.
     * @param operationDTO 操作DTO：OperationDTO
     */
    void upadteOperation( OperationDTO operationDTO );

    /**
     * 根据编号删除操作.
     * @param resNo 操作编号：Long resNo
     * @return boolean true为删除成功，反之失败
     */
    Boolean deleteOperation( Long resNo );

    /**
     * 批量删除操作.
     * @param resNos 操作编号数组：Long[] resNos
     * @return boolean true为删除成功，反之失败
     */
    Boolean deleteOperations( Long[] resNos );

    /**
     * 查询操作是否存在.
     * @param qdto 操作DTO：OperationQueryDTO
     */
    Boolean findOperationExist( OperationQueryDTO qdto );
    
    /**
     * 根据ID获取OperationDTO
     * @param resNo Long resNo
     * return OperationDTO
     */
    OperationDTO findById(Long resNo);
    
    /**
     * 导出资源（操作）.
     * @param qdto
     * @return InputStream
     */
    InputStream exportOperation(OperationQueryDTO qdto);
    
    /**
     * 导入资源（操作）.
     * @param filePath 导入文件路径
     * @return import result
     */
    String importOperation(String filePath);
}
