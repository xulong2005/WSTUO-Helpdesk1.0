package com.wstuo.common.security.entity;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Transient;

/**
 * Entity class OrganizationServices.
 */
@SuppressWarnings( "serial" )
@Entity
public class OrganizationServices
    extends Organization
{
	@OneToMany
    private List<Holiday> holidays;
    



    public List<Holiday> getHolidays(  )
    {
        return holidays;
    }

    public void setHolidays( List<Holiday> holidays )
    {
        this.holidays = holidays;
    }
    
    

    @Transient
    public String getOrgType() {
		return "services";
	}


}
