package com.wstuo.common.jaas.service;

import java.security.Principal;

/**
 * Principal Name
 * @author Administrator
 *
 */
public final class TransferPrincipal implements Principal {
	private String name;
	public TransferPrincipal(String name) {
		this.name = name;
	}
	public String getName() {
		return name;
	}
	public boolean equals(Object o) {
		return (o instanceof TransferPrincipal)
				&& this.name.equalsIgnoreCase(((TransferPrincipal) o).name);
	}
	public int hashCode() {
		return name.toUpperCase().hashCode();
	}
}