package com.wstuo.common.acl.dao;

import java.util.List;
import java.util.Set;

import com.wstuo.common.acl.entity.ACL_Entry;
import com.wstuo.common.dao.IEntityDAO;

/**
 * ACL Interface 类
 * @author Administrator
 *
 */
public interface IAclsDAO extends IEntityDAO<ACL_Entry>{
	
	/**
	 * 查询资源权限
	 * @param sid 角色
	 * @param aclClass 类
	 * @param permissions 操作权限
	 * @param object_id_identity 资源ID
	 * @return all acl entry
	 */
	List<ACL_Entry> findAcl(Set<String> sid,String aclClass,Integer[] permissions,Long[] object_id_identity);
	
}
