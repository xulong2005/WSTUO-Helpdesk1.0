package com.wstuo.common.security.dao;

import java.util.List;
import java.util.Set;

import com.wstuo.common.dao.IEntityDAO;
import com.wstuo.common.dto.PageDTO;
import com.wstuo.common.security.dto.UserQueryDTO;
import com.wstuo.common.security.entity.Role;
import com.wstuo.common.security.entity.User;

/**
 * 用户DAO接口.
 * @author will
 */
public interface IUserDAO
    extends IEntityDAO<User>
{
    /**
     * 分页查找用户数据.
     * @param qdto 查询DTO：UserQueryDTO
     * @param sord
     * @param sidx
     * @return 分页数据：PageDTO
     */
    PageDTO findPager( final UserQueryDTO qdto, String sord, String sidx );

    /**
     * 查看用户是否被禁用
     * @param userName
     * @param pwd
     * @return String
     */
    String findDisable( String userName, String pwd );
    
    /**
     * 用户登录.
     * @param userName
     * @param password
     * @return boolean
     */
     Boolean userLogin(String userName,String password);
     
     /**
      * 根据邮件查询用户
      * @param emails
      * @return List<User>
      */
     List<User> findUserByEmail(String[] emails);
     
     /**
      * 根据电话、手机号码查询所有用户
      * @param qdto
      * @return List<User>
      */
     List<User> findUserByNumber(final UserQueryDTO qdto);
     
     
     /**
      * 统计已经使用的技术员数量.
      * @return technician count result
      */
      int countTechnician();
     
      /**
       * 根据用户名列表查询用户列表.
       * @param userNames
       * @return user list
       */
      List<User> findByUserNames(String [] userNames);
      
      /**
       * 判断是否技术员。
       * @param roles
       * @return boolean
       */
      boolean isTc(Set<Role> roles);
  
      /**
       * 根据用户真实姓名查找是否存在。
       * @param fullName
       * @return boolean
       */
  	Boolean findByUserFullNames(String fullName);
  	
  	/**
  	 * 根据真实姓名查找用户.
  	 * @param fullNames
  	 * @return user list
  	 */
  	List<User> findByUserFullNames(String [] fullNames);
      
  	/**
     * 根据登录名查找是否存在。
     * @param loginName
     * @return boolean
     */
	Boolean findByUserLoginName(String loginName);
    
	/**
     * 根据登录名及密码查询用户
     * @param userName
     * @param pwd
     * @return User
     */
    User findUserByLoginNameAndPwd(String userName,String pwd);

    /**
     * 根据登录名查询用户
     * @param loginName
     * @return User
     */
	User findUserByLoginName(String loginName);
    
	
	/**
	 * 分页查询技术员用户
	 * @param start
	 * @param limit
	 * @param sidx
	 * @param sord
	 * @return PageDTO
	 */
	PageDTO findUserByTechnician(int start,int limit,String sidx,String sord);
	/**
	 * 统计技术员数量
	 */
	Integer countUserByTechnician();
	/**
	 * 查询用户List集合
	 * @param qdto
	 * @param sord
	 * @param sidx
	 */
	List<User> findUserList(UserQueryDTO qdto, String sord, String sidx);
	/**
	 * 用户是否有某个角色
	 * @param loginName
	 * @param roleCode
	 */
	Boolean isExistRoleByUserLoginName(String loginName,String roleCode);
	public List<User> findByUserClientId(String clientId);
	
	
	User mergeByRoles(User entity,Set<Role> roles);
	
	User mergeByLDAP(User entity,Set<Role> roles,Set<Role> rolesOld);
}
