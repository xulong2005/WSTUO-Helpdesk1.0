package com.wstuo.common.bpm.dto;

import java.util.List;
import java.util.Set;

import com.wstuo.common.bpm.dto.FlowTransitionDTO;
import com.wstuo.common.dto.BaseDTO;

/**
 * Flow Activity DTO Class
 * @author wstuo
 *
 */
@SuppressWarnings("serial")
public class FlowActivityDTO extends BaseDTO {
	public static String TaskActivityType = "task";
	public static String StateActivityType = "state";
	public static String DecisionActivityType = "decision";
	public static String EndActivityType = "end";
	public static String StartActivityType = "start";
	private Long id;
	private String activityName;//活动名
	private String activityType;//活动类型
	private List<FlowTransitionDTO> flowTransitionDto;
	
	private Long groupNo;//指派组
	private String groupName;//指派组
	private Long assigneeNo;//指派技术员
	private String assigneeName;//指派技术员
	private String formName;//表单
	private Set<String> taskActions;//任务动作
	private String noticeRule;//通知规则
	private Boolean dynamicAssignee = false;//是否动态指派
	private Boolean eventAssign = false;//是否使用业务数据中的指派组及指派技术员
	private String validMethod;//验证方法(JS验证函数)
	private Boolean remarkRequired = false;//备注信息是否必填
	private Long statusNo;//对应状态
	private Set<String> roleCodes;//角色CODE
	private String assignType;//指派方式
	private Boolean approverTask = false;//是否是审批任务
	@Deprecated
	private Boolean allowUseDynamicAssignee = false;//允许动态指派覆盖已有指派
	private Boolean allowUdateDefaultAssignee = false;//允许修改默认指派
	private Long noticeRuleId;//通知规则ID
	private String noticeRuleIds;//多个通知规则ID存放
	private Boolean taskAssigneeUserNotice = false;//通知任务指派用户
	private Boolean taskAssigneeGroupNotice = false;//通知任务指派组组员
	private Boolean allowUdateDefaultAssigneeGroup = false;//允许修改默认指派组
	private String noticSpecifiedUser;//通知指定的用户
	private String noticSpecifiedEmail;//通知指定的邮箱
	private String variablesAssigneeType;//变量指派类型
	private Long leaderNum;//级层
	private Boolean allowUseVariablesAssignee = false; //是否允许变量指派覆盖已有指派
	private Long variablesAssigneeGroupNo; //变量指派指派组
	private String variablesAssigneeGroupName; //变量指派指派组
	private Long matchRuleId;//规则编号
	private String matchRuleIds;//多个规则编号
	private String matchRuleName;//规则名称
	private Boolean mailHandlingProcess = false; //是否邮件处理流程
	private Boolean isUpdateEventAssign = false;//是否更新事件指派
	private Boolean isFallbackToTechnician = false ;//是否回退给上次任务指派的技术员
	private String candidateGroupsNo;//候选组
	private String candidateGroupsName;//候选组名
	private String candidateUsersNo;//候选人
	private String candidateUsersName;//候选人名
	private int index;
	
	
	
	public int getIndex() {
		return index;
	}
	public void setIndex(int index) {
		this.index = index;
	}
	public String getCandidateGroupsNo() {
		return candidateGroupsNo;
	}
	public void setCandidateGroupsNo(String candidateGroupsNo) {
		this.candidateGroupsNo = candidateGroupsNo;
	}
	public String getCandidateGroupsName() {
		return candidateGroupsName;
	}
	public void setCandidateGroupsName(String candidateGroupsName) {
		this.candidateGroupsName = candidateGroupsName;
	}
	public String getCandidateUsersNo() {
		return candidateUsersNo;
	}
	public void setCandidateUsersNo(String candidateUsersNo) {
		this.candidateUsersNo = candidateUsersNo;
	}
	public String getCandidateUsersName() {
		return candidateUsersName;
	}
	public void setCandidateUsersName(String candidateUsersName) {
		this.candidateUsersName = candidateUsersName;
	}
	public Boolean getIsFallbackToTechnician() {
		return isFallbackToTechnician;
	}
	public void setIsFallbackToTechnician(Boolean isFallbackToTechnician) {
		this.isFallbackToTechnician = isFallbackToTechnician;
	}
	public static String getTaskActivityType() {
		return TaskActivityType;
	}
	public static void setTaskActivityType(String taskActivityType) {
		TaskActivityType = taskActivityType;
	}
	public static String getStateActivityType() {
		return StateActivityType;
	}
	public static void setStateActivityType(String stateActivityType) {
		StateActivityType = stateActivityType;
	}
	public static String getDecisionActivityType() {
		return DecisionActivityType;
	}
	public static void setDecisionActivityType(String decisionActivityType) {
		DecisionActivityType = decisionActivityType;
	}
	public static String getEndActivityType() {
		return EndActivityType;
	}
	public static void setEndActivityType(String endActivityType) {
		EndActivityType = endActivityType;
	}
	public static String getStartActivityType() {
		return StartActivityType;
	}
	public static void setStartActivityType(String startActivityType) {
		StartActivityType = startActivityType;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getActivityName() {
		return activityName;
	}
	public void setActivityName(String activityName) {
		this.activityName = activityName;
	}
	public String getActivityType() {
		return activityType;
	}
	public void setActivityType(String activityType) {
		this.activityType = activityType;
	}
	public List<FlowTransitionDTO> getFlowTransitionDto() {
		return flowTransitionDto;
	}
	public void setFlowTransitionDto(List<FlowTransitionDTO> flowTransitionDto) {
		this.flowTransitionDto = flowTransitionDto;
	}
	public Long getGroupNo() {
		return groupNo;
	}
	public void setGroupNo(Long groupNo) {
		this.groupNo = groupNo;
	}
	public String getGroupName() {
		return groupName;
	}
	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}
	public Long getAssigneeNo() {
		return assigneeNo;
	}
	public void setAssigneeNo(Long assigneeNo) {
		this.assigneeNo = assigneeNo;
	}
	public String getAssigneeName() {
		return assigneeName;
	}
	public void setAssigneeName(String assigneeName) {
		this.assigneeName = assigneeName;
	}
	public String getFormName() {
		return formName;
	}
	public void setFormName(String formName) {
		this.formName = formName;
	}
	public Set<String> getTaskActions() {
		return taskActions;
	}
	public void setTaskActions(Set<String> taskActions) {
		this.taskActions = taskActions;
	}
	public String getNoticeRule() {
		return noticeRule;
	}
	public void setNoticeRule(String noticeRule) {
		this.noticeRule = noticeRule;
	}
	public Boolean getDynamicAssignee() {
		return dynamicAssignee;
	}
	public void setDynamicAssignee(Boolean dynamicAssignee) {
		this.dynamicAssignee = dynamicAssignee;
	}
	public Boolean getEventAssign() {
		return eventAssign;
	}
	public void setEventAssign(Boolean eventAssign) {
		this.eventAssign = eventAssign;
	}
	public String getValidMethod() {
		return validMethod;
	}
	public void setValidMethod(String validMethod) {
		this.validMethod = validMethod;
	}
	public Boolean getRemarkRequired() {
		return remarkRequired;
	}
	public void setRemarkRequired(Boolean remarkRequired) {
		this.remarkRequired = remarkRequired;
	}
	public Long getStatusNo() {
		return statusNo;
	}
	public void setStatusNo(Long statusNo) {
		this.statusNo = statusNo;
	}
	public Set<String> getRoleCodes() {
		return roleCodes;
	}
	public void setRoleCodes(Set<String> roleCodes) {
		this.roleCodes = roleCodes;
	}
	public String getAssignType() {
		return assignType;
	}
	public void setAssignType(String assignType) {
		this.assignType = assignType;
	}
	public Boolean getApproverTask() {
		return approverTask;
	}
	public void setApproverTask(Boolean approverTask) {
		this.approverTask = approverTask;
	}
	public Boolean getAllowUseDynamicAssignee() {
		return allowUseDynamicAssignee;
	}
	public void setAllowUseDynamicAssignee(Boolean allowUseDynamicAssignee) {
		this.allowUseDynamicAssignee = allowUseDynamicAssignee;
	}
	public Boolean getAllowUdateDefaultAssignee() {
		return allowUdateDefaultAssignee;
	}
	public void setAllowUdateDefaultAssignee(Boolean allowUdateDefaultAssignee) {
		this.allowUdateDefaultAssignee = allowUdateDefaultAssignee;
	}
	public Long getNoticeRuleId() {
		return noticeRuleId;
	}
	public void setNoticeRuleId(Long noticeRuleId) {
		this.noticeRuleId = noticeRuleId;
	}
	public String getNoticeRuleIds() {
		return noticeRuleIds;
	}
	public void setNoticeRuleIds(String noticeRuleIds) {
		this.noticeRuleIds = noticeRuleIds;
	}
	public Boolean getTaskAssigneeUserNotice() {
		return taskAssigneeUserNotice;
	}
	public void setTaskAssigneeUserNotice(Boolean taskAssigneeUserNotice) {
		this.taskAssigneeUserNotice = taskAssigneeUserNotice;
	}
	public Boolean getTaskAssigneeGroupNotice() {
		return taskAssigneeGroupNotice;
	}
	public void setTaskAssigneeGroupNotice(Boolean taskAssigneeGroupNotice) {
		this.taskAssigneeGroupNotice = taskAssigneeGroupNotice;
	}
	public Boolean getAllowUdateDefaultAssigneeGroup() {
		return allowUdateDefaultAssigneeGroup;
	}
	public void setAllowUdateDefaultAssigneeGroup(
			Boolean allowUdateDefaultAssigneeGroup) {
		this.allowUdateDefaultAssigneeGroup = allowUdateDefaultAssigneeGroup;
	}
	public String getNoticSpecifiedUser() {
		return noticSpecifiedUser;
	}
	public void setNoticSpecifiedUser(String noticSpecifiedUser) {
		this.noticSpecifiedUser = noticSpecifiedUser;
	}
	public String getNoticSpecifiedEmail() {
		return noticSpecifiedEmail;
	}
	public void setNoticSpecifiedEmail(String noticSpecifiedEmail) {
		this.noticSpecifiedEmail = noticSpecifiedEmail;
	}
	public String getVariablesAssigneeType() {
		return variablesAssigneeType;
	}
	public void setVariablesAssigneeType(String variablesAssigneeType) {
		this.variablesAssigneeType = variablesAssigneeType;
	}
	public Long getLeaderNum() {
		return leaderNum;
	}
	public void setLeaderNum(Long leaderNum) {
		this.leaderNum = leaderNum;
	}
	public Boolean getAllowUseVariablesAssignee() {
		return allowUseVariablesAssignee;
	}
	public void setAllowUseVariablesAssignee(Boolean allowUseVariablesAssignee) {
		this.allowUseVariablesAssignee = allowUseVariablesAssignee;
	}
	public Long getVariablesAssigneeGroupNo() {
		return variablesAssigneeGroupNo;
	}
	public void setVariablesAssigneeGroupNo(Long variablesAssigneeGroupNo) {
		this.variablesAssigneeGroupNo = variablesAssigneeGroupNo;
	}
	public String getVariablesAssigneeGroupName() {
		return variablesAssigneeGroupName;
	}
	public void setVariablesAssigneeGroupName(String variablesAssigneeGroupName) {
		this.variablesAssigneeGroupName = variablesAssigneeGroupName;
	}
	public Long getMatchRuleId() {
		return matchRuleId;
	}
	public void setMatchRuleId(Long matchRuleId) {
		this.matchRuleId = matchRuleId;
	}
	public String getMatchRuleIds() {
		return matchRuleIds;
	}
	public void setMatchRuleIds(String matchRuleIds) {
		this.matchRuleIds = matchRuleIds;
	}
	public String getMatchRuleName() {
		return matchRuleName;
	}
	public void setMatchRuleName(String matchRuleName) {
		this.matchRuleName = matchRuleName;
	}
	public Boolean getMailHandlingProcess() {
		return mailHandlingProcess;
	}
	public void setMailHandlingProcess(Boolean mailHandlingProcess) {
		this.mailHandlingProcess = mailHandlingProcess;
	}
	public Boolean getIsUpdateEventAssign() {
		return isUpdateEventAssign;
	}
	public void setIsUpdateEventAssign(Boolean isUpdateEventAssign) {
		this.isUpdateEventAssign = isUpdateEventAssign;
	}
	@Override
	public String toString() {
		return "FlowActivityDTO [id=" + id + ", activityName=" + activityName
				+ ", activityType=" + activityType + ", flowTransitionDto="
				+ flowTransitionDto + ", groupNo=" + groupNo + ", groupName="
				+ groupName + ", assigneeNo=" + assigneeNo + ", assigneeName="
				+ assigneeName + ", formName=" + formName + ", taskActions="
				+ taskActions + ", noticeRule=" + noticeRule
				+ ", dynamicAssignee=" + dynamicAssignee + ", eventAssign="
				+ eventAssign + ", validMethod=" + validMethod
				+ ", remarkRequired=" + remarkRequired + ", statusNo="
				+ statusNo + ", roleCodes=" + roleCodes + ", assignType="
				+ assignType + ", approverTask=" + approverTask
				+ ", allowUseDynamicAssignee=" + allowUseDynamicAssignee
				+ ", allowUdateDefaultAssignee=" + allowUdateDefaultAssignee
				+ ", noticeRuleId=" + noticeRuleId + ", noticeRuleIds="
				+ noticeRuleIds + ", taskAssigneeUserNotice="
				+ taskAssigneeUserNotice + ", taskAssigneeGroupNotice="
				+ taskAssigneeGroupNotice + ", allowUdateDefaultAssigneeGroup="
				+ allowUdateDefaultAssigneeGroup + ", noticSpecifiedUser="
				+ noticSpecifiedUser + ", noticSpecifiedEmail="
				+ noticSpecifiedEmail + ", variablesAssigneeType="
				+ variablesAssigneeType + ", leaderNum=" + leaderNum
				+ ", allowUseVariablesAssignee=" + allowUseVariablesAssignee
				+ ", variablesAssigneeGroupNo=" + variablesAssigneeGroupNo
				+ ", variablesAssigneeGroupName=" + variablesAssigneeGroupName
				+ ", matchRuleId=" + matchRuleId + ", matchRuleIds="
				+ matchRuleIds + ", matchRuleName=" + matchRuleName
				+ ", mailHandlingProcess=" + mailHandlingProcess
				+ ", isUpdateEventAssign=" + isUpdateEventAssign
				+ ", isFallbackToTechnician=" + isFallbackToTechnician
				+ ", candidateGroupsNo=" + candidateGroupsNo
				+ ", candidateGroupsName=" + candidateGroupsName
				+ ", candidateUsersNo=" + candidateUsersNo
				+ ", candidateUsersName=" + candidateUsersName + ", index="
				+ index + "]";
	}
	
	
}
