package com.wstuo.common.security.utils;

import com.wstuo.common.util.StringUtils;

/**
 * 中文姓名显示规则
 * @author Administrator
 *
 */
public class ChineseNameCode implements FullName{

	public String getFullName(String fn, String ln) {
		String fullName =  "";
		if(!StringUtils.hasText(fn)){
			fullName = ln;
		}else if(!StringUtils.hasText(ln)){
			fullName = fn;
		}else{
			fullName = fn+ln;
		}
		return fullName;
	}

}
