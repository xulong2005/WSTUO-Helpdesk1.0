package com.wstuo.common.security.entity;

import java.util.Set;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.ManyToMany;

import org.apache.struts2.json.annotations.JSON;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.wstuo.common.entity.BaseEntity;

/**
 * 
 * Entity class Role
 * 
 */
@SuppressWarnings({ "serial", "rawtypes" })
@Entity
@Cacheable
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Inheritance(strategy = InheritanceType.JOINED)
public class Role extends BaseEntity {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long roleId; // 角色ID
	@Column(unique = true)
	private String roleCode; // 角色编码
	private String roleName; // 角色名称
	private Boolean roleState = false; // 角色状
	@ManyToMany(mappedBy = "roles", fetch = FetchType.LAZY)
	private Set<User> users; // 用户
	@ManyToMany(fetch = FetchType.LAZY)
	private Set<Resource> resources;
	public Long getRoleId() {
		return roleId;
	}

	public void setRoleId(Long roleId) {
		this.roleId = roleId;
	}

	public String getRoleCode() {
		return roleCode;
	}

	public void setRoleCode(String roleCode) {
		this.roleCode = roleCode;
	}

	public String getRoleName() {
		return roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}

	public Boolean getRoleState() {
		return roleState;
	}

	public void setRoleState(Boolean roleState) {
		this.roleState = roleState;
	}

	@JSON(serialize = false)
	public Set<User> getUsers() {
		return users;
	}

	public void setUsers(Set<User> users) {
		this.users = users;
	}

	@JSON(serialize = false)
	public Set<Resource> getResources() {
		return resources;
	}

	public void setResources(Set<Resource> resources) {
		this.resources = resources;
	}

}
