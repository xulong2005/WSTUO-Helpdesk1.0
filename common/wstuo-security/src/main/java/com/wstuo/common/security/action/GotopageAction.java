package com.wstuo.common.security.action;


import javax.servlet.http.HttpServletRequest;

import org.apache.struts2.ServletActionContext;
import org.springframework.beans.factory.annotation.Autowired;

import com.opensymphony.xwork2.ActionSupport;
import com.wstuo.common.security.utils.AppContext;
import com.wstuo.common.util.StringUtils;

/**
 * 默认页面跳转Action类
 * @author Administrator
 *
 */
@SuppressWarnings("serial")
public class GotopageAction extends ActionSupport {
	
	//时间类型
	private String eventType;
	//时间编号
	private String eventId;
	//去到页面类型
	private String pageType;
	@Autowired
	private AppContext appctx;
	/**
	 *  外部入口
	 * @return page url
	 */
	public String AccessFromExternal(){
		HttpServletRequest req = ServletActionContext.getRequest();
		eventType = req.getParameter("eventType");
		eventId = req.getParameter("eventId");
		pageType = req.getParameter("pageType");
		if(StringUtils.hasText(eventType) && StringUtils.hasText(eventId)){
			appctx.setAttribute("eventType", eventType);
			appctx.setAttribute("eventId", eventId);
		}
		if(StringUtils.hasText(pageType)){
			appctx.setAttribute("pageType", pageType);
		}
		return "goto";
	}
	

}
