package com.wstuo.common.security.action;

import org.springframework.beans.factory.annotation.Autowired;

import com.opensymphony.xwork2.ActionSupport;
import com.wstuo.common.security.dto.LDAPAuthenticationSettingDTO;
import com.wstuo.common.security.service.ILDAPAuthenticationSettingService;

/**
 * LDAP验证配置信息ACTION
 * @author will
 *
 */
@SuppressWarnings("serial")
public class LDAPAuthenticationSettingAction extends ActionSupport {
	
	@Autowired
	private ILDAPAuthenticationSettingService  ldapAuthenticationSettingService;
	private LDAPAuthenticationSettingDTO ldapAuthenticationDTO;
	public LDAPAuthenticationSettingDTO getLdapAuthenticationDTO() {
		return ldapAuthenticationDTO;
	}
	public void setLdapAuthenticationDTO(
			LDAPAuthenticationSettingDTO ldapAuthenticationDTO) {
		this.ldapAuthenticationDTO = ldapAuthenticationDTO;
	}
	
	/**
	 * 查询LDAP验证配置信息
	 * @return ldap autoentication dto
	 */
	public String findLDAPAuth(){
		ldapAuthenticationDTO=ldapAuthenticationSettingService.findLDAPAuthenticationSetting();
		return SUCCESS;
		
	}
	
	/**
	 * 保存LDAP验证配置信息
	 * @return SUCCESS
	 */
	public String saveLDAPAuth(){
		ldapAuthenticationSettingService.saveLDAPAuthenticationSetting(ldapAuthenticationDTO);
		return SUCCESS;
	}
	
}
