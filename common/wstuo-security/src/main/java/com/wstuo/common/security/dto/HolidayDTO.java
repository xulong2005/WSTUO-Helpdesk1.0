package com.wstuo.common.security.dto;

import com.wstuo.common.dto.BaseDTO;

import java.util.Date;

/**
 * class Holiday DTO.
 * @author will
 *
 */
public class HolidayDTO
    extends BaseDTO
{
    private Long hid;
    private Date hdate;
    private String hdesc;
    private String orgName;
    private Long orgNo;
    private Date hedate;
    private Byte dataFlag;
    private String orgType;

    
    public String getOrgType() {
		return orgType;
	}

	public void setOrgType(String orgType) {
		this.orgType = orgType;
	}

	public Date getHedate(  )
    {
        return hedate;
    }

    public void setHedate( Date hedate )
    {
        this.hedate = hedate;
    }

    public Long getHid(  )
    {
        return hid;
    }

    public void setHid( Long hid )
    {
        this.hid = hid;
    }

    public Date getHdate(  )
    {
        return hdate;
    }

    public void setHdate( Date hdate )
    {
        this.hdate = hdate;
    }

    public String getHdesc(  )
    {
        return hdesc;
    }

    public void setHdesc( String hdesc )
    {
        this.hdesc = hdesc;
    }

    public String getOrgName(  )
    {
        return orgName;
    }

    public void setOrgName( String orgName )
    {
        this.orgName = orgName;
    }

    public Long getOrgNo(  )
    {
        return orgNo;
    }

    public void setOrgNo( Long orgNo )
    {
        this.orgNo = orgNo;
    }

    
    public Byte getDataFlag() {
		return dataFlag;
	}

	public void setDataFlag(Byte dataFlag) {
		this.dataFlag = dataFlag;
	}

	public HolidayDTO(){
    	
    }
    
	public HolidayDTO(Long hid, Date hdate, String hdesc, Long orgNo,
			Date hedate) {
		super();
		this.hid = hid;
		this.hdate = hdate;
		this.hdesc = hdesc;
		this.orgNo = orgNo;
		this.hedate = hedate;
	}
	
    
    
}
