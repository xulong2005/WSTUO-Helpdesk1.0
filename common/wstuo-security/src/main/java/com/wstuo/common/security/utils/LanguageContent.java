package com.wstuo.common.security.utils;

import java.util.Locale;
import java.util.ResourceBundle;

import org.apache.log4j.Logger;
import org.springframework.context.ApplicationContext;
import org.springframework.util.StringUtils;

import com.wstuo.common.security.utils.AppContext;
import com.wstuo.common.util.AppliactionBaseListener;

/**
 * 国际化内容
 * @author Administrator
 *
 */
public class LanguageContent{
	final static Logger LOGGER = Logger.getLogger(LanguageContent.class);
	private static LanguageContent languageContent;
	public static LanguageContent getInstance() {
		if( languageContent == null ){
			languageContent = new LanguageContent();
		}
		return languageContent;
	}

	public String getContent(String content){
		String result = "";
		Locale lc = new Locale("zh", "CN");
		try{
			ApplicationContext act = AppliactionBaseListener.ctx;
			AppContext appctx = (AppContext) act.getBean("appctx");
			String lang=appctx.getCurrentLanguage();
			if(lang.equals("zh_CN")){
				lc=new Locale("zh", "CN");
			}else if(lang.equals("en_US")){
				lc=new Locale("en", "US");
			}else if(lang.equals("ja_JP")){
				lc=new Locale("ja", "JP");
			}else if(lang.equals("zh_TW")){
				lc=new Locale("zh", "TW");
			}
			
			result = ResourceBundle.getBundle("i18n.itsmbest",lc).getString(content);
		}catch (Exception e) {
			LOGGER.debug("security LanguageContent");
		}
		return result;
	}
	
	public String getContent(String content,String lang){
		if(!StringUtils.hasText(lang)){
			lang="zh_CN";
		}
		Locale lc = new Locale("zh", "CN");
		try{
			if(lang!=null){
				if(lang.equals("zh_CN")){
					lc=new Locale("zh", "CN");
				}else if(lang.equals("en_US")){
					lc=new Locale("en", "US");
				}else if(lang.equals("ja_JP")){
					lc=new Locale("ja", "JP");
				}else if(lang.equals("zh_TW")){
					lc=new Locale("zh", "TW");
				}
			}
		}catch (Exception e) {
			LOGGER.error(e.getMessage());
		}
		return  ResourceBundle.getBundle("i18n.itsmbest",lc).getString(content);
	}
}

