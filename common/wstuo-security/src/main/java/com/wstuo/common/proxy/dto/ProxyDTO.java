package com.wstuo.common.proxy.dto;

import java.util.Date;

import org.apache.commons.beanutils.BeanUtils;

import com.wstuo.common.exception.ApplicationException;

/**
 * 代理DTO类
 * @author Administrator
 *
 */
public class ProxyDTO {
	private Long proxyId;//代理Id
	private Date startTime;//开始时间
	private Date endTime;//结束时间
	private Boolean proxyState = false;//代理状态
	private Long userId;//代理人Id
	private String userName;//代理人姓名
	private Long proxieduserId;//代理人Id
	
	public Long getProxyId() {
		return proxyId;
	}
	public void setProxyId(Long proxyId) {
		this.proxyId = proxyId;
	}
	public Date getStartTime() {
		return startTime;
	}
	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}
	public Date getEndTime() {
		return endTime;
	}
	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}
	public Boolean getProxyState() {
		return proxyState;
	}
	public void setProxyState(Boolean proxyState) {
		this.proxyState = proxyState;
	}
	public Long getUserId() {
		return userId;
	}
	public void setUserId(Long userId) {
		this.userId = userId;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public Long getProxieduserId() {
		return proxieduserId;
	}
	public void setProxieduserId(Long proxieduserId) {
		this.proxieduserId = proxieduserId;
	}
	/**
     * Transform dto to entity.
     * @param dto        DTO object
     * @param entity domain entity object
     */
    public static void dto2entity(Object dto, Object entity) {
        try {
            if (dto != null) {
                BeanUtils.copyProperties(entity, dto);
            }
        } catch (Exception ex) {
            throw new ApplicationException(ex);
        }
    }
    /**
     * Transform entity to dto.
     * @param entity doamin entity object
     * @param dto dto object
     */
    public static void entity2dto(Object entity, Object dto) {
        try {
            if (entity != null) {
                BeanUtils.copyProperties(dto, entity);
            }
        } catch (Exception ex) {
            throw new ApplicationException(ex);
        }
    }
}
