package com.wstuo.common.security.entity;

import java.util.Date;

import javax.persistence.Cacheable;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.wstuo.common.entity.BaseEntity;

/**
 * Entity class Holiday.
 */
@SuppressWarnings( {"serial",
    "rawtypes"
} )
@Entity
@Cacheable
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Holiday
    extends BaseEntity
{
	@Id
    @GeneratedValue( strategy = GenerationType.AUTO )
    private Long hid;
    private Date hdate;
    private String hdesc;
    @ManyToOne(fetch = FetchType.LAZY)
    private Organization organization;

    public Long getHid(  )
    {
        return hid;
    }

    public void setHid( Long hid )
    {
        this.hid = hid;
    }

    public Date getHdate(  )
    {
        return hdate;
    }

    public void setHdate( Date hdate )
    {
        this.hdate = hdate;
    }

    public String getHdesc(  )
    {
        return hdesc;
    }

    public void setHdesc( String hdesc )
    {
        this.hdesc = hdesc;
    }

    public Organization getOrganization(  )
    {
        return organization;
    }

    public void setOrganization( Organization organization )
    {
        this.organization = organization;
    }
}
