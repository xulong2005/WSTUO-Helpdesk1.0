package com.wstuo.common.security.service;

import java.io.InputStream;

import com.wstuo.common.dto.PageDTO;
import com.wstuo.common.security.dto.CustomerDTO;
import com.wstuo.common.security.dto.CustomerQueryDTO;
import com.wstuo.common.security.dto.OrganizationDTO;
import com.wstuo.common.security.entity.Customer;

/**
 * 客户业务类接口
 */
public interface ICustomerService
{
    /**
     * 分页查询数据.
     * @param customerQueryDTO 分页查询DTO：CustomerQueryDTO
     * @param sidx
     * @param sord
     * @return 分页数据：PageDTO
     */
    PageDTO findPagerCustomer( CustomerQueryDTO customerQueryDTO,String sidx, String sord );

    /**
     * 新增客户.
     * @param dto 机构DTO：OrganizationDTO
     */
    void saveCustomer( OrganizationDTO dto );

    /**
     * 新增客户.
     * @param dto 客户DTO：CustomerDTO
     */
    void saveCustomer( CustomerDTO dto );

    /**
     * 修改客户信息.
     * @param dto 客户DTO：CustomerDTO
     */
    void mergeCustomer( CustomerDTO dto );

    /**
     * 修改客户信息.
     * @param dto 机构DTO：OrganizationDTO
     */
    void mergeCustomer( OrganizationDTO dto );

    /**
     * 根据编号删除客户.
     * @param orgNo 机构编号：Long orgNo
     */
    void deleteCustomer( Long orgNo );

    /**
     * 批量删除客户.
     * @param orgNo 客户编号列表：Long[] orgNo
     */
    void deleteCustomer( Long[] orgNo );

    /**
     * 根据ID查找客户信息.
     * @param id 客户编号：Long id
     */
    Customer findCustomerById( Long id );
    
    /**
     * 根据ID查找客户信息.
     * @param id 客户编号：Long id
     * retrun CustomerDTO
     */
    CustomerDTO findById( Long id );
    
    /**
     * 导出数据字典到EXCEL.
     * @param customerQueryDTO
     * @param sidx
     * @param sord
     * @return InputStream
     */
    InputStream exportCustomerItems(CustomerQueryDTO customerQueryDTO,String sidx, String sord);
    
    /**
     * 从EXCEL文件导入数据.
     * @param customerQueryDTO
     * @return import quantity
     */
    Integer importCustomerItems(CustomerQueryDTO customerQueryDTO);
}
