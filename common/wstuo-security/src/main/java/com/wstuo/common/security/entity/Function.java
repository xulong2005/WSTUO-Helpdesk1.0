package com.wstuo.common.security.entity;

import java.util.Set;

import javax.persistence.Cacheable;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * Entity class Function.
 * 
 */
@SuppressWarnings("serial")
@Entity
@Cacheable
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Table(name = "T_Function")
@PrimaryKeyJoinColumn(name = "resNo")
public class Function extends Resource {
	@ManyToOne
	@JoinColumn(name = "parentFunctionNo")
	private Function parentFunction;
	@OneToMany(mappedBy = "parentFunction", cascade = CascadeType.REMOVE)
	private Set<Function> subFunctions;
	@OneToMany(cascade = CascadeType.REMOVE)
	@JoinColumn(name = "functionNo")
	private Set<Operation> operations;

	public Function getParentFunction() {
		return parentFunction;
	}

	public void setParentFunction(Function parentFunction) {
		this.parentFunction = parentFunction;
	}

	public Set<Function> getSubFunctions() {
		return subFunctions;
	}

	public void setSubFunctions(Set<Function> subFunctions) {
		this.subFunctions = subFunctions;
	}

	public Set<Operation> getOperations() {
		return operations;
	}

	public void setOperations(Set<Operation> operations) {
		this.operations = operations;
	}

	/**
	 * 构造，添加数据.
	 * 
	 * @param resNo
	 * @param resName
	 * @param resCode
	 * @param resUrl
	 * @param parentFunctionNo
	 */
	public Function(Long resNo, String resCode, String resName, String resUrl, Function parentFunctionNo) {

		super.setResNo(resNo);
		super.setResName(resName);
		super.setResCode(resCode);
		super.setResUrl(resUrl);
		this.parentFunction = parentFunctionNo;
	}

	/**
	 * 构造，添加数据.
	 * 
	 * @param resName
	 * @param resCode
	 * @param resUrl
	 * @param parentFunctionNo
	 */
	public Function(String resName, String resCode, String resUrl, Function parentFunctionNo) {

		super.setResName(resName);
		super.setResCode(resCode);
		super.setResUrl(resUrl);
		this.parentFunction = parentFunctionNo;
	}

	public Function() {

	}
}
