package com.wstuo.common.security.dto;

import java.util.List;

import com.wstuo.common.security.entity.Holiday;
import com.wstuo.common.security.entity.ServiceTime;

/**
 * 工作时间(节假日、工作时间周、工作时间段)
 */
public class WorkingTimeDTO {
	private List<Holiday> holidays;
	private ServiceTime serviceTime;
	public List<Holiday> getHolidays() {
		return holidays;
	}
	public void setHolidays(List<Holiday> holidays) {
		this.holidays = holidays;
	}
	public ServiceTime getServiceTime() {
		return serviceTime;
	}
	public void setServiceTime(ServiceTime serviceTime) {
		this.serviceTime = serviceTime;
	}
	public WorkingTimeDTO(){
		super();
	}
	public WorkingTimeDTO(List<Holiday> holidays , ServiceTime serviceTime){
		this.holidays = holidays;
		this.serviceTime = serviceTime;
	}
}
