package com.wstuo.common.acl.service;

import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.security.acls.model.Acl;

import com.wstuo.common.acl.dto.AclDTO;

/**
 * ACL Service Interface Class
 * @author Administrator
 *
 */
public interface IAclsService {
	/**
	 * 创建ACL权限
	 * @param aclClass 类名
	 * @param identifier 标识码
	 */
	void createAcl(String aclClass, Long identifier);
	/**
	 * 创建ACL权限,默认赋予管理员限
	 * @param aclClass
	 * @param identifier
	 */
	void createAclEveryone(String aclClass, Long identifier);
	/**
	 * 删除ACL权限
	 * @param aclClass 类名
	 * @param identifier 标识码
	 */
	void deleteAcl(String aclClass, Long identifier);
	
	/**
	 * 查询当前账号查询拥有相应(读）权限的资源
	 * @param loginName 类名
	 * @param aclClass 标识码
	 * @param permissions 权限标识
	 * @return all acl resource
	 */
	Set<Long> findAclResourceId(String loginName,String aclClass,Integer[] permissions);
	
	/**
	 * 查询当前
	 * @param loginName
	 * @param aclClass
	 * @param identitys
	 * @return acl permissiion
	 */
	Map<String,String> findAclPermission(String loginName,String aclClass,Long[] identitys);
	
	/**
	 * 查询ACL
	 * @param aclClass 类名
	 * @param identifier 标识码
	 * @return acl
	 */
	Acl findAclByClassObjectId(String aclClass, Long identifier);
	
	/**
	 * 查询标识的权限
	 * @param identitys
	 * @param aclclass
	 * @return List<AclDTO>
	 */
	List<AclDTO> findAclByClzIds(Long[] identitys,String aclclass);
	
	/**
	 * 更新授权
	 * @param ids
	 * @param clz
	 * @param allowMasks
	 * @param rejectMasks
	 */
	void updatePermission(Long[] ids, String clz, String[] recipients, int[] allowMasks, int[] rejectMasks);
	
	/**
	 * 添加授权
	 * @param ids
	 * @param clz
	 * @param recipients
	 * @param masks
	 */
	void addPermission(Long[] ids, String clz, String[] recipients, int[] masks);
	
	/**
	 * 删除授权
	 * @param ids
	 * @param clz
	 * @param recipients
	 * @param masks
	 */
	void deletePermission(Long[] ids, String clz, String[] recipients,int[] masks);
	
}
