package com.wstuo.common.bpm.dao;

import com.wstuo.common.bpm.entity.FlowActivity;
import com.wstuo.common.dao.IEntityDAO;

/**
 * Flow Activity DAO Interface Class
 * @author wstuo
 *
 */
public interface IFlowActivityDAO extends IEntityDAO<FlowActivity> {
	/**
	 * 根据DeploymentId、activityType、activityName查询流程活动
	 * @param processDefinitionId
	 * @param activityType
	 * @param activityName
	 * @return FlowActivity
	 */
	FlowActivity findFlowActivityByDeploymentId(String processDefinitionId,String[] activityType,String activityName);
}
