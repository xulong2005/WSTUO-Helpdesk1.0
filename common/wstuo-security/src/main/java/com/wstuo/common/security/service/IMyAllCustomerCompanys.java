package com.wstuo.common.security.service;

import java.util.Set;

import com.wstuo.common.security.entity.Company;

public interface IMyAllCustomerCompanys {

	/**
	 * 我所在的公司和我负责的客户
	 * @return Set<Company>
	 */
	Set<Company> findCompanysMyAllCustomer(String loginName);
	
}
