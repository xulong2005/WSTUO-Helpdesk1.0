package com.wstuo.md5.action;

import java.io.BufferedInputStream;

import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Properties;

import org.apache.log4j.Logger;

/**
 * 测试主函数
 * 
 * @author Administrator
 * 
 */
public class TestMain {
	private static final Logger LOGGER = Logger.getLogger(TestMain.class);

	// 根据key读取value
	public static String readValue(String filePath, String key) {
		String value = null;
		Properties props = new Properties();
		try {
			InputStream in = new BufferedInputStream(new FileInputStream(
					filePath));
			props.load(in);
			value = props.getProperty(key);
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
		}
		return value;
	}
}