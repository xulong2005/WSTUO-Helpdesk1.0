package com.wstuo.common.config.attachment.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Lob;

import com.wstuo.common.entity.BaseEntity;
import com.wstuo.common.util.StringUtils;

/**
 * 附件实体
 * @author QXY
 * 
 */
@SuppressWarnings({ "serial", "rawtypes" })
@Entity
@Inheritance(strategy = InheritanceType.JOINED)
public class Attachment extends BaseEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long aid;
	@Column(nullable = false)
	private String url;
	@Column(nullable = false)
	private String attachmentName;
	@Column(name = "o_size")
	private Long size;
	@Column(nullable = true)
	private String type;
	@Lob
	private String attachmentContent;

	public String getAttachmentContentShort() {
		String result = null;
		if (attachmentContent != null) {
			result = StringUtils.cutStr(attachmentContent.trim(), 80,StringUtils.ELLIPSIS) ;
		}
		return result;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Long getAid() {
		return aid;
	}

	public void setAid(Long aid) {
		this.aid = aid;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public Long getSize() {
		return size;
	}

	public void setSize(Long size) {
		this.size = size;
	}

	public String getAttachmentName() {
		return attachmentName;
	}

	public void setAttachmentName(String attachmentName) {
		this.attachmentName = attachmentName;
	}

	public String getAttachmentContent() {
		return attachmentContent;
	}

	public void setAttachmentContent(String attachmentContent) {
		this.attachmentContent = attachmentContent;
	}

}
