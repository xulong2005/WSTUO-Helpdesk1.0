package com.wstuo.common.config.cab.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import com.wstuo.common.entity.BaseEntity;
import com.wstuo.common.security.entity.User;

/**
 * 变更审批委员会成员实体
 * @author WSTUO
 *
 */
@SuppressWarnings( {"serial","rawtypes"})
@Entity
public class CABMember extends BaseEntity{
	@Id
	@GeneratedValue( strategy = GenerationType.AUTO )
	private Long cabMemberId;
	@ManyToOne
	private User approvalMember; 
	@ManyToOne
	private User delegateMember;
	@ManyToOne
	private CAB cab;
	
	public Long getCabMemberId() {
		return cabMemberId;
	}
	public void setCabMemberId(Long cabMemberId) {
		this.cabMemberId = cabMemberId;
	}
	public User getApprovalMember() {
		return approvalMember;
	}
	public void setApprovalMember(User approvalMember) {
		this.approvalMember = approvalMember;
	}
	public User getDelegateMember() {
		return delegateMember;
	}
	public void setDelegateMember(User delegateMember) {
		this.delegateMember = delegateMember;
	}
	public CAB getCab() {
		return cab;
	}
	public void setCab(CAB cab) {
		this.cab = cab;
	}
	
}
