package com.wstuo.common.config.comment.entity;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.wstuo.common.config.attachment.entity.Attachment;
import com.wstuo.common.entity.BaseEntity;
import com.wstuo.common.security.entity.User;

@SuppressWarnings({ "serial", "rawtypes" })
@Entity
@Table(name="o_commentTb")
public class Comment extends BaseEntity{

	@Id
    @GeneratedValue( strategy = GenerationType.AUTO )
	private Long commentNo;
	
	@Column(name="stars")
    private String stars;//星级
	
	@Column(name="type")
	private String type;//类型
	
	
	public String getStars() {
		return stars;
	}

	public void setStars(String stars) {
		this.stars = stars;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	@ManyToMany(fetch=FetchType.LAZY)
	private List<Attachment> attachments;

	public Long getCommentNo() {
		return commentNo;
	}

	public void setCommentNo(Long commentNo) {
		this.commentNo = commentNo;
	}

	public List<Attachment> getAttachments() {
		return attachments;
	}

	public void setAttachments(List<Attachment> attachments) {
		this.attachments = attachments;
	}
	
	

}


