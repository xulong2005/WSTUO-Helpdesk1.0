package com.wstuo.common.config.cab.dao;

import com.wstuo.common.config.cab.dto.CABQueryDTO;
import com.wstuo.common.config.cab.entity.CAB;
import com.wstuo.common.dao.IEntityDAO;
import com.wstuo.common.dto.PageDTO;
/**
 * CAB DAO 接口类
 * @author WSTUO
 *
 */
public interface ICABDAO extends IEntityDAO<CAB> {
	/**
	 * CAB分页查询
	 * @param cabQueryDto
	 * @param start
	 * @param limit
	 * @return PageDTO
	 */
	PageDTO findPagerCAB(CABQueryDTO cabQueryDto,int start,int limit);
}
