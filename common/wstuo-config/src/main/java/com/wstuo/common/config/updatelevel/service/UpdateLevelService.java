package com.wstuo.common.config.updatelevel.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.wstuo.common.config.updatelevel.dao.IUpdateLevelDAO;
import com.wstuo.common.config.updatelevel.dto.UpdateLevelDTO;
import com.wstuo.common.config.updatelevel.dto.UpdateLevelQueryDTO;
import com.wstuo.common.config.updatelevel.entity.UpdateLevel;
import com.wstuo.common.dto.PageDTO;
import com.wstuo.common.security.dao.IUserDAO;


/**
 * UpdateLevelService service class
 * @author QXY
 *
 */
public class UpdateLevelService implements IUpdateLevelService{

	@Autowired
	public IUpdateLevelDAO updateLevelDAO;
	@Autowired
    public IUserDAO userDAO;
    
    
	/**
	 * 分页查询升级级别
	 * @param qdto
	 * @return PageDTO
	 */
    @Transactional()
    public PageDTO findPagerUpdateLevel(UpdateLevelQueryDTO qdto) {

        PageDTO p = updateLevelDAO.findPager(qdto);
        List<UpdateLevel> entities = (List<UpdateLevel>) p.getData();

        List<UpdateLevelDTO> dtos = new ArrayList<UpdateLevelDTO>(entities.size());

        for (UpdateLevel ul : entities) {

        	UpdateLevelDTO dto = new UpdateLevelDTO();
        	
            UpdateLevelDTO.entity2dto(ul, dto);
            
            if (ul.getApproval() != null) {

                dto.setApprovalName(ul.getApproval().getFullName());
                dto.setApprovalNo(ul.getApproval().getUserId());
             }

            dtos.add(dto);
        }

        p.setData(dtos);

        return p;
    }
    
    /**
	 * 保存升级级别
	 * @param dto
	 */
    @Transactional()
    public void saveUpdateLevel(UpdateLevelDTO dto){
    	
    	
    	UpdateLevel entity=new UpdateLevel();
    	UpdateLevelDTO.dto2entity(dto, entity);
    	
    	if(dto.getApprovalNo()!=null){
    		entity.setApproval(userDAO.findById(dto.getApprovalNo()));
    	}
    	updateLevelDAO.save(entity);
    	dto.setUlId(entity.getUlId());
    }
    
    /**
	 * 更新升级级别
	 * @param dto
	 */
    @Transactional()
    public void mergeUpdateLevel(UpdateLevelDTO dto) {

    	UpdateLevel entity=updateLevelDAO.findById(dto.getUlId());

    	if(entity!=null){
    		
    		UpdateLevelDTO.dto2entity(dto, entity);
        	
        	if(dto.getApprovalNo()!=null){
        		
        		entity.setApproval(userDAO.findById(dto.getApprovalNo()));
        	}
        	
        	updateLevelDAO.merge(entity);
    	}
    	
    }
    
    /**
	 * 删除升级级别
	 * @param ulIds
	 * @return boolean
	 */
    @Transactional()
    public boolean deleteUpdateLevel(Long[] ulIds){
    	boolean result = true;
        Byte delete=1;
    	for(Long id:ulIds){
    		UpdateLevel updateLevel=updateLevelDAO.findById(id);
    		if(updateLevel.getDataFlag().equals(delete)){
    			result=false;
    		}
    	}
    	if(result){
    		updateLevelDAO.deleteByIds(ulIds);
    	}
    	
        return result;
    }
    
    /**
     * 根据升级编号查找升级
     */
    @Transactional()
	public UpdateLevel findById(Long id){
		return updateLevelDAO.findById(id);
	}
}
