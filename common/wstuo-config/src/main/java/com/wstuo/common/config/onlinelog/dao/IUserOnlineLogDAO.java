package com.wstuo.common.config.onlinelog.dao;


import com.wstuo.common.config.onlinelog.dto.UserOnlineLogQueryDTO;
import com.wstuo.common.config.onlinelog.entity.UserOnlineLog;
import com.wstuo.common.dao.IEntityDAO;
import com.wstuo.common.dto.PageDTO;


/**
 * 用户在线日志DAO类接口.
 * @author QXY date 2011-02-11
 *
 */
public interface IUserOnlineLogDAO extends IEntityDAO<UserOnlineLog> {

	/**
	 * 根据会话ID查找在线日志
	 * @param sessionid 会话编号
	 * @return UserOnlineLog
	 */
    UserOnlineLog findBySessionId(String sessionid);

    /**
     * 分页查找用户在线日志
     * @param useronlinelogQueryDTO 查询DTO
     * @param start 开始数据行
     * @param limit 每页数据条数
     * @return PageDTO 分页数据
     */
    PageDTO findPager(final UserOnlineLogQueryDTO useronlinelogQueryDTO,
        int start, int limit);
}