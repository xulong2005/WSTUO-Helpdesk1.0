package com.wstuo.common.config.userCustom.action;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.opensymphony.xwork2.ActionSupport;
import com.wstuo.common.config.userCustom.dto.DashboardDTO;
import com.wstuo.common.config.userCustom.dto.DashboardQueryDTO;
import com.wstuo.common.config.userCustom.service.IDashboardService;
import com.wstuo.common.dto.PageDTO;
import com.wstuo.common.exception.ApplicationException;

/**
 * 面板action
 * @author WSTUO
 *
 */
@SuppressWarnings("serial")
public class DashboardAction extends ActionSupport {
	@Autowired
	private IDashboardService dashboardService;
	private DashboardDTO dashboardDTO;
	private Long[] ids;
	private List<DashboardDTO> lists=new ArrayList<DashboardDTO>();
	private PageDTO pageDTO=new PageDTO();
	private DashboardQueryDTO queryDTO=new DashboardQueryDTO();
	private String sidx;
	private String sord;
	private int page = 1;
    private int rows = 10;
	public DashboardDTO getDashboardDTO() {
		return dashboardDTO;
	}
	public void setDashboardDTO(DashboardDTO dashboardDTO) {
		this.dashboardDTO = dashboardDTO;
	}
	public Long[] getIds() {
		return ids;
	}
	public void setIds(Long[] ids) {
		this.ids = ids;
	}
	public List<DashboardDTO> getLists() {
		return lists;
	}
	public void setLists(List<DashboardDTO> lists) {
		this.lists = lists;
	}
	public IDashboardService getDashboardService() {
		return dashboardService;
	}
	public void setDashboardService(IDashboardService dashboardService) {
		this.dashboardService = dashboardService;
	}
	public PageDTO getPageDTO() {
		return pageDTO;
	}
	public void setPageDTO(PageDTO pageDTO) {
		this.pageDTO = pageDTO;
	}
	public String getSidx() {
		return sidx;
	}
	public void setSidx(String sidx) {
		this.sidx = sidx;
	}
	public String getSord() {
		return sord;
	}
	public void setSord(String sord) {
		this.sord = sord;
	}
	public int getPage() {
		return page;
	}
	public void setPage(int page) {
		this.page = page;
	}
	public int getRows() {
		return rows;
	}
	public void setRows(int rows) {
		this.rows = rows;
	}
	public DashboardQueryDTO getQueryDTO() {
		return queryDTO;
	}
	public void setQueryDTO(DashboardQueryDTO queryDTO) {
		this.queryDTO = queryDTO;
	}
	/**
	 * 查询全部面板
	 * @return String
	 */
	public String findDashboardAll(){

		lists=dashboardService.findDashboardAll();
		return SUCCESS;
	}
	
	/**
	 * 分页查询
	 * @return String
	 */
	public String findPageDashboard(){
		int start = ( page - 1 ) * rows;
		queryDTO.setStart(start);
		queryDTO.setLimit(rows);
		queryDTO.setSidx(sidx);
		queryDTO.setSord(sord);
		pageDTO=dashboardService.findPageDashboard(queryDTO);
		pageDTO.setPage( page );
		pageDTO.setRows( rows );
		return "pageDTO";
	}
	
	/**
	 * 保存面板
	 * @return String
	 */
	public String saveDashboard(){
		dashboardService.saveDashboard(dashboardDTO);
		return SUCCESS;
	}
	/**
	 * 编辑面板
	 * @return String
	 */
	public String editDashboard(){
		dashboardService.editDashboard(dashboardDTO);
		return SUCCESS;
	}
	/**
	 * 删除面板
	 * @return String
	 */
	public String deteleDashboard(){
		try{
			dashboardService.deteleDashboard(ids);
		}catch(Exception ex){
			throw new ApplicationException("ERROR_DATA_CAN_NOT_DELETE",ex);
		}
		return SUCCESS;
	}
	
	
}
