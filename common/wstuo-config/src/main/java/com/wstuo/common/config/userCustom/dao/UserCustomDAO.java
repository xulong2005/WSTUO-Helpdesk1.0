package com.wstuo.common.config.userCustom.dao;

import java.util.List;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import com.wstuo.common.config.userCustom.dto.UserCustomQueryDTO;
import com.wstuo.common.config.userCustom.entity.UserCustom;
import com.wstuo.common.dao.BaseDAOImplHibernate;
import com.wstuo.common.dto.PageDTO;
import com.wstuo.common.util.StringUtils;

/**
 * 用户自定义DAO
 * @author WSTUO
 *
 */
public  class UserCustomDAO extends BaseDAOImplHibernate<UserCustom> implements IUserCustomDAO {
	/**
	 * 用户自定义查询
	 * @param queryDTO
	 * @return List<UserCustom>
	 */
	@SuppressWarnings("unchecked")
	public List<UserCustom> findUserCustom(final UserCustomQueryDTO queryDTO){
		
		final DetachedCriteria dc = DetachedCriteria.forClass(UserCustom.class);
		if(queryDTO!=null){
			if(StringUtils.hasText(queryDTO.getLoginName())){
				dc.add(Restrictions.eq("loginName", queryDTO.getLoginName()));
			}
			if(StringUtils.hasText(queryDTO.getEventType())){
				dc.add(Restrictions.eq("eventType", queryDTO.getEventType()));
			}
			if(queryDTO.getCustomType()!=null){
				dc.add(Restrictions.eq("customType", queryDTO.getCustomType()));
			}
			if(StringUtils.hasText(queryDTO.getCustomResult())){
				dc.add(Restrictions.eq("customResult", queryDTO.getCustomResult()));
			}
			if(queryDTO.getUserCustomId()!=null){
				dc.add(Restrictions.eq("userCustomId", queryDTO.getUserCustomId()));
			}
		}
		
		//已优化
		return super.getHibernateTemplate().findByCriteria(dc);

//		PageDTO p=findPageByCriteria(dc, 0, 10000);
//		return (List<UserCustom>)p.getData();
	}
	
	/**
	 * 分页查询
	 * @param queryDTO
	 * @return PageDTO
	 */
	public PageDTO findPagerUserCustom(final UserCustomQueryDTO queryDTO){
		final DetachedCriteria dc = DetachedCriteria.forClass(UserCustom.class);
		Integer start = 0;
		Integer limit = 10;
		if(queryDTO!=null){
			start = queryDTO.getStart();
			limit = queryDTO.getLimit();
			if(StringUtils.hasText(queryDTO.getLoginName())){
				dc.add(Restrictions.eq("loginName", queryDTO.getLoginName()));
			}
			if(StringUtils.hasText(queryDTO.getEventType())){
				dc.add(Restrictions.eq("eventType", queryDTO.getEventType()));
			}
			if(queryDTO.getCustomType()!=null){
				dc.add(Restrictions.eq("customType", queryDTO.getCustomType()));
			}
			if(StringUtils.hasText(queryDTO.getCustomResult())){
				dc.add(Restrictions.eq("customResult", queryDTO.getCustomResult()));
			}
			
			//排序
	        if(StringUtils.hasText(queryDTO.getSord())&&StringUtils.hasText(queryDTO.getSidx())){
	        	if("desc".equals(queryDTO.getSord())){
	        		dc.addOrder(Order.desc(queryDTO.getSidx()));
	        	}else{
	        		dc.addOrder(Order.asc(queryDTO.getSidx()));
	        	}
	        }
		}
		return super.findPageByCriteria(dc,start,limit);
		
		
	}
	

	
	
}
