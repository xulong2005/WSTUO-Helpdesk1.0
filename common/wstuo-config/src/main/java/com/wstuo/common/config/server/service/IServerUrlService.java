package com.wstuo.common.config.server.service;

import com.wstuo.common.config.server.dto.ServerUrlDTO;

/**
 * ServerUrl Service interface class
 * @author Will
 *
 */
public interface IServerUrlService {

	
	public void saveOrUpdateServiceUrl(ServerUrlDTO dto);
	
	public ServerUrlDTO findServiceUrl();
}
