package com.wstuo.common.config.updatelevel.dao;

import com.wstuo.common.config.updatelevel.dto.UpdateLevelQueryDTO;
import com.wstuo.common.config.updatelevel.entity.UpdateLevel;
import com.wstuo.common.dao.IEntityDAO;
import com.wstuo.common.dto.PageDTO;
/**
 * UpdateLevel DAO interface class
 * @author QXY
 *
 */
public interface IUpdateLevelDAO extends IEntityDAO<UpdateLevel>{
	
	/**
	 * 分页查询升级级别
	 * @param qdto
	 * @return PageDTO
	 */
	 PageDTO findPager(UpdateLevelQueryDTO qdto);
}
