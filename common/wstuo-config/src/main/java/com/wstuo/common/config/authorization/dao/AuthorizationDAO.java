package com.wstuo.common.config.authorization.dao;



import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Restrictions;

import com.wstuo.common.config.authorization.dto.AuthorizationDTO;
import com.wstuo.common.config.authorization.entity.Authorization;
import com.wstuo.common.dao.BaseDAOImplHibernate;
import com.wstuo.common.dto.PageDTO;
import com.wstuo.common.util.DaoUtils;
import com.wstuo.common.util.StringUtils;

/**
 * Authorization DAO
 * @author Will
 *
 */

public class AuthorizationDAO extends BaseDAOImplHibernate<Authorization> implements IAuthorizationDAO{

	public PageDTO findPagerAuthorization(AuthorizationDTO authorizationDTO,
			int start, int limit, String sidx, String sord) {
		
		DetachedCriteria dc = DetachedCriteria.forClass( Authorization.class );
		if(authorizationDTO!=null){
			if ( StringUtils.hasText(authorizationDTO.getAuthName() ) )
            {
                dc.add( Restrictions.eq( "authName",authorizationDTO.getAuthType()) );
            }
			if(StringUtils.hasText(authorizationDTO.getAuthType())){
				dc.add(Restrictions.like("authType", authorizationDTO.getAuthType()));
			}
			
		}
		//排序
        dc = DaoUtils.orderBy(sidx, sord, dc);
		return super.findPageByCriteria( dc, start, limit );

	}

}
