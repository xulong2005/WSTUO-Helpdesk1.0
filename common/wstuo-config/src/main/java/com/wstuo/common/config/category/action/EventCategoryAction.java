package com.wstuo.common.config.category.action;

import java.io.File;
import java.io.InputStream;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.opensymphony.xwork2.ActionSupport;
import com.wstuo.common.config.category.dto.CategoryTreeViewDTO;
import com.wstuo.common.config.category.dto.EventCategoryDTO;
import com.wstuo.common.config.category.dto.PullDownTreeViewDTO;
import com.wstuo.common.config.category.service.IEventCategoryService;


/**
 * 请求分类的Action类
 * @author yeshu date:2010-09-10
 */
@SuppressWarnings("serial")
public class EventCategoryAction extends ActionSupport {

    private EventCategoryDTO eventCategoryDto;
    private CategoryTreeViewDTO categoryTreeViewDto;
    private PullDownTreeViewDTO pullDownTreeViewDTO;
    @Autowired
    private IEventCategoryService eventCategoryService;
    private String types;
    private String effect;
    private String flag;
    private InputStream exportStream;
    private String fileName="";
    private File importFile;
    private Long categoryId;
    private List<EventCategoryDTO> subcategorys;
    private Long categoryNos[];
    private Long maxViewLevel;
    private Long parentEventId;
    private List<CategoryTreeViewDTO> categoryTreeViewDtoList;
    private String pageFlag;
    private String msg;
    private int num=0;//显示条数
    private int start=0;//当前页数
    private Long eventId;
    
    
    
    public Long getEventId() {
		return eventId;
	}

	public void setEventId(Long eventId) {
		this.eventId = eventId;
	}

	public PullDownTreeViewDTO getPullDownTreeViewDTO() {
		return pullDownTreeViewDTO;
	}

	public void setPullDownTreeViewDTO(PullDownTreeViewDTO pullDownTreeViewDTO) {
		this.pullDownTreeViewDTO = pullDownTreeViewDTO;
	}

	public int getStart() {
		return start;
	}

	public void setStart(int start) {
		this.start = start;
	}

	public int getNum() {
		return num;
	}

	public void setNum(int num) {
		this.num = num;
	}

	public String getPageFlag() {
		return pageFlag;
	}

	public void setPageFlag(String pageFlag) {
		this.pageFlag = pageFlag;
	}

	public Long[] getCategoryNos() {
		return categoryNos;
	}

	public void setCategoryNos(Long[] categoryNos) {
		this.categoryNos = categoryNos;
	}
    
    public String getFlag() {
		return flag;
	}

	public void setFlag(String flag) {
		this.flag = flag;
	}

	public String getTypes() {

        return types;
    }

    public void setTypes(String types) {

        this.types = types;
    }

    public EventCategoryDTO getEventCategoryDto() {

        return eventCategoryDto;
    }

    public void setEventCategoryDto(EventCategoryDTO eventCategoryDto) {

        this.eventCategoryDto = eventCategoryDto;
    }

    public CategoryTreeViewDTO getCategoryTreeViewDto() {

        return categoryTreeViewDto;
    }

    public void setCategoryTreeViewDto(
        CategoryTreeViewDTO categoryTreeViewDto) {

        this.categoryTreeViewDto = categoryTreeViewDto;
    }

    public String getEffect() {
		return effect;
	}

	public void setEffect(String effect) {
		this.effect = effect;
	}

	public InputStream getExportStream() {
		return exportStream;
	}

	public void setExportStream(InputStream exportStream) {
		this.exportStream = exportStream;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public File getImportFile() {
		return importFile;
	}

	public void setImportFile(File importFile) {
		this.importFile = importFile;
	}

	
	
	public Long getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(Long categoryId) {
		this.categoryId = categoryId;
	}

	public List<EventCategoryDTO> getSubcategorys() {
		return subcategorys;
	}

	public void setSubcategorys(List<EventCategoryDTO> subcategorys) {
		this.subcategorys = subcategorys;
	}
	
	public Long getMaxViewLevel() {
		return maxViewLevel;
	}

	public void setMaxViewLevel(Long maxViewLevel) {
		this.maxViewLevel = maxViewLevel;
	}

	public Long getParentEventId() {
		return parentEventId;
	}

	public void setParentEventId(Long parentEventId) {
		this.parentEventId = parentEventId;
	}
	public List<CategoryTreeViewDTO> getCategoryTreeViewDtoList() {
		return categoryTreeViewDtoList;
	}

	public void setCategoryTreeViewDtoList(
			List<CategoryTreeViewDTO> categoryTreeViewDtoList) {
		this.categoryTreeViewDtoList = categoryTreeViewDtoList;
	}
	public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    /**
     * selete type is eventCategory
     * @return SUCCESS
     */
    public String getCategoryTree() {
    	if(parentEventId==null || parentEventId ==0){
    		categoryTreeViewDto = eventCategoryService.findEventCategoryTree(types, flag,parentEventId,maxViewLevel,pageFlag,num,start);
    		return SUCCESS;
    	}else{
    		categoryTreeViewDtoList = eventCategoryService.findEventCategoryTreeSub(types, flag,parentEventId,maxViewLevel,pageFlag,num,start);
    		return "categoryTreeViewDtoList";
    	}
    }
    
    /**
     *  select type is eventCategory by no login
     */
    public String getCategoryTreeClient(){
    	  if(eventCategoryService.findEventCategorys(types,flag,parentEventId,maxViewLevel,num,start).size()>0){
          	categoryTreeViewDto = eventCategoryService.findEventCategoryTree(types, flag,parentEventId,maxViewLevel,pageFlag,num,start);
          }
          return SUCCESS;
    }    

    /**
     * add eventCategory
     * @return SUCCESS
     */
    public String save() {
    	eventCategoryService.saveEventCategory(eventCategoryDto);
    	return SUCCESS;
    }
    
    public String isCategoryExisted() {
        boolean con = eventCategoryService.isCategoryExisted(eventCategoryDto);
        msg = con ? "isExist" : "";
        return "msg";
    }
    
    /**
      * 判断编辑时分类是否存在
      */
    public String isCategoryExistdOnEdit() {
        boolean con = eventCategoryService.isCategoryExistdOnEdit(eventCategoryDto);
        msg = con ? "isExist" : "";
        return "msg";
    }
    
    /**
     * update eventCategory
     * @return SUCCESS
     */
    public String update() {
    	eventCategoryService.updateEventCategory(eventCategoryDto);
        return SUCCESS;
    }
    
    /**
     * delete eventCategory
     * @return SUCCESS
     */
    public String remove() {
    	try{
    		eventCategoryService.removeEventCategory(eventCategoryDto.getEventId());
    		effect="1";
    	}catch(Exception ex){
    		effect="0";
    	}
        return "effect";
    }
    
    /**
     * move eventCategory
     * @return SUCCESS
     */
    public String changeParents() {

    	eventCategoryService.changeParent(eventCategoryDto);

        return SUCCESS;
    }
    
    /**
     * copyCateGory
     * @return String
     */
    public String copyCategory(){
    	eventCategoryDto=eventCategoryService.copyCategory(eventCategoryDto);
    	return SUCCESS;
    }
    
    /**
     * 导出数据.
     * @return String
     */
    public String exportEventCategory() {
    	fileName=types+"_category.csv";
        exportStream = eventCategoryService.exportEventCategory(types);
    	return "exportFileSuccessful";
    }
    
    /**
     * 导入数据.
     * @return String
     */
    public String importEventCategory(){
    	effect=eventCategoryService.importEventCategory(importFile);
    	return "importResult";
    }
    
    /**
     * 递归查找子分类.
     */
    public String findSubCategorys(){
    	subcategorys=eventCategoryService.findSubCategorys(categoryId);
    	return "subcategorys";
    }
    /**
     * 根据权限递归查找子分类.
     */
    public String findSubCategorysByResType(){
    	subcategorys=eventCategoryService.findSubCategorysByResType(categoryId,types);
    	return "subcategorys";
    }
    
    /**
     * find Category
     */
   public String findByIdCategorys(){
	   eventCategoryDto=eventCategoryService.findEventCategoryById(categoryId);
	   return "category";
   } 
   
   /**
    * 根据数组查询节点数据
    * */
   public String findSubCategoryArray(){
	   subcategorys=eventCategoryService.findSubCategoryArray(categoryNos);
   	   return "subcategorys";
   }
   
   /**
    * 根据分类编号数组查询分类信息
    * */
   public String findScheduledCategoryArray(){
	   	subcategorys=eventCategoryService.findScheduledCategoryArray(categoryNos);
	   	return "subcategorys";
	}
   
   /**
    * 更新所有的请求分类资源
    * @return String
    */
   public String updateRequestCaOperation(){
	   if(types.equals("request")){
		   eventCategoryService.syncRequestCategorys();
	   }else if(types.equals("knowledge"))
		   eventCategoryService.syncKnowledgeCategorys();
	   
	   return SUCCESS;
   } 
   
   /**
    * 获取下拉树所需数据
    * @return String
    */
   public String getCategoryCombotree(){
	   pullDownTreeViewDTO = eventCategoryService.getCategoryCombotreeData(types);
	   return "pullDownTreeViewDTO";
   }
   public String findLocationNameById(){
	   eventCategoryDto=eventCategoryService.findLocationNameById(eventId);
	   return "findLocationNameById";
   }
}