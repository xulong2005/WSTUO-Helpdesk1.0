package com.wstuo.common.config.customfilter.dao;

import com.wstuo.common.config.customfilter.entity.CustomExpression;
import com.wstuo.common.dao.IEntityDAO;




public interface IExpressionDAO extends IEntityDAO<CustomExpression>{

}
