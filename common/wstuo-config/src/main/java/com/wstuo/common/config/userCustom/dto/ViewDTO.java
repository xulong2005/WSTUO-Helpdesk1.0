package com.wstuo.common.config.userCustom.dto;

import com.wstuo.common.dto.BaseDTO;


/**
 *  viewDTO
 * @author Mark
 *
 */
@SuppressWarnings("serial")
public class ViewDTO extends BaseDTO{

	private Long viewId; // 视图ID
	private String viewName; //视图名称
	private String viewType; //视图内容类型
	private String viewContent;  //视图内容
	private Integer proportion;  //视图最大显示数据条数
	private String viewRow;
	private String viewCode;
	public Long getViewId() {
		return viewId;
	}
	public void setViewId(Long viewId) {
		this.viewId = viewId;
	}
	public String getViewName() {
		return viewName;
	}
	public void setViewName(String viewName) {
		this.viewName = viewName;
	}
	public String getViewType() {
		return viewType;
	}
	public void setViewType(String viewType) {
		this.viewType = viewType;
	}
	public String getViewContent() {
		return viewContent;
	}
	public void setViewContent(String viewContent) {
		this.viewContent = viewContent;
	}
	public Integer getProportion() {
		return proportion;
	}
	public void setProportion(Integer proportion) {
		this.proportion = proportion;
	}
	public String getViewRow() {
		return viewRow;
	}
	public void setViewRow(String viewRow) {
		this.viewRow = viewRow;
	}
	public String getViewCode() {
		return viewCode;
	}
	public void setViewCode(String viewCode) {
		this.viewCode = viewCode;
	}
	
	
}
