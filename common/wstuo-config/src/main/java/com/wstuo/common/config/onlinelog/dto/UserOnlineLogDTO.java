package com.wstuo.common.config.onlinelog.dto;

import java.util.Date;

import org.apache.commons.beanutils.BeanUtils;

import com.wstuo.common.config.onlinelog.entity.UserOnlineLog;
import com.wstuo.common.exception.ApplicationException;

/***
 * UserOnlineLogDTO class.
 * 
 * @author spring date 2010-10-11
 */
public class UserOnlineLogDTO {
	/**
	 * UserOnlineLog ID
	 */
	private Long id;

	/**
	 * UserOnlineLog session ID
	 */
	private String sessionId;

	/**
	 * UserOnlineLog User
	 */
	private String userName;

	/**
	 * UserOnlineLog OnlineTime
	 */
	private Date onlineTime;

	/**
	 * UserOnlineLog OfflineTime
	 */
	private Date offlineTime;

	/**
	 * UserOnlineLog Period
	 */
	private long period;

	/**
	 * UserOnlineLog IP
	 */
	private String ip;

	private Long companyNo;

	private String userFullName;

	public Long getCompanyNo() {
		return companyNo;
	}

	public void setCompanyNo(Long companyNo) {
		this.companyNo = companyNo;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getSessionId() {
		return sessionId;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public Date getOnlineTime() {
		return onlineTime;
	}

	public void setOnlineTime(Date onlineTime) {
		this.onlineTime = onlineTime;
	}

	public Date getOfflineTime() {
		return offlineTime;
	}

	public void setOfflineTime(Date offlineTime) {
		this.offlineTime = offlineTime;
	}

	public String getPeriod() {
		String s = "";
		if ((offlineTime != null) && (onlineTime != null)) {

			period = offlineTime.getTime() - onlineTime.getTime();

			int i = 0;
			int j = 0;
			long te = period / 60;
			long m = 0;

			for (i = 0; te >= 60;) {
				i++;
				te = te - 60;
			}
			m = period % 60;
			for (j = 0; i >= 24;) {
				j++;
				i = i - 24;
			}
			s = String.format("%d天%d小时%d分钟%d秒", j, i, te, m);
			return s;
		} else {

			return s;
		}
	}

	public void setPeriod(long period) {
		this.period = period;
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public String getUserFullName() {
		return userFullName;
	}

	public void setUserFullName(String userFullName) {
		this.userFullName = userFullName;
	}

	/**
	 * dto2entity
	 * @param dto
	 * @param entity
	 */
	public static void dto2entity(UserOnlineLogDTO dto, UserOnlineLog entity) {
		try {
			BeanUtils.copyProperties(entity, dto);
		} catch (Exception ex) {
			throw new ApplicationException(ex);
		}
	}

	/**
	 * entity2dto
	 * @param entity
	 * @param dto
	 */
	public static void entity2dto(UserOnlineLog entity, UserOnlineLogDTO dto) {
		try {
			BeanUtils.copyProperties(dto, entity);
		} catch (Exception ex) {
			throw new ApplicationException(ex);
		}
	}
}
