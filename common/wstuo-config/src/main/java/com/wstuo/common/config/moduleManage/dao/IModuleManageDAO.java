package com.wstuo.common.config.moduleManage.dao;

import java.util.List;

import com.wstuo.common.config.moduleManage.dto.ModuleManageDTO;
import com.wstuo.common.config.moduleManage.entity.ModuleManage;
import com.wstuo.common.dao.IEntityDAO;
import com.wstuo.common.dto.PageDTO;
/**
 * 模块管理DAO接口
 * @author Wstuo
 *
 */
public interface IModuleManageDAO extends IEntityDAO<ModuleManage>{
	/**
	 * 分页查询模块信息
	 * @param queryDto
	 * @param sord
	 * @param sidx
	 * @return PageDTO
	 */
	PageDTO findPager(ModuleManageDTO queryDto, String sord, String sidx);
	
	List<ModuleManage> findAllC();
	
	void saveC(ModuleManage entity);
}
