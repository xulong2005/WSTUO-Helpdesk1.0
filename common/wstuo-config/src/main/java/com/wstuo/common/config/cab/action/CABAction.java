package com.wstuo.common.config.cab.action;

import org.springframework.beans.factory.annotation.Autowired;

import com.opensymphony.xwork2.ActionSupport;
import com.wstuo.common.config.cab.dto.CABDTO;
import com.wstuo.common.config.cab.dto.CABQueryDTO;
import com.wstuo.common.config.cab.service.ICABService;
import com.wstuo.common.dto.PageDTO;

/**
 *  变更委员会Action类
 * @author WSTUO
 *
 */
@SuppressWarnings("serial")
public class CABAction extends ActionSupport {

	/**
	 * 变更委员会业务实现 接口
	 */
	@Autowired
	private ICABService cabService;
	/**
	 * 分页DTO 
	 */
	private PageDTO cabPager;
	/**
	 *  变更委员会DTO
	 */
	private CABDTO cabDto;
	/**
	 * 变更委员会查询DTO
	 */
	private CABQueryDTO cabQueryDto=new CABQueryDTO();
	/**
	 * 变更委员组ID数组
	 */
	private Long[] cabIds;
	/**
	 * 变更委员会ID
	 */
	private Long cabId;
	/**
	 * 页数
	 */
	private int page = 1;
	/**
	 * 记录数
	 */
    private int rows = 10;
    /**
     * 排序字段
     */
    private String sidx;
    /**
     * 排序规则
     */
    private String sord;
    
	public String getSidx() {
		return sidx;
	}
	public void setSidx(String sidx) {
		this.sidx = sidx;
	}
	public String getSord() {
		return sord;
	}
	public void setSord(String sord) {
		this.sord = sord;
	}
	public PageDTO getCabPager() {
		return cabPager;
	}
	public void setCabPager(PageDTO cabPager) {
		this.cabPager = cabPager;
	}
	public CABDTO getCabDto() {
		return cabDto;
	}
	public void setCabDto(CABDTO cabDto) {
		this.cabDto = cabDto;
	}
	public CABQueryDTO getCabQueryDto() {
		return cabQueryDto;
	}
	public void setCabQueryDto(CABQueryDTO cabQueryDto) {
		this.cabQueryDto = cabQueryDto;
	}
	public Long[] getCabIds() {
		return cabIds;
	}
	public void setCabIds(Long[] cabIds) {
		this.cabIds = cabIds;
	}
	public int getPage() {
		return page;
	}
	public void setPage(int page) {
		this.page = page;
	}
	public int getRows() {
		return rows;
	}
	public void setRows(int rows) {
		this.rows = rows;
	}
	
	public Long getCabId() {
		return cabId;
	}
	public void setCabId(Long cabId) {
		this.cabId = cabId;
	}
	
	/**
	 * 分页查询
	 * @return SUCCESS 分页DTO
	 */
	public String findPagerCAB(){
		cabQueryDto.setSidx(sidx);
		cabQueryDto.setSord(sord);
		int start = (page - 1) * rows;
		cabPager = cabService.findPagerCAB(cabQueryDto,start,rows);
		cabPager.setRows(rows);
        cabPager.setPage(page);
		return SUCCESS;
	}
	
	/**
	 * CAB保存 
	 * @return SUCCESS 无
	 */
	public String saveCAB(){
		cabService.saveCAB(cabDto);
		return SUCCESS;
	}
	
	/**
	 * 编辑CAB
	 * @return SUCCESS 无
	 */
	public String editCAB(){
		cabService.editCAB(cabDto);
		return SUCCESS;
	}
	
	/**
	 * 删除CAB
	 * @return SUCCESS 无
	 */
	public String deleteCAB(){
		cabService.deleteCAB(cabId,cabIds);
		return SUCCESS;
	}
	
	/**
	 * 根据CABID获取成员列表
	 * @return  String
	 */
	public String findCABMember(){
		cabPager=cabService.findPagerCABMember(cabId,page,rows);
		cabPager.setPage(page);
		cabPager.setRows(rows);
		return SUCCESS;
	}
}
