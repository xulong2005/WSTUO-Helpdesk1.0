package com.wstuo.common.config.systeminfo.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import com.wstuo.common.entity.BaseEntity;


/**
 * SystemInfo entity class
 * @author Will
 *
 */
@SuppressWarnings({ "serial", "rawtypes" })
@Entity
public class SystemInfo extends BaseEntity{
	@Id
	@GeneratedValue( strategy = GenerationType.AUTO )
	private Long id;
	private String systemDataVer;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getSystemDataVer() {
		return systemDataVer;
	}
	public void setSystemDataVer(String systemDataVer) {
		this.systemDataVer = systemDataVer;
	}
	
}
