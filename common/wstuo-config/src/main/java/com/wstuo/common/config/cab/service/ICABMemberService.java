package com.wstuo.common.config.cab.service;

import com.wstuo.common.config.cab.dto.CABMemberDTO;


/**
 * 变更审批委员会成员Service接口类
 * @author WSTUO
 *
 */
public interface ICABMemberService {
	
	/**
	 * 保存CABMember
	 * @param cabMembeDto
	 */
	public void saveCABMember(CABMemberDTO cabMembeDto);
	
	/**
	 * 编辑CABMember
	 * @param cabMembeDto
	 */
	public void editCABMember(CABMemberDTO cabMembeDto);
	
	/**
	 * 删除CABMember
	 * @param cabId
	 * @param cabMemberIds
	 */
	public void deleteCABMember(final Long cabId,final Long[] cabMemberIds);
}
