package com.wstuo.common.config.customfilter.service;

import java.util.List;

import com.wstuo.common.config.customfilter.dto.CustomExpressionDTO;


public interface IExpressionService {


	/**
	 * 添加表达式
	 * @param dto
	 */
	void save(CustomExpressionDTO dto);
	/**
	 * 批量添加表达式
	 * @param list
	 */
	void batchSave(List<CustomExpressionDTO> list);
	
}
