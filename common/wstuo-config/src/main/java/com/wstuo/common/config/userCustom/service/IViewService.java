package com.wstuo.common.config.userCustom.service;

import java.io.File;
import java.util.List;

import com.wstuo.common.config.userCustom.dto.UserCustomDTO;
import com.wstuo.common.config.userCustom.dto.ViewDTO;
import com.wstuo.common.config.userCustom.dto.ViewQueryDTO;
import com.wstuo.common.dto.PageDTO;

/**
 *  view service interface
 * @author Mark
 *
 */
public interface IViewService {
	/**
	 * 分页查找视图
	 * @param dto
	 * @return PageDTO
	 */
	PageDTO findPageView(ViewQueryDTO dto);
	/**
	 * 新增视图
	 * @param dto
	 * @return boolean
	 */
	void saveView(final ViewDTO dto);
	/**
	 * 编辑视图
	 * @param dto
	 */
	void updateView(final ViewDTO dto);
	/**
	 * 删除视图
	 * @param ids
	 * @return boolean
	 */
	boolean deleteView(final Long[] ids);
	/**
	 * 检查该用户是否已经存在视图
	 * @param dto
	 * @return Long
	 */
	boolean checkOutViewIsshow(ViewQueryDTO dto);
	/**
	 * 查询所有视图
	 * @param dto
	 * @return List<ViewDTO>
	 */
	List<ViewDTO> findAllView(ViewQueryDTO dto);
	/**
	 * 导出视图数据
	 * @param importFile
	 * @return String
	 */
	
	String importViewData(File importFile);
	/**
	 *   查询所有
	 * @return List<ViewDTO>
	 */
	List<ViewDTO> findAllSystemView();
	/**
	 * 查询所有视图和用户视图
	 * @param dto
	 * @return UserCustomDTO
	 */
	UserCustomDTO findAllViewAndUserView(ViewQueryDTO dto);
	/**
	 * 删除视图
	 * @param viewDTO
	 * @return boolean
	 */
	boolean updateViewProportionById(ViewDTO viewDTO);
	
	/**
	 *  导入系统默认数据
	 * @param importFile 导入文件
	 * @param defaultFilterId 默认的过滤器视图ID
	 * @param defaultReportId 默认的自定义视图ID
	 * @return String 
	 */
	String importSystemDefautlViewData(File importFile,Long defaultFilterId,Long defaultReportId);
	
	/**
	 * 升级更新用户自定义表 viewCode 
	 * @author will
	 */
	void upateViewCode();
}
