package com.wstuo.common.config.server.dao;

import com.wstuo.common.config.server.entity.ServerUrl;
import com.wstuo.common.dao.IEntityDAO;
/**
 * ServerUrl DAO interface class
 * @author will
 *
 */
public interface IServerUrlDAO extends IEntityDAO<ServerUrl>{

}
