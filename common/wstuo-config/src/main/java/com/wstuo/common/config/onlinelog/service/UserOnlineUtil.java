package com.wstuo.common.config.onlinelog.service;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import com.wstuo.common.config.onlinelog.entity.UserOnlineLog;


public class UserOnlineUtil {
	//在线终端用户
	public static final String ENDUSER="ENDUSER";
	//在线技术员用户
	public static final String TECHNICIAN="TECHNICIAN";
	private static Map<String, Map> userOnlineMap; 
	private static UserOnlineUtil userOnline;
	
	public static UserOnlineUtil getInstance(){
		if(userOnlineMap==null){
			userOnlineMap = new HashMap<String, Map>();
		}
		if(userOnline==null)
		userOnline = new UserOnlineUtil();
		return userOnline;
	} 
	
	
	public void put(String key, UserOnlineLog userOnlineLog, String userRole){
		//UserRole -> Set<LoginName>
		Map<String, Set<UserOnlineLog>> tenantValue = userOnlineMap.get(key);
		if(tenantValue==null){
			tenantValue = new HashMap<String,Set<UserOnlineLog>>();
		}
		
		//Set<LoginName>
		Set<UserOnlineLog> userRoleValue = tenantValue.get(userRole); 
		if(userRoleValue==null){
			userRoleValue = new HashSet<UserOnlineLog>();
		}
		userRoleValue.add(userOnlineLog);
		
		tenantValue.put(userRole, userRoleValue);
		
		userOnlineMap.put(key,tenantValue);
	}
	
	public Object get(String key){
		return userOnlineMap.get(key);
	}
	
	
	public boolean remove(String key, UserOnlineLog userOnlineLog, String userRole){
		boolean  result = false;
		//UserRole -> Set<LoginName>
		Map<String, Set<UserOnlineLog>> tenantValue = userOnlineMap.get(key);
		if(tenantValue!=null){
			Set<UserOnlineLog> userRoleValue = tenantValue.get(userRole); 
			if(userRoleValue!=null){
				result = userRoleValue.remove(userOnlineLog);
			}
			tenantValue.put(userRole, userRoleValue);
			userOnlineMap.put(key, tenantValue);
		}
		return result;
	}
	
	
}
