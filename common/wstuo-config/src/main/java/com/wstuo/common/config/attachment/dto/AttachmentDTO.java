package com.wstuo.common.config.attachment.dto;

import java.util.Date;

import com.wstuo.common.dto.BaseDTO;
/**
 * 附件信息
 * @author QXY
 *
 */
@SuppressWarnings("serial")
public class AttachmentDTO extends BaseDTO{

	private Long aid;
	private String url;
	private Long size;
	private String attachmentName;
	private String description;
	private String creator;
	private String createTime;
	private Date startTime;
	private Date endTime;
	private String startSize;
	private String endSize;
	private String type;
	
	
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getStartSize() {
		return startSize;
	}
	public void setStartSize(String startSize) {
		this.startSize = startSize;
	}
	public String getEndSize() {
		return endSize;
	}
	public void setEndSize(String endSize) {
		this.endSize = endSize;
	}
	public Date getStartTime() {
		return startTime;
	}
	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}
	public Date getEndTime() {
		return endTime;
	}
	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}
	public String getCreateTime() {
		return createTime;
	}
	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}
	public String getCreator() {
		return creator;
	}
	public void setCreator(String creator) {
		this.creator = creator;
	}
	public String getAttachmentName() {
		return attachmentName;
	}
	public void setAttachmentName(String attachmentName) {
		this.attachmentName = attachmentName;
	}
	public Long getAid() {
		return aid;
	}
	public void setAid(Long aid) {
		this.aid = aid;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public Long getSize() {
		return size;
	}
	public void setSize(Long size) {
		this.size = size;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
	
	
	
	
}
