package com.wstuo.common.config.cab.dao;


import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import com.wstuo.common.config.cab.dto.CABQueryDTO;
import com.wstuo.common.config.cab.entity.CAB;
import com.wstuo.common.dao.BaseDAOImplHibernate;
import com.wstuo.common.dto.PageDTO;
import com.wstuo.common.util.StringUtils;

/**
 * CAB DAO 类
 * @author WSTUO
 *
 */
public class CABDAO extends BaseDAOImplHibernate<CAB> implements ICABDAO {
	/**
	 * CAB分页条件查询
	 * @param cabQueryDto
	 * @param start
	 * @param limit
	 * @return PageDTO
	 *
	 */
	public PageDTO findPagerCAB(CABQueryDTO cabQueryDto, int start, int limit) {
		 final DetachedCriteria dc = DetachedCriteria.forClass(CAB.class);
	        if (cabQueryDto != null) {

	            if (StringUtils.hasText(cabQueryDto.getCabName())) {
	                 dc.add(Restrictions.like("cabName", cabQueryDto.getCabName(),MatchMode.ANYWHERE));
	            }
	            
	            if(cabQueryDto.getCabDesc()!=null){
	            	
	            	 dc.add(Restrictions.like("cabDesc", cabQueryDto.getCabName(),MatchMode.ANYWHERE));
	            }
	            
	            
	            
	    	 		
	   	 		 //排序
	   	        if(StringUtils.hasText(cabQueryDto.getSord())&&StringUtils.hasText(cabQueryDto.getSidx())){

	   	        	if("desc".equals(cabQueryDto.getSord())){
	   	        		
	   	        		 dc.addOrder(Order.desc(cabQueryDto.getSidx()));
	   	        		 
	   	        	}else{
	   	        		 dc.addOrder(Order.asc(cabQueryDto.getSidx()));
	   	        	}
	   	        }else{
	   	        
	   	        	dc.addOrder(Order.desc("cabId"));
	   	        }

	            
	            
	        }
	        
	        return super.findPageByCriteria(dc, start, limit);
	}

}
