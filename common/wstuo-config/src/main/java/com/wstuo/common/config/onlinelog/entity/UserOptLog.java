package com.wstuo.common.config.onlinelog.entity;

import com.wstuo.common.security.entity.Operation;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;

/**
 * user opt log entity class
 * @author QXY
 *
 */
@Entity
@Inheritance( strategy = InheritanceType.JOINED )
public class UserOptLog
{
    /**
    * UserOptLog ID
    */
    @Id
    @GeneratedValue( strategy = GenerationType.AUTO )
    private Long id;

    /**
    * UserOptLog UserName
    */
    private String userName;
    
    private String userFullName;
    

    public String getUserFullName() {
		return userFullName;
	}

	public void setUserFullName(String userFullName) {
		this.userFullName = userFullName;
	}

	public String getResourceName(  )
    {
        return resourceName;
    }

    public void setResourceName( String resourceName )
    {
        this.resourceName = resourceName;
    }

    /**
    * UserOptLog Operation
    */
    private Operation operation;

    /**
    * UserOptLog MethodName
    */
    private String methodName;

    /**
    * UserOptLog ResourceId
    */
    private Long resourceId;

    /**
     * the resource name which the user was in operation.
     */
    private String resourceName;

    /**
    * UserOptLog Time
    */
    private Date time;

    /**
     * UserOptLog CompanyNo
     */
    private Long companyNo;

    /**
     * Execution Time.
     */
    private Long execTime;

    public Long getExecTime(  )
    {
        return execTime;
    }

    public void setExecTime( Long execTime )
    {
        this.execTime = execTime;
    }

    public Long getCompanyNo(  )
    {
        return companyNo;
    }

    public void setCompanyNo( Long companyNo )
    {
        this.companyNo = companyNo;
    }

    public String getMethodName(  )
    {
        return methodName;
    }

    public void setMethodName( String methodName )
    {
        this.methodName = methodName;
    }

    public Long getId(  )
    {
        return id;
    }

    public void setId( Long id )
    {
        this.id = id;
    }

    public String getUserName(  )
    {
        return userName;
    }

    public void setUserName( String userName )
    {
        this.userName = userName;
    }

    public Operation getOperation(  )
    {
        return operation;
    }

    public void setOperation( Operation operation )
    {
        this.operation = operation;
    }

    public Long getResourceId(  )
    {
        return resourceId;
    }

    public void setResourceId( Long resourceId )
    {
        this.resourceId = resourceId;
    }

    public Date getTime(  )
    {
        return time;
    }

    public void setTime( Date time )
    {
        this.time = time;
    }
}
