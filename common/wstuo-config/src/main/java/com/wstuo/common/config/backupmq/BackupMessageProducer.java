package com.wstuo.common.config.backupmq;

import javax.jms.Destination;

import org.springframework.jms.core.JmsTemplate;

import com.wstuo.common.config.backup.dto.BackupFileDTO;


/**
 * 发送消息
 * @author WSTUO
 *
 */
public class BackupMessageProducer {
	private JmsTemplate template;

	private Destination destination;


	public void setTemplate(JmsTemplate template) {
	   this.template = template;
	}

	public void setDestination(Destination destination) {
	   this.destination = destination;
	}

	public void send(BackupFileDTO order) {
	  template.convertAndSend(this.destination, order);
	} 
}
