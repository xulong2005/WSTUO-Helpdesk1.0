package com.wstuo.common.config.onlinelog.dto;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
/**
 * ADTO interface
 * @author QXY
 *
 */
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface ADTO {
    String id();
}