package com.wstuo.common.sla.dto;

import com.wstuo.common.dto.BaseDTO;

/**
 * Promote Rule DTO
 * 
 * @author QXY
 * 
 */
@SuppressWarnings("serial")
public class PromoteRuleDTO extends BaseDTO {
	private Long ruleNo;
	private String ruleName;
	private Boolean beforeOrAfter = false;
	private String beforeOrAfterStr;
	private Long ruleTime;
	private String timeStr;
	private Boolean isIncludeHoliday = false;
	private String assigneeName;
	private Long contractNo;
	private Byte dataFlag;
	private Long updateLevelNo;//用户ID
	private String referType;

	private String ruleType;

	// private Boolean ruleOverdue;

	public String getRuleType() {
		return ruleType;
	}

	public void setRuleType(String ruleType) {
		this.ruleType = ruleType;
	}

	public Long getUpdateLevelNo() {
		return updateLevelNo;
	}

	public void setUpdateLevelNo(Long updateLevelNo) {
		this.updateLevelNo = updateLevelNo;
	}

	public Long getContractNo() {
		return contractNo;
	}

	public void setContractNo(Long contractNo) {
		this.contractNo = contractNo;
	}

	public Long getRuleNo() {
		return ruleNo;
	}

	public void setRuleNo(Long ruleNo) {
		this.ruleNo = ruleNo;
	}

	public String getRuleName() {
		return ruleName;
	}

	public void setRuleName(String ruleName) {
		this.ruleName = ruleName;
	}

	public Boolean getIsIncludeHoliday() {
		return isIncludeHoliday;
	}

	public void setIsIncludeHoliday(Boolean isIncludeHoliday) {
		this.isIncludeHoliday = isIncludeHoliday;
	}

	

	public String getAssigneeName() {
		return assigneeName;
	}

	public void setAssigneeName(String assigneeName) {
		this.assigneeName = assigneeName;
	}

	public Boolean getBeforeOrAfter() {
		return beforeOrAfter;
	}

	public void setBeforeOrAfter(Boolean beforeOrAfter) {
		this.beforeOrAfter = beforeOrAfter;
	}


	public Long getRuleTime() {
        return ruleTime;
    }

    public void setRuleTime(Long ruleTime) {
        this.ruleTime = ruleTime;
    }

    public String getBeforeOrAfterStr() {

		if (beforeOrAfter) {

			return "逾期后升级";

		} else {

			return "逾期前升级";
		}

	}

	public void setBeforeOrAfterStr(String beforeOrAfterStr) {
		this.beforeOrAfterStr = beforeOrAfterStr;
	}

	public String getTimeStr() {

		Long day = ruleTime / 86400;

		Long hour = (ruleTime % 86400) / (60 * 60);

		Long mintue = (ruleTime % 3600) / 60;

		return day + "天" + hour + "小时" + mintue + "分钟";
	}

	public void setTimeStr(String timeStr) {
		this.timeStr = timeStr;
	}

	public Byte getDataFlag() {
		return dataFlag;
	}

	public void setDataFlag(Byte dataFlag) {
		this.dataFlag = dataFlag;
	}

	public PromoteRuleDTO() {

	}

	public PromoteRuleDTO(Long ruleNo, String ruleName, Boolean beforeOrAfter,
			String beforeOrAfterStr, Long ruleTime, String timeStr,
			Boolean isIncludeHoliday, String assigneeName,
			Long contractNo,String ruleType,Long updateLevelNo) {
		super();
		this.ruleNo = ruleNo;
		this.ruleName = ruleName;
		this.beforeOrAfter = beforeOrAfter;
		this.beforeOrAfterStr = beforeOrAfterStr;
		this.ruleTime = ruleTime;
		this.timeStr = timeStr;
		this.isIncludeHoliday = isIncludeHoliday;
		this.assigneeName = assigneeName;
		this.contractNo = contractNo;
		this.ruleType = ruleType;
		this.updateLevelNo = updateLevelNo;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((assigneeName == null) ? 0 : assigneeName.hashCode());
		
		result = prime * result
				+ ((beforeOrAfter == null) ? 0 : beforeOrAfter.hashCode());
		result = prime
				* result
				+ ((beforeOrAfterStr == null) ? 0 : beforeOrAfterStr.hashCode());
		result = prime * result
				+ ((contractNo == null) ? 0 : contractNo.hashCode());
		result = prime * result
				+ ((dataFlag == null) ? 0 : dataFlag.hashCode());
		result = prime
				* result
				+ ((isIncludeHoliday == null) ? 0 : isIncludeHoliday.hashCode());
		result = prime * result
				+ ((referType == null) ? 0 : referType.hashCode());
		result = prime * result
				+ ((ruleName == null) ? 0 : ruleName.hashCode());
		result = prime * result + ((ruleNo == null) ? 0 : ruleNo.hashCode());
		result = prime * result
				+ ((ruleTime == null) ? 0 : ruleTime.hashCode());
		result = prime * result
				+ ((ruleType == null) ? 0 : ruleType.hashCode());
		result = prime * result + ((timeStr == null) ? 0 : timeStr.hashCode());
		result = prime * result
				+ ((updateLevelNo == null) ? 0 : updateLevelNo.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PromoteRuleDTO other = (PromoteRuleDTO) obj;
		if (assigneeName == null) {
			if (other.assigneeName != null)
				return false;
		} else if (!assigneeName.equals(other.assigneeName))
			return false;
		if (beforeOrAfter == null) {
			if (other.beforeOrAfter != null)
				return false;
		} else if (!beforeOrAfter.equals(other.beforeOrAfter))
			return false;
		if (beforeOrAfterStr == null) {
			if (other.beforeOrAfterStr != null)
				return false;
		} else if (!beforeOrAfterStr.equals(other.beforeOrAfterStr))
			return false;
		if (contractNo == null) {
			if (other.contractNo != null)
				return false;
		} else if (!contractNo.equals(other.contractNo))
			return false;
		if (dataFlag == null) {
			if (other.dataFlag != null)
				return false;
		} else if (!dataFlag.equals(other.dataFlag))
			return false;
		if (isIncludeHoliday == null) {
			if (other.isIncludeHoliday != null)
				return false;
		} else if (!isIncludeHoliday.equals(other.isIncludeHoliday))
			return false;
		if (referType == null) {
			if (other.referType != null)
				return false;
		} else if (!referType.equals(other.referType))
			return false;
		if (ruleName == null) {
			if (other.ruleName != null)
				return false;
		} else if (!ruleName.equals(other.ruleName))
			return false;
		if (ruleNo == null) {
			if (other.ruleNo != null)
				return false;
		} else if (!ruleNo.equals(other.ruleNo))
			return false;
		if (ruleTime == null) {
			if (other.ruleTime != null)
				return false;
		} else if (!ruleTime.equals(other.ruleTime))
			return false;
		if (ruleType == null) {
			if (other.ruleType != null)
				return false;
		} else if (!ruleType.equals(other.ruleType))
			return false;
		if (timeStr == null) {
			if (other.timeStr != null)
				return false;
		} else if (!timeStr.equals(other.timeStr))
			return false;
		if (updateLevelNo == null) {
			if (other.updateLevelNo != null)
				return false;
		} else if (!updateLevelNo.equals(other.updateLevelNo))
			return false;
		return true;
	}

	public String getReferType() {
		return referType;
	}

	public void setReferType(String referType) {
		this.referType = referType;
	}

}