package com.wstuo.common.sla.dto;

import com.wstuo.common.dto.AbstractValueObject;

/**
 * SLA Contract Query DTO
 * @author QXY
 *
 */
@SuppressWarnings("serial")
public class SLAContractQueryDTO extends AbstractValueObject {

    private String contractName;
    private String orgName;
    private String dname;
    private Long orgNo;
    private Long dcode;
    private Long[] orgNos;
    private Long[] dirNos;
    private Long contractNo;
    /**
    * Page start*/
    private Integer start;
    /**
     * Page limit*/
    private Integer limit;
    private Boolean isDefault = false;//是否默认

    
    public Long[] getDirNos() {
		return dirNos;
	}

	public void setDirNos(Long[] dirNos) {
		this.dirNos = dirNos;
	}

	public Boolean getIsDefault() {
		return isDefault;
	}

	public void setIsDefault(Boolean isDefault) {
		this.isDefault = isDefault;
	}

	public Long getContractNo() {
		return contractNo;
	}

	public void setContractNo(Long contractNo) {
		this.contractNo = contractNo;
	}

	public Long[] getOrgNos() {
		return orgNos;
	}

	public void setOrgNos(Long[] orgNos) {
		this.orgNos = orgNos;
	}

	public String getContractName() {

        return contractName;
    }

    public void setContractName(String contractName) {

        this.contractName = contractName;
    }

    public String getOrgName() {

        return orgName;
    }

    public void setOrgName(String orgName) {

        this.orgName = orgName;
    }

    public String getDname() {

        return dname;
    }

    public void setDname(String dname) {

        this.dname = dname;
    }

    public Integer getStart() {
		return start;
	}

	public void setStart(Integer start) {
		this.start = start;
	}

	public Integer getLimit() {
		return limit;
	}

	public void setLimit(Integer limit) {
		this.limit = limit;
	}

	public Long getOrgNo() {

        return orgNo;
    }

    public void setOrgNo(Long orgNo) {

        this.orgNo = orgNo;
    }

    public Long getDcode() {

        return dcode;
    }

    public void setDcode(Long dcode) {

        this.dcode = dcode;
    }
}